package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


public class RangeModel {

    /**
     **/
    @SerializedName("min")
    private Integer min = null;

    /**
     **/
    @SerializedName("max")
    private Integer max = null;


    public Integer getMin() {
        return min;
    }


    public void setMin(Integer min) {
        this.min = min;
    }


    public Integer getMax() {
        return max;
    }


    public void setMax(Integer max) {
        this.max = max;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class RangeModel {\n");

        sb.append("  min: ").append(min).append("\n");
        sb.append("  max: ").append(max).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
