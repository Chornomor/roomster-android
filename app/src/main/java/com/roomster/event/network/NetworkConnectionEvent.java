package com.roomster.event.network;


import com.roomster.event.Event;


/**
 * Created by Bogoi
 */
public class NetworkConnectionEvent extends Event {

    public static final class NetworkConnectionAvailable extends NetworkConnectionEvent {

    }


    public static final class NetworkConnectionUnavailable extends NetworkConnectionEvent {

    }
}
