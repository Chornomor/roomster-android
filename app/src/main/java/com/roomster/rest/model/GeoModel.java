package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


public class GeoModel {

    /**
     * Latitude South West
     **/
    @SerializedName("lat_sw")
    private Double latSw = null;

    /**
     * Longitude South West
     **/
    @SerializedName("lng_sw")
    private Double lngSw = null;

    /**
     * Latitude North East
     **/
    @SerializedName("lat_ne")
    private Double latNe = null;

    /**
     * Longitude North East
     **/
    @SerializedName("lng_ne")
    private Double lngNe = null;

    /**
     * Must be betweeen 0.1 and 2. Default: 1. maximum: 2.0
     **/
    @SerializedName("radius_scale")
    private Float radiusScale = null;


    public Double getLatSw() {
        return latSw;
    }


    public void setLatSw(Double latSw) {
        this.latSw = latSw;
    }


    public Double getLngSw() {
        return lngSw;
    }


    public void setLngSw(Double lngSw) {
        this.lngSw = lngSw;
    }


    public Double getLatNe() {
        return latNe;
    }


    public void setLatNe(Double latNe) {
        this.latNe = latNe;
    }


    public Double getLngNe() {
        return lngNe;
    }


    public void setLngNe(Double lngNe) {
        this.lngNe = lngNe;
    }


    public Float getRadiusScale() {
        return radiusScale;
    }


    public void setRadiusScale(Float radiusScale) {
        this.radiusScale = radiusScale;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class GeoModel {\n");

        sb.append("  latSw: ").append(latSw).append("\n");
        sb.append("  lngSw: ").append(lngSw).append("\n");
        sb.append("  latNe: ").append(latNe).append("\n");
        sb.append("  lngNe: ").append(lngNe).append("\n");
        sb.append("  radiusScale: ").append(radiusScale).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
