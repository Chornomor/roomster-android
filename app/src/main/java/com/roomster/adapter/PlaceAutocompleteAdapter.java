/*
 * Copyright (C) 2015 Google Inc. All Rights Reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.roomster.adapter;


import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataBufferUtils;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLngBounds;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.roomster.R;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class PlaceAutocompleteAdapter extends BaseAdapter implements Filterable {

    private static final CharacterStyle CHARACTER_STYLE = new StyleSpan(Typeface.NORMAL);

    private ArrayList<AutocompletePrediction> mResultList;
    private GoogleApiClient                   mGoogleApiClient;
    private LatLngBounds                      mBounds;
    private AutocompleteFilter                mPlaceFilter;
    private Context                           mContext;


    public PlaceAutocompleteAdapter(Context context, GoogleApiClient googleApiClient, LatLngBounds bounds,
                                    AutocompleteFilter filter) {
        mContext = context;
        mGoogleApiClient = googleApiClient;
        mBounds = bounds;
        mPlaceFilter = filter;
        mResultList = new ArrayList<>();
    }


    @Override
    public int getCount() {
        return mResultList != null ? mResultList.size() : 0;
    }


    @Override
    @Nullable
    public AutocompletePrediction getItem(int position) {
        if (mResultList != null && position < mResultList.size()) {
            return mResultList.get(position);
        }

        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.list_item_place_autocomplete, parent, false);
            convertView.setTag(new ViewHolder(convertView));
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();
        AutocompletePrediction item = getItem(position);
        if (item != null) {
            CharSequence primaryText = item.getPrimaryText(CHARACTER_STYLE);
            if (primaryText != null) {
                holder.primaryText.setText(primaryText);
            }
            CharSequence secondaryText = item.getSecondaryText(CHARACTER_STYLE);
            if (secondaryText != null) {
                holder.secondaryText.setText(secondaryText);
            }
        }
        return convertView;
    }


    @Override
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint != null) {
                    ArrayList<AutocompletePrediction> autocompleteList = getAutocomplete(constraint);
                    if (autocompleteList != null) {
                        results.values = autocompleteList;
                        results.count = autocompleteList.size();
                    }
                }
                return results;
            }


            @Override
            @SuppressWarnings("unchecked")
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    if (results.values instanceof ArrayList<?>) {
                        mResultList = (ArrayList<AutocompletePrediction>) results.values;
                        notifyDataSetChanged();
                    }
                } else {
                    notifyDataSetInvalidated();
                }
            }


            @Override
            public CharSequence convertResultToString(Object resultValue) {
                if (resultValue instanceof AutocompletePrediction) {
                    return ((AutocompletePrediction) resultValue).getFullText(null);
                } else {
                    return super.convertResultToString(resultValue);
                }
            }
        };
    }


    private ArrayList<AutocompletePrediction> getAutocomplete(CharSequence constraint) {
        if (mGoogleApiClient.isConnected()) {
            PendingResult<AutocompletePredictionBuffer> results = Places.GeoDataApi
              .getAutocompletePredictions(mGoogleApiClient, constraint.toString(), mBounds, mPlaceFilter);
            AutocompletePredictionBuffer autocompletePredictions = results.await(60, TimeUnit.SECONDS);
            final Status status = autocompletePredictions.getStatus();
            if (!status.isSuccess()) {
                autocompletePredictions.release();
                return null;
            }
            return DataBufferUtils.freezeAndClose(autocompletePredictions);
        }
        return null;
    }

    private class ViewHolder {

        TextView primaryText;
        TextView secondaryText;
        View     root;

        ViewHolder(View root) {
            this.root = root;
            primaryText = (TextView) root.findViewById(R.id.list_place_autocomplete_primary_text);
            secondaryText = (TextView) root.findViewById(R.id.list_place_autocomplete_secondary_text);
        }
    }
}