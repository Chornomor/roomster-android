package com.roomster.dialog;


import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by michaelkatkov on 12/29/15.
 */
public class ImageChooserDialog extends android.support.v7.app.AlertDialog {

    @Inject
    LayoutInflater mInflater;

    @Bind(R.id.image_choose_from_facebook)
    TextView mFKTv;

    @Bind(R.id.image_choose_from_camera)
    TextView mCameraTv;

    @Bind(R.id.image_choose_from_gallery)
    TextView mGalleryTv;

    private Listener mListener;


    public ImageChooserDialog(Context context, Listener listener) {
        super(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
        mListener = listener;
        initViews();
        initButtons();
        initCancelListener();
    }


    private void initViews() {
        View contentView = mInflater.inflate(R.layout.dialog_image_chooser, null);
        ButterKnife.bind(this, contentView);
        setView(contentView);
    }


    private void initButtons() {
        setButton(BUTTON_NEGATIVE, getContext().getString(R.string.report_user_dialog_cancel_button), new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mListener != null) {
                    mListener.onCancel();
                }
            }
        });
    }


    private void initCancelListener() {
        setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                if (mListener != null) {
                    mListener.onCancel();
                }
            }
        });
    }


    @OnClick(R.id.image_choose_from_facebook)
    public void onFacebookClick() {
        if (mListener != null) {
            mListener.onFacebookClick();
        }
    }


    @OnClick(R.id.image_choose_from_camera)
    public void onCameraClick() {
        if (mListener != null) {
            mListener.onCameraClick();
        }
    }


    @OnClick(R.id.image_choose_from_gallery)
    public void onGalleryClick() {
        if (mListener != null) {
            mListener.onGalleryClick();
        }
    }


    public interface Listener {

        void onFacebookClick();

        void onCameraClick();

        void onGalleryClick();

        void onCancel();
    }
}
