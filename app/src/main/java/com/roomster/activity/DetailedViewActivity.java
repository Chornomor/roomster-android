package com.roomster.activity;


import android.os.Bundle;
import android.view.View;

import com.roomster.application.RoomsterApplication;
import com.roomster.fragment.DetailedViewFragment;
import com.roomster.manager.BookmarkManager;
import com.roomster.manager.SearchManager;
import com.roomster.rest.model.ResultItemModel;

import javax.inject.Inject;


/**
 * Created by michaelkatkov on 2/10/16.
 */
public class DetailedViewActivity extends BaseToolbarActivity {

    public static final String LISTING_ID = "listing_id";
    public static final String IS_BOOKMARK_TYPE = "bookmark_type";

    @Inject
    SearchManager mSearchManager;

    @Inject
    BookmarkManager bookmarkManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        long listingId = getIntent().getLongExtra(LISTING_ID, -1);
        boolean isBookmark = getIntent().getBooleanExtra(IS_BOOKMARK_TYPE, false);
        ResultItemModel item;
        if(isBookmark) {
            item = bookmarkManager.findItemModelByListingId(listingId);
        } else {
            item = mSearchManager.findItemModelByListingId(listingId);
        }
        setContent(DetailedViewFragment.newInstance(item));
        mTitleTv.setText("");
        mTitleTv.setVisibility(View.GONE);
        mToolbarIcon.setVisibility(View.VISIBLE);
    }
}
