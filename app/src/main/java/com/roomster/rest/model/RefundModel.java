package com.roomster.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by yuliasokolova on 26.10.16.
 */
public class RefundModel {

    @SerializedName("Id")
    private int id;
    @SerializedName("UserId")
    private int userId;
    @SerializedName("TransId")
    private int transId;
    @SerializedName("DateAdded")
    private Date dateAdded;
    @SerializedName("DateProcessed")
    private Date dateProcessed;

    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public int getTransId() {
        return transId;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public Date getDateProcessed() {
        return dateProcessed;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }
}
