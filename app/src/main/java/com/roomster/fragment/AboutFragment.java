package com.roomster.fragment;


import com.google.gson.Gson;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.roomster.R;
import com.roomster.constants.IntentExtras;
import com.roomster.model.PrivacyJSON;

import java.io.IOException;
import java.io.InputStream;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Fragment which displays about information on the screen.
 * <p/>
 * Created by Emil Atanasov
 */
public class AboutFragment extends Fragment {

    @Bind(R.id.webview)
    WebView mWebview;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_about_page, container, false);
        ButterKnife.bind(this, root);

        Bundle args = getArguments();
        String url = args.getString(IntentExtras.EXTRA_ABOUT_URL);
        if (url != null) {
            loadJSON(url);
        }

        return root;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    public String loadJSONFromAsset(String filename) {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    private void loadJSON(String option) {
        Gson gson = new Gson();
        PrivacyJSON privacy = gson.fromJson(loadJSONFromAsset("privacy-terms" + ".json"), PrivacyJSON.class);

        if (option.equals("privacy")) {
            mWebview.loadData(privacy.privacy, "text/html", "UTF-8");
        } else {
            mWebview.loadData(privacy.terms, "text/html", "UTF-8");
        }
    }
}