package com.roomster.manager;


import com.roomster.application.RoomsterApplication;
import com.roomster.cache.sharedpreferences.SharedPreferencesFiles;

import javax.inject.Inject;


/**
 * Created by Bogoi on 11/11/2015.
 */
public class ReferrerManager {

    private final static String REFERRER = "referrer";

    @Inject
    SharedPreferencesFiles sharedPreferencesFiles;


    public ReferrerManager() {
        //Needed to inject the dependencies
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void saveReferrer(String referrer) {
        sharedPreferencesFiles.getAppSharedPreferences().edit().putString(REFERRER, referrer).commit();
    }


    public String getReferrer() {
        return sharedPreferencesFiles.getAppSharedPreferences().getString(REFERRER, "");
    }
}
