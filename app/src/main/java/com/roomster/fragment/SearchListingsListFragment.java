package com.roomster.fragment;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.roomster.R;
import com.roomster.activity.DiscoveryPreferencesActivity;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.SearchRestServiceEvent;
import com.roomster.event.application.ApplicationEvent;
import com.roomster.listener.BookmarksCallbackListener;
import com.roomster.manager.MeManager;
import com.roomster.manager.SearchManager;
import com.roomster.rest.model.ResultItemModel;
import com.roomster.rest.model.ResultModel;
import com.roomster.rest.service.SearchRestService;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class SearchListingsListFragment extends ListingListViewFragment {

    private boolean mIsInitialLoading;

    public BookmarksCallbackListener mCallback = null;

    @Inject
    SearchManager mSearchManager;

    @Inject
    SearchRestService mSearchRestService;

    @Inject
    EventBus mEventBus;

    @Inject
    MeManager meManager;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        super.setListener(mCallback);
        RoomsterApplication.getRoomsterComponent().inject(this);
        mIsInitialLoading = true;
    }


    @Override
    public void onStart() {
        super.onStart();
        if (!mEventBus.isRegistered(this)) {
            mEventBus.register(this);
        }
    }


    @Override
    public void onStop() {
        if (mEventBus.isRegistered(this)) {
            mEventBus.unregister(this);
        }
        super.onStop();
    }


    @Override
    protected void onInitEmptyLayoutViews() {
        mEmptyResultHeaderTV.setText(getString(R.string.empty_search_header));
        mEmptyResultSubtextTV.setText(getString(R.string.empty_search_subtext));

        mEmptyLayoutButtonTV.setText(getString(R.string.empty_search_button));
        mEmptyLayoutButtonTV.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DiscoveryPreferencesActivity.class);
                startActivity(intent);
            }
        });
        mEmptyLayoutButtonTV.setVisibility(View.VISIBLE);
    }


    @Override
    protected boolean hasMoreListings() {
        return mSearchRestService.hasMoreResults();
    }


    @Override
    protected void onRequestListings() {
        if (mIsInitialLoading) {
            mSearchRestService.loadNextResultsPage(PAGE_SIZE, true);
            mIsInitialLoading = false;
        } else {
            mSearchRestService.loadNextResultsPage(PAGE_SIZE, false);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(SearchRestServiceEvent.SearchSuccess event) {
        Long uid = meManager.getMe().getId();
        List<ResultItemModel> list = clearOutByUserId(mSearchManager.getResultModel().getItems(),uid);
        onNewListings(list);
        if (mCallback != null) mCallback.onBookmarksCountChanged(mSearchManager.getResultModel().getCount());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(SearchRestServiceEvent.SearchFailure event) {
        Toast.makeText(mContext, getString(R.string.unable_to_get_search_info), Toast.LENGTH_SHORT).show();
    }

    private List<ResultItemModel> clearOutByUserId(List<ResultItemModel> list,Long uid){
        if(list == null){
            return null;
        }
        List<ResultItemModel> delteList = new ArrayList<>();
        for(ResultItemModel m : list){
            if (m.getUser().getId().equals(uid)){
                delteList.add(m);
            }
        }
        list.removeAll(delteList);
        return list;
    }
}