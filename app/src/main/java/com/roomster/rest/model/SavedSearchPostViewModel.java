package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import com.roomster.enums.ServiceTypeEnum;


public class SavedSearchPostViewModel {

    @SerializedName("name")
    private String name = null;

    @SerializedName("path")
    private String path = null;

    @SerializedName("service_type")
    private ServiceTypeEnum serviceType = null;


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getPath() {
        return path;
    }


    public void setPath(String path) {
        this.path = path;
    }


    public ServiceTypeEnum getServiceType() {
        return serviceType;
    }


    public void setServiceType(ServiceTypeEnum serviceType) {
        this.serviceType = serviceType;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class SavedSearchPostViewModel {\n");

        sb.append("  name: ").append(name).append("\n");
        sb.append("  path: ").append(path).append("\n");
        sb.append("  serviceType: ").append(serviceType).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
