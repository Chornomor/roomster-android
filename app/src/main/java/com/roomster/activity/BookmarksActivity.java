package com.roomster.activity;


import com.google.android.gms.analytics.HitBuilders;

import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.v4.app.Fragment;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.SearchRestServiceEvent;
import com.roomster.event.bookmarks.BookmarksEvent;
import com.roomster.fragment.BookmarksDetailedViewPagerFragment;
import com.roomster.fragment.BookmarksListViewFragment;
import com.roomster.fragment.BookmarksMapViewFragment;
import com.roomster.listener.BookmarksCallbackListener;
import com.roomster.manager.BookmarkManager;
import com.roomster.manager.DiscoveryPrefsManager;
import com.roomster.manager.SearchManager;

import javax.inject.Inject;


public class BookmarksActivity extends BaseToolbarActivity implements BookmarksCallbackListener {

    private Fragment mFragment;
    private int mCount           = -1;
    private int mPageNumber      = 0;
    private int mPageFinalNumber = 1;
    private String mNoResultsNumber = " (0)";
    private String viewType;
    private String counterBookmarks = "";

    @Inject
    DiscoveryPrefsManager mDPrefsManager;

    @Inject
    SearchManager mSearchManager;

    @Inject
    BookmarkManager bookmarkManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        mFragment = getBookmarksFragment();
        setContent(mFragment);
        mTitleTv.setText(getString(R.string.bookmarks));
    }


    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName(getString(R.string.screen_name_bookmarks));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    @Override
    public void onBookmarksCountChanged(int count) {
        mCount = count;
        setTitleBarCounter();
    }


    @Override
    public void onPageChanged(int pageNumber) {
        mPageNumber = pageNumber;
        setTitleBarCounter();
    }


    @Override
    public void onBackPressed() {
        if(viewType.equals(DiscoveryPrefsManager.LIST)) {
            mSearchManager.clearResultModel();
        }
        mEventBus.post(new BookmarksEvent.CloseBookmarksEvent());
        super.onBackPressed();
    }


    private Fragment getBookmarksFragment() {
        viewType = mDPrefsManager.getViewType();
        if (viewType.equals(DiscoveryPrefsManager.LIST)) {
            BookmarksListViewFragment fragment = new BookmarksListViewFragment();
            fragment.setCallback(this);
            return fragment;
        } else
            if (viewType.equals(DiscoveryPrefsManager.MAP)) {
                BookmarksMapViewFragment fragment = new BookmarksMapViewFragment();
                fragment.setCallback(this);
                return fragment;
            }

        //The default fragment for bookmarks is detailed view.
        BookmarksDetailedViewPagerFragment fragment = new BookmarksDetailedViewPagerFragment().setParentBookmarkType(true);
        fragment.setCallback(this);
        return fragment;
    }


    private void setTitleBarCounter() {
        if (mCount > 0){
            switch (viewType){
                case DiscoveryPrefsManager.LIST:
                case DiscoveryPrefsManager.MAP:{
                     counterBookmarks = " (" + mPageFinalNumber + "/" + mCount + ")";
                     break;
                }
                default:
                    counterBookmarks = " (" + (((BookmarksDetailedViewPagerFragment)mFragment).getCurrentItem() + 1) + "/" + mCount + ")";
            }
        } else {
            counterBookmarks = mNoResultsNumber;
        }
        mTitleTv.setText(getString(R.string.bookmarks) + counterBookmarks);
    }

}
