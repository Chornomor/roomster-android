package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


public class ConversationMessagePostViewModel {

    /**
     * Message text
     **/
    @SerializedName("message_text")
    private String messageText = null;

    /**
     * Required if recipient_user_id is null
     **/
    @SerializedName("conversation")
    private String conversation = null;

    /**
     * Required if conversation is null
     **/
    @SerializedName("recipient_user_id")
    private Long recipientUserId = null;

    /**
     **/
    @SerializedName("listing_id")
    private Long listingId = null;


    public String getMessageText() {
        return messageText;
    }


    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }


    public String getConversation() {
        return conversation;
    }


    public void setConversation(String conversation) {
        this.conversation = conversation;
    }


    public Long getRecipientUserId() {
        return recipientUserId;
    }


    public void setRecipientUserId(Long recipientUserId) {
        this.recipientUserId = recipientUserId;
    }


    public Long getListingId() {
        return listingId;
    }


    public void setListingId(Long listingId) {
        this.listingId = listingId;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ConversationMessagePostViewModel {\n");

        sb.append("  messageText: ").append(messageText).append("\n");
        sb.append("  conversation: ").append(conversation).append("\n");
        sb.append("  recipientUserId: ").append(recipientUserId).append("\n");
        sb.append("  listingId: ").append(listingId).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
