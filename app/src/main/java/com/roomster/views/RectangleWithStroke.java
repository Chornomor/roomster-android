package com.roomster.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CompoundButton;

import com.roomster.R;

/**
 * Created by eugenetroyanskii on 30.08.16.
 */
public class RectangleWithStroke extends CompoundButton {
    OnRectClickInterface listener;

    public RectangleWithStroke(Context context) {
        super(context);
        this.setOnClickListener(onRectClickListner);
    }

    public RectangleWithStroke(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setOnClickListener(onRectClickListner);
    }

    public RectangleWithStroke(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setOnClickListener(onRectClickListner);
    }

    public void setListener (OnRectClickInterface listener){
        this.listener = listener;
    }

    private View.OnClickListener onRectClickListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            updateTextRectWithStroke((RectangleWithStroke) v);
            if (listener != null){ // for Megaphone screen
                listener.onRectClick();
            }
        }
    };

    private void updateTextRectWithStroke(RectangleWithStroke rect) {
        if (rect.isChecked()) {
            rect.setTextColor(getResources().getColor(R.color.orange));
        } else {
            rect.setTextColor(getResources().getColor(R.color.gray_border));
        }
    }


    public interface OnRectClickInterface {
        void onRectClick ();
    }
}
