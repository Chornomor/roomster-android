package com.roomster.rest.service;


import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.UserListingsRestServiceEvent;
import com.roomster.manager.MeManager;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.SettingsManager;
import com.roomster.rest.api.UserListingsApi;
import com.roomster.rest.model.ListingGetViewModel;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UserListingsRestService {

    @Inject
    UserListingsApi mUserListingsApi;

    @Inject
    SettingsManager mSettingsManager;

    @Inject
    MeManager mMeManager;

    @Inject
    EventBus mEventBus;

    @Inject
    NetworkManager mNetworkManager;


    public UserListingsRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void getUserListings(final Long userId, Boolean activeOnly) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<List<ListingGetViewModel>> call = mUserListingsApi
              .userListingsGet(userId, activeOnly, mSettingsManager.getCurrency(), mSettingsManager.getLocale());
            call.enqueue(new Callback<List<ListingGetViewModel>>() {

                @Override
                public void onResponse(Call<List<ListingGetViewModel>> call, Response<List<ListingGetViewModel>> response) {
                    if (response.code() == ResponseCode.OK) {
                        if (mMeManager.getMe() != null && mMeManager.getMe().getId() != null && mMeManager.getMe().getId()
                          .equals(userId)) {
                            mMeManager.setMyListings(response.body());
                        }
                        mEventBus.post(new UserListingsRestServiceEvent.GetListingsSuccess(response.body()));
                    } else {
                        mEventBus.post(new UserListingsRestServiceEvent.GetListingsFailed());
                    }
                }


                @Override
                public void onFailure(Call<List<ListingGetViewModel>> call, Throwable t) {
                    mEventBus.post(new UserListingsRestServiceEvent.GetListingsFailed());
                }
            });
        } else {
            mEventBus.post(new UserListingsRestServiceEvent.GetListingsFailed());
        }
    }
}