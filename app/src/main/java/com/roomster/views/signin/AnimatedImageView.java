package com.roomster.views.signin;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.roomster.R;

/**
 * Created by andreybofanov on 14.07.16.
 */
public class AnimatedImageView extends HorizontalScrollView {
    private static final int ANIM_TIME = 16000;
    private int mSrcId = R.mipmap.slide_1;
    LinearLayout layout;
    public AnimatedImageView(Context context) {
        super(context);
        init();
    }

    public AnimatedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        checkAttrs(attrs);
        init();
    }

    public AnimatedImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        checkAttrs(attrs);
        init();
    }

    void checkAttrs(AttributeSet attrs){
        if(isInEditMode()) {
            mSrcId = R.mipmap.slide_1;
            return;
        }
        TypedArray a = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.AnimatedImageView,
                0, 0);
        mSrcId = a.getResourceId(R.styleable.AnimatedImageView_src,R.mipmap.slide_1);
    }

    void init(){
        layout = new LinearLayout(getContext());
        ImageView imageView = new DynamicImageView(getContext());
        imageView.setScaleType(ImageView.ScaleType.FIT_START);
        imageView.setImageResource(mSrcId);
        imageView.setAdjustViewBounds(true);

        LinearLayout.LayoutParams paramsImg = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layout.addView(imageView,paramsImg);
        HorizontalScrollView.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        addView(layout,params);
        setHorizontalScrollBarEnabled(false);
        setOverScrollMode(OVER_SCROLL_NEVER);
    }
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setUpAnimation();
    }
    ObjectAnimator animator;
    public void setUpAnimation(){
        if(animator != null)
            return;
        animator = ObjectAnimator.ofInt(this,"scrollX",0,layout.getMeasuredWidth() - getMeasuredWidth());
        animator.setDuration(ANIM_TIME);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setRepeatMode(ValueAnimator.REVERSE);
        animator.start();

    }
}
