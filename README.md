# Roomster-Android
Roomster project - Android repo
This is the base code of the Roomster Android App

There is settings.jar file in "settings" folder in the root of the project. Please, include these settings into the Android Studio, so all developers would share same code formatter.

#External frameworks that are being used:
- EventBus
- Dagger 2 
- Butterknife
- Retrofit
- Picasso
- Gson
- Flipboard
- Crashlythics


Software:
1. Android Studio v2.2
2. Android SDK v15-24
3. Android SDK Tools v25.2.2
4. Android SDK Platform-tools v24.0.4
4. Android SDK Build-tools v23.0.3

Steps for build apk:
1. Checkout repo
2. Checkout to branch staging
3. do git pull
4. Go to Build Variants -> select Build Variant release or debug
5. Menu Build -> Build APK
Done. You have a build in folder ..\roomster-android\app\build\outputs\apk\
For Fabric need a debug build. For GP need a release build.
For load debug build on Fabric need to install Fabric plugin on our Android Studio.