package com.roomster.adapter;


import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


/**
 * Created by michaelkatkov on 11/18/15.
 */
public abstract class BasePageAdapter extends FragmentStatePagerAdapter {

    public BasePageAdapter(FragmentManager fm) {
        super(fm);
    }


    public abstract int getSelectedIndicator(int position);

    public abstract int getDiselectedIndicator(int position);
}
