package com.roomster.rest.api;


import com.roomster.rest.model.ConversationMessagePostViewModel;
import com.roomster.rest.model.MessageView;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface MessagesApi {

    /**
     * Get conversation messages &lt;p&gt;Messages are contained within a conversation&lt;/p&gt; \n is_new_delta in
     * combination with last_message are used to load more messages&lt;br /&gt; \n is_new_delta = true + last_message
     * load new messages that came in after the freshest message (last_message) was loaded&lt;br /&gt; \n is_new_delta =
     * false + last_message load messages that are beyond the page size&lt;br /&gt;
     *
     * @param conversation
     *   Conversation id
     * @param pageSize
     *   0 - unlimited
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @param lastMessage
     *   Message id
     * @param isNewDelta
     *   true|false
     * @return Call<List<MessageView>>
     */

    @GET("Messages")
    Call<List<MessageView>> messagesGet(@Query("conversation") String conversation, @Query("page_size") Integer pageSize,
                                        @Header("Authorization") String authorization, @Query("last_message") String lastMessage,
                                        @Query("is_new_delta") Boolean isNewDelta);

    /**
     * Send message To send &lt;strong&gt;initial message&lt;/strong&gt; include: \n &lt;ul&gt;&lt;li&gt;message_text
     * (&lt;b&gt;Required&lt;/b&gt;)&lt;/li&gt;&lt;li&gt;recipient_user_id
     * (&lt;b&gt;Required&lt;/b&gt;)&lt;/li&gt;&lt;li&gt;listing_id (Optional)&lt;/li&gt;&lt;/ul&gt; \n To send
     * &lt;strong&gt;reply message&lt;/strong&gt; include: \n &lt;ul&gt;&lt;li&gt;message_text
     * (&lt;b&gt;Required&lt;/b&gt;)&lt;/li&gt;&lt;li&gt;conversation
     * (&lt;b&gt;Required&lt;/b&gt;)&lt;/li&gt;&lt;/ul&gt;
     *
     * @param model
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<Object>
     */
    //TODO Investigate whether the final slash is needed for all POST calls, because without it the call passes but retrofit reports failure with timeout
    @POST("Messages/")
    Call<String> messagesPost(@Body ConversationMessagePostViewModel model, @Header("Authorization") String authorization);

    /**
     * Get conversation messages &lt;p&gt;Messages are contained within a conversation&lt;/p&gt; \n is_new_delta in
     * combination with last_message are used to load more messages&lt;br /&gt; \n is_new_delta = true + last_message
     * load new messages that came in after the freshest message (last_message) was loaded&lt;br /&gt; \n is_new_delta =
     * false + last_message load messages that are beyond the page size&lt;br /&gt;
     *
     * @param conversation
     *   Conversation id
     * @param pageSize
     *   0 - unlimited
     * @param isNewDelta
     *   true|false
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @param lastMessage
     *   Message id
     * @return Call<List<MessageView>>
     */

    @GET("messages/{conversation}/{page_size}/{is_new_delta}")
    Call<List<MessageView>> messagesGet_1(@Path("conversation") String conversation, @Path("page_size") Integer pageSize,
                                          @Path("is_new_delta") Boolean isNewDelta, @Header("Authorization") String authorization,
                                          @Query("last_message") String lastMessage);
}
