package com.roomster.rest.api;


import com.roomster.rest.model.NotificationsViewModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PUT;


public interface NotificationsApi {

    /**
     * Get user notifications
     *
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<NotificationsViewModel>
     */

    @GET("Notifications")
    Call<NotificationsViewModel> notificationsGet(@Header("Authorization") String authorization);

    /**
     * Update user notifications
     *
     * @param model
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<NotificationsViewModel>
     */

    @PUT("Notifications")
    Call<NotificationsViewModel> notificationsPut(@Body NotificationsViewModel model,
                                                  @Header("Authorization") String authorization);
}
