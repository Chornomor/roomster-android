package com.roomster.enums;

public enum MaritalStatusEnum {
    Undefined, Single, Married, InRelationship, Dating, Widowed,
}