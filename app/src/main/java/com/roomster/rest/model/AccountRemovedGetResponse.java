package com.roomster.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by andreybofanov on 04.08.16.
 */
public class AccountRemovedGetResponse {
    public String id = null;
    @SerializedName("removal_text")
    public String message = null;
}
