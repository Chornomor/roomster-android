package com.roomster.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A generic adapter for handling messages views.
 *
 * @author Bogoi Bogdanov
 */
public abstract class MessagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected static final int MY_MESSAGE_VIEW_TYPE    = 0;
    protected static final int THEIR_MESSAGE_VIEW_TYPE = 1;

    protected List<? extends Object> mMessages;


    public MessagesAdapter(List<? extends Object> messages) {
        if (messages != null) {
            mMessages = messages;
        } else {
            mMessages = new ArrayList<>();
        }
    }


    /**
     * @return Layout resource id to be used for my message layout.
     */
    abstract protected int getMyMessageListItemLayout();

    /**
     * @return Layout resource id to be used for their message layout.
     */
    abstract protected int getTheirMessageListItemLayout();

    /**
     * @param root
     *   - the root view to be used
     * @return new instance of a view holder for holding my message.
     */
    abstract protected MyMessageVH getNewMyMessageViewHolder(View root);

    /**
     * @param root
     *   - the root view to be used
     * @return new instance of a view holder for holding their message.
     */
    abstract protected TheirMessageVH getNewTheirMessageViewHolder(View root);

    /**
     * Fills the message data into the ViewHolder
     *
     * @param viewHolder
     * @param messageModel
     */
    abstract protected void fillMyMessage(MyMessageVH viewHolder, Object messageModel);

    /**
     * Fills the message data into the ViewHolder
     *
     * @param viewHolder
     * @param messageModel
     */
    abstract protected void fillTheirMessage(TheirMessageVH viewHolder, Object messageModel);

    /**
     * Return the view type of the view based on the position in the list.
     *
     * @param position
     * @return MY_MESSAGE_VIEW_TYPE constant for my message and THEIR_MESSAGE_VIEW_TYPE otherwise.
     */
    abstract public int getItemViewType(int position);


    @Override
    public int getItemCount() {
        return mMessages.size();
    }


    public void setMessages(List<? extends Object> messages) {
        if (mMessages != null) {
            mMessages = messages;
            notifyDataSetChanged();
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View root;
        switch (viewType) {
            case MY_MESSAGE_VIEW_TYPE:
                root = layoutInflater.inflate(getMyMessageListItemLayout(), parent, false);
                return getNewMyMessageViewHolder(root);
            case THEIR_MESSAGE_VIEW_TYPE:
                root = layoutInflater.inflate(getTheirMessageListItemLayout(), parent, false);
                return getNewTheirMessageViewHolder(root);
            default:
                return null;
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Object message = mMessages.get(position);
        switch (getItemViewType(position)) {
            case MY_MESSAGE_VIEW_TYPE:
                fillMyMessage((MyMessageVH) holder, message);
                break;
            case THEIR_MESSAGE_VIEW_TYPE:
                fillTheirMessage((TheirMessageVH) holder, message);
                break;
            default:
                break;
        }
    }


    public static abstract class MyMessageVH extends RecyclerView.ViewHolder {

        public MyMessageVH(View root) {
            super(root);
        }
    }


    public static abstract class TheirMessageVH extends RecyclerView.ViewHolder {

        public TheirMessageVH(View root) {
            super(root);
        }
    }
}