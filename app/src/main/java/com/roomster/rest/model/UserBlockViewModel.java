package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


public class UserBlockViewModel {

    /**
     **/
    @SerializedName("user_id")
    private Long userId = null;

    /**
     **/
    @SerializedName("block")
    private Boolean block = null;


    public Long getUserId() {
        return userId;
    }


    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public Boolean getBlock() {
        return block;
    }


    public void setBlock(Boolean block) {
        this.block = block;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class UserBlockViewModel {\n");

        sb.append("  userId: ").append(userId).append("\n");
        sb.append("  block: ").append(block).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
