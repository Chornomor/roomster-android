package com.roomster.views;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.Releasable;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.dialog.CatalogListChooserDialog;
import com.roomster.manager.CatalogsManager;
import com.roomster.model.CatalogModel;
import com.roomster.rest.model.ShareDetails;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by michaelkatkov on 1/21/16.
 */
public class SharedDetailsView extends LinearLayout {

    @Bind(R.id.el_share_details_layout)
    LinearLayout mSharedDetailsLayout;

    @Bind(R.id.el_cleanliness)
    LinearLayout mCleanlinessItem;

    @Bind(R.id.el_cleanliness_value)
    TextView mCleanlinessValue;

    @Bind(R.id.el_overnight_guests)
    LinearLayout mOvernightGuestsItem;

    @Bind(R.id.el_overnight_guests_value)
    TextView mOvernightGuestsValue;

    @Bind(R.id.el_party_habits)
    LinearLayout mPartyHabitsItem;

    @Bind(R.id.el_party_habits_value)
    TextView mPartyHabitsValue;

    @Bind(R.id.el_get_up)
    LinearLayout mGetUpItem;

    @Bind(R.id.el_get_up_value)
    TextView mGetUpValue;

    @Bind(R.id.el_go_to_bed)
    LinearLayout mGoToBedItem;

    @Bind(R.id.el_go_to_bed_value)
    TextView mGoToBedValue;

    @Bind(R.id.el_food_preferences)
    LinearLayout mFoodPreferencesItem;

    @Bind(R.id.el_food_preferences_value)
    TextView mFoodPreferencesValue;

    @Bind(R.id.el_smoking_habits)
    LinearLayout mSmokingHabitsItem;

    @Bind(R.id.el_smoking_habits_value)
    TextView mSmokinghabitsValue;

    @Bind(R.id.el_work_schedule)
    LinearLayout mWorkScheduleItem;

    @Bind(R.id.el_work_schedule_value)
    TextView mWorkScheduleValue;

    @Bind(R.id.el_occupation)
    LinearLayout mOccupationItem;

    @Bind(R.id.el_occupation_value)
    TextView mOccupationValue;

    @Inject
    CatalogsManager mCatalogsManager;

    private ShareDetails mSharedDetails;


    public SharedDetailsView(Context context) {
        super(context);
        initView();
    }


    public SharedDetailsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }


    public SharedDetailsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }


    private void initView() {
        RoomsterApplication.getRoomsterComponent().inject(this);
        inflate(getContext(), R.layout.view_shared_details, this);
        ButterKnife.bind(this);
    }


    //------------------------------ Common methods---------------------------------//
    public void setModel(ShareDetails shareDetails) {
        mSharedDetails = shareDetails;
        //visualize chosen cleanliness item
        List<CatalogModel> cleanlinessList = mCatalogsManager.getCleanlinessList();
        showChosenValue(cleanlinessList, getCleanlinessId(), mCleanlinessValue);
        //visualize chosen overnightGuests item
        List<CatalogModel> guestsList = mCatalogsManager.getOvernightGuestsList();
        showChosenValue(guestsList, getOvernightGuestsId(), mOvernightGuestsValue);
        //visualize chosen partyHabits item
        List<CatalogModel> partyHabitsList = mCatalogsManager.getPartyHabitsList();
        showChosenValue(partyHabitsList, getPartyHabitsId(), mPartyHabitsValue);
        //visualize chosen getUp item
        List<CatalogModel> getUpList = mCatalogsManager.getGetUpList();
        showChosenValue(getUpList, getGetUpId(), mGetUpValue);
        //visualize chosen goToBed item
        List<CatalogModel> goToBedList = mCatalogsManager.getGoToBedList();
        showChosenValue(goToBedList, getGoToBedId(), mGoToBedValue);
        //visualize chosen foodPrefs item
        List<CatalogModel> foodPreferencesList = mCatalogsManager.getFoodPreferencesList();
        showChosenValue(foodPreferencesList, getFoodPreferencesId(), mFoodPreferencesValue);
        //visualize chosen smokingHabits item
        List<CatalogModel> smokingHabitsList = mCatalogsManager.getSmokingHabitsList();
        showChosenValue(smokingHabitsList, getSmokingHabitsId(), mSmokinghabitsValue);
        //visualize chosen workSchedule item
        List<CatalogModel> workScheduleList = mCatalogsManager.getWorkScheduleList();
        showChosenValue(workScheduleList, getWorkScheduleId(), mWorkScheduleValue);
        //visualize chosen workSchedule item
        List<CatalogModel> occupationList = mCatalogsManager.getOccupationList();
        showChosenValue(occupationList, getOccupationId(), mOccupationValue);
    }


    public ShareDetails getModel() {
        return mSharedDetails;
    }


    private CatalogModel getCatalogById(List<CatalogModel> catalogModels, Integer categoryId) {
        if (categoryId != null && categoryId != -1) {
            for (CatalogModel catalogModel : catalogModels) {
                if (catalogModel.getId() == categoryId) {
                    return catalogModel;
                }
            }
        }
        return null;
    }


    private void showChosenValue(List<CatalogModel> catalog, Integer categoryId, TextView valueTv) {
        CatalogModel catalogModel = getCatalogById(catalog, categoryId);
        if (catalogModel != null) {
            valueTv.setText(catalogModel.getName());
            valueTv.setFocusableInTouchMode(true);
            valueTv.setFocusable(true);
            valueTv.requestFocus();
        } else {
            valueTv.setText("");
        }
    }


    private void showSharedDetailsDialog(List<CatalogModel> catalogList, Integer currentId,
                                         CatalogListChooserDialog.OKButtonClickListener okButtonClick, String title) {
        ArrayList<Integer> currentIdList = new ArrayList<>();
        currentIdList.add(currentId);
        CatalogListChooserDialog sharedDetailsChooserDialog = new CatalogListChooserDialog(getContext(), catalogList,
          currentIdList, false, okButtonClick, title);
        sharedDetailsChooserDialog.show();
    }


    //------------------------------ Cleanliness---------------------------------//
    @OnClick(R.id.el_cleanliness)
    public void onCleanliness() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getCleanlinessList();
        showSharedDetailsDialog(catalogModels, getCleanlinessId(), new CatalogListChooserDialog.OKButtonClickListener() {

            @Override
            public void onOkClick(List<Integer> categoryIds) {
                if (categoryIds != null && !categoryIds.isEmpty()) {
                    setCleanlinessId(categoryIds.get(0));
                    showChosenValue(catalogModels, categoryIds.get(0), mCleanlinessValue);
                }
            }
        }, getContext().getString(R.string.cleanliness));
    }


    private int getCleanlinessId() {
        int cleanliness = -1;
        if (mSharedDetails != null && mSharedDetails.getCleanliness() != null) {
            cleanliness = mSharedDetails.getCleanliness();
        }
        return cleanliness;
    }


    private void setCleanlinessId(Integer categoryId) {
        if (mSharedDetails == null) {
            mSharedDetails = new ShareDetails();
        }
        if (categoryId != null && categoryId != -1) {
            mSharedDetails.setCleanliness(categoryId);
        }
    }


    //------------------------------ Overnight Guests---------------------------------//
    @OnClick(R.id.el_overnight_guests)
    public void onOvernightGuests() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getOvernightGuestsList();
        showSharedDetailsDialog(catalogModels, getOvernightGuestsId(), new CatalogListChooserDialog.OKButtonClickListener() {

            @Override
            public void onOkClick(List<Integer> categoryIds) {
                if (categoryIds != null && !categoryIds.isEmpty()) {
                    setOvernightGuestsId(categoryIds.get(0));
                    showChosenValue(catalogModels, categoryIds.get(0), mOvernightGuestsValue);
                }
            }
        }, getContext().getString(R.string.overnight_guests));
    }


    private int getOvernightGuestsId() {
        int overnightGuests = -1;
        if (mSharedDetails != null && mSharedDetails.getOvernightGuests() != null) {
            overnightGuests = mSharedDetails.getOvernightGuests();
        }
        return overnightGuests;
    }


    private void setOvernightGuestsId(Integer categoryId) {
        if (mSharedDetails == null) {
            mSharedDetails = new ShareDetails();
        }
        if (categoryId != null && categoryId != -1) {
            mSharedDetails.setOvernightGuests(categoryId);
        }
    }


    //------------------------------ Party Habits---------------------------------//
    @OnClick(R.id.el_party_habits)
    public void onPartyHabits() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getPartyHabitsList();
        showSharedDetailsDialog(catalogModels, getPartyHabitsId(), new CatalogListChooserDialog.OKButtonClickListener() {

            @Override
            public void onOkClick(List<Integer> categoryIds) {
                if (categoryIds != null && !categoryIds.isEmpty()) {
                    setPartyHabitsId(categoryIds.get(0));
                    showChosenValue(catalogModels, categoryIds.get(0), mPartyHabitsValue);
                }
            }
        }, getContext().getString(R.string.party_habits));
    }


    private int getPartyHabitsId() {
        int partyHabits = -1;
        if (mSharedDetails != null && mSharedDetails.getPartyHabits() != null) {
            partyHabits = mSharedDetails.getPartyHabits();
        }
        return partyHabits;
    }


    private void setPartyHabitsId(Integer categoryId) {
        if (mSharedDetails == null) {
            mSharedDetails = new ShareDetails();
        }
        if (categoryId != null && categoryId != -1) {
            mSharedDetails.setPartyHabits(categoryId);
        }
    }


    //------------------------------ Get Up---------------------------------//
    @OnClick(R.id.el_get_up)
    public void onGetUp() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getGetUpList();
        showSharedDetailsDialog(catalogModels, getGetUpId(), new CatalogListChooserDialog.OKButtonClickListener() {

            @Override
            public void onOkClick(List<Integer> categoryIds) {
                if (categoryIds != null && !categoryIds.isEmpty()) {
                    setGetUpId(categoryIds.get(0));
                    showChosenValue(catalogModels, categoryIds.get(0), mGetUpValue);
                }
            }
        }, getContext().getString(R.string.get_up));
    }


    private int getGetUpId() {
        int getUp = -1;
        if (mSharedDetails != null && mSharedDetails.getGetUp() != null) {
            getUp = mSharedDetails.getGetUp();
        }
        return getUp;
    }


    private void setGetUpId(Integer categoryId) {
        if (mSharedDetails == null) {
            mSharedDetails = new ShareDetails();
        }
        if (categoryId != null && categoryId != -1) {
            mSharedDetails.setGetUp(categoryId);
        }
    }


    //------------------------------ Go To bed---------------------------------//
    @OnClick(R.id.el_go_to_bed)
    public void onGoToBed() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getGoToBedList();
        showSharedDetailsDialog(catalogModels, getGoToBedId(), new CatalogListChooserDialog.OKButtonClickListener() {

            @Override
            public void onOkClick(List<Integer> categoryIds) {
                if (categoryIds != null && !categoryIds.isEmpty()) {
                    setGoToBedId(categoryIds.get(0));
                    showChosenValue(catalogModels, categoryIds.get(0), mGoToBedValue);
                }
            }
        }, getContext().getString(R.string.go_to_bed));
    }


    private int getGoToBedId() {
        int goToBed = -1;
        if (mSharedDetails != null && mSharedDetails.getGoToBed() != null) {
            goToBed = mSharedDetails.getGoToBed();
        }
        return goToBed;
    }


    private void setGoToBedId(Integer categoryId) {
        if (mSharedDetails == null) {
            mSharedDetails = new ShareDetails();
        }
        if (categoryId != null && categoryId != -1) {
            mSharedDetails.setGoToBed(categoryId);
        }
    }


    //------------------------------ food preferences---------------------------------//
    @OnClick(R.id.el_food_preferences)
    public void onFoodPreferences() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getFoodPreferencesList();
        showSharedDetailsDialog(catalogModels, getFoodPreferencesId(), new CatalogListChooserDialog.OKButtonClickListener() {

            @Override
            public void onOkClick(List<Integer> categoryIds) {
                if (categoryIds != null && !categoryIds.isEmpty()) {
                    setFoodPreferencesId(categoryIds.get(0));
                    showChosenValue(catalogModels, categoryIds.get(0), mFoodPreferencesValue);
                }
            }
        }, getContext().getString(R.string.food_preferences));
    }


    private int getFoodPreferencesId() {
        int foodPref = -1;
        if (mSharedDetails != null && mSharedDetails.getFoodPreference() != null) {
            foodPref = mSharedDetails.getFoodPreference();
        }
        return foodPref;
    }


    private void setFoodPreferencesId(Integer categoryId) {
        if (mSharedDetails == null) {
            mSharedDetails = new ShareDetails();
        }
        if (categoryId != null && categoryId != -1) {
            mSharedDetails.setFoodPreference(categoryId);
        }
    }


    //------------------------------ smoking habits---------------------------------//
    @OnClick(R.id.el_smoking_habits)
    public void onSmokingHabits() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getSmokingHabitsList();
        showSharedDetailsDialog(catalogModels, getSmokingHabitsId(), new CatalogListChooserDialog.OKButtonClickListener() {
            @Override
            public void onOkClick(List<Integer> categoryId) {
                if (categoryId != null && !categoryId.isEmpty()) {
                    setSmokingHabitsId(categoryId.get(0));
                    showChosenValue(catalogModels, categoryId.get(0), mSmokinghabitsValue);
                }
            }
        }, getContext().getString(R.string.smoking_habits));
    }


    private int getSmokingHabitsId() {
        int shokinghabit = -1;
        if (mSharedDetails != null && mSharedDetails.getMySmokingHabits() != null) {
            shokinghabit = (int) (long) mSharedDetails.getMySmokingHabits();
        }
        return shokinghabit;
    }


    private void setSmokingHabitsId(Integer categoryId) {
        if (mSharedDetails == null) {
            mSharedDetails = new ShareDetails();
        }
        if (categoryId != null && categoryId != -1) {
            mSharedDetails.setMySmokingHabits((long) categoryId);
        }
    }


    //------------------------------ work schedule---------------------------------//
    @OnClick(R.id.el_work_schedule)
    public void onWorkSchedule() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getWorkScheduleList();
        showSharedDetailsDialog(catalogModels, getWorkScheduleId() , new CatalogListChooserDialog.OKButtonClickListener() {
            @Override
            public void onOkClick(List<Integer> categoryId) {
                if(categoryId == null || categoryId.isEmpty()){
                    return;
                }
                setWorkScheduleId(categoryId.get(0));
                showChosenValue(catalogModels, categoryId.get(0), mWorkScheduleValue);
            }
        }, getContext().getString(R.string.work_schedule));
    }

    private int getWorkScheduleId() {
        int workSchedule = -1;
        if (mSharedDetails != null && mSharedDetails.getWorkSchedule() != null) {
            workSchedule = (int) (long) mSharedDetails.getWorkSchedule();
        }
        return workSchedule;
    }

    private void setWorkScheduleId(Integer categoryId) {
        if (mSharedDetails == null) {
            mSharedDetails = new ShareDetails();
        }
        if (categoryId != null && categoryId != -1) {
            mSharedDetails.setWorkSchedule((long) categoryId);
        }
    }


    //------------------------------ occupation ---------------------------------//
    @OnClick(R.id.el_occupation)
    public void onOccupation() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getOccupationList();
        showSharedDetailsDialog(catalogModels, getOccupationId() , new CatalogListChooserDialog.OKButtonClickListener() {
            @Override
            public void onOkClick(List<Integer> categoryId) {
                if(categoryId == null || categoryId.isEmpty())
                    return;
                setOccupationId(categoryId.get(0));
                showChosenValue(catalogModels, categoryId.get(0), mOccupationValue);
            }
        }, getContext().getString(R.string.bs_lifestyle_occupation));
    }

    private int getOccupationId() {
        int occupation = -1;
        if (mSharedDetails != null && mSharedDetails.getOccupation() != null) {
            occupation = (int) (long) mSharedDetails.getOccupation();
        }
        return occupation;
    }

    private void setOccupationId(Integer categoryId) {
        if (mSharedDetails == null) {
            mSharedDetails = new ShareDetails();
        }
        if (categoryId != null && categoryId != -1) {
            mSharedDetails.setOccupation((long) categoryId);
        }
    }
}
