package com.roomster.constants;

public class ListingType {
    public static final int OFFERING_ENTIRE_PLACE_TYPE = 5;
    public static final int OFFERING_ROOM_TYPE         = 2;
    public static final int FINDING_ENTIRE_PLACE_TYPE  = 4;
    public static final int FINDING_ROOM_TYPE          = 1;

    public static final String NEED_ROOM      = "NeedRoom";
    public static final String HAVE_SHARE     = "HaveShare";
    public static final String NEED_APARTMENT = "NeedApartment";
    public static final String HAVE_APARTMENT = "HaveApartment";
}
