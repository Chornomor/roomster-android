package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class HaveApartment implements Serializable {

    /**
     * Allowed values @ /v1/catalogues/bedrooms
     **/
    @SerializedName("bedrooms_id")
    private Integer bedroomsId = null;

    /**
     * Allowed values @ /v1/catalogues/bathrooms
     **/
    @SerializedName("bathrooms_id")
    private Integer bathroomsId  = null;
    /**
     * Allowed values @ /v1/catalogies/apt_size_units
     **/
    @SerializedName("apt_size_units")
    private Integer aptSizeUnits = null;

    ;
    /**
     **/
    @SerializedName("apt_size")
    private Integer aptSize     = null;
    /**
     **/
    @SerializedName("is_furnished")
    private Boolean isFurnished = null;


    public Integer getBedroomsId() {
        return bedroomsId;
    }


    public void setBedroomsId(Integer bedroomsId) {
        this.bedroomsId = bedroomsId;
    }


    public Integer getBathroomsId() {
        return bathroomsId;
    }


    public void setBathroomsId(Integer bathroomsId) {
        this.bathroomsId = bathroomsId;
    }


    public Integer getAptSizeUnits() {
        return aptSizeUnits;
    }


    public void setAptSizeUnits(Integer aptSizeUnits) {
        this.aptSizeUnits = aptSizeUnits;
    }


    public Integer getAptSize() {
        return aptSize;
    }


    public void setAptSize(Integer aptSize) {
        this.aptSize = aptSize;
    }


    public Boolean getIsFurnished() {
        return isFurnished;
    }


    public void setIsFurnished(Boolean isFurnished) {
        this.isFurnished = isFurnished;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class HaveApartment {\n");

        sb.append("  bedroomsId: ").append(bedroomsId).append("\n");
        sb.append("  bathroomsId: ").append(bathroomsId).append("\n");
        sb.append("  aptSizeUnits: ").append(aptSizeUnits).append("\n");
        sb.append("  aptSize: ").append(aptSize).append("\n");
        sb.append("  isFurnished: ").append(isFurnished).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
