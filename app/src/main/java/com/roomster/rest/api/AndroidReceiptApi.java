package com.roomster.rest.api;


import com.roomster.rest.model.AndroidReceiptModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;


public interface AndroidReceiptApi {

    @POST("AndroidReceipt")
    @Headers("Content-Type: application/json")
    Call<String> androidReceiptPost(@Header("Authorization") String authorization, @Body AndroidReceiptModel model);
}
