package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import com.roomster.enums.LabelEnum;
import com.roomster.enums.StatusEnum;


public class ConversationPutViewModel {

    @SerializedName("conversation")
    private String conversation = null;

    @SerializedName("status")
    private StatusEnum status = null;


    @SerializedName("label")
    private LabelEnum label = null;


    public String getConversation() {
        return conversation;
    }

    public void setConversation(String conversation) {
        this.conversation = conversation;
    }


    public StatusEnum getStatus() {
        return status;
    }


    public void setStatus(StatusEnum status) {
        this.status = status;
    }


    public LabelEnum getLabel() {
        return label;
    }


    public void setLabel(LabelEnum label) {
        this.label = label;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ConversationPutViewModel {\n");

        sb.append("  conversation: ").append(conversation).append("\n");
        sb.append("  status: ").append(status).append("\n");
        sb.append("  label: ").append(label).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}