package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ListingGetViewModel implements Serializable {

    /**
     **/
    @SerializedName("refreshed")
    private Date refreshed = null;

    /**
     **/
    @SerializedName("images")
    private ArrayList<String> images = null;

    /**
     **/
    @SerializedName("is_bookmarked")
    private Boolean isBookmarked = null;

    /**
     **/
    @SerializedName("rates")
    private RatesViewModel rates = null;

    /**
     **/
    @SerializedName("photos")
    private ArrayList<Photo> photos    = null;
    /**
     **/
    @SerializedName("listing_id")
    private Long             listingId = null;

    /**
     **/
    @SerializedName("headline_actual")
    private String headline_actual = null;

    /**
     **/
    @SerializedName("headline")
    private String headline = null;

    /**
     **/
    @SerializedName("description_actual")
    private String description_actual = null;

    /**
     **/
    @SerializedName("description")
    private String description = null;

    /**
     **/
    @SerializedName("is_active")
    private Boolean isActive = true;

    /**
     **/
    @SerializedName("share_details")
    private ShareDetails shareDetails = null;

    /**
     **/
    @SerializedName("questions")
    private ArrayList<String> questions = null;

    /**
     **/
    @SerializedName("tags")
    private ArrayList<String> tags = null;

    /**
     **/
    @SerializedName("apartment_amenities")
    private ArrayList<Long> apartmentAmenities = null;

    /**
     **/
    @SerializedName("service_type")
    private Integer serviceType = null;

    /**
     **/
    @SerializedName("geo_location")
    private GeoLocation   geoLocation   = null;
    /**
     **/
    @SerializedName("calendar")
    private Calendar      calendar      = null;
    /**
     **/
    @SerializedName("need_apartment")
    private NeedApartment needApartment = null;
    /**
     **/
    @SerializedName("have_apartment")
    private HaveApartment haveApartment = null;

    /**
     **/
    @SerializedName("url")
    private String url = null;

    /**
     **/
    @SerializedName("is_new")
    private Boolean isNew = null;


    public String getUrl() {
        return url;
    }


    public void setUrl(String url) {
        this.url = url;
    }


    public Date getRefreshed() {
        return refreshed;
    }


    public void setRefreshed(Date refreshed) {
        this.refreshed = refreshed;
    }


    public List<String> getImages() {
        return images;
    }


    public void setImages(List<String> images) {
        this.images = new ArrayList<>(images);
    }


    public Boolean getIsBookmarked() {
        return isBookmarked;
    }


    public void setIsBookmarked(Boolean isBookmarked) {
        this.isBookmarked = isBookmarked;
    }


    public RatesViewModel getRates() {
        return rates;
    }


    public void setRates(RatesViewModel rates) {
        this.rates = rates;
    }


    public Long getListingId() {
        return listingId;
    }


    public void setListingId(Long listingId) {
        this.listingId = listingId;
    }


    public String getHeadlineActual() {
        return headline_actual;
    }


    public void setHeadlineActual(String headline_actual) {
        this.headline_actual = headline_actual;
    }


    public String getHeadline() {
        return headline;
    }


    public void setHeadline(String headline) {
        this.headline = headline;
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }


    public String getDescriptionActual() {
        return description_actual;
    }


    public void setDescriptionActual(String description_actual) {
        this.description_actual = description_actual;
    }


    public Boolean getIsActive() {
        return isActive;
    }


    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }


    public ShareDetails getShareDetails() {
        return shareDetails;
    }


    public void setShareDetails(ShareDetails shareDetails) {
        this.shareDetails = shareDetails;
    }


    public List<String> getQuestions() {
        return questions;
    }


    public void setQuestions(List<String> questions) {
        this.questions = new ArrayList<>(questions);
    }


    public List<String> getTags() {
        return tags;
    }


    public void setTags(List<String> tags) {
        this.tags = new ArrayList<>(tags);
    }


    public List<Long> getApartmentAmenities() {
        return apartmentAmenities;
    }


    public void setApartmentAmenities(List<Long> apartmentAmenities) {
        this.apartmentAmenities = new ArrayList<>(apartmentAmenities);
    }


    public Integer getServiceType() {
        return serviceType;
    }


    public void setServiceType(Integer serviceType) {
        this.serviceType = serviceType;
    }


    public GeoLocation getGeoLocation() {
        return geoLocation;
    }


    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }


    public Calendar getCalendar() {
        return calendar;
    }


    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }


    public NeedApartment getNeedApartment() {
        return needApartment;
    }


    public void setNeedApartment(NeedApartment needApartment) {
        this.needApartment = needApartment;
    }


    public HaveApartment getHaveApartment() {
        return haveApartment;
    }


    public void setHaveApartment(HaveApartment haveApartment) {
        this.haveApartment = haveApartment;
    }


    public List<Photo> getPhotos() {
        return photos;
    }


    public void setPhotos(List<Photo> photos) {
        this.photos = new ArrayList<>(photos);
    }


    public Boolean getIsNew() {
        return isNew;
    }


    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ListingGetViewModel {\n");

        sb.append("  refreshed: ").append(refreshed).append("\n");
        sb.append("  images: ").append(images).append("\n");
        sb.append("  isBookmarked: ").append(isBookmarked).append("\n");
        sb.append("  rates: ").append(rates).append("\n");
        sb.append("  listingId: ").append(listingId).append("\n");
        sb.append("  headline: ").append(headline).append("\n");
        sb.append("  description: ").append(description).append("\n");
        sb.append("  isActive: ").append(isActive).append("\n");
        sb.append("  shareDetails: ").append(shareDetails).append("\n");
        sb.append("  questions: ").append(questions).append("\n");
        sb.append("  tags: ").append(tags).append("\n");
        sb.append("  apartmentAmenities: ").append(apartmentAmenities).append("\n");
        sb.append("  serviceType: ").append(serviceType).append("\n");
        sb.append("  geoLocation: ").append(geoLocation).append("\n");
        sb.append("  calendar: ").append(calendar).append("\n");
        sb.append("  needApartment: ").append(needApartment).append("\n");
        sb.append("  haveApartment: ").append(haveApartment).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
