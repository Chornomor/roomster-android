package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


public class AppInstallsViewModel {

    /**
     * Device Model (optional)
     **/
    @SerializedName("device_model")
    private String deviceModel = null;

    /**
     * Device OS type (iOS, Android) = ['iOS', 'Android'] (optional)
     **/
    @SerializedName("os_type")
    private String osType = null;

    /**
     * OS Version (optional)
     **/
    @SerializedName("os_version")
    private String osVersion = null;

    /**
     * App Version (optional)
     **/
    @SerializedName("app_version")
    private String appVersion = null;

    /**
     * Push notification unique id ('Instance ID' on Android, 'Device token' on iOS) (optional)
     **/
    @SerializedName("instance_id")
    private String instanceId = null;


    public String getAppVersion() {
        return appVersion;
    }


    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }


    public String getDeviceModel() {
        return deviceModel;
    }


    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }


    public String getInstanceId() {
        return instanceId;
    }


    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }


    public String getOsType() {
        return osType;
    }


    public void setOsType(String osType) {
        this.osType = osType;
    }


    public String getOsVersion() {
        return osVersion;
    }


    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class AppInstallsViewModel {\n");

        sb.append("  deviceModel: ").append(deviceModel).append("\n");
        sb.append("  osType: ").append(osType).append("\n");
        sb.append("  osVersion: ").append(osVersion).append("\n");
        sb.append("  appVersion: ").append(appVersion).append("\n");
        sb.append("  deviceModel: ").append(deviceModel).append("\n");
        sb.append("  instanceId: ").append(instanceId).append("\n");
        sb.append("  osType: ").append(osType).append("\n");
        sb.append("  osVersion: ").append(osVersion).append("\n");

        sb.append("}\n");
        return sb.toString();
    }
}


