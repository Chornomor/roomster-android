package com.roomster.adapter;


import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.model.ProductItem;

import java.util.ArrayList;
import java.util.List;


public class PackagesAdapter extends RecyclerView.Adapter<PackagesAdapter.PackageViewHolder> {

    public interface OnItemClickListener {

        void onClick(ProductItem productItem);
    }


    private List<ProductItem>   mPackages;
    private OnItemClickListener mOnItemClickListener;


    public PackagesAdapter(@Nullable List<ProductItem> packages) {
        if (packages != null) {
            mPackages = packages;
        } else {
            mPackages = new ArrayList<>();
        }
    }


    @Override
    public PackageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return new PackageViewHolder(layoutInflater.inflate(R.layout.list_item_package, parent, false));
    }


    @Override
    public void onBindViewHolder(PackageViewHolder holder, int position) {
        final ProductItem productItem = mPackages.get(position);
        holder.titleTextView.setText(productItem.getTitle());
        holder.priceTextView.setText(productItem.getFormattedPrice());
        holder.currencyCodeTextView.setText(productItem.getCurrencyCode());
        holder.descriptionTextView.setText(productItem.getDescription());

        if (mOnItemClickListener != null) {
            holder.root.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onClick(productItem);
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return mPackages.size();
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }


    public static class PackageViewHolder extends RecyclerView.ViewHolder {

        View     root;
        TextView titleTextView;
        TextView priceTextView;
        TextView currencyCodeTextView;
        TextView descriptionTextView;


        public PackageViewHolder(View itemView) {
            super(itemView);
            root = itemView;
            titleTextView = (TextView) root.findViewById(R.id.list_item_package_title);
            priceTextView = (TextView) root.findViewById(R.id.list_item_package_price);
            currencyCodeTextView = (TextView) root.findViewById(R.id.list_item_package_currency_code);
            descriptionTextView = (TextView) root.findViewById(R.id.list_item_package_description);
        }
    }
}
