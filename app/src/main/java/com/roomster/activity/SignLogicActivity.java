package com.roomster.activity;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.AccountRestServiceEvent;
import com.roomster.event.RestServiceEvents.CatalogRestServiceEvent;
import com.roomster.event.RestServiceEvents.MeRestServiceEvent;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.RestServiceEvents.UserListingsRestServiceEvent;
import com.roomster.manager.FacebookManager;
import com.roomster.manager.GcmManager;
import com.roomster.manager.MeManager;
import com.roomster.manager.SettingsManager;
import com.roomster.rest.model.LocalizationSettingsModel;
import com.roomster.rest.model.UserGetViewModel;
import com.roomster.rest.service.AccountRestService;
import com.roomster.rest.service.CatalogRestService;
import com.roomster.rest.service.CheckAppRestService;
import com.roomster.rest.service.ListingsRestService;
import com.roomster.rest.service.LocaleRestService;
import com.roomster.rest.service.MeRestService;
import com.roomster.rest.service.UserListingsRestService;
import com.roomster.rest.service.UserStatusRestService;
import com.roomster.utils.LocaleUtil;
import com.roomster.utils.LogUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;


public class SignLogicActivity extends AppCompatActivity {

    protected static final int LOGIN_CODE_GET_ME_FAILURE = 0;
    protected static final int LOGIN_CODE_INACTIVE_USER = 1;
    protected static final int LOGIN_CODE_REMOVED_USER = 2;

    @Inject
    EventBus mEventBus;

    @Inject
    AccountRestService mAccountRestService;

    @Inject
    FacebookManager mFacebookManager;

    @Inject
    GcmManager mGcmManager;

    @Inject
    MeManager mMeManager;

    @Inject
    MeRestService mMeRestService;


    @Inject
    LocaleRestService mLocaleRestService;

    @Inject
    UserListingsRestService mUserListingsRestService;

    @Inject
    CatalogRestService mCatalogRestService;

    @Inject
    SettingsManager mSettingsManager;

    @Inject
    CheckAppRestService mCheckAppRestService;

    @Inject
    UserStatusRestService mUserStatusRestService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mEventBus.isRegistered(this)) {
            mEventBus.register(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mEventBus.isRegistered(this)) {
            mEventBus.unregister(this);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(AccountRestServiceEvent.LoginSuccess event) {
        if (mMeManager.getMe() != null) {
            if (mMeManager.getMe().getStatus() == UserGetViewModel.UserStatus.Active) {
                mGcmManager.registerToGcm();
                updateLocaleSettingsAndContinue();
                getListings();
            } else {
                loginFailure(LOGIN_CODE_INACTIVE_USER, "");
            }
        } else {
            mMeRestService.requestMe();
        }
    }


    protected void loginFailure(int code, String errorMessage) {
        if (code == LOGIN_CODE_INACTIVE_USER) {
            showDisabledProfileDialog();
        } else if (errorMessage != null) {
            Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
        }
    }


    @Subscribe
    public void onEventMainThread(CatalogRestServiceEvent.GetCatalogSuccess event) {
        openMainActivity();
    }


    @Subscribe
    public void onEventMainThread(CatalogRestServiceEvent.GetCatalogFailure event) {
        openMainActivity();
    }


    void getListings() {
        mUserListingsRestService.getUserListings(mMeManager.getMe().getId(), true);
    }


    @Subscribe
    public void onEventMainThread(UserListingsRestServiceEvent.GetListingsSuccess getListingsSuccess) {
        mMeManager.setMyListings(getListingsSuccess.listings);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetMeSuccess(MeRestServiceEvent.GetMeSuccess event) {
        if (mMeManager.getMe() != null) {
            mGcmManager.registerToGcm();
            updateLocaleSettingsAndContinue();
            getListings();
        } else {
            loginFailure(LOGIN_CODE_GET_ME_FAILURE, getString(R.string.roomster_login_failed));
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetMeFailure(MeRestServiceEvent.GetMeFailure event) {
        if (event.needReLogin) {
            mAccountRestService.reLogin();
        } else {
            loginFailure(LOGIN_CODE_GET_ME_FAILURE, getString(R.string.roomster_login_failed));
        }
    }

    private void openMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        //set a flag which will replace current activity
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        //if user doesn't have listings, add special extra
        if (mMeManager.getMe().getTotalListings() == 0) {
            intent.putExtra(MainActivity.START_ADD_LISTING, true);
        }

        startActivity(intent);
        finish();
    }


    private void updateLocaleSettingsAndContinue() {
        if (mSettingsManager.getCurrency() == null || mSettingsManager.getCurrency().isEmpty() ||
                mSettingsManager.getLocale() == null || mSettingsManager.getLocale().isEmpty()) {
            mLocaleRestService.getLocalizationSettings(new RestServiceListener<LocalizationSettingsModel>() {

                @Override
                public void onSuccess(LocalizationSettingsModel result) {
                    mSettingsManager.saveCurrency(result.getCurrency());
                    mSettingsManager.saveLocale(result.getLocale());
                    LocaleUtil.updateResources(SignLogicActivity.this, result.getLocale());
                    mSettingsManager.refreshCurrentLocale();
                    mCatalogRestService.loadCatalogs();
                }


                @Override
                public void onFailure(int code, Throwable throwable, String msg) {
                    mCatalogRestService.loadCatalogs();
                }
            });
        } else {
            mSettingsManager.refreshCurrentLocale();
            mCatalogRestService.loadCatalogs();
        }
    }

    private void showDisabledProfileDialog() {
        new AlertDialog.Builder(this).setMessage(R.string.user_profile_disabled_message)
                .setTitle(R.string.user_profile_disabled_title)
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mUserStatusRestService.activateUser(new RestServiceListener<String>() {

                            @Override
                            public void onSuccess(String result) {
                                mMeManager.setMe(null);
                                mMeRestService.requestMe();
                            }

                            @Override
                            public void onFailure(int code, Throwable throwable, String msg) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        Toast.makeText(SignLogicActivity.this, R.string.login_cant_activate_account, Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                });
                            }
                        });
                    }
                }).setNegativeButton(R.string.button_cancel, null).show();
    }
}