package com.roomster.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.fragment.ImageGridFragment;
import com.roomster.rest.model.Photo;
import com.roomster.views.ImagePager;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by michaelkatkov on 1/5/16.
 */
public class ImagePagerAdapter extends FragmentStatePagerAdapter {

    private static final int GRID_SIZE = 6;

    private List<Photo> mList;

    private ImagePager.ImageClickListener mImageClickListener;


    public ImagePagerAdapter(FragmentManager fragmentManager, List<Photo> list,
            ImagePager.ImageClickListener imageClickListener) {
        super(fragmentManager);
        mList = list;
        mImageClickListener = imageClickListener;
    }


    @Override
    public int getCount() {
        return mList.size() / GRID_SIZE + 1;
    }


    @Override
    public Fragment getItem(int position) {
        ArrayList<Photo> dataList = new ArrayList<>();
        for (int i = position * GRID_SIZE; i < (position + 1) * GRID_SIZE; i++) {
            if (i < mList.size() && mList.get(i).getId() != 0) { // check on 0 for ignore default empty photo
                dataList.add(mList.get(i));
            }
        }
        ImageGridFragment imageGridFragment = ImageGridFragment.newInstance(dataList);
        imageGridFragment.setImageClickListener(mImageClickListener);
        return imageGridFragment;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return RoomsterApplication.context.getString(R.string.image_chooser_page_title) + position;
    }
}
