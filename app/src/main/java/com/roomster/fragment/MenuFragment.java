package com.roomster.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.roomster.R;
import com.roomster.activity.*;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.MeRestServiceEvent;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.application.ApplicationEvent;
import com.roomster.manager.BookmarkManager;
import com.roomster.manager.MeManager;
import com.roomster.rest.model.BillingAvailableModel;
import com.roomster.rest.service.UserStatusRestService;
import com.roomster.utils.ImageHelper;
import com.roomster.views.MenuItemView;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;


/**
 * Created by "Michael Katkov" on 10/19/2015.
 */
public class MenuFragment extends Fragment {

    @Bind(R.id.menu_background_image)
    ImageView mBackgroundImage;

    @Bind(R.id.menu_profile_icon)
    ImageView mProfileIconIv;

    @Bind(R.id.menu_profile_text)
    TextView mProfileTv;

    @Bind(R.id.menu_profile_image_icon)
    ImageView mProfileFaceIv;

    @Bind(R.id.menu_profile_name_text)
    TextView mProfileNameTv;

    @Bind(R.id.menu_listings_icon)
    ImageView mListingsIcon;

    @Bind(R.id.menu_listings_text)
    TextView mListingsText;

    @Bind(R.id.menu_listings_counter)
    TextView mListingsCounter;

    @Bind(R.id.menu_item_upgrade)
    MenuItemView mUpgradeView;

    @Bind(R.id.menu_item_separator_upgrade)
    View mUpgradeViewSeparator;

    @Bind(R.id.menu_item_discovery_preferences)
    MenuItemView mDiscoveryPrefView;

    @Bind(R.id.menu_item_bookmarks)
    MenuItemView mBookmarksView;

    @Bind(R.id.menu_item_megaphone)
    MenuItemView mMegaphoneView;

    @Bind(R.id.menu_item_app_settings)
    MenuItemView mAppSettingsView;

    @Bind(R.id.menu_item_support)
    MenuItemView mSupportView;

    @Bind(R.id.menu_item_billing)
    MenuItemView mBillingView;

    @Bind(R.id.menu_item_separator_billing)
    View mBillingViewSeparator;

    @Inject
    EventBus mEventBus;

    @Inject
    MeManager mMeManager;

    @Inject
    Context mContext;

    @Inject
    BookmarkManager bookmarkManager;

    @Inject
    UserStatusRestService mUserStatusRestService;


    private Boolean isBillingAvailable = false;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        ButterKnife.bind(this, view);
        fillData();
        checkBillingAvailable();
        return view;
    }

    private void checkBillingAvailable() {
        mUserStatusRestService.checkUserStatus(new RestServiceListener<BillingAvailableModel>() {
            @Override
            public void onSuccess(BillingAvailableModel result) {
                isBillingAvailable = result.billing_available;
                if (isBillingAvailable) showBillingView();
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        mEventBus.register(this);
        if (mMeManager.getMe() != null && mMeManager.getMe().getFullAccess() != null && mMeManager.getMe()
                .getFullAccess()) {
            mUpgradeView.setVisibility(View.GONE);
            mUpgradeViewSeparator.setVisibility(View.GONE);
            setListingsCount();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        mEventBus.unregister(this);
    }


    private void checkImages() {
        if (mMeManager.getMe() != null) {
            loadData();
        }
    }


    private void fillData() {
        setUpgradeView();
        setDiscoveryPrefView();
        setBookmarksView();
        setMegaphoneView();
        setAppSettingsView();
        setSupportView();
        setBillingView();
        checkImages();
    }


    private void setUpgradeView() {
        mUpgradeView.setIcon(R.drawable.menu_item_upgrade_icon_selector);
        mUpgradeView.setUpperText(R.string.menu_upgrade_up_text);
        mUpgradeView.setLowerText(R.string.menu_upgrade_low_text);
    }


    private void setDiscoveryPrefView() {
        mDiscoveryPrefView.setIcon(R.drawable.menu_item_discovery_icon_selector);
        mDiscoveryPrefView.setUpperText(R.string.menu_discovery_preferences_up_text);
        mDiscoveryPrefView.setLowerText(R.string.menu_discovery_preferences_low_text);
    }


    private void setBookmarksView() {
        mBookmarksView.setIcon(R.drawable.menu_item_bookmarks_icon_selector);
        mBookmarksView.setUpperText(R.string.menu_bookmarks_up_text);
        mBookmarksView.setLowerText(R.string.menu_bookmarks_low_text);
    }


    private void setMegaphoneView() {
        mMegaphoneView.setIcon(R.drawable.menu_item_megaphone_icon_selector);
        mMegaphoneView.setUpperText(R.string.menu_megaphone_up_text);
        mMegaphoneView.setLowerText(R.string.menu_megaphone_low_text);
    }


    private void setAppSettingsView() {
        mAppSettingsView.setIcon(R.drawable.menu_item_settings_icon_selector);
        mAppSettingsView.setUpperText(R.string.menu_app_settings_up_text);
        mAppSettingsView.setLowerText(R.string.menu_app_settings_low_text);
    }


    private void setSupportView() {
        mSupportView.setIcon(R.drawable.menu_item_24_7_icon_selector);
        mSupportView.setUpperText(R.string.menu_support_up_text);
        mSupportView.setLowerText(R.string.menu_support_low_text);
    }


    private void setBillingView() {
            mBillingView.setIcon(R.drawable.menu_item_billing_icon_selector);
            mBillingView.setUpperText(R.string.menu_billing_up_text);
            mBillingView.setLowerText(R.string.menu_billing_low_text);
    }

    private void showBillingView(){
        mBillingView.setVisibility(View.VISIBLE);
        mBillingViewSeparator.setVisibility(View.VISIBLE);
    }

    @OnClick({ R.id.menu_profile_icon, R.id.menu_profile_text, R.id.menu_profile_image_icon, R.id.menu_profile_name_text
             })
    public void onMyProfile() {
        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        startActivity(intent);
    }


    @OnClick({ R.id.menu_listings_icon, R.id.menu_listings_text })
    public void onMyListings() {
        Intent intent = new Intent(getActivity(), UserListingsListActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.menu_item_upgrade)
    public void onUpgrade() {
        Intent intent = new Intent(getActivity(), UpgradeAccountActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.menu_item_discovery_preferences)
    public void onDiscoveryPref() {
        Intent intent = new Intent(getActivity(), DiscoveryPreferencesActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.menu_item_bookmarks)
    public void onBookmarks() {
        Intent intent = new Intent(getActivity(), BookmarksActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.menu_item_megaphone)
    public void onMegaphone() {
        Intent intent = new Intent(mContext, MegaphoneActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.menu_item_app_settings)
    public void onAppSettings() {
        Intent intent = new Intent(getActivity(), AppSettingsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.menu_item_billing)
    public void onBilling(){
        Intent intent = new Intent(getActivity(), BillingActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.menu_item_support)
    public void onSupportClicked() {
        Intent intent = new Intent(getActivity(), SupportActivity.class);
        startActivity(intent);
    }


    private void loadData() {
        loadTitleImage();
        setFirstName();
        setListingsCount();
    }


    private void setListingsCount() {
        if (mMeManager.getMe() != null) {
            Integer listingsCount = mMeManager.getMe().getTotalListings();
            if (listingsCount == null) {
                listingsCount = 0;
            }
            mListingsCounter.setText(String.valueOf(listingsCount));
        }
    }


    private void loadTitleImage() {
        String titleImageUrl = mMeManager.getMe().getTitleImage();
        if (titleImageUrl != null) {
            ImageHelper.loadImageInto(mContext, titleImageUrl, mBackgroundImage);
            ImageHelper.loadCircledImageInto(mContext, titleImageUrl, mProfileFaceIv);
        }
    }


    private void setFirstName() {
        String firstName = mMeManager.getMe().getFirstName();
        if (firstName != null) {
            mProfileNameTv.setText(firstName);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(MeRestServiceEvent.GetMeSuccess event) {
        loadData();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(MeRestServiceEvent.GetMeFailure event) {
        Toast.makeText(mContext, getString(R.string.unable_to_get_user_info), Toast.LENGTH_SHORT).show();
    }
}
