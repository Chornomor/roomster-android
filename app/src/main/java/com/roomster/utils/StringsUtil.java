package com.roomster.utils;


import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


/**
 * Created by "Michael Katkov" on 10/11/2015.
 */
public class StringsUtil {

    public static String getFirstName(String name) {
        int firstSpace = name.indexOf(" ");
        if (firstSpace != -1) {
            return name.substring(0, firstSpace).trim();
        } else {
            return "";
        }
    }


    public static String getLastName(String name) {
        int firstSpace = name.indexOf(" ");
        if (firstSpace != -1) {
            return name.substring(firstSpace).trim();
        } else {
            return "";
        }
    }


    public static String capitalizeFirst(String string) {
        return string.substring(0, 1).toUpperCase() + string.substring(1);
    }

    //TODO: Check if we need this method
    public static String getLocationName(Context context, Location location) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        String locationName = "";
        if(geocoder == null){
            // geocoder broken, rare issue can be fixed only by rebooting device
            //TODO add some notification here ?
            Toast.makeText(context, "Please, reboot you device", Toast.LENGTH_SHORT).show();
            return locationName;
        }
        if(location == null){
            return locationName;
        }
        try {
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (addresses != null && !addresses.isEmpty()) {
                String cityName = addresses.get(0).getAddressLine(0);
                if (cityName != null) {
                    locationName += cityName + ", ";
                }

                String stateName = addresses.get(0).getAddressLine(1);
                if (stateName != null) {
                    locationName += stateName + ", ";
                }

                String countryName = addresses.get(0).getAddressLine(2);
                if (countryName != null) {
                    locationName += countryName;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            // Nothing to do
        }

        return locationName;
    }
}

