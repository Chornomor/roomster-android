package com.roomster.rest.service;

import com.facebook.AccessToken;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.RestCode;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.manager.FacebookManager;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.TokenManager;
import com.roomster.rest.api.FBMutualFriendsApi;
import com.roomster.rest.model.ConversationView;
import com.roomster.rest.model.MembersCountModel;
import com.roomster.rest.model.MutualFriendsModel;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yuliasokolova on 11.10.16.
 */
public class FbMutualFriendsService {
    @Inject
    EventBus mEventBus;

    @Inject
    FacebookManager mFbManager;

    @Inject
    FBMutualFriendsApi mFbMutualFriendsApi;

    @Inject
    NetworkManager mNetworkManager;


    public FbMutualFriendsService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }

    public void callGetFbMutualFriends(Long user_id, final RestServiceListener<MutualFriendsModel> listener){
        if (mNetworkManager.hasNetworkAccess()) {
            Call<MutualFriendsModel> callGetMutualFriends = mFbMutualFriendsApi
                    .fBMutualFriendsGet(user_id, mFbManager.getAccessToken());
            callGetMutualFriends.enqueue(new Callback<MutualFriendsModel>() {

                @Override
                public void onResponse(Call<MutualFriendsModel> call, Response<MutualFriendsModel>response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            listener.onSuccess(response.body());
                            break;
                        case ResponseCode.BAD_REQUEST:
                        case ResponseCode.SERVER_ERROR:
                        default:
                            listener.onFailure(response.code(), null, null);
                    }
                }

                @Override
                public void onFailure(Call<MutualFriendsModel> call, Throwable t) {
                    if (listener != null)
                        listener.onFailure(RestCode.REQUEST_NOT_MADE, null, null);
                }
            });
        } else {
            listener.onFailure(RestCode.REQUEST_NOT_MADE, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
        }

    }

}
