package com.roomster.dialog;


import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import com.roomster.R;
import com.roomster.adapter.CatalogListAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.model.CatalogModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by michaelkatkov on 1/6/16.
 */
public class CatalogListChooserDialog extends android.support.v7.app.AlertDialog {

    @Inject
    LayoutInflater mInflater;

    @Bind(R.id.shared_details_list)
    ListView mList;

    private List<CatalogModel> mCatalogList;

    private List<Integer> mCurrentIds;

    private CatalogListAdapter mAdapter;

    private OKButtonClickListener mOkButtonClick;

    private boolean mIsMultipleChoice;


    public CatalogListChooserDialog(Context context, List<CatalogModel> catalogList, List<Integer> currentIds,
                                    boolean isMultipleChoice, OKButtonClickListener okButtonClickListener, String title) {
        super(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
        if (catalogList != null ) {
            mCatalogList = catalogList;
        } else {
            mCatalogList = new ArrayList<>();
        }
        if (currentIds != null) {
            mCurrentIds = currentIds;
        } else {
            mCurrentIds = new ArrayList<>();
        }
        mIsMultipleChoice = isMultipleChoice;
        mOkButtonClick = okButtonClickListener;
        setTitle(title);
        initViews();
        initListView();
        initButtons();
    }


    private void initViews() {
        View contentView = mInflater.inflate(R.layout.dialog_shared_details, null);
        ButterKnife.bind(this, contentView);
        setView(contentView);
    }


    private void initListView() {
        mAdapter = new CatalogListAdapter(getContext(), mCatalogList, mIsMultipleChoice, mCurrentIds);
        mList.setAdapter(mAdapter);
    }


    private void initButtons() {
        if (mIsMultipleChoice) {
            setButton(BUTTON_POSITIVE, getContext().getString(R.string.done), new OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mOkButtonClick.onOkClick(mAdapter.getSelectedIds());
                }
            });
        } else {
            mAdapter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOkButtonClick.onOkClick(mAdapter.getSelectedIds());
                    dismiss();
                }
            });
        }

        setButton(BUTTON_NEGATIVE, getContext().getString(R.string.dialog_cancel_button), new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });
    }


    public interface OKButtonClickListener {

        void onOkClick(List<Integer> categoryId);
    }
}
