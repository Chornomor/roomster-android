package com.roomster.rest.service;


import com.google.android.gms.maps.model.LatLngBounds;
import com.roomster.adapter.PackagesAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.cache.sharedpreferences.SharedPreferencesFiles;
import com.roomster.constants.DiscoveryConstants;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.RestServiceEvents.SearchRestServiceEvent;
import com.roomster.event.application.ApplicationEvent;
import com.roomster.manager.BookmarkManager;
import com.roomster.manager.BookmarkManager;
import com.roomster.manager.DiscoveryPrefsManager;
import com.roomster.manager.MeManager;
import com.roomster.manager.MegaphoneManager;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.SearchManager;
import com.roomster.manager.SettingsManager;
import com.roomster.manager.TokenManager;
import com.roomster.model.SearchCriteria;
import com.roomster.rest.api.SearchApi;
import com.roomster.rest.model.BudgetSettingsModel;
import com.roomster.rest.model.ResultModel;
import com.roomster.utils.LogUtil;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.Locale;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SearchRestService {

    public static final String BOOKMARKED_TYPE = "Bookmarked";

    private static volatile boolean mCallInProgress;
    private static volatile boolean mBookmarkCallInProgress;

    @Inject
    EventBus mEventBus;

    @Inject
    SearchApi mSearchApi;

    @Inject
    DiscoveryPrefsManager mDiscoveryPrefManager;

    @Inject
    MegaphoneManager megaphoneManager;

    @Inject
    SearchManager mSearchManager;

    @Inject
    BookmarkManager bookmarkManager;

    @Inject
    SettingsManager mSettingsManager;

    @Inject
    TokenManager mTokenManager;

    @Inject
    MeManager mMeManager;

    @Inject
    NetworkManager mNetworkManager;

    @Inject
    SharedPreferencesFiles mSharedPreferencesFiles;

    private boolean isRequestCancelled = false;
    private Call lastCall;

    private SearchCriteria mSearchCriteria;

    public SearchRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }

    public boolean mapMode = false;

    public boolean hasMoreResults() {
        if (mSearchManager.getResultModel() != null) {
            return mSearchManager.getResultModel().getCount() > mSearchManager.getResultModel().getItems().size();
        } else {
            return true;
        }
    }


    public boolean hasMoreBookmarks() {
        if (bookmarkManager.getResultModel() != null) {
            return bookmarkManager.getResultModel().getCount() > bookmarkManager.getResultModel().getItems().size();
        } else {
            return true;
        }
    }


    public void loadNextResultsPage(int pageSize, boolean withLoading) {
        if (!isCallInProgress()) {
            if (hasMoreResults()) {
                executeSearch(mSearchManager.getPageCount() + 1, pageSize, null, withLoading);
            }
        }
    }


    public void loadNextBookmarkedResultsPage(int pageSize, boolean withLoading) {
        if (!isBookmarkCallInProgress()) {
            if (hasMoreBookmarks()) {
                executeSearch(bookmarkManager.getPageCount() + 1, pageSize, BOOKMARKED_TYPE, withLoading);
            }
        }
    }


    private void executeSearch(final int pageNumber, final int pageSize, final String bookmark, final boolean withLoading) {
        isRequestCancelled = false;
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callExecuteSearch(result, pageNumber, pageSize, bookmark, withLoading);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                mEventBus.post(new SearchRestServiceEvent.SearchFailure(isRequestCancelled));
            }
        });
    }


    private void callExecuteSearch(String auth, int pageNumber, int pageSize, final String bookmark, final boolean withLoading) {
        if (mNetworkManager.hasNetworkAccess()) {
            turnOnCallInProgress(bookmark);
            if (withLoading) {
                mEventBus.post(new ApplicationEvent.ShowLoading());
            }

            final SearchCriteria criteria = getSearchCriteria();
            if (!mapMode && mSearchManager.getSearchCriteria() != null) {
                if (  !mSearchManager.getSearchCriteria().equals(criteria)) {
                    mSearchManager.clearResultModel();
                }
            }

            Call<ResultModel> call = prepareCall(auth, pageNumber, pageSize, bookmark, criteria);
            call.enqueue(new Callback<ResultModel>() {

                @Override
                public void onResponse(Call<ResultModel> call, Response<ResultModel> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            if(bookmark == null) {
                                if (mSearchManager.getPageCount() == 0 ) {
                                    mSearchManager.setResultModel(response.body(), criteria);
                                } else {
                                    mSearchManager.addResultModel(response.body());
                                }
                                mEventBus.post(new SearchRestServiceEvent.SearchSuccess());
                            } else {
                                if(bookmarkManager.getPageCount() == 0) {
                                    bookmarkManager.setResultModel(response.body());
                                } else {
                                    bookmarkManager.addResultModel(response.body());
                                }
                                mEventBus.post(new SearchRestServiceEvent.BookmarkSearchSuccess());
                            }
                            break;
                        default:
                            mEventBus.post(new SearchRestServiceEvent.SearchFailure());
                    }
                    if (withLoading) {
                        mEventBus.post(new ApplicationEvent.HideLoading());
                    }
                    turnOffCallInProgress(bookmark);
                }

                @Override
                public void onFailure(Call<ResultModel> call, Throwable t) {
                    if (withLoading) {
                        mEventBus.post(new ApplicationEvent.HideLoading());
                    }
                    turnOffCallInProgress(bookmark);
                    mEventBus.post(new SearchRestServiceEvent.SearchFailure(isRequestCancelled));
                }
            });
            lastCall = call;
        } else {
            mEventBus.post(new SearchRestServiceEvent.SearchFailure());
        }
    }
    public void cancel(){
        if(lastCall != null){
            isRequestCancelled = true;
            lastCall.cancel();
            setCallInProgress(false);
        }
    }

    private void turnOnCallInProgress(String type) {
        if(type == null) {
            setCallInProgress(true);
        } else {
            setBookmarkCallInProgress(true);
        }
    }

    private void turnOffCallInProgress(String type) {
        if(type == null) {
            setCallInProgress(false);
        } else {
            setBookmarkCallInProgress(false);
        }
    }

    public SearchCriteria getSearchCriteria() {
        if(mapMode && mSearchManager.getSearchCriteria() != null)
            return mSearchManager.getSearchCriteria();
        if(mSearchCriteria!= null)
            return mSearchCriteria;
        return mDiscoveryPrefManager.getSearchCriteria();
    }
    public void resetSearchCriteria(){
        mSearchCriteria = null;
    }

    /**
     * To make bookmarks call we use almost all parameters as null, otherwise we need to fill parameters
     */
    private Call<ResultModel> prepareCall(String auth, int pageNumber, int pageSize, final String bookmark,
                                          SearchCriteria criteria) {
        if (bookmark == null) {
            return mSearchApi
              .searchGet(pageNumber, pageSize, criteria.getSort(), criteria.getServiceType(), criteria.getLatSouthWest(),
                criteria.getLongSouthWest(), criteria.getLatNorthEast(), criteria.getLongNorthEast(), criteria.getRadiusScale(),
                criteria.getBudgetMin(), criteria.getBudgetMax(), criteria.getAgeMin(), criteria.getAgeMax(),
                criteria.getHouseholdSex(), criteria.getSex(),
                    /* zodiac */ null, criteria.getPetsPrefs(), criteria.getMyPets(), criteria.getBedrooms(),
                criteria.getBathrooms(), criteria.getAmenities(), bookmark, criteria.getDataIn(), criteria.getDataOut(),
                criteria.getCurrency(),  mSettingsManager.getLocale(), auth);
        } else {
            return mSearchApi
              .searchGet(pageNumber, pageSize, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
                null, null, null, null, null, bookmark, null, null, criteria.getCurrency(), mSettingsManager.getLocale(), auth);
        }
    }


    private synchronized void setCallInProgress(boolean isInProgress) {
        mCallInProgress = isInProgress;
    }


    private synchronized boolean isCallInProgress() {
        return mCallInProgress;
    }


    private synchronized void setBookmarkCallInProgress(boolean isInProgress) {
        mBookmarkCallInProgress = isInProgress;
    }


    private synchronized boolean isBookmarkCallInProgress() {
        return mBookmarkCallInProgress;
    }


    public void updateCurrentBudget(Integer minInUSD, Integer maxInUSD, String currency, String locate) {
        Call<BudgetSettingsModel> call = prepearCurrentBudget(minInUSD, maxInUSD, currency, locate);
        call.enqueue(new Callback<BudgetSettingsModel>() {
            @Override
            public void onResponse(Call<BudgetSettingsModel> call, Response<BudgetSettingsModel> response) {
                try {
                    BudgetSettingsModel model = response.body();
                    int maxBudget = model.getMax();
                    mDiscoveryPrefManager.storeMinBudget(model.getMin());
                    mDiscoveryPrefManager.storeMaxBudget(maxBudget);
                    megaphoneManager.storeMinBudget(model.getMin());
                    megaphoneManager.storeMaxBudget(maxBudget);
                    DiscoveryConstants.MAX_BUDGET = maxBudget;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<BudgetSettingsModel> call, Throwable t) {
                LogUtil.logE("App set default budge because update Budget Settings was failed");
            }
        });
    }


    private Call<BudgetSettingsModel> prepearCurrentBudget(Integer minInUSD, Integer maxInUSD, String currency, String locate) {
        return mSearchApi.getCurrentBudget(minInUSD, maxInUSD, currency, locate);
    }
}
