package com.roomster.utils;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Parcelable;
import android.widget.Toast;

import com.roomster.R;
import com.roomster.rest.model.ListingGetViewModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by michaelkatkov on 1/12/16.
 */
public class ShareUtil {

    public static void shareListing(Context context, ListingGetViewModel listingGetViewModel, Uri bitmapUri) {
        Resources resources = context.getResources();
        String subject = prepareSubjectString(resources, listingGetViewModel);
        String body = prepareBodyString(resources, listingGetViewModel);
        List<Intent> targetShareIntents = prepareShareIntents(context, subject, body, bitmapUri);
        showChooser(context, targetShareIntents);
    }


    private static void showChooser(Context context, List<Intent> targetShareIntents) {
        if (!targetShareIntents.isEmpty()) {
            Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), context.getString(R.string.share_title));
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
            context.startActivity(chooserIntent);
        } else {
            Toast.makeText(context, context.getString(R.string.no_apps_to_share), Toast.LENGTH_SHORT).show();
        }
    }


    private static List<Intent> prepareShareIntents(Context context, String subject, String body, Uri bitmapUri) {
        List<Intent> targetShareIntents = new ArrayList<Intent>();
        PackageManager packageManager = context.getPackageManager();
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        List<ResolveInfo> resInfos = context.getPackageManager().queryIntentActivities(shareIntent, 0);
        for (ResolveInfo resInfo : resInfos) {
            String packageName = resInfo.activityInfo.packageName;
            Intent intent = makeIntent(resInfo, subject, body, bitmapUri);
            if (intent != null) {
                targetShareIntents.add(new LabeledIntent(intent, packageName, resInfo.loadLabel(packageManager), resInfo.icon));
            }
        }
        return targetShareIntents;
    }


    private static Intent makeIntent(ResolveInfo resInfo, String subject, String body, Uri bitmapUri) {
        String packageName = resInfo.activityInfo.packageName;
        Intent intent;
        if (packageName.contains("sms") || packageName.contains("mms") || packageName.contains("twitter")) {
            intent = twitterOrSmsIntent(resInfo, subject, bitmapUri);
        } else if (packageName.contains("facebook")) {
            intent = facebookIntent(resInfo, subject);
        } else if (packageName.contains("android.gm") || packageName.contains("android.email")) {
            intent = emailIntent(resInfo, body, subject, bitmapUri);
        } else {
            intent = null;
        }
        return intent;
    }


    private static Intent prepareStandardIntent(ResolveInfo resInfo) {
        String packageName = resInfo.activityInfo.packageName;
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
        intent.setAction(Intent.ACTION_SEND);
        intent.setPackage(resInfo.activityInfo.packageName);
        return intent;
    }


    private static Intent twitterOrSmsIntent(ResolveInfo resInfo, String subject, Uri bitmapUri) {
        Intent intent = prepareStandardIntent(resInfo);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_TEXT, subject);
        intent.putExtra(Intent.EXTRA_STREAM, bitmapUri);
        return intent;
    }


    private static Intent facebookIntent(ResolveInfo resInfo, String text) {
        Intent intent = prepareStandardIntent(resInfo);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, text);
        return intent;
    }


    private static Intent emailIntent(ResolveInfo resInfo, String text, String subject, Uri bitmapUri) {
        Intent intent = prepareStandardIntent(resInfo);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_STREAM, bitmapUri);
        return intent;
    }


    private static String prepareSubjectString(Resources resources, ListingGetViewModel listingGetViewModel) {
        return resources.getString(R.string.share_subject) + listingGetViewModel.getUrl();
    }


    private static String prepareBodyString(Resources resources, ListingGetViewModel listingGetViewModel) {
        return String.format(resources.getString(R.string.share_body), listingGetViewModel.getGeoLocation().getFullAddress(),
          listingGetViewModel.getRates().getMonthlyRate(), listingGetViewModel.getUrl(), "{link to itunes}",
          "{link to GooglePlay}");
    }
}
