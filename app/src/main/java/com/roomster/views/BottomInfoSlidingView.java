package com.roomster.views;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import butterknife.Bind;
import butterknife.ButterKnife;

import com.roomster.R;
import com.roomster.adapter.AmenitiesAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.ListingType;
import com.roomster.enums.GenderEnum;
import com.roomster.manager.CatalogsManager;
import com.roomster.manager.MeManager;
import com.roomster.model.CatalogModel;
import com.roomster.model.CatalogValueModel;
import com.roomster.rest.model.*;
import com.roomster.utils.ImageHelper;

import javax.inject.Inject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Currency;
import java.util.List;


public class BottomInfoSlidingView extends RelativeLayout {

    private static final int AMENITIES_COLUMNS = 5;

    @Bind(R.id.bs_profile_content)
    public FrameLayout mContentView;

    @Bind(R.id.bottom_view_scroll)
    ScrollView mBottomView;

    @Bind(R.id.bs_scroll_content)
    View scrollContent;

    @Bind(R.id.bs_iv_name)
    TextView mNameTv;

    @Bind(R.id.bs_iv_gender)
    ImageView mGenderIv;

    @Bind(R.id.listing_type)
    TextView mListingTypeTextView;

    @Bind(R.id.currency_sign_text_view)
    TextView mCurrencySign;

    @Bind(R.id.price_text_view)
    TextView mPrice;

    @Bind(R.id.currency_text_view)
    TextView mCurrencyText;

    @Bind(R.id.bs_iv_period_text)
    TextView mPeriodText;

    @Bind(R.id.listing_headline_text)
    TextView mLocationText;

    @Bind(R.id.bs_iv_summary_text)
    TextView mSummaryText;

    @Bind(R.id.bs_iv_description_text)
    TextView mDescriptionText;

    @Bind(R.id.bs_grid_amenities_layout)
    LinearLayout mAmenitiesGridLayout;

    @Bind(R.id.bs_amenities_grid)
    GridView mAmenitiesGrid;

    @Bind(R.id.bs_amenities_title_layout)
    LinearLayout mAmenitiesTitle;

    @Bind(R.id.bs_lifestyle_items_layout)
    LinearLayout mLifestyleItemsLayout;

    @Bind(R.id.bs_lifestyle_title_layout)
    LinearLayout mLifestyleTitle;

    @Bind(R.id.bs_lifestyle_household_age_container)
    LinearLayout mLifestyleHouseholdAgeContainer;

    @Bind(R.id.bs_lifestyle_household_age)
    TextView mLifestyleHouseholdAge;

    @Bind(R.id.bs_lifestyle_smoker_container)
    LinearLayout mLifestyleSmokerContainer;

    @Bind(R.id.bs_lifestyle_smoker)
    TextView mLifestyleSmoker;

    @Bind(R.id.bs_lifestyle_people_in_household_container)
    LinearLayout mLifestylePeopleInHouseholdContainer;

    @Bind(R.id.bs_lifestyle_people_in_household)
    TextView mLifestylePeopleInHousehold;

    @Bind(R.id.bs_lifestyle_bed_time_container)
    LinearLayout mLifestyleBedTimeContainer;

    @Bind(R.id.bs_lifestyle_bed_time)
    TextView mLifestyleBedTime;

    @Bind(R.id.bs_lifestyle_cleanliness_container)
    LinearLayout mLifestyleCleanlinessContainer;

    @Bind(R.id.bs_lifestyle_cleanliness)
    TextView mLifestyleCleanliness;

    @Bind(R.id.bs_lifestyle_food_preference_container)
    LinearLayout mLifestyleFoodPreferenceContainer;

    @Bind(R.id.bs_lifestyle_food_preference)
    TextView mLifestyleFoodPreference;

    @Bind(R.id.bs_lifestyle_overnight_guests_container)
    LinearLayout mLifestyleOvernightGuestsContainer;

    @Bind(R.id.bs_lifestyle_overnight_guests)
    TextView mLifestyleOvernightGuests;

    @Bind(R.id.bs_lifestyle_work_schedule_container)
    LinearLayout mLifestyleWorkScheduleContainer;

    @Bind(R.id.bs_lifestyle_work_schedule)
    TextView mLifestyleWorkSchedule;

    @Bind(R.id.bs_lifestyle_party_habits_container)
    LinearLayout mLifestylePartyHabitsContainer;

    @Bind(R.id.bs_lifestyle_party_habits)
    TextView mLifestylePartyHabits;

    @Bind(R.id.bs_lifestyle_occupation_container)
    LinearLayout mLifestyleOccupationContainer;

    @Bind(R.id.bs_lifestyle_occupation)
    TextView mLifestyleOccupation;

    @Bind(R.id.bs_lifestyle_pets_owner_container)
    LinearLayout mLifestylePetsOwnerContainer;

    @Bind(R.id.pets_table_layout)
    PetsTableLayout mPetsTableLayout;

    @Bind(R.id.bs_iv_roommate_items_layout)
    LinearLayout mRoommateItemsLayout;

    @Bind(R.id.bs_iv_roommate_title_layout)
    LinearLayout mRoommatePrefsTitle;

    @Bind(R.id.bs_roommate_age)
    TextView mRoommateAge;

    @Bind(R.id.bs_roommate_age_container)
    LinearLayout mRoommateAgeContainer;

    @Bind(R.id.bs_roommate_smoking)
    TextView mRoommateSmoking;

    @Bind(R.id.bs_roommate_smoking_container)
    LinearLayout mRoommateSmokingContainer;

    @Bind(R.id.roommate_pets_table_layout)
    PetsTableLayout mRoommatePetsTableLayout;

    @Bind(R.id.bs_roommate_pets_container)
    LinearLayout mRoommatePetsContainer;

    @Bind(R.id.bs_roommate_students_only)
    TextView mRoommateStudentsOnly;

    @Bind(R.id.bs_roommate_students_only_container)
    LinearLayout mRoommateStudentsOnlyContainer;

    @Bind(R.id.bs_apartment_preferences_title_layout)
    View mApartmentPreferencesTitle;

    @Bind(R.id.bs_apartment_preferences_items_layout)
    View mApartmentPreferencesItems;

    @Bind(R.id.bs_apartment_preferences_type_container)
    View mApartmentPreferencesTypeContainer;

    @Bind(R.id.bs_apartment_preferences_type)
    TextView mApartmentPreferencesType;

    @Bind(R.id.bs_apartment_preferences_furnished_container)
    View mApartmentPreferencesFurnishedContainer;

    @Bind(R.id.bs_apartment_preferences_furnished)
    TextView mApartmentPreferencesFurnished;

    @Bind(R.id.bottom_profile_pic)
    ImageView mBottomProfileImageView;

    @Bind(R.id.view_user_profile_title)
    TextView mViewUserProfileTitle;

    @Bind(R.id.profile_pic)
    ImageView mProfileImageView;

    @Bind(R.id.bs_residence_items_layout)
    LinearLayout mResidenceItemsLayout;

    @Bind(R.id.bs_residence_title_layout)
    LinearLayout mResidenceTitle;

    @Bind(R.id.bs_residence_building_type_container)
    View mResidenceBuildingTypeContainer;

    @Bind(R.id.bs_residence_building_type)
    TextView mResidenceBuildingType;

    @Bind(R.id.bs_residence_parking_rent_container)
    View mResidenceParkingRentContainer;

    @Bind(R.id.bs_residence_parking_rent)
    TextView mResidenceParkingRent;

    @Bind(R.id.bs_residence_move_in_fee_container)
    View mResidenceMoveInFeeContainer;

    @Bind(R.id.bs_residence_move_in_fee)
    TextView mResidenceMoveInFee;

    @Bind(R.id.bs_residence_utilities_cost_container)
    View mResidenceUtilitiesCostContainer;

    @Bind(R.id.bs_residence_utilities_cost)
    TextView mResidenceUtilitiesCost;

    @Bind(R.id.bs_residence_is_furnished_container)
    View mResidenceFurnishedContainer;

    @Bind(R.id.bs_residence_is_furnished)
    TextView mResidenceFurnished;

    @Bind(R.id.bs_residence_availability_date_container)
    View mResidenceAvailabilityDateContainer;

    @Bind(R.id.bs_residence_availability_date)
    TextView mResidenceAvailabilityDate;

    @Bind(R.id.bs_residence_minimum_stay_container)
    View mResidenceMinimumStayContainer;

    @Bind(R.id.bs_residence_minimum_stay)
    TextView mResidenceMinimumStay;

    @Bind(R.id.bs_residence_bathrooms_container)
    View mResidenceBathroomsContainer;

    @Bind(R.id.bs_residence_bathrooms)
    TextView mResidenceBathrooms;

    @Bind(R.id.bs_residence_bedrooms_container)
    View mResidenceBedroomsContainer;

    @Bind(R.id.bs_residence_bedrooms)
    TextView mResidenceBedrooms;

    @Bind(R.id.bs_residence_apartment_size_container)
    View mResidenceApartmentSizeContainer;

    @Bind(R.id.bs_residence_apartment_size)
    TextView mResidenceApartmentSize;

    @Bind(R.id.see_user_profile_wrapper)
    RelativeLayout mSeeUserProfileRL;

    @Bind(R.id.bs_description_layout)
    LinearLayout mDescriptionLayout;

    @Bind(R.id.bs_profile_actions_container)
    View mProfileActionsContainer;

    @Bind(R.id.report_button)
    TextView mReportTv;

    @Bind(R.id.block_button)
    TextView mBlockTv;

    @Bind(R.id.clickable_view)
    View mClickableView;

    @Inject
    CatalogsManager mCatalogsManager;

    @Inject
    MeManager meManager;


    public BottomInfoSlidingView(Context context) {
        super(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
        initView();
    }


    public BottomInfoSlidingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        RoomsterApplication.getRoomsterComponent().inject(this);
        initView();
    }


    public void setData(ResultItemModel resultItemModel) {
        setName(resultItemModel);
        setGender(resultItemModel);
        setProfilePicture(resultItemModel);
        setCurrency(resultItemModel);
        setPrice(resultItemModel);
        setLocation(resultItemModel);
        setSummary(resultItemModel);
        setDescription(resultItemModel);
        setSeeUserProfile(resultItemModel);
        setListingInfo(resultItemModel);
    }
    public void setLocationExpanded(){
        mLocationText.setSingleLine(false);
        mLocationText.setMaxEms(100);
    }

    public void setIsMyListing(boolean isMyListing){
        if(isMyListing){
            return;
        }
        float negativeOffset = getResources().getDimension(R.dimen.offset_minus_5);
        View v = findViewById(R.id.rl_container);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) v.getLayoutParams();
        params.bottomMargin = (int) negativeOffset;
        v.setLayoutParams(params);
    }

    public void setCurrency(String currencyCode) {

        mCurrencyText.setText(currencyCode);

        String currencySymbol = Currency.getInstance(currencyCode).getSymbol();
        if (currencySymbol != null && !currencySymbol.isEmpty()) {
            mCurrencySign.setText(Character.toString(currencySymbol.charAt(currencySymbol.length() - 1)));
        }
    }


    public void setPrice(int price) {
        mPrice.setText(String.valueOf(price));
    }


    public void displayProfileOptions(boolean shouldDisplay) {
        if (shouldDisplay) {
            mSeeUserProfileRL.setVisibility(VISIBLE);
            mProfileActionsContainer.setVisibility(VISIBLE);
        } else {
            mSeeUserProfileRL.setVisibility(GONE);
            mProfileActionsContainer.setVisibility(GONE);
        }
    }


    public void setBottomViewVisibility(int visibility) {
        ViewGroup.LayoutParams layoutParams = mContentView.getLayoutParams();
        if (visibility == VISIBLE) {
            layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
            mClickableView.setVisibility(GONE);
            int toolbarHeight = (int) getResources()
                    .getDimension(R.dimen.detailed_view_toolbar_height_with_border);
            scrollContent.setPadding(scrollContent.getPaddingLeft(), scrollContent.getPaddingTop(),
                    scrollContent.getPaddingRight(), toolbarHeight);

        } else {
            layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            mClickableView.setVisibility(VISIBLE);

        }
        mDescriptionText.setVisibility(visibility);
        mDescriptionLayout.setVisibility(visibility);
        mContentView.setLayoutParams(layoutParams);

    }


    public void setOnSeeUserProfileClickListener(View.OnClickListener listener) {
        mSeeUserProfileRL.setOnClickListener(listener);
    }


    public void setOnReportClickListener(View.OnClickListener listener) {
        mReportTv.setOnClickListener(listener);
    }


    public void setOnBlockClickListener(View.OnClickListener listener) {
        mBlockTv.setOnClickListener(listener);
    }


    private void initView() {
        inflate(getContext(), R.layout.view_info_sliding, this);
        ButterKnife.bind(this);
        setBottomViewVisibility(GONE);
        mNameTv.setVisibility(GONE);
        mGenderIv.setVisibility(GONE);
        mLocationText.setVisibility(GONE);
        mSummaryText.setVisibility(GONE);
        mAmenitiesGridLayout.setVisibility(GONE);
        mLifestyleItemsLayout.setVisibility(GONE);
        mRoommateItemsLayout.setVisibility(GONE);
        mProfileImageView.setVisibility(GONE);
    }


    private void setName(ResultItemModel resultItemModel) {
        if (resultItemModel.getUser() != null && resultItemModel.getUser().getFirstName() != null) {
            String userName = resultItemModel.getUser().getFirstName() + ", " + resultItemModel.getUser().getAge();
            mNameTv.setText(userName);
            mNameTv.setVisibility(VISIBLE);
        }
    }


    private void setGender(ResultItemModel resultItemModel) {
        if (resultItemModel.getUser() != null && resultItemModel.getUser().getGender() != null) {
            if (resultItemModel.getUser().getGender().equals(GenderEnum.Male)) {
                mGenderIv.setImageResource(R.drawable.ic_male);
            } else {
                mGenderIv.setImageResource(R.drawable.ic_female);
            }
            mGenderIv.setVisibility(VISIBLE);
        }
    }


    private void setProfilePicture(ResultItemModel resultItemModel) {
        if (resultItemModel.getUser() != null && (isListingOfOfferType(resultItemModel.getListing()) || resultItemModel.getUser().getId().equals(meManager.getMe().getId()))) {
            ImageHelper.loadBorderedCircledImageInto(getContext(), resultItemModel.getUser().getTitleImage(),
                    mProfileImageView);
            mProfileImageView.setVisibility(VISIBLE);
        }
    }


    private void setCurrency(ResultItemModel resultItemModel) {
        if (resultItemModel.getListing().getRates() != null
                && resultItemModel.getListing().getRates().getRequestedCurrency() != null) {
            // Set currency
            String currencyCode = resultItemModel.getListing().getRates().getRequestedCurrency();
            setCurrency(currencyCode);
        }
    }


    private void setPrice(ResultItemModel resultItemModel) {
        if (resultItemModel.getListing().getRates() != null
                && resultItemModel.getListing().getRates().getRequestedMonthlyRate() != null) {
            setPrice(resultItemModel.getListing().getRates().getRequestedMonthlyRate());
        }
    }


    private void setLocation(ResultItemModel resultItemModel) {
        if (resultItemModel.getListing() != null && resultItemModel.getListing().getGeoLocation() != null
                && resultItemModel.getListing().getGeoLocation().getFullAddress() != null) {
            mLocationText.setText(resultItemModel.getListing().getGeoLocation().getGoogleFormattedAddress());
            mLocationText.setVisibility(VISIBLE);
        }
    }


    private void setSummary(ResultItemModel resultItemModel) {
        if (resultItemModel.getListing() != null) {
            String headline = resultItemModel.getListing().getHeadline();

            if (headline != null && !headline.isEmpty()) {
                mSummaryText.setText(headline);
                mSummaryText.setVisibility(VISIBLE);
            }
        }
    }


    private void setDescription(ResultItemModel resultItemModel) {
        if (resultItemModel.getListing() != null) {
            String description = resultItemModel.getListing().getDescription();

            if (description != null && !description.isEmpty()) {
                mDescriptionText.setText(description);
                mDescriptionText.setVisibility(View.VISIBLE);
            }
        }
    }


    private void setSeeUserProfile(final ResultItemModel resultItemModel) {
        if (resultItemModel.getUser() != null) {
            ImageHelper.loadBorderedCircledImageInto(getContext(), resultItemModel.getUser().getTitleImage(),
                    mBottomProfileImageView);

            mViewUserProfileTitle.setText(
                    String.format(getContext().getString(R.string.messages_action_view_profile_format),
                            resultItemModel.getUser().getFirstName()));
        }
    }


    private void setListingInfo(ResultItemModel resultItemModel) {
        if (resultItemModel.getListing() != null) {
            CatalogModel serviceTypeModel = mCatalogsManager
                    .getServiceTypeById(resultItemModel.getListing().getServiceType());
            if (serviceTypeModel != null) {
                switch (serviceTypeModel.getName()) {
                    case ListingType.NEED_ROOM:
                        setLifestyle(resultItemModel.getListing().getShareDetails(), false);
                        setRoommatePreferences(resultItemModel.getListing().getShareDetails());
                        mListingTypeTextView.setText(getContext().getString(R.string.al_looking_for_room));
                        break;
                    case ListingType.NEED_APARTMENT:
                        setApartmentPreferences(resultItemModel.getListing().getCalendar(), resultItemModel.getListing().getNeedApartment());
                        setApartmentAmenities(resultItemModel.getListing().getApartmentAmenities());
                        mListingTypeTextView.setText(getContext().getString(R.string.al_looking_for_apartment));
                        break;
                    case ListingType.HAVE_SHARE:
                        if (resultItemModel.getListing().getShareDetails() != null) {
                            setResidenceInfo(resultItemModel.getListing());
                            setRoomAmenities(resultItemModel.getListing().getShareDetails().getRoomAmenities());
                            setLifestyle(resultItemModel.getListing().getShareDetails(), true);
                            setRoommatePreferences(resultItemModel.getListing().getShareDetails());
                        }
                        mListingTypeTextView.setText(getContext().getString(R.string.al_offering_room));
                        break;
                    case ListingType.HAVE_APARTMENT:
                        setHaveApartmentResidenceInfo(resultItemModel.getListing().getCalendar(),
                                resultItemModel.getListing().getHaveApartment());
                        setApartmentAmenities(resultItemModel.getListing().getApartmentAmenities());
                        mListingTypeTextView.setText(getContext().getString(R.string.al_offering_apartment));
                        break;
                    default:
                        break;
                }
            }
        }
    }


    private void setApartmentPreferences(Calendar calendar, NeedApartment needApartment) {
        if (needApartment != null) {
            boolean isApartmentPrefsVisible = setApartmentPrefsTypes(needApartment.getApartmentTypeIds());
            isApartmentPrefsVisible =
                    setResidenceFurnished(needApartment.getIsFurnished()) || isApartmentPrefsVisible;

           // show Bedrooms field
           // setNeedApartmentResidenceInfo(needApartment);

            if (calendar != null) {
                isApartmentPrefsVisible = setResidenceAvailabilityDate(calendar);
                isApartmentPrefsVisible = setResidenceMinimumStay(calendar) || isApartmentPrefsVisible;
            }

            if (isApartmentPrefsVisible) {
//                mApartmentPreferencesTitle.setVisibility(View.VISIBLE);
//                mApartmentPreferencesItems.setVisibility(View.VISIBLE);
                mResidenceItemsLayout.setVisibility(View.VISIBLE);
                mResidenceTitle.setVisibility(View.VISIBLE);
            }
        }
    }


    private boolean setApartmentPrefsFurnished(Boolean isFurnished) {
        if (isFurnished != null) {
            if (isFurnished) {
                mApartmentPreferencesFurnished.setText(R.string.button_yes);
            } else {
                mApartmentPreferencesFurnished.setText(R.string.button_no);
            }
            mApartmentPreferencesFurnishedContainer.setVisibility(View.VISIBLE);
            return true;
        } else {
            return false;
        }
    }


    private boolean setApartmentPrefsTypes(List<Integer> apartmentTypeIds) {
        if (apartmentTypeIds != null) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < apartmentTypeIds.size(); i++) {
                Integer apartmentTypeId = apartmentTypeIds.get(i);
                if (apartmentTypeId != null) {
                    CatalogModel apartmentTypeModel = mCatalogsManager.getApartmentTypeById(apartmentTypeId);
                    if (apartmentTypeModel != null) {
                        sb.append(apartmentTypeModel.getName());
                        if (i != apartmentTypeIds.size() - 1) {
                            sb.append(", ");
                            if ((i + 1) % 2 == 0) {
                                sb.append('\n');
                            }
                        }
                    }
                }
            }

            if (sb.length() != 0) {
                mApartmentPreferencesType.setText(sb.toString());
                mApartmentPreferencesTypeContainer.setVisibility(View.VISIBLE);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    private void setLifestyle(ShareDetails shareDetails, boolean isShareRoom) {
        if (shareDetails != null) {
            boolean isLifestyleVisible = false;

            if (isShareRoom) {
                isLifestyleVisible = setHouseholdAge(shareDetails.getHouseholdAgeMin(),
                        shareDetails.getHouseholdAgeMax());
            }

            isLifestyleVisible = setCleanliness(shareDetails.getCleanliness()) || isLifestyleVisible;
            isLifestyleVisible = setOvernightGuests(shareDetails.getOvernightGuests()) || isLifestyleVisible;
            isLifestyleVisible = setPartyHabits(shareDetails.getPartyHabits()) || isLifestyleVisible;
            isLifestyleVisible = setBedTime(shareDetails.getGetUp(), shareDetails.getGoToBed()) || isLifestyleVisible;
            isLifestyleVisible = setFoodPreference(shareDetails.getFoodPreference()) || isLifestyleVisible;
            isLifestyleVisible = setSmokingHabits(shareDetails.getMySmokingHabits()) || isLifestyleVisible;
            isLifestyleVisible = setWorkSchedule(shareDetails.getWorkSchedule()) || isLifestyleVisible;
            isLifestyleVisible = setOccupation(shareDetails.getOccupation()) || isLifestyleVisible;
            isLifestyleVisible = setPetsOwner(shareDetails.getMyPets()) || isLifestyleVisible;
            isLifestyleVisible = setPeopleInHousehold(shareDetails.getPeopleInHousehold()) || isLifestyleVisible;

            if (isLifestyleVisible) {
                mLifestyleItemsLayout.setVisibility(View.VISIBLE);
                mLifestyleTitle.setVisibility(View.VISIBLE);
            }
        }
    }


    private boolean setPetsOwner(List<Long> myPets) {
        if (myPets != null) {
            List<CatalogValueModel> catalog = mCatalogsManager.getPetsOwnedList();
            List<CatalogValueModel> chosenPets = new ArrayList<>();
            for (int i = 0; i < myPets.size(); i++) {
                Long petId = myPets.get(i);
                if (petId != null) {
                    CatalogValueModel petModel = mCatalogsManager.getCatalogValueModelModelByValue(catalog, petId);
                    if (petModel != null) {
                        chosenPets.add(petModel);
                    }
                }
            }
            chosenPets = sortPetsAlphabetically(chosenPets);

            if (!chosenPets.isEmpty()) {
                mPetsTableLayout.addContent(chosenPets);
                mLifestylePetsOwnerContainer.setVisibility(View.VISIBLE);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    private boolean setOccupation(Long occupation) {
        if (occupation != null) {
            CatalogModel occupationModel = mCatalogsManager
                    .getCatalogModeltModelById(mCatalogsManager.getOccupationList(), occupation.intValue());
            if (occupationModel != null) {
                mLifestyleOccupation.setText(occupationModel.getName());
                mLifestyleOccupationContainer.setVisibility(View.VISIBLE);
                return true;
            }
        }
        return false;
    }


    private boolean setPartyHabits(Integer partyHabits) {
        if (partyHabits != null) {
            CatalogModel partyHabitsModel = mCatalogsManager
                    .getCatalogModeltModelById(mCatalogsManager.getPartyHabitsList(), partyHabits);
            if (partyHabitsModel != null) {
                mLifestylePartyHabits.setText(partyHabitsModel.getName());
                mLifestylePartyHabitsContainer.setVisibility(View.VISIBLE);
                return true;
            }
        }
        return false;
    }


    private boolean setWorkSchedule(Long workSchedule) {
        if (workSchedule != null) {
            CatalogModel workScheduleModel = mCatalogsManager
                    .getCatalogModeltModelById(mCatalogsManager.getWorkScheduleList(), workSchedule.intValue());
            if (workScheduleModel != null) {
                mLifestyleWorkSchedule.setText(workScheduleModel.getName());
                mLifestyleWorkScheduleContainer.setVisibility(View.VISIBLE);
                return true;
            }
        }
        return false;
    }


    private boolean setOvernightGuests(Integer overnightGuests) {
        if (overnightGuests != null) {
            CatalogModel overnightGuestsModel = mCatalogsManager
                    .getCatalogModeltModelById(mCatalogsManager.getOvernightGuestsList(), overnightGuests);
            if (overnightGuestsModel != null) {
                mLifestyleOvernightGuests.setText(overnightGuestsModel.getName());
                mLifestyleOvernightGuestsContainer.setVisibility(View.VISIBLE);
                return true;
            }
        }
        return false;
    }


    private boolean setFoodPreference(Integer foodPreference) {
        if (foodPreference != null) {
            CatalogModel foodPrefModel = mCatalogsManager
                    .getCatalogModeltModelById(mCatalogsManager.getFoodPreferencesList(), foodPreference);
            if (foodPrefModel != null) {
                mLifestyleFoodPreference.setText(foodPrefModel.getName());
                mLifestyleFoodPreferenceContainer.setVisibility(View.VISIBLE);
                return true;
            }
        }
        return false;
    }


    private boolean setCleanliness(Integer cleanliness) {
        if (cleanliness != null) {
            CatalogModel cleanlinessModel = mCatalogsManager
                    .getCatalogModeltModelById(mCatalogsManager.getCleanlinessList(), cleanliness);
            if (cleanlinessModel != null) {
                mLifestyleCleanliness.setText(cleanlinessModel.getName());
                mLifestyleCleanlinessContainer.setVisibility(View.VISIBLE);
                return true;
            }
        }
        return false;
    }


    private boolean setBedTime(Integer getUp, Integer goToBed) {
        if (getUp != null && goToBed != null) {
            CatalogModel getUpModel = mCatalogsManager.getCatalogModeltModelById(mCatalogsManager.getGetUpList(), getUp);
            CatalogModel goToBedModel = mCatalogsManager.getCatalogModeltModelById(mCatalogsManager.getGoToBedList(), goToBed);
            if (goToBedModel != null && getUpModel != null) {
                mLifestyleBedTime.setText(getUpModel.getName() + " / " + goToBedModel.getName());
                mLifestyleBedTimeContainer.setVisibility(View.VISIBLE);
                return true;
            }
        }

        return false;
    }


    private boolean setPeopleInHousehold(Integer peopleInHousehold) {
        if (peopleInHousehold != null) {
            mLifestylePeopleInHousehold.setText(String.valueOf(peopleInHousehold));
            mLifestylePeopleInHouseholdContainer
                    .setVisibility(View.VISIBLE);
            return true;
        } else {
            return false;
        }
    }


    private boolean setSmokingHabits(Long mySmokingHabits) {
        if (mySmokingHabits != null) {
            CatalogModel smokingHabitsModel = mCatalogsManager
                    .getCatalogModeltModelById(mCatalogsManager.getSmokingHabitsList(), mySmokingHabits.intValue());
            if (smokingHabitsModel != null) {
                mLifestyleSmoker.setText(smokingHabitsModel.getName());
                mLifestyleSmokerContainer.setVisibility(View.VISIBLE);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    private boolean setHouseholdAge(Integer householdAgeMin, Integer householdAgeMax) {
        if (householdAgeMin != null && householdAgeMax != null) {
            mLifestyleHouseholdAge.setText(householdAgeMin + " - " + householdAgeMax);
            mLifestyleHouseholdAgeContainer.setVisibility(View.VISIBLE);
            return true;
        } else {
            return false;
        }
    }


    private void setRoommatePreferences(ShareDetails shareDetails) {
        if (shareDetails != null) {
            boolean isRoommatePrefsVisible = setRoommateAgePref(shareDetails.getAgePreferenceMin(),
                    shareDetails.getAgePreferenceMax());

            isRoommatePrefsVisible =
                    setRoommateSmokingPref(shareDetails.getSmokingPreference()) || isRoommatePrefsVisible;
            isRoommatePrefsVisible = setRoommatePetsPref(shareDetails.getPetsPreference()) || isRoommatePrefsVisible;
            isRoommatePrefsVisible = setRoommateStudentPref(shareDetails.getStudentsOnly()) || isRoommatePrefsVisible;

            if (isRoommatePrefsVisible) {
                mRoommateItemsLayout.setVisibility(View.VISIBLE);
                mRoommatePrefsTitle.setVisibility(View.VISIBLE);
            }
        }
    }


    private boolean setRoommateStudentPref(Boolean studentsOnly) {
        if (studentsOnly != null) {
            if (studentsOnly) {
                mRoommateStudentsOnly.setText(R.string.button_yes);
            } else {
                mRoommateStudentsOnly.setText(R.string.button_no);
            }
            mRoommateStudentsOnlyContainer.setVisibility(View.VISIBLE);
            return true;
        } else {
            return false;
        }
    }


    private boolean setRoommatePetsPref(List<Long> petsPreference) {
        if (petsPreference != null) {
            List<CatalogValueModel> catalog = mCatalogsManager.getPetsPreferredList();
            List<CatalogValueModel> chosenPets = new ArrayList<>();
            for (int i = 0; i < petsPreference.size(); i++) {
                Long petId = petsPreference.get(i);
                if (petId != null) {
                    CatalogValueModel petModel = mCatalogsManager.getCatalogValueModelModelByValue(catalog, petId);
                    if(petModel != null) {
                        chosenPets.add(petModel);
                    }
                }
            }
            chosenPets = sortPetsAlphabetically(chosenPets);

            if (!chosenPets.isEmpty()) {
                mRoommatePetsTableLayout.addContent(chosenPets);
                mRoommatePetsContainer.setVisibility(View.VISIBLE);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private List<CatalogValueModel> sortPetsAlphabetically(List<CatalogValueModel> chosenPets) {
        Collections.sort(chosenPets, new Comparator<CatalogValueModel>() {
            @Override
            public int compare(CatalogValueModel lhs, CatalogValueModel rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });
        return chosenPets;
    }

    private boolean setRoommateSmokingPref(Long smokingId) {
        if (smokingId != null) {
            CatalogModel smokingModel = mCatalogsManager
                    .getCatalogModeltModelById(mCatalogsManager.getSmokingPreferencesList(), smokingId.intValue());
            if (smokingModel != null) {
                mRoommateSmoking.setText(smokingModel.getName());
                mRoommateSmokingContainer.setVisibility(View.VISIBLE);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    private boolean setRoommateAgePref(Integer minAge, Integer maxAge) {
        if (minAge != null && maxAge != null) {
            mRoommateAge.setText(minAge + " - " + maxAge);
            mRoommateAgeContainer.setVisibility(View.VISIBLE);
            return true;
        } else {
            return false;
        }
    }


    private void setResidenceInfo(ListingGetViewModel viewModel) {
        ShareDetails shareDetails = viewModel.getShareDetails();
        if (shareDetails != null) {
            boolean isResidenceVisible = setResidenceBuildingType(shareDetails.getBuildingType());
            isResidenceVisible = setResidenceMinimumStay(viewModel.getCalendar()) || isResidenceVisible;
            isResidenceVisible = setResidenceBuildingType(shareDetails.getBuildingType()) || isResidenceVisible;
            isResidenceVisible = setResidenceParkingRent(shareDetails.getParkingRent()) || isResidenceVisible;
            isResidenceVisible = setResidenceMoveInFee(shareDetails.getMoveinFee()) || isResidenceVisible;
            isResidenceVisible = setResidenceFurnished(shareDetails.getIsFurnished()) || isResidenceVisible;
            isResidenceVisible = setResidenceUtilitiesCost(shareDetails.getUtilitiesCost()) || isResidenceVisible;

            if (isResidenceVisible) {
                mResidenceItemsLayout.setVisibility(View.VISIBLE);
                mResidenceTitle.setVisibility(View.VISIBLE);
            }
        }
    }


    private void setHaveApartmentResidenceInfo(Calendar calendar, HaveApartment haveApartment) {
        boolean isResidenceVisible = false;
        if (calendar != null) {
            isResidenceVisible = setResidenceAvailabilityDate(calendar);
            isResidenceVisible = setResidenceMinimumStay(calendar) || isResidenceVisible;
        }

        if (haveApartment != null) {
            isResidenceVisible = setResidenceFurnished(haveApartment.getIsFurnished()) || isResidenceVisible;
            isResidenceVisible = setResidenceBathrooms(haveApartment.getBathroomsId()) || isResidenceVisible;
            isResidenceVisible = setResidenceBedrooms(haveApartment.getBedroomsId()) || isResidenceVisible;
            isResidenceVisible = setResidenceApartmentSize(haveApartment.getAptSize(), haveApartment.getAptSizeUnits())
                    || isResidenceVisible;
        }

        if (isResidenceVisible) {
            mResidenceItemsLayout.setVisibility(View.VISIBLE);
            mResidenceTitle.setVisibility(View.VISIBLE);
        }
    }


    private void setNeedApartmentResidenceInfo(NeedApartment needApartment) {
        if (needApartment != null && needApartment.getApartmentTypeIds() != null) {
            setResidenceBedrooms(needApartment.getApartmentTypeIds().get(0));
        }
    }


    private boolean setResidenceUtilitiesCost(Integer utilitiesCost) {
        if (utilitiesCost != null) {
            mResidenceUtilitiesCost.setText(String.valueOf(utilitiesCost));
            mResidenceUtilitiesCostContainer.setVisibility(View.VISIBLE);
            return true;
        } else {
            return false;
        }
    }


    private boolean setResidenceMoveInFee(Integer moveInFee) {
        if (moveInFee != null) {
            mResidenceMoveInFee.setText(String.valueOf(moveInFee));
            mResidenceMoveInFeeContainer.setVisibility(View.VISIBLE);
            return true;
        } else {
            return false;
        }
    }


    private boolean setResidenceParkingRent(Integer parkingRent) {
        if (parkingRent != null) {
            mResidenceParkingRent.setText(String.valueOf(parkingRent));
            mResidenceParkingRentContainer.setVisibility(View.VISIBLE);
            return true;
        } else {
            return false;
        }
    }


    private boolean setResidenceBuildingType(Long buildingType) {
        if (buildingType != null) {
            CatalogModel buildingTypeModel = mCatalogsManager
                    .getCatalogModeltModelById(mCatalogsManager.getBuildingTypesList(), buildingType.intValue());
            if (buildingTypeModel != null) {
                mResidenceBuildingType.setText(buildingTypeModel.getName());
                mResidenceBuildingTypeContainer.setVisibility(View.VISIBLE);
                return true;
            }
        }

        return false;
    }


    private boolean setResidenceAvailabilityDate(Calendar calendar) {
        if (calendar != null && calendar.getDateIn() != null) {
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            String date = df.format(calendar.getDateIn());
            mResidenceAvailabilityDate.setText(date);
            mResidenceAvailabilityDateContainer.setVisibility(View.VISIBLE);
            return true;
        } else {
            return false;
        }
    }


    private boolean setResidenceMinimumStay(Calendar calendar) {
        if (calendar != null && calendar.getMinimumStayId() != null) {
            CatalogModel minimumStay = mCatalogsManager.getMinimumStayById(calendar.getMinimumStayId());
            if (minimumStay != null) {
                mResidenceMinimumStay.setText(minimumStay.getName());
                mResidenceMinimumStayContainer.setVisibility(View.VISIBLE);
                return true;
            }
        }

        return false;
    }


    private boolean setResidenceFurnished(Boolean isFurnished) {
        if (isFurnished != null) {
            if (isFurnished) {
                mResidenceFurnished.setText(R.string.button_yes);
            } else {
                mResidenceFurnished.setText(R.string.button_no);
            }
            mResidenceFurnishedContainer.setVisibility(View.VISIBLE);
            return true;
        } else {
            return false;
        }
    }


    private boolean setResidenceBathrooms(Integer bathroomId) {
        if (bathroomId != null) {
            CatalogModel bathroomModel = mCatalogsManager.getBathroomById(bathroomId);
            if (bathroomModel != null) {
                mResidenceBathrooms.setText(bathroomModel.getName());
                mResidenceBathroomsContainer.setVisibility(View.VISIBLE);
                return true;
            }
        }

        return false;
    }


    private boolean setResidenceBedrooms(Integer bedroomsId) {
        if (bedroomsId != null) {
            CatalogModel bedroomModel = mCatalogsManager.getBedroomById(bedroomsId);
            if (bedroomModel != null) {
                mResidenceBedrooms.setText(bedroomModel.getName());
                mResidenceBedroomsContainer.setVisibility(View.VISIBLE);
                return true;
            }
        }

        return false;
    }


    private boolean setResidenceApartmentSize(Integer apartmentSize, Integer apartmentSizeUnitsId) {
        if (apartmentSize != null && apartmentSizeUnitsId != null) {
            CatalogModel apartmentSizeUnitsModel = mCatalogsManager.getApartmentSizeUnitById(apartmentSizeUnitsId);
            if (apartmentSizeUnitsModel != null) {
                StringBuilder sb = new StringBuilder();
                sb.append(apartmentSize).append(' ').append(apartmentSizeUnitsModel.getName());
                mResidenceApartmentSize.setText(sb.toString());
                mResidenceApartmentSizeContainer.setVisibility(View.VISIBLE);
                return true;
            }
        }
        return false;
    }


    private void setApartmentAmenities(List<Long> amenityIds) {
        setAmenities(amenityIds, mCatalogsManager.getApartmentAmenitiesList(), false);
    }


    private void setRoomAmenities(List<Long> amenityIds) {
        setAmenities(amenityIds, mCatalogsManager.getRoomAmenitiesList(), true);
    }


    private void setAmenities(List<? extends Number> amenitiesIds, List<CatalogValueModel> allAmenityIds, boolean isRoomAmenities) {
        if (amenitiesIds != null && allAmenityIds != null) {
            List<CatalogValueModel> amenityModels = new ArrayList<>();

            for (Number amenityValue : amenitiesIds) {
                CatalogValueModel amenityModel;
                if(isRoomAmenities) {
                    amenityModel = mCatalogsManager.getCatalogValueModelModelByValue(mCatalogsManager.getRoomAmenitiesList(), amenityValue.longValue());
                } else {
                    amenityModel = mCatalogsManager.getCatalogValueModelModelById(allAmenityIds, amenityValue.longValue());
                }
                if (amenityModel != null) {
                    amenityModels.add(amenityModel);
                }
            }

            if (!amenityModels.isEmpty()) {
                AmenitiesAdapter amenitiesAdapter = new AmenitiesAdapter(getContext(), amenityModels, null);
                mAmenitiesGrid.setAdapter(amenitiesAdapter);
                measureLayout(amenityModels.size(), mAmenitiesGrid);
                mAmenitiesTitle.setVisibility(View.VISIBLE);
                mAmenitiesGridLayout.setVisibility(View.VISIBLE);
            }
        }
    }


    private void measureLayout(int itemsSize, GridView layout) {
        ViewGroup.LayoutParams params = layout.getLayoutParams();
        int amenityHeight = getResources().getDimensionPixelOffset(R.dimen.amenities_layout_height);
        int margin = getResources().getDimensionPixelSize(R.dimen.offset_4);
        int verticalSpacing = getResources().getDimensionPixelOffset(R.dimen.offset_2);
        int height = (amenityHeight + verticalSpacing) * ((int) Math.ceil(((double) itemsSize / (AMENITIES_COLUMNS))));
        params.height = height + margin;
        layout.setLayoutParams(params);
    }


    private boolean isListingOfOfferType(ListingGetViewModel listingGetViewModel) {
        CatalogModel serviceType = mCatalogsManager.getServiceTypeById(listingGetViewModel.getServiceType());
        if (serviceType != null && serviceType.getName() != null) {
            String name = serviceType.getName();
            return name.equals(ListingType.HAVE_APARTMENT) || name.equals(ListingType.HAVE_SHARE);
        }

        return false;
    }
}