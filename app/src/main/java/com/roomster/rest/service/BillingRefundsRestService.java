package com.roomster.rest.service;

import com.roomster.application.RoomsterApplication;
import com.roomster.enums.BillingTypeEnum;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.billing.BillingEvent;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.TokenManager;
import com.roomster.rest.api.BillingApi;
import com.roomster.rest.model.BillingRespModel;
import com.roomster.rest.model.DisputeModel;
import com.roomster.rest.model.RefundModel;
import com.roomster.utils.DateUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.Date;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yuliasokolova on 25.10.16.
 */
public class BillingRefundsRestService {
    @Inject
    EventBus mEventBus;

    @Inject
    NetworkManager mNetworkManager;

    @Inject
    TokenManager mTokenManager;

    @Inject
    BillingApi mBillingApi;


    public BillingRefundsRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }

    public void getBillingInfo (final BillingTypeEnum type){
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callGetEligible(result, type);
            }

            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                mEventBus.post(new BillingEvent.EligibleGetFail());
            }
        });

    }

    private void callGetEligible(String authToken, final BillingTypeEnum type) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<BillingRespModel> callBillingInfo;
            if (type == BillingTypeEnum.GET)
                 callBillingInfo = mBillingApi.getEligible(authToken);
            else
                 callBillingInfo = mBillingApi.postEligible(authToken);
            callBillingInfo.enqueue(new Callback<BillingRespModel>() {
                @Override
                public void onResponse(Call<BillingRespModel> call, Response<BillingRespModel> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            postSuccessBilling(response.body(), type);
                            break;
                        default:
                            mEventBus.post(new BillingEvent.EligibleGetFail());
                    }
                }


                @Override
                public void onFailure(Call<BillingRespModel> call, Throwable t) {
                    mEventBus.post(new BillingEvent.EligibleGetFail());
                }
            });
        } else {
            mEventBus.post(new BillingEvent.EligibleGetFail());
        }
    }


    private void postSuccessBilling(BillingRespModel value, BillingTypeEnum type) {
        BillingEvent.EligibleGetSuccess event = new BillingEvent.EligibleGetSuccess();
        event.type = type;

        event.eligible = value.isEligible();
        if (!event.eligible){
            event.refund = value.getRefund();
            event.dispute = value.getDispute();
            event.message = value.getMessage();
        }

        //test

//        event.eligible =  false;
//        event.refund = new RefundModel();
//        Date testDate =  DateUtil.parseDateBilling("2016-10-26T07:24:35.607Z");
//        event.refund.setDateAdded(testDate);
       //// event.dispute = new DisputeModel();

        mEventBus.post(event);
    }

}
