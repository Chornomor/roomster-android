package com.roomster.views.undo;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by andreybofanov on 06.07.16.
 */
public class UndoScnackbar extends Snackbar.Callback implements View.OnClickListener {
    public static final String KEY_DELTED_ITEM = "delted_item";
    private String mMessage;
    private SnackBarEvent mCallback;
    private ViewGroup mLayout;
    private boolean mUndoClicked = false;

    public UndoScnackbar
            (@NonNull ViewGroup layout, String message, @NonNull SnackBarEvent callback)
    {
        this.mMessage = message;
        this.mCallback = callback;
        this.mLayout = layout;
    }

    public void make(){
        Snackbar snackbar = Snackbar.make(mLayout,mMessage, Snackbar.LENGTH_SHORT);
        snackbar.setAction("UNDO",this);
        snackbar.setCallback(this);
        snackbar.getView().setBackgroundColor(Color.WHITE);
        TextView textView = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.BLACK);
        snackbar.setActionTextColor(Color.GREEN);
        snackbar.show();
    }

    @Override
    public void onDismissed(Snackbar snackbar, int event) {
        super.onDismissed(snackbar, event);
        if(!mUndoClicked)
            mCallback.onDismiss();
    }

    @Override
    public void onClick(View v) {
        mUndoClicked = true;
        mCallback.onUndo();
    }


    public interface SnackBarEvent {
        void onUndo();
        void onDismiss();
    }
}
