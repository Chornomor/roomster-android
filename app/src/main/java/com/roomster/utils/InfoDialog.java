package com.roomster.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.roomster.R;

/**
 * Created by eugenetroyanskii on 05.08.16.
 */
public class InfoDialog extends AlertDialog{

    Context context;

    public InfoDialog(Context context) {
        super(context);
        this.context = context;
    }

    public void createInfoDialog(String title, String message) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton(context.getString(R.string.button_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
    }
}
