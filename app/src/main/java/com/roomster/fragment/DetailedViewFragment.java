package com.roomster.fragment;


import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.telephony.PhoneNumberUtils;
import android.view.*;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.roomster.R;
import com.roomster.activity.UserProfileActivity;
import com.roomster.application.RoomsterApplication;
import com.roomster.dialog.ReportUserDialog;
import com.roomster.dialog.SendMessageDialog;
import com.roomster.dialog.SocialConnectionsDialog;
import com.roomster.event.RestServiceEvents.BookmarksRestServiceEvent;
import com.roomster.event.RestServiceEvents.ConversationRestServiceEvent;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.manager.MeManager;
import com.roomster.rest.model.*;
import com.roomster.rest.service.BookmarksRestService;
import com.roomster.rest.service.ConversationsRestService;
import com.roomster.rest.service.UsersRestService;
import com.roomster.views.DetailedView;
import com.roomster.views.toolbar.RRToolbar;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;
import java.util.List;


/**
 * Created by "Michael Katkov" on 11/18/2015.
 */
public class DetailedViewFragment extends Fragment {

    public static final String LISTING = "listing";
    public static final String SOCIAL_VISIBLE = "social_Visible";
    protected ResultItemModel mListing;
    private   Menu            mActionMenu;
    private   String          mSmsBody;
    private   String          mLinkMyProfile = " @ https://www.roomster.com/profile/";

    @Bind(R.id.detailed_view)
    DetailedView mDetailedView;

    @Bind(R.id.detailed_view_toolbar)
    RRToolbar mBottomToolbar;

//    @Bind(R.id.detailed_view_toolbar_menu)
//    ActionMenuView mToolbarMenu;

    @Bind(R.id.detailed_view_toolbar_container)
    LinearLayout mToolbarContainer;

    @Inject
    EventBus mEventBus;

    @Inject
    ConversationsRestService mConversationsRestService;

    @Inject
    UsersRestService mUsersRestService;

    @Inject
    BookmarksRestService mBookmarksService;

    @Inject
    MeManager meManager;

    public static DetailedViewFragment newInstance(ResultItemModel item) {
        DetailedViewFragment fragment = new DetailedViewFragment();

        //Save the listing item so to be restored on fragment recreation
        Bundle args = new Bundle();
        args.putSerializable(LISTING, item);

        fragment.setArguments(args);
        return fragment;
    }
    public static DetailedViewFragment newInstance(ResultItemModel item, boolean visible) {
        DetailedViewFragment fragment = new DetailedViewFragment();
        //Save the listing item so to be restored on fragment recreation
        Bundle args = new Bundle();
        args.putSerializable(LISTING, item);
        args.putBoolean(SOCIAL_VISIBLE,visible);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detailed_view, container, false);
        ButterKnife.bind(this, view);

        recoverState();

        setUpDetailedView();
        boolean toolbarVisible = true;
        if(getArguments() != null) {
            toolbarVisible = getArguments().getBoolean(SOCIAL_VISIBLE, true);
        }
        initToolbar(toolbarVisible);
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        mEventBus.register(this);
    }


    @Override
    public void onPause() {
        super.onPause();
        mEventBus.unregister(this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(BookmarksRestServiceEvent.AddBookmarkSuccess event) {
        if (event.listingId == mListing.getListing().getListingId()) {
            updateBookmark(true);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(BookmarksRestServiceEvent.AddBookmarkFailure event) {
        if (event.listingId == mListing.getListing().getListingId()) {
            Toast.makeText(getContext(), getString(R.string.error_add_bookmark), Toast.LENGTH_SHORT).show();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(BookmarksRestServiceEvent.RemoveBookmarkSuccess event) {
        if (event.listingId == mListing.getListing().getListingId()) {
            updateBookmark(false);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(BookmarksRestServiceEvent.RemoveBookmarkFailure event) {
        if (event.listingId == mListing.getListing().getListingId()) {
            Toast.makeText(getContext(), getString(R.string.error_delete_bookmark), Toast.LENGTH_SHORT).show();
        }
    }


    public void updateBookmark(boolean isBookmarked) {
        if (mListing != null) {
            mBottomToolbar.updateBookmarkedListing(isBookmarked);
            mListing.getListing().setIsBookmarked(isBookmarked);
            mDetailedView.updateBookmarkView(mListing);
        }
    }


    protected void setUpDetailedView() {
        if (mListing != null) {
            if(mListing.getUser().getId().equals(meManager.getMe().getId())){
                setupIfMeListing();
            }
            mDetailedView.populateViews(getChildFragmentManager(), mListing);
            mDetailedView.setOnSeeProfileClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    UserGetViewModel user = mListing.getUser();

                    if (user != null) {
                        Long userId = user.getId();
                        Intent intent = new Intent(getActivity(), UserProfileActivity.class);
                        intent.putExtra(UserProfileActivity.USER_ID, userId);
                        intent.putExtra(UserProfileActivity.LISTING_ID, mListing.getListing().getListingId());
                        try {
                            startActivity(intent);
                        } catch (ActivityNotFoundException e) {
                            //Nothing to do
                        }
                    }
                }
            });
        }

        mDetailedView.setOnReportClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showReportUserDialog();
            }
        });
        mDetailedView.setOnBlockClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showBlockUserDialog();
            }
        });
        mDetailedView.requestLayout();
    }


    private void recoverState() {
        Bundle args = getArguments();
        if (args != null) {
            mListing = (ResultItemModel) args.getSerializable(LISTING);
        }
    }


    private void updateActionMenu(boolean isBookmarked) {
        MenuItem bookmarkMenuItem = mActionMenu.findItem(R.id.contact_action_bookmark);
        if (isBookmarked) {
            bookmarkMenuItem.setTitle(getString(R.string.detailed_view_toolbar_action_unbookmark));
        } else {
            bookmarkMenuItem.setTitle(getString(R.string.detailed_view_toolbar_action_bookmark));
        }
    }


    private void initToolbar(boolean toolbarVisible) {
//        mBottomToolbar.setTitle(null);
//        mToolbarMenu.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
//
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                onToolbarItemClick(item);
//                return true;
//            }
//        });
//
//        MenuInflater inflater = getActivity().getMenuInflater();
//        mActionMenu = mToolbarMenu.getMenu();
//
//
//        inflater.inflate(R.menu.contact, mActionMenu);
//        onCreateToolbarMenu(mActionMenu);
//        mToolbarMenu.invalidate();
        mBottomToolbar.setUpFromListing(mListing);
        mBottomToolbar.setVisible(toolbarVisible);
        mBottomToolbar.setListener(new RRToolbar.OnActionListener() {
            @Override
            public void onRRToolbarActionClick(int id) {
                onToolbarItemClick(id);
            }
        });

        mDetailedView.setPanelSlidingListener(new ToolbarSlider());
    }


    public void setupIfMeListing(){
        mBottomToolbar.setVisibility(View.GONE);
        mBottomToolbar.setVisible(false);
        mDetailedView.setMyListing(true);
    }

    protected void onCreateToolbarMenu(Menu menu) {
        if (mListing != null) {
            UserGetViewModel user = mListing.getUser();
            String phone = user.getPhone();

            boolean hasPhoneNumber = phone != null && !phone.isEmpty();
            boolean hasSocialConnections =
                    user.getSocialConnections() != null && !user.getSocialConnections().isEmpty();

            MenuItem actionCall = menu.findItem(R.id.contact_action_call);
            if (actionCall != null) {
                actionCall.setVisible(hasPhoneNumber);
            }

            MenuItem actionSms = menu.findItem(R.id.contact_action_sms);
            if (actionSms != null) {
                actionSms.setVisible(hasPhoneNumber);
            }

            MenuItem actionSocial = menu.findItem(R.id.contact_action_social);
            if (actionSocial != null) {
                actionSocial.setVisible(hasSocialConnections);
            }

            if (mListing.getListing() != null) {
                updateActionMenu(mListing.getListing().getIsBookmarked());
            }
        }
    }


    private void onToolbarItemClick(int id) {
        if (mListing != null) {
            String phone = mListing.getUser().getPhone();
            switch (id) {
            case R.id.contact_action_message:
                showMessageDialog();
                break;
            case R.id.contact_action_call:
                sendPhoneIntent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone), null);
                break;
            case R.id.contact_action_sms:
                mLinkMyProfile = mLinkMyProfile.concat(String.valueOf(meManager.getMe().getId()));
                mSmsBody = String.format(getString(R.string.send_sms_body), mListing.getUser().getFirstName()) + mLinkMyProfile;
                sendPhoneIntent(Intent.ACTION_VIEW, Uri.fromParts("sms", phone, null), mSmsBody);
                break;
            case R.id.contact_action_report:
                showReportUserDialog();
                break;
            case R.id.contact_action_social:
                showSocialConnectionsDialog();
                break;
            case R.id.contact_action_bookmark:
                mBookmarksService.toggleBookmark(mListing);
                break;
            case R.id.contact_action_share:
                sendShareListingIntent();
                break;
            }
        }
    }


    private void sendShareListingIntent() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, mListing.getListing().getUrl());
        try {
            startActivity(Intent.createChooser(shareIntent, null));
        } catch (ActivityNotFoundException e) {
            //Nothing to do
        }
    }


    private void sendPhoneIntent(String intentAction, Uri uri, String body) {
        String phone = mListing.getUser().getPhone();
        if (phone != null) {
            if (PhoneNumberUtils.isGlobalPhoneNumber(phone)) {
                try {
                    Intent intent = new Intent(intentAction, uri);
                    if (body != null) {
                        intent.putExtra("sms_body", body);
                    }
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    //Nothing to do
                }
            } else
                if (!phone.isEmpty()) {
                    Toast.makeText(getContext(), phone, Toast.LENGTH_SHORT).show();
                }
        }
    }


    private void showSocialConnectionsDialog() {
        List<SocialConnectionViewModel> connections = mListing.getUser().getSocialConnections();
        new SocialConnectionsDialog(getContext(), connections, mListing.getUser().getFirstName()).show();
    }


    private void showMessageDialog() {
        final UserGetViewModel user = mListing.getUser();
        SendMessageDialog dialog = new SendMessageDialog(getContext(), user.getFirstName(),
                new SendMessageDialog.SendMessageListener() {

                    @Override
                    public void onSendMessage(String message) {
                        mConversationsRestService
                                .sendInitialMessage(user.getId(), message, mListing.getListing().getListingId(),
                                        new RestServiceListener<String>() {

                                            @Override
                                            public void onSuccess(String result) {
                                                mEventBus.post(new ConversationRestServiceEvent.ConversationUpdated());
                                                if (getContext() != null) {
                                                    Toast.makeText(getContext(),
                                                            R.string.detailed_view_send_message_success,
                                                            Toast.LENGTH_SHORT).show();
                                                }
                                            }


                                            @Override
                                            public void onFailure(int code, Throwable throwable, String msg) {
                                                if (getContext() != null) {
                                                    Toast.makeText(getContext(),
                                                            R.string.detailed_view_send_message_fail,
                                                            Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                    }
                });

        dialog.show();
    }


    private void showReportUserDialog() {
        ReportUserDialog dialog = new ReportUserDialog(getContext(), new ReportUserDialog.SubmitReportListener() {

            @Override
            public void onReportSubmitted(UserReportViewModel userReportViewModel) {
                userReportViewModel.setUserId(mListing.getUser().getId());
                mUsersRestService.reportUser(userReportViewModel, new RestServiceListener<String>() {

                    @Override
                    public void onSuccess(String result) {
                        if (getContext() != null) {
                            Toast.makeText(getContext(), getString(R.string.messages_user_reported_toast),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }


                    @Override
                    public void onFailure(int code, Throwable throwable, String msg) {
                        if (getContext() != null) {
                            Toast.makeText(getContext(), getString(R.string.messages_user_not_reported_toast),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        dialog.show();
    }


    private void showBlockUserDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(R.string.block_user_message).setTitle(R.string.block_user_title)
                .setNegativeButton(R.string.dialog_cancel_button, null)
                .setPositiveButton(R.string.block_button, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        UserBlockViewModel blockViewModel = new UserBlockViewModel();
                        blockViewModel.setBlock(true);
                        blockViewModel.setUserId(mListing.getUser().getId());
                        mUsersRestService.blockUser(blockViewModel, new RestServiceListener<String>() {

                            @Override
                            public void onSuccess(String result) {
                                if (getContext() != null) {
                                    Toast.makeText(getContext(), R.string.block_user_success, Toast.LENGTH_SHORT)
                                            .show();
                                }
                            }


                            @Override
                            public void onFailure(int code, Throwable throwable, String msg) {
                                if (getContext() != null) {
                                    Toast.makeText(getContext(), R.string.block_user_failure, Toast.LENGTH_SHORT)
                                            .show();
                                }
                            }
                        });
                    }
                });
        builder.show();
    }


    private class ToolbarSlider implements SlidingUpPanelLayout.PanelSlideListener {

        private int mToolbarHeight;


        private ToolbarSlider() {
            mToolbarHeight = (int) getActivity().getResources()
                    .getDimension(R.dimen.detailed_view_toolbar_height_with_border);
        }


        @Override
        public void onPanelSlide(View panel, float slideOffset) {
//            slideToolbar(slideOffset);
        }


        @Override
        public void onPanelCollapsed(View panel) {
//            slideToolbar(0);
            mDetailedView.setCurrentPositionSelected();
        }


        @Override
        public void onPanelExpanded(View panel) {
//            slideToolbar(1);
            mDetailedView.setInfoPositionSelected();
        }


        @Override
        public void onPanelAnchored(View panel) {
            // nothing to do
        }


        @Override
        public void onPanelHidden(View panel) {
            // nothing to do
        }


        private void slideToolbar(float offset) {
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mToolbarContainer.getLayoutParams();
            params.height = (int) (mToolbarHeight * offset);
            mToolbarContainer.setLayoutParams(params);
        }
    }

    public void setToolbarVisibility(boolean visible){
        mBottomToolbar.setVisible(visible);
    }
}
