package com.roomster.event.fragment;

import com.roomster.event.Event;

public class FragmentRequest extends Event {

    public static class SetTitleRequest {

        public String title;
        public String subtitle;

        public SetTitleRequest(String titleResId, String subTitleResId) {
            this.title = titleResId;
            this.subtitle = subTitleResId;
        }
    }
}
