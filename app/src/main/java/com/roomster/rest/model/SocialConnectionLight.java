package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import com.roomster.enums.SocialNetworkEnum;

import java.io.Serializable;


public class SocialConnectionLight implements Serializable {

    @SerializedName("SocialNetwork")
    private SocialNetworkEnum socialNetwork = null;

    @SerializedName("Url")
    private String url = null;

    @SerializedName("IsVisible")
    private Boolean isVisible = null;


    public SocialNetworkEnum getSocialNetwork() {
        return socialNetwork;
    }


    public void setSocialNetwork(SocialNetworkEnum socialNetwork) {
        this.socialNetwork = socialNetwork;
    }


    public String getUrl() {
        return url;
    }


    public void setUrl(String url) {
        this.url = url;
    }


    public Boolean getIsVisible() {
        return isVisible;
    }


    public void setIsVisible(Boolean isVisible) {
        this.isVisible = isVisible;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class SocialConnectionLight {\n");

        sb.append("  socialNetwork: ").append(socialNetwork).append("\n");
        sb.append("  url: ").append(url).append("\n");
        sb.append("  isVisible: ").append(isVisible).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}