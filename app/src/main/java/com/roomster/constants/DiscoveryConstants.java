package com.roomster.constants;


/**
 * Created by Bogoi on 4/3/2016.
 */
public class DiscoveryConstants {

    public static final double DEGREES_IN_KM_LAT  = 110.574235;
    public static final double DEGREES_IN_KM_LONG = 110.572833;
    public static final double DEGREES_DELTA      = 1000.0;
    public static final int    DEFAULT_DISTANCE   = 1000;
    public static final int    MIN_BUDGET         = 0;
    public static final int    MIN_AGE            = 18;
    public static final int    MAX_AGE            = 100;
    public static final int    MAX_RADIUS         = 50000;
    public static final int    DEFAULT_RADIUS     = 25000;
    public static final int    RADIUS_DIVIDER     = 1000;

    public static int          MAX_BUDGET         = 10000; //this value not constant anymore depends of current location and currency

    public static int prepareBudgetRangeStep(){
        if (DiscoveryConstants.MAX_BUDGET > 0 && DiscoveryConstants.MAX_BUDGET < 10_001) {
            return 10;
        }
        if (DiscoveryConstants.MAX_BUDGET > 10_000 && DiscoveryConstants.MAX_BUDGET < 100_001) {
            return 100;
        }
        if (DiscoveryConstants.MAX_BUDGET > 100_000 && DiscoveryConstants.MAX_BUDGET < 1_000_001) {
            return 1000;
        }
        if (DiscoveryConstants.MAX_BUDGET > 1_000_000 && DiscoveryConstants.MAX_BUDGET < 10_000_001) {
            return 10_000;
        }
        if (DiscoveryConstants.MAX_BUDGET > 10_000_000 && DiscoveryConstants.MAX_BUDGET < 100_000_001) {
            return 100_000;
        }
        if (DiscoveryConstants.MAX_BUDGET > 100_000_000 && DiscoveryConstants.MAX_BUDGET < 1_000_000_001) {
            return 1_000_000;
        }
        else {
            return 10_000_000;
        }
    }
}
