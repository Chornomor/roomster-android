package com.roomster.adapter;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.ListingType;
import com.roomster.fragment.ImageFragment;
import com.roomster.fragment.ListingMapFragment;
import com.roomster.manager.CatalogsManager;
import com.roomster.model.CatalogModel;
import com.roomster.rest.model.ListingGetViewModel;

import javax.inject.Inject;


/**
 * Created by michaelkatkov on 12/15/15.
 */
public class UserListingAdapter extends BasePageAdapter {

    public static final int MAP_NUMBER          = 1;
    public static final int MAP_AND_EDIT_NUMBER = 2;

    private ListingGetViewModel mListingGetItemModel;

    @Inject
    Context mContext;

    @Inject
    CatalogsManager mCatalogsManager;

    private int mSelectedIndicators[];
    private int mDeselectedIndicators[];


    public UserListingAdapter(FragmentManager fragmentManager, ListingGetViewModel resultItemModel) {
        super(fragmentManager);
        RoomsterApplication.getRoomsterComponent().inject(this);
        mListingGetItemModel = resultItemModel;
        mSelectedIndicators = new int[mListingGetItemModel.getPhotos().size() + MAP_AND_EDIT_NUMBER];
        mDeselectedIndicators = new int[mListingGetItemModel.getPhotos().size() + MAP_AND_EDIT_NUMBER];
        fillSelectedIndicators();
        fillDeselectedIndicators();
    }


    @Override
    public int getCount() {
        int photosSize = mListingGetItemModel.getPhotos().size();
        return isFindType() ? photosSize : photosSize + MAP_NUMBER;
    }


    @Override
    public Fragment getItem(int position) {
        if (position == mListingGetItemModel.getPhotos().size()) {
            double latitude = mListingGetItemModel.getGeoLocation().getLat();
            double longitude = mListingGetItemModel.getGeoLocation().getLng();
            return ListingMapFragment.newInstance(latitude, longitude);
        } else {
            return ImageFragment.newInstance(mListingGetItemModel.getPhotos().get(position).getPath());
        }
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getString(R.string.listing_title) + position;
    }


    private void fillSelectedIndicators() {
        int photosCount = mListingGetItemModel.getPhotos().size();
        for (int i = 0; i < photosCount; i++) {
            mSelectedIndicators[i] = R.drawable.ic_bubble_selected;
        }
        if (isFindType()) {
            mSelectedIndicators[photosCount] = R.drawable.ic_info_bubble_selected;
        } else {
            mSelectedIndicators[photosCount] = R.drawable.ic_map_bubble_selected;
            mSelectedIndicators[photosCount + 1] = R.drawable.ic_info_bubble_selected;
        }
    }


    private void fillDeselectedIndicators() {
        int photosCount = mListingGetItemModel.getPhotos().size();
        for (int i = 0; i < photosCount; i++) {
            mDeselectedIndicators[i] = R.drawable.ic_bubble_deselected;
        }
        if (isFindType()) {
            mDeselectedIndicators[photosCount] = R.drawable.ic_info_bubble_deselected;
        } else {
            mDeselectedIndicators[photosCount] = R.drawable.ic_map_bubble_deselected;
            mDeselectedIndicators[photosCount + 1] = R.drawable.ic_info_bubble_deselected;
        }
    }


    @Override
    public int getSelectedIndicator(int position) {
        return mSelectedIndicators[position];
    }


    @Override
    public int getDiselectedIndicator(int position) {
        return mDeselectedIndicators[position];
    }


    private boolean isFindType() {
        CatalogModel serviceType = mCatalogsManager.getServiceTypeById(mListingGetItemModel.getServiceType());
        String name = serviceType.getName();
        return name.equals(ListingType.NEED_APARTMENT) || name.equals(ListingType.NEED_ROOM);
    }
}
