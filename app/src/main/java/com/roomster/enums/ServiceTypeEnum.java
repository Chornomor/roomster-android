package com.roomster.enums;

public enum ServiceTypeEnum {
    Undefined, NeedRoom, HaveShare, NeedApartment, HaveApartment
}
