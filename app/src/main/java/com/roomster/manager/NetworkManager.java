package com.roomster.manager;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.roomster.application.RoomsterApplication;

import javax.inject.Inject;


/**
 * Created by Bogoi
 */
public class NetworkManager {

    @Inject
    Context mContext;


    public NetworkManager() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public boolean hasNetworkAccess() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in air plan mode it will be null
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }
}
