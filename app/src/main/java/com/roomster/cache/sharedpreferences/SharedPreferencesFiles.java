package com.roomster.cache.sharedpreferences;


import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;


/**
 * Provides distinguished Shared Preferences files.
 */
public class SharedPreferencesFiles {

    private static final String USER_SHARED_PREFERENCES_FILE = "user_shared_preferences";
    private SharedPreferences mAppSharedPreferences;
    private SharedPreferences mUserSharedPreferences;


    public SharedPreferencesFiles(Application app) {
        mAppSharedPreferences = app.getSharedPreferences(app.getApplicationInfo().name, Context.MODE_PRIVATE);
        mUserSharedPreferences = app.getSharedPreferences(USER_SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
    }


    public SharedPreferences getAppSharedPreferences() {
        return mAppSharedPreferences;
    }


    public SharedPreferences getUserSharedPreferences() {
        return mUserSharedPreferences;
    }
}
