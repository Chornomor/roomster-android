package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class ResultModel {

    /**
     **/
    @SerializedName("count")
    private Integer count = null;

    /**
     **/
    @SerializedName("geo")
    private GeoModel geo = null;

    /**
     **/
    @SerializedName("items")
    private List<ResultItemModel> items = new ArrayList<>();


    public Integer getCount() {
        return count;
    }


    public void setCount(Integer count) {
        this.count = count;
    }


    public GeoModel getGeo() {
        return geo;
    }


    public void setGeo(GeoModel geo) {
        this.geo = geo;
    }


    public List<ResultItemModel> getItems() {
        return items;
    }


    public void setItems(List<ResultItemModel> items) {
        this.items = items;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ResultModel {\n");

        sb.append("  count: ").append(count).append("\n");
        sb.append("  geo: ").append(geo).append("\n");
        sb.append("  items: ").append(items).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
