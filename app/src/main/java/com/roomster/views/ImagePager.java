package com.roomster.views;


import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.roomster.R;
import com.roomster.adapter.ImagePagerAdapter;
import com.roomster.rest.model.Photo;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by michaelkatkov on 1/5/16.
 */
public class ImagePager extends FrameLayout {

    private static final int GRID_SIZE        = 6;
    private static final int HIDDEN_TAB_COUNT = 1;

    public ImagePagerAdapter mAdapter;
    private int mCurrentPage = 0;

    @Bind(R.id.image_pager)
    ViewPager mViewPager;

    @Bind(R.id.image_pager_indicator_layout)
    LinearLayout mIndicatorLayout;


    public ImagePager(Context context) {
        super(context);
        initView();
    }


    public ImagePager(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }


    private void initView() {
        inflate(getContext(), R.layout.view_image_pager, this);
        ButterKnife.bind(this);
        mViewPager.setSaveEnabled(false);
    }


    public void setAdapter(FragmentManager fragmentManager, List<Photo> photos, ImageClickListener imageClickListener) {
        clearIndicatiors();
        int tabsCount = photos.size() / GRID_SIZE + 1;
        initIndicators(tabsCount, mCurrentPage);
//        checkIfNecessaryToShowIndicators(tabsCount);
        mAdapter = new ImagePagerAdapter(fragmentManager, photos, imageClickListener);
        mViewPager.setAdapter(mAdapter);
        mViewPager.addOnPageChangeListener(onPageChangeListener);
        mViewPager.setCurrentItem(mCurrentPage);
    }


    private void clearIndicatiors() {
        mIndicatorLayout.removeAllViews();
    }


    private void initIndicators(int indicatorsCount, int selectedIndicatorPosition) {
        for (int i = 0; i < indicatorsCount; i++) {
            ImageView imageView = (ImageView) LayoutInflater.from(getContext()).inflate(R.layout.image_view_indicator, null);
            if (i == selectedIndicatorPosition) {
                imageView.setImageResource(R.drawable.ic_image_bubble_selected);
            } else {
                imageView.setImageResource(R.drawable.ic_image_bubble_deselected);
            }
            mIndicatorLayout.addView(imageView);
        }
    }


    private void checkIfNecessaryToShowIndicators(int tabsCount) {
        if (tabsCount > HIDDEN_TAB_COUNT) {
            mIndicatorLayout.setVisibility(VISIBLE);
        } else {
            mIndicatorLayout.setVisibility(GONE);
        }
    }


    public void setPositionSelected(int position) {
        mCurrentPage = position;
        for (int i = 0; i < mIndicatorLayout.getChildCount(); i++) {
            ((ImageView) mIndicatorLayout.getChildAt(i)).setImageResource(R.drawable.ic_image_bubble_deselected);
        }
        ((ImageView) mIndicatorLayout.getChildAt(position)).setImageResource(R.drawable.ic_image_bubble_selected);
    }


    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }


        @Override
        public void onPageSelected(int position) {
            setPositionSelected(position);
        }


        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };


    public interface ImageClickListener {

        void onImageClick(Photo photo);
    }


    public void setIndicatorsVisibility(int visibility) {
        mIndicatorLayout.setVisibility(visibility);
    }
}
