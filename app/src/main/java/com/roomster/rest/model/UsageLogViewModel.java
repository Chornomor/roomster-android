package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import java.util.Date;


public class UsageLogViewModel {

    /**
     **/
    @SerializedName("created")
    private Date created = null;

    /**
     **/
    @SerializedName("ipv4")
    private String ipv4 = null;

    /**
     **/
    @SerializedName("type")
    private String type = null;


    public Date getCreated() {
        return created;
    }


    public void setCreated(Date created) {
        this.created = created;
    }


    public String getIpv4() {
        return ipv4;
    }


    public void setIpv4(String ipv4) {
        this.ipv4 = ipv4;
    }


    public String getType() {
        return type;
    }


    public void setType(String type) {
        this.type = type;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class UsageLogViewModel {\n");

        sb.append("  created: ").append(created).append("\n");
        sb.append("  ipv4: ").append(ipv4).append("\n");
        sb.append("  type: ").append(type).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
