package com.roomster.adapter;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.fragment.ImageFragment;
import com.roomster.fragment.ListingMapFragment;
import com.roomster.rest.model.Photo;
import com.roomster.rest.model.ResultItemModel;

import javax.inject.Inject;
import java.util.List;


/**
 * Created by michaelkatkov on 11/18/15.
 */
public class ListingPagerAdapter extends BasePageAdapter {

    public static final int MAP_NUMBER                  = 1;
    public static final int MAP_AND_EDIT_NUMBER         = 2;
    public static final int NEED_ROOM_SERVICE_TYPE      = 1;
    public static final int NEED_APARTMENT_SERVICE_TYPE = 4;

    private ResultItemModel mResultItemModel;
    private int             mSelectedIndicators[];
    private int             mDeselectedIndicators[];
    private List<Photo>     mPhotos;

    @Inject
    Context mContext;


    public ListingPagerAdapter(FragmentManager fragmentManager, ResultItemModel resultItemModel) {
        super(fragmentManager);
        RoomsterApplication.getRoomsterComponent().inject(this);
        mResultItemModel = resultItemModel;
        if (isProfileListing()) {
            mPhotos = mResultItemModel.getUser().getPhotos();
        } else {
            mPhotos = mResultItemModel.getListing().getPhotos();
        }
        mSelectedIndicators = new int[mPhotos.size() + MAP_AND_EDIT_NUMBER];
        mDeselectedIndicators = new int[mPhotos.size() + MAP_AND_EDIT_NUMBER];
        fillSelectedIndicators();
        fillDeselectedIndicators();
    }


    @Override
    public int getCount() {
        return isProfileListing() ? mPhotos.size() : mPhotos.size() + MAP_NUMBER;
    }


    @Override
    public Fragment getItem(int position) {
        if (position == mPhotos.size()) {
            double latitude = mResultItemModel.getListing().getGeoLocation().getLat();
            double longitude = mResultItemModel.getListing().getGeoLocation().getLng();
            return ListingMapFragment.newInstance(latitude, longitude);
        } else {
            return ImageFragment.newInstance(mPhotos.get(position).getPath());
        }
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getString(R.string.listing_title) + position;
    }


    @Override
    public int getSelectedIndicator(int position) {
        return mSelectedIndicators[position];
    }


    @Override
    public int getDiselectedIndicator(int position) {
        return mDeselectedIndicators[position];
    }


    private void fillSelectedIndicators() {
        int photosCount = mPhotos.size();
        for (int i = 0; i < photosCount; i++) {
            mSelectedIndicators[i] = R.drawable.ic_bubble_selected;
        }
        if (isProfileListing()) {
            mSelectedIndicators[photosCount] = R.drawable.ic_info_bubble_selected;
        } else {
            mSelectedIndicators[photosCount] = R.drawable.ic_map_bubble_selected;
            mSelectedIndicators[photosCount + 1] = R.drawable.ic_info_bubble_selected;
        }
    }


    private void fillDeselectedIndicators() {
        int photosCount = mPhotos.size();
        for (int i = 0; i < photosCount; i++) {
            mDeselectedIndicators[i] = R.drawable.ic_bubble_deselected;
        }
        if (isProfileListing()) {
            mDeselectedIndicators[photosCount] = R.drawable.ic_info_bubble_deselected;
        } else {
            mDeselectedIndicators[photosCount] = R.drawable.ic_map_bubble_deselected;
            mDeselectedIndicators[photosCount + 1] = R.drawable.ic_info_bubble_deselected;
        }
    }


    private boolean isProfileListing() {
        return mResultItemModel.getListing().getServiceType() == NEED_ROOM_SERVICE_TYPE
                || mResultItemModel.getListing().getServiceType() == NEED_APARTMENT_SERVICE_TYPE;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}