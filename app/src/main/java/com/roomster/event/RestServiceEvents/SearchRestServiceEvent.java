package com.roomster.event.RestServiceEvents;


/**
 * Created by michaelkatkov on 11/17/15.
 */
public class SearchRestServiceEvent {

    public static class BookmarkSearchSuccess extends SearchRestServiceEvent {

    }

    public static class SearchSuccess extends SearchRestServiceEvent {

    }

    public static class SearchFailure extends SearchRestServiceEvent {
        public boolean canceled = false;
        public String error = "";
        public SearchFailure(){

        }
        public SearchFailure(String msg){
            error = msg;
        }
        public SearchFailure(String msg,boolean canceled){
            error = msg;
            this.canceled = canceled;
        }
        public SearchFailure(boolean canceled) {
            this.canceled = canceled;
        }
    }
}
