package com.roomster.rest.model;


import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.annotations.SerializedName;
import com.google.maps.android.clustering.ClusterItem;

import java.io.Serializable;


public class ResultItemModel implements ClusterItem, Serializable {

    /**
     **/
    @SerializedName("listing")
    private ListingGetViewModel listing = null;

    /**
     **/
    @SerializedName("user")
    private UserGetViewModel user = null;

    public int count = 0;
    private LatLngBounds customBounds;
    private LatLng customLocation;


    public void setCustomBounds(LatLngBounds customBounds) {
        this.customBounds = customBounds;
    }

    public LatLngBounds getCustomBounds() {
        return customBounds;
    }

    public ListingGetViewModel getListing() {
        return listing;
    }


    public void setListing(ListingGetViewModel listing) {
        this.listing = listing;
    }


    public UserGetViewModel getUser() {
        return user;
    }


    public void setUser(UserGetViewModel user) {
        this.user = user;
    }


    public void setCustomLocaton(LatLng locaton){
        customLocation = locaton;
    }
    public boolean isCountCluster(){ return customLocation != null; }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ResultItemModel {\n");

        sb.append("  listing: ").append(listing).append("\n");
        sb.append("  user: ").append(user).append("\n");
        sb.append("}\n");
        return sb.toString();
    }


    @Override
    public LatLng getPosition() {
        if(customLocation != null)
            return customLocation;
        return new LatLng(listing.getGeoLocation().getLat(), listing.getGeoLocation().getLng());
    }

    @Override
    public boolean equals(Object o) {
        if(o == null || !(o instanceof ResultItemModel))
            return false;
        ResultItemModel other = (ResultItemModel) o;
        if(isCountCluster() != other.isCountCluster()){
            return false;
        }
        if(isCountCluster()){
           return customLocation.equals(other.customLocation);
        }
        return listing.getListingId().equals(other.listing.getListingId());
    }

    @Override
    public int hashCode() {
        if(customLocation != null)
            return customLocation.hashCode();
        return listing.getListingId().hashCode();
    }
}
