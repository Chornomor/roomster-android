package com.roomster.manager;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import com.roomster.R;
import com.roomster.activity.MessagesActivity;
import com.roomster.activity.SupportActivity;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.IntentExtras;
import com.roomster.constants.Notifications;

import javax.inject.Inject;


public class RoomsterNotificationManager {

    @Inject
    Context mContext;


    public RoomsterNotificationManager() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }

    /**
     * Rarely this method could throw the
     * NameNotFoundException so we should catch it
     * to prevent crash in application
     */
    public void showUserMessage(String title, String message, String conversationId, Long userId, Long otherUserId) throws Exception{
        Intent intent = new Intent(mContext, MessagesActivity.class);
        intent.putExtra(IntentExtras.CONVERSATION_ID_EXTRA, conversationId);
        intent.putExtra(IntentExtras.USER_ID_EXTRA, userId);
        intent.putExtra(IntentExtras.OTHER_USER_ID_EXTRA, otherUserId);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);
        stackBuilder.addParentStack(MessagesActivity.class);
        stackBuilder.addNextIntent(intent);

        PendingIntent pendingIntent = stackBuilder
                .getPendingIntent(Notifications.GCM_SUPPORT_NOTIFICATION_PENDING_INTENT_CODE,
                        PendingIntent.FLAG_CANCEL_CURRENT);

        showNotification(title, message, pendingIntent);
    }


    public void showSupportMessage(String title, String message) {
        Intent intent = new Intent(mContext, SupportActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);
        stackBuilder.addParentStack(SupportActivity.class);
        stackBuilder.addNextIntent(intent);

        PendingIntent pendingIntent = stackBuilder
                .getPendingIntent(Notifications.GCM_SUPPORT_NOTIFICATION_PENDING_INTENT_CODE,
                        PendingIntent.FLAG_CANCEL_CURRENT);

        showNotification(title, message, pendingIntent);
    }


    private void showNotification(String title, String message, PendingIntent pendingIntent) {

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.drawable.ic_app_icon).setContentTitle(title).setContentText(message).setAutoCancel(true)
                .setSound(defaultSoundUri).setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) mContext
                .getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(Notifications.GCM_USER_NOTIFICATION_ID, notificationBuilder.build());
    }
}
