package com.roomster.rest.service;


import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.TokenManager;
import com.roomster.rest.api.HelpdeskApi;
import com.roomster.rest.model.HelpdeskViewModel;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Rest service class for handling rest calls for customer support functionality.
 *
 * @author Bogoi.Bogdanov
 */
public class SupportRestService {

    @Inject
    TokenManager mTokenManager;

    @Inject
    HelpdeskApi mHelpdeskApi;

    @Inject
    NetworkManager mNetworkManager;


    public SupportRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void getAllMessages(final RestServiceListener<HelpdeskViewModel> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String token) {
                callGetSupportMessages(token, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                listener.onFailure(code, throwable, msg);
            }
        });
    }


    private void callGetSupportMessages(String authToken, final RestServiceListener<HelpdeskViewModel> listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            //TODO have a better way to provide parameters for the conversations call
            Call<HelpdeskViewModel> callGetSupportMessages = mHelpdeskApi.helpdeskGet(authToken);
            callGetSupportMessages.enqueue(new Callback<HelpdeskViewModel>() {

                @Override
                public void onResponse(Call<HelpdeskViewModel> call, Response<HelpdeskViewModel> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            if (listener != null) {
                                listener.onSuccess(response.body());
                            }
                            break;
                        case ResponseCode.BAD_REQUEST:
                        case ResponseCode.SERVER_ERROR:
                        default:
                            if (listener != null) {
                                listener.onFailure(response.code(), null, null);
                            }
                    }
                }


                @Override
                public void onFailure(Call<HelpdeskViewModel> call, Throwable t) {
                    if (listener != null) {
                        listener.onFailure(-1, t, RoomsterApplication.context.getString(R.string.support_cant_make_rest_call_to_get_message));
                    }
                }
            });
        } else {
            if (listener != null) {
                listener.onFailure(-1, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
            }
        }
    }


    public void sendMessage(final String messageSubject, final String messageText, final RestServiceListener<String> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callMessagePost(result, messageSubject, messageText, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                listener.onFailure(code, throwable, msg);
            }
        });
    }


    private void callMessagePost(String authToken, String messageSubject, String messageText,
                                 final RestServiceListener<String> listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<String> callMessagePost = mHelpdeskApi.helpdeskPost(messageSubject, messageText, authToken);
            callMessagePost.enqueue(new Callback<String>() {

                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            if (listener != null) {
                                listener.onSuccess(null);
                            }
                            break;
                        default:
                            if (listener != null) {
                                listener.onFailure(response.code(), null, null);
                            }
                    }
                }


                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    if (listener != null) {
                        listener.onFailure(-1, t, RoomsterApplication.context.getString(R.string.support_cant_make_rest_call_to_post_message));
                    }
                }
            });
        } else {
            if (listener != null) {
                listener.onFailure(-1, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
            }
        }
    }
}