package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


public class SavedSearch {

    /**
     **/
    @SerializedName("id")
    private Integer id = null;

    /**
     **/
    @SerializedName("name")
    private String name = null;

    /**
     **/
    @SerializedName("path")
    private String path = null;

    /**
     **/
    @SerializedName("is_default")
    private Boolean isDefault = null;


    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getPath() {
        return path;
    }


    public void setPath(String path) {
        this.path = path;
    }


    public Boolean getIsDefault() {
        return isDefault;
    }


    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class SavedSearch {\n");

        sb.append("  id: ").append(id).append("\n");
        sb.append("  name: ").append(name).append("\n");
        sb.append("  path: ").append(path).append("\n");
        sb.append("  isDefault: ").append(isDefault).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
