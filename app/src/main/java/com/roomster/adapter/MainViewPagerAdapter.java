package com.roomster.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import com.roomster.views.ReplacableFragmentStatePagerAdapter;

import java.util.List;


/**
 * Created by Bogoi.Bogdanov on 10/7/2015.
 */
public class MainViewPagerAdapter extends ReplacableFragmentStatePagerAdapter {

    private List<Fragment> mFragments;


    public void clearAll(FragmentManager fm) {
        for (int i = 0; i < mFragments.size(); i++) {
            fm.beginTransaction().remove(mFragments.get(i)).commitAllowingStateLoss();
        }
        mFragments.clear();
    }


    public MainViewPagerAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        mFragments = fragments;
    }


    @Override
    public int getCount() {
        return mFragments.size();
    }


    @Override
    public Fragment getItem(int position) {
        return (Fragment) mFragments.get(position);
    }


    @Override
    protected void handleGetItemInvalidated(View container, Fragment oldFragment, Fragment newFragment) {

    }


    @Override
    protected int getFragmentPosition(Fragment fragment) {
        for (int i = 0; i < mFragments.size(); i++) {
            if (mFragments.get(i).getClass().isInstance(fragment)) {
                return i;
            }
        }

        return 0;
    }
}
