package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class ShareDetails implements Serializable {

    /**
     * Allowed values @ /v1/catalogues/cleanliness
     **/
    @SerializedName("cleanliness")
    private Integer cleanliness = null;

    /**
     * Allowed values @ /v1/catalogues/overnight_guests
     **/
    @SerializedName("overnight_guests")
    private Integer overnightGuests = null;

    /**
     * Allowed values @ /v1/catalogues/party_habits
     **/
    @SerializedName("party_habits")
    private Integer partyHabits = null;

    /**
     * Allowed values @ /v1/catalogues/get_up
     **/
    @SerializedName("get_up")
    private Integer getUp = null;

    /**
     * Allowed values @ /v1/catalogues/go_to_bed
     **/
    @SerializedName("go_to_bed")
    private Integer goToBed = null;

    /**
     * Allowed values @ /v1/catalogues/food_preference
     **/
    @SerializedName("food_preference")
    private Integer foodPreference = null;

    /**
     * Allowed values @ /v1/catalogues/smoking_habits
     **/
    @SerializedName("my_smoking_habits")
    private Long mySmokingHabits = null;

    /**
     * Allowed values @ /v1/catalogues/work_schedule
     **/
    @SerializedName("work_schedule")
    private Long workSchedule = null;

    /**
     * Allowed values @ /v1/catalogues/occupation
     **/
    @SerializedName("occupation")
    private Long occupation = null;

    /**
     * Allowed values @ /v1/catalogues/pets_owned
     **/
    @SerializedName("my_pets")
    private ArrayList<Long> myPets = null;

    /**
     **/
    @SerializedName("age_preference_min")
    private Integer agePreferenceMin = null;

    /**
     **/
    @SerializedName("age_preference_max")
    private Integer agePreferenceMax = null;

    /**
     **/
    @SerializedName("smoking_preference")
    private Long smokingPreference = null;

    /**
     * Allowed values @ /v1/catalogues/pets_preferred
     **/
    @SerializedName("pets_preference")
    private ArrayList<Long> petsPreference = null;

    /**
     **/
    @SerializedName("students_only")
    private Boolean studentsOnly = null;

    /**
     **/
    @SerializedName("household_age_min")
    private Integer householdAgeMin = null;

    /**
     **/
    @SerializedName("household_age_max")
    private Integer householdAgeMax = null;

    /**
     * Allowed values @ /v1/catalogues/household_sex_category
     **/
    @SerializedName("household_sex")
    private Integer householdSex = null;

    /**
     **/
    @SerializedName("people_in_household")
    private Integer peopleInHousehold = null;

    /**
     **/
    @SerializedName("movein_fee")
    private Integer moveinFee = null;

    /**
     **/
    @SerializedName("utilities_cost")
    private Integer utilitiesCost = null;

    /**
     **/
    @SerializedName("parking_rent")
    private Integer parkingRent = null;

    /**
     **/
    @SerializedName("general_location")
    private String generalLocation = null;

    /**
     **/
    @SerializedName("is_furnished")
    private Boolean isFurnished = null;

    /**
     * Allowed values @ /v1/catalogues/building_types
     **/
    @SerializedName("building_type")
    private Long buildingType = null;

    /**
     * Allowed values @ /v1/catalogues/room_amenities
     **/
    @SerializedName("room_amenities")
    private ArrayList<Long> roomAmenities = null;


    public Integer getCleanliness() {
        return cleanliness;
    }


    public void setCleanliness(Integer cleanliness) {
        this.cleanliness = cleanliness;
    }


    public Integer getOvernightGuests() {
        return overnightGuests;
    }


    public void setOvernightGuests(Integer overnightGuests) {
        this.overnightGuests = overnightGuests;
    }


    public Integer getPartyHabits() {
        return partyHabits;
    }


    public void setPartyHabits(Integer partyHabits) {
        this.partyHabits = partyHabits;
    }


    public Integer getGetUp() {
        return getUp;
    }


    public void setGetUp(Integer getUp) {
        this.getUp = getUp;
    }


    public Integer getGoToBed() {
        return goToBed;
    }


    public void setGoToBed(Integer goToBed) {
        this.goToBed = goToBed;
    }


    public Integer getFoodPreference() {
        return foodPreference;
    }


    public void setFoodPreference(Integer foodPreference) {
        this.foodPreference = foodPreference;
    }


    public Long getMySmokingHabits() {
        return mySmokingHabits;
    }


    public void setMySmokingHabits(Long mySmokingHabits) {
        this.mySmokingHabits = mySmokingHabits;
    }


    public Long getWorkSchedule() {
        return workSchedule;
    }


    public void setWorkSchedule(Long workSchedule) {
        this.workSchedule = workSchedule;
    }


    public Long getOccupation() {
        return occupation;
    }


    public void setOccupation(Long occupation) {
        this.occupation = occupation;
    }


    public List<Long> getMyPets() {
        return myPets;
    }


    public void setMyPets(ArrayList<Long> myPets) {
        if (myPets != null) {
            this.myPets = new ArrayList<>(myPets);
        } else {
            this.myPets = new ArrayList<>();
        }
    }


    public Integer getAgePreferenceMin() {
        return agePreferenceMin;
    }


    public void setAgePreferenceMin(Integer agePreferenceMin) {
        this.agePreferenceMin = agePreferenceMin;
    }


    public Integer getAgePreferenceMax() {
        return agePreferenceMax;
    }


    public void setAgePreferenceMax(Integer agePreferenceMax) {
        this.agePreferenceMax = agePreferenceMax;
    }


    public Long getSmokingPreference() {
        return smokingPreference;
    }


    public void setSmokingPreference(Long smokingPreference) {
        this.smokingPreference = smokingPreference;
    }


    public List<Long> getPetsPreference() {
        return petsPreference;
    }


    public void setPetsPreference(List<Long> petsPreference) {
        if (petsPreference != null) {
            this.petsPreference = new ArrayList<>(petsPreference);
        } else {
            this.petsPreference = new ArrayList<>();
        }
    }


    public Boolean getStudentsOnly() {
        return studentsOnly;
    }


    public void setStudentsOnly(Boolean studentsOnly) {
        this.studentsOnly = studentsOnly;
    }


    public Integer getHouseholdAgeMin() {
        return householdAgeMin;
    }


    public void setHouseholdAgeMin(Integer householdAgeMin) {
        this.householdAgeMin = householdAgeMin;
    }


    public Integer getHouseholdAgeMax() {
        return householdAgeMax;
    }


    public void setHouseholdAgeMax(Integer householdAgeMax) {
        this.householdAgeMax = householdAgeMax;
    }


    public Integer getHouseholdSex() {
        return householdSex;
    }


    public void setHouseholdSex(Integer householdSex) {
        this.householdSex = householdSex;
    }


    public Integer getPeopleInHousehold() {
        return peopleInHousehold;
    }


    public void setPeopleInHousehold(Integer peopleInHousehold) {
        this.peopleInHousehold = peopleInHousehold;
    }


    public Integer getMoveinFee() {
        return moveinFee;
    }


    public void setMoveinFee(Integer moveinFee) {
        this.moveinFee = moveinFee;
    }


    public Integer getUtilitiesCost() {
        return utilitiesCost;
    }


    public void setUtilitiesCost(Integer utilitiesCost) {
        this.utilitiesCost = utilitiesCost;
    }


    public Integer getParkingRent() {
        return parkingRent;
    }


    public void setParkingRent(Integer parkingRent) {
        this.parkingRent = parkingRent;
    }


    public String getGeneralLocation() {
        return generalLocation;
    }


    public void setGeneralLocation(String generalLocation) {
        this.generalLocation = generalLocation;
    }


    public Boolean getIsFurnished() {
        return isFurnished;
    }


    public void setIsFurnished(Boolean isFurnished) {
        this.isFurnished = isFurnished;
    }


    public Long getBuildingType() {
        return buildingType;
    }


    public void setBuildingType(Long buildingType) {
        this.buildingType = buildingType;
    }


    public List<Long> getRoomAmenities() {
        return roomAmenities;
    }


    public void setRoomAmenities(List<Long> roomAmenities) {
        if (roomAmenities != null) {
            this.roomAmenities = new ArrayList<>(roomAmenities);
        } else {
            this.roomAmenities = new ArrayList<>();
        }
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ShareDetails {\n");

        sb.append("  cleanliness: ").append(cleanliness).append("\n");
        sb.append("  overnightGuests: ").append(overnightGuests).append("\n");
        sb.append("  partyHabits: ").append(partyHabits).append("\n");
        sb.append("  getUp: ").append(getUp).append("\n");
        sb.append("  goToBed: ").append(goToBed).append("\n");
        sb.append("  foodPreference: ").append(foodPreference).append("\n");
        sb.append("  mySmokingHabits: ").append(mySmokingHabits).append("\n");
        sb.append("  workSchedule: ").append(workSchedule).append("\n");
        sb.append("  occupation: ").append(occupation).append("\n");
        sb.append("  myPets: ").append(myPets).append("\n");
        sb.append("  agePreferenceMin: ").append(agePreferenceMin).append("\n");
        sb.append("  agePreferenceMax: ").append(agePreferenceMax).append("\n");
        sb.append("  smokingPreference: ").append(smokingPreference).append("\n");
        sb.append("  petsPreference: ").append(petsPreference).append("\n");
        sb.append("  studentsOnly: ").append(studentsOnly).append("\n");
        sb.append("  householdAgeMin: ").append(householdAgeMin).append("\n");
        sb.append("  householdAgeMax: ").append(householdAgeMax).append("\n");
        sb.append("  householdSex: ").append(householdSex).append("\n");
        sb.append("  peopleInHousehold: ").append(peopleInHousehold).append("\n");
        sb.append("  moveinFee: ").append(moveinFee).append("\n");
        sb.append("  utilitiesCost: ").append(utilitiesCost).append("\n");
        sb.append("  parkingRent: ").append(parkingRent).append("\n");
        sb.append("  generalLocation: ").append(generalLocation).append("\n");
        sb.append("  isFurnished: ").append(isFurnished).append("\n");
        sb.append("  buildingType: ").append(buildingType).append("\n");
        sb.append("  roomAmenities: ").append(roomAmenities).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
