package com.roomster.activity;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.IntentExtras;
import com.roomster.event.RestServiceEvents.UserListingsRestServiceEvent;
import com.roomster.fragment.MyListingFragment;
import com.roomster.manager.CatalogsManager;
import com.roomster.manager.MeManager;
import com.roomster.model.CatalogModel;
import com.roomster.rest.model.ListingGetViewModel;
import com.roomster.rest.service.ListingsRestService;
import com.roomster.rest.service.UserListingsRestService;
import com.roomster.utils.LogUtil;
import com.roomster.utils.ShareUtil;
import com.roomster.views.undo.UndoScnackbar;

import javax.inject.Inject;


/**
 * Created by michaelkatkov on 12/11/15.
 */
public class MyListingActivity extends BaseActivity implements MyListingFragment.SwipeCallback {

    private static final int REQ_GALLERY = 1;

    public static final String LISTING_INDEX = "listing_index";
    public static final String LISTING_ID = "listing_id";
    private static final String NEED_ROOM = "NeedRoom";
    private static final String HAVE_SHARE = "HaveShare";
    private static final String NEED_APARTMENT = "NeedApartment";
    private static final String HAVE_APARTMENT = "HaveApartment";

    private ListingGetViewModel mCurrentListingModel;
    private long mListingId;
    private int mListingIndex;

    @Bind(R.id.my_listing_toolbar)
    Toolbar mToolbar;

    @Bind(R.id.my_listing_back)
    ImageView mBackBtn;

    @Bind(R.id.my_listing_title)
    TextView mTitleTv;

    @Bind(R.id.my_listing_second_line)
    TextView mSecondLineTv;

    @Bind(R.id.my_listing_edit)
    ImageView mEditBtn;

    @Inject
    MeManager mMeManager;

    @Inject
    CatalogsManager mCatalogsManager;

    @Inject
    ListingsRestService mListingRestService;

    @Inject
    UserListingsRestService mUserListingRestService;

    @Inject
    Tracker mTracker;


    @Override
    protected int getMainContentLayout() {
        return R.layout.activity_my_listing;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        ButterKnife.bind(this);
        initToolbar();
        readIntent();
    }


    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName(getString(R.string.screen_name_my_listings_detailed));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        initView();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_litings, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.share_listing:
                checkWriteStoragePermissionBeforeShare();
                break;
            case R.id.delete_listing:
                showDeleteListingDialog();
                break;
            case R.id.add_new_listing:
                startAddNewListingActivity();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onPageChanged(int page) {
        if (!(page < mMeManager.getMyListings().size())) return;
        mListingIndex = page;
        mCurrentListingModel = mMeManager.getMyListings().get(mListingIndex);
        mListingId = mCurrentListingModel.getListingId();
        setTitleBarListingType();
        setTitleBarTotalListing(mListingIndex);
    }


    @OnClick(R.id.my_listing_back)
    void onToolbarBackClick() {
        try {
            onBackPressed();
        } catch (IllegalStateException e) {
            // could not do this action after onSaveInstanceState
        }

    }


    @OnClick(R.id.my_listing_edit)
    void onEditClick() {
        startEditListingActivity();
    }


    public void setContent(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.my_listing_content, fragment).commitAllowingStateLoss();
    }


    private void setTitleBarListingType() {
        CatalogModel serviceTypeModel = mCatalogsManager.getServiceTypeById(mCurrentListingModel.getServiceType());
        if (serviceTypeModel == null) {
            return;
        }
//        switch (serviceTypeModel.getName()) {
//        case NEED_ROOM:
//            mSecondLineTv.setText(getString(R.string.el_looking_for_room));
//            break;
//        case HAVE_SHARE:
//            mSecondLineTv.setText(getString(R.string.al_offering_room));
//            break;
//        case NEED_APARTMENT:
//            mSecondLineTv.setText(getString(R.string.looking_for_apartment));
//            break;
//        case HAVE_APARTMENT:
//            mSecondLineTv.setText(getString(R.string.al_offering_apartment));
//            break;
//        default:
//            break;
//        }
    }


    private void initView() {
        mCurrentListingModel = mMeManager.findListingById(mListingId);
        if (mCurrentListingModel != null) {
            MyListingFragment listingFragment = MyListingFragment.newInstance(mListingIndex);
            listingFragment.setCallback(this);
            setContent(listingFragment);
            setTitleBarListingType();
            setTitleBarTotalListing(mListingIndex);
        } else {
            finish();
        }
    }


    private void readIntent() {
        mListingIndex = getIntent().getIntExtra(LISTING_INDEX, -1);
        mListingId = getIntent().getLongExtra(LISTING_ID, -1L);
    }


    private void initToolbar() {
        setSupportActionBar(mToolbar);
        mToolbar.setTitle("");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }


    private void setTitleBarTotalListing(int position) {
        String stringToFill = getString(R.string.my_listing);
        mTitleTv.setText(stringToFill);
    }


    private void showDeleteListingDialog() {
        new AlertDialog.Builder(this, R.style.AppTheme_AlertDialog).setMessage(R.string.delete_listing_dialog_content)
                .setTitle(R.string.delete_listing)
                .setPositiveButton(R.string.delete_listing_dialog_delete_button, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mEventBus.post(new UserListingsRestServiceEvent.ListingDeleted(mListingId));
                        Intent data = new Intent();
                        data.putExtra(UndoScnackbar.KEY_DELTED_ITEM, mListingId);
                        setResult(RESULT_OK, data);
                        mUserListingRestService.getUserListings(mMeManager.getMe().getId(), true);
                        finish();
                    }
                }).setNegativeButton(R.string.dialog_cancel_button, null).show();
    }


    private void startAddNewListingActivity() {
        Intent addNewListingIntent = new Intent(this, AddNewListingActivity.class);
        addNewListingIntent.putExtra(IntentExtras.WITH_BACK_ARROW, true);
        startActivity(addNewListingIntent);
    }


    private void startEditListingActivity() {
        Intent editListingIntent = new Intent(this, EditMyListingActivity.class);
        editListingIntent.putExtra(EditMyListingActivity.LISTING_ID, mListingId);
        editListingIntent.putExtra(EditMyListingActivity.LISTING_INDEX, mListingIndex);
        startActivity(editListingIntent);
    }


    private void checkWriteStoragePermissionBeforeShare() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permission = checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permission == PackageManager.PERMISSION_GRANTED) {
                shareListing();
            } else {
                requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQ_GALLERY);
            }
        } else {
            shareListing();
        }
    }


    private void shareListing() {
        Target<Bitmap> loadTarget = new SimpleTarget<Bitmap>() {

            @Override
            public void onResourceReady(Bitmap bitmap, GlideAnimation glideAnimation) {
                String path = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "bitmap", null);
                if (path != null) {
                    Uri uri = Uri.parse(path);
                    ShareUtil.shareListing(MyListingActivity.this, mCurrentListingModel, uri);
                } else {
                    Toast.makeText(MyListingActivity.this, R.string.my_listing_share_failed, Toast.LENGTH_SHORT).show();
                }
            }
        };
        Glide.with(this).load(mCurrentListingModel.getPhotos().get(0).getPath()).asBitmap().into(loadTarget);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQ_GALLERY:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    shareListing();
                } else {
                    LogUtil.logE("WRITE_EXTERNAL_STORAGE permission denied");
                }
                break;
        }
    }

}
