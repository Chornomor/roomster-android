package com.roomster.dialog;


import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.di.component.RoomsterComponent;
import com.roomster.event.application.ApplicationEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.ButterKnife;


public class LoadingDialog extends Dialog {

    @Inject
    LayoutInflater mInflater;

    private AnimationDrawable mAnimation;


    public LoadingDialog(Context context) {
//        super(context);
        super(context, R.style.LoadingDialogTheme);
        RoomsterApplication.getRoomsterComponent().inject(this);
        initView();
        initAnimation();
    }


    private void initView() {
        View view = mInflater.inflate(R.layout.activity_loading, null);
        setContentView(view);
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (mAnimation != null) {
            mAnimation.start();
        }
    }


    private void initAnimation() {
        ImageView loadingImage = (ImageView) findViewById(R.id.loading_image);
        if (loadingImage != null) {
            loadingImage.setBackgroundResource(R.drawable.loading_animation);
            mAnimation = (AnimationDrawable) loadingImage.getBackground();
        }
    }


    @Override
    public void onBackPressed() {
        //do nothing
    }
}
