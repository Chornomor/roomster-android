package com.roomster.views.signin;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.roomster.R;

/**
 * Created by andreybofanov on 05.07.16.
 */
public class SigninButton extends ImageView {
    private Bitmap mBtnBitmap,mClickedBtnBitmap;

    public SigninButton(Context context) {
        super(context);
        init();
    }

    public SigninButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SigninButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        makeLayout();
        makeBitmapFromView();
        makeFilter();
    }

    void makeLayout() {
        setClickable(true);
        setBackgroundResource(R.drawable.btn_signing_selector);
    }

    void makeBitmapFromView() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.button_maker_signing, null);
        v.measure(0, 0);
        v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
        mBtnBitmap = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mBtnBitmap);
        v.draw(canvas);

    }

    void makeFilter() {
        float[] colorTransform = {
                0, 0, 0, 0, 255,
                0, 0, 0, 0, 255,
                0, 0, 0, 0, 255,
                -1, 0, 0, 0, 255
        };

        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0f); //Remove Colour
        colorMatrix.set(colorTransform); //Apply the Red

        ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
        Paint paint = new Paint();
        paint.setColorFilter(colorFilter);
        mClickedBtnBitmap = Bitmap.createBitmap(mBtnBitmap.getWidth(),mBtnBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mClickedBtnBitmap);
        canvas.drawBitmap(mBtnBitmap,0,0,paint);
        StateListDrawable states = new StateListDrawable();
        BitmapDrawable drw = new BitmapDrawable(getContext().getResources(),mBtnBitmap);
        BitmapDrawable drwPressed = new BitmapDrawable(getContext().getResources(),mClickedBtnBitmap);
        states.addState(new int[]{android.R.attr.state_pressed},drwPressed);
        states.addState(new int[]{-android.R.attr.state_pressed},drw);
        setImageDrawable(states);
        setAdjustViewBounds(true);
    }


}
