package com.roomster.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.model.CatalogModel;

import java.util.List;

import javax.inject.Inject;


public class CatalogSpinnerAdapter extends ArrayAdapter<CatalogModel> {

    @Inject
    LayoutInflater mLayoutInflater;


    public CatalogSpinnerAdapter(Context context, List<CatalogModel> models) {
        super(context, 0, models);
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.item_spinner_plain, parent, false);
        }
        CatalogModel model = getItem(position);
        TextView textV = (TextView) view.findViewById(R.id.item_spinner_plain);
        textV.setText(model.getName());
        return view;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.item_spinner_dropdown_plain, parent, false);
        }

        CatalogModel model = getItem(position);
        TextView textV = (TextView) view.findViewById(R.id.item_spinner_dropdown_plain);
        textV.setText(model.getName());
        return view;
    }
}
