package com.roomster.rest.api;

import com.roomster.rest.model.BillingAvailableModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface UserStatusApi {

    @PUT("UserStatus")
    Call<String> statusPut(@Query("is_active") Boolean isActive, @Header("Authorization") String authorization,
                           @Header("Currency") String currency, @Header("Locale") String locale);

    @GET("UserStatus")
    Call<BillingAvailableModel> statusGet(@Header("Authorization") String authorization, @Header("Currency") String currency,
                           @Header("Locale") String locale);
}
