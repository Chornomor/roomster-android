package com.roomster.activity;


import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.roomster.R;


/**
 * Created by "Michael Katkov" on 10/19/2015.
 */
public class BaseToolbarActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.iv_back)
    ImageView mBackButton;

    @Bind(R.id.tv_title)
    TextView mTitleTv;

    @Bind(R.id.tv_counter)
    TextView mCounterTv;

    @Bind(R.id.toolbar_icon)
    ImageView mToolbarIcon;


    public void setContent(Fragment fragment) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN){
            if (!isDestroyed() || !isFinishing())
                getSupportFragmentManager().beginTransaction().replace(R.id.fl_content, fragment).commit();
        }else{
            if (!isFinishing())
                getSupportFragmentManager().beginTransaction().replace(R.id.fl_content, fragment).commit();
        }
    }


    @Override
    protected int getMainContentLayout() {
        return R.layout.activity_base_toolbar;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        setUpActionBar();
    }


    @OnClick(R.id.iv_back)
    void onToolbarBackClick() {
        handleOnBackPress();
    }


    private void handleOnBackPress() {
        if (!isFinishing()) {
            try {
                onBackPressed();
            } catch (ActivityNotFoundException e) {
                //ignore if activity don't exist
            } catch (IllegalStateException ignored) {
            // There's no way to avoid getting this if saveInstanceState has already been called.
             }
        }
    }


    private void setUpActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
    }
}
