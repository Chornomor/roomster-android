package com.roomster.activity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.roomster.R;
import com.roomster.constants.IntentExtras;
import com.roomster.fragment.AddListingDetailedFragment;
import com.roomster.fragment.AddNewListingFragment;


/**
 * Created by "Michael Katkov" on 12/7/2015.
 */
public class AddNewListingActivity extends BaseToolbarActivity implements AddNewListingFragment.AddNewListingListener {

    private boolean mWithBackArrow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AddNewListingFragment addNewListingFragment = new AddNewListingFragment();
        setContent(addNewListingFragment);
        readIntent();
    }


    @Override
    protected void onResume() {
        super.onResume();
        setUpTitle();
    }


    @Override
    public void onBackPressed() {
        if (mWithBackArrow) {
            super.onBackPressed();
        }
    }


    @Override
    public void onNewListingItemClick(int listingType) {
        Intent intent = new Intent(this, AddNewDetailedListingActivity.class);
        intent.putExtra(AddListingDetailedFragment.LISTING_TYPE, listingType);
        intent.putExtra(IntentExtras.WITH_BACK_ARROW, mWithBackArrow);
        startActivity(intent);
        finish();
    }

    
    private void readIntent() {
        mWithBackArrow = getIntent().getBooleanExtra(IntentExtras.WITH_BACK_ARROW, false);
        if (!mWithBackArrow) {
            mBackButton.setVisibility(View.GONE);
        }
    }


    private void setUpTitle() {
        mTitleTv.setVisibility(View.GONE);
        if (mTitleTv.getText().toString().isEmpty()) {
            mTitleTv.setText(getString(R.string.al_add_new_listing));
        }
    }
}