package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


public class NotificationsViewModel {

    /**
     **/
    @SerializedName("account_updates")
    private Boolean accountUpdates = null;

    /**
     **/
    @SerializedName("matches")
    private Boolean matches = null;

    /**
     **/
    @SerializedName("helpdesk_activity")
    private Boolean helpdeskActivity = null;

    /**
     **/
    @SerializedName("new_messages")
    private Boolean newMessages = null;


    public Boolean getAccountUpdates() {
        return accountUpdates;
    }


    public void setAccountUpdates(Boolean accountUpdates) {
        this.accountUpdates = accountUpdates;
    }


    public Boolean getMatches() {
        return matches;
    }


    public void setMatches(Boolean matches) {
        this.matches = matches;
    }


    public Boolean getHelpdeskActivity() {
        return helpdeskActivity;
    }


    public void setHelpdeskActivity(Boolean helpdeskActivity) {
        this.helpdeskActivity = helpdeskActivity;
    }


    public Boolean getNewMessages() {
        return newMessages;
    }


    public void setNewMessages(Boolean newMessages) {
        this.newMessages = newMessages;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class NotificationsViewModel {\n");

        sb.append("  accountUpdates: ").append(accountUpdates).append("\n");
        sb.append("  matches: ").append(matches).append("\n");
        sb.append("  helpdeskActivity: ").append(helpdeskActivity).append("\n");
        sb.append("  newMessages: ").append(newMessages).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
