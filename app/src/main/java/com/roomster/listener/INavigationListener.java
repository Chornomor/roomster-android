package com.roomster.listener;


/**
 * Created by emil on 9/14/15. Email: emil@alttab.co
 * <p/>
 * Project: Roomster-Android
 * <p/>
 * Copyright 2015 (c)
 */
public interface INavigationListener {

    void navigateToProfileScreen();
}
