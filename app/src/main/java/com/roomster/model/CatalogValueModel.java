package com.roomster.model;


import com.google.gson.internal.LinkedTreeMap;


public class CatalogValueModel extends CatalogModel {

    private long mValue;


    public CatalogValueModel(LinkedTreeMap<String, Object> linkedTreeMap) {
        super(linkedTreeMap);
        if (linkedTreeMap.containsKey("Value")) {
            mValue = ((Double) linkedTreeMap.get("Value")).longValue();
        }
    }


    public CatalogValueModel(int id, String name, long value) {
        super(id, name);
        mValue = value;
    }


    public long getValue() {
        return mValue;
    }


    public void setValue(long value) {
        this.mValue = mValue;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CatalogValueModel)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        CatalogValueModel that = (CatalogValueModel) o;

        return mValue == that.mValue;
    }


    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) (mValue ^ (mValue >>> 32));
        return result;
    }
}
