package com.roomster.dialog;


import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.rest.model.SocialConnectionViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class SocialConnectionsDialog extends android.support.v7.app.AlertDialog {

    private static final String UPGRADE_TO_VIEW = "UPGRADE TO VIEW";

    private static final String FACEBOOK_APP_PACKAGE_NAME = "com.facebook.katana";
    private static final String FACEBOOK_IN_APP_PREFIX    = "fb://facewebmodal/f?href=";

    private static final String TWITTER_APP_PACKAGE_NAME = "com.twitter.android";
    private static final String TWITTER_IN_APP_PREFIX    = "twitter://user?screen_name=";

    private static final String INSTAGRAM_APP_PACKAGE_NAME = "com.instagram.android";
    private static final String INSTAGRAM_IN_APP_PREFIX    = "http://instagram.com/_u/";

    @Bind(R.id.dialog_social_facebook)
    TextView mFacebookView;

    @Bind(R.id.dialog_social_twitter)
    TextView mTwitterView;

    @Bind(R.id.dialog_social_instagram)
    TextView mInstagramView;

    @Bind(R.id.dialog_social_linkedin)
    TextView mLinkedinView;

    private List<SocialConnectionViewModel> mConnections;
    private String                          mUserName;
    private SocialConnectionClickListener   mSocialConnectionClickListener;


    public SocialConnectionsDialog(Context context, List<SocialConnectionViewModel> connections, String userName) {
        super(context);

        if (connections != null) {
            mConnections = connections;
        } else {
            mConnections = new ArrayList<>();
        }
        mUserName = userName;
        mSocialConnectionClickListener = new SocialConnectionClickListener();

        setTitle(String.format(context.getString(R.string.dialog_social_connections_title_format), mUserName));
        setButtonText(R.string.dialog_cancel_button);
        initView();
    }


    private void initSocialConnectionsViews() {
        for (SocialConnectionViewModel connection : mConnections) {
            if (connection.getProfileUrl().equals(UPGRADE_TO_VIEW)) {
                setMessage(
                  String.format(getContext().getString(R.string.dialog_social_connections_upgrade_to_view_format), mUserName));
                setButtonText(R.string.dialog_ok_button);
                setTitle(null);
                break;
            }
            if (connection.getIsVisible() && connection.getSocialNetwork() != null) {
                switch (connection.getSocialNetwork()) {
                    case Facebook:
                        enableSocialConnectionView(mFacebookView, connection);
                        break;
                    case Twitter:
                        enableSocialConnectionView(mTwitterView, connection);
                        break;
                    case LinkedIn:
                        enableSocialConnectionView(mLinkedinView, connection);
                        break;
                    case Instagram:
                        enableSocialConnectionView(mInstagramView, connection);
                        break;
                }
            }
        }
    }


    private void enableSocialConnectionView(View view, SocialConnectionViewModel model) {
        view.setVisibility(View.VISIBLE);
        view.setOnClickListener(mSocialConnectionClickListener);
        view.setTag(model);
    }


    private void initView() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.dialog_social_connections, null);
        ButterKnife.bind(this, contentView);
        setView(contentView);
        initSocialConnectionsViews();
    }


    private void setButtonText(int textResId) {
        setButton(BUTTON_NEGATIVE, getContext().getString(textResId), new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
    }


    private class SocialConnectionClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Object socialConnection = v.getTag();
            if (socialConnection instanceof SocialConnectionViewModel) {
                dismiss();
                handleSocialConnectionClick((SocialConnectionViewModel) socialConnection);
            }
        }


        private void handleSocialConnectionClick(SocialConnectionViewModel model) {
            switch (model.getSocialNetwork()) {
                case Facebook:
                    try {
                        getContext().startActivity(getOpenFacebookIntent(model));
                    } catch (ActivityNotFoundException ex) {
                        // Nothing to do
                    }
                    break;
                case Twitter:
                    try {
                        getContext().startActivity(getOpenTwitterIntent(model.getProfileUrl()));
                    } catch (ActivityNotFoundException ex) {
                        // Nothing to do
                    }
                    break;
                case Instagram:
                    try {
                        getContext().startActivity(getOpenInstagramIntent(model.getProfileUrl()));
                    } catch (ActivityNotFoundException ex) {
                        // Nothing to do
                    }
                    break;
                case LinkedIn:
                    // Do nothing - apparently LinkedIn don't expose public API for deep linking.
                    // In order to integrate deep linking, we need to integrate LinkedIn SDK.
                    // But it seems LinkedIn app handles normal http requests naturally, so there is
                    // no need for integration the LinkedIn SDK.
                default:
                    sendViewUrlIntent(model.getProfileUrl());
            }
        }


        private Intent getOpenFacebookIntent(SocialConnectionViewModel model) {
            String facebookAppUri = FACEBOOK_IN_APP_PREFIX + model.getProfileUrl();
            if (isAppInstalled(FACEBOOK_APP_PACKAGE_NAME)) {
                return new Intent(Intent.ACTION_VIEW, Uri.parse(facebookAppUri)).setPackage(FACEBOOK_APP_PACKAGE_NAME)
                  .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }

            return new Intent(Intent.ACTION_VIEW, Uri.parse(model.getProfileUrl()));
        }


        private Intent getOpenTwitterIntent(String url) {
            String userId = url.substring(url.lastIndexOf('/') + 1);
            String twitterAppUri = TWITTER_IN_APP_PREFIX + userId;
            //Check if twitter app is installed.
            if (isAppInstalled(TWITTER_APP_PACKAGE_NAME)) {
                return new Intent(Intent.ACTION_VIEW, Uri.parse(twitterAppUri)).setPackage(TWITTER_APP_PACKAGE_NAME)
                  .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }

            return new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        }


        private Intent getOpenInstagramIntent(String url) {
            if (url.endsWith("/")) {
                url = url.substring(0, url.length() - 1);
            }
            String userId = url.substring(url.lastIndexOf('/') + 1);
            String instagramAppUri = INSTAGRAM_IN_APP_PREFIX + userId;

            //Check if instagram app is installed.
            if (isAppInstalled(INSTAGRAM_APP_PACKAGE_NAME)) {
                return new Intent(Intent.ACTION_VIEW, Uri.parse(instagramAppUri)).setPackage(INSTAGRAM_APP_PACKAGE_NAME)
                  .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }

            return new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        }


        private boolean isAppInstalled(String uri) {
            PackageManager pm = getContext().getPackageManager();
            boolean isAppInstalled;
            try {
                pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
                isAppInstalled = true;
            } catch (PackageManager.NameNotFoundException e) {
                isAppInstalled = false;
            }
            return isAppInstalled;
        }


        private void sendViewUrlIntent(String url) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            getContext().startActivity(i);
        }
    }
}
