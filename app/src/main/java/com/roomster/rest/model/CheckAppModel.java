package com.roomster.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bogoi.Bogdanov on 5/13/2016.
 */
public class CheckAppModel {

    @SerializedName("force_update")
    private Boolean forceUpdate = null;

    public Boolean shouldForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(Boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }
}