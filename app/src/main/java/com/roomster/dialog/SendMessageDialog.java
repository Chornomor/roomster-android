package com.roomster.dialog;


import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.roomster.R;


public class SendMessageDialog extends android.support.v7.app.AlertDialog {

    public interface SendMessageListener {

        void onSendMessage(String message);
    }


    private EditText            mMessageText;
    private SendMessageListener mSendMessageListener;


    public SendMessageDialog(Context context, String userName, SendMessageListener listener) {
        super(context);

        mSendMessageListener = listener;

        initViews();
        initButtons();
        setTitle(String.format(context.getString(R.string.dialog_send_message_title_format), userName));
    }


    private void initButtons() {
        setButton(BUTTON_POSITIVE, getContext().getString(R.string.dialog_send_message_send_button), new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mSendMessageListener != null) {
                    mSendMessageListener.onSendMessage(mMessageText.getText().toString());
                }
                mMessageText.setText("");
            }
        });

        setButton(BUTTON_NEGATIVE, getContext().getString(R.string.report_user_dialog_cancel_button), new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                mMessageText.setText("");
            }
        });
    }


    private void initViews() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.dialog_send_message, null);
        setView(contentView);

        mMessageText = (EditText) contentView.findViewById(R.id.dialog_send_message_text);
    }
}
