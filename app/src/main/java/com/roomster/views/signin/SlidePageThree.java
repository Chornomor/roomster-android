package com.roomster.views.signin;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.roomster.R;

/**
 * Created by andreybofanov on 18.07.16.
 */
public class SlidePageThree extends AbstactSlidePage {

    final String openGreenColorTag = "<font color='#00ff00'>";
    final String closeGreenColorTag = "</font>";

    public SlidePageThree(Context context) {
        super(context);
        init(context);
    }

    public SlidePageThree(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SlidePageThree(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    void init(Context context){
        View v = LayoutInflater.from(getContext()).inflate(R.layout.signin_page_three,null);
        ((TextView)v.findViewById(R.id.signin_three_live_better)).setText(Html.fromHtml(context.getString(R.string.signin_slide_2_line_1_live)
                + " " + openGreenColorTag
                + context.getString(R.string.signin_slide_2_line_1_better) + closeGreenColorTag));
        addView(v);
        layout = (ViewGroup) v.findViewById(R.id.layout);
        setTypefaces();
    }
}
