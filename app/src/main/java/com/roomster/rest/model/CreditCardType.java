package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


public class CreditCardType {

    /**
     **/
    @SerializedName("Code")
    private String code = null;

    /**
     **/
    @SerializedName("Name")
    private String name = null;


    public String getCode() {
        return code;
    }


    public void setCode(String code) {
        this.code = code;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class CreditCardType {\n");

        sb.append("  code: ").append(code).append("\n");
        sb.append("  name: ").append(name).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
