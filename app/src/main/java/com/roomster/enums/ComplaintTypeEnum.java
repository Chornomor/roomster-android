package com.roomster.enums;

import com.roomster.R;

public enum ComplaintTypeEnum {
    InappropriateOffensiveContent, ScamArtist, CommercialPosting, Other, Miscategorized;


    public int toStringRes() {
        switch (this) {
            case InappropriateOffensiveContent:
                return R.string.report_user_complaint_type_inappropriate_offensive_content;
            case ScamArtist:
                return R.string.report_user_complaint_type_scam_artist;
            case CommercialPosting:
                return R.string.report_user_complaint_type_commercial_posting;
            case Other:
                return R.string.report_user_complaint_type_other;
            case Miscategorized:
                return R.string.report_user_complaint_type_miscategorized;
        }
        return 0;
    }

}