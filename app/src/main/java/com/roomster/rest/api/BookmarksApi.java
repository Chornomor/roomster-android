package com.roomster.rest.api;


import com.roomster.rest.model.BookmarkCounter;
import com.roomster.rest.model.BookmarkPostModel;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface BookmarksApi {

    /**
     * Get bookmark counters Returns a list of all bookmark counters.
     *
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<List<BookmarkCounter>>
     */
    @GET("Bookmarks")
    Call<List<BookmarkCounter>> bookmarksGet(@Header("Authorization") String authorization);

    /**
     * Add bookmark
     *
     * @param model
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<Object>
     */
    @POST("Bookmarks")
    Call<ResponseBody> bookmarksPost(@Body BookmarkPostModel model, @Header("Authorization") String authorization);

    /**
     * Clear bookmark counters
     *
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<Object>
     */
    @DELETE("Bookmarks")
    Call<ResponseBody> bookmarksDelete(@Query("model.listing_id") Long listingId, @Query("model.service_type") String serviceType,
                                       @Query("model.bookmark_type") String bookmarkType,
                                       @Header("Authorization") String authorization);
}
