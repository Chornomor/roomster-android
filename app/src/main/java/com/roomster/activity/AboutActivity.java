package com.roomster.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.IntentExtras;
import com.roomster.event.network.NetworkConnectionEvent;
import com.roomster.fragment.AboutFragment;
import com.roomster.manager.NetworkManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Shows about screen in a separate activity.
 */
public class AboutActivity extends AppCompatActivity {

    @Bind(R.id.tv_title)
    TextView mTitle;

    @Bind(R.id.no_network_connection)
    RelativeLayout mNoNetworkConnectionView;

    @Inject
    NetworkManager mNetworkManager;

    @Inject
    Tracker mTracker;

    @Inject
    EventBus mEventBus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        setContentView(R.layout.activity_root);
        ButterKnife.bind(this);
        mEventBus.register(this);
        mTitle.setText(R.string.title_about);
        Intent intent = getIntent();
        Bundle bundle = new Bundle();
        bundle.putString(IntentExtras.EXTRA_ABOUT_URL, intent.getStringExtra(IntentExtras.EXTRA_ABOUT_URL));

        Fragment fragment = new AboutFragment();
        fragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction().replace(R.id.fl_content, fragment).commit();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mNetworkManager.hasNetworkAccess()) {
            hideNoNetworkConnection();
        } else {
            showNoNetworkConnection();
        }
        mTracker.setScreenName(getString(R.string.screen_name_about));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mEventBus.unregister(this);
    }


    @OnClick(R.id.iv_back)
    void onCloseClick() {
        onBackPressed();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NetworkConnectionEvent.NetworkConnectionAvailable event) {
        hideNoNetworkConnection();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NetworkConnectionEvent.NetworkConnectionUnavailable event) {
        showNoNetworkConnection();
    }


    private void showNoNetworkConnection() {
        mNoNetworkConnectionView.setVisibility(View.VISIBLE);
    }


    private void hideNoNetworkConnection() {
        mNoNetworkConnectionView.setVisibility(View.GONE);
    }
}