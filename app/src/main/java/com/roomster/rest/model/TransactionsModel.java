package com.roomster.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;


/**
 * Created by Bogoi.Bogdanov on 4/20/2016.
 */
public class TransactionsModel {

    public static final String TYPE_CANCELLATION = "Cancellation";

    public static final String STATUS_SUCCESS          = "Success";
    public static final String STATUS_APPROVED         = "Approved";
    public static final String STATUS_ERROR            = "Error";
    public static final String STATUS_DECLINED         = "Declined";
    public static final String STATUS_UNSUPPORTED_CARD = "Unsupported Card";

    @SerializedName("date")
    private Date date = null;

    @SerializedName("comment")
    private String comment = null;

    @SerializedName("by_user")
    private Boolean byUser = null;

    @SerializedName("product_name")
    private String productName = null;

    @SerializedName("amount")
    private Double amount = null;

    @SerializedName("currency")
    private String currency = null;

    @SerializedName("status")
    private String status = null;

    @SerializedName("type")
    private String type = null;


    @SerializedName("payment_method")
    private String payment_method = null;

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Boolean getByUser() {
        return byUser;
    }

    public void setByUser(Boolean by_user) {
        this.byUser = by_user;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPaymentMethod() {
        return payment_method;
    }

    public void setPaymentMethod(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String product_name) {
        this.productName = product_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
