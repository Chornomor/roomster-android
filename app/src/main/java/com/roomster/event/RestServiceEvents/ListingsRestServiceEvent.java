package com.roomster.event.RestServiceEvents;


import com.roomster.event.Event;
import com.roomster.rest.model.ListingGetViewModel;


/**
 * Created by "Michael Katkov" on 12/10/2015.
 */
public class ListingsRestServiceEvent extends Event {

    public static class AddListingSuccess extends ListingsRestServiceEvent {
        public long id;

        public AddListingSuccess() {
        }

        public AddListingSuccess(long id) {
            this.id = id;
        }
    }


    public static class AddListingFailed extends ListingsRestServiceEvent {

    }


    public static class EditListingSuccess extends ListingsRestServiceEvent {

    }


    public static class EditListingFailed extends ListingsRestServiceEvent {

    }


    public static class GetListingSuccess extends ListingsRestServiceEvent {

        public ListingGetViewModel listingGetViewModel;
    }


    public static class GetListingFailed extends ListingsRestServiceEvent {

    }
}
