package com.roomster.activity;


import com.google.android.gms.analytics.HitBuilders;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.adapter.MainViewPagerAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.IntentExtras;
import com.roomster.event.fragment.ConversationsListFragmentEvent;
import com.roomster.event.service.ConversationCounterEvent;
import com.roomster.fragment.ConversationsListFragment;
import com.roomster.fragment.DetailedViewPagerFragment;
import com.roomster.fragment.MapViewFragment;
import com.roomster.fragment.MenuFragment;
import com.roomster.fragment.SearchListingsListFragment;
import com.roomster.manager.DiscoveryPrefsManager;
import com.roomster.manager.FacebookManager;
import com.roomster.manager.MeManager;
import com.roomster.manager.SearchManager;
import com.roomster.manager.SettingsManager;
import com.roomster.model.SearchCriteria;
import com.roomster.rest.service.SearchRestService;
import com.roomster.views.MainToolBarView;
import com.roomster.views.NonSwipableViewPager;
import com.roomster.views.ReplacableViewPager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends BaseActivity {

    public static final String START_ADD_LISTING = "start_add_listing";
    private static final String IS_LOCALE_CHANGE = "isLocaleChange";
    private static final int FRAGMENT_MENU_INDEX = 0;
    private static final int FRAGMENT_MAIN_INDEX = 1;
    private static final int FRAGMENT_CONVERSATIONS_INDEX = 2;

    private MainViewPagerAdapter mPagerAdapter;
    List<Fragment> fragments;

    private String currentCurrency;
    private int mCurrentPage = FRAGMENT_MAIN_INDEX;
    private SearchCriteria mLastSearchCriteria;
    private String mLastViewType;
    private Typeface tutorialTypeface;

    @Bind(R.id.view_pager)
    NonSwipableViewPager mViewPager;

    @Bind(R.id.main_toolbar)
    MainToolBarView mToolBarView;

    @Bind(R.id.tutorial_layout)
    LinearLayout tutorialLayout;

    @Bind(R.id.btn_tutorial_ok)
    Button btnTutorialOk;

    @Bind(R.id.tv_tutorial_settings)
    TextView tvSettings;

    @Bind(R.id.tv_tutorial_message)
    TextView tvMessage;

    @Bind(R.id.tv_tutorial_bookmark)
    TextView tvBookmark;

    @Bind(R.id.tv_tutorial_swipe)
    TextView tvSwipe;

    @Bind(R.id.tv_tutorial_choose)
    TextView tvChoose;

    @Bind(R.id.tv_tutorial_contact_msg)
    TextView tvContactMessage;

    @Bind(R.id.tv_tutorial_contact_sms)
    TextView tvContactSms;

    @Bind(R.id.tv_tutorial_contact_call)
    TextView tvContactCall;

    @Bind(R.id.tv_tutorial_contact_social)
    TextView tvContactSocial;

    @Bind(R.id.tv_tutorial_contact_other)
    TextView tvContactOther;

    @Inject
    DiscoveryPrefsManager mDPrefsManager;

    @Inject
    MeManager mMeManager;

    @Inject
    FacebookManager mFacebookManager;

    @Inject
    SearchManager mSearchManager;

    @Inject
    SearchRestService mSearchRestService;

    @Inject
    SettingsManager mSettingsManager;

    DetailedViewPagerFragment detailedViewPagerFragment;

    @Override
    protected int getMainContentLayout() {
        return R.layout.activity_main;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        RoomsterApplication.getRoomsterComponent().inject(this);
        showOrNotAddNewListing();
    }


    @Override
    protected void onResume() {
        super.onResume();
        updateCurrentBudget();
        initViewPager();
        mToolBarView.setViewPager(mViewPager);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    public void onBackPressed() {
        if (mViewPager.getCurrentItem() != FRAGMENT_MAIN_INDEX) {
            mViewPager.setCurrentItem(FRAGMENT_MAIN_INDEX);
        } else {
            super.onBackPressed();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ConversationCounterEvent.ConversationCounterUpdate event) {
        mToolBarView.showUnreadMessages(event.inboxCounter.getCountNew() > 0);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ConversationsListFragmentEvent.NoUnreadMessages event) {
        mToolBarView.showUnreadMessages(false);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ConversationsListFragmentEvent.UnreadMessages event) {
        mToolBarView.showUnreadMessages(true);
    }

    private void showOrNotAddNewListing() {
        boolean shouldAddListing = getIntent().getBooleanExtra(START_ADD_LISTING, false);
        if (shouldAddListing) {
            Intent addNewListingIntent = new Intent(this, AddNewListingActivity.class);
            addNewListingIntent.putExtra(IntentExtras.WITH_BACK_ARROW, false);
            startActivity(addNewListingIntent);
        }
    }


    private void initViewPager() {
        if (isNeedInitViewPager()) {
            mLastViewType = mDPrefsManager.getViewType();
            mLastSearchCriteria = mDPrefsManager.getSearchCriteria();
            mCurrentPage = getCurrentPage();
            cleanPager();
            mPagerAdapter = new MainViewPagerAdapter(getSupportFragmentManager(), initFragments());
            mViewPager.setAdapter(mPagerAdapter);
            mViewPager.addOnPageChangeListener(mPageChangeListener);
            initViewPagerPaging();
            mViewPager.setCurrentItem(mCurrentPage);

            if (!mDPrefsManager.isTutorialWasShowed()) {
                showTutorialView();
            }
        } else {
            updateDetailedViewBookmarks();
        }
    }

    private int getCurrentPage(){
        if (mSettingsManager.isLocaleChange()) {
            mSettingsManager.refreshCurrentLocale();
            return FRAGMENT_MENU_INDEX;
        } else {
            return FRAGMENT_MAIN_INDEX;
        }
    }


    private void updateDetailedViewBookmarks() {
        if ((fragments.get(FRAGMENT_MAIN_INDEX) instanceof DetailedViewPagerFragment) && (mSearchManager.listUnbookmarkedId.size() > 0)) {
            if (((DetailedViewPagerFragment) fragments.get(FRAGMENT_MAIN_INDEX)).mPagerAdapter != null) {
                ((DetailedViewPagerFragment) fragments.get(FRAGMENT_MAIN_INDEX)).mPagerAdapter.clearUnbookmarkedBookmarks();
            }
        }
    }


    private void updateCurrentBudget() {
        if (isNeedUpdateBudget()) {
            currentCurrency = mSettingsManager.getCurrency();
            mSearchRestService.updateCurrentBudget(null, null, currentCurrency, mSettingsManager.getLocale());
        }
    }


    private boolean isNeedUpdateBudget() {
        return currentCurrency == null || !currentCurrency.equals(mSettingsManager.getCurrency());
    }


    private boolean isNeedInitViewPager() {
        boolean needInit = false;
        if ((!mDPrefsManager.getSearchCriteria().equals(mLastSearchCriteria) ||
                !mDPrefsManager.getViewType().equals(mLastViewType))) {
            needInit = true;
        }
        return needInit;
    }


    private List<Fragment> initFragments() {
        fragments = new ArrayList<>();
        fragments.add(new MenuFragment());
        fragments.add(initSearchFragment());
        fragments.add(new ConversationsListFragment());
        return fragments;
    }


    private void cleanPager() {
        if (mPagerAdapter != null) {
            mPagerAdapter.clearAll(getSupportFragmentManager());
            mViewPager.removeAllViews();
            mViewPager.setAdapter(null);
        }
    }


    private Fragment initSearchFragment() {
        String viewType = mDPrefsManager.getViewType();
        if (viewType.equals(DiscoveryPrefsManager.DETAILED)) {
            detailedViewPagerFragment = new DetailedViewPagerFragment().setType(false)
            .setIsUser(false);
            return detailedViewPagerFragment;
        } else if (viewType.equals(DiscoveryPrefsManager.LIST)) {
            return new SearchListingsListFragment();
        } else if (viewType.equals(DiscoveryPrefsManager.MAP)) {
            return new MapViewFragment();
        }
        return new DetailedViewPagerFragment();
    }


    private void initViewPagerPaging() {
        String viewType = mDPrefsManager.getViewType();
        if (viewType.equals(DiscoveryPrefsManager.DETAILED)) {
            mViewPager.setPagingEnabled(false);
        } else if (viewType.equals(DiscoveryPrefsManager.LIST)) {
            mViewPager.setPagingEnabled(true);
        } else if (viewType.equals(DiscoveryPrefsManager.MAP)) {
            mViewPager.setPagingEnabled(true);
        }
    }


    private ReplacableViewPager.SimpleOnPageChangeListener mPageChangeListener = new ReplacableViewPager.SimpleOnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            mCurrentPage = position;
            String screenName = null;
            switch (mCurrentPage) {
                case FRAGMENT_MENU_INDEX:
                    screenName = getString(R.string.screen_name_main_menu_screen);
                    break;
                case FRAGMENT_MAIN_INDEX:
                    String viewType = mDPrefsManager.getViewType();
                    switch (viewType) {
                        case DiscoveryPrefsManager.DETAILED:
                            screenName = getString(R.string.screen_name_listings_detailed);
                            break;
                        case DiscoveryPrefsManager.LIST:
                            screenName = getString(R.string.screen_name_listings_list);
                            break;
                        case DiscoveryPrefsManager.MAP:
                            screenName = getString(R.string.screen_name_listings_map);
                            break;
                    }
                    break;
                case FRAGMENT_CONVERSATIONS_INDEX:
                    screenName = getString(R.string.screen_name_conversations_list);
                    break;
                default:
                    break;
            }
            mTracker.setScreenName(screenName);
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        }
    };


    @OnClick(R.id.btn_tutorial_ok)
    public void onBtnGotIt() {
        mDPrefsManager.storeTutorialWasShowed(true);
        tutorialLayout.setVisibility(View.GONE);
        if (detailedViewPagerFragment != null) {
            detailedViewPagerFragment.setSocialBtnsVisibility(true);
        }
    }


    private void showTutorialView() {
        implementTutorialTypeface();
        tutorialLayout.setVisibility(View.VISIBLE);
        if (detailedViewPagerFragment != null) {
            detailedViewPagerFragment.setSocialBtnsVisibility(false);
        }
    }


    private void implementTutorialTypeface() {
        initTypeFace();
        tvSettings.setTypeface(tutorialTypeface);
        tvBookmark.setTypeface(tutorialTypeface);
        tvMessage.setTypeface(tutorialTypeface);
        tvSwipe.setTypeface(tutorialTypeface);
        tvChoose.setTypeface(tutorialTypeface);
    }


    /*we exclude implement typeface on language
    * that contains not latin characters*/
    private void initTypeFace() {
        Resources resources = getApplicationContext().getResources();
        String lang = resources.getConfiguration().locale.getLanguage();
        if (!lang.equals("el") && !lang.equals("pl") && !lang.equals("cs") && !lang.equals("tr")) {
            tutorialTypeface = Typeface.createFromAsset(getAssets(), "fonts/mathilde.otf");
        }
    }
}