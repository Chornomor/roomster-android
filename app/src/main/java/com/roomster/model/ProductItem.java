package com.roomster.model;


import com.google.gson.annotations.SerializedName;


public class ProductItem {

    @SerializedName("productId")
    private String id;

    @SerializedName("price")
    private String formattedPrice;

    @SerializedName("type")
    private String type;

    @SerializedName("price_amount_micros")
    private long priceMicros;

    @SerializedName("price_currency_code")
    private String currencyCode;

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getFormattedPrice() {
        return formattedPrice;
    }


    public void setFormattedPrice(String formattedPrice) {
        this.formattedPrice = formattedPrice;
    }


    public long getPriceMicros() {
        return priceMicros;
    }


    public void setPriceMicros(long priceMicros) {
        this.priceMicros = priceMicros;
    }


    public String getCurrencyCode() {
        return currencyCode;
    }


    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }


    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }
}
