package com.roomster.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.ListingType;
import com.roomster.manager.CatalogsManager;
import com.roomster.manager.SearchManager;
import com.roomster.model.CatalogModel;
import com.roomster.rest.model.ResultItemModel;
import com.roomster.utils.DateUtil;
import com.roomster.utils.ImageHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.inject.Inject;


/**
 * Created by Bogoi.Bogdanov on 12/1/2015.
 */
public class ListingListAdapter extends RecyclerView.Adapter<ListingListAdapter.ViewHolder> {

    private List<ResultItemModel>   mItems;
    private Context                 mContext;
    private OnListingClickListener  mOnListingClickListener;
    private OnBookmarkClickListener mOnBookmarkClickListener;
    private boolean                 mShouldShowProfileImage;

    @Inject
    CatalogsManager mCatalogsManager;


    public interface OnListingClickListener {

        void onClick(int index, ResultItemModel model);
    }


    public interface OnBookmarkClickListener {

        void onClick(int index, ResultItemModel model);
    }


    public ListingListAdapter(Context context, List<ResultItemModel> items, boolean shouldShowProfileImage) {
        RoomsterApplication.getRoomsterComponent().inject(this);
        mContext = context;
        mShouldShowProfileImage = shouldShowProfileImage;
        if (items != null) {
            mItems = items;
        } else {
            mItems = new ArrayList<>();
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_listing, parent, false);

        return new ViewHolder(root);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ResultItemModel listItem = mItems.get(position);

        ImageHelper.loadImageInto(mContext, listItem.getListing().getImages().get(0), holder.apartmentImageView);

        if (mShouldShowProfileImage) {
            String userPhoto = listItem.getUser().getTitleImage();
            ImageHelper.loadBorderedCircledImageInto(mContext, userPhoto, holder.personProfileImage);
        } else {
            holder.personProfileImage.setVisibility(View.GONE);
        }

        setLocation(listItem, holder);
        setHeadline(listItem, holder);
        setBookmark(listItem, holder);
        setDate(listItem, holder);
        setPriceAndCurrency(listItem, holder);
        setNameAndAge(listItem, holder);
        setGender(listItem, holder);
        setListingType(listItem, holder);

        View.OnClickListener wrapperClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mOnListingClickListener != null) {
                    mOnListingClickListener.onClick(position, listItem);
                }
            }
        };
        holder.itemView.setOnClickListener(wrapperClickListener);

        View.OnClickListener bookmarkClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mOnBookmarkClickListener != null) {
                    mOnBookmarkClickListener.onClick(position, listItem);
                }
            }
        };
        holder.bookmarkImageView.setOnClickListener(bookmarkClickListener);
    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }


    private void setListingType(ResultItemModel listItem, ViewHolder holder) {
        CatalogModel serviceTypeModel = mCatalogsManager.getServiceTypeById(listItem.getListing().getServiceType());
        if (serviceTypeModel != null) {
            switch (serviceTypeModel.getName()) {
                case ListingType.NEED_ROOM:
                    holder.listingType.setText(mContext.getString(R.string.al_looking_for_room));
                    break;
                case ListingType.NEED_APARTMENT:
                    holder.listingType.setText(mContext.getString(R.string.al_looking_for_apartment));
                    break;
                case ListingType.HAVE_SHARE:
                    holder.listingType.setText(mContext.getString(R.string.al_offering_room));
                    break;
                case ListingType.HAVE_APARTMENT:
                    holder.listingType.setText(mContext.getString(R.string.al_offering_apartment));
                    break;
                default:
                    break;
            }
        }
    }


    private void setHeadline(ResultItemModel listItem, ViewHolder holder) {
        String headline = listItem.getListing().getHeadlineActual();

        if (headline != null && !headline.isEmpty()) {
            holder.descriptionTextView.setText(headline);
        }
    }


    private void setLocation(ResultItemModel item, ViewHolder holder) {
        if (item.getListing() != null && item.getListing().getGeoLocation() != null && item.getListing().getGeoLocation()
          .getFullAddress() != null) {
            holder.headlineTextView.setText(item.getListing().getGeoLocation().getGoogleFormattedAddress());
        }
    }


    public void setBookmarked(long listingId) {
        if (mItems != null && !mItems.isEmpty()) {
            for (ResultItemModel item : mItems) {
                if (item.getListing().getListingId() == listingId) {
                    item.getListing().setIsBookmarked(true);
                    notifyDataSetChanged();
                    break;
                }
            }
        }
    }


    public void setUnbookmarked(long listingId) {
        if (mItems != null && !mItems.isEmpty()) {
            for (ResultItemModel item : mItems) {
                if (item.getListing().getListingId() == listingId) {
                    item.getListing().setIsBookmarked(false);
                    notifyDataSetChanged();
                    break;
                }
            }
        }
    }


    public void setOnListingClickListener(OnListingClickListener listingClickListener) {
        mOnListingClickListener = listingClickListener;
    }


    public void setOnBookmarkClickListener(OnBookmarkClickListener bookmarkClickListener) {
        mOnBookmarkClickListener = bookmarkClickListener;
    }


    public void updateItems(List<ResultItemModel> newItems) {
        if (newItems != null && !newItems.isEmpty()) {
            mItems.clear();
            mItems.addAll(0, newItems);
            notifyDataSetChanged();
        }
    }


    public ResultItemModel getItem(int position) {
        if (mItems.size() > position) {
            return mItems.get(position);
        } else {
            return null;
        }
    }


    public List<ResultItemModel> getItems() {
        return new ArrayList<>(mItems);
    }


    private void setBookmark(ResultItemModel item, ViewHolder holder) {
        if (item.getListing().getIsBookmarked()) {
            holder.bookmarkImageView.setImageResource(R.drawable.ic_bookmark_light_active);
        } else {
            holder.bookmarkImageView.setImageResource(R.drawable.ic_bookmark_light);
        }
    }


    private void setDate(ResultItemModel item, ViewHolder holder) {
            String date = DateUtil.getRelativeTimeSpanString(mContext, item.getListing().getRefreshed());
            holder.listingDateTextView.setText(date);
    }


    private void setNameAndAge(ResultItemModel item, ViewHolder holder) {
        String nameAndAge = item.getUser().getFirstName() + ", " + item.getUser().getAge();
        holder.personInfoTextView.setText(nameAndAge);
    }


    private void setGender(ResultItemModel item, ViewHolder holder) {
        if (item.getUser() != null && item.getUser().getGender() != null) {
            switch (item.getUser().getGender()) {
                case Male:
                    holder.personGenderImageView.setImageResource(R.drawable.ic_male);
                    break;
                case Female:
                    holder.personGenderImageView.setImageResource(R.drawable.ic_female_small);
                    break;
                default:
                    break;
            }
        }
    }


    private void setPriceAndCurrency(ResultItemModel item, ViewHolder holder) {
        if (item.getListing().getRates() != null && item.getListing().getRates().getRequestedMonthlyRate() != null) {
            // Set price
            holder.priceTextView.setText(String.valueOf(item.getListing().getRates().getRequestedMonthlyRate()));
            // Set currency
            String currencyCode = item.getListing().getRates().getRequestedCurrency();
            holder.currencyCodeTextView.setText(currencyCode);
            String currencySymbol = Currency.getInstance(currencyCode).getSymbol();
            if (currencySymbol != null && !currencySymbol.isEmpty()) {
                // use just the last char of the symbol (US$ -> $)
                holder.currencySignTextView.setText(Character.toString(currencySymbol.charAt(currencySymbol.length() - 1)));
            }
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView apartmentImageView;
        TextView  personInfoTextView;
        ImageView personProfileImage;
        TextView  listingType;
        ImageView personGenderImageView;
        TextView  descriptionTextView;
        TextView  headlineTextView;
        TextView  priceTextView;
        TextView  currencySignTextView;
        TextView  currencyCodeTextView;
        TextView  listingDateTextView;
        ImageView bookmarkImageView;


        public ViewHolder(View root) {
            super(root);
            apartmentImageView = (ImageView) root.findViewById(R.id.list_item_listing_apartment_image);
            personInfoTextView = (TextView) root.findViewById(R.id.user_name);
            personProfileImage = (ImageView) root.findViewById(R.id.list_item_person_image);
            listingType = (TextView) root.findViewById(R.id.listing_type);
            personGenderImageView = (ImageView) root.findViewById(R.id.list_item_listing_person_gender);
            descriptionTextView = (TextView) root.findViewById(R.id.listing_description_text);
            headlineTextView = (TextView) root.findViewById(R.id.listing_headline_text);
            priceTextView = (TextView) root.findViewById(R.id.price_text_view);
            currencySignTextView = (TextView) root.findViewById(R.id.currency_sign_text_view);
            currencyCodeTextView = (TextView) root.findViewById(R.id.currency_text_view);
            listingDateTextView = (TextView) root.findViewById(R.id.list_item_listing_date);
            bookmarkImageView = (ImageView) root.findViewById(R.id.list_item_listing_bookmark_icon);
        }


        @Override
        public void onClick(View v) {

        }
    }
}
