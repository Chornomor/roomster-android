package com.roomster.activity;


import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.IntentExtras;
import com.roomster.fragment.MyListingListFragment;
import com.roomster.manager.MeManager;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class UserListingsListActivity extends BaseActivity {

    private MyListingListFragment mMyListingListFragment;

    @Bind(R.id.my_listing_list_toolbar)
    Toolbar mToolbar;

    @Bind(R.id.my_listing_list_back)
    ImageView mBackBtn;

    @Bind(R.id.my_listing_list_title)
    TextView mTitleTv;

    @Inject
    MeManager mMeManager;

    @Inject
    Tracker mTracker;


    @Override
    protected int getMainContentLayout() {
        return R.layout.activity_my_listing_list;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        ButterKnife.bind(this);
        initView();

    }


    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName(getString(R.string.screen_name_my_listings_list));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        setTitleBarTotalListing();

    }


    @OnClick(R.id.my_listing_list_back)
    void onToolbarBackClick() {
        if(!isFinishing()) {
            onBackPressed();
        }
    }


    @OnClick(R.id.my_listing_list_add)
    void onToolbarAddClick() {
        startAddNewListingActivity();
    }


    public void setContent(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.my_listing_list_content, fragment).commitAllowingStateLoss();
    }


    private void initView() {
        setSupportActionBar(mToolbar);
        mMyListingListFragment = new MyListingListFragment();
        mMyListingListFragment.setTitle(mTitleTv);
        setContent(mMyListingListFragment);
    }


    private void setTitleBarTotalListing() {
        if (mMeManager.getMyListings() != null && mTitleTv != null) {
            String totalListingsAdd = "(" + mMeManager.getMyListings().size() + ")";
            String stringToFill = getString(R.string.my_listings) + " " + totalListingsAdd;
            mTitleTv.setText(stringToFill);
        }
    }


    private void startAddNewListingActivity() {
        Intent addNewListingIntent = new Intent(this, AddNewListingActivity.class);
        addNewListingIntent.putExtra(IntentExtras.WITH_BACK_ARROW, true);
        startActivity(addNewListingIntent);
    }
}