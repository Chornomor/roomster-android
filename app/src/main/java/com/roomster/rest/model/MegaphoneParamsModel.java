package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import com.roomster.enums.ServiceTypeEnum;


public class MegaphoneParamsModel {

    @SerializedName("service_type")
    private ServiceTypeEnum serviceType = null;


    @SerializedName("budget")
    private RangeModel budget = null;

    @SerializedName("age")
    private RangeModel age = null;

    /**
     * CSV list of ids from /v1/catalogues/sex_category
     **/
    @SerializedName("sex")
    private String sex = null;

    /**
     * CSV list of ids from /v1/catalogues/bedrooms
     **/
    @SerializedName("bedrooms")
    private String bedrooms = null;

    /**
     * Message to broadcast
     **/
    @SerializedName("message")
    private String message = null;


    public ServiceTypeEnum getServiceType() {
        return serviceType;
    }


    public void setServiceType(ServiceTypeEnum serviceType) {
        this.serviceType = serviceType;
    }


    public RangeModel getBudget() {
        return budget;
    }


    public void setBudget(RangeModel budget) {
        this.budget = budget;
    }


    public RangeModel getAge() {
        return age;
    }


    public void setAge(RangeModel age) {
        this.age = age;
    }


    public String getSex() {
        return sex;
    }


    public void setSex(String sex) {
        this.sex = sex;
    }


    public String getBedrooms() {
        return bedrooms;
    }


    public void setBedrooms(String bedrooms) {
        this.bedrooms = bedrooms;
    }


    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class MegaphoneParamsModel {\n");

        sb.append("  serviceType: ").append(serviceType).append("\n");
        sb.append("  budget: ").append(budget).append("\n");
        sb.append("  age: ").append(age).append("\n");
        sb.append("  sex: ").append(sex).append("\n");
        sb.append("  bedrooms: ").append(bedrooms).append("\n");
        sb.append("  message: ").append(message).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}