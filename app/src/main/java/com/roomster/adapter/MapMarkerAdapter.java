package com.roomster.adapter;


import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.enums.GenderEnum;
import com.roomster.rest.model.ResultItemModel;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MapMarkerAdapter implements GoogleMap.InfoWindowAdapter {

    private static final String NEW_LISTING = "New Listing";

    @Inject
    LayoutInflater mLayoutInflater;

    private ResultItemModel mResultItemModel;


    public MapMarkerAdapter() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }


    @Override
    public View getInfoContents(Marker marker) {
        if (mResultItemModel != null) {
            View view = mLayoutInflater.inflate(R.layout.map_info_window_layout, null);
            ViewHolder viewHolder = new ViewHolder(view);
            setName(viewHolder);
            setGender(viewHolder);
            setDescription(viewHolder);
            setCurrency(viewHolder);
            setPrice(viewHolder);
            setNew(viewHolder);
            return view;
        } else {
            return null;
        }
    }


    public void setModel(ResultItemModel resultItemModel) {
        mResultItemModel = resultItemModel;
    }


    private void setName(ViewHolder viewHolder) {
        String nameAndAge = mResultItemModel.getUser().getFirstName() + "," + mResultItemModel.getUser().getAge();
        viewHolder.mNameTv.setText(nameAndAge);
    }


    private void setGender(ViewHolder viewHolder) {
        if (mResultItemModel.getUser().getGender() != null && mResultItemModel.getUser().getGender().equals(GenderEnum.Male)) {
            viewHolder.mGenderIm.setImageResource(R.drawable.ic_male);
        } else {
            viewHolder.mGenderIm.setImageResource(R.drawable.ic_female);
        }
    }


    private void setDescription(ViewHolder viewHolder) {
        if (mResultItemModel.getListing() != null) {
            String description = mResultItemModel.getListing().getDescriptionActual();
            if (description == null || description.isEmpty()) {
                description = mResultItemModel.getListing().getDescription();
            }

            if (description != null && !description.isEmpty()) {
                viewHolder.mInfoSummaryTv.setText(description);
            } else {
                viewHolder.mInfoSummaryTv.setVisibility(View.GONE);
            }
        }
    }


    private void setCurrency(ViewHolder viewHolder) {
        if (mResultItemModel.getListing().getRates() != null && mResultItemModel.getListing().getRates().getCurrency() != null) {
            viewHolder.mCurrencyText.setText(mResultItemModel.getListing().getRates().getCurrency());
        } else {
            viewHolder.mCurrencyText.setVisibility(View.GONE);
        }
    }


    private void setPrice(ViewHolder viewHolder) {
        if (mResultItemModel.getListing().getRates() != null && mResultItemModel.getListing().getRates()
          .getMonthlyRate() != null) {
            viewHolder.mPrice.setText(String.valueOf(mResultItemModel.getListing().getRates().getMonthlyRate()));
        } else {
            viewHolder.mPrice.setVisibility(View.GONE);
        }
    }


    private void setNew(ViewHolder viewHolder) {
        if (mResultItemModel.getListing().getIsNew()) {
            viewHolder.mNewText.setVisibility(View.VISIBLE);
        } else {
            viewHolder.mNewText.setVisibility(View.GONE);
        }
    }


    static class ViewHolder {

        @Bind(R.id.map_info_name)
        TextView mNameTv;

        @Bind(R.id.map_info_gender)
        ImageView mGenderIm;

        @Bind(R.id.map_info_summary_text)
        TextView mInfoSummaryTv;

        @Bind(R.id.map_info_currency_sign)
        TextView mCurrencySign;

        @Bind(R.id.map_info_price)
        TextView mPrice;

        @Bind(R.id.map_info_currency_text)
        TextView mCurrencyText;

        @Bind(R.id.map_info_period_text)
        TextView mPeriodText;

        @Bind(R.id.map_info_new_text)
        TextView mNewText;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
