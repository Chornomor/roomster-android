package com.roomster.utils;


/**
 * Created by Emil Atanasov on 9/9/15.
 * <p/>
 * Email: emil@alttab.co
 * <p/>
 * Project: Roomster-Android
 * <p/>
 * Copyright 2015 (c)
 */
public class Keys {

    //JSON
    public static final String DATA          = "data";
    public static final String PICTURE       = "picture";
    public static final String IS_SILHOUETTE = "is_silhouette";
    public static final String URL           = "url";
    public static final String LAT           = "lat";
    public static final String LON           = "lon";
}
