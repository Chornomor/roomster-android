package com.roomster.rest.service;


import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.SettingsManager;
import com.roomster.manager.TokenManager;
import com.roomster.rest.api.PackagesApi;
import com.roomster.rest.model.PackageViewModel;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UpgradeAccountRestService {

    @Inject
    TokenManager mTokenManager;

    @Inject
    SettingsManager mSettingsManager;

    @Inject
    PackagesApi mPackagesApi;

    @Inject
    NetworkManager mNetworkManager;


    public UpgradeAccountRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void getUpgradePackages(final RestServiceListener<List<PackageViewModel>> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callGetUpgradePackages(result, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                if (listener != null) {
                    listener.onFailure(code, throwable, msg);
                }
            }
        });
    }


    private void callGetUpgradePackages(String authToken, final RestServiceListener<List<PackageViewModel>> listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<List<PackageViewModel>> callGetPackages = mPackagesApi
              .packagesGet(authToken, mSettingsManager.getCurrency(), mSettingsManager.getLocale());
            callGetPackages.enqueue(new Callback<List<PackageViewModel>>() {

                @Override
                public void onResponse(Call<List<PackageViewModel>> call, Response<List<PackageViewModel>> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            if (listener != null) {
                                listener.onSuccess(response.body());
                            }
                            break;
                        default:
                            if (listener != null) {
                                listener.onFailure(response.code(), null, response.message());
                            }
                    }
                }


                @Override
                public void onFailure(Call<List<PackageViewModel>> call, Throwable t) {
                    if (listener != null) {
                        listener.onFailure(-1, t, RoomsterApplication.context.getString(R.string.unable_to_make_get_packages_call));
                    }
                }
            });
        } else {
            if (listener != null) {
                listener.onFailure(-1, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
            }
        }
    }
}
