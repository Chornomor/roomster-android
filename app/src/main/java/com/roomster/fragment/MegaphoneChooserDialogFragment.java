package com.roomster.fragment;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.*;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.ListingType;
import com.roomster.event.dialog.MegaphoneChooserDialogEvent;
import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;


public class MegaphoneChooserDialogFragment extends DialogFragment {

    @Inject
    EventBus mEventBus;

    @Bind(R.id.tv_megaphone_short_message_1)
    TextView message_1;

    @Bind(R.id.tv_megaphone_short_message_2)
    TextView message_2;

    @Bind(R.id.tv_megaphone_short_message_3)
    TextView message_3;

    private int message_type = -1;

    private int mRadioButtonState;

    public static MegaphoneChooserDialogFragment getMegaphoneChooserDialogFragment(int radioButtonState) {
        MegaphoneChooserDialogFragment dialodFragment = new MegaphoneChooserDialogFragment();
        dialodFragment.mRadioButtonState = radioButtonState;
        return  dialodFragment;
    }

    @Override
    public void onStart() {
        super.onStart();

        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.90f;
        windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(windowParams);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        // Pick a style based on the num.
        int style = DialogFragment.STYLE_NO_FRAME, theme = 0;
        setStyle(style, theme);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setCanceledOnTouchOutside(true);
        View v = inflater.inflate(R.layout.fragment_dialog_short_message, container, false);
        ButterKnife.bind(this, v);
        getDialog().setTitle(R.string.megaphone_use_short_messages);

        fillMessages();
        return v;
    }

    private void fillMessages() {
        switch (mRadioButtonState){
            case ListingType.FINDING_ROOM_TYPE:
                message_1.setText(getString(R.string.megaphone_short_message_1_find_room));
                message_2.setText(getString(R.string.megaphone_short_message_2_find_room));
                message_3.setText(getString(R.string.megaphone_short_message_3_find));
                break;
            case ListingType.OFFERING_ROOM_TYPE:
                message_1.setText(getString(R.string.megaphone_short_message_1_offer_room));
                message_2.setText(getString(R.string.megaphone_short_message_2_offer_room));
                message_3.setText(getString(R.string.megaphone_short_message_3_offer));
                break;
            case ListingType.FINDING_ENTIRE_PLACE_TYPE:
                message_1.setText(getString(R.string.megaphone_short_message_1_find_place));
                message_2.setText(getString(R.string.megaphone_short_message_2_find_place));
                message_3.setText(getString(R.string.megaphone_short_message_3_find));
                break;
            case ListingType.OFFERING_ENTIRE_PLACE_TYPE:
                message_1.setText(getString(R.string.megaphone_short_message_1_offer_place));
                message_2.setText(getString(R.string.megaphone_short_message_2_offer_place));
                message_3.setText(getString(R.string.megaphone_short_message_3_offer));
                break;
            default:{}
        }
    }


    @OnClick(R.id.tv_megaphone_short_message_1)
    void onShortMessage1Clicked() {
        onShortMessageClicked(message_1.getText().toString());
    }


    @OnClick(R.id.tv_megaphone_short_message_2)
    void onShortMessage2Clicked() {
        onShortMessageClicked(message_2.getText().toString());
    }


    @OnClick(R.id.tv_megaphone_short_message_3)
    void onShortMessage3Clicked() {
        onShortMessageClicked(message_3.getText().toString());
    }


    private void onShortMessageClicked(String message) {
        mEventBus.post(new MegaphoneChooserDialogEvent(message));
        dismiss();
    }
}