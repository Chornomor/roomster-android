package com.roomster.constants;


public class RoomsterEndpoint {

    public static final String SHARE_LINK          = "https://getapp.roomster.com";
    public static final String BASE_URL_PRODUCTION = "https://api.roomster.com/v1/";
    public static final String BASE_URL_STAGING    = "https://stage-api.roomster.com:443";

}
