package com.roomster.rest.service;


import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.Nullable;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.TokenManager;
import com.roomster.rest.api.LocalizationSettingsApi;
import com.roomster.rest.model.LocalizationSettingsModel;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LocaleRestService {

    @Inject
    LocalizationSettingsApi mLocalizationSettingsApi;

    @Inject
    Context mContext;

    @Inject
    NetworkManager mNetworkManager;

    @Inject
    TokenManager mTokenManager;


    public LocaleRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void saveLocalizationSettings(final String locale, final RestServiceListener<LocalizationSettingsModel> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String token) {
                callPutLocalizationSettings(token, locale, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                if (listener != null) {
                    listener.onFailure(code, throwable, msg);
                }
            }
        });
    }


    private void callPutLocalizationSettings(String token, String locale
            , @Nullable final RestServiceListener<LocalizationSettingsModel> listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<LocalizationSettingsModel> callLocalizationSettings = mLocalizationSettingsApi
                    .localizationSettingsPut(locale, token);
            callLocalizationSettings.enqueue(new Callback<LocalizationSettingsModel>() {

                @Override
                public void onResponse(Call<LocalizationSettingsModel> call, Response<LocalizationSettingsModel> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            if (listener != null) {
                                listener.onSuccess(response.body());
                            }
                            break;
                        case ResponseCode.BAD_REQUEST:
                        case ResponseCode.SERVER_ERROR:
                        default:
                            if (listener != null) {
                                listener.onFailure(response.code(), null, null);
                            }
                    }
                }


                @Override
                public void onFailure(Call<LocalizationSettingsModel> call, Throwable t) {
                    if (listener != null) {
                        listener.onFailure(-1, t, null);
                    }
                }
            });
        } else {
            if (listener != null) {
                listener.onFailure(-1, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
            }
        }
    }


    public void getLocalizationSettings(final RestServiceListener<LocalizationSettingsModel> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String token) {
                callGetLocalizationSettings(token, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                if (listener != null) {
                    listener.onFailure(code, throwable, msg);
                }
            }
        });
    }


    public void callGetLocalizationSettings(final String token, final RestServiceListener<LocalizationSettingsModel> listener) {

        if (mNetworkManager.hasNetworkAccess()) {
            String countryCode = mContext.getResources().getConfiguration().locale.getCountry();

            Call<LocalizationSettingsModel> localizationSettingsModelCall;
            if (mTokenManager.getIsNewUser()) {
                localizationSettingsModelCall = mLocalizationSettingsApi.localizationSettingsGet(countryCode);
            } else {
                localizationSettingsModelCall = mLocalizationSettingsApi.localizationSettingsGet(token, countryCode);
            }
            localizationSettingsModelCall.enqueue(new Callback<LocalizationSettingsModel>() {

                @Override
                public void onResponse(Call<LocalizationSettingsModel> call, Response<LocalizationSettingsModel> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            if (mTokenManager.getIsNewUser()) {
                                callPutLocalizationSettings(token, response.body().getLocale(), null);
                            }
                            if (listener != null) {
                                listener.onSuccess(response.body());
                            }
                            break;
                        default:
                            if (listener != null) {
                                listener.onFailure(response.code(), null, null);
                            }
                    }
                }


                @Override
                public void onFailure(Call<LocalizationSettingsModel> call, Throwable t) {
                    if (listener != null) {
                        listener.onFailure(-1, t, RoomsterApplication.context.getString(R.string.settings_cant_get_rest_localization));
                    }
                }
            });
        } else {
            if (listener != null) {
                listener.onFailure(-1, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
            }
        }
    }

}