package com.roomster.event.fragment;


import com.roomster.event.Event;


/**
 * Created by Bogoi.Bogdanov on 11/20/2015.
 */
public class ConversationsListFragmentEvent {

    public static class UnreadMessages extends Event {

    }


    public static class NoUnreadMessages extends Event {

    }
}
