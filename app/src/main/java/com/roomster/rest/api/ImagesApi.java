package com.roomster.rest.api;


import com.roomster.rest.model.UploadedImageModel;

import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ImagesApi {

    /**
     * Delete image.
     *
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<Object>
     */

    @DELETE("Images/{id}")
    Call<ResponseBody> imagesDelete(@Path("id") Long id, @Query("image_entity_type") String imageEntityType,
                                    @Header("Authorization") String authorization);

    /**
     * Delete image.
     *
     * @param entityId
     *   listing id if image_entity_type == 'listing'
     * @param imageEntityType
     *   'user' or 'listing'
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @param file
     *   Image to upload
     * @return Call<Object>
     */

    @Multipart
    @POST("Images/")
    Call<List<UploadedImageModel>> imagePost(@Query("image_entity_type") String imageEntityType,
                                             @Query("entity_id") Long entityId, @Part("file") RequestBody file,
                                             @Header("Authorization") String authorization);
}
