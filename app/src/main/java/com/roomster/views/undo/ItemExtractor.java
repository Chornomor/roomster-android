package com.roomster.views.undo;

/**
 * Created by andreybofanov on 06.07.16.
 */
public class ItemExtractor {
    public int position;
    public Object object;

    public ItemExtractor(int position, Object object) {
        this.position = position;
        this.object = object;
    }
}
