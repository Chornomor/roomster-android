package com.roomster.rest.api;


import com.roomster.rest.model.HelpdeskViewModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;


public interface HelpdeskApi {

    /**
     * Get all helpdesk messages Returns a list of helpdesk messages and a counter of unread messages.
     *
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<HelpdeskViewModel>
     */

    @GET("Helpdesk")
    Call<HelpdeskViewModel> helpdeskGet(@Header("Authorization") String authorization);

    /**
     * Mark all helpdesk messages as read
     *
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<Object>
     */

    @PUT("Helpdesk")
    Call<Object> helpdeskPut(@Header("Authorization") String authorization);

    /**
     * Add new helpdesk message
     *
     * @param subject
     *   Message subject
     * @param body
     *   Message text
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<Object>
     */

    @POST("Helpdesk/")
    Call<String> helpdeskPost(@Query("subject") String subject, @Query("body") String body,
                              @Header("Authorization") String authorization);
}
