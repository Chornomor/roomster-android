package com.roomster.dialog;


import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.enums.ComplaintTypeEnum;
import com.roomster.rest.model.UserReportViewModel;


public class ReportUserDialog extends android.support.v7.app.AlertDialog {

    public interface SubmitReportListener {

        void onReportSubmitted(UserReportViewModel userReportViewModel);
    }


    private Spinner              mComplaintTypeSpinner;
    private EditText             mDescriptionEditText;
    private SubmitReportListener mSubmitReportListener;


    public ReportUserDialog(Context context, SubmitReportListener listener) {
        super(context);
        mSubmitReportListener = listener;

        initViews();
        initButtons();
        initSpinner();
        setTitle(R.string.report_user_dialog_title);
    }


    private void initViews() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.dialog_report_user, null);
        setView(contentView);
        mComplaintTypeSpinner = (Spinner) contentView.findViewById(R.id.dialog_report_user_complaint_type);
        mDescriptionEditText = (EditText) contentView.findViewById(R.id.dialog_report_user_description_text);
    }


    private void initSpinner() {
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<ComplaintTypeEnum> adapter = new ArrayAdapter<ComplaintTypeEnum>(getContext(),
          android.R.layout.simple_spinner_dropdown_item, ComplaintTypeEnum.values()) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return setComplaintTypeText(super.getView(position, convertView, parent), getItem(position));
            }


            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                return setComplaintTypeText(super.getDropDownView(position, convertView, parent), getItem(position));
            }


            private View setComplaintTypeText(View root, ComplaintTypeEnum type) {
                TextView textView = (TextView) root.findViewById(android.R.id.text1);
                if (textView != null) {
                    textView.setText(type.toStringRes());
                }
                return root;
            }
        };

        mComplaintTypeSpinner.setAdapter(adapter);
    }


    private void initButtons() {
        setButton(BUTTON_POSITIVE, getContext().getString(R.string.report_user_dialog_submit_button), new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mSubmitReportListener != null) {
                    UserReportViewModel reportViewModel = new UserReportViewModel();
                    reportViewModel.setComplaintType((ComplaintTypeEnum) mComplaintTypeSpinner.getSelectedItem());
                    reportViewModel.setDescription(mDescriptionEditText.getText().toString());
                    mSubmitReportListener.onReportSubmitted(reportViewModel);
                }
                mDescriptionEditText.setText("");
            }
        });

        setButton(BUTTON_NEGATIVE, getContext().getString(R.string.report_user_dialog_cancel_button), new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDescriptionEditText.setText("");
            }
        });
    }
}
