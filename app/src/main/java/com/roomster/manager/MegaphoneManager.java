package com.roomster.manager;

import com.roomster.application.RoomsterApplication;
import com.roomster.cache.sharedpreferences.SharedPreferencesFiles;
import com.roomster.constants.DiscoveryConstants;
import com.roomster.enums.ServiceTypeEnum;
import com.roomster.utils.StringsUtil;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

/**
 * Created by yuliasokolova on 23.09.16.
 */
public class MegaphoneManager {
    public static final  String MEGAPHONE_SEX_FIND                 = "megaphone_4_sex";
    public static final  String MEGAPHONE_SEX_RENT                 = "megaphone_3_sex";
    public static final  String MEGAPHONE_SEX_FIND_STR             = "megaphone_4_sex_str";
    public static final  String MEGAPHONE_SEX_RENT_STR             = "megaphone_3_sex_str";
    public static final  String APARTMENT_RENT                     = "megaphone_apartment_rent";
    public static final  String TYPE                               = "megaphone_type";
    public static final  String TYPE_FAIL                          = "megaphone_type_fail";
    public static final  int    TYPE_DEFAULT                       = -1;
    public static final  int    MEGAPHONE_SEX_DEFAULT              = -1;
    public static final  String MEGAPHONE_SEX_DEFAULT_STRING       = "";
    public static final  String APARTMENT_DEFAULT                  = "";
    public static final  String AGE_MIN                            = "megaphone_age_min";
    public static final  String AGE_MAX                            = "megaphone_age_max";
    public static final  int    DEFAULT_MIN_AGE                    = 18;
    public static final  int    DEFAULT_MAX_AGE                    = 99;
    public static final  String BUDGET_MAX                         = "megaphone_budget_max";
    public static final  String BUDGET_MIN                         = "megaphone_budget_min";
    public static final  int    DEFAULT_MAX_BUDGET                 = 10000;
    public static final  int    DEFAULT_MIN_BUDGET                 = 0;


    @Inject
    SharedPreferencesFiles mSharedPreferencesFiles;

    @Inject
    EventBus mEventBus;

    @Inject
    CatalogsManager mCatalogsManager;

    public MegaphoneManager() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }

    /** sex  */
    public void storeMegaphoneIntSexFind(int value) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putInt(MEGAPHONE_SEX_FIND, value).commit();
    }
    public int getMegaphoneIntSexFind() {
        return mSharedPreferencesFiles.getUserSharedPreferences().getInt(MEGAPHONE_SEX_FIND, MEGAPHONE_SEX_DEFAULT);
    }
    public void storeMegaphoneIntSexRent(int value) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putInt(MEGAPHONE_SEX_RENT, value).commit();
    }
    public int getMegaphoneIntSexRent() {
        return mSharedPreferencesFiles.getUserSharedPreferences().getInt(MEGAPHONE_SEX_RENT, MEGAPHONE_SEX_DEFAULT);
    }


    /** bedrooms  */
    public void storeBedApartment(String types) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putString(APARTMENT_RENT, types).commit();
    }
    public String getBedApartment() {
        return mSharedPreferencesFiles.getUserSharedPreferences().getString(APARTMENT_RENT, APARTMENT_DEFAULT);
    }

    /** search type  */
    public void saveType(int serviceType) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putInt(TYPE, serviceType).commit();
    }
    public int getType (){
        return mSharedPreferencesFiles.getUserSharedPreferences().getInt(TYPE, TYPE_DEFAULT);
    }

    /** age  */
    public void storeMegaphoneAgeMin (int valueMin){
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putInt(AGE_MIN, valueMin).commit();
    }
    public void storeMegaphoneAgeMax (int valueMax) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putInt(AGE_MAX, valueMax).commit();
    }
    public int getAgeMin(){
        return mSharedPreferencesFiles.getUserSharedPreferences().getInt(AGE_MIN, DEFAULT_MIN_AGE);
    }
    public int getAgeMax(){
        return mSharedPreferencesFiles.getUserSharedPreferences().getInt(AGE_MAX, DEFAULT_MAX_AGE);
    }

    /** budget  */
    public void storeMaxBudget( int value){
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putInt(BUDGET_MAX, value).commit();
    }
    public int getMaxBudget(){
        return mSharedPreferencesFiles.getUserSharedPreferences().getInt(BUDGET_MAX, DiscoveryConstants.MAX_BUDGET);
    }
    public void storeMinBudget( int value){
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putInt(BUDGET_MIN, value).commit();
    }
    public int getMinBudget(){
        return mSharedPreferencesFiles.getUserSharedPreferences().getInt(BUDGET_MIN, DEFAULT_MIN_BUDGET);
    }
}
