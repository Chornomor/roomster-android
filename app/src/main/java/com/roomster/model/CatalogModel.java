package com.roomster.model;


import com.google.gson.internal.LinkedTreeMap;


/**
 * Created by michaelkatkov on 12/8/15.
 */
public class CatalogModel {

    private int mId;

    private String mName;
    private String description;


    public CatalogModel(int id, String name) {
        mId = id;
        mName = name;
    }


    public CatalogModel(LinkedTreeMap<String, Object> linkedTreeMap) {
        mId = ((Double) linkedTreeMap.get("Id")).intValue();
        mName = (String) linkedTreeMap.get("Name");
        if(linkedTreeMap.containsKey("Description"))
        description = (String) linkedTreeMap.get("Description");
    }


    public int getId() {
        return mId;
    }


    public void setId(int id) {
        mId = id;
    }


    public String getName() {
        if(description != null)
            return description;
        return mName;
    }


    public void setName(String mName) {
        if(description != null){
            description = mName;
            return;
        }
        this.mName = mName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CatalogModel)) {
            return false;
        }

        CatalogModel that = (CatalogModel) o;

        if (mId != that.mId) {
            return false;
        }
        return !(mName != null ? !mName.equals(that.mName) : that.mName != null);
    }


    @Override
    public int hashCode() {
        int result = mId;
        result = 31 * result + (mName != null ? mName.hashCode() : 0);
        return result;
    }
}
