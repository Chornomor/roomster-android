package com.roomster.event.application;


import com.roomster.event.Event;


public class ApplicationEvent extends Event {

    public static class ShowLoading extends ApplicationEvent {

    }


    public static class HideLoading extends ApplicationEvent {

    }


    public static class HideLoadingSticky extends ApplicationEvent {

    }
}
