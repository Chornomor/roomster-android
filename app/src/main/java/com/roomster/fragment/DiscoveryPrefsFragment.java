package com.roomster.fragment;


import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.edmodo.rangebar.RangeBar;
import com.roomster.R;
import com.roomster.adapter.AmenitiesGridAdapter;
import com.roomster.adapter.PetsAdapter;
import com.roomster.adapter.PlaceAutocompleteAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.DiscoveryConstants;
import com.roomster.event.RestServiceEvents.CatalogRestServiceEvent;
import com.roomster.manager.CatalogsManager;
import com.roomster.manager.DiscoveryPrefsManager;
import com.roomster.manager.RoomsterLocationManager;
import com.roomster.manager.SearchManager;
import com.roomster.model.CatalogModel;
import com.roomster.model.CatalogValueModel;
import com.roomster.model.SearchCriteria;
import com.roomster.rest.service.CatalogRestService;
import com.roomster.rest.service.SearchRestService;
import com.roomster.utils.DateUtil;
import com.roomster.utils.LocationUtil;
import com.roomster.utils.PermissionsAware;
import com.roomster.utils.PermissionsHelper;
import com.roomster.utils.StringsUtil;
import com.roomster.views.CatalogModelRadioGroup;
import com.roomster.views.DoneAutoCompleteEditText;
import com.roomster.views.RectangleWithStroke;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by "Michael Katkov" on 11/3/2015.
 */
public class DiscoveryPrefsFragment extends Fragment implements RoomsterLocationManager.LocationCallbacks {

    private static final int COLUMNS           = 5;
    private int budgetRangeStep = 10;

    private final String STUDIO      = "0";
    private final String BEDROOM_1   = "1";
    private final String BEDROOMS_2  = "2";
    private final String BEDROOMS_3  = "3";
    private final String BEDROOMS_4  = "4";
    private final String BEDROOMS_5  = "5";
    private final String BATH_1      = "0";
    private final String BATH_1_5    = "1";
    private final String BATH_2      = "2";
    private final String BATH_2_5    = "3";
    private final String BATH_3      = "4";
    private final String BATH_3_5    = "5";

    private static final int    DATE_IN     = 1;
    private static final int    DATE_OUT    = 2;
    private static final String DATE_PICKER = "data_picker";

    private PlaceAutocompleteAdapter mPlaceAutocompleteAdapter;
    private AmenitiesGridAdapter     mAmenitiesAdapter;
    private PetsAdapter              mPetsAdapter;
    private Date                     mDateIn;
    private Date                     mDateOut;
    private int                      mBudgetRangeBarTickCount;
    private int                      mAgeRangeBarTickCount;

    private RoomsterLocationManager mRoomsterLocationManager;

    List<RectangleWithStroke> listBedroomType = new ArrayList<>();
    List<RectangleWithStroke> listBathroomType = new ArrayList<>();

    @Bind(R.id.global_discovery_pref_sv)
    ScrollView mScrollView;

    @Bind(R.id.dp_rg_view_type)
    RadioGroup mViewTypeRG;

    @Bind(R.id.dp_rb_detailed)
    RadioButton mDetailedRB;

    @Bind(R.id.dp_rb_list)
    RadioButton mListRB;

    @Bind(R.id.dp_rb_map)
    RadioButton mMapRB;

    @Bind(R.id.dp_rb_renting)
    RadioButton mRentingRb;

    @Bind(R.id.dp_rb_finding)
    RadioButton mFindingRb;

    @Bind(R.id.dp_rb_entire_place)
    RadioButton mEntirePlaceRb;

    @Bind(R.id.dp_rb_private_room)
    RadioButton mPrivateRoomRb;

    @Bind(R.id.dp_current_location_tv)
    DoneAutoCompleteEditText mCurrentLocationTv;

    @Bind(R.id.dp_location_pin)
    ImageView mLocationPin;

    @Bind(R.id.dp_radius_tv)
    TextView mRadiusText;

    @Bind(R.id.dp_radius_slider)
    SeekBar mRadiusSlider;

    @Bind(R.id.dp_budget_min_max)
    TextView mBudgetMinMaxTv;

    @Bind(R.id.dp_budget_rangebar)
    RangeBar mBudgetRangeBar;

    @Bind(R.id.dp_age_layout)
    RelativeLayout mAgeLayout;

    @Bind(R.id.dp_age_min_max)
    TextView mAgeMinMaxTv;

    @Bind(R.id.dp_age_rangebar)
    RangeBar mAgeRangeBar;

    @Bind(R.id.dp_bedrooms_layout)
    LinearLayout mBedroomLayout;

    @Bind(R.id.dp_bathrooms_layout)
    LinearLayout mBathroomLayout;

    @Bind(R.id.dp_household_sex_rg)
    CatalogModelRadioGroup mHouseholdSexRG;

    @Bind(R.id.dp_sex_3_rg)
    CatalogModelRadioGroup mSexRG;

    @Bind(R.id.dp_pets_layout)
    LinearLayout mPetsLayout;

    @Bind(R.id.dp_pets_tv)
    TextView tvPetsTitle;

    @Bind(R.id.dp_pets_grid)
    GridView mPetsGrid;

    @Bind(R.id.dp_amenities_tv)
    TextView mAmenitiesTitle;

    @Bind(R.id.dp_amenity_grid)
    GridView mApartmentAmenityGridView;

    @Bind(R.id.dp_grid_layout)
    LinearLayout mApartmentAmenityGridLayout;

    @Bind(R.id.dp_rg_sort_type)
    RadioGroup mSortType;

    @Bind(R.id.dp_rb_last_activity)
    RadioButton mLastActivityRb;

    @Bind(R.id.dp_rb_newest)
    RadioButton mNewestRb;

    @Bind(R.id.dp_rb_rent)
    RadioButton mRentRb;

    @Bind(R.id.dp_clear_button)
    ImageView mClearBtn;

    @Bind(R.id.dp_date_in_tv)
    TextView mDateInTv;

    @Bind(R.id.dp_date_out_tv)
    TextView mDateOutTv;

    @Bind(R.id.dp_date_in_clear)
    ImageView dateInClear;

    @Bind(R.id.dp_date_out_clear)
    ImageView dateOutClear;

    @Bind(R.id.cb_bedroom0)
    RectangleWithStroke cbBedroom0;

    @Bind(R.id.cb_bedroom1)
    RectangleWithStroke cbBedroom1;

    @Bind(R.id.cb_bedroom2)
    RectangleWithStroke cbBedroom2;

    @Bind(R.id.cb_bedroom3)
    RectangleWithStroke cbBedroom3;

    @Bind(R.id.cb_bedroom4)
    RectangleWithStroke cbBedroom4;

    @Bind(R.id.cb_bedroom5)
    RectangleWithStroke cbBedroom5;

    @Bind(R.id.cb_bathroom1)
    RectangleWithStroke cbBathroom1;

    @Bind(R.id.cb_bathroom15)
    RectangleWithStroke cbBathroom15;

    @Bind(R.id.cb_bathroom2)
    RectangleWithStroke cbBathroom2;

    @Bind(R.id.cb_bathroom25)
    RectangleWithStroke cbBathroom25;

    @Bind(R.id.cb_bathroom3)
    RectangleWithStroke cbBathroom3;

    @Bind(R.id.cb_bathroom35)
    RectangleWithStroke cbBathroom35;

    @Inject
    DiscoveryPrefsManager mDPrefManager;

    @Inject
    CatalogsManager mCatalogsManager;

    @Inject
    CatalogRestService mCatalogRestService;

    @Inject
    SearchManager mSearchManager;

    @Inject
    SearchRestService mSearchRestService;

    @Inject
    EventBus mEventBus;

    @Inject
    Context mContext;

    @Inject
    LayoutInflater mLayoutInflater;

    private AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            AutocompletePrediction item = mPlaceAutocompleteAdapter.getItem(position);
            if (item != null) {
                String locationString = item.getPrimaryText(new StyleSpan(Typeface.NORMAL)) + ", " + item
                  .getSecondaryText(new StyleSpan(Typeface.NORMAL));

                mDPrefManager.storeLocationString(locationString);

                PendingResult<PlaceBuffer> placeResult = mRoomsterLocationManager.getPlaceById(item.getPlaceId());
                placeResult.setResultCallback(mAdditionalDetailsCallback);
            }
        }
    };

    private ResultCallback<PlaceBuffer> mAdditionalDetailsCallback = new ResultCallback<PlaceBuffer>() {

        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                places.release();
                return;
            }
            try {
                final Place place = places.get(0);
                Location location = fromLatLng(place.getLatLng());
                setAdapterBounds(location, DiscoveryConstants.DEFAULT_DISTANCE);
                mDPrefManager.storeLocation(location);
            } catch (IllegalStateException exception) {
                //nothing to do
            } finally {
                places.release();
            }
        }
    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_discovery_prefs, container, false);
        ButterKnife.bind(this, view);
        RoomsterApplication.getRoomsterComponent().inject(this);
        setUpScrollView();
        addBathroomsToList();
        addBedsToList();
        return view;
    }


    private void setUpScrollView() {
        mScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                onClickAway();
                return false;
            }
        });

    }


    protected void onClickAway() {
        if (isFragmentUIActive()) {
            //hide soft keyboard
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }


    public boolean isFragmentUIActive() {
        return isAdded() && !isDetached() && !isRemoving();
    }


    @Override
    public void onStart() {
        super.onStart();
        mSearchRestService.resetSearchCriteria();
        initLocationManager();
        setUpGeoLocation();
        setUpViewType();
        setUpRentingOrFinding();
        setUpRoomOrPlace();
        setUpDateIn();
        setUpDateOut();
        setUpRadiusBar();
        setUpAgeBar();
        calculateBudgetRangeStep();
        setUpBudgetBar();
        setUpBedrooms();
        setUpBathroom();
        setUpHouseholdSex();
        setUpSex();
        setUpPetsGrid();
        setUpApartmentAmenitiesGrid();
        setUpSortBy();
    }


    @Override
    public void onPause() {
        super.onPause();
        storeBadrooms();
        storeBathrooms();
        checkForPrefsChanges();
    }


    @Override
    public void onStop() {
        super.onStop();
        if (mRoomsterLocationManager != null) {
            mRoomsterLocationManager.stopConnection();
            mRoomsterLocationManager = null;
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        mRoomsterLocationManager.stopLocationUpdates();
        if (getActivity() != null && location != null) {
            mDPrefManager.storeLocation(location);
            String locationString = StringsUtil.getLocationName(getActivity(), location);
            mCurrentLocationTv.setText(locationString);
            mDPrefManager.storeLocationString(locationString);
            setAdapterWithBounds(mRoomsterLocationManager.getLastBounds());
        }
        mLocationPin.setEnabled(true);
    }


    @Override
    public void onConnected() {
        // nothing to do
    }


    @OnClick(R.id.dp_location_pin)
    void onLocationPinClick() {
        mLocationPin.setEnabled(false);
        getPermissionsHelper().actionOnLocationPermission(
                new PermissionsHelper.OnLocationPermissionActionListener() {

                    @Override
                    public void onLocationPermissionGranted() {
                        if (mRoomsterLocationManager != null && mRoomsterLocationManager.isConnected()) {
                            mRoomsterLocationManager.requestLocationUpdates();
                        } else {
                            if (getActivity() != null) {
                                Toast.makeText(getActivity(), R.string.no_location_detected, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }


                    @Override
                    public void onLocationPermissionDenied() {
                        if (getActivity() != null) {
                            Toast.makeText(getActivity(), R.string.no_location_detected, Toast.LENGTH_SHORT).show();
                        }
                        mLocationPin.setEnabled(true);
                    }
                }

        );
    }


    @OnClick({R.id.dp_rb_last_activity, R.id.dp_rb_newest, R.id.dp_rb_rent})
    public void onSortBy() {
        if (mLastActivityRb.isChecked()) {
            mDPrefManager.storeSortBy(DiscoveryPrefsManager.LAST_ACTIVITY);
        } else if (mRentRb.isChecked()) {
            mDPrefManager.storeSortBy(DiscoveryPrefsManager.RENT);
        } else {
            mDPrefManager.storeSortBy(DiscoveryPrefsManager.NEWEST);
        }
    }


    @OnClick({R.id.dp_rb_detailed, R.id.dp_rb_list, R.id.dp_rb_map})
    public void onViewType() {
        if (mDetailedRB.isChecked()) {
            mDPrefManager.setViewType(DiscoveryPrefsManager.DETAILED);
        } else if (mListRB.isChecked()) {
            mDPrefManager.setViewType(DiscoveryPrefsManager.LIST);
        } else {
            mDPrefManager.setViewType(DiscoveryPrefsManager.MAP);
        }
    }


    @OnClick({R.id.dp_rb_renting, R.id.dp_rb_finding})
    public void onRentingOrFinding() {
        if (mRentingRb.isChecked()) {
            mDPrefManager.storeRentingPlace();
        } else {
            mDPrefManager.storeFindingPlace();
        }
        onRoomOrPlace();
        setUpPetsGrid();
    }


    @OnClick(R.id.dp_date_in_clear)
    void onDateInClear() {
        mDateIn = null;
        mDateInTv.setText("");
        mDPrefManager.storeDateIn(null);
        dateInClear.setVisibility(View.GONE);
    }


    @OnClick(R.id.dp_date_out_clear)
    void onDateOutClear() {
        mDateOut = null;
        mDateOutTv.setText("");
        mDPrefManager.storeDateOut(null);
        dateOutClear.setVisibility(View.GONE);
    }


    @OnClick(R.id.dp_clear_button)
    public void onClear() {
        mCurrentLocationTv.setText("");
        mCurrentLocationTv.setHint("");
    }


    @OnClick({R.id.dp_rb_private_room, R.id.dp_rb_entire_place})
    public void onRoomOrPlace() {
        if (mEntirePlaceRb.isChecked()) {
            setEntirePlace();
        } else {
            setRoom();
        }
        setUpApartmentAmenitiesGrid();
    }


    @OnClick(R.id.dp_date_in_container)
    public void onDateInClick() {
        pickDate(mDateInTv, DATE_IN);
    }


    @OnClick(R.id.dp_date_out_container)
    public void onDateOutClick() {
        pickDate(mDateOutTv, DATE_OUT);
    }


    public void pickDate(final TextView textView, final int type) {
        DataPickerDialogFragment dateFragment;
        if (type == DATE_OUT) {
            dateFragment = DataPickerDialogFragment.newInstance(mDateIn, null);
        } else {
            dateFragment = DataPickerDialogFragment.newInstance(null, mDateOut);
        }
        dateFragment.setDataPickerListener(new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                storeDate(type, year, month, day);
                Date date = DateUtil.createDate(day, month, year);
                textView.setText(DateUtil.formatDate(date));
                setClearBtnVisible(type);
            }
        });
        dateFragment.show(getActivity().getSupportFragmentManager(), DATE_PICKER);
    }


    private void setClearBtnVisible(int type) {
        if (type == DATE_OUT) {
            dateOutClear.setVisibility(View.VISIBLE);
        } else {
            dateInClear.setVisibility(View.VISIBLE);
        }
    }


    public void setAdapterWithBounds(LatLngBounds bounds) {
        if (mRoomsterLocationManager != null) {
            mPlaceAutocompleteAdapter = new PlaceAutocompleteAdapter(mContext, mRoomsterLocationManager.getGoogleApiClient(),
              bounds, null);
            mCurrentLocationTv.setAdapter(mPlaceAutocompleteAdapter);
        }
    }


    private void storeDate(int type, int year, int month, int day) {
        if (type == DATE_OUT) {
            mDateOut = DateUtil.createDate(day, month, year);
            mDPrefManager.storeDateOut(DateUtil.formatDateParam(mDateOut));
        } else {
            mDateIn = DateUtil.createDate(day, month, year);
            mDPrefManager.storeDateIn(DateUtil.formatDateParam(mDateIn));
        }
    }


    private void setUpDateIn() {
        mDateIn = DateUtil.parseDateParam(mDPrefManager.getDateIn());
        if (mDateIn != null) {
            mDateInTv.setText(DateUtil.formatDate(mDateIn));
            dateInClear.setVisibility(View.VISIBLE);
        }
    }


    private void setUpDateOut() {
        mDateOut = DateUtil.parseDateParam(mDPrefManager.getDateOut());
        if (mDateOut != null) {
            mDateOutTv.setText(DateUtil.formatDate(mDateOut));
            dateOutClear.setVisibility(View.VISIBLE);
        }
    }


    private void checkForPrefsChanges() {
        SearchCriteria currentSearchCriteria = mSearchManager.getSearchCriteria();
        SearchCriteria prefsSearchCriteria = mDPrefManager.getSearchCriteria();

        if (currentSearchCriteria != null) {
            // Ignore currency since it is not part of the discovery preferences
            prefsSearchCriteria.setCurrency(currentSearchCriteria.getCurrency());
        }

        if (!prefsSearchCriteria.equals(currentSearchCriteria)) {
            mSearchManager.clearResultModel();
        }
    }


    private void setUpViewType() {
        String viewType = mDPrefManager.getViewType();
        if (viewType.equals(DiscoveryPrefsManager.DETAILED)) {
            mDetailedRB.setChecked(true);
        } else if (viewType.equals(DiscoveryPrefsManager.LIST)) {
            mListRB.setChecked(true);
        } else {
            mMapRB.setChecked(true);
        }
    }


    private void setUpRentingOrFinding() {
        String rentingOrFinding = mDPrefManager.getRentingOrFinding();
        if (rentingOrFinding != null && rentingOrFinding.equals(DiscoveryPrefsManager.RENTING_A_PLACE)) {
            mRentingRb.setChecked(true);
        } else {
            mFindingRb.setChecked(true);
        }
    }


    private void setUpRoomOrPlace() {
        String roomOrPlace = mDPrefManager.getRoomOrPlace();
        if (roomOrPlace != null && roomOrPlace.equals(DiscoveryPrefsManager.ENTIRE_PLACE)) {
            mEntirePlaceRb.setChecked(true);
        } else {
            mPrivateRoomRb.setChecked(true);
        }
        onRoomOrPlace();
    }


    private void setRoom() {
        mBedroomLayout.setVisibility(View.GONE);
        mBathroomLayout.setVisibility(View.GONE);
        mApartmentAmenityGridLayout.setVisibility(View.GONE);
        mAmenitiesTitle.setVisibility(View.GONE);
        if (mFindingRb.isChecked()) {
            mHouseholdSexRG.setVisibility(View.VISIBLE);
            mSexRG.setVisibility(View.GONE);
        } else {
            mHouseholdSexRG.setVisibility(View.GONE);
            mSexRG.setVisibility(View.VISIBLE);
        }
        mPetsLayout.setVisibility(View.VISIBLE);
        mAgeLayout.setVisibility(View.VISIBLE);
        mDPrefManager.storePrivateRoom();
    }


    private void setEntirePlace() {
        if(mFindingRb.isChecked()) {
            mBathroomLayout.setVisibility(View.VISIBLE);
        } else {
            mBathroomLayout.setVisibility(View.GONE);
        }
        mBedroomLayout.setVisibility(View.VISIBLE);
        mApartmentAmenityGridLayout.setVisibility(View.VISIBLE);
        mAmenitiesTitle.setVisibility(View.VISIBLE);
        mHouseholdSexRG.setVisibility(View.GONE);
        mPetsLayout.setVisibility(View.GONE);
        mAgeLayout.setVisibility(View.GONE);
        mSexRG.setVisibility(View.GONE);
        mDPrefManager.storeEntirePlace();
    }


    private void initLocationManager() {
        mRoomsterLocationManager = new RoomsterLocationManager();
        mRoomsterLocationManager.startConnection(this);
    }


    private void setUpGeoLocation() {
        mCurrentLocationTv.setOnItemClickListener(mAutocompleteClickListener);
        mCurrentLocationTv.setText(mRoomsterLocationManager.getLastAddress());
        setAdapterWithBounds(mRoomsterLocationManager.getLastBounds());
    }


    private void setAdapterBounds(Location location, int distanceInMeters) {
        LatLngBounds bounds = LocationUtil.getBounds(location.getLatitude(), location.getLongitude(), distanceInMeters);
        setAdapterWithBounds(bounds);
    }


    private Location fromLatLng(LatLng latLng) {
        Location location = new Location("");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);
        return location;
    }


    private void updateBudgetText(int left, int right) {
        int leftValue = (left * budgetRangeStep) + DiscoveryConstants.MIN_BUDGET;
        int rightValue = (right * budgetRangeStep) + DiscoveryConstants.MIN_BUDGET;
        onBudgetChanged(leftValue, rightValue);
    }


    private void updateAgeText(int left, int right) {
        int leftValue = left + DiscoveryConstants.MIN_AGE;
        int rightValue = right + DiscoveryConstants.MIN_AGE;
        onAgeChanged(leftValue, rightValue);
    }


    private void setStoredAge() {
        int minAge = mDPrefManager.getMinAge();
        if (minAge < DiscoveryConstants.MIN_AGE) {
            mDPrefManager.storeMinAge(DiscoveryConstants.MIN_AGE);
            minAge = DiscoveryConstants.MIN_AGE;
        }
        int maxAge = mDPrefManager.getMaxAge();
        if (maxAge > DiscoveryConstants.MAX_AGE) {
            mDPrefManager.storeMinAge(DiscoveryConstants.MAX_AGE);
            maxAge = DiscoveryConstants.MAX_AGE;
        }
        int leftValue = minAge - DiscoveryConstants.MIN_AGE;
        int rightValue = maxAge - DiscoveryConstants.MIN_AGE;
        if (leftValue >= 0 || rightValue < mAgeRangeBarTickCount) {
            try {
                mAgeRangeBar.setThumbIndices(leftValue, rightValue);
            } catch (IllegalArgumentException e) {
                //Nothing to do
            }
        }
    }


    private void onAgeChanged(int leftValue, int rightValue) {
        String minValue = String.valueOf(leftValue);
        String maxValue = String.valueOf(rightValue);
        String str = String.format(getString(R.string.megaphone_age_min_max), minValue, maxValue);
        mAgeMinMaxTv.setText(str);
        mDPrefManager.storeMinAge(leftValue);
        mDPrefManager.storeMaxAge(rightValue);
    }


    private void setStoredBudget() {
        int minBudget = mDPrefManager.getMinBudget();
        int maxBudget = mDPrefManager.getMaxBudget();
        if (minBudget < DiscoveryConstants.MIN_BUDGET) {
            mDPrefManager.storeMinBudget(DiscoveryConstants.MIN_BUDGET);
            minBudget = DiscoveryConstants.MIN_BUDGET;
        }
        if (maxBudget > DiscoveryConstants.MAX_BUDGET) {
            mDPrefManager.storeMaxBudget(DiscoveryConstants.MAX_BUDGET);
            maxBudget = DiscoveryConstants.MAX_BUDGET;
        }

        int leftValue = (minBudget - DiscoveryConstants.MIN_BUDGET) / budgetRangeStep;
        int rightValue = ((maxBudget - DiscoveryConstants.MIN_BUDGET) / budgetRangeStep);
        if (leftValue >= 0 || rightValue < mBudgetRangeBarTickCount) {
            try {
                mBudgetRangeBar.setThumbIndices(leftValue, rightValue);
                updateBudgetText(leftValue, rightValue);
            } catch (IllegalArgumentException e) {
                //Nothing to do
            }
        }
    }


    private void onBudgetChanged(int leftValue, int rightValue) {
        String minValue = String.valueOf(leftValue);
        String maxValue = String.valueOf(rightValue);
        String str = String.format(getString(R.string.megaphone_budget_min_max), minValue, maxValue);
        mBudgetMinMaxTv.setText(str);
        mDPrefManager.storeMinBudget(leftValue);
        mDPrefManager.storeMaxBudget(rightValue);
    }


    private void setUpAgeBar() {
        mAgeRangeBarTickCount = DiscoveryConstants.MAX_AGE - DiscoveryConstants.MIN_AGE + 1;
        mAgeRangeBar.setTickCount(mAgeRangeBarTickCount);
        mAgeRangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {

            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int leftThumbIndex, int rightThumbIndex) {

                //There is a known bug in the RangeBar  - sometimes the indexes go beyond the set range!
                if (leftThumbIndex >= 0 && rightThumbIndex < mAgeRangeBarTickCount) {
                    updateAgeText(leftThumbIndex, rightThumbIndex);
                } else {
                    //If the indexes are not correct - return to last known values
                    setStoredAge();
                }
            }
        });
        setStoredAge();
    }


    private void setUpRadiusBar() {
        mRadiusSlider.setMax(DiscoveryConstants.MAX_RADIUS);
        updateRadius(mDPrefManager.getRadius());
        mRadiusSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateRadius(progress);
            }


            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }


            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mDPrefManager.storeRadius(seekBar.getProgress());
            }
        });
    }


    private void setUpBudgetBar() {
        mBudgetRangeBarTickCount = ((DiscoveryConstants.MAX_BUDGET - DiscoveryConstants.MIN_BUDGET) / budgetRangeStep) + 1;
        mBudgetRangeBar.setTickCount(mBudgetRangeBarTickCount);
        mBudgetRangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {

            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int leftThumbIndex, int rightThumbIndex) {
                //There is a known bug in the RangeBar  - sometimes the indexes go beyond the set range!
                if (leftThumbIndex >= 0 && rightThumbIndex < mBudgetRangeBarTickCount) {
                    updateBudgetText(leftThumbIndex, rightThumbIndex);
                } else {
                    //If the indexes are not correct - return to last known values
                    setStoredBudget();
                }
            }
        });
        setStoredBudget();
    }


    private void calculateBudgetRangeStep(){
        budgetRangeStep = DiscoveryConstants.prepareBudgetRangeStep();
    }


    private void updateRadius(int progress) {
        mRadiusSlider.setProgress(progress);
        int currentProgress = progress / DiscoveryConstants.RADIUS_DIVIDER;
        String stringToSet = String.format(getString(R.string.discovery_radius), currentProgress);
        mRadiusText.setText(stringToSet);
    }


    private void setUpBedrooms() {
        if (mCatalogsManager.getBedroomsList() == null) {
            mCatalogRestService.loadCatalogs();
        } else {
            String[] types = mDPrefManager.getApartmentTypes().split(",");
            for (int i = 0; i < types.length; ++i) {
                updateBedroom(types[i]);
            }
            setNamesInRectangles(mCatalogsManager.getBedroomsList(), listBedroomType);
        }

    }


    private void setNamesInRectangles(List<CatalogModel> models, List<RectangleWithStroke> listRectangles) {
        for(int i = 0; i < models.size(); ++i) {
            listRectangles.get(i).setText(models.get(i).getName());
        }
    }


    private void updateBedroom(String index) {
        switch (index) {
            case STUDIO:
                updateRectWithStroke(cbBedroom0);
                break;
            case BEDROOM_1:
                updateRectWithStroke(cbBedroom1);
                break;
            case BEDROOMS_2:
                updateRectWithStroke(cbBedroom2);
                break;
            case BEDROOMS_3:
                updateRectWithStroke(cbBedroom3);
                break;
            case BEDROOMS_4:
                updateRectWithStroke(cbBedroom4);
                break;
            case BEDROOMS_5:
                updateRectWithStroke(cbBedroom5);
                break;
        }
    }


    private void storeBadrooms() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < listBedroomType.size(); ++i) {
            if(listBedroomType.get(i).isChecked()) {
                if(i != listBedroomType.size() - 1) {
                    sb.append(i + ",");
                } else {
                    sb.append(i);
                }
            }
        }
        mDPrefManager.storeApartmentTypes(sb.toString());
    }


    private void addBedsToList() {
        listBedroomType.add(cbBedroom0);
        listBedroomType.add(cbBedroom1);
        listBedroomType.add(cbBedroom2);
        listBedroomType.add(cbBedroom3);
        listBedroomType.add(cbBedroom4);
        listBedroomType.add(cbBedroom5);
    }


    private void updateRectWithStroke(RectangleWithStroke rect) {
        rect.setChecked(true);
        rect.setTextColor(getResources().getColor(R.color.orange));
    }


    private void setUpBathroom() {
        if (mCatalogsManager.getBathroomsList() == null) {
            mCatalogRestService.loadCatalogs();
        } else {
            String[] types = mDPrefManager.getBathrooms().split(",");
            for (int i = 0; i < types.length; ++i) {
                updateBathroom(types[i]);
            }
            setNamesInRectangles(mCatalogsManager.getBathroomsList(), listBathroomType);
        }

    }


    private void addBathroomsToList() {
        listBathroomType.add(cbBathroom1);
        listBathroomType.add(cbBathroom15);
        listBathroomType.add(cbBathroom2);
        listBathroomType.add(cbBathroom25);
        listBathroomType.add(cbBathroom3);
        listBathroomType.add(cbBathroom35);
    }


    private void updateBathroom(String index) {
        switch (index) {
            case BATH_1:
                updateRectWithStroke(cbBathroom1);
                break;
            case BATH_1_5:
                updateRectWithStroke(cbBathroom15);
                break;
            case BATH_2:
                updateRectWithStroke(cbBathroom2);
                break;
            case BATH_2_5:
                updateRectWithStroke(cbBathroom25);
                break;
            case BATH_3:
                updateRectWithStroke(cbBathroom3);
                break;
            case BATH_3_5:
                updateRectWithStroke(cbBathroom35);
                break;
        }
    }


    private void storeBathrooms() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < listBathroomType.size(); ++i) {
            if(listBathroomType.get(i).isChecked()) {
                if(i != listBathroomType.size() - 1) {
                    sb.append(i + ",");
                } else {
                    sb.append(i);
                }
            }
        }
        mDPrefManager.storeBathrooms(sb.toString());
    }


    private void setUpHouseholdSex() {
        int householdSex = mDPrefManager.getHouseholdSex();
        if (mCatalogsManager.getHouseholdSexList() == null) {
            mCatalogRestService.loadCatalogs();
        }else {
            List <CatalogModel> options = mCatalogsManager.getHouseholdSexList();
            options.add(new CatalogModel(-1, RoomsterApplication.context.getString(R.string.discovery_household_sex_everyone)));

            mHouseholdSexRG.setOptions(getString(R.string.discovery_household_sex), options, householdSex);
            mHouseholdSexRG.setTitleTextSize(R.dimen.text_size_4);

            mHouseholdSexRG.setOnCheckedChangedListener(new CatalogModelRadioGroup.OnCheckedChangedListener() {

                @Override
                public void onCheckedChanged(CatalogModel option) {
                    mDPrefManager.storeHouseholdSex(option.getId());
                }
            });
        }
    }


    private void setUpSex() {
        if (mCatalogsManager.getSexCategoriesList() == null) {
            mCatalogRestService.loadCatalogs();
        }else {
        List <CatalogModel> options = mCatalogsManager.getSexCategoriesList();
        mSexRG.setOptions(getString(R.string.discovery_sex), options, mDPrefManager.getSex());

        mSexRG.setOnCheckedChangedListener(new CatalogModelRadioGroup.OnCheckedChangedListener() {

            @Override
            public void onCheckedChanged(CatalogModel option) {
                mDPrefManager.storeSex(option.getId());
            }
        });
        }
    }


    private void setUpPetsGrid() {
        if (mRentingRb.isChecked()) {
            if (mCatalogsManager.getPetsOwnedList() == null) mCatalogRestService.loadCatalogs();
            else {
                mPetsAdapter = new PetsAdapter(getActivity(), mCatalogsManager.getPetsOwnedList(), mDPrefManager.restoreOwnedPets());
                tvPetsTitle.setText(getString(R.string.discovery_pets));
            }
        } else {
            if (mCatalogsManager.getPetsPreferredList() == null) mCatalogRestService.loadCatalogs();
            else {
                mPetsAdapter = new PetsAdapter(getActivity(), mCatalogsManager.getPetsPreferredList(),
                        mDPrefManager.restorePreferredPets());
                tvPetsTitle.setText(getString(R.string.discovery_pets_preference));
            }

        }
        mPetsAdapter.setOnPetItemClickListener(new PetsAdapter.OnPetItemClick() {

            @Override
            public void onItemClick(CatalogValueModel model, boolean isSelected) {
                if (mRentingRb.isChecked()) {
                    mDPrefManager.storeOwnedPets(model);
                } else {
                    mDPrefManager.storePreferredPets(model);
                }
            }
        });
        mPetsGrid.setAdapter(mPetsAdapter);
    }


    private PermissionsHelper getPermissionsHelper() {
        Activity activity = getActivity();
        if (activity instanceof PermissionsAware) {
            return ((PermissionsAware) activity).getPermissionsHelper();
        } else {
            return new PermissionsHelper(getActivity());
        }
    }


    private void setUpSortBy() {
        String sortBy = mDPrefManager.getSortBy();
        if (sortBy.equals(DiscoveryPrefsManager.RENT)) {
            mRentRb.setChecked(true);
        } else if (sortBy.equals(DiscoveryPrefsManager.LAST_ACTIVITY)) {
            mLastActivityRb.setChecked(true);
        } else {
            mNewestRb.setChecked(true);
        }
    }


    private void setUpApartmentAmenitiesGrid() {
        List<CatalogValueModel> allAmenities;
        if (mEntirePlaceRb.isChecked()) {
            allAmenities = mCatalogsManager.getApartmentAmenitiesList();
            if (allAmenities == null) mCatalogRestService.loadCatalogs();
            else{
                mAmenitiesAdapter = new AmenitiesGridAdapter(getContext(), allAmenities, mDPrefManager.restoreApartmentAmenities(),
                        true);
                mAmenitiesAdapter.setOnAmenityItemClickListener(new AmenitiesGridAdapter.OnAmenityItemClick() {

                    @Override
                    public void onItemClick(CatalogValueModel model, boolean isSelected) {
                        mDPrefManager.storeApartmentAmenities(model);
                    }
                });
            }

        } else {
            allAmenities = mCatalogsManager.getRoomAmenitiesList();
            if (allAmenities == null) mCatalogRestService.loadCatalogs();
            else {
                mAmenitiesAdapter = new AmenitiesGridAdapter(getContext(), allAmenities, mDPrefManager.restoreRoomAmenities(), true);
                mAmenitiesAdapter.setOnAmenityItemClickListener(new AmenitiesGridAdapter.OnAmenityItemClick() {

                    @Override
                    public void onItemClick(CatalogValueModel model, boolean isSelected) {
                        mDPrefManager.storeRoomAmenities(model);
                    }
                });
            }
        }

        if (allAmenities != null) {
            mApartmentAmenityGridView.setAdapter(mAmenitiesAdapter);
            measureLayout(allAmenities.size());
        }
    }


    private void measureLayout(int itemsSize) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mApartmentAmenityGridLayout.getLayoutParams();
        int amenityHeight = getResources().getDimensionPixelOffset(R.dimen.amenities_layout_height);
        int margin = getResources().getDimensionPixelSize(R.dimen.offset_4);
        int height = amenityHeight * (1 + itemsSize / (COLUMNS + 1));
        params.height = height + margin;
        mApartmentAmenityGridLayout.setLayoutParams(params);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(CatalogRestServiceEvent.GetCatalogSuccess event) {
        setUpBedrooms();
        setUpBathroom();
        setUpHouseholdSex();
        setUpSex();
        setUpPetsGrid();
        setUpApartmentAmenitiesGrid();
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(CatalogRestServiceEvent.GetCatalogFailure event) {
        Toast.makeText(mContext, R.string.alert_failed_receive_catlog, Toast.LENGTH_LONG).show();
    }
}