package com.roomster.utils;


import android.content.Context;
import android.content.res.Resources;
import android.text.format.DateUtils;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.annotation.Resource;


/**
 * Created by "Michael Katkov" on 10/11/2015.
 */
public class DateUtil {

    public static final String TAG = "DateUtil";
    private static String[] times;
    private static int[] maxVal = {24, 7, 5, 12, 20};   // {hours, days ago, weeks ago, month ago, years ago}


    public static void resetTimes() {
        times = null;

    }


    public static String getRelativeTimeSpanString(Context context, Date date) {
        if (times == null) {
            initTimes(context);
        }

        int minutes = (int) ((new Date().getTime() - date.getTime()) / DateUtils.MINUTE_IN_MILLIS);
        return timeDiffDeeper(minutes, 60, 0);
    }


    private static String timeDiffDeeper(int value, int prevMax, int deepnes) {
        if (deepnes >= maxVal.length)
            return "Long time ago";

        value = value / prevMax;
        if (value < maxVal[deepnes]) {
            if (deepnes == 0)
                return times[deepnes];
            else
                return value + " " + times[++deepnes];
        } else if (deepnes == 0 && value <= maxVal[deepnes] * 2)
            return times[++deepnes];
        else
            return timeDiffDeeper(value, maxVal[deepnes], ++deepnes);
    }


    private static void initTimes(Context context) {
        times = new String[]{context.getString(R.string.listing_created_today), context.getString(R.string.listing_created_yesterday),
                context.getString(R.string.listing_created_days_ago), context.getString(R.string.listing_created_weeks_ago),
                context.getString(R.string.listing_created_month_ago), context.getString(R.string.listing_created_years_ago)};
    }


    public static Calendar parseDate(String format, String value) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date date = new Date(System.currentTimeMillis());
        try {
            date = sdf.parse(value);
        } catch (ParseException e) {
            LogUtil.logD(TAG, e.getMessage());
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }


    public static String formatToDayAndHour(Date date) {
        if (date != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE HH:mm", Locale.getDefault());
            String formattedDate = dateFormat.format(date);
            return formattedDate;
        } else {
            return null;
        }
    }


    public static String formatDate(Date date) {
        if (date != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
            return dateFormat.format(date);
        } else {
            return null;
        }
    }


    public static String formatDateParam(Date date) {
        if (date != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault());
            return dateFormat.format(date);
        }
        return null;
    }


    public static Date parseDateParam(String value) {
        if (value != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault());
            try {
                return dateFormat.parse(value);
            } catch (ParseException e) {
                LogUtil.logD(TAG, e.getMessage());
            }
        }
        return null;
    }


    public static Date parseDateBilling(String value) {
        if (value != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S'Z'", Locale.getDefault());
            try {
                return dateFormat.parse(value);
            } catch (ParseException e) {
                LogUtil.logD(TAG, e.getMessage());
            }
        }
        return null;
    }


    public static Date createDate(int day, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        return calendar.getTime();
    }


    public static String parseDateBilling(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, MMMM dd, yyyy HH:mm:ss aa", Locale.getDefault());
        return dateFormat.format(date);
    }
}