package com.roomster.activity;


import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.constants.Asset;
import com.roomster.constants.IntentExtras;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ShowAssetActivity extends BaseActivity {

    private Asset mAsset;

    @Bind(R.id.show_asset_text_content)
    TextView mTextContent;

    @Bind(R.id.tv_title)
    TextView mToolbarTitle;


    @Override
    protected int getMainContentLayout() {
        return R.layout.activity_show_asset;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        handleIntent();
        showTextAsset();
    }


    @OnClick(R.id.iv_back)
    void onBackClick() {
        onBackPressed();
    }


    private void showTextAsset() {
        if (mAsset != null) {

            try {
                InputStream is = getAssets().open(mAsset.path);

                int size = is.available();

                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();

                String text = new String(buffer);

                mTextContent.setText(text);
            } catch (IOException e) {
                // nothing to do
            }
        }
    }


    private void handleIntent() {
        Intent intent = getIntent();

        if (intent.hasExtra(IntentExtras.TITLE_EXTRA)) {
            mToolbarTitle.setText(intent.getStringExtra(IntentExtras.TITLE_EXTRA));
        }

        if (intent.hasExtra(IntentExtras.CONTENT_EXTRA)) {
            Serializable asset = intent.getSerializableExtra(IntentExtras.CONTENT_EXTRA);

            if (asset instanceof Asset) {
                mAsset = (Asset) asset;
            }
        }
    }
}
