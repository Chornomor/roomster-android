package com.roomster.constants;


/**
 * Class, holding all String constants, used in Intents as a key of an extra. Created by Bogoi.Bogdanov on 10/14/2015.
 */
public class IntentExtras {

    public static final String CONVERSATION_ID_EXTRA = "com.roomster.constants.CONVERSATION_ID_EXTRA";

    public static final String USER_ID_EXTRA = "com.roomster.constants.USER_ID_EXTRA";

    public static final String OTHER_USER_ID_EXTRA = "com.roomster.constants.OTHER_USER_ID_EXTRA";

    public static final String TITLE_EXTRA = "com.roomster.constants.TITLE_EXTRA";

    public static final String CONTENT_EXTRA = "com.roomster.constants.CONTENT_EXTRA";

    public static final String LOADING_INTENT_FILTER = "com.roomster.constants.LOADING";

    public static final String WITH_BACK_ARROW = "com.roomster.constants.WITH_BACK_ARROW";

    public static final String EXTRA_ABOUT_URL = "com.roomster.constants.ABOUT_URL";

    public static final String FINISH_ON_CONTINUE = "com.roomster.constants.FINISH_ON_CONTINUE";

    public static final String RESPONSE_ERROR = "com.roomster.constants.RESPONSE_ERROR";
}
