package com.roomster.utils;


import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.TimeZone;


/**
 * Created by "Michael Katkov" on 10/12/2015.
 */
public class DateTypeDeserializer implements JsonDeserializer<Date> {

    private static final String UNPARSABLE_DATE   = "Unparseable date: \"";
    private static final String SUPPORTED_FORMATS = "\". Supported formats: \n";

    private static final String[] DATE_FORMATS = new String[]{"yyyy-MM-dd'T'HH:mm:ssZ", "yyyy-MM-dd'T'HH:mm:ss", "yyyy-MM-dd",
                                                              "EEE MMM dd HH:mm:ss z yyyy", "HH:mm:ss", "MM/dd/yyyy HH:mm:ss aaa",
                                                              "yyyy-MM-dd'T'HH:mm:ss.SSSSSS", "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS",
                                                              "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS'Z'", "MMM d',' yyyy H:mm:ss a"};


    @Override
    public Date deserialize(JsonElement jsonElement, Type typeOF, JsonDeserializationContext context) throws JsonParseException {
        for (String format : DATE_FORMATS) {
            try {
                SimpleDateFormat dateFormatter = new SimpleDateFormat(format);
                dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date value = dateFormatter.parse(jsonElement.getAsString());

                return value;
            } catch (ParseException e) {
            }
        }
        throw new JsonParseException(
          UNPARSABLE_DATE + jsonElement.getAsString() + SUPPORTED_FORMATS + Arrays.toString(DATE_FORMATS));
    }
}
