package com.roomster.views;


import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.model.CatalogModel;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class CatalogModelRadioGroup extends LinearLayout {

    private static int sLastViewId = 0;


    public interface OnCheckedChangedListener {

        void onCheckedChanged(CatalogModel option);
    }


    private List<CatalogModel>       mOptions;
    private OnCheckedChangedListener mCheckedChangedListener;
    private CatalogModel             mCheckedOption;

    @Bind(R.id.catalog_model_radio_group_title)
    TextView mTitle;

    @Bind(R.id.catalog_model_radio_group)
    RadioGroup mRadioGroup;


    public CatalogModelRadioGroup(Context context) {
        super(context);
        init();
    }


    public CatalogModelRadioGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    public void setOptions(String title, List<CatalogModel> options, int checkedOptionId) {
        if (title != null) {
            mTitle.setText(title);
        }
        if (options != null) {
            mOptions = options;
            initRadioButtons(checkedOptionId);
        }
    }


    public void setOnCheckedChangedListener(OnCheckedChangedListener onCheckedChangedListener) {
        mCheckedChangedListener = onCheckedChangedListener;
    }


    public void setTitleTextSize(int textSizeResId) {
        float textSize = getResources().getDimension(textSizeResId) / getResources().getDisplayMetrics().density;
        mTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
    }


    public CatalogModel getCheckedOption() {
        return mCheckedOption;
    }


    private void initRadioButtons(int checkedOptionId) {
        mRadioGroup.removeAllViews();
        if (mOptions != null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            for (CatalogModel option : mOptions) {
                RadioButton radioButton = (RadioButton) layoutInflater.inflate(R.layout.green_radio_button, mRadioGroup, false);
                radioButton.setId(getNextViewId());
                radioButton.setTag(option);
                radioButton.setText(option.getName());
                radioButton.setGravity(Gravity.LEFT);
                if (option.getId() == checkedOptionId) {
                    radioButton.setChecked(true);
                }
                RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT,
                  RadioGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.weight = 1f;
//                layoutParams.setMargins(10, 0, 10, 0);
                radioButton.setLayoutParams(layoutParams);
                mRadioGroup.addView(radioButton);
            }
            mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    View checkedView = mRadioGroup.findViewById(checkedId);
                    if (checkedView != null) {
                        Object tag = checkedView.getTag();
                        if (tag instanceof CatalogModel) {
                            mCheckedOption = (CatalogModel) tag;
                            if (mCheckedChangedListener != null) {
                                mCheckedChangedListener.onCheckedChanged(mCheckedOption);
                            }
                        }
                    }
                }
            });
        }
    }


    private static int getNextViewId() {
        return ++sLastViewId;
    }


    private void init() {
        inflate(getContext(), R.layout.view_catalog_model_radio_group, this);
        ButterKnife.bind(this);
    }
}
