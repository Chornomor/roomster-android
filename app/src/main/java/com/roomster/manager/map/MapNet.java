package com.roomster.manager.map;



import android.support.v4.util.LongSparseArray;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.clustering.algo.StaticCluster;
import com.google.maps.android.projection.Point;
import com.google.maps.android.projection.SphericalMercatorProjection;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Boff on 15.08.2016.
 */
public class MapNet {
    private static final double GRID_SIZE = 150.0D;
    SphericalMercatorProjection proj;
    LongSparseArray<MapCluster> clusterArray = new LongSparseArray<>();
    double lastZoom = 0;
    public Set<MapCluster> getClusters(double zoom, List<LatLng> positions){
        if(lastZoom != zoom){
            clusterArray.clear();
            lastZoom = zoom;
        }
        long numCells = (long)Math.ceil(256.0D * Math.pow(2.0D, zoom) / GRID_SIZE);
        proj = new SphericalMercatorProjection((double)numCells);
        HashSet<MapCluster> clusters = new HashSet<>();
        for(LatLng pos : positions){
            clusters.add(getCluster(numCells,pos));
        }
        return clusters;
    }

    MapCluster getCluster(long numCells, LatLng position){
        Point p = proj.toPoint(position);
        long coord = getCoord(numCells, p.x, p.y);
        MapCluster cluster = clusterArray.get(coord);
        if(cluster == null) {
            LatLng center =
                    proj.toLatLng(new com.google.maps.android.geometry.Point(Math.floor(p.x) + 0.5D, Math.floor(p.y) + 0.5D));
            LatLng toN =
                    proj.toLatLng(new com.google.maps.android.geometry.Point(Math.floor(p.x) + 0.5D, Math.floor(p.y -1) + 0.5D));
            LatLng toS =
                    proj.toLatLng(new com.google.maps.android.geometry.Point(Math.floor(p.x) + 0.5D, Math.floor(p.y +1) + 0.5D));
            double latN = center.latitude + (toN.latitude-center.latitude)/2;
            double latS = center.latitude + (toS.latitude-center.latitude)/2;
            double w = 360.0D/numCells;
            double west = center.longitude - w/2.0D;
            double east = center.longitude + w/2.0D;

            LatLng sw = new LatLng(latS,west);
            LatLng ne = new LatLng(latN,east);
            LatLngBounds bounds = new LatLngBounds(sw,ne);
            cluster = new MapCluster(
                    center,
                    bounds);
            clusterArray.put(coord, cluster);
        }
        return cluster;
    }
    private static long getCoord(long numCells, double x, double y) {
        return (long)((double)numCells * Math.floor(x) + Math.floor(y));
    }
}
