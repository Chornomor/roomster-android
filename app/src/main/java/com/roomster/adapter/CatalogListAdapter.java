package com.roomster.adapter;


import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.dialog.CatalogListChooserDialog;
import com.roomster.model.CatalogModel;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Created by michaelkatkov on 1/6/16.
 */
public class CatalogListAdapter extends ArrayAdapter<CatalogModel> {

    @Inject
    LayoutInflater mLayoutInflater;

    private Set<Integer> mSelectedIds;
    private boolean      mIsMultipleChoice;
    private View.OnClickListener onClickListener;


    public CatalogListAdapter(Context context, List<CatalogModel> objects, boolean isMultipleChoice,
            List<Integer> currentIds) {
        super(context, 0, objects);
        RoomsterApplication.getRoomsterComponent().inject(this);
        mIsMultipleChoice = isMultipleChoice;
        if (currentIds != null) {
            mSelectedIds = new HashSet<>(currentIds);
        } else {
            mSelectedIds = new HashSet<>();
        }
    }


    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.item_shared_details, parent, false);
        }
        CatalogModel catalogModel = getItem(position);
        TextView categoryName = (TextView) view.findViewById(R.id.detail_name);
        categoryName.setText(catalogModel.getName());
        CheckBox checkBox = (CheckBox) view.findViewById(R.id.detail_checkbox);
        checkBox.setOnCheckedChangeListener(null);
        checkBox.setChecked(mSelectedIds.contains(catalogModel.getId()));
        checkBox.setOnCheckedChangeListener(new SharedDetailsOnCheckedListener(catalogModel.getId()));
        return view;
    }


    private class SharedDetailsOnCheckedListener implements CompoundButton.OnCheckedChangeListener {

        private Integer mCatalogModelId;


        public SharedDetailsOnCheckedListener(Integer catalogModelId) {
            mCatalogModelId = catalogModelId;
        }


        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                if (!mIsMultipleChoice) {
                    mSelectedIds.clear();
                }
                mSelectedIds.add(mCatalogModelId);
            } else {
                mSelectedIds.remove(mCatalogModelId);
            }
            notifyDataSetChanged();
            if (onClickListener != null) {
                onClickListener.onClick(buttonView);
            }
        }
    }


    public List<Integer> getSelectedIds() {
        return new ArrayList<>(mSelectedIds);
    }
}
