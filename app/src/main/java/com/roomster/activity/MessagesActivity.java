package com.roomster.activity;


import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.roomster.R;
import com.roomster.adapter.MessagesListAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.IntentExtras;
import com.roomster.constants.RestCode;
import com.roomster.dialog.ReportUserDialog;
import com.roomster.enums.StatusEnum;
import com.roomster.event.RestServiceEvents.ConversationRestServiceEvent;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.message.MessageEvent;
import com.roomster.rest.model.MessageView;
import com.roomster.rest.model.UserGetViewModel;
import com.roomster.rest.model.UserReportViewModel;
import com.roomster.rest.service.ConversationsRestService;
import com.roomster.rest.service.UsersRestService;
import com.roomster.utils.ImageHelper;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


/**
 * Activity that manages a single conversation's messages.
 * Created by Bogoi.Bogdanov on 10/14/2015.
 */
public class MessagesActivity extends BaseActivity {

    private String              mConversationId;
    private Long                mUserId;
    private Long                mOtherUserId;
    private UserGetViewModel    mOtherUser;
    private List<MessageView>   mMessagesList;
    private MessagesListAdapter mMessagesListAdapter;
    private int                 mLastMessageIndex;
    private MessageView         mPendingMessage;
    private ReportUserDialog    mReportUserDialog;

    @Bind(R.id.messages_toolbar)
    Toolbar mToolbar;

    @Bind(R.id.toolbar_contact_image)
    ImageView mToolbarContactImage;

    @Bind(R.id.contact_name)
    TextView mContactName;

    @Bind(R.id.messages_recycler_view)
    RecyclerView mMessagesRV;

    @Bind(R.id.messages_new_message_et)
    EditText mNewMessageET;

    @Bind(R.id.send_message_btn)
    Button mSendBtn;

    @Inject
    ConversationsRestService mConversationsService;

    @Inject
    UsersRestService mUsersRestService;

    @Inject
    Tracker mTracker;


    @Override
    protected int getMainContentLayout() {
        return R.layout.activity_messages;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        RoomsterApplication.getRoomsterComponent().inject(this);
        mLastMessageIndex = -1;

        handleIntent();

        initToolbar();
        initAdapter();
        loadOtherUser();
        loadMessages();
        initReportUserDialog();
    }


    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName(getString(R.string.screen_name_messages));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mOtherUser != null) {
            getMenuInflater().inflate(R.menu.messages, menu);

            String otherUserFirstName = mOtherUser.getFirstName();

            MenuItem reportItem = menu.findItem(R.id.messages_action_report);
            if (reportItem != null) {
                reportItem
                        .setTitle(String.format(getString(R.string.messages_action_report_format), otherUserFirstName));
            }

            MenuItem viewProfileItem = menu.findItem(R.id.messages_action_view_profile);
            if (viewProfileItem != null) {
                viewProfileItem.setTitle(
                        String.format(getString(R.string.messages_action_view_profile_format), otherUserFirstName));
            }
        }

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.messages_action_delete_conversation:
            promptDeleteConversation();
            break;
        case R.id.messages_action_mark_unread:
            markConversationUnread();
            break;
        case R.id.messages_action_report:
            mReportUserDialog.show();
            break;
        case R.id.messages_action_view_profile:
            openOtherUserProfile();
            break;
        case R.id.messages_action_show_listings:
            openOtherUserListings();
            break;
        default:
            break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(MessageEvent.NewMessage event) {
        if (event.getOtherUserId() != null && mOtherUserId != null && event.getOtherUserId().equals(mOtherUserId)) {
            loadMessages();
        }
    }


    private void openOtherUserListings() {
        if (mOtherUserId != null) {
            Intent intent = new Intent(this, UserListingsActivity.class);
            intent.putExtra(IntentExtras.OTHER_USER_ID_EXTRA, mOtherUserId);
            startActivity(intent);
        }
    }


    private void openOtherUserProfile() {
        if (mOtherUserId != null) {
            Intent intent = new Intent(this, UserProfileActivity.class);
            intent.putExtra(UserProfileActivity.USER_ID, mOtherUserId);
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                //Nothing to do
            }
        }
    }


    @OnClick(R.id.messages_toolbar_back)
    void onToolbarBackClick() {
        onBackPressed();
    }


    @OnClick(R.id.contact_info_container)
    void onContactInfoHeaderClicked() {
        openOtherUserProfile();
    }


    @OnClick(R.id.send_message_btn)
    void onSendBtnClick() {
        String newMessage = mNewMessageET.getText().toString();
        newMessage = newMessage.trim();
        if (!newMessage.isEmpty()) {
            mNewMessageET.setEnabled(false);
            mNewMessageET.setText("");
            mSendBtn.setEnabled(false);
            addPendingMessage(newMessage);
            mConversationsService.replyToMessage(mConversationId, newMessage, new RestServiceListener<String>() {

                @Override
                public void onSuccess(String result) {
                    mNewMessageET.setEnabled(true);
                    mSendBtn.setEnabled(true);
                    loadMessages();
                }


                @Override
                public void onFailure(int code, Throwable throwable, String msg) {
                    mNewMessageET.setEnabled(true);
                    mSendBtn.setEnabled(true);
                    removePendingMessage();
                    mMessagesListAdapter.setMessages(mMessagesList);
                    int messageId;
                    if (code == RestCode.REQUEST_NOT_MADE) {
                        messageId = R.string.message_check_connection;
                    } else {
                        messageId = R.string.messages_unable_to_send;
                    }
                    Toast.makeText(MessagesActivity.this, messageId, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    @OnTextChanged(R.id.messages_new_message_et)
    public void onNewMessageTextChanged() {
        if (mNewMessageET.getText().length() > 0) {
            mSendBtn.setTextColor(ContextCompat.getColor(this, R.color.green_roomster));
        } else {
            mSendBtn.setTextColor(ContextCompat.getColor(this, R.color.white));
        }
    }


    /**
     * Adds a pending message in the list, which is being send to the server, so the user has indication that the
     * message is being processed.
     */
    private void addPendingMessage(String message) {
        mPendingMessage = new MessageView();
        mPendingMessage.setText(message);
        mPendingMessage.setUserId(mUserId);
        if (mMessagesList == null) {
            mMessagesList = new ArrayList<>();
        }
        mMessagesList.add(mPendingMessage);
        mMessagesListAdapter.setMessages(mMessagesList);
        mMessagesRV.smoothScrollToPosition(mMessagesList.size() - 1);
    }


    private void removePendingMessage() {
        if (mPendingMessage != null) {
            mMessagesList.remove(mPendingMessage);
            mPendingMessage = null;
        }
    }


    private void handleIntent() {
        //TODO: get the conversationId and mMessagesList from the savedInstanceState or some cache if exists.
        mConversationId = getIntent().getStringExtra(IntentExtras.CONVERSATION_ID_EXTRA);
        mUserId = getIntent().getLongExtra(IntentExtras.USER_ID_EXTRA, -1);
        mOtherUserId = getIntent().getLongExtra(IntentExtras.OTHER_USER_ID_EXTRA, -1);
    }


    private void initToolbar() {
        setSupportActionBar(mToolbar);
        mToolbar.setTitle("");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }


    private void initReportUserDialog() {
        mReportUserDialog = new ReportUserDialog(this, new ReportUserDialog.SubmitReportListener() {

            @Override
            public void onReportSubmitted(UserReportViewModel userReportViewModel) {
                reportUser(userReportViewModel);
            }
        });
    }


    private void reportUser(UserReportViewModel reportViewModel) {
        if (mOtherUser != null) {
            reportViewModel.setUserId(mOtherUser.getId());

            mUsersRestService.reportUser(reportViewModel, new RestServiceListener<String>() {

                @Override
                public void onSuccess(String result) {
                    Toast.makeText(MessagesActivity.this, getString(R.string.messages_user_reported_toast),
                            Toast.LENGTH_SHORT).show();
                }


                @Override
                public void onFailure(int code, Throwable throwable, String msg) {
                    Toast.makeText(MessagesActivity.this, getString(R.string.messages_user_not_reported_toast),
                            Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    private void markConversationUnread() {
        mConversationsService.updateConversation(mConversationId, StatusEnum.New, new RestServiceListener() {

            @Override
            public void onSuccess(Object result) {
                mEventBus.post(new ConversationRestServiceEvent.ConversationUpdated());
                finish();
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                Toast.makeText(MessagesActivity.this, getString(R.string.messages_conversation_not_marked_as_read),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void promptDeleteConversation() {
        new AlertDialog.Builder(this).setTitle(R.string.messages_delete_conversation_dialog_title)
                .setMessage(R.string.messages_delete_conversation_dialog_content)
                .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteConversation();
                    }
                }).setNegativeButton(R.string.button_no, null).show();
    }


    private void deleteConversation() {
        mEventBus.post(new ConversationRestServiceEvent.ConversationDeleted(mConversationId));
        finish();

    }


    private void initOtherUser() {
        if (mOtherUser != null) {
            mContactName.setText(mOtherUser.getFirstName());
            ImageHelper.loadBorderedCircledImageInto(this, mOtherUser.getImages().get(0), mToolbarContactImage);
        }
    }


    private void initAdapter() {
        mMessagesListAdapter = new MessagesListAdapter(this, mMessagesList, mUserId);
        mMessagesListAdapter.setOnItemClickListener(new MessagesListAdapter.OnItemClickListener() {

            @Override
            public void onItemClick() {
                openOtherUserProfile();
            }
        });
        mMessagesRV.setAdapter(mMessagesListAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        mMessagesRV.setLayoutManager(linearLayoutManager);
    }


    private void requestAllMessages() {
        mConversationsService.getAllMessages(mConversationId, new RestServiceListener<List<MessageView>>() {

            public void onSuccess(List<MessageView> messages) {
                if (messages != null) {
                    mMessagesList = messages;
                    mLastMessageIndex = mMessagesList.size() - 1;
                    mMessagesListAdapter.setMessages(mMessagesList);
                    mMessagesRV.scrollToPosition(mLastMessageIndex);

                    markAsRead();
                }
            }


            public void onFailure(int code, Throwable throwable, String msg) {
            }
        });
    }


    private void requestNewMessages(String lastMessageId) {
        mConversationsService
                .getMessages(mConversationId, lastMessageId, true, new RestServiceListener<List<MessageView>>() {

                    @Override
                    public void onSuccess(List<MessageView> result) {
                        removePendingMessage();

                        mMessagesList.addAll(result);
                        mLastMessageIndex = mMessagesList.size() - 1;
                        mMessagesListAdapter.setMessages(mMessagesList);
                        mMessagesRV.smoothScrollToPosition(mLastMessageIndex);
                        markAsRead();
                    }


                    @Override
                    public void onFailure(int code, Throwable throwable, String msg) {
                        removePendingMessage();
                        mMessagesListAdapter.setMessages(mMessagesList);
                    }
                });
    }


    private void loadMessages() {
        if (mLastMessageIndex >= 0) {
            requestNewMessages(mMessagesList.get(mLastMessageIndex).getId());
        } else {
            requestAllMessages();
        }
    }


    private void loadOtherUser() {
        mUsersRestService.getUserById(mOtherUserId, new RestServiceListener<UserGetViewModel>() {

            @Override
            public void onSuccess(UserGetViewModel result) {
                mOtherUser = result;
                initOtherUser();
                invalidateOptionsMenu();
            }
        });
    }


    private void markAsRead() {
        mConversationsService.updateConversation(mConversationId, StatusEnum.Read, new RestServiceListener() {

            @Override
            public void onSuccess(Object result) {
                mEventBus.post(new ConversationRestServiceEvent.ConversationUpdated());
            }

            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                //Nothing to do
            }
        });
    }
}
