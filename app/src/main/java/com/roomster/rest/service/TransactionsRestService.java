package com.roomster.rest.service;


import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.transactions.TransactionsEvent;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.TokenManager;
import com.roomster.rest.api.TransactionsApi;
import com.roomster.rest.model.TransactionsModel;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Bogoi.Bogdanov on 4/20/2016.
 */
public class TransactionsRestService {

    @Inject
    TokenManager mTokenManager;

    @Inject
    TransactionsApi mTransactionsApi;

    @Inject
    EventBus mEventBus;

    @Inject
    NetworkManager mNetworkManager;


    public TransactionsRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void getTransactions() {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callGetTransactionsModel(result);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {

            }
        });
    }


    private void callGetTransactionsModel(String authToken) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<List<TransactionsModel>> callGetTransactions = mTransactionsApi.transactionsGet(authToken);
            callGetTransactions.enqueue(new Callback<List<TransactionsModel>>() {

                @Override
                public void onResponse(Call<List<TransactionsModel>> call, Response<List<TransactionsModel>> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            mEventBus.post(new TransactionsEvent.TransactionGetSuccess(response.body()));
                            break;
                        case ResponseCode.BAD_REQUEST:
                        case ResponseCode.SERVER_ERROR:
                        default:
                            mEventBus.post(new TransactionsEvent.TransactionGetFail());
                    }
                }


                @Override
                public void onFailure(Call<List<TransactionsModel>> call, Throwable t) {
                    mEventBus.post(new TransactionsEvent.TransactionGetFail());
                }
            });
        } else {
            mEventBus.post(new TransactionsEvent.TransactionGetFail());
        }
    }
}