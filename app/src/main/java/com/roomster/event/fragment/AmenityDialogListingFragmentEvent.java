package com.roomster.event.fragment;


import com.roomster.model.CatalogValueModel;


/**
 * Created by "Michael Katkov" on 12/27/2015.
 */
public class AmenityDialogListingFragmentEvent {

    public static class AddAmenityEvent extends AmenityDialogListingFragmentEvent {

        public CatalogValueModel amenity;
        public int               amenityType;
    }


    public static class RemoveAmenityEvent extends AmenityDialogListingFragmentEvent {

        public CatalogValueModel amenity;
        public int               amenityType;
    }
}
