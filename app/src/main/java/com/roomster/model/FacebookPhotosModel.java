package com.roomster.model;


import java.util.List;


/**
 * Created by "Michael Katkov" on 10/27/2015.
 */
public class FacebookPhotosModel {

    private List<Data> data;

    private PagingModel paging;


    public List<Data> getData() {
        return data;
    }


    public void setData(List<Data> data) {
        this.data = data;
    }


    public PagingModel getPaging() {
        return paging;
    }


    public void setPaging(PagingModel paging) {
        this.paging = paging;
    }


    public static class Data {

        private String id;

        private List<Image> images;


        public String getId() {
            return id;
        }


        public void setId(String id) {
            this.id = id;
        }


        public List<Image> getImages() {
            return images;
        }


        public void setImages(List<Image> images) {
            this.images = images;
        }


        public Image getMaxImage() {
            Image maxImage = null;
            if (images != null && !images.isEmpty()) {//check if images exist
                maxImage = images.get(0);
                for (Image image : images) {//find an image with max resolution
                    if (image.getHeight() > maxImage.getHeight()) {
                        maxImage = image;
                    }
                }
            }
            return maxImage;
        }


        public static class Image {

            private int height;

            private String source;

            private int width;


            public int getHeight() {
                return height;
            }


            public void setHeight(int height) {
                this.height = height;
            }


            public String getSource() {
                return source;
            }


            public void setSource(String source) {
                this.source = source;
            }


            public int getWidth() {
                return width;
            }


            public void setWidth(int width) {
                this.width = width;
            }
        }
    }


    public static class PagingModel {

        private Cursors cursors;
        private String  previous;
        private String  next;


        public Cursors getCursors() {
            return cursors;
        }


        public void setCursors(Cursors cursors) {
            this.cursors = cursors;
        }


        public String getPrevious() {
            return previous;
        }


        public void setPrevious(String previous) {
            this.previous = previous;
        }


        public String getNext() {
            return next;
        }


        public void setNext(String next) {
            this.next = next;
        }


        public static class Cursors {

            private String after;

            private String before;


            public String getAfter() {
                return after;
            }


            public void setAfter(String after) {
                this.after = after;
            }


            public String getBefore() {
                return before;
            }


            public void setBefore(String before) {
                this.before = before;
            }
        }
    }
}
