package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


/**
 * Created by Bogoi.Bogdanov on 2/29/2016.
 */
public class InAppProductViewModel {

    @SerializedName("Id")
    private Integer id;

    @SerializedName("OsType")
    private Integer osType;

    @SerializedName("SKU")
    private String sku;

    @SerializedName("Name")
    private String name;

    @SerializedName("ProductType")
    private String productType;

    @SerializedName("BasePrice")
    private Double bestPrice;

    @SerializedName("BaseCurrency")
    private String baseCurrency;

    @SerializedName("IsActive")
    private Boolean isActive;


    public String getBaseCurrency() {
        return baseCurrency;
    }


    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }


    public Double getBestPrice() {
        return bestPrice;
    }


    public void setBestPrice(Double bestPrice) {
        this.bestPrice = bestPrice;
    }


    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public Boolean getIsActive() {
        return isActive;
    }


    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public Integer getOsType() {
        return osType;
    }


    public void setOsType(Integer osType) {
        this.osType = osType;
    }


    public String getProductType() {
        return productType;
    }


    public void setProductType(String productType) {
        this.productType = productType;
    }


    public String getSku() {
        return sku;
    }


    public void setSku(String sku) {
        this.sku = sku;
    }
}
