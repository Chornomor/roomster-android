package com.roomster.model;


import com.facebook.AccessToken;


/**
 * A class representing a the user account data.
 * <p/>
 * Created by Bogoi.Bogdanov on 10/6/2015.
 */
public class UserAccount {

    /**
     * Check whether the user is logged in.
     *
     * @return <code>true</code> if the user is logged in and <code>false</code> otherwise.
     */
    public boolean isLoggedIn() {
        return AccessToken.getCurrentAccessToken() != null;
    }
}
