package com.roomster.fragment;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.roomster.R;
import com.roomster.activity.AddNewDetailedListingActivity;
import com.roomster.adapter.AptSizesSpinnerAdapter;
import com.roomster.adapter.CurrenciesSpinnerAdapter;
import com.roomster.adapter.PhotoGridAdapter;
import com.roomster.adapter.PlaceAutocompleteAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.DiscoveryConstants;
import com.roomster.constants.ListingType;
import com.roomster.dialog.CatalogListChooserDialog;
import com.roomster.dialog.ImageChooserDialog;
import com.roomster.enums.ServiceTypeEnum;
import com.roomster.event.RestServiceEvents.CatalogRestServiceEvent;
import com.roomster.event.RestServiceEvents.ListingsRestServiceEvent;
import com.roomster.listener.AddListing;
import com.roomster.manager.CatalogsManager;
import com.roomster.manager.DiscoveryPrefsManager;
import com.roomster.manager.MegaphoneManager;
import com.roomster.manager.RoomsterLocationManager;
import com.roomster.manager.SettingsManager;
import com.roomster.model.CatalogModel;
import com.roomster.rest.model.Calendar;
import com.roomster.rest.model.HaveApartment;
import com.roomster.rest.model.ListingPostViewModel;
import com.roomster.rest.model.ListingPutViewModel;
import com.roomster.rest.model.NeedApartment;
import com.roomster.rest.model.Photo;
import com.roomster.rest.model.Rates;
import com.roomster.rest.model.ShareDetails;
import com.roomster.rest.service.CatalogRestService;
import com.roomster.rest.service.ImagesRestService;
import com.roomster.rest.service.ListingsRestService;
import com.roomster.utils.DateUtil;
import com.roomster.utils.LocaleUtil;
import com.roomster.utils.LocationUtil;
import com.roomster.utils.PermissionsAware;
import com.roomster.utils.PermissionsHelper;
import com.roomster.views.AddListingAdditionalFields;
import com.roomster.views.DoneAutoCompleteEditText;
import com.roomster.views.ImagePager;
import com.roomster.views.RectangleWithStroke;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

import static com.roomster.utils.StringsUtil.getLocationName;


public class AddListingDetailedFragment extends Fragment implements RoomsterLocationManager.LocationCallbacks, ImagePager.ImageClickListener, ImageChooserListener {

    public static final String LISTING_TYPE = "listing_type";

    private static final String DATA_PICKER = "data_picker";
    private static final String LISTING = "Listing";
    private static final int TEMP_PHOTO_ID = 1;
    private static final int DEFAULT_NEW_PHOTO_ID = 0;

    private static final int AVAILABLE_DATE = 20;
    private static final int LEAVE_BY_DATE = 10;
    private static final int ONE = 1;


    @Bind(R.id.al_scroll_view)
    ScrollView mScrollView;

    @Bind(R.id.al_current_location_tv)
    DoneAutoCompleteEditText mLocationView;

    @Bind(R.id.al_location_pin)
    ImageView mLocationPin;

    @Nullable
    @Bind(R.id.al_bedroom_layout)
    LinearLayout mBedroomLayout;

    @Nullable
    @Bind(R.id.al_bedroom_value)
    TextView mBedroomValueTv;

    @Nullable
    @Bind(R.id.al_looking_bedrooms_layout)
    LinearLayout mLookingBedroomLayout;

    @Nullable
    @Bind(R.id.cb_bedroom0)
    RectangleWithStroke cbBedroom0;

    @Nullable
    @Bind(R.id.cb_bedroom1)
    RectangleWithStroke cbBedroom1;

    @Nullable
    @Bind(R.id.cb_bedroom2)
    RectangleWithStroke cbBedroom2;

    @Nullable
    @Bind(R.id.cb_bedroom3)
    RectangleWithStroke cbBedroom3;

    @Nullable
    @Bind(R.id.cb_bedroom4)
    RectangleWithStroke cbBedroom4;

    @Nullable
    @Bind(R.id.cb_bedroom5)
    RectangleWithStroke cbBedroom5;

    @Nullable
    @Bind(R.id.al_bath_layout)
    LinearLayout mBathLayout;

    @Nullable
    @Bind(R.id.al_bath_value)
    TextView mBathValueTv;

    @Nullable
    @Bind(R.id.al_minimum_stay_layout)
    LinearLayout mMinStayLayout;

    @Nullable
    @Bind(R.id.al_minimum_stay_value)
    TextView mMinStayValueTv;

    @Bind(R.id.al_measurement_title)
    TextView mMeasurementTitleTv;

    @Bind(R.id.al_measurement_layout)
    LinearLayout mMeasurementLayout;

    @Bind(R.id.al_measurement_et)
    EditText mMeasurementEt;

    @Bind(R.id.al_measurement_spinner)
    Spinner mMeasurementSpinner;

    @Bind(R.id.al_is_furnished_layout)
    RelativeLayout mFurnishedLayout;

    @Bind(R.id.al_is_furnished_toggle)
    ToggleButton mFurnishedToggle;

    @Bind(R.id.al_rental_et)
    EditText mRentalEt;

    @Bind(R.id.al_rental_spinner)
    Spinner mRentalSpinner;

    @Bind(R.id.al_rb_long_term)
    RadioButton mLongTermRb;

    @Bind(R.id.al_rb_short_term)
    RadioButton mShortTermRb;

    @Bind(R.id.al_available_date_tv)
    TextView mAvailableDateTv;

    @Bind(R.id.al_leave_by_title)
    TextView mLeaveByTitleTv;

    @Bind(R.id.al_leave_by_tv)
    TextView mLeaveByTv;

    @Bind(R.id.al_show_more_fields_button)
    Button mShowMoreButton;

    @Bind(R.id.al_show_more_fields_text)
    TextView tvShowMore;

    @Bind(R.id.al_additional_fields)
    AddListingAdditionalFields mAdditionalFields;

    @Bind(R.id.al_residence_title)
    TextView mResidenceTitle;

    @Nullable
    @Bind(R.id.al_add_images_grid)
    ImagePager mImagesPager;

    @Inject
    Context mContext;

    @Inject
    SettingsManager mSettingsManager;

    @Inject
    CatalogsManager mCatalogsManager;

    @Inject
    CatalogRestService mCatalogRestService;

    @Inject
    ListingsRestService mListingsRestService;

    @Inject
    ImagesRestService mImagesRestService;

    @Inject
    EventBus mEventBus;

    @Inject
    DiscoveryPrefsManager mDiscoveryPrefsManager;

    @Inject
    MegaphoneManager mMegaphoneManager;

    View view;

    private boolean isFurnished = false;
    private boolean isFirstTimeOpen = true;
    private boolean isFirstCurrentLocationFilled = false;

    private int mType;
    private PlaceAutocompleteAdapter mPlaceAutocompleteAdapter;
    private LatLngBounds mLatLngBounds;
    private Date mDateIn;
    private Date mDateOut;
    private Integer mBedroomId = -1;
    private Integer mBathroomId = -1;
    private Integer mMinStayId = -1;
    private boolean mNeedToShowBedroomsDialog = false;
    private boolean mNeedToShowBathDialog = false;
    private boolean mNeedToShowMinStayDialog = false;
    private RoomsterLocationManager mRoomsterLocationManager;
    private ImageChooserDialog mImageChooserDialog;
    private PermissionsHelper mPermissionsHelper;
    private ImageChooserManager mImageChooserManager;
    private int mChooserType;
    private String mFilePath;
    private ListingPutViewModel mCurrentListingModel;
    private List<Photo> listPhotos = new ArrayList<>();
    private List<Integer> bedroomIdList  = new ArrayList<>();
    private List<RectangleWithStroke> listBedroomType = new ArrayList<>();


    private ResultCallback<PlaceBuffer> mAdditionalDetailsCallback = new ResultCallback<PlaceBuffer>() {

        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                places.release();
                return;
            }
            try {
                final Place place = places.get(0);
                Location location = fromLatLng(place.getLatLng());
                setBounds(location, DiscoveryConstants.DEFAULT_DISTANCE);
                mDiscoveryPrefsManager.storeLocation(location);
            } catch (IllegalStateException exception) {
                // nothing to do
            } finally {
                places.release();
            }
        }
    };

    private AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            AutocompletePrediction item = mPlaceAutocompleteAdapter.getItem(position);
            if (item != null) {

                PendingResult<PlaceBuffer> placeResult = mRoomsterLocationManager.getPlaceById(item.getPlaceId());
                placeResult.setResultCallback(mAdditionalDetailsCallback);
            }
        }
    };


    public static AddListingDetailedFragment newInstance(int type) {
        AddListingDetailedFragment fragment = new AddListingDetailedFragment();
        Bundle args = new Bundle();
        args.putInt(LISTING_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
        mEventBus.register(this);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mType = getArguments().getInt(LISTING_TYPE);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        mRoomsterLocationManager.setActivity(getActivity());
    }


    @Override
    public void onPause() {
        super.onPause();
        mRoomsterLocationManager.resetActivity();
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mEventBus.unregister(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        switch (mType) {
            case ListingType.FINDING_ENTIRE_PLACE_TYPE:
                view = inflater.inflate(R.layout.fragment_add_listing_detailed_looking_entire, container, false);
                break;
            default:
                view = inflater.inflate(R.layout.fragment_add_listing_detailed, container, false);
        }
        ButterKnife.bind(this, view);
        setUpShowMoreButton();
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpScrollView();
    }


    @Override
    public void onStart() {
        super.onStart();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            if (activity instanceof PermissionsAware) {
                mPermissionsHelper = ((PermissionsAware) activity).getPermissionsHelper();
            } else {
                mPermissionsHelper = new PermissionsHelper(activity);
            }
        }
        if (isFirstTimeOpen) {
            initLayout();
        }
        mLocationView.setOnItemClickListener(mAutocompleteClickListener);

        setUpMeasurementSpinner();
        setUpFurnishedToggle(isFurnished);
        setUpRentalSpinner();
        setUpAvailableDate();
        setUpLeaveByDate();
        initLocationManager();
    }


    private void setUpShowMoreButton() {
        if (mShowMoreButton.getTag() == null) {
            mShowMoreButton.setTag(true);
        }
    }


    private void setUpScrollView() {
        mScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                onClickAway();
                return false;
            }
        });

    }


    protected void onClickAway() {
        //hide soft keyboard
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            if (!getActivity().isFinishing() && !getActivity().isDestroyed()) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        } else {
            if (!getActivity().isFinishing()) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && (requestCode == ChooserType.REQUEST_PICK_PICTURE
                || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
            if (mImageChooserManager == null) {
                reinitializeImageChooser();
            }
            mImageChooserManager.submit(requestCode, data);
        } else {
            setUpImageGrid();
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        if (mRoomsterLocationManager != null) {
            mRoomsterLocationManager.stopConnection();
            mRoomsterLocationManager = null;
        }
        storeDataOnStop();
    }


    private void storeDataOnStop() {
        storeFurnishedValue();
    }


    private void storeFurnishedValue() {
        if (mFurnishedToggle.getVisibility() == View.VISIBLE) {
            isFurnished = mFurnishedToggle.isChecked();
        }
    }


    @Override
    public void onConnected() {
        if (!isFirstCurrentLocationFilled) {
            mLocationPin.setEnabled(false);
            getPermissionsHelper().actionOnLocationPermission(new PermissionsHelper.OnLocationPermissionActionListener() {

                @Override
                public void onLocationPermissionGranted() {
                    if (mRoomsterLocationManager != null) {
                        if (!mRoomsterLocationManager.isConnected()) {
                            setupInitialGeoLocation();
                        }
                        if (mRoomsterLocationManager.getLastKnownLocation() != null) {
                            updateLocationView(mRoomsterLocationManager.getLastKnownLocation());
                        } else {
                            mRoomsterLocationManager.requestLocationUpdates();
                        }

                    } else {
                        setupInitialGeoLocation();
                    }

                    mLocationPin.setEnabled(true);
                }


                @Override
                public void onLocationPermissionDenied() {
                    setupInitialGeoLocation();
                    mLocationPin.setEnabled(true);
                }
            });
            isFirstCurrentLocationFilled = true;
        } else {
            if (mRoomsterLocationManager == null || mRoomsterLocationManager.getLastKnownLocation() == null)
                return;
            setBounds(mRoomsterLocationManager.getLastKnownLocation(), DiscoveryConstants.DEFAULT_DISTANCE);
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        mRoomsterLocationManager.stopLocationUpdates();
        if (getActivity() != null && location != null) {
            mDiscoveryPrefsManager.storeLocation(location);
            updateLocationView(location);
        }
    }


    @OnClick(R.id.al_location_pin)
    void onLocationPinClick() {
        mLocationPin.setEnabled(false);
        getPermissionsHelper().actionOnLocationPermission(
                new PermissionsHelper.OnLocationPermissionActionListener() {

                    @Override
                    public void onLocationPermissionGranted() {
                        if (mRoomsterLocationManager != null && mRoomsterLocationManager.isConnected()) {
                            mRoomsterLocationManager.requestLocationUpdates();
                        } else {
                            if (getActivity() != null) {
                                Toast.makeText(getActivity(), R.string.no_location_detected, Toast.LENGTH_SHORT).show();
                            }
                        }
                        mLocationPin.setEnabled(true);
                    }


                    @Override
                    public void onLocationPermissionDenied() {
                        if (getActivity() != null) {
                            Toast.makeText(getActivity(), R.string.no_location_detected, Toast.LENGTH_SHORT).show();
                        }
                        mLocationPin.setEnabled(true);
                    }
                }

        );
    }


    @OnClick(R.id.al_clear_button)
    public void onClear() {
        mLocationView.setText("");
        mLocationView.setHint("");
        mLocationView.requestFocus();
    }


    @OnTextChanged({R.id.al_rental_et, R.id.al_measurement_et})
    void onTextChanged() {
    }


    @OnClick(R.id.al_show_more_fields_button)
    void onClickShowMore() {
        hideKeyboard();
        if (mShowMoreButton.getTag() != null && (boolean) mShowMoreButton.getTag()) {
            tvShowMore.setText(getString(R.string.al_button_add_less));
            mShowMoreButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.remove_button));
            mShowMoreButton.setTag(false);
            mAdditionalFields.setVisibility(View.VISIBLE);
            initFurnishedLayout();
            setUpImageGrid();
        } else {
            tvShowMore.setText(getString(R.string.al_button_add_more));
            mShowMoreButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.add_button));
            mShowMoreButton.setTag(true);
            mAdditionalFields.setVisibility(View.GONE);
            initFurnishedLayout();
            if (mImagesPager != null) {
                mImagesPager.setVisibility(View.GONE);
            }
        }
    }


    public void hideKeyboard() {
        if (getActivity() == null || getActivity().isFinishing())
            return;
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    @OnClick(R.id.al_available_date_tv)
    public void onAvailableDateClick() {
        pickDate(mAvailableDateTv, AVAILABLE_DATE);
    }


    @OnClick(R.id.al_leave_by_tv)
    public void onLeaveDateClick() {
        pickDate(mLeaveByTv, LEAVE_BY_DATE);
    }


    @OnClick({R.id.al_rb_short_term, R.id.al_rb_long_term})
    public void onShortOrLong() {
        if (mShortTermRb.isChecked()) {
            mLeaveByTv.setVisibility(View.VISIBLE);
            mLeaveByTitleTv.setVisibility(View.VISIBLE);
        } else {
            mLeaveByTv.setVisibility(View.GONE);
            mLeaveByTitleTv.setVisibility(View.GONE);
        }
    }


    @Override
    public void onImageClick(Photo photo) {
        if (photo != null) {
            if (photo.getPath().equals(PhotoGridAdapter.ADD_NEW_IMAGE_PLACEHOLDER)) {
                showImageChooserDialog();
            } else {
                removePhotoByPath(photo.getPath());
                setUpImageGrid();
            }
        }
    }


    private void removePhotoByPath(String path) {
        for (int i = 0; i < listPhotos.size(); ++i) {
            if (listPhotos.get(i).getPath().equals(path)) {
                listPhotos.remove(i);
                break;
            }
        }
    }


    @Override
    public void onImageChosen(final ChosenImage image) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    String imageUrl = image.getFileThumbnail();
                    if (imageUrl == null) {
                        imageUrl = image.getFilePathOriginal();
                    }
                    Photo photo = new Photo();
                    photo.setPath(imageUrl);
                    photo.setId(TEMP_PHOTO_ID); /* need to set temporary id for show adapter difference from
                                                empty images while we don't load them to server for get actualy picture id*/
                    listPhotos.add(photo);
                    setUpImageGrid();
                }
            });
        }
    }


    @Override
    public void onError(final String reason) {
        final FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(activity, reason, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    private void showImageChooserDialog() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            mImageChooserDialog = new ImageChooserDialog(activity, new ImageChooserDialog.Listener() {

                @Override
                public void onFacebookClick() {

                }


                @Override
                public void onCameraClick() {
                    mImageChooserDialog.dismiss();
                    mPermissionsHelper
                            .actionOnCameraPermission(new PermissionsHelper.OnCameraPermissionActionListener() {

                                @Override
                                public void onCameraPermissionGranted() {
                                    mPermissionsHelper.actionOnStoragePermission(new PermissionsHelper.OnStoragePermissionActionListener() {
                                        @Override
                                        public void onStoragePermissionGranted() {
                                            takePicture();
                                        }


                                        @Override
                                        public void onStoragePermissionDenied() {
                                            //TODO: Fix this hack - we need to remove all loading indicators from the grid
                                            setUpImageGrid();
                                        }
                                    });
                                }


                                @Override
                                public void onCameraPermissionDenied() {
                                    setUpImageGrid();
                                }
                            });
                }


                @Override
                public void onGalleryClick() {
                    mImageChooserDialog.dismiss();
                    mPermissionsHelper
                            .actionOnStoragePermission(new PermissionsHelper.OnStoragePermissionActionListener() {

                                @Override
                                public void onStoragePermissionGranted() {
                                    chooseImage();
                                }


                                @Override
                                public void onStoragePermissionDenied() {
                                    setUpImageGrid();
                                }
                            });
                }


                @Override
                public void onCancel() {
                    mImageChooserDialog.dismiss();
                    setUpImageGrid();
                }
            });
            mImageChooserDialog.show();
        }
    }


    private void chooseImage() {
        mChooserType = ChooserType.REQUEST_PICK_PICTURE;
        mImageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_PICK_PICTURE, true);
        mImageChooserManager.setImageChooserListener(this);
        mImageChooserManager.clearOldFiles();
        try {
            mFilePath = mImageChooserManager.choose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void takePicture() {
        mChooserType = ChooserType.REQUEST_CAPTURE_PICTURE;
        mImageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_CAPTURE_PICTURE, true);
        mImageChooserManager.setImageChooserListener(this);
        try {
            mFilePath = mImageChooserManager.choose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void reinitializeImageChooser() {
        mImageChooserManager = new ImageChooserManager(this, mChooserType, true);
        mImageChooserManager.setImageChooserListener(this);
        mImageChooserManager.reinitialize(mFilePath);
    }


    private void sendPicturesToServer() {
        for (int i = 0; i < listPhotos.size(); ++i) {
            if (listPhotos.get(i).getPath() != null) {
                listPhotos.get(i).setId(DEFAULT_NEW_PHOTO_ID);
                mImagesRestService.postImage(LISTING, mCurrentListingModel.getListingId(), listPhotos.get(i).getPath());
            }
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(CatalogRestServiceEvent.GetCatalogSuccess event) {
        setUpMeasurementSpinner();
        setUpRentalSpinner();
        if (mNeedToShowBedroomsDialog) {
            mNeedToShowBedroomsDialog = false;
            onBedroom();
        }
        if (mNeedToShowBathDialog) {
            mNeedToShowBathDialog = false;
            onBath();
        }
        if (mNeedToShowMinStayDialog) {
            mNeedToShowMinStayDialog = false;
            onMinStay();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(CatalogRestServiceEvent.GetCatalogFailure event) {
        Toast.makeText(mContext, R.string.alert_failed_receive_catlog, Toast.LENGTH_LONG).show();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ListingsRestServiceEvent.AddListingSuccess addListingSuccess) {
        ListingPostViewModel postModel = prepareModel();
        mCurrentListingModel = ListingPutViewModel.fromPostToPut(postModel);
        mCurrentListingModel.setListingId(addListingSuccess.id);
        mCurrentListingModel.setHeadline(mAdditionalFields.getHeadline());
        mCurrentListingModel.setDescription(mAdditionalFields.getDescription());
        mAdditionalFields.buildApartmentAmenities(mCurrentListingModel);
        sendPicturesToServer();
        mListingsRestService.editListing(mCurrentListingModel);
    }


    public void pickDate(final TextView textView, final int type) {
        DataPickerDialogFragment dateFragment = DataPickerDialogFragment.newInstance(null, null);
        dateFragment.setDataPickerListener(new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                storeDate(type, year, month, day);
                Date date = DateUtil.createDate(day, month, year);
                textView.setText(DateUtil.formatDate(date));
            }
        });
        dateFragment.show(getActivity().getSupportFragmentManager(), DATA_PICKER);
    }


    public void setAdapterWithBounds(LatLngBounds bounds) {
        if (mRoomsterLocationManager != null) {
            AutocompleteFilter filter = new AutocompleteFilter.Builder()
                    .setTypeFilter(AutocompleteFilter.TYPE_FILTER_GEOCODE).build();
            mPlaceAutocompleteAdapter = new PlaceAutocompleteAdapter(mContext,
                    mRoomsterLocationManager.getGoogleApiClient(), bounds, filter);
            mLocationView.setAdapter(mPlaceAutocompleteAdapter);
        }
    }


    private void updateLocationView(Location location) {
        mLocationView.setText(getLocationName(mContext, location));
        setBounds(location, DiscoveryConstants.DEFAULT_DISTANCE);
    }


    private void initLayout() {
        switch (mType) {
            case ListingType.FINDING_ENTIRE_PLACE_TYPE:
                addBedsToList();
                initFindingLayout();
                break;
            case ListingType.FINDING_ROOM_TYPE:
                initFindingLayout();
                break;
            case ListingType.OFFERING_ENTIRE_PLACE_TYPE:
            case ListingType.OFFERING_ROOM_TYPE:
                initOfferingLayout();
                break;
        }
        isFirstTimeOpen = false;
        mAdditionalFields.setListingType(mType);
    }


    private void initFindingLayout() {
        wipePickers();
        initFurnishedLayout();
    }


    private void initFurnishedLayout() {
        if (mShowMoreButton.getTag() != null && !(boolean) mShowMoreButton.getTag()) {
            if (mType == ListingType.FINDING_ENTIRE_PLACE_TYPE) {
                mFurnishedLayout.setVisibility(View.VISIBLE);
            }
        } else {
            mFurnishedLayout.setVisibility(View.GONE);
        }
    }


    private void addBedsToList() {
        listBedroomType.add(cbBedroom0);
        listBedroomType.add(cbBedroom1);
        listBedroomType.add(cbBedroom2);
        listBedroomType.add(cbBedroom3);
        listBedroomType.add(cbBedroom4);
        listBedroomType.add(cbBedroom5);
    }


    private void initOfferingLayout() {
        switch (mType) {
            case ListingType.OFFERING_ROOM_TYPE:
                wipePickers();
                initFurnishedLayout();
                break;
        }
    }


    private void wipePickers() {
        if (mBedroomLayout != null) {
            mBedroomLayout.setVisibility(View.GONE);
            mMinStayLayout.setVisibility(View.GONE);
            mBathLayout.setVisibility(View.GONE);
        }
        mResidenceTitle.setVisibility(View.GONE);
        mMeasurementTitleTv.setVisibility(View.GONE);
        mMeasurementLayout.setVisibility(View.GONE);
    }


    private void initLocationManager() {
        mRoomsterLocationManager = new RoomsterLocationManager();
        mRoomsterLocationManager.startConnection(this);
    }


    private void setupInitialGeoLocation() {
        setAdapterWithBounds(mDiscoveryPrefsManager.getLastBounds());
        mLocationView.setText(mDiscoveryPrefsManager.getLocationString());
    }


    private void setBounds(Location location, int mDistanceInMeters) {
        mLatLngBounds = LocationUtil.getBounds(location.getLatitude(), location.getLongitude(), mDistanceInMeters);
        setAdapterWithBounds(mLatLngBounds);
    }


    private Location fromLatLng(LatLng latLng) {
        Location location = new Location("");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);
        return location;
    }


    private void setUpImageGrid() {
        if (mType == ListingType.OFFERING_ROOM_TYPE || mType == ListingType.OFFERING_ENTIRE_PLACE_TYPE) {
            if (listPhotos == null) {
                listPhotos = new ArrayList<>();
            }
            mImagesPager.setAdapter(getChildFragmentManager(), listPhotos, this);
            mImagesPager.setVisibility(View.VISIBLE);
        }
    }


    private void setUpMeasurementSpinner() {
        if (mCatalogsManager.getAptSizeUnitsList() == null) {
            mCatalogRestService.loadCatalogs();
        } else {
            List<CatalogModel> list = mCatalogsManager.getAptSizeUnitsList();
            AptSizesSpinnerAdapter adapter = new AptSizesSpinnerAdapter(mContext, list);
            mMeasurementSpinner.setAdapter(adapter);
            selectDefaultMeasure(mMeasurementSpinner, list);
        }
    }


    private void selectDefaultMeasure(Spinner spinner, List<CatalogModel> list) {
        String defaultValue = LocaleUtil.isLocaleMeasureUnitImperial() ?
                DiscoveryPrefsManager.APT_SIZE_UNIT_IMPERIAL :
                DiscoveryPrefsManager.APT_SIZE_UNIT_METRIC;
        int position = -1;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getName().equals(defaultValue)) {
                position = i;
                break;
            }
        }
        if (position != -1) {
            spinner.setSelection(position);
        }
    }


    private void setUpFurnishedToggle(boolean isFurnished) {
        mFurnishedToggle.setChecked(isFurnished);
    }


    private void setUpRentalSpinner() {
        if (mCatalogsManager.getCurrenciesList() == null) {
            mCatalogRestService.loadCatalogs();
        } else {
            List<String> list = mCatalogsManager.getCurrenciesList();
            CurrenciesSpinnerAdapter adapter = new CurrenciesSpinnerAdapter(mContext, list,
                    CurrenciesSpinnerAdapter.ADD_LISTING_TYPE);
            mRentalSpinner.setAdapter(adapter);
            String defaultValue = mSettingsManager.getCurrency();
            selectDefault(mRentalSpinner, list, defaultValue);
        }
    }


    private void selectDefault(Spinner spinner, List<String> list, String defaultValue) {
        int position = list.indexOf(defaultValue);
        if (position != -1) {
            spinner.setSelection(position);
        }
    }


    //----------------------- Data pickers methods----------------//
    private void setUpDate(Date date, TextView textView) {
        if (date != null) {
            textView.setText(DateUtil.formatDate(date));
        }
    }


    private void setUpAvailableDate() {
        mDateIn = new Date();
        setUpDate(mDateIn, mAvailableDateTv);
        mLongTermRb.setChecked(true);
        onShortOrLong();
    }


    private void setUpLeaveByDate() {//leave by date should be 1 month after current date
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(java.util.Calendar.MONTH, ONE);
        mDateOut = calendar.getTime();
        setUpDate(mDateOut, mLeaveByTv);
    }


    private void storeDate(int type, int year, int month, int day) {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.set(year, month, day);
        if (type == LEAVE_BY_DATE) {
            mDateOut = calendar.getTime();
        } else {
            mDateIn = calendar.getTime();
        }
    }


    //------------------ Bathroom, Badroom, Min stay methods --------------------------//
    private CatalogModel getCatalogById(List<CatalogModel> catalogModels, Integer categoryId) {
        if (categoryId != null && categoryId != -1) {
            for (CatalogModel catalogModel : catalogModels) {
                if (catalogModel.getId() == categoryId) {
                    return catalogModel;
                }
            }
        }
        return null;
    }


    private void showChosenValue(List<CatalogModel> catalog, Integer categoryId, TextView valueTv) {
        CatalogModel catalogModel = getCatalogById(catalog, categoryId);
        if (catalogModel != null) {
            valueTv.setText(catalogModel.getName());
        } else {
            valueTv.setText("");
        }
    }


    private void showSharedDetailsDialog(List<CatalogModel> catalogList, Integer currentId,
                                         CatalogListChooserDialog.OKButtonClickListener okButtonClick, String title) {
        List<Integer> currentIdList = new ArrayList<>();
        currentIdList.add(currentId);
        CatalogListChooserDialog sharedDetailsChooserDialog = new CatalogListChooserDialog(getContext(), catalogList,
                currentIdList, false, okButtonClick, title);
        sharedDetailsChooserDialog.show();
    }


    private void storeListBedrooms() {
        List<CatalogModel> catalogModels = mCatalogsManager.getBedroomsList();
        bedroomIdList.clear();
        for (int i = 0; i < listBedroomType.size(); ++i) {
            if (listBedroomType.get(i).isChecked()) {
               bedroomIdList.add(catalogModels.get(i).getId());
            }
        }
    }


    @Nullable
    @OnClick(R.id.al_bedroom_layout)
    public void onBedroom() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getBedroomsList();
        if (catalogModels == null) {
            mNeedToShowBedroomsDialog = true;
            mCatalogRestService.loadCatalogs();
        } else {
            showSharedDetailsDialog(catalogModels, mBedroomId, new CatalogListChooserDialog.OKButtonClickListener() {

                @Override
                public void onOkClick(List<Integer> categoryIds) {
                    if (categoryIds != null && !categoryIds.isEmpty()) {
                        mBedroomId = categoryIds.get(0);
                        showChosenValue(catalogModels, categoryIds.get(0), mBedroomValueTv);
                    }
                }
            }, getContext().getString(R.string.al_bedrooms));
        }
    }


    @Nullable
    @OnClick(R.id.al_bath_layout)
    public void onBath() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getBathroomsList();
        if (catalogModels == null) {
            mNeedToShowBathDialog = true;
            mCatalogRestService.loadCatalogs();
        } else {
            showSharedDetailsDialog(catalogModels, mBathroomId, new CatalogListChooserDialog.OKButtonClickListener() {

                @Override
                public void onOkClick(List<Integer> categoryIds) {
                    if (categoryIds != null && !categoryIds.isEmpty()) {
                        mBathroomId = categoryIds.get(0);
                        showChosenValue(catalogModels, categoryIds.get(0), mBathValueTv);
                    }
                }
            }, getContext().getString(R.string.al_bath));
        }
    }


    @Nullable
    @OnClick(R.id.al_minimum_stay_layout)
    public void onMinStay() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getMinimumStayList();
        if (catalogModels == null) {
            mNeedToShowMinStayDialog = true;
            mCatalogRestService.loadCatalogs();
        } else {
            showSharedDetailsDialog(catalogModels, mMinStayId, new CatalogListChooserDialog.OKButtonClickListener() {

                @Override
                public void onOkClick(List<Integer> categoryIds) {
                    if (categoryIds != null && !categoryIds.isEmpty()) {
                        mMinStayId = categoryIds.get(0);
                        showChosenValue(catalogModels, categoryIds.get(0), mMinStayValueTv);
                    }
                }
            }, getContext().getString(R.string.al_minimum_stay));
        }
    }


    //-------------------------------- Save listing methods----------------------//


    public void saveListing(AddListing listener) {
        ListingPostViewModel listingViewModel = prepareModel();
        if (listingViewModel != null) {
            ((AddNewDetailedListingActivity) getActivity()).getAddNewListingSaveBtn().setClickable(false);
            mDiscoveryPrefsManager.storeLocationString(mLocationView.getText().toString());
            mMegaphoneManager.saveType(mType);
            mListingsRestService.addListing(listingViewModel, listener);
            listener.onAddListing();
        }

    }


    public ListingPostViewModel prepareModel() {
        ListingPostViewModel listingViewModel = new ListingPostViewModel();
        if (canBuildListViewModel(listingViewModel)) {
            if (mAdditionalFields != null && mAdditionalFields.getVisibility() == View.VISIBLE) {
                mAdditionalFields.saveSelection(listingViewModel);
            }
            return listingViewModel;
        }
        return null;
    }


    private boolean canBuildListViewModel(ListingPostViewModel listingViewModel) {
        boolean result = false;
        switch (mType) {
            case ListingType.OFFERING_ENTIRE_PLACE_TYPE:
                result = canBuildOfferEntirePlaceModel(listingViewModel);
                break;
            case ListingType.OFFERING_ROOM_TYPE:
                result = canBuildOfferRoomModel(listingViewModel);
                break;
            case ListingType.FINDING_ENTIRE_PLACE_TYPE:
                result = canBuildFindEntirePlaceModel(listingViewModel);
                break;
            case ListingType.FINDING_ROOM_TYPE:
                result = canBuildFindRoomModel(listingViewModel);
                break;
        }
        return result;
    }


    private boolean canBuildOfferEntirePlaceModel(ListingPostViewModel listingViewModel) {
        listingViewModel.setServiceType(ServiceTypeEnum.HaveApartment);
        boolean isCommonPropertiesBuilt = buildCommonProperties(listingViewModel);
        boolean isHaveApartmentBuilt = buildHaveApartment(listingViewModel);
        return isCommonPropertiesBuilt && isHaveApartmentBuilt;
    }


    private boolean canBuildOfferRoomModel(ListingPostViewModel listingViewModel) {
        listingViewModel.setServiceType(ServiceTypeEnum.HaveShare);
        return buildCommonProperties(listingViewModel);
    }


    private boolean canBuildFindEntirePlaceModel(ListingPostViewModel listingViewModel) {
        listingViewModel.setServiceType(ServiceTypeEnum.NeedApartment);
        boolean isCommonPropertiesBuilt = buildCommonProperties(listingViewModel);
        boolean isNeedApartmentBuilt = buildNeedApartment(listingViewModel);
        return isCommonPropertiesBuilt && isNeedApartmentBuilt;
    }


    private boolean canBuildFindRoomModel(ListingPostViewModel listingViewModel) {
        listingViewModel.setServiceType(ServiceTypeEnum.NeedRoom);
        return buildCommonProperties(listingViewModel);
    }


    private boolean buildRates(ListingPostViewModel listingViewModel) {
        String valueString = mRentalEt.getText().toString();
        if (valueString.isEmpty()) {
            Toast.makeText(getContext(), getString(R.string.alert_fill_the_rate), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            try {
                Rates rates = new Rates();
                rates.setCurrency((String) mRentalSpinner.getSelectedItem());
                rates.setMonthlyRate(Integer.parseInt(valueString));
                listingViewModel.setRates(rates);
                return true;
            } catch (NumberFormatException exception) {
                Toast.makeText(getContext(), getString(R.string.alert_fill_the_rate_correctly), Toast.LENGTH_SHORT)
                        .show();
                return false;
            }
        }
    }


    private boolean buildCommonProperties(ListingPostViewModel listingViewModel) {
        listingViewModel.setIsActive(true);
        listingViewModel.setShareDetails(new ShareDetails());
        boolean isAddressBuilt = buildAddress(listingViewModel);
        boolean isRatesBuilt = buildRates(listingViewModel);
        boolean isCalendarBuilt = buildCalendarOrGiveError(listingViewModel);
        return isAddressBuilt && isRatesBuilt && isCalendarBuilt;
    }


    private boolean buildAddress(ListingPostViewModel listingPostViewModel) {
        String addressString = mLocationView.getText().toString();
        if (addressString.isEmpty()) {
            Toast.makeText(getContext(), getString(R.string.alert_fill_the_address), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            listingPostViewModel.setFullAddress(addressString);
            return true;
        }
    }


    private boolean buildCalendarOrGiveError(ListingPostViewModel listingPostViewModel) {
        Date currentDate = new Date();
        if (mDateIn == null) {
            Toast.makeText(getContext(), getString(R.string.alert_available_date_is_empty), Toast.LENGTH_SHORT).show();
            return false;
        } else if (mDateOut != null && mDateOut.before(currentDate)) {
            Toast.makeText(getContext(), getString(R.string.alert_date_out_should_be_in_future), Toast.LENGTH_SHORT)
                    .show();
            return false;
        } else if (mDateIn != null && mDateOut != null && mDateIn.after(mDateOut)) {
            Toast.makeText(getContext(), getString(R.string.alert_date_out_before_date_in), Toast.LENGTH_SHORT)
                    .show();
            return false;
        } else {
            buildCalendar(listingPostViewModel);
            return true;
        }
    }


    private boolean buildLookingBedrooms(NeedApartment listingPostViewModel) {
        storeListBedrooms();
        listingPostViewModel.setApartmentTypeIds(bedroomIdList);
        return true;
    }


    private void buildCalendar(ListingPostViewModel listingPostViewModel) {
        Calendar calendar = new Calendar();
        calendar.setDateIn(specifyDataIn());
        if (mShortTermRb.isChecked()) {
            calendar.setDateOut(mDateOut);
        }
        if (mMinStayId != -1) {
            calendar.setMinimumStayId(mMinStayId);
        }
        listingPostViewModel.setCalendar(calendar);
    }


    private Date specifyDataIn() {
        //server doesn't support date in past
        Date currentDate = new Date();
        if (mDateIn.before(currentDate)) {
            return currentDate;
        }
        return mDateIn;
    }


    private boolean buildHaveApartment(ListingPostViewModel listingPostViewModel) {
        HaveApartment haveApartment = new HaveApartment();
        haveApartment.setIsFurnished(mFurnishedToggle.isChecked());
        haveApartment.setAptSizeUnits(((CatalogModel) mMeasurementSpinner.getSelectedItem()).getId());
        boolean areBathroomsBuilt = buildBathrooms(haveApartment);
        boolean areBedroomsBuilt = buildBedrooms(haveApartment);
        boolean areApartmentSizeBuilt = buildApartmentSize(haveApartment);
        if (areBathroomsBuilt && areBedroomsBuilt && areApartmentSizeBuilt) {
            listingPostViewModel.setHaveApartment(haveApartment);
            return true;
        }
        return false;
    }


    private boolean buildNeedApartment(ListingPostViewModel listingPostViewModel) {
        NeedApartment needApartment = new NeedApartment();
        needApartment.setIsFurnished(mFurnishedToggle.isChecked());
        boolean areBedroomsBuilt = buildLookingBedrooms(needApartment);
        if (areBedroomsBuilt) {
            listingPostViewModel.setNeedApartment(needApartment);
            return true;
        } else
            return false;
    }


    private boolean buildBathrooms(HaveApartment haveApartment) {
        if (mBathroomId != -1) {
            haveApartment.setBathroomsId(mBathroomId);
            return true;
        } else {
            Toast.makeText(getContext(), getString(R.string.alert_specify_bathrooms), Toast.LENGTH_SHORT).show();
            return false;
        }
    }


    private boolean buildBedrooms(HaveApartment haveApartment) {
        if (mBedroomId != -1) {
            haveApartment.setBedroomsId(mBedroomId);
            return true;
        } else {
            Toast.makeText(getContext(), getString(R.string.alert_specify_bedrooms), Toast.LENGTH_SHORT).show();
            return false;
        }
    }


    private boolean buildApartmentSize(HaveApartment haveApartment) {
        String sizeValue = mMeasurementEt.getText().toString();
        if (sizeValue.isEmpty()) {
            Toast.makeText(getContext(), getString(R.string.alert_apartment_type_is_empty), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            try {
                haveApartment.setAptSize(Integer.parseInt(sizeValue));
            } catch (NumberFormatException exception) {
                Toast.makeText(getContext(), getString(R.string.alert_apartment_type_invalid_number), Toast.LENGTH_SHORT).show();
            }
            return true;
        }
    }


    private PermissionsHelper getPermissionsHelper() {
        Activity activity = getActivity();
        if (activity instanceof PermissionsAware) {
            return ((PermissionsAware) activity).getPermissionsHelper();
        } else {
            return new PermissionsHelper(getActivity());
        }
    }
}