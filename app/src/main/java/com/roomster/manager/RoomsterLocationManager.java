package com.roomster.manager;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLngBounds;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;

import javax.inject.Inject;


/**
 * Created by Bogoi.Bogdanov on 3/18/2016.
 */
public class RoomsterLocationManager implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final int LOCATION_UPDATE_FASTEST_INTERVAL = 100;
    private static final int LOCATION_UPDATE_INTERVAL = 1000;
    public static final int REQUEST_PERMISSION_LAST_LOCATION = 123;
    public static final int REQUEST_PERMISSION_UPDATE_LOCATION = 124;

    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;

    private LocationRequest mLocationRequest;

    private LocationCallbacks mLocationCallbacks;

    @Inject
    Context mContext;

    private Activity mActivity;

    @Inject
    DiscoveryPrefsManager mDiscoveryPrefManager;


    public interface LocationCallbacks {

        void onLocationChanged(Location location);

        void onConnected();
    }


    public RoomsterLocationManager() {
        RoomsterApplication.getRoomsterComponent().inject(this);

        setInitialLocation();

        mGoogleApiClient = new GoogleApiClient.Builder(mContext).addConnectionCallbacks(this).addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).addApi(Places.GEO_DATA_API).build();
    }


    public void resetActivity() {
        mActivity = null;
    }


    public void setActivity(Activity activity) {
        this.mActivity = activity;
    }


    @Override
    public void onConnected(Bundle bundle) {
        if (mLocationCallbacks != null) {
            mLocationCallbacks.onConnected();
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(mContext, mContext.getString(R.string.google_api_error), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onLocationChanged(Location location) {
        mLocation = location;
        if (mLocationCallbacks != null) {
            mLocationCallbacks.onLocationChanged(location);
        }
    }


    public void startConnection(LocationCallbacks callbacks) {
        mLocationCallbacks = callbacks;
        mGoogleApiClient.connect();
    }


    public void stopConnection() {
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
            mGoogleApiClient.disconnect();
            mLocationCallbacks = null;
            mLocationRequest = null;
        }
    }


    public PendingResult<PlaceBuffer> getPlaceById(String placeId) {
        return Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
    }


    public Location getCurrentLocation() {
        return mLocation;
    }


    public String getLastAddress() {
        return mDiscoveryPrefManager.getLocationString();
    }


    public LatLngBounds getLastBounds() {
        if (!mDiscoveryPrefManager.isLocationEmpty()) {
            return mDiscoveryPrefManager.getLastBounds();
        }
        return null;
    }


    public GoogleApiClient getGoogleApiClient() {
        return mGoogleApiClient;
    }


    public void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mLocationRequest = null;
        }
    }


    public Location getLastKnownLocation() {
        Location lastLocation = null;
        if (checkSelfPermission()) {
            lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        } else {
            askForPermission(REQUEST_PERMISSION_LAST_LOCATION);
        }
        return lastLocation;
    }


    private boolean checkSelfPermission() {
        return (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }


    public void requestLocationUpdates() {
        mLocationRequest = LocationRequest.create().setPriority(LocationRequest.PRIORITY_LOW_POWER)
                .setInterval(LOCATION_UPDATE_INTERVAL).setFastestInterval(LOCATION_UPDATE_FASTEST_INTERVAL);
        if (isConnected() && mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            if (checkSelfPermission()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } else {
                askForPermission(REQUEST_PERMISSION_UPDATE_LOCATION);
            }
        }
    }


    private void askForPermission(int code) {
        if (mActivity != null) {
            ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, code);
        }
    }


    public boolean isConnected() {
        LocationManager lm = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }


    private void setInitialLocation() {
        mLocation = new Location("");
        if (!mDiscoveryPrefManager.isLocationEmpty()) {
            mLocation.setLatitude(mDiscoveryPrefManager.getLatitude());
            mLocation.setLongitude(mDiscoveryPrefManager.getLongitude());
        }
    }
}