package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class RatesViewModel implements Serializable {

    /**
     **/
    @SerializedName("requested_currency")
    private String requestedCurrency = null;

    /**
     **/
    @SerializedName("requested_monthly_rate")
    private Integer requestedMonthlyRate = null;

    /**
     * Allowed values @ /v1/catalogues/currencies
     **/
    @SerializedName("currency")
    private String currency = null;

    /**
     **/
    @SerializedName("monthly_rate")
    private Integer monthlyRate = null;

    /**
     **/
    @SerializedName("guests")
    private Integer guests = null;


    public String getRequestedCurrency() {
        return requestedCurrency;
    }


    public void setRequestedCurrency(String requestedCurrency) {
        this.requestedCurrency = requestedCurrency;
    }


    public Integer getRequestedMonthlyRate() {
        return requestedMonthlyRate;
    }


    public void setRequestedMonthlyRate(Integer requestedMonthlyRate) {
        this.requestedMonthlyRate = requestedMonthlyRate;
    }


    public String getCurrency() {
        return currency;
    }


    public void setCurrency(String currency) {
        this.currency = currency;
    }


    public Integer getMonthlyRate() {
        return monthlyRate;
    }


    public void setMonthlyRate(Integer monthlyRate) {
        this.monthlyRate = monthlyRate;
    }


    public Integer getGuests() {
        return guests;
    }


    public void setGuests(Integer guests) {
        this.guests = guests;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class RatesViewModel {\n");

        sb.append("  requestedCurrency: ").append(requestedCurrency).append("\n");
        sb.append("  requestedMonthlyRate: ").append(requestedMonthlyRate).append("\n");
        sb.append("  currency: ").append(currency).append("\n");
        sb.append("  monthlyRate: ").append(monthlyRate).append("\n");
        sb.append("  guests: ").append(guests).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
