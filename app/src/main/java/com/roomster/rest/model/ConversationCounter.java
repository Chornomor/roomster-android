package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


public class ConversationCounter {

    /**
     **/
    @SerializedName("label")
    private String label = null;

    /**
     **/
    @SerializedName("count_all")
    private Integer countAll = null;

    /**
     **/
    @SerializedName("count_new")
    private Integer countNew = null;


    public String getLabel() {
        return label;
    }


    public void setLabel(String label) {
        this.label = label;
    }


    public Integer getCountAll() {
        return countAll;
    }


    public void setCountAll(Integer countAll) {
        this.countAll = countAll;
    }


    public Integer getCountNew() {
        return countNew;
    }


    public void setCountNew(Integer countNew) {
        this.countNew = countNew;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ConversationCounter {\n");

        sb.append("  label: ").append(label).append("\n");
        sb.append("  countAll: ").append(countAll).append("\n");
        sb.append("  countNew: ").append(countNew).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
