package com.roomster.utils;


import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.roomster.R;

import de.hdodenhof.circleimageview.CircleImageView;

import jp.wasabeef.glide.transformations.CropCircleTransformation;


/**
 * Created by emil on 9/14/15. Email: emil@alttab.co
 * <p/>
 * Project: Roomster-Android
 * <p/>
 * Copyright 2015 (c)
 */
public class ImageHelper {

    public static void loadImageInto(Context context, String url, ImageView imageView) {
        if (!canLoadImage(context)) {
            return;
        }
        Glide.with(context).load(url).into(imageView);
    }


    public static void loadCircledImageInto(Context context, String url, ImageView imageView) {
        if (!canLoadImage(context)) {
            return;
        }
        imageView.setBackgroundResource(0);

        imageView.setPadding(0, 0, 0, 0);

        Glide.with(context).load(url).bitmapTransform(new CropCircleTransformation(context)).placeholder(R.mipmap.ic_launcher)
          .into(imageView);
    }


    public static void loadBorderedCircledImageInto(Context context, String url, ImageView imageView) {
        if (!canLoadImage(context)) {
            return;
        }
        imageView.setBackgroundResource(R.drawable.white_circle);
        int borderSize = (int) context.getResources().getDimension(R.dimen.image_helper_circle_border_size);
        imageView.setPadding(borderSize, borderSize, borderSize, borderSize);
        Glide.with(context).load(url).bitmapTransform(new CropCircleTransformation(context)).into(imageView);
    }


    public static void loadGreenBorderedCircledImageInto(Context context, String url, ImageView imageView) {
        if (!canLoadImage(context)) {
            return;
        }
        imageView.setBackgroundResource(R.drawable.ic_new_message_indicator);
        int borderSize = (int) context.getResources().getDimension(R.dimen.image_helper_green_circle_border_size);
        imageView.setPadding(borderSize, borderSize, borderSize, borderSize);
        Glide.with(context).load(url).bitmapTransform(new CropCircleTransformation(context)).into(imageView);
    }


    public static void loadScaledImageInto(Context context, String url, ImageView imageView) {
        if (!canLoadImage(context)) {
            return;
        }
        if (url != null && !url.equals("")) {
            int size = (int) Math.ceil(Math.sqrt(800 * 600));
            Glide.with(context).load(url).override(600, 800).centerCrop().into(imageView);
        }
    }


    public static void loadCircleImageIntoMapMarker(Context context, String url, CircleImageView imageView,final MapImageLoadedListener listener) {
        if (!canLoadImage(context)) {
            return;
        }
        int imageSize = context.getResources().getDimensionPixelOffset(R.dimen.marker_width);
        Glide.with(context).load(url).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                e.printStackTrace();
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                if(listener != null) {
                    listener.onMapImageLoaded(model,resource);
                }
                return false;
            }
        }).bitmapTransform(new CropCircleTransformation(context))
                .placeholder(R.drawable.ic_profile)
          .override(imageSize, imageSize).into(imageView);
    }

    private static boolean canLoadImage(Context context) {
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            if (activity.isFinishing()) {
                return false;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if (activity.isDestroyed()) {
                    return false;
                }
            }
        }
        return true;
    }

    public interface MapImageLoadedListener {
         void onMapImageLoaded(String url, Drawable drawable);
    }
}
