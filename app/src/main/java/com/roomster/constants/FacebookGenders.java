package com.roomster.constants;


/**
 * Created by Bogoi.Bogdanov on 1/13/2016.
 */
public class FacebookGenders {

    public static final String MALE   = "male";
    public static final String FEMALE = "female";
}
