package com.roomster.views;


import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.roomster.R;
import com.roomster.adapter.BasePageAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by Katkov on 10/10/2015.
 */
public class ViewPagerWithIndicators extends FrameLayout {

    private static final int OFFSCREEN_PAGE_LIMIT = 4;

    private BasePageAdapter          mAdapter;
    private int                      mIndicatorsCount;
    private BottomViewExpandListener mListener;

    private boolean mIsScrollableDown = true;
    private int     mCurrentPosition  = 0;

    @Bind(R.id.vertical_pager)
    NonSwipableVerticalViewPager mViewPager;

    @Bind(R.id.vertical_pager_indicator)
    LinearLayout mIndicatorLayout;

    public interface BottomViewExpandListener {

        void onShouldExpand();

        void onShouldCollapse();
    }

    public ViewPagerWithIndicators(Context context) {
        super(context);
        initView();
    }


    public ViewPagerWithIndicators(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (mListener != null) {
            mListener.onShouldCollapse();
        }
        return super.onInterceptTouchEvent(ev);
    }


    public void setAdapter(BasePageAdapter pagerAdapter, int indicatorsCount) {
        if (mAdapter != null) {
            clearIndicatiors();
        }
        mIndicatorsCount = indicatorsCount;

        mAdapter = pagerAdapter;
        mViewPager.setAdapter(pagerAdapter);
        mViewPager.setOnPageChangeListener(onPageChangeListener);
        mViewPager.setOffscreenPageLimit(OFFSCREEN_PAGE_LIMIT);
        mViewPager.destroyDrawingCache();
        mAdapter.notifyDataSetChanged();
        mViewPager.invalidate();
        initIndicators();
    }

    public void setCurrentPositionSelected() {
        setPositionSelected(mCurrentPosition);
        mViewPager.setPagingEnabled(true);
    }


    public void setInfoPositionSelected() {
        if (mAdapter != null) {
            setPositionSelected(mAdapter.getCount());
            mViewPager.setPagingEnabled(false);
        }
    }

    public void setOnBottomViewExpandListener(BottomViewExpandListener listener) {
        mListener = listener;
    }


    public void setPositionSelected(int position) {
        if (0 <= position && position < mIndicatorLayout.getChildCount()) {
            for (int i = 0; i < mIndicatorLayout.getChildCount(); i++) {
                ((ImageView) mIndicatorLayout.getChildAt(i)).setImageResource(mAdapter.getDiselectedIndicator(i));
            }
            ((ImageView) mIndicatorLayout.getChildAt(position)).setImageResource(mAdapter.getSelectedIndicator(position));
        }
    }

    private void initView() {
        inflate(getContext(), R.layout.view_pager_with_indicators, this);
        ButterKnife.bind(this);
    }


    private void initIndicators() {
        for (int i = 0; i < mIndicatorsCount; i++) {
            ImageView imageView = (ImageView) LayoutInflater.from(getContext()).inflate(R.layout.image_view_indicator, mIndicatorLayout, false);
            if (i == 0) {
                imageView.setImageResource(mAdapter.getSelectedIndicator(i));
            } else {
                imageView.setImageResource(mAdapter.getDiselectedIndicator(i));
            }
            mIndicatorLayout.addView(imageView);
        }
    }


    private void clearIndicatiors() {
        mIndicatorLayout.removeAllViews();
    }


    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            checkIfNeedToExpand(position);
        }


        @Override
        public void onPageSelected(int position) {
            setPositionSelected(position);
            mCurrentPosition = position;
        }


        @Override
        public void onPageScrollStateChanged(int state) {
            mIsScrollableDown = true;
        }
    };


    private void checkIfNeedToExpand(int position) {
        if (position == mAdapter.getCount() - 1) {
            if (!mIsScrollableDown) {
                mListener.onShouldExpand();
                setPositionSelected(mAdapter.getCount());
                mViewPager.setPagingEnabled(false);
            }
            mIsScrollableDown = false;
        }
    }
}