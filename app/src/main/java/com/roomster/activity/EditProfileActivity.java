package com.roomster.activity;


import com.google.android.gms.analytics.HitBuilders;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.roomster.R;
import com.roomster.adapter.CountryCatalogAdapter;
import com.roomster.adapter.LanguagesAdapter;
import com.roomster.adapter.PhotoGridAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.FacebookGenders;
import com.roomster.dialog.ImageChooserDialog;
import com.roomster.dialog.PickerDialog;
import com.roomster.enums.SocialNetworkEnum;
import com.roomster.event.RestServiceEvents.AccountRestServiceEvent;
import com.roomster.event.RestServiceEvents.CatalogRestServiceEvent;
import com.roomster.event.RestServiceEvents.ImageRestServiceEvent;
import com.roomster.event.RestServiceEvents.MeRestServiceEvent;
import com.roomster.event.updateprofle.UpdateProfileEvent;
import com.roomster.manager.CatalogsManager;
import com.roomster.manager.FacebookManager;
import com.roomster.manager.MeManager;
import com.roomster.model.CountryCatalogModel;
import com.roomster.rest.model.AccountDetailsViewModel;
import com.roomster.rest.model.Photo;
import com.roomster.rest.model.SocialConnectionViewModel;
import com.roomster.rest.model.UserContactViewModel;
import com.roomster.rest.model.UserGetViewModel;
import com.roomster.rest.service.AccountRestService;
import com.roomster.rest.service.CatalogRestService;
import com.roomster.rest.service.ImagesRestService;
import com.roomster.rest.service.MeRestService;
import com.roomster.utils.LogUtil;
import com.roomster.utils.PermissionsHelper;
import com.roomster.views.ExpandableHeightGridView;
import com.roomster.views.ImagePager;
import com.roomster.views.NonEditableEditText;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;


/**
 * Created by Bogoi on 12/22/2015.
 */
public class EditProfileActivity extends BaseActivity implements ImageChooserListener, ImagePager.ImageClickListener {

    private static final int COUNTRY_ISO_LENGTH = 2;
    private final int MAX_PHONE_LENGTH = 13;
    private static final int ABOUT_TEXT_MAX_CHARS = 300;
    private static final String USER = "User";

    private UserGetViewModel mCurrentUser;
    private ImageChooserManager mImageChooserManager;
    private ImageChooserDialog mImageChooserDialog;
    private int mChooserType;
    private String mFilePath;
    private PickerDialog mCountryPicker;

    @Bind(R.id.global_edit_profile_sv)
    ScrollView mScrollView;

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.tv_title)
    TextView mTitleTv;

    @Bind(R.id.images_pager)
    ImagePager mImagesPager;

    @Bind(R.id.name_and_age)
    TextView mNameAndAgeTextView;

    @Bind(R.id.gender_name)
    TextView mGenderNameTextView;

    @Bind(R.id.gender_image)
    ImageView mGenderImage;

    @Bind(R.id.about_character_count)
    TextView mAboutTextCharacterCountTV;

    @Bind(R.id.about_edit_text)
    EditText mAboutEditText;

    @Bind(R.id.languages_container)
    ExpandableHeightGridView mLanguagesContainer;

    @Bind(R.id.phone_number_code)
    TextView mPhoneNumberCodeTextView;

    @Bind(R.id.phone_number_code_label)
    TextView mPhoneNumberCodeLabelTextView;

    @Bind(R.id.phone_number_edit_text)
    EditText mPhoneNumberEditText;

    @Bind(R.id.phone_toggle)
    ToggleButton mPhoneToggle;

    @Bind(R.id.facebook_edit_text)
    EditText mFacebookEditText;

    @Bind(R.id.facebook_toggle)
    ToggleButton mFacebookToggle;

    @Bind(R.id.twitter_edit_text)
    EditText mTwitterEditText;

    @Bind(R.id.twitter_toggle)
    ToggleButton mTwitterToggle;

    @Bind(R.id.instagram_edit_text)
    EditText mInstagramEditText;

    @Bind(R.id.instagram_toggle)
    ToggleButton mInstagramToggle;

    @Bind(R.id.linkedin_edit_text)
    EditText mLinkedInEditText;

    @Bind(R.id.linkedin_toggle)
    ToggleButton mLinkedInToggle;

    @Inject
    MeRestService mMeRestService;

    @Inject
    MeManager mMeManager;

    @Inject
    CatalogsManager mCatalogsManager;

    @Inject
    CatalogRestService mCatalogRestService;

    @Inject
    AccountRestService mAccountRestService;

    @Inject
    ImagesRestService mImagesRestService;

    @Inject
    FacebookManager mFacebookManager;

    @Inject
    Context mContext;


    @Override
    protected int getMainContentLayout() {
        return R.layout.activity_edit_profile;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        ButterKnife.bind(this);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        topLevelFocus();
        setUpScrollView();
        mMeRestService.requestMe();
    }


    private void setUpScrollView() {
        mScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                onClickAway();
                return false;
            }
        });

    }


    protected void onClickAway() {
        //hide soft keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }


    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName(getString(R.string.screen_name_edit_my_profile));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
            if (mImageChooserManager == null) {
                reinitializeImageChooser();
            }
            mImageChooserManager.submit(requestCode, data);
        } else {
            initImageGrid();
        }
    }


    @Override
    public void onImageChosen(final ChosenImage image) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                String imageUrl = image.getFileThumbnail();
                if (imageUrl == null) {
                    imageUrl = image.getFilePathOriginal();
                }
                mImagesRestService.postImage(USER, null, imageUrl);
            }
        });
    }


    @Override
    public void onError(String s) {

    }


    @Override
    public void onImageClick(Photo photo) {
        if (photo.getPath().equals(PhotoGridAdapter.ADD_NEW_IMAGE_PLACEHOLDER)) {
            showImageChooserDialog();
        } else {
            mImagesRestService.deleteImage(USER, photo.getId());
        }
    }


    @OnClick(R.id.tv_done)
    void onToolbarDoneClick() {
        saveProfileInfo();
    }


    @OnClick(R.id.phone_number_code)
    void onPhoneCodeClick() {
        if (mCountryPicker != null) {
            mCountryPicker.show();
        }
    }


    @OnTextChanged(R.id.twitter_edit_text)
    public void onTwitterUrlChange(CharSequence text) {
        if (text.toString().equals(getString(R.string.twitter_url_placeholder)) || text.toString().equals("")) {
            mTwitterEditText.setTextColor(ContextCompat.getColor(mContext, R.color.disabled_green));
            mTwitterToggle.setEnabled(false);
            mTwitterToggle.setChecked(false);
        } else {
            mTwitterEditText.setTextColor(ContextCompat.getColor(mContext, R.color.dark_green_text));
            mTwitterToggle.setEnabled(true);
            mTwitterToggle.setChecked(true);
        }
    }


    @OnTextChanged(R.id.instagram_edit_text)
    public void onInstagramUrlChange(CharSequence text) {
        if (text.toString().equals(getString(R.string.instagram_url_placeholder)) || text.toString().equals("")) {
            mInstagramEditText.setTextColor(ContextCompat.getColor(mContext, R.color.disabled_green));
            mInstagramToggle.setEnabled(false);
            mInstagramToggle.setChecked(false);
        } else {
            mInstagramEditText.setTextColor(ContextCompat.getColor(mContext, R.color.dark_green_text));
            mInstagramToggle.setEnabled(true);
            mInstagramToggle.setChecked(true);
        }
    }


    @OnTextChanged(R.id.linkedin_edit_text)
    public void onLinkedInUrlChange(CharSequence text) {
        if (text.toString().equals(getString(R.string.linkedin_url_placeholder)) || text.toString().equals("")) {
            mLinkedInEditText.setTextColor(ContextCompat.getColor(mContext, R.color.disabled_green));
            mLinkedInToggle.setEnabled(false);
            mLinkedInToggle.setChecked(false);
        } else {
            mLinkedInEditText.setTextColor(ContextCompat.getColor(mContext, R.color.dark_green_text));
            mLinkedInToggle.setEnabled(true);
            mLinkedInToggle.setChecked(true);
        }
    }


    @OnTextChanged(R.id.about_edit_text)
    void onAboutTextChanged(CharSequence text) {
        String aboutText = text.toString();
        int remainingChars = getRemainingCharactersCount(aboutText);
        mAboutTextCharacterCountTV.setText(Integer.toString(remainingChars));

        //Make the first letter of the "about me" text capital.
        if (text.length() == 1 && Character.isLowerCase(text.charAt(0))) {
            mAboutEditText.setText(aboutText.toUpperCase());
            mAboutEditText.setSelection(1);
        }
    }


    @OnTextChanged(R.id.phone_number_edit_text)
    void onPhoneTextChanged(CharSequence text) {
        if (text.toString().isEmpty()) {
            mPhoneToggle.setChecked(false);
            mPhoneToggle.setEnabled(false);
        } else {
            mPhoneToggle.setChecked(true);
            mPhoneToggle.setEnabled(true);
        }
    }


    @OnClick(R.id.phone_number_code_label)
    void onPhoneNumberLabelClick() {
        if (mCountryPicker != null) {
            mCountryPicker.show();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(AccountRestServiceEvent.UpdateSuccess event) {
        Toast.makeText(this, getString(R.string.profile_info_update_success), Toast.LENGTH_SHORT).show();
        mEventBus.post(new UpdateProfileEvent());
        onBackPressed();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(AccountRestServiceEvent.UpdateFailure event) {
        Toast.makeText(this, getString(R.string.profile_info_update_fail), Toast.LENGTH_SHORT).show();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(MeRestServiceEvent.GetMeSuccess event) {
        mCurrentUser = mMeManager.getMe();
        initView();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(CatalogRestServiceEvent.GetCatalogSuccess event) {
        initLanguages();
        initCountrySpinner();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ImageRestServiceEvent.ImageDeleteSuccess event) {
        mMeRestService.requestMe();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ImageRestServiceEvent.ImageDeleteFailed event) {
        initImageGrid();
        Toast.makeText(this, getString(R.string.failed_to_delete_image), Toast.LENGTH_SHORT).show();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ImageRestServiceEvent.ImageAddSuccess event) {
        mMeRestService.requestMe();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ImageRestServiceEvent.ImageAddFailed event) {
        initImageGrid();
        Toast.makeText(this, getString(event.errorMessageId), Toast.LENGTH_SHORT).show();
    }


    private void showImageChooserDialog() {
        mImageChooserDialog = new ImageChooserDialog(this, new ImageChooserDialog.Listener() {

            @Override
            public void onFacebookClick() {

            }


            @Override
            public void onCameraClick() {
                mImageChooserDialog.dismiss();
                getPermissionsHelper().actionOnCameraPermission(new PermissionsHelper.OnCameraPermissionActionListener() {

                    @Override
                    public void onCameraPermissionGranted() {

                        getPermissionsHelper().actionOnStoragePermission(new PermissionsHelper.OnStoragePermissionActionListener() {
                            @Override
                            public void onStoragePermissionGranted() {
                                takePicture();
                            }

                            @Override
                            public void onStoragePermissionDenied() {
                                //TODO: Fix this hack - we need to remove all loading indicators from the grid
                                initImageGrid();
                            }
                        });

                    }


                    @Override
                    public void onCameraPermissionDenied() {
                        //TODO: Fix this hack - we need to remove all loading indicators from the grid
                        initImageGrid();
                    }
                });
            }


            @Override
            public void onGalleryClick() {
                mImageChooserDialog.dismiss();
                getPermissionsHelper().actionOnStoragePermission(new PermissionsHelper.OnStoragePermissionActionListener() {

                    @Override
                    public void onStoragePermissionGranted() {
                        chooseImage();
                    }


                    @Override
                    public void onStoragePermissionDenied() {
                        //TODO: Fix this hack - we need to remove all loading indicators from the grid
                        initImageGrid();
                    }
                });
            }


            @Override
            public void onCancel() {
                mImageChooserDialog.dismiss();
                initImageGrid();
            }
        });
        mImageChooserDialog.show();
    }


    private void chooseImage() {
        mChooserType = ChooserType.REQUEST_PICK_PICTURE;
        mImageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_PICK_PICTURE, true);
        mImageChooserManager.setImageChooserListener(this);
        mImageChooserManager.clearOldFiles();
        try {
            mFilePath = mImageChooserManager.choose();
        } catch (Exception e) {

        }
    }


    private void takePicture() {
        mChooserType = ChooserType.REQUEST_CAPTURE_PICTURE;
        mImageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_CAPTURE_PICTURE, true);
        mImageChooserManager.setImageChooserListener(this);
        try {
            mFilePath = mImageChooserManager.choose();
        } catch (Exception e) {

        }
    }


    private void reinitializeImageChooser() {
        mImageChooserManager = new ImageChooserManager(this, mChooserType, true);
        mImageChooserManager.setImageChooserListener(this);
        mImageChooserManager.reinitialize(mFilePath);
    }


    private void initView() {
        setSupportActionBar(mToolbar);
        setUpActionBar();
        mTitleTv.setText(getString(R.string.edit_profile_title));
        if (mCurrentUser != null) {
            initImageGrid();
            initNameAndAge();
            initGender();
            initAbout();
            initLanguages();
            initPhone();
            initSocialConnections();
        }
    }


    private void setUpActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeButtonEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(false);
        }
    }


    private void initImageGrid() {
        if (mCurrentUser != null) {
            List<Photo> userPhotos = mCurrentUser.getPhotos();
            mImagesPager.setAdapter(getSupportFragmentManager(), userPhotos, this);
        }
    }


    private void initGender() {
        if (mFacebookManager.getCurrentUser() != null) {

            String gender = mFacebookManager.getCurrentUser().getGender();
            if (gender == null) {
                return;
            }

            switch (gender.toLowerCase()) {
                case FacebookGenders.FEMALE:
                    mGenderImage.setBackgroundResource(R.drawable.ic_female);
                    mGenderNameTextView.setText(getString(R.string.female));
                    break;
                case FacebookGenders.MALE:
                    mGenderImage.setBackgroundResource(R.drawable.ic_male);
                    mGenderNameTextView.setText(getString(R.string.male));
                    break;
                default:
                    break;
            }
        }
    }


    private void initNameAndAge() {
        StringBuilder nameAndAge = new StringBuilder();
        if (mCurrentUser != null) {
            if (mCurrentUser.getFirstName() != null) {
                nameAndAge.append(mCurrentUser.getFirstName());
            }
            if (mCurrentUser.getAge() != null) {
                nameAndAge.append(", ");
                nameAndAge.append(mCurrentUser.getAge());
            }
        }
        mNameAndAgeTextView.setText(nameAndAge.toString());
    }


    private void initAbout() {
        String description = "";
        if (mCurrentUser != null && mCurrentUser.getDescription() != null && !mCurrentUser.getDescription().trim().equals("")) {
            mAboutEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(ABOUT_TEXT_MAX_CHARS)});
            description = mCurrentUser.getDescription();
            mAboutEditText.setText(description);
        }
        mAboutTextCharacterCountTV.setText(Integer.toString(getRemainingCharactersCount(description)));
    }


    private void initLanguages() {
        if (mCurrentUser != null && mCatalogsManager.getSpokenLanguagesList() != null) {
            List<String> languages = mCurrentUser.getSpokenLanguages();
            mLanguagesContainer.setAdapter(new LanguagesAdapter(this, languages));
            mLanguagesContainer.setExpanded(true);
        }
    }


    private void initPhone() {
        if (mCatalogsManager.getCountriesList() != null) {
            initCountrySpinner();
        } else {
            mCatalogRestService.loadCatalogs();
        }

        if (mCurrentUser.getContactPhone() != null) {
            mPhoneNumberEditText.setText(mCurrentUser.getContactPhone().getPhone());
            if (mCurrentUser.getContactPhone().getIsVisible()) {
                mPhoneToggle.setChecked(true);
            } else {
                mPhoneToggle.setChecked(false);
            }
        } else {
            mPhoneToggle.setChecked(false);
            mPhoneToggle.setEnabled(false);
        }
    }


    private void initCountrySpinner() {
        if (mCatalogsManager.getCountriesList() != null) {
            initCountryDialog();

            if (mCurrentUser.getContactPhone() != null) {
                mPhoneNumberCodeLabelTextView
                        .setText(getCountryShortNameByPhoneCode(mCurrentUser.getContactPhone().getCountryCode()));
                mPhoneNumberCodeTextView.setText(mCurrentUser.getContactPhone().getCountryCode());
            } else {
                TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
                String countryCode = manager.getNetworkCountryIso().toUpperCase();
                if (countryCode.length() == COUNTRY_ISO_LENGTH) {
                    mPhoneNumberCodeLabelTextView.setText(countryCode);
                    mPhoneNumberCodeTextView.setText(getCountryPhoneCodeByShortName(countryCode));
                }
            }
        }
    }


    private void initCountryDialog() {
        final CountryCatalogAdapter adapter = new CountryCatalogAdapter(this, mCatalogsManager.getCountriesList());
        mCountryPicker = new PickerDialog(this, adapter, R.string.megaphone_select_country,
                new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        CountryCatalogModel selectedItem = ((CountryCatalogModel) adapter.getItem(position));
                        mPhoneNumberCodeTextView.setText(selectedItem.getPhoneCode());
                        mPhoneNumberCodeLabelTextView.setText(selectedItem.getShortName());
                    }
                });
    }


    private String getCountryShortNameByPhoneCode(String phoneCode) {
        if (mCatalogsManager.getCountriesList() != null) {
            for (CountryCatalogModel country : mCatalogsManager.getCountriesList()) {
                if (country.getPhoneCode().equals(phoneCode)) {
                    return country.getShortName();
                }
            }
        }
        return null;
    }


    private String getCountryPhoneCodeByShortName(String shortName) {
        if (mCatalogsManager.getCountriesList() != null) {
            for (CountryCatalogModel country : mCatalogsManager.getCountriesList()) {
                if (country.getShortName().equals(shortName)) {
                    return country.getPhoneCode();
                }
            }
        }
        return null;
    }


    private void initSocialConnections() {
        initOnFocusChangedListener();
        initOnKeyListener();
        List<SocialConnectionViewModel> socialConnections = mCurrentUser.getSocialConnections();
        for (SocialConnectionViewModel connection : socialConnections) {
            if (connection.getSocialNetwork() != null) {
                switch (connection.getSocialNetwork()) {
                    case Facebook:
                        initFacebook(connection);
                        break;
                    case Twitter:
                        initTwitter(connection);
                        break;
                    case LinkedIn:
                        initLinkedIn(connection);
                        break;
                    case Instagram:
                        initInstagram(connection);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    
    private void initOnFocusChangedListener() {
        EditText.OnFocusChangeListener listener = new EditText.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    ((EditText) v).setTextColor(ContextCompat.getColor(mContext, R.color.dark_green_text));
                    checkFocusEditText(v);
                } else {
                    v.clearFocus();
                    checkHintEditText(v);
                }
            }
        };
        mFacebookEditText.setOnFocusChangeListener(listener);
        mTwitterEditText.setOnFocusChangeListener(listener);
        mLinkedInEditText.setOnFocusChangeListener(listener);
        mInstagramEditText.setOnFocusChangeListener(listener);
    }

    private void initOnKeyListener() {
        View.OnKeyListener listener = new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    hideKeyboard();
                    return true;
                }
                return false;
            }
        };
        mFacebookEditText.setOnKeyListener(listener);
        mTwitterEditText.setOnKeyListener(listener);
        mLinkedInEditText.setOnKeyListener(listener);
        mInstagramEditText.setOnKeyListener(listener);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mFacebookEditText.getWindowToken(), 0);
    }


    private void checkFocusEditText(View v) {
        if (v == mTwitterEditText) {
            if (((EditText) v).getText().toString().isEmpty()) {
                ((EditText) v).setText(getString(R.string.twitter_url_placeholder));
            }
        }
        if (v == mLinkedInEditText) {
            if (((EditText) v).getText().toString().isEmpty()) {
                ((EditText) v).setText(getString(R.string.linkedin_url_placeholder));
            }
        }
        if (v == mInstagramEditText) {
            if (((EditText) v).getText().toString().isEmpty()) {
                ((EditText) v).setText(getString(R.string.instagram_url_placeholder));
            }
        }
    }

    private void checkHintEditText(View v) {
        if (v == mTwitterEditText) {
            if (((EditText) v).getText().toString().equals(getString(R.string.twitter_url_placeholder))) {
                ((NonEditableEditText) v).resetText();
                ((EditText) v).setHint(getString(R.string.twitter_url_hint));
            } else {
                ((EditText) v).setText(((EditText) v).getText());
            }
        }
        if (v == mLinkedInEditText) {
            if (((EditText) v).getText().toString().equals(getString(R.string.linkedin_url_placeholder))) {
                ((NonEditableEditText) v).resetText();
                ((EditText) v).setHint(getString(R.string.linkedin_url_hint));
            } else {
                ((EditText) v).setText(((EditText) v).getText());
            }
        }
        if (v == mInstagramEditText) {
            if (((EditText) v).getText().toString().equals(getString(R.string.instagram_url_placeholder))) {
                ((NonEditableEditText) v).resetText();
                ((EditText) v).setHint(getString(R.string.instagram_url_hint));
            } else {
                ((EditText) v).setText(((EditText) v).getText());
            }
        }
    }

    private void initFacebook(SocialConnectionViewModel connection) {
        mFacebookEditText.setText(connection.getProfileUrl());

        if (connection.getIsVisible()) {
            mFacebookToggle.setChecked(true);
        } else {
            mFacebookToggle.setChecked(false);
        }
    }


    private void initTwitter(SocialConnectionViewModel connection) {
        mTwitterEditText.setText(connection.getProfileUrl());

        if (connection.getIsVisible()) {
            mTwitterToggle.setChecked(true);
        } else {
            mTwitterToggle.setChecked(false);
        }
    }


    private void initLinkedIn(SocialConnectionViewModel connection) {
        mLinkedInEditText.setText(connection.getProfileUrl());

        if (connection.getIsVisible()) {
            mLinkedInToggle.setChecked(true);
        } else {
            mLinkedInToggle.setChecked(false);
        }
    }


    private void initInstagram(SocialConnectionViewModel connection) {
        mInstagramEditText.setText(connection.getProfileUrl());

        if (connection.getIsVisible()) {
            mInstagramToggle.setChecked(true);
        } else {
            mInstagramToggle.setChecked(false);
        }
    }


    private void saveProfileInfo() {
        AccountDetailsViewModel accountDetailsViewModel = new AccountDetailsViewModel();

        String aboutText = getAboutText();
        accountDetailsViewModel.setAbout(aboutText);

        if (mLanguagesContainer.getAdapter() != null) {
            accountDetailsViewModel.setSpokenLanguages(((LanguagesAdapter) mLanguagesContainer.getAdapter()).getLanguageIds());
        }

        List<SocialConnectionViewModel> socialConnections = getSocialConnectionsList();
        accountDetailsViewModel.setSocialConnections(socialConnections);

        UserContactViewModel contactPhone = getUserContactViewModel();
        accountDetailsViewModel.setContactPhone(contactPhone);

        mAccountRestService.updateAccount(accountDetailsViewModel);

        //Local caching of the current user
        List<String> languages = null;
        if (mLanguagesContainer.getAdapter() != null) {
            languages = ((LanguagesAdapter) mLanguagesContainer.getAdapter()).getLanguages();
        }

        cacheCurrentUser(aboutText, socialConnections, contactPhone, languages);
    }


    private String getAboutText() {
        return mAboutEditText.getText().toString();
    }


    private List<SocialConnectionViewModel> getSocialConnectionsList() {
        List<SocialConnectionViewModel> socialConnectionsList = new ArrayList<>();

        String facebookUrl = mFacebookEditText.getText().toString();
        if (!facebookUrl.equals(getString(R.string.facebook_url_placeholder))) {
            boolean isFacebookVisible = mFacebookToggle.isChecked();

            SocialConnectionViewModel facebookConnectionViewModel = new SocialConnectionViewModel();
            facebookConnectionViewModel.setIsVisible(isFacebookVisible);
            facebookConnectionViewModel.setProfileUrl(facebookUrl);

            facebookConnectionViewModel.setSocialNetwork(SocialNetworkEnum.Facebook);
            socialConnectionsList.add(facebookConnectionViewModel);
        }

        String twitterUrl = mTwitterEditText.getText().toString();
        if (!twitterUrl.equals(getString(R.string.twitter_url_placeholder))) {
            boolean isTwitterVisible = mTwitterToggle.isChecked();

            SocialConnectionViewModel twitterConnectionViewModel = new SocialConnectionViewModel();
            twitterConnectionViewModel.setIsVisible(isTwitterVisible);
            twitterConnectionViewModel.setProfileUrl(twitterUrl);

            twitterConnectionViewModel.setSocialNetwork(SocialNetworkEnum.Twitter);
            socialConnectionsList.add(twitterConnectionViewModel);
        }

        String instagramUrl = mInstagramEditText.getText().toString();
        if (!instagramUrl.equals(getString(R.string.instagram_url_placeholder))) {
            boolean isInstagramVisible = mInstagramToggle.isChecked();

            SocialConnectionViewModel instagramConnectionViewModel = new SocialConnectionViewModel();
            instagramConnectionViewModel.setIsVisible(isInstagramVisible);
            instagramConnectionViewModel.setProfileUrl(instagramUrl);

            instagramConnectionViewModel.setSocialNetwork(SocialNetworkEnum.Instagram);
            socialConnectionsList.add(instagramConnectionViewModel);
        }

        String linkedinUrl = mLinkedInEditText.getText().toString();
        if (!linkedinUrl.equals(getString(R.string.linkedin_url_placeholder))) {
            boolean isLinkedInVisible = mLinkedInToggle.isChecked();

            SocialConnectionViewModel linkedinConnectionViewModel = new SocialConnectionViewModel();
            linkedinConnectionViewModel.setIsVisible(isLinkedInVisible);
            linkedinConnectionViewModel.setProfileUrl(linkedinUrl);
            linkedinConnectionViewModel.setSocialNetwork(SocialNetworkEnum.LinkedIn);

            socialConnectionsList.add(linkedinConnectionViewModel);
        }

        return socialConnectionsList;
    }


    private int getRemainingCharactersCount(String text) {
        if (text != null) {
            return ABOUT_TEXT_MAX_CHARS - text.length();
        }
        return ABOUT_TEXT_MAX_CHARS;
    }


    private UserContactViewModel getUserContactViewModel() {
        boolean isPhoneVisible = mPhoneToggle.isChecked();
        String countryShortName = mPhoneNumberCodeLabelTextView.getText().toString();
        String phoneNumber = mPhoneNumberEditText.getText().toString();
        String countryCode = null;
        Integer country_id = null;
        List<CountryCatalogModel> countryCatalogModels = mCatalogsManager.getCountriesList();
        if (countryCatalogModels != null) {
            for (CountryCatalogModel country : countryCatalogModels) {
                if (country.getShortName().equals(countryShortName)) {
                    countryCode = country.getPhoneCode();
                    country_id = country.getId();
                    break;
                }
            }
        }

        UserContactViewModel userContactViewModel = new UserContactViewModel();
        userContactViewModel.setIsVisible(isPhoneVisible);
        userContactViewModel.setPhone(phoneNumber);
        userContactViewModel.setCountryCode(countryCode);
        userContactViewModel.setCountryId(country_id);

        return userContactViewModel;
    }


    private void cacheCurrentUser(String aboutText, List<SocialConnectionViewModel> socialConnections,
                                  UserContactViewModel contactPhone, List<String> languages) {
        if (mCurrentUser != null) {
            mCurrentUser.setDescription(aboutText);
            mCurrentUser.setSpokenLanguages(languages);
            mCurrentUser.setSocialConnections(socialConnections);
            mCurrentUser.setContactPhone(contactPhone);
            mMeManager.setMe(mCurrentUser);
        }
    }


    /**
     * set focus to top level window
     * disposes descendant focus
     * disposes softInput
     */
    private void topLevelFocus() {
        ViewGroup tlView = (ViewGroup) getWindow().getDecorView();
        if (tlView != null) {
            tlView.setFocusable(true);
            tlView.setFocusableInTouchMode(true);
            tlView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        }
    }
}