package com.roomster.event.RestServiceEvents;


import com.roomster.event.Event;


/**
 * Created by "Michael Katkov" on 10/7/2015.
 */
public class CatalogRestServiceEvent extends Event {

    public static class GetCatalogSuccess extends CatalogRestServiceEvent {

    }


    public static class GetCatalogFailure extends CatalogRestServiceEvent {

    }
}
