package com.roomster.activity;


import com.google.android.gms.analytics.HitBuilders;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.ListingsRestServiceEvent;
import com.roomster.fragment.EditMyListingFragment;
import com.roomster.manager.CatalogsManager;
import com.roomster.manager.MeManager;
import com.roomster.model.CatalogModel;
import com.roomster.rest.model.ListingGetViewModel;
import com.roomster.rest.service.UserListingsRestService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditMyListingActivity extends BaseActivity {

    public static final  String LISTING_ID     = "listing_id";
    public static final  String LISTING_INDEX  = "listing_index";
    private static final String NEED_ROOM      = "NeedRoom";
    private static final String HAVE_SHARE     = "HaveShare";
    private static final String NEED_APARTMENT = "NeedApartment";
    private static final String HAVE_APARTMENT = "HaveApartment";

    private EditMyListingFragment mEditMyListingFragment;
    private ListingGetViewModel   mCurrentListingModel;
    private int                   mListingIndex;
    private long                  mListingId;

    @Bind(R.id.edit_listing_toolbar)
    Toolbar mToolbar;

    @Bind(R.id.edit_listing_title)
    TextView mTitleTv;

    @Bind(R.id.edit_listing_done)
    TextView mDoneBtn;

    @Inject
    MeManager mMeManager;

    @Inject
    CatalogsManager mCatalogsManager;

    @Inject
    UserListingsRestService mUserListingRestService;


    @Override
    protected int getMainContentLayout() {
        return R.layout.activity_edit_listing;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        readIntent();
        initContent();
    }


    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName(getString(R.string.screen_name_edit_my_listings));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    @OnClick(R.id.edit_listing_done)
    void onDone() {
        mEditMyListingFragment.saveListing();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ListingsRestServiceEvent.EditListingSuccess event) {
        mUserListingRestService.getUserListings(mMeManager.getMe().getId(), true);
        finish();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ListingsRestServiceEvent.EditListingFailed event) {
        Toast.makeText(this, getString(R.string.alert_edit_listing_error), Toast.LENGTH_SHORT).show();
    }


    private void readIntent() {
        mListingId = getIntent().getLongExtra(LISTING_ID, -1);
        mListingIndex = getIntent().getIntExtra(LISTING_INDEX, -1);
    }


    private void initContent() {
        if (mListingId != -1) {
            mCurrentListingModel = mMeManager.findListingById(mListingId);
            if (mCurrentListingModel != null) {
                setTitleBar(mCurrentListingModel.getServiceType());
                mEditMyListingFragment = EditMyListingFragment.newInstance(mListingId);
                setContent(mEditMyListingFragment);
            }
        }
    }


    private void setContent(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.edit_listing_content, fragment).commit();
    }


    private void setTitleBar(Integer serviceType) {
        CatalogModel serviceTypeModel = mCatalogsManager.getServiceTypeById(serviceType);
        if (serviceTypeModel == null) {
            return;
        }
        switch (serviceTypeModel.getName()) {
            case NEED_ROOM:
                mTitleTv.setText(getString(R.string.el_looking_for_room));
                break;
            case HAVE_SHARE:
                mTitleTv.setText(getString(R.string.el_offering_room));
                break;
            case NEED_APARTMENT:
                mTitleTv.setText(getString(R.string.al_looking_for_apartment));
                break;
            case HAVE_APARTMENT:
                mTitleTv.setText(getString(R.string.al_offering_apartment));
                break;
            default:
                break;
        }
    }
}

