package com.roomster.rest.api;


import com.roomster.rest.model.MegaphoneParamsModel;

import java.util.Map;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface MegaphoneApi {

    /**
     * Get megaphone counter For &lt;b&gt;NeedRoom&lt;/b&gt; include: \n &lt;ul&gt;&lt;li&gt;service_type
     * (&lt;b&gt;Required&lt;/b&gt;)&lt;/li&gt;&lt;li&gt;budget.min (Optional)&lt;/li&gt;&lt;li&gt;budget.max
     * (Optional)&lt;/li&gt;&lt;li&gt;age.min (Optional)&lt;/li&gt;&lt;li&gt;age.max (Optional)&lt;/li&gt;&lt;li&gt;sex
     * (Optional)&lt;/li&gt;&lt;/ul&gt; \n For &lt;b&gt;HaveShare&lt;/b&gt; include: \n &lt;ul&gt;&lt;li&gt;service_type
     * (&lt;b&gt;Required&lt;/b&gt;)&lt;/li&gt;&lt;li&gt;budget.min (Optional)&lt;/li&gt;&lt;li&gt;budget.max
     * (Optional)&lt;/li&gt;&lt;li&gt;age.min (Optional)&lt;/li&gt;&lt;li&gt;age.max (Optional)&lt;/li&gt;&lt;/ul&gt; \n
     * For &lt;b&gt;NeedApartment&lt;/b&gt; include: \n &lt;ul&gt;&lt;li&gt;service_type
     * (&lt;b&gt;Required&lt;/b&gt;)&lt;/li&gt;&lt;li&gt;budget.min (Optional)&lt;/li&gt;&lt;li&gt;budget.max
     * (Optional)&lt;/li&gt;&lt;li&gt;bedrooms (Optional)&lt;/li&gt;&lt;/ul&gt; \n For &lt;b&gt;HaveApartment&lt;/b&gt;
     * include: \n &lt;ul&gt;&lt;li&gt;service_type (&lt;b&gt;Required&lt;/b&gt;)&lt;/li&gt;&lt;li&gt;budget.min
     * (Optional)&lt;/li&gt;&lt;li&gt;budget.max (Optional)&lt;/li&gt;&lt;li&gt;bedrooms
     * (Optional)&lt;/li&gt;&lt;/ul&gt;
     *
     * @param searchParamsServiceType
     *   Service type
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @param searchParamsBudgetMin
     * @param searchParamsBudgetMax
     * @param searchParamsAgeMin
     * @param searchParamsAgeMax
     * @param searchParamsSex
     *   CSV list of ids from /v1/catalogues/sex_category
     * @param searchParamsBedrooms
     *   CSV list of ids from /v1/catalogues/bedrooms
     * @param searchParamsMessage
     *   Message to broadcast
     * @param householdSex
     * @return Call<Object>
     */

    @GET("Megaphone")
    Call<Map<String, Object>> megaphoneGet(@Query("search_params.service_type") String searchParamsServiceType,
                                           @Header("Authorization") String authorization,
                                           @Query("search_params.budget.min") Integer searchParamsBudgetMin,
                                           @Query("search_params.budget.max") Integer searchParamsBudgetMax,
                                           @Query("search_params.age.min") Integer searchParamsAgeMin,
                                           @Query("search_params.age.max") Integer searchParamsAgeMax,
                                           @Query("search_params.sex") String searchParamsSex,
                                           @Query("search_params.bedrooms") String searchParamsBedrooms,
                                           @Query("search_params.message") String searchParamsMessage,
                                           @Query("search_params.household_sex")String householdSex);

    /**
     * Post Megaphone message For &lt;b&gt;NeedRoom&lt;/b&gt; include: \n &lt;ul&gt;&lt;li&gt;service_type
     * (&lt;b&gt;Required&lt;/b&gt;)&lt;/li&gt;&lt;li&gt;message
     * (&lt;b&gt;Required&lt;/b&gt;)&lt;/li&gt;&lt;li&gt;budget.min (Optional)&lt;/li&gt;&lt;li&gt;budget.max
     * (Optional)&lt;/li&gt;&lt;li&gt;age.min (Optional)&lt;/li&gt;&lt;li&gt;age.max (Optional)&lt;/li&gt;&lt;li&gt;sex
     * (Optional)&lt;/li&gt;&lt;/ul&gt; \n For &lt;b&gt;HaveShare&lt;/b&gt; include: \n &lt;ul&gt;&lt;li&gt;service_type
     * (&lt;b&gt;Required&lt;/b&gt;)&lt;/li&gt;&lt;li&gt;message
     * (&lt;b&gt;Required&lt;/b&gt;)&lt;/li&gt;&lt;li&gt;budget.min (Optional)&lt;/li&gt;&lt;li&gt;budget.max
     * (Optional)&lt;/li&gt;&lt;li&gt;age.min (Optional)&lt;/li&gt;&lt;li&gt;age.max (Optional)&lt;/li&gt;&lt;/ul&gt; \n
     * For &lt;b&gt;NeedApartment&lt;/b&gt; include: \n &lt;ul&gt;&lt;li&gt;service_type
     * (&lt;b&gt;Required&lt;/b&gt;)&lt;/li&gt;&lt;li&gt;message
     * (&lt;b&gt;Required&lt;/b&gt;)&lt;/li&gt;&lt;li&gt;budget.min (Optional)&lt;/li&gt;&lt;li&gt;budget.max
     * (Optional)&lt;/li&gt;&lt;li&gt;bedrooms (Optional)&lt;/li&gt;&lt;/ul&gt; \n For &lt;b&gt;HaveApartment&lt;/b&gt;
     * include: \n &lt;ul&gt;&lt;li&gt;service_type (&lt;b&gt;Required&lt;/b&gt;)&lt;/li&gt;&lt;li&gt;message
     * (&lt;b&gt;Required&lt;/b&gt;)&lt;/li&gt;&lt;li&gt;budget.min (Optional)&lt;/li&gt;&lt;li&gt;budget.max
     * (Optional)&lt;/li&gt;&lt;li&gt;bedrooms (Optional)&lt;/li&gt;&lt;/ul&gt;
     *
     * @param searchParams
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<Object>
     */

    @POST("Megaphone/")
    Call<String> megaphonePost(@Body MegaphoneParamsModel searchParams, @Header("Authorization") String authorization);
}
