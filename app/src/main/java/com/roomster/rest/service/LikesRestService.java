package com.roomster.rest.service;


import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.LikesRestServiceEvent;
import com.roomster.manager.NetworkManager;
import com.roomster.rest.api.LikesApi;
import com.roomster.rest.model.FacebookPage;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by michaelkatkov on 1/8/16.
 */
public class LikesRestService {

    @Inject
    LikesApi mLikesApi;

    @Inject
    EventBus mEventBus;

    @Inject
    NetworkManager mNetworkManager;


    public LikesRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void getLikes(Long userId, Long userIdCompare) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<List<FacebookPage>> call = mLikesApi.likesGet(userId, userIdCompare);
            call.enqueue(new Callback<List<FacebookPage>>() {

                @Override
                public void onResponse(Call<List<FacebookPage>> call, Response<List<FacebookPage>> response) {
                    if (response.code() == ResponseCode.OK) {
                        LikesRestServiceEvent.GetLikesSuccess success = new LikesRestServiceEvent.GetLikesSuccess();
                        success.likes = response.body();
                        mEventBus.post(success);
                    } else {
                        mEventBus.post(new LikesRestServiceEvent.GetLikesFailure());
                    }
                }


                @Override
                public void onFailure(Call<List<FacebookPage>> call, Throwable t) {
                    mEventBus.post(new LikesRestServiceEvent.GetLikesFailure());
                }
            });
        } else {
            mEventBus.post(new LikesRestServiceEvent.GetLikesFailure());
        }
    }
}