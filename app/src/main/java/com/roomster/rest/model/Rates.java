package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


public class Rates {

    /**
     * Allowed values @ /v1/catalogues/currencies
     **/
    @SerializedName("currency")
    private String currency = null;

    /**
     **/
    @SerializedName("monthly_rate")
    private Integer monthlyRate = null;

    /**
     **/
    @SerializedName("guests")
    private Integer guests = null;


    public String getCurrency() {
        return currency;
    }


    public void setCurrency(String currency) {
        this.currency = currency;
    }


    public Integer getMonthlyRate() {
        return monthlyRate;
    }


    public void setMonthlyRate(Integer monthlyRate) {
        this.monthlyRate = monthlyRate;
    }


    public Integer getGuests() {
        return guests;
    }


    public void setGuests(Integer guests) {
        this.guests = guests;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Rates {\n");

        sb.append("  currency: ").append(currency).append("\n");
        sb.append("  monthlyRate: ").append(monthlyRate).append("\n");
        sb.append("  guests: ").append(guests).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
