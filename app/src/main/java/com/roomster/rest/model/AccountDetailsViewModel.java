package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import com.roomster.enums.MaritalStatusEnum;

import java.util.List;


public class AccountDetailsViewModel {

    @SerializedName("marital_status")
    private MaritalStatusEnum maritalStatus = null;


    @SerializedName("home_town")
    private String homeTown = null;


    @SerializedName("contact_phone")
    private UserContactViewModel contactPhone = null;

    /**
     * Spoken languages ids. Allowed values @ /v1/spoken_langauges
     **/
    @SerializedName("spoken_languages")
    private List<Integer> spokenLanguages = null;

    @SerializedName("social_connections")
    private List<SocialConnectionViewModel> socialConnections = null;

    @SerializedName("about")
    private String about = null;

    @SerializedName("music_tag")
    private String musicTag = null;

    @SerializedName("book_tag")
    private String bookTag = null;

    @SerializedName("tv_tag")
    private String tvTag = null;

    @SerializedName("movie_tag")
    private String movieTag = null;

    @SerializedName("interest_tag")
    private String interestTag = null;

    @SerializedName("is_active")
    private Boolean isActive = null;


    public MaritalStatusEnum getMaritalStatus() {
        return maritalStatus;
    }


    public void setMaritalStatus(MaritalStatusEnum maritalStatus) {
        this.maritalStatus = maritalStatus;
    }


    public String getHomeTown() {
        return homeTown;
    }


    public void setHomeTown(String homeTown) {
        this.homeTown = homeTown;
    }


    public UserContactViewModel getContactPhone() {
        return contactPhone;
    }


    public void setContactPhone(UserContactViewModel contactPhone) {
        this.contactPhone = contactPhone;
    }


    public List<Integer> getSpokenLanguages() {
        return spokenLanguages;
    }


    public void setSpokenLanguages(List<Integer> spokenLanguages) {
        this.spokenLanguages = spokenLanguages;
    }


    public List<SocialConnectionViewModel> getSocialConnections() {
        return socialConnections;
    }


    public void setSocialConnections(List<SocialConnectionViewModel> socialConnections) {
        this.socialConnections = socialConnections;
    }


    public String getAbout() {
        return about;
    }


    public void setAbout(String about) {
        this.about = about;
    }


    public String getMusicTag() {
        return musicTag;
    }


    public void setMusicTag(String musicTag) {
        this.musicTag = musicTag;
    }


    public String getBookTag() {
        return bookTag;
    }


    public void setBookTag(String bookTag) {
        this.bookTag = bookTag;
    }


    public String getTvTag() {
        return tvTag;
    }


    public void setTvTag(String tvTag) {
        this.tvTag = tvTag;
    }


    public String getMovieTag() {
        return movieTag;
    }


    public void setMovieTag(String movieTag) {
        this.movieTag = movieTag;
    }


    public String getInterestTag() {
        return interestTag;
    }


    public void setInterestTag(String interestTag) {
        this.interestTag = interestTag;
    }


    public Boolean getIsActive() {
        return isActive;
    }


    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class AccountDetailsViewModel {\n");

        sb.append("  maritalStatus: ").append(maritalStatus).append("\n");
        sb.append("  homeTown: ").append(homeTown).append("\n");
        sb.append("  contactPhone: ").append(contactPhone).append("\n");
        sb.append("  spokenLanguages: ").append(spokenLanguages).append("\n");
        sb.append("  socialConnections: ").append(socialConnections).append("\n");
        sb.append("  about: ").append(about).append("\n");
        sb.append("  musicTag: ").append(musicTag).append("\n");
        sb.append("  bookTag: ").append(bookTag).append("\n");
        sb.append("  tvTag: ").append(tvTag).append("\n");
        sb.append("  movieTag: ").append(movieTag).append("\n");
        sb.append("  interestTag: ").append(interestTag).append("\n");
        sb.append("  isActive: ").append(isActive).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}