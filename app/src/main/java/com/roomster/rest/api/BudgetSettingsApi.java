package com.roomster.rest.api;

import java.util.Currency;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;

/**
 * Created by eugenetroyanskii on 16.08.16.
 */
public interface BudgetSettingsApi {

    @GET("/BudgetSettings")
    Call<ResponseBody> getCurrentBudget(@Header("Currency") String currency, @Header("Locale") String locale);
}
