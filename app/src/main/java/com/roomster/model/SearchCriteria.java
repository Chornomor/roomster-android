package com.roomster.model;


import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

public class SearchCriteria implements Cloneable {

    private String  sort;
    private String  serviceType;
    private double  latSouthWest;
    private double  longSouthWest;
    private double  latNorthEast;
    private double  longNorthEast;
    private Float   radiusScale;
    private Integer budgetMin;
    private Integer budgetMax;
    private Integer ageMin;
    private Integer ageMax;
    private String  currency;
    private String  householdSex;
    private String  sex;
    private String  zodiac;
    private String  petsPrefs;
    private String  myPets;
    private String  bedrooms;
    private String  bathrooms;
    private String  amenities;
    private String  dataIn;
    private String  dataOut;
    private String  locale;




    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SearchCriteria that = (SearchCriteria) o;

        if (Double.compare(that.latSouthWest, latSouthWest) != 0) {
            return false;
        }
        if (Double.compare(that.longSouthWest, longSouthWest) != 0) {
            return false;
        }
        if (Double.compare(that.latNorthEast, latNorthEast) != 0) {
            return false;
        }
        if (Double.compare(that.longNorthEast, longNorthEast) != 0) {
            return false;
        }
        if (sort != null ? !sort.equals(that.sort) : that.sort != null) {
            return false;
        }
        if (serviceType != null ? !serviceType.equals(that.serviceType) : that.serviceType != null) {
            return false;
        }
        if (radiusScale != null ? !radiusScale.equals(that.radiusScale) : that.radiusScale != null) {
            return false;
        }
        if (budgetMin != null ? !budgetMin.equals(that.budgetMin) : that.budgetMin != null) {
            return false;
        }
        if (budgetMax != null ? !budgetMax.equals(that.budgetMax) : that.budgetMax != null) {
            return false;
        }
        if (ageMin != null ? !ageMin.equals(that.ageMin) : that.ageMin != null) {
            return false;
        }
        if (ageMax != null ? !ageMax.equals(that.ageMax) : that.ageMax != null) {
            return false;
        }
        if (currency != null ? !currency.equals(that.currency) : that.currency != null) {
            return false;
        }
        if (householdSex != null ? !householdSex.equals(that.householdSex) : that.householdSex != null) {
            return false;
        }
        if (sex != null ? !sex.equals(that.sex) : that.sex != null) {
            return false;
        }
        if (zodiac != null ? !zodiac.equals(that.zodiac) : that.zodiac != null) {
            return false;
        }
        if (petsPrefs != null ? !petsPrefs.equals(that.petsPrefs) : that.petsPrefs != null) {
            return false;
        }
        if (myPets != null ? !myPets.equals(that.myPets) : that.myPets != null) {
            return false;
        }
        if (bedrooms != null ? !bedrooms.equals(that.bedrooms) : that.bedrooms != null) {
            return false;
        }
        if (bathrooms != null ? !bathrooms.equals(that.bathrooms) : that.bathrooms != null) {
            return false;
        }
        if (amenities != null ? !amenities.equals(that.amenities) : that.amenities != null) {
            return false;
        }
        if (dataIn != null ? !dataIn.equals(that.dataIn) : that.dataIn != null) {
            return false;
        }
        if (dataOut != null ? !dataOut.equals(that.dataOut) : that.dataOut != null) {
            return false;
        }
        return !(locale != null ? !locale.equals(that.locale) : that.locale != null);
    }


    @Override
    public int hashCode() {
        int result;
        long temp;
        result = sort != null ? sort.hashCode() : 0;
        result = 31 * result + (serviceType != null ? serviceType.hashCode() : 0);
        temp = Double.doubleToLongBits(latSouthWest);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longSouthWest);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(latNorthEast);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longNorthEast);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (radiusScale != null ? radiusScale.hashCode() : 0);
        result = 31 * result + (budgetMin != null ? budgetMin.hashCode() : 0);
        result = 31 * result + (budgetMax != null ? budgetMax.hashCode() : 0);
        result = 31 * result + (ageMin != null ? ageMin.hashCode() : 0);
        result = 31 * result + (ageMax != null ? ageMax.hashCode() : 0);
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        result = 31 * result + (householdSex != null ? householdSex.hashCode() : 0);
        result = 31 * result + (sex != null ? sex.hashCode() : 0);
        result = 31 * result + (zodiac != null ? zodiac.hashCode() : 0);
        result = 31 * result + (petsPrefs != null ? petsPrefs.hashCode() : 0);
        result = 31 * result + (myPets != null ? myPets.hashCode() : 0);
        result = 31 * result + (bedrooms != null ? bedrooms.hashCode() : 0);
        result = 31 * result + (bathrooms != null ? bathrooms.hashCode() : 0);
        result = 31 * result + (amenities != null ? amenities.hashCode() : 0);
        result = 31 * result + (dataIn != null ? dataIn.hashCode() : 0);
        result = 31 * result + (dataOut != null ? dataOut.hashCode() : 0);
        result = 31 * result + (locale != null ? locale.hashCode() : 0);
        return result;
    }


    @Override
    public String toString() {
        return "SearchCriteria{" +
                "sort='" + sort + '\'' +
                ", serviceType='" + serviceType + '\'' +
                ", latSouthWest=" + latSouthWest +
                ", longSouthWest=" + longSouthWest +
                ", latNorthEast=" + latNorthEast +
                ", longNorthEast=" + longNorthEast +
                ", radiusScale=" + radiusScale +
                ", budgetMin=" + budgetMin +
                ", budgetMax=" + budgetMax +
                ", ageMin=" + ageMin +
                ", ageMax=" + ageMax +
                ", currency='" + currency + '\'' +
                ", householdSex='" + householdSex + '\'' +
                ", sex='" + sex + '\'' +
                ", zodiac='" + zodiac + '\'' +
                ", petsPrefs='" + petsPrefs + '\'' +
                ", myPets='" + myPets + '\'' +
                ", bedrooms='" + bedrooms + '\'' +
                ", bathrooms='" + bathrooms + '\'' +
                ", amenities='" + amenities + '\'' +
                ", dataIn='" + dataIn + '\'' +
                ", dataOut='" + dataOut + '\'' +
                ", locale='" + locale + '\'' +
                '}';
    }


    public String getSort() {
        return sort;
    }


    public void setSort(String sort) {
        this.sort = sort;
    }


    public String getServiceType() {
        return serviceType;
    }


    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }


    public double getLatSouthWest() {
        return latSouthWest;
    }


    public void setLatSouthWest(double latSouthWest) {
        this.latSouthWest = latSouthWest;
    }


    public double getLongSouthWest() {
        return longSouthWest;
    }


    public void setLongSouthWest(double longSouthWest) {
        this.longSouthWest = longSouthWest;
    }


    public double getLatNorthEast() {
        return latNorthEast;
    }


    public void setLatNorthEast(double latNorthEast) {
        this.latNorthEast = latNorthEast;
    }


    public double getLongNorthEast() {
        return longNorthEast;
    }


    public void setLongNorthEast(double longNorthEast) {
        this.longNorthEast = longNorthEast;
    }


    public Float getRadiusScale() {
        return radiusScale;
    }


    public void setRadiusScale(Float radiusScale) {
        this.radiusScale = radiusScale;
    }


    public Integer getBudgetMin() {
        return budgetMin;
    }


    public void setBudgetMin(Integer budgetMin) {
        this.budgetMin = budgetMin;
    }


    public Integer getBudgetMax() {
        return budgetMax;
    }


    public void setBudgetMax(Integer budgetMax) {
        this.budgetMax = budgetMax;
    }


    public Integer getAgeMin() {
        return ageMin;
    }


    public void setAgeMin(Integer ageMin) {
        this.ageMin = ageMin;
    }


    public Integer getAgeMax() {
        return ageMax;
    }


    public void setAgeMax(Integer ageMax) {
        this.ageMax = ageMax;
    }


    public String getCurrency() {
        return currency;
    }


    public void setCurrency(String currency) {
        this.currency = currency;
    }


    public String getHouseholdSex() {
        return householdSex;
    }


    public void setHouseholdSex(String householdSex) {
        this.householdSex = householdSex;
    }


    public String getSex() {
        return sex;
    }


    public void setSex(String sex) {
        this.sex = sex;
    }


    public String getZodiac() {
        return zodiac;
    }


    public void setZodiac(String zodiac) {
        this.zodiac = zodiac;
    }


    public String getPetsPrefs() {
        return petsPrefs;
    }


    public void setPetsPrefs(String petsPrefs) {
        this.petsPrefs = petsPrefs;
    }


    public String getMyPets() {
        return myPets;
    }


    public void setMyPets(String myPets) {
        this.myPets = myPets;
    }


    public String getBedrooms() {
        return bedrooms;
    }


    public void setBedrooms(String bedrooms) {
        this.bedrooms = bedrooms;
    }


    public String getBathrooms() {
        return bathrooms;
    }


    public void setBathrooms(String bathrooms) {
        this.bathrooms = bathrooms;
    }


    public String getAmenities() {
        return amenities;
    }


    public void setAmenities(String amenities) {
        this.amenities = amenities;
    }


    public String getDataIn() {
        return dataIn;
    }


    public void setDataIn(String dataIn) {
        this.dataIn = dataIn;
    }


    public String getDataOut() {
        return dataOut;
    }


    public void setDataOut(String dataOut) {
        this.dataOut = dataOut;
    }


    public String getLocale() {
        return locale;
    }


    public void setLocale(String locale) {
        this.locale = locale;
    }

    public LatLngBounds getSearchBounds(){
        return new LatLngBounds(new LatLng(latSouthWest,longSouthWest),
                new LatLng(latNorthEast,longNorthEast));
    }

    public void setSearchBounds(LatLngBounds bounds){
        latSouthWest = bounds.southwest.latitude;
        longSouthWest = bounds.southwest.longitude;
        latNorthEast = bounds.northeast.latitude;
        longNorthEast = bounds.northeast.longitude;
    }

    public SearchCriteria createClone(){
        try {
            return (SearchCriteria) clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            throw new RuntimeException("copy error");
        }
    }
}
