package com.roomster.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by yuliasokolova on 25.10.16.
 */
public class BillingActivity extends  BaseActivity{

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.tv_title)
    TextView mToolbarTitle;

    @Bind(R.id.iv_back)
    ImageView mBackButton;

    @Override
    protected int getMainContentLayout() {
        return R.layout.activity_billing;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        ButterKnife.bind(this);
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName(getString(R.string.screen_name_billing));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @OnClick(R.id.iv_back)
    void onToolbarBackClicked() {
        onBackPressed();
    }

    private void initViews() {
        mToolbarTitle.setText(getString(R.string.toolbar_billing_title));
    }

    @OnClick(R.id.billing_transactions_container)
    void onClickTransactionsHistory() {
        startActivity(new Intent(mContext, TransactionsActivity.class));
    }

    @OnClick(R.id.billing_subscription)
    void onClickSubcription() {
        startActivity(new Intent(mContext, SubscriptionActivity.class));
    }

//    @OnClick (R.id.billing_refund_container)
//    void onClickRefund(){
//        startActivity(new Intent(mContext, RefundActivity.class));
//    }

}