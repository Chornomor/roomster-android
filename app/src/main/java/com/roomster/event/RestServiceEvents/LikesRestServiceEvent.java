package com.roomster.event.RestServiceEvents;


import com.roomster.event.Event;
import com.roomster.rest.model.FacebookPage;

import java.util.List;


/**
 * Created by michaelkatkov on 1/8/16.
 */
public class LikesRestServiceEvent extends Event {

    public static class GetLikesSuccess extends LikesRestServiceEvent {

        public List<FacebookPage> likes;
    }

    public static class GetLikesFailure extends LikesRestServiceEvent {

    }
}
