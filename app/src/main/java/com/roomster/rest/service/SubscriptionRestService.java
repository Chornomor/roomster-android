package com.roomster.rest.service;


import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.TokenManager;
import com.roomster.rest.api.SubscriptionApi;
import com.roomster.rest.model.SubscriptionGetModel;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SubscriptionRestService {

    @Inject
    TokenManager mTokenManager;

    @Inject
    SubscriptionApi mSubscriptionApi;

    @Inject
    NetworkManager mNetworkManager;


    public SubscriptionRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void getSubscription(final RestServiceListener<SubscriptionGetModel> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callGetSubscription(result, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                listener.onFailure(code, throwable, msg);
            }
        });
    }


    public void cancelSubscription(final RestServiceListener<Object> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callPutSubscription(result, false, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                listener.onFailure(code, throwable, msg);
            }
        });
    }


    private void callGetSubscription(String authorization, final RestServiceListener<SubscriptionGetModel> listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<SubscriptionGetModel> callGetSubscription = mSubscriptionApi.subscriptionGet(authorization);
            callGetSubscription.enqueue(new Callback<SubscriptionGetModel>() {

                @Override
                public void onResponse(Call<SubscriptionGetModel> call, Response<SubscriptionGetModel> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            if (listener != null) {
                                listener.onSuccess(response.body());
                            }
                            break;
                        case ResponseCode.BAD_REQUEST:
                        case ResponseCode.SERVER_ERROR:
                        default:
                            if (listener != null) {
                                listener.onFailure(response.code(), null, null);
                            }
                    }
                }


                @Override
                public void onFailure(Call<SubscriptionGetModel> call, Throwable t) {
                    if (listener != null) {
                        listener.onFailure(-1, t, RoomsterApplication.context.getString(R.string.settings_cant_make_rest_call_to_get_subscription));
                    }
                }
            });
        } else {
            if (listener != null) {
                listener.onFailure(-1, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
            }
        }
    }


    private void callPutSubscription(String authorization, boolean isActive, final RestServiceListener<Object> listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<Void> callPutSubscription = mSubscriptionApi.subscriptionPut(isActive, authorization);
            callPutSubscription.enqueue(new Callback<Void>() {

                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            if (listener != null) {
                                listener.onSuccess(response.body());
                            }
                            break;
                        case ResponseCode.BAD_REQUEST:
                        case ResponseCode.SERVER_ERROR:
                        default:
                            if (listener != null) {
                                listener.onFailure(response.code(), null, null);
                            }
                    }
                }


                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    if (listener != null) {
                        listener.onFailure(-1, t, RoomsterApplication.context.getString(R.string.settings_cant_make_rest_call_to_update_subscription));
                    }
                }
            });
        } else {
            if (listener != null) {
                listener.onFailure(-1, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
            }
        }
    }
}