package com.roomster.enums;

public enum BookmarkTypeEnum {
    Bookmarked, Viewed, Mailed
}
