package com.roomster.fragment;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.roomster.R;
import com.roomster.activity.DiscoveryPreferencesActivity;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.SearchRestServiceEvent;
import com.roomster.event.application.ApplicationEvent;
import com.roomster.listener.BookmarksCallbackListener;
import com.roomster.manager.BookmarkManager;
import com.roomster.manager.SearchManager;
import com.roomster.rest.service.SearchRestService;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;


public class SearchListingsListBookmarkFragment extends ListingListViewBookmarkFragment {

//    private boolean mIsInitialLoading;

    public BookmarksCallbackListener mCallback = null;

    @Inject
    SearchManager mSearchManager;

    @Inject
    BookmarkManager bookmarkManager;

    @Inject
    SearchRestService mSearchRestService;

    @Inject
    EventBus mEventBus;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        super.setListener(mCallback);
        RoomsterApplication.getRoomsterComponent().inject(this);
        updateBookmarks();
//        mIsInitialLoading = true;
    }


    @Override
    public void onStart() {
        super.onStart();
        if (!mEventBus.isRegistered(this)) {
            mEventBus.register(this);
        }
    }


    @Override
    public void onStop() {
        if (mEventBus.isRegistered(this)) {
            mEventBus.unregister(this);
        }
        super.onStop();
    }


    @Override
    protected void onInitEmptyLayoutViews() {
        mEmptyResultHeaderTV.setText(getString(R.string.empty_search_header));
        mEmptyResultSubtextTV.setText(getString(R.string.empty_search_subtext));

        mEmptyLayoutButtonTV.setText(getString(R.string.empty_search_button));
        mEmptyLayoutButtonTV.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DiscoveryPreferencesActivity.class);
                startActivity(intent);
            }
        });
        mEmptyLayoutButtonTV.setVisibility(View.VISIBLE);
    }


    @Override
    protected boolean hasMoreBookmarks() {
        return mSearchRestService.hasMoreBookmarks();
    }
//
//
//    @Override
//    protected void onRequestListings() {
//        if (mIsInitialLoading) {
//            mSearchRestService.loadNextResultsPage(PAGE_SIZE, true);
//            mIsInitialLoading = false;
//        } else {
//            mSearchRestService.loadNextResultsPage(PAGE_SIZE, false);
//        }
//    }


    @Override
    protected void onRequestBookmarks() {
        mSearchRestService.loadNextBookmarkedResultsPage(PAGE_SIZE, true);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(SearchRestServiceEvent.BookmarkSearchSuccess event) {
        onNewListings(bookmarkManager.getResultModel().getItems());
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(SearchRestServiceEvent.SearchFailure event) {
        Toast.makeText(mContext, getString(R.string.unable_to_get_search_info), Toast.LENGTH_SHORT).show();
    }


    public void updateBookmarks() {
        bookmarkManager.clearBookmarkModel();
        initListings();
    }
}