package com.roomster.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.UserListingsRestServiceEvent;
import com.roomster.event.fragment.FragmentRequest;
import com.roomster.rest.model.ListingGetViewModel;
import com.roomster.rest.model.ResultItemModel;
import com.roomster.rest.model.UserGetViewModel;
import com.roomster.rest.service.UserListingsRestService;
import com.roomster.rest.service.UsersRestService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class UserListingsListFragment extends ListingListViewFragment {

    private static final String USER_KEY = "user_id";

    private List<ListingGetViewModel> mUserListings;
    private List<ResultItemModel>     mResultItemModels;
    private UserGetViewModel          mUser;
    private boolean                   mListingsReturned;

    @Inject
    UserListingsRestService mUserListingsRestService;

    @Inject
    UsersRestService mUsersRestService;

    @Inject
    EventBus mEventBus;


    public static UserListingsListFragment newInstance(UserGetViewModel user) {
        UserListingsListFragment fragment = new UserListingsListFragment();
        Bundle args = new Bundle();
        args.putSerializable(USER_KEY, user);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mUser = (UserGetViewModel) getArguments().getSerializable(USER_KEY);
            setTitle(-1);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (mUser != null) {
            mUserListingsRestService.getUserListings(mUser.getId(), true);
        } else {
            showEmptyResult();
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Override
    public void onStart() {
        super.onStart();
        if (!mEventBus.isRegistered(this)) {
            mEventBus.register(this);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        (getActivity()).invalidateOptionsMenu();

    }
    private void setTitle(int listingsCount){
        String title = mUser.getFirstName() + " " + getString(R.string.listings);
        if(listingsCount != -1){
            title +=  " (" + listingsCount + ")";
        }
        mEventBus.post(new FragmentRequest.SetTitleRequest(title, null));
    }

    @Override
    public void onStop() {
        if (mEventBus.isRegistered(this)) {
            mEventBus.unregister(this);
        }
        super.onStop();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    protected boolean hasMoreListings() {
        return !mListingsReturned;
    }


    @Override
    protected List<ResultItemModel> getListings() {
        return mResultItemModels;
    }


    @Override
    protected void onRequestListings() {

    }


    @Override
    protected void onInitEmptyLayoutViews() {
        //TODO fill text
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(UserListingsRestServiceEvent.GetListingsFailed event) {
        showEmptyResult();
        mListingsReturned = true;
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(UserListingsRestServiceEvent.GetListingsSuccess event) {
        mUserListings = event.listings;
        setTitle(mUserListings.size());
        mListingsReturned = true;
        returnListings();
    }


    private void returnListings() {
        if (mListingsReturned && mUser != null && mUserListings != null) {
            mResultItemModels = new ArrayList<>();
            for (ListingGetViewModel model : mUserListings) {
                ResultItemModel resultItemModel = new ResultItemModel();
                resultItemModel.setUser(mUser);
                resultItemModel.setListing(model);
                mResultItemModels.add(resultItemModel);
            }
            onNewListings(mResultItemModels);
        }
    }
}
