package com.roomster.di.module;


import com.roomster.manager.BookmarkManager;
import com.roomster.manager.CatalogsManager;
import com.roomster.manager.DiscoveryPrefsManager;
import com.roomster.manager.FacebookManager;
import com.roomster.manager.GcmManager;
import com.roomster.manager.MeManager;
import com.roomster.manager.MegaphoneManager;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.ReferrerManager;
import com.roomster.manager.RoomsterNotificationManager;
import com.roomster.manager.SearchManager;
import com.roomster.manager.SettingsManager;
import com.roomster.manager.UsersManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


/**
 * Created by "Michael Katkov" on 10/8/2015.
 */
@Module
public class ManagerModule {

    @Singleton
    @Provides
    SettingsManager provideSettingsManager() {
        return new SettingsManager();
    }


    @Singleton
    @Provides
    FacebookManager provideFacebookManager() {
        return new FacebookManager();
    }


    @Singleton
    @Provides
    MeManager provideMeManager() {
        return new MeManager();
    }


    @Singleton
    @Provides
    DiscoveryPrefsManager provideDiscoveryPreferencesManager() {
        return new DiscoveryPrefsManager();
    }


    @Singleton
    @Provides
    ReferrerManager provideReferrerManager() {
        return new ReferrerManager();
    }


    @Singleton
    @Provides
    SearchManager provideSearchManager() {
        return new SearchManager();
    }


    @Singleton
    @Provides
    BookmarkManager provideBookmarkManager() {
        return new BookmarkManager();
    }


    @Singleton
    @Provides
    GcmManager provideGcmManager() {
        return new GcmManager();
    }


    @Singleton
    @Provides
    RoomsterNotificationManager provideNotificationManager() {
        return new RoomsterNotificationManager();
    }


    @Singleton
    @Provides
    UsersManager provideUsersManager() {
        return new UsersManager();
    }


    @Singleton
    @Provides
    CatalogsManager provideCatalogsManager() {
        return new CatalogsManager();
    }


    @Singleton
    @Provides
    NetworkManager provideNetworkManager() {
        return new NetworkManager();
    }


    @Singleton
    @Provides
    MegaphoneManager provideMegaphoneManager() {
        return new MegaphoneManager();
    }
}
