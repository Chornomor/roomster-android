package com.roomster.di.component;


import com.roomster.activity.AboutActivity;
import com.roomster.activity.AddNewDetailedListingActivity;
import com.roomster.activity.AppSettingsActivity;
import com.roomster.activity.BaseActivity;
import com.roomster.activity.RefundActivity;
import com.roomster.activity.BookmarksActivity;
import com.roomster.activity.DetailedViewActivity;
import com.roomster.activity.DiscoveryPreferencesActivity;
import com.roomster.activity.EditMyListingActivity;
import com.roomster.activity.EditProfileActivity;
import com.roomster.activity.MainActivity;
import com.roomster.activity.MapListViewActivity;
import com.roomster.activity.MegaphoneActivity;
import com.roomster.activity.MessagesActivity;
import com.roomster.activity.MyListingActivity;
import com.roomster.activity.ProfileActivity;
import com.roomster.activity.SignInActivity;
import com.roomster.activity.SignLogicActivity;
import com.roomster.activity.SplashActivity;
import com.roomster.activity.SubscriptionActivity;
import com.roomster.activity.SupportActivity;
import com.roomster.activity.TransactionsActivity;
import com.roomster.activity.UpgradeAccountActivity;
import com.roomster.activity.UserListingsActivity;
import com.roomster.activity.UserListingsListActivity;
import com.roomster.activity.UserProfileActivity;
import com.roomster.adapter.AmenitiesAdapter;
import com.roomster.adapter.AmenitiesGridAdapter;
import com.roomster.adapter.AptSizesSpinnerAdapter;
import com.roomster.adapter.CatalogListAdapter;
import com.roomster.adapter.CatalogSpinnerAdapter;
import com.roomster.adapter.CurrenciesSpinnerAdapter;
import com.roomster.adapter.LanguagesAdapter;
import com.roomster.adapter.ListingListAdapter;
import com.roomster.adapter.ListingPagerAdapter;
import com.roomster.adapter.LocaleSpinnerAdapter;
import com.roomster.adapter.MapMarkerAdapter;
import com.roomster.adapter.MyListingListAdapter;
import com.roomster.adapter.PetsAdapter;
import com.roomster.adapter.PhotoGridAdapter;
import com.roomster.adapter.UserListingAdapter;
import com.roomster.di.module.AccountModule;
import com.roomster.di.module.ApplicationModule;
import com.roomster.di.module.ManagerModule;
import com.roomster.di.module.RestModule;
import com.roomster.di.module.RestServiceModule;
import com.roomster.di.module.TokenModule;
import com.roomster.dialog.CatalogListChooserDialog;
import com.roomster.dialog.ImageChooserDialog;
import com.roomster.dialog.LoadingDialog;
import com.roomster.fragment.AddListingDetailedFragment;
import com.roomster.fragment.AddNewListingFragment;
import com.roomster.fragment.AmenityDialogFragment;
import com.roomster.fragment.AmenityDialogListingFragment;
import com.roomster.fragment.ChoosePackageFragment;
import com.roomster.fragment.ConversationsListFragment;
import com.roomster.fragment.DetailedListingListFragment;
import com.roomster.fragment.DetailedViewFragment;
import com.roomster.fragment.DetailedViewPagerFragment;
import com.roomster.fragment.DiscoveryPrefsFragment;
import com.roomster.fragment.EditMyListingFragment;
import com.roomster.fragment.ImageFragment;
import com.roomster.fragment.ListingListViewBookmarkFragment;
import com.roomster.fragment.ListingListViewFragment;
import com.roomster.fragment.MapListingsListFragment;
import com.roomster.fragment.MapViewFragment;
import com.roomster.fragment.MegaphoneChooserDialogFragment;
import com.roomster.fragment.MenuFragment;
import com.roomster.fragment.MyListingFragment;
import com.roomster.fragment.MyListingListFragment;
import com.roomster.fragment.MyListingPageFragment;
import com.roomster.fragment.ProfileFragment;
import com.roomster.fragment.ProfileMapFragment;
import com.roomster.fragment.SearchListingsListBookmarkFragment;
import com.roomster.fragment.SearchListingsListFragment;
import com.roomster.fragment.SettingsFragment;
import com.roomster.fragment.UpgradeAccountSuccessFragment;
import com.roomster.fragment.UserListingsListFragment;
import com.roomster.fragment.UserProfileFragment;
import com.roomster.manager.BookmarkManager;
import com.roomster.manager.CatalogsManager;
import com.roomster.manager.DiscoveryPrefsManager;
import com.roomster.manager.FacebookManager;
import com.roomster.manager.GcmManager;
import com.roomster.manager.MeManager;
import com.roomster.manager.MegaphoneManager;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.ReferrerManager;
import com.roomster.manager.RoomsterLocationManager;
import com.roomster.manager.RoomsterNotificationManager;
import com.roomster.manager.SearchManager;
import com.roomster.manager.SettingsManager;
import com.roomster.manager.TokenManager;
import com.roomster.manager.UsersManager;
import com.roomster.manager.map.MapRestService;
import com.roomster.receiver.AppUpgradeReceiver;
import com.roomster.receiver.InstallReceiver;
import com.roomster.receiver.NetworkChangeReceiver;
import com.roomster.rest.service.AccountRestService;
import com.roomster.rest.service.AndroidReceiptRestService;
import com.roomster.rest.service.AppInstallsRestService;
import com.roomster.rest.service.BillingRefundsRestService;
import com.roomster.rest.service.BookmarksRestService;
import com.roomster.rest.service.CatalogRestService;
import com.roomster.rest.service.CheckAppRestService;
import com.roomster.rest.service.ConversationsRestService;
import com.roomster.rest.service.FbMutualFriendsService;
import com.roomster.rest.service.ImagesRestService;
import com.roomster.rest.service.InAppProductsRestService;
import com.roomster.rest.service.LikesRestService;
import com.roomster.rest.service.ListingsRestService;
import com.roomster.rest.service.LocaleRestService;
import com.roomster.rest.service.MeRestService;
import com.roomster.rest.service.MegaphoneRestService;
import com.roomster.rest.service.NotificationsRestService;
import com.roomster.rest.service.SearchRestService;
import com.roomster.rest.service.SubscriptionRestService;
import com.roomster.rest.service.SupportRestService;
import com.roomster.rest.service.TokenRestService;
import com.roomster.rest.service.TransactionsRestService;
import com.roomster.rest.service.UpgradeAccountRestService;
import com.roomster.rest.service.UserListingsRestService;
import com.roomster.rest.service.UserStatusRestService;
import com.roomster.rest.service.UsersRestService;
import com.roomster.service.GPlayInAppBillingService;
import com.roomster.service.RoomsterGcmListenerService;
import com.roomster.service.RoomsterInstanceIDListenerService;
import com.roomster.views.AddListingAdditionalFields;
import com.roomster.views.BottomInfoSlidingView;
import com.roomster.views.BottomProfileSlidingView;
import com.roomster.views.DetailedView;
import com.roomster.views.MainToolBarView;
import com.roomster.views.PetsTableLayout;
import com.roomster.views.SharedDetailsView;

import javax.inject.Singleton;

import dagger.Component;


/**
 * Created by "Michael Katkov" on 10/17/2015.
 */
@Singleton
@Component(
        modules = {ApplicationModule.class, AccountModule.class, RestServiceModule.class, ManagerModule.class, RestModule.class,
                TokenModule.class})
public interface RoomsterComponent {

    //activities
    void inject(SplashActivity splashActivity);

    void inject(MapListViewActivity mapListViewActivity);

    void inject(SignInActivity signInActivity);

    void inject(MessagesActivity messagesActivity);

    void inject(SignLogicActivity signLogicActivity);

    void inject(SupportActivity supportActivity);

    void inject(MainActivity mainActivity);

    void inject(UpgradeAccountActivity upgradeAccountActivity);

    void inject(AddNewDetailedListingActivity addNewDetailedListingActivity);

    void inject(MyListingActivity myListingActivity);

    void inject(EditProfileActivity editProfileActivity);

    void inject(UserListingsListActivity myListingListActivity);

    void inject(EditMyListingActivity editMyListingActivity);

    void inject(MegaphoneActivity megaphoneActivity);

    void inject(DiscoveryPreferencesActivity discoveryPreferencesActivity);

    void inject(DetailedViewActivity detailedViewActivity);

    void inject(BookmarksActivity bookmarksActivity);

    void inject(UserProfileActivity userProfileActivity);

    void inject(UserListingsActivity userListingsActivity);

    void inject(AboutActivity aboutActivity);

    void inject(BaseActivity baseActivity);

    void inject(RefundActivity refundActivity);

    void inject(ProfileActivity profileActivity);

    void inject(TransactionsActivity transactionsActivity);

    void inject(SubscriptionActivity subscriptionActivity);

    void inject(AppSettingsActivity appSettingsActivity);

    //services

    void inject(RoomsterInstanceIDListenerService roomsterInstanceIDListenerService);

    void inject(RoomsterGcmListenerService roomsterGcmListenerService);

    void inject(GPlayInAppBillingService gPlayInAppBillingService);

    //fragments
    void inject(SettingsFragment settingsFragment);

    void inject(ProfileFragment profileFragment);

    void inject(ConversationsListFragment conversationsListFragment);

    void inject(MenuFragment menuFragment);

    void inject(DiscoveryPrefsFragment discoveryPrefsFragment);

    void inject(AddNewListingFragment addNewListingFragment);

    void inject(AmenityDialogFragment amenityDialogFragment);

    void inject(ChoosePackageFragment choosePackageFragment);

    void inject(DetailedViewFragment detailedViewFragment);

    void inject(MapViewFragment mapViewFragment);

    void inject(DetailedViewPagerFragment detailedViewPagerFragment);

    void inject(ImageFragment imageFragment);

    void inject(ListingListViewFragment listingListViewFragment);

    void inject(ListingListViewBookmarkFragment listingListViewBookmarkFragment);

    void inject(SearchListingsListFragment searchListingsListFragment);

    void inject(SearchListingsListBookmarkFragment searchListingsListBookmarkFragment);

    void inject(MapListingsListFragment mapListingsListFragment);

    void inject(UserListingsListFragment userListingsListFragment);

    void inject(ProfileMapFragment profileMapFragment);

    void inject(AddListingDetailedFragment addListingDetailedFragment);

    void inject(MyListingFragment myListingFragment);

    void inject(MyListingPageFragment myListingPageFragment);

    void inject(MyListingListFragment myListingListFragment);

    void inject(EditMyListingFragment editMyListingFragment);

    void inject(AmenityDialogListingFragment amenityDialogListingFragment);

    void inject(UpgradeAccountSuccessFragment upgradeAccountSuccessFragment);

    void inject(UserProfileFragment userProfileFragment);

    void inject(DetailedListingListFragment detailedListingListFragment);

    void inject(CheckAppRestService checkAppRestService);

    //adapters
    void inject(CurrenciesSpinnerAdapter currenciesSpinnerAdapter);

    void inject(LocaleSpinnerAdapter localeSpinnerAdapter);

    void inject(AmenitiesAdapter amenitiesAdapter);

    void inject(PetsAdapter petsAdapter);

    void inject(MapMarkerAdapter mapMarkerAdapter);

    void inject(ListingPagerAdapter listingPagerAdapter);

    void inject(AptSizesSpinnerAdapter aptSizesSpinnerAdapter);

    void inject(UserListingAdapter userListingAdapter);

    void inject(LanguagesAdapter languagesAdapter);

    void inject(PhotoGridAdapter photoGridAdapter);

    void inject(CatalogListAdapter sharedDetailsAdapter);

    void inject(MyListingListAdapter adapter);

    void inject(CatalogSpinnerAdapter catalogSpinnerAdapter);

    void inject(ListingListAdapter listingListAdapter);

    void inject(AmenitiesGridAdapter amenitiesGridAdapter);

    //views
    void inject(BottomProfileSlidingView bottomProfileSlidingView);

    void inject(BottomInfoSlidingView bottomInfoSlidingView);

    void inject(DetailedView detailedView);

    void inject(MainToolBarView mainToolBarView);

    void inject(SharedDetailsView sharedDetailsView);

    void inject(AddListingAdditionalFields addListingAdditionalFields);

    void inject(PetsTableLayout petsTableLayout);

    //rest services
    void inject(AccountRestService accountRestService);

    void inject(CatalogRestService catalogRestService);

    void inject(TokenRestService tokenRestService);

    void inject(ConversationsRestService conversationsRestService);

    void inject(MeRestService meRestService);

    void inject(SupportRestService supportRestService);

    void inject(MegaphoneRestService megaphoneRestService);

    void inject(UpgradeAccountRestService upgradeAccountRestService);

    void inject(SearchRestService searchRestService);

    void inject(BookmarksRestService bookmarksRestService);

    void inject(UsersRestService usersRestService);

    void inject(ListingsRestService listingsRestService);

    void inject(UserListingsRestService userListingsRestService);

    void inject(AppInstallsRestService appInstallsRestService);

    void inject(ImagesRestService imagesRestService);

    void inject(LikesRestService likesRestService);

    void inject(InAppProductsRestService inAppProductsRestService);

    void inject(AndroidReceiptRestService androidReceiptRestService);

    void inject(TransactionsRestService transactionsRestService);

    void inject(SubscriptionRestService subscriptionRestService);

    void inject(NotificationsRestService notificationsRestService);

    void inject(LocaleRestService localeRestService);

    void inject(UserStatusRestService userStatusRestService);

    void inject(FbMutualFriendsService fbMutualFriendsService);

    void inject(BillingRefundsRestService refundsService);


    //managers
    void inject(SettingsManager settingsManager);

    void inject(TokenManager tokenManager);

    void inject(FacebookManager facebookManager);

    void inject(MeManager meManager);

    void inject(ReferrerManager referrerManager);

    void inject(SearchManager searchManager);

    void inject(BookmarkManager bookmarkManager);

    void inject(MegaphoneManager megaphoneManager);

    void inject(GcmManager gcmManager);

    void inject(RoomsterNotificationManager notificationManager);

    void inject(UsersManager usersManager);

    void inject(CatalogsManager catalogsManager);

    void inject(RoomsterLocationManager roomsterLocationManager);

    void inject(NetworkManager networkManager);

    void inject(DiscoveryPrefsManager discoveryPreferencesManager);

    //receivers
    void inject(InstallReceiver installReceiver);

    void inject(AppUpgradeReceiver appUpgradeReceiver);

    void inject(NetworkChangeReceiver networkChangeReceiver);

    //dialog
    void inject(ImageChooserDialog imageChooserDialog);

    void inject(LoadingDialog loadingDialog);

    void inject(CatalogListChooserDialog sharedDetailsChooserDialog);

    void inject(MegaphoneChooserDialogFragment megaphoneChooserDialogFragment);

    void inject(MapRestService mapRestService);
}