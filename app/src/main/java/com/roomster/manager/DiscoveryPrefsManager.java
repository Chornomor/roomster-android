package com.roomster.manager;


import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.roomster.application.RoomsterApplication;
import com.roomster.cache.sharedpreferences.SharedPreferencesFiles;
import com.roomster.constants.ListingType;
import com.roomster.event.ManagersEvents.DiscoveryPrefsManagerEvent;
import com.roomster.model.CatalogModel;
import com.roomster.model.CatalogValueModel;
import com.roomster.model.SearchCriteria;
import com.roomster.rest.model.ListingGetViewModel;
import com.roomster.utils.LocationUtil;
import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;
import java.lang.reflect.Type;
import java.util.List;


/**
 * Created by "Michael Katkov" on 11/8/2015.
 */
public class DiscoveryPrefsManager {

    public static final String VIEW_TYPE = "view_type";
    public static final String DETAILED  = "detailed";
    public static final String LIST      = "list";
    public static final String MAP       = "map";

    public static final String RENTING_OR_FINDING = "renting_or_finding";
    public static final String RENTING_A_PLACE    = "renting_a_place";
    public static final String FINDING_A_PLACE    = "finding_a_place";

    public static final String ROOM_OR_PLACE = "room_or_place";
    public static final String ENTIRE_PLACE  = "entire_place";
    public static final String PRIVATE_ROOM  = "private_room";

    public static final String NEED_ROOM      = "NeedRoom";
    public static final String HAVE_SHARE     = "HaveShare";
    public static final String NEED_APARTMENT = "NeedApartment";
    public static final String HAVE_APARTMENT = "HaveApartment";

    public static final String SOUTH_WEST_LATITUDE  = "south_west_latitude";
    public static final String SOUTH_WEST_LONGITUDE = "south_west_longitude";
    public static final String NORTH_EAST_LATITUDE  = "north_east_latitude";
    public static final String NORTH_EAST_LONGITUDE = "north_east_longitude";
    public static final String LATITUDE             = "latitude";
    public static final String LONGITUDE            = "longitude";
    public static final String LOCATION_STRING      = "location_string";

    public static final String DEFAULT_LOCATION_STRING = "";

    public static final String  TUTORIAL_KEY = "tutorial";
    public static final boolean TUTORIAL_DEFAULT = false;
    public static final  String RADIUS                           = "radius";
    public static final  int    DEFAULT_RADIUS                   = 25000;//radius in meters
    public static final  String MIN_AGE                          = "min_age";
    public static final  int    DEFAULT_MIN_AGE                  = 18;
    public static final  String MAX_AGE                          = "max_age";
    public static final  int    DEFAULT_MAX_AGE                  = 99;
    public static final  String MIN_BUDGET                       = "min_budget";
    public static final  int    DEFAULT_MIN_BUDGET               = 0;
    public static final  String MAX_BUDGET                       = "max_budget";
    public static final  int    DEFAULT_MAX_BUDGET               = 10000;
    public static final  String APARTMENT_TYPE                   = "apartment_type";
    public static final  String BATHROOMS_TYPE                   = "bathrooms_type";
    public static final  String DEFAULT_APARTMENT_TYPE           = "";
    public static final  String DEFAULT_BATHROOM_TYPE            = "";
    public static final  String HOUSEHOLD_SEX                    = "household_sex";
    public static final  int    HOUSEHOLD_SEX_DEFAULT            = -1;
    public static final  String SEX                              = "sex";
    public static final  int    SEX_DEFAULT                      = -1;
    public static final  String APARTMENT_AMENITIES              = "apartment_amenities";
    public static final  String ROOM_AMENITIES                   = "room_amenities";
    public static final  String OWNED_PETS                       = "owned_pets";
    public static final  String PREFERRED_PETS                   = "preferred_pets";
    public static final  String SORT_BY                          = "sort_by";
    public static final  String LAST_ACTIVITY                    = "LastActivity";
    public static final  String NEWEST                           = "Newest";
    public static final  String RENT                             = "MonthlyRent";
    public static final  String DATE_IN                          = "date_in";
    public static final  String DATE_OUT                         = "date_out";
    public static final  String APT_SIZE_UNIT_METRIC             = "SquareMeters";
    public static final  String APT_SIZE_UNIT_IMPERIAL           = "SquareFeet";
    private static final float  MIN_RADIUS_SCALE                 = 0.1f;
    private static final float  MAX_RADIUS_SCALE                 = 2.0f;
    private static final float  MAX_RADIUS                       = 50000.0f;

    @Inject
    SharedPreferencesFiles mSharedPreferencesFiles;

    @Inject
    EventBus mEventBus;

    @Inject
    CatalogsManager mCatalogsManager;

    @Inject
    SettingsManager mSettingsManager;

    @Inject
    MeManager mMeManager;

    @Inject
    Context mContext;


    public DiscoveryPrefsManager() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public int getApartmentTypesIndexById(Integer id) {
        List<CatalogModel> apartmentTypes = mCatalogsManager.getApartmentTypesList();
        if (apartmentTypes != null) {
            for (int i = 0; i < apartmentTypes.size(); i++) {
                CatalogModel type = apartmentTypes.get(i);
                if (type.getId() == id) {
                    return i;
                }
            }
        }
        return -1;
    }


    public String getApartmentTypeByIndex(int index) {
        List<CatalogModel> apartmentTypes = mCatalogsManager.getApartmentTypesList();
        if (apartmentTypes != null && index < apartmentTypes.size()) {
            return apartmentTypes.get(index).getName();
        }
        return null;
    }


    public int getApartmentTypeIdByIndex(int index) {
        List<CatalogModel> apartmentTypes = mCatalogsManager.getApartmentTypesList();
        if (apartmentTypes != null && index < apartmentTypes.size()) {
            return apartmentTypes.get(index).getId();
        }
        return -1;
    }


    public boolean isTutorialWasShowed() {
        return mSharedPreferencesFiles.getUserSharedPreferences().getBoolean(TUTORIAL_KEY, TUTORIAL_DEFAULT);
    }


    public void storeTutorialWasShowed(boolean wasShowed) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putBoolean(TUTORIAL_KEY, wasShowed).commit();
    }


    public void storeEntirePlace() {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putString(ROOM_OR_PLACE, ENTIRE_PLACE).commit();
    }


    public void storePrivateRoom() {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putString(ROOM_OR_PLACE, PRIVATE_ROOM).commit();
    }


    public String getRoomOrPlace() {
        return mSharedPreferencesFiles.getUserSharedPreferences().getString(ROOM_OR_PLACE, null);
    }


    public void storeRentingPlace() {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putString(RENTING_OR_FINDING, RENTING_A_PLACE)
                .commit();
    }


    public void storeFindingPlace() {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putString(RENTING_OR_FINDING, FINDING_A_PLACE)
                .commit();
    }


    public String getRentingOrFinding() {
        return mSharedPreferencesFiles.getUserSharedPreferences().getString(RENTING_OR_FINDING, null);
    }


    public String getServiceType() {
        String roomOrPlace = getRoomOrPlace();
        String rentOrFind = getRentingOrFinding();
        if (roomOrPlace == null || rentOrFind == null) {
            return null;
        }
        if (roomOrPlace.equals(PRIVATE_ROOM) && rentOrFind.equals(FINDING_A_PLACE)) {
            return HAVE_SHARE;
        } else
            if (roomOrPlace.equals(ENTIRE_PLACE) && rentOrFind.equals(FINDING_A_PLACE)) {
                return HAVE_APARTMENT;
            } else
                if (roomOrPlace.equals(PRIVATE_ROOM) && rentOrFind.equals(RENTING_A_PLACE)) {
                    return NEED_ROOM;
                } else
                    if (rentOrFind.equals(ENTIRE_PLACE) && rentOrFind.equals(RENTING_A_PLACE)) {
                        return NEED_APARTMENT;
                    } else {
                        return NEED_APARTMENT;
                    }
    }


    public String getViewType() {
        return mSharedPreferencesFiles.getUserSharedPreferences().getString(VIEW_TYPE, DETAILED);
    }


    public void setViewType(String viewType) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putString(VIEW_TYPE, viewType).commit();
    }


    public void storeLocationString(String locationString) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putString(LOCATION_STRING, locationString).commit();
    }


    public String getLocationString() {
        return mSharedPreferencesFiles.getUserSharedPreferences().getString(LOCATION_STRING, DEFAULT_LOCATION_STRING);
    }


    public void storeLatitude(double latitude) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putString(LATITUDE, String.valueOf(latitude))
                .commit();
    }


    public Double getLatitude() {
        String latidude = mSharedPreferencesFiles.getUserSharedPreferences().getString(LATITUDE, null);
        if (latidude == null) {
            return null;
        }
        return Double.parseDouble(latidude);
    }


    public void storeLongitude(double longitude) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putString(LONGITUDE, String.valueOf(longitude))
                .commit();
    }


    public Double getLongitude() {
        String longitude = mSharedPreferencesFiles.getUserSharedPreferences().getString(LONGITUDE, null);
        if (longitude == null) {
            return null;
        }
        return Double.parseDouble(longitude);
    }


    public boolean isLocationEmpty() {
        return (getLatitude() == null && getLongitude() == null) || (getSouthWestLatitude() == null
                && getNorthEastLatitude() == null && getNorthEastLongitude() == null && getSouthWestLongitude() == null)
                || getLocationString() == null;
    }


    public Double getSouthWestLatitude() {
        String southWestLatitude = mSharedPreferencesFiles.getUserSharedPreferences()
                .getString(SOUTH_WEST_LATITUDE, null);

        if (southWestLatitude != null) {
            return Double.parseDouble(southWestLatitude);
        } else {
            return null;
        }
    }


    public Double getSouthWestLongitude() {
        String southWestLongitude = mSharedPreferencesFiles.getUserSharedPreferences()
                .getString(SOUTH_WEST_LONGITUDE, null);

        if (southWestLongitude == null) {
            return null;
        }
        return Double.parseDouble(southWestLongitude);
    }


    public Double getNorthEastLatitude() {
        String northEastLatitude = mSharedPreferencesFiles.getUserSharedPreferences()
                .getString(NORTH_EAST_LATITUDE, null);

        if (northEastLatitude == null) {
            return null;
        }
        return Double.parseDouble(northEastLatitude);
    }


    public Double getNorthEastLongitude() {
        String northEastLongitude = mSharedPreferencesFiles.getUserSharedPreferences()
                .getString(NORTH_EAST_LONGITUDE, null);

        if (northEastLongitude == null) {
            return null;
        }
        return Double.parseDouble(northEastLongitude);
    }


    public void storeRadius(int value) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putInt(RADIUS, value).apply();
        if (!isLocationEmpty()) {
            Location location = new Location("");
            location.setLatitude(getLatitude());
            location.setLongitude(getLongitude());
            storeLocation(location);
        }
    }


    public int getRadius() {
        return mSharedPreferencesFiles.getUserSharedPreferences().getInt(RADIUS, DEFAULT_RADIUS);
    }


    public float getRadiusScale() {
        return MIN_RADIUS_SCALE + getRadius() * (MAX_RADIUS_SCALE - MIN_RADIUS_SCALE) / MAX_RADIUS;
    }


    public void storeMinAge(int age) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putInt(MIN_AGE, age).apply();
    }


    public int getMinAge() {
        return mSharedPreferencesFiles.getUserSharedPreferences().getInt(MIN_AGE, DEFAULT_MIN_AGE);
    }


    public void storeMaxAge(int age) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putInt(MAX_AGE, age).apply();
    }


    public int getMaxAge() {
        return mSharedPreferencesFiles.getUserSharedPreferences().getInt(MAX_AGE, DEFAULT_MAX_AGE);
    }


    public void storeMinBudget(int value) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putInt(MIN_BUDGET, value).apply();
    }


    public int getMinBudget() {
        return mSharedPreferencesFiles.getUserSharedPreferences().getInt(MIN_BUDGET, DEFAULT_MIN_BUDGET);
    }


    public void storeMaxBudget(int value) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putInt(MAX_BUDGET, value).apply();
    }


    public int getMaxBudget() {
        return mSharedPreferencesFiles.getUserSharedPreferences().getInt(MAX_BUDGET, DEFAULT_MAX_BUDGET);
    }


    public void storeApartmentTypes(String types) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putString(APARTMENT_TYPE, types).apply();
    }


    public String getApartmentTypes() {
        return mSharedPreferencesFiles.getUserSharedPreferences()
                .getString(APARTMENT_TYPE, DEFAULT_APARTMENT_TYPE);
    }


    public void storeBathrooms(String types) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putString(BATHROOMS_TYPE, types).apply();
    }


    public String getBathrooms() {
        return mSharedPreferencesFiles.getUserSharedPreferences().getString(BATHROOMS_TYPE, DEFAULT_BATHROOM_TYPE);
    }


    public void storeHouseholdSex(int value) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putInt(HOUSEHOLD_SEX, value).commit();
    }


    public int getHouseholdSex() {
        return mSharedPreferencesFiles.getUserSharedPreferences().getInt(HOUSEHOLD_SEX, HOUSEHOLD_SEX_DEFAULT);
    }


    public void storeSex(int value) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putInt(SEX, value).commit();
    }


    public int getSex() {
        return mSharedPreferencesFiles.getUserSharedPreferences().getInt(SEX, SEX_DEFAULT);
    }


    public void storeOwnedPets(CatalogValueModel petModel) {
        Gson gson = new Gson();
        List<CatalogValueModel> storedPets = restoreOwnedPets();
        if (storedPets.contains(petModel)) {
            storedPets.remove(petModel);
        } else {
            storedPets.add(petModel);
        }
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putString(OWNED_PETS, gson.toJson(storedPets))
                .commit();
    }


    public void storePreferredPets(CatalogValueModel petModel) {
        Gson gson = new Gson();
        List<CatalogValueModel> storedPets = restorePreferredPets();
        if (storedPets.contains(petModel)) {
            storedPets.remove(petModel);
        } else {
            storedPets.add(petModel);
        }
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putString(PREFERRED_PETS, gson.toJson(storedPets))
                .commit();
    }


    public List<CatalogValueModel> restorePreferredPets() {
        Gson gson = new Gson();
        String jsonOutput = mSharedPreferencesFiles.getUserSharedPreferences().getString(PREFERRED_PETS, "[]");
        Type listType = new TypeToken<List<CatalogValueModel>>() {

        }.getType();
        return gson.fromJson(jsonOutput, listType);
    }


    public List<CatalogValueModel> restoreOwnedPets() {
        Gson gson = new Gson();
        String jsonOutput = mSharedPreferencesFiles.getUserSharedPreferences().getString(OWNED_PETS, "[]");
        Type listType = new TypeToken<List<CatalogValueModel>>() {

        }.getType();
        return gson.fromJson(jsonOutput, listType);
    }


    public void storeApartmentAmenities(CatalogValueModel amenityModel) {
        Gson gson = new Gson();
        List<CatalogValueModel> storedAmenities = restoreApartmentAmenities();
        if (storedAmenities.contains(amenityModel)) {
            storedAmenities.remove(amenityModel);
        } else {
            storedAmenities.add(amenityModel);
        }
        mSharedPreferencesFiles.getUserSharedPreferences().edit()
                .putString(APARTMENT_AMENITIES, gson.toJson(storedAmenities)).commit();
        mEventBus.post(new DiscoveryPrefsManagerEvent.UpdateAmenitiesEvent());
    }


    public List<CatalogValueModel> restoreApartmentAmenities() {
        Gson gson = new Gson();
        String jsonOutput = mSharedPreferencesFiles.getUserSharedPreferences().getString(APARTMENT_AMENITIES, "[]");
        Type listType = new TypeToken<List<CatalogValueModel>>() {

        }.getType();
        return gson.fromJson(jsonOutput, listType);
    }


    public void storeRoomAmenities(CatalogValueModel amenityModel) {
        Gson gson = new Gson();
        List<CatalogValueModel> storedAmenities = restoreRoomAmenities();
        if (storedAmenities.contains(amenityModel)) {
            storedAmenities.remove(amenityModel);
        } else {
            storedAmenities.add(amenityModel);
        }
        mSharedPreferencesFiles.getUserSharedPreferences().edit()
                .putString(ROOM_AMENITIES, gson.toJson(storedAmenities)).commit();
        mEventBus.post(new DiscoveryPrefsManagerEvent.UpdateAmenitiesEvent());
    }


    public List<CatalogValueModel> restoreRoomAmenities() {
        Gson gson = new Gson();
        String jsonOutput = mSharedPreferencesFiles.getUserSharedPreferences().getString(ROOM_AMENITIES, "[]");
        Type listType = new TypeToken<List<CatalogValueModel>>() {

        }.getType();
        return gson.fromJson(jsonOutput, listType);
    }


    public void storeSortBy(String value) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putString(SORT_BY, value).commit();
    }


    public String getSortBy() {
        return mSharedPreferencesFiles.getUserSharedPreferences().getString(SORT_BY, LAST_ACTIVITY);
    }


    public void storeDateIn(String date) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putString(DATE_IN, date).commit();
    }


    public String getDateIn() {
        return mSharedPreferencesFiles.getUserSharedPreferences().getString(DATE_IN, null);
    }


    public void storeDateOut(String date) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putString(DATE_OUT, date).commit();
    }


    public String getDateOut() {
        return mSharedPreferencesFiles.getUserSharedPreferences().getString(DATE_OUT, null);
    }


    public void storeLocation(Location location) {
        double radiusMeters = LocationUtil.milesToMeters(getRadius() / 1000);
        LatLngBounds bounds = LocationUtil.getBounds(location.getLatitude(), location.getLongitude(), radiusMeters);
        storeSouthWestLatitude(bounds.southwest.latitude);
        storeSouthWestLongitude(bounds.southwest.longitude);
        storeNorthEastLatitude(bounds.northeast.latitude);
        storeNorthEastLongitude(bounds.northeast.longitude);
        storeLatitude(location.getLatitude());
        storeLongitude(location.getLongitude());
    }


    public LatLngBounds getLastBounds() {
        if (!isLocationEmpty()) {
            Double southWestLat = getSouthWestLatitude();
            Double southWestLong = getSouthWestLongitude();
            Double northEastLat = getNorthEastLatitude();
            Double northEastLong = getNorthEastLongitude();

            return new LatLngBounds(new LatLng(southWestLat, southWestLong), new LatLng(northEastLat, northEastLong));
        }
        return null;
    }


    public SearchCriteria getSearchCriteria() {
        initLocation();

        SearchCriteria criteria = new SearchCriteria();
        criteria.setSort(getSortBy());
        criteria.setServiceType(getServiceType());
        criteria.setRadiusScale(getRadiusScale());
        criteria.setBudgetMin(getMinBudget());
        criteria.setBudgetMax(getMaxBudget());
        criteria.setAgeMin(getMinAge());
        criteria.setAgeMax(getMaxAge());
        criteria.setDataIn(getDateIn());
        criteria.setDataOut(getDateOut());

        String currency = mSettingsManager.getCurrency();
        criteria.setCurrency(currency);

        Double latSouthWest = getSouthWestLatitude();
        if (latSouthWest != null) {
            criteria.setLatSouthWest(latSouthWest);
        }

        Double longSouthWest = getSouthWestLongitude();
        if (longSouthWest != null) {
            criteria.setLongSouthWest(longSouthWest);
        }

        Double latNorthEast = getNorthEastLatitude();
        if (latNorthEast != null) {
            criteria.setLatNorthEast(latNorthEast);
        }

        Double longNorthEast = getNorthEastLongitude();
        if (longNorthEast != null) {
            criteria.setLongNorthEast(longNorthEast);
        }

        //If there is no service type set, we get the service type from the most recent user's listing if exist
        if (criteria.getServiceType() == null) {
            initServiceTypeFromListing();
        }

        //There maybe no listing to get the service type from.
        if (criteria.getServiceType() != null) {
            if (criteria.getServiceType().equals(DiscoveryPrefsManager.NEED_APARTMENT) || criteria.getServiceType()
                    .equals(DiscoveryPrefsManager.HAVE_APARTMENT)) {
                criteria.setBedrooms(prepareBedroomsParam());
                criteria.setBathrooms(prepareBathroomsParam());
                criteria.setAmenities(prepareAmenitiesParam(restoreApartmentAmenities()));
            }

            if (criteria.getServiceType().equals(DiscoveryPrefsManager.HAVE_SHARE)) {
                int householdSex = getHouseholdSex();
                if (householdSex != HOUSEHOLD_SEX_DEFAULT) {
                    criteria.setHouseholdSex(String.valueOf(householdSex));
                } else {
                    criteria.setHouseholdSex(null);
                }
                criteria.setPetsPrefs(preparePetsParam(restorePreferredPets()));
            }

            if (criteria.getServiceType().equals(DiscoveryPrefsManager.NEED_ROOM)) {
                criteria.setMyPets(preparePetsParam(restoreOwnedPets()));
                int sex = getSex();
                if (sex != SEX_DEFAULT) {
                    criteria.setSex(String.valueOf(sex));
                } else {
                    criteria.setSex(null);
                }
            }
        }

        return criteria;
    }


    private void initLocation() {
        if (isLocationEmpty()) {
            //If we don't have any stored location, we use the location of the user's initial listing
            List<ListingGetViewModel> userListings = mMeManager.getMyListings();
            if (userListings != null && userListings.size() != 0) {
                ListingGetViewModel listing = userListings.get(userListings.size() - 1);
                //                storeSouthWestLatitude(listing.getGeoLocation().getSwLat());
                //                storeSouthWestLongitude(listing.getGeoLocation().getSwLng());
                //                storeNorthEastLatitude(listing.getGeoLocation().getNeLat());
                //                storeNorthEastLongitude(listing.getGeoLocation().getNeLng());
                //                storeLatitude(listing.getGeoLocation().getLat());
                //                storeLongitude(listing.getGeoLocation().getLng());
                Location location = new Location("");
                location.setLatitude(listing.getGeoLocation().getLat());
                location.setLongitude(listing.getGeoLocation().getLng());
                storeLocation(location);
                storeLocationString(listing.getGeoLocation().getFullAddress());
            }
        }
    }


    private void initServiceTypeFromListing() {
        // get service type from most recent listing
        List<ListingGetViewModel> userListings = mMeManager.getMyListings();
        if (userListings != null && userListings.size() != 0) {
            ListingGetViewModel listing = userListings.get(userListings.size() - 1);
            CatalogModel serviceTypeModel = mCatalogsManager.getServiceTypeById(listing.getServiceType());
            if (serviceTypeModel != null) {
                switch (serviceTypeModel.getName()) {
                case ListingType.NEED_ROOM:
                    storeFindingPlace();
                    storePrivateRoom();
                    break;
                case ListingType.HAVE_SHARE:
                    storeRentingPlace();
                    storePrivateRoom();
                    break;
                case ListingType.NEED_APARTMENT:
                    storeFindingPlace();
                    storeEntirePlace();
                    break;
                case ListingType.HAVE_APARTMENT:
                    storeRentingPlace();
                    storeEntirePlace();
                    break;
                default:
                    break;
                }
            }
        }
    }


    private String preparePetsParam(List<CatalogValueModel> pets) {
        String param;
        if (pets.isEmpty()) {
            param = null;
        } else {
            Integer[] ids = new Integer[pets.size()];
            for (int i = 0; i < pets.size(); i++) {
                ids[i] = pets.get(i).getId();
            }
            param = TextUtils.join(",", ids);
        }
        return param;
    }


    private String prepareAmenitiesParam(List<CatalogValueModel> amenities) {
        String param = null;
        if (!amenities.isEmpty()) {
            Integer[] ids = new Integer[amenities.size()];
            for (int i = 0; i < amenities.size(); i++) {
                ids[i] = amenities.get(i).getId();
            }
            param = TextUtils.join(",", ids);
        }
        return param;
    }


    private String prepareBedroomsParam() {
        List<CatalogModel> bedrooms = mCatalogsManager.getBedroomsList();
        if (bedrooms != null && !getApartmentTypes().equals(DEFAULT_APARTMENT_TYPE)) {
            String[] types = getApartmentTypes().split(",");
            int size = types.length;
            Integer[] ids = new Integer[size];
            for (int i = 0; i < size; i++) {
                ids[i] = bedrooms.get(Integer.parseInt(types[i])).getId();
            }
            return TextUtils.join(",", ids);
        } else {
            return "";
        }
    }


    private String prepareBathroomsParam() {
        List<CatalogModel> bathrooms = mCatalogsManager.getBathroomsList();
        if (bathrooms != null && !getBathrooms().equals(DEFAULT_BATHROOM_TYPE)) {
            String[] types = getBathrooms().split(",");
            int size = types.length;
            Integer[] ids = new Integer[size];
            for (int i = 0; i < size; i++) {
                ids[i] = bathrooms.get(Integer.parseInt(types[i])).getId();
            }
            return TextUtils.join(",", ids);
        } else {
            return "";
        }
    }


    private void storeSouthWestLatitude(double latitude) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit()
                .putString(SOUTH_WEST_LATITUDE, String.valueOf(latitude)).commit();
    }


    private void storeSouthWestLongitude(double longitude) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit()
                .putString(SOUTH_WEST_LONGITUDE, String.valueOf(longitude)).commit();
    }


    private void storeNorthEastLatitude(double latitude) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit()
                .putString(NORTH_EAST_LATITUDE, String.valueOf(latitude)).commit();
    }


    private void storeNorthEastLongitude(double longitude) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit()
                .putString(NORTH_EAST_LONGITUDE, String.valueOf(longitude)).commit();
    }
}