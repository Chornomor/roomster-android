package com.roomster.utils;


import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.roomster.views.ReplacableViewPager;


/**
 * Created by "Michael Katkov" on 2/2/2016.
 */
public class FancyImageAnimator implements ReplacableViewPager.OnPageChangeListener {

    private static final String TAG = "FancyImageAnimator";

    private static final int DELAY = 200;
    private static final int LEFT  = 1;
    private static final int RIGHT = 2;
    private static final int NONE  = 3;

    private int     mDirection;
    private boolean mIsAnimationStarted;

    private int   mPosition        = 0;
    private float mPossitionOffset = 0.0f;

    private int mState;

    private ImageView mLeftImage;

    private ImageView mRightImage;

    private ReplacableViewPager mViewPager;


    public FancyImageAnimator(ReplacableViewPager viewPager, ImageView leftImage, ImageView rightImage) {
        mLeftImage = leftImage;
        mRightImage = rightImage;
        mViewPager = viewPager;
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        LogUtil.logD(TAG + "3",
                "Position: " + position + " PositionOffset: " + positionOffset + " PositionOffsetPixels: " + positionOffsetPixels);
        if (!mIsAnimationStarted) {
            mIsAnimationStarted = isAnimationStarted(position, positionOffset);
        } else {
            checkAnimationChange(position);
        }
        mPosition = position;
        mPossitionOffset = positionOffset;
    }


    @Override
    public void onPageSelected(int position) {
        LogUtil.logD(TAG + "2", "Position: " + position);
        stopAnimate();
    }


    @Override
    public void onPageScrollStateChanged(int state) {
        LogUtil.logD(TAG + "1", "State: " + state);
        if (state == ReplacableViewPager.SCROLL_STATE_SETTLING) {
            stopAnimate();
        } else if (state == ReplacableViewPager.SCROLL_STATE_IDLE && mState != ReplacableViewPager.SCROLL_STATE_SETTLING) {
            stopAnimate();
        } else if (state == ReplacableViewPager.SCROLL_STATE_DRAGGING) {
            mIsAnimationStarted = false;
        }
        mState = state;
    }


    private boolean isAnimationStarted(int position, float positionOffset) {
        if (mPosition == position && mPossitionOffset < positionOffset) {
            mDirection = LEFT;
            showLeft();
            return true;
        } else if (mPosition > position) {
            mDirection = RIGHT;
            showRight();
            return true;
        }
        return false;
    }


    private void checkAnimationChange(int position) {
        if(mViewPager == null || mViewPager.getAdapter() == null) {
            return;
        }
        int lastItem = mViewPager.getAdapter().getCount() - 1;
        boolean isNotLastItem = position != lastItem && mPosition != lastItem;
        if (mState == ReplacableViewPager.SCROLL_STATE_DRAGGING && isNotLastItem) {
            if (mPosition < position) {
                mDirection = LEFT;
                showLeft();
                hideRight();
            } else if (mPosition > position) {
                mDirection = RIGHT;
                showRight();
                hideLeft();
            }
        }
    }


    private void stopAnimate() {
        switch (mDirection) {
            case LEFT:
                hideLeft();
                break;
            case RIGHT:
                hideRight();
                break;
        }
        mDirection = NONE;
    }


    public void showLeft() {
        fadeIn(mLeftImage);
    }


    public void showRight() {
        fadeIn(mRightImage);
    }


    public void hideLeft() {
        fadeOut(mLeftImage);
    }


    public void hideRight() {
        fadeOut(mRightImage);
    }


    private void fadeIn(View view) {
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator());
        fadeIn.setDuration(DELAY);
        view.startAnimation(fadeIn);
        view.setVisibility(View.VISIBLE);
    }


    private void fadeOut(View view) {
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setStartOffset(DELAY);
        fadeOut.setDuration(DELAY);
        view.startAnimation(fadeOut);
        view.setVisibility(View.GONE);
    }
}
