package com.roomster.event.RestServiceEvents;


import com.roomster.event.Event;
import com.roomster.rest.service.AccountRestService;


/**
 * Created by Bogoi.Bogdanov on 10/6/2015.
 */
public class AccountRestServiceEvent extends Event {

    public static class LoginSuccess extends AccountRestServiceEvent {

        public boolean isNewUser;
    }


    public static class LoginFailure extends AccountRestServiceEvent {

        public String errorMessage;
        public int    code;

        public LoginFailure() {
        }

        public LoginFailure(int code) {
            this.code = code;
        }
    }


    public static class EmailWasNotConfirmOrAdd extends AccountRestServiceEvent {

    }


    public static class DeleteSuccess extends AccountRestServiceEvent {

    }


    public static class DeleteFailure extends AccountRestServiceEvent {
        public int code = 0;
        public String errorMessage;

        public DeleteFailure(){

        }
        public DeleteFailure(int code) {
            this.code = code;
        }
    }


    public static class UpdateSuccess extends AccountRestServiceEvent {

    }


    public static class UpdateFailure extends AccountRestServiceEvent {

    }
}
