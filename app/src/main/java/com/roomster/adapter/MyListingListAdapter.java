package com.roomster.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.ListingType;
import com.roomster.manager.CatalogsManager;
import com.roomster.model.CatalogModel;
import com.roomster.rest.model.ListingGetViewModel;
import com.roomster.rest.model.UserGetViewModel;
import com.roomster.utils.DateUtil;
import com.roomster.utils.ImageHelper;
import com.roomster.views.undo.ItemExtractor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.inject.Inject;


public class MyListingListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int NORMAL = 1;
    private static final int FOOTER = 2;

    private List<ListingGetViewModel> mItems;
    private UserGetViewModel mMeModel;
    private ItemClickListener mItemClickListener;
    private Context mContext;
    private View.OnClickListener mFooterClickListener;

    @Inject
    CatalogsManager mCatalogsManager;


    public MyListingListAdapter(Context context, List<ListingGetViewModel> items, UserGetViewModel meModel) {
        RoomsterApplication.getRoomsterComponent().inject(this);
        mContext = context;
        setItems(items);
        setModel(meModel);
    }


    public void setItems(List<ListingGetViewModel> items) {
        if (items != null) {
            mItems = items;
        } else {
            mItems = new ArrayList<>();
        }
    }


    public void setModel(UserGetViewModel meModel) {
        if (meModel != null) {
            mMeModel = meModel;
        } else {
            mMeModel = new UserGetViewModel();
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (position == getItemCount() - 1) {
            return FOOTER;
        } else {
            return NORMAL;
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root;
        switch (viewType) {
            case NORMAL:
                root = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_listing, parent, false);
                return new ViewHolder(root);
            case FOOTER:
                root = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_add_listing, parent, false);
                return new FooterViewHolder(root);
            default:
                root = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_listing, parent, false);
                return new ViewHolder(root);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        switch (getItemViewType(position)) {
            case NORMAL:
                onBindNormalViewHolder((ViewHolder) holder, position);
                break;
            case FOOTER:
                onBindFooterViewHolder((FooterViewHolder) holder);
                break;
        }
    }


    @Override
    public int getItemCount() {
        return mItems.size() + 1;
    }

    private void onBindNormalViewHolder(final ViewHolder holder, final int position) {
        final ListingGetViewModel listItem = mItems.get(position);

        ImageHelper.loadImageInto(mContext, listItem.getImages().get(0), holder.apartmentImageView);

        loadOrHideBubbleImage(holder, listItem);

        setLocation(listItem, holder);
        setHeadline(listItem, holder);
        setBookmark(listItem, holder);
        setDate(listItem, holder);
        setListingType(listItem, holder);
        setPriceAndCurrency(listItem, holder);
        setNameAndAge(holder);
        setGender(holder);
        removeBookmark(holder);
        holder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(position, listItem);
                }
            }
        });
        holder.editImageView.setVisibility(View.VISIBLE);
        holder.editImageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClickEdit(position, listItem);
                }
            }
        });
        holder.optionsImageView.setVisibility(View.VISIBLE);
        holder.optionsImageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClickOptions(position, listItem, holder.optionsImageView);
                }
            }
        });
    }


    private void removeBookmark(ViewHolder holder) {
        holder.bookmarkImageView.setVisibility(View.GONE);
    }


    private void setHeadline(ListingGetViewModel listItem, ViewHolder holder) {
        String headline = listItem.getHeadlineActual();

        if (headline != null && !headline.isEmpty()) {
            holder.descriptionTextView.setText(headline);
        }
    }


    private void loadOrHideBubbleImage(ViewHolder holder, ListingGetViewModel listItem) {
        String userPhoto = mMeModel.getTitleImage();
        if (isListingOfOfferType(listItem)) {
            holder.personProfileImage.setVisibility(View.VISIBLE);
            ImageHelper.loadBorderedCircledImageInto(mContext, userPhoto, holder.personProfileImage);
        } else {
            holder.personProfileImage.setVisibility(View.GONE);
        }
    }


    private void onBindFooterViewHolder(FooterViewHolder holder) {
        holder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mFooterClickListener.onClick(v);
            }
        });
    }


    private void setLocation(ListingGetViewModel item, ViewHolder holder) {
        if (item != null && item.getGeoLocation() != null && item.getGeoLocation().getFullAddress() != null) {
            holder.headlineTextView.setText(item.getGeoLocation().getGoogleFormattedAddress());
        }
    }


    public ListingGetViewModel getItem(int position) {
        if (0 <= position && position < mItems.size()) {
            return mItems.get(position);
        } else {
            return null;
        }
    }


    private void setBookmark(ListingGetViewModel item, ViewHolder holder) {
        if (item.getIsBookmarked()) {
            holder.bookmarkImageView.setImageResource(R.drawable.ic_bookmark_light_active);
        } else {
            holder.bookmarkImageView.setImageResource(R.drawable.ic_bookmark_light);
        }
    }


    private void setDate(ListingGetViewModel item, ViewHolder holder) {
        String date = DateUtil.getRelativeTimeSpanString(mContext, item.getRefreshed());
        holder.listingDateTextView.setText(date);
    }


    private void setNameAndAge(ViewHolder holder) {
        String nameAndAge = mMeModel.getFirstName() + ", " + mMeModel.getAge();
        holder.userNameAndGender.setText(nameAndAge);
    }


    private void setListingType(ListingGetViewModel listItem, ViewHolder holder) {
        CatalogModel serviceTypeModel = mCatalogsManager.getServiceTypeById(listItem.getServiceType());
        if (serviceTypeModel != null) {
            switch (serviceTypeModel.getName()) {
                case ListingType.NEED_ROOM:
                    holder.listingType.setText(mContext.getString(R.string.al_looking_for_room));
                    break;
                case ListingType.NEED_APARTMENT:
                    holder.listingType.setText(mContext.getString(R.string.al_looking_for_apartment));
                    break;
                case ListingType.HAVE_SHARE:
                    holder.listingType.setText(mContext.getString(R.string.al_offering_room));
                    break;
                case ListingType.HAVE_APARTMENT:
                    holder.listingType.setText(mContext.getString(R.string.al_offering_apartment));
                    break;
                default:
                    break;
            }
        }
    }


    private void setGender(ViewHolder holder) {
        if (mMeModel != null && mMeModel.getGender() != null) {
            switch (mMeModel.getGender()) {
                case Male:
                    holder.personGenderImageView.setImageResource(R.drawable.ic_male);
                    break;
                case Female:
                    holder.personGenderImageView.setImageResource(R.drawable.ic_female_small);
                    break;
                default:
                    break;
            }
        }
    }

    public ItemExtractor extract(long id) {
        ListingGetViewModel model = getById(id);
        if (model == null)
            return null;
        int position = mItems.indexOf(model);
        mItems.remove(model);
        notifyItemRemoved(position);
        return new ItemExtractor(position, model);
    }

    public ListingGetViewModel getById(long id) {
        for (ListingGetViewModel model : mItems)
            if (model.getListingId().equals(id))
                return model;
        return null;
    }

    public void restoreListing(ItemExtractor itemExtractor) {
        mItems.add(itemExtractor.position, (ListingGetViewModel) itemExtractor.object);
        notifyDataSetChanged();
    }


    private void setPriceAndCurrency(ListingGetViewModel item, ViewHolder holder) {
        if (item.getRates() != null && item.getRates().getRequestedMonthlyRate() != null) {

            // Set price
            holder.priceTextView.setText(String.valueOf(item.getRates().getMonthlyRate()));
            // Set currency
            String currencyCode = item.getRates().getCurrency();

            holder.currencyCodeTextView.setText(currencyCode);
            String currencySymbol = Currency.getInstance(currencyCode).getSymbol();
            if (currencySymbol != null && !currencySymbol.isEmpty()) {
                // use just the last char of the symbol (US$ -> $)
                holder.currencySignTextView.setText(Character.toString(currencySymbol.charAt(currencySymbol.length() - 1)));
            }
        }
    }


    private boolean isListingOfOfferType(ListingGetViewModel listingGetViewModel) {
        CatalogModel serviceType = mCatalogsManager.getServiceTypeById(listingGetViewModel.getServiceType());
        String name = null;
        if (serviceType != null) {
            name = serviceType.getName();
        }

        return name != null && (name.equals(ListingType.HAVE_APARTMENT) || name.equals(ListingType.HAVE_SHARE));
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView apartmentImageView;
        TextView userNameAndGender;
        ImageView personProfileImage;
        ImageView personGenderImageView;
        TextView listingType;
        TextView descriptionTextView;
        TextView headlineTextView;
        TextView priceTextView;
        TextView currencySignTextView;
        TextView currencyCodeTextView;
        TextView listingDateTextView;
        ImageView bookmarkImageView;
        ImageView editImageView;
        ImageView optionsImageView;


        public ViewHolder(View root) {
            super(root);
            apartmentImageView = (ImageView) root.findViewById(R.id.list_item_listing_apartment_image);
            userNameAndGender = (TextView) root.findViewById(R.id.user_name);
            personProfileImage = (ImageView) root.findViewById(R.id.list_item_person_image);
            personGenderImageView = (ImageView) root.findViewById(R.id.list_item_listing_person_gender);
            listingType = (TextView) root.findViewById(R.id.listing_type);
            descriptionTextView = (TextView) root.findViewById(R.id.listing_description_text);
            headlineTextView = (TextView) root.findViewById(R.id.listing_headline_text);
            priceTextView = (TextView) root.findViewById(R.id.price_text_view);
            currencySignTextView = (TextView) root.findViewById(R.id.currency_sign_text_view);
            currencyCodeTextView = (TextView) root.findViewById(R.id.currency_text_view);
            listingDateTextView = (TextView) root.findViewById(R.id.list_item_listing_date);
            bookmarkImageView = (ImageView) root.findViewById(R.id.list_item_listing_bookmark_icon);
            editImageView = (ImageView) root.findViewById(R.id.list_item_listing_edit_icon);
            optionsImageView = (ImageView) root.findViewById(R.id.list_item_listing_options_icon);
        }


        public void setOnClickListener(View.OnClickListener onClickListener) {
            itemView.setOnClickListener(onClickListener);
        }
    }


    public interface ItemClickListener {

        void onItemClick(int listingIndex, ListingGetViewModel listingGetViewModel);

        void onItemClickEdit(int listingIndex, ListingGetViewModel listingGetViewModel);

        void onItemClickOptions(int listingIndex, ListingGetViewModel listingGetViewModel, View view);
    }


    public void setItemClickListener(ItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }


    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout layoutView;
        ImageView addButtonView;
        TextView text1View;


        public FooterViewHolder(View root) {
            super(root);
            layoutView = (RelativeLayout) root.findViewById(R.id.footer_add_listing_layout);
            addButtonView = (ImageView) root.findViewById(R.id.footer_add_listing_add);
            text1View = (TextView) root.findViewById(R.id.footer_add_listing_text1);
        }


        public void setOnClickListener(View.OnClickListener onClickListener) {
            itemView.setOnClickListener(onClickListener);
        }
    }


    public void setFooterClickListener(View.OnClickListener onClickListener) {
        mFooterClickListener = onClickListener;
    }


}
