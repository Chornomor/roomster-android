package com.roomster.event.RestServiceEvents;


import com.roomster.event.Event;


/**
 * Created by michaelkatkov on 1/11/16.
 */
public class FBMutualFriendsServiceEvent extends Event {

    public static class GetSuccess extends Event {

        public int friendsCount;
    }

    public static class GetFailed extends Event {

    }
}
