package com.roomster.views;


import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import com.edmodo.rangebar.RangeBar;
import com.roomster.R;
import com.roomster.adapter.AmenitiesGridAdapter;
import com.roomster.adapter.PetsAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.DiscoveryConstants;
import com.roomster.constants.ListingType;
import com.roomster.dialog.CatalogListChooserDialog;
import com.roomster.event.RestServiceEvents.CatalogRestServiceEvent;
import com.roomster.manager.CatalogsManager;
import com.roomster.manager.DiscoveryPrefsManager;
import com.roomster.model.CatalogModel;
import com.roomster.model.CatalogValueModel;
import com.roomster.rest.model.Calendar;
import com.roomster.rest.model.HaveApartment;
import com.roomster.rest.model.ListingPostViewModel;
import com.roomster.rest.model.ListingPutViewModel;
import com.roomster.rest.model.NeedApartment;
import com.roomster.rest.model.ShareDetails;
import com.roomster.rest.service.CatalogRestService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


public class AddListingAdditionalFields extends LinearLayout {

    private static final int HEADLINE_CHARS_LIMIT    = 35;
    private static final int DESCRIPTION_CHARS_LIMIT = 300;
    private static final int ROW                     = 5;
    private static final int AMENITIES_COLUMNS       = 4;


    private Integer mAdditionalMinStayId                = -1;
    private Integer mResidenceBuildTypeId               = -1;
    private Integer mPeopleInHouseholdId                = -1;
    private Integer mSexHouseholdId                     = -1;
    private Integer mSmokingPreferenceId                = -1;
    private int prefAgeRangeBarTickCount;
    private int houseHoldAgeRangeBarTickCount;

    private boolean mNeedToShowAdditionalMinStayDialog  = false;
    private boolean mNeedToShowBuildTypeDialog          = false;
    private boolean mNeedToShowPeopleInHouseholdDialog  = false;
    private boolean mNeedToShowSexHouseholdDialog       = false;
    private boolean mNeedToSmokingPreferenceDialog      = false;

    private int                  mType;
    private AmenitiesGridAdapter mAmenitiesAdapter;

    @Inject
    CatalogsManager mCatalogsManager;

    @Bind(R.id.al_additional_minimum_stay_layout)
    LinearLayout mMinAdditionalStayLayout;

    @Bind(R.id.al_additional_minimum_stay_value)
    TextView mAdditionalMinStayValueTv;

    @Bind(R.id.al_residence_building_type_layout)
    LinearLayout mResidenceBuildTypeLayout;

    @Bind(R.id.al_residence_building_type_value)
    TextView mResidenceBuildTypeValue;

    @Bind(R.id.al_people_in_household_layout)
    LinearLayout mPeopleInHouseholdLayout;

    @Bind(R.id.al_people_in_household_value_tv)
    TextView mPeopleInHouseholdValueTv;

    @Bind(R.id.al_sex_household_layout)
    LinearLayout mSexHouseholdLayour;

    @Bind(R.id.al_sex_household_value_tv)
    TextView mSexHouseholdValueTv;

    @Bind(R.id.al_sex_household_arrow)
    ImageView mSexHouseholdArrowIv;

    @Bind(R.id.al_smoking_preference_layout)
    LinearLayout mSmokingPreferenceLayout;

    @Bind(R.id.al_smoking_preference_value_tv)
    TextView mSmokingPreferenceValueTv;

    @Bind(R.id.al_smoking_preference_arrow)
    ImageView mSmokingPreferenceArrowIv;

    @Bind(R.id.al_headline_limit)
    TextView mHeadlineLimitTextView;

    @Bind(R.id.al_headline_text)
    EditText mHeadlineEditText;

    @Bind(R.id.al_description_limit)
    TextView mDescriptionLimitTextView;

    @Bind(R.id.al_description_text)
    EditText mDescriptionEditText;

    @Nullable
    @Bind(R.id.al_global_household_layout)
    View mHouseholdLayout;

    @Bind(R.id.al_lifestyle_title)
    View mLifestyleTitle;

    @Bind(R.id.al_lifestyle_shared_details_view)
    SharedDetailsView mLifestyleShareDetails;

    @Bind(R.id.al_household_age_layout)
    View mHouseholdAgeLayout;

    @Bind(R.id.al_household_age_value)
    TextView mHouseholdAgeValue;

    @Bind(R.id.al_household_age_range)
    RangeBar mHouseholdAgeRange;

    @Bind(R.id.al_my_pets_title)
    TextView mMyPetsTitle;

    @Bind(R.id.al_my_pets_grid)
    GridView mMyPetsGrid;

    @Bind(R.id.al_roommate_prefs_layout)
    View mRoommatePrefsLayout;

    @Bind(R.id.al_roommate_prefs_age_layout)
    View mRoommatePrefsAgeLayout;

    @Bind(R.id.al_roommate_prefs_age_value)
    TextView mRoommatePrefsAgeValue;

    @Bind(R.id.al_roommate_prefs_age_range)
    RangeBar mRoommatePrefsAgeRange;

    @Bind(R.id.al_roommate_prefs_pets_title)
    TextView mRoommatePrefsPetsTitle;

    @Bind(R.id.al_roommate_prefs_pets_grid)
    GridView mRoommatePrefsPetsGrid;

    @Bind(R.id.al_residence_layout)
    View mResidenceLayout;

    @Bind(R.id.al_residence_move_in_fee_layout)
    View mResidenceMoveInFeeLayout;

    @Bind(R.id.al_residence_move_in_fee_et)
    EditText mResidenceMoveInFee;

    @Bind(R.id.al_residence_cost_of_utilities_layout)
    View mResidenceUtilitiesCostLayout;

    @Bind(R.id.al_residence_cost_of_utilities_et)
    EditText mResidenceUtilitiesCost;

    @Bind(R.id.al_residence_parking_rent_layout)
    View mResidenceParkingRentLayout;

    @Bind(R.id.al_residence_parking_rent_et)
    EditText mResidenceParkingRent;

    @Bind(R.id.al_residence_furnished_layout)
    RelativeLayout mResidenceFurnishedLayout;

    @Bind(R.id.al_residence_furnished_toggle)
    ToggleButton mResidenceFurnishedToggle;

    @Bind(R.id.al_students_only_layout)
    View mStudentsOnlyLayout;

    @Bind(R.id.al_students_only_toggle)
    ToggleButton mStudentsOnlyToggle;

    @Bind(R.id.al_room_amenities_layout)
    LinearLayout mRoomAmenitiesLayout;

    @Bind(R.id.al_room_amenities)
    GridView mRoomAmenitiesGrid;

    @Bind(R.id.al_amenity_grid)
    GridView mAmenityGridView;

    @Bind(R.id.al_amenities_grid_layout)
    LinearLayout mAmenityGridLayout;
    
    @Bind(R.id.al_amenities_tv)
    TextView apartmentAmenitiesTitle;

    @Inject
    CatalogRestService mCatalogRestService;

    @Inject
    DiscoveryPrefsManager mDPrefManager;

    private AmenitiesGridAdapter mRoomAmenitiesAdapter;
    private PetsAdapter          mPetsPreferredAdapter;
    private PetsAdapter          mPetsOwnedAdapter;


    public AddListingAdditionalFields(Context context) {
        super(context);
        init();
    }


    public AddListingAdditionalFields(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    public AddListingAdditionalFields(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    @OnTextChanged(R.id.al_headline_text)
    void onHeadlineTextChanged(CharSequence text) {
        String headlineText = text.toString();
        int remainingChars = getHeadlineRemainingCharactersCount(headlineText);
        mHeadlineLimitTextView.setText(String.valueOf(remainingChars));

        //Make the first letter of the headline text capital.
        if (text.length() == 1 && Character.isLowerCase(text.charAt(0))) {
            mHeadlineEditText.setText(headlineText.toUpperCase());
            mHeadlineEditText.setSelection(1);
        }
    }


    @OnTextChanged(R.id.al_description_text)
    void onDescriptionTextChanged(CharSequence text) {
        String descriptionText = text.toString();
        int remainingChars = getDescriptionRemainingCharactersCount(descriptionText);
        mDescriptionLimitTextView.setText(String.valueOf(remainingChars));

        //Make the first letter of the description text capital.
        if (text.length() == 1 && Character.isLowerCase(text.charAt(0))) {
            mDescriptionEditText.setText(descriptionText.toUpperCase());
            mDescriptionEditText.setSelection(1);
        }
    }


    public void setListingType(int type) {
        mType = type;
        initLayoutForType();
    }


    public void saveSelection(ListingPostViewModel listingPostViewModel) {
        listingPostViewModel.setIsActive(true);
        switch (mType) {
        case ListingType.FINDING_ENTIRE_PLACE_TYPE:
            saveNeedApartment(listingPostViewModel);
            break;
        case ListingType.OFFERING_ENTIRE_PLACE_TYPE:
            saveHaveApartment(listingPostViewModel);
            break;
        case ListingType.OFFERING_ROOM_TYPE:
            saveHaveRoom(listingPostViewModel);
            break;
        case ListingType.FINDING_ROOM_TYPE:
            saveNeedRoom(listingPostViewModel);
            break;
        default:
            break;
        }
    }


    private void saveNeedRoom(ListingPostViewModel listingPostViewModel) {
        ShareDetails shareDetails = listingPostViewModel.getShareDetails() == null ?
                new ShareDetails() :
                listingPostViewModel.getShareDetails();
        saveShareDetails(shareDetails);
        listingPostViewModel.setShareDetails(shareDetails);
    }


    private void mapShareDetails(SharedDetailsView view, ShareDetails shareDetails) {
        ShareDetails roommatePrefsDetails = view.getModel();
        if (roommatePrefsDetails != null) {
            shareDetails.setCleanliness(roommatePrefsDetails.getCleanliness());
            shareDetails.setOvernightGuests(roommatePrefsDetails.getOvernightGuests());
            shareDetails.setPartyHabits(roommatePrefsDetails.getPartyHabits());
            shareDetails.setGetUp(roommatePrefsDetails.getGetUp());
            shareDetails.setGoToBed(roommatePrefsDetails.getGoToBed());
            shareDetails.setFoodPreference(roommatePrefsDetails.getFoodPreference());
            shareDetails.setMySmokingHabits(roommatePrefsDetails.getMySmokingHabits());
            shareDetails.setWorkSchedule(roommatePrefsDetails.getWorkSchedule());
            shareDetails.setOccupation(roommatePrefsDetails.getOccupation());
        }
    }


    private void saveHaveRoom(ListingPostViewModel listingPostViewModel) {
        ShareDetails shareDetails = listingPostViewModel.getShareDetails() == null ?
                new ShareDetails() :
                listingPostViewModel.getShareDetails();
        saveShareDetails(shareDetails);
        listingPostViewModel.setShareDetails(shareDetails);
        setAdditionalMinStayId(listingPostViewModel);
    }


    private void saveShareDetails(ShareDetails shareDetails) {
        if (mHouseholdAgeLayout.getVisibility() == View.VISIBLE) {
            shareDetails.setHouseholdAgeMin(mHouseholdAgeRange.getLeftIndex() + DiscoveryConstants.MIN_AGE);
            shareDetails.setHouseholdAgeMax(mHouseholdAgeRange.getRightIndex() + DiscoveryConstants.MIN_AGE);
        }
        if (mPeopleInHouseholdLayout.getVisibility() == View.VISIBLE) {
            Integer peopleInHousehold = null;
            if (mPeopleInHouseholdId != -1) {
                peopleInHousehold = mPeopleInHouseholdId;
            }
            shareDetails.setPeopleInHousehold(peopleInHousehold);
        }
        if (mSexHouseholdLayour.getVisibility() == View.VISIBLE) {
            Integer householdSex = null;
            if (mSexHouseholdId != -1) {
                householdSex = mSexHouseholdId;
            }
            shareDetails.setHouseholdSex(householdSex);
        }
        if (mSmokingPreferenceLayout.getVisibility() == View.VISIBLE) {
            Long smokingPreference = null;
            if (mSmokingPreferenceId != -1) {
                smokingPreference = (long)mSmokingPreferenceId;
            }
            shareDetails.setSmokingPreference(smokingPreference);
        }
        if (mPetsOwnedAdapter != null) {
            if (mPetsOwnedAdapter.getSelected() != null && !mPetsOwnedAdapter.getSelected().isEmpty()) {
                ArrayList<Long> petsOwnedValues = new ArrayList<>();
                for (CatalogValueModel model : mPetsOwnedAdapter.getSelected()) {
                    petsOwnedValues.add(model.getValue());
                }
                shareDetails.setMyPets(petsOwnedValues);
            } else {
                shareDetails.setMyPets(null);
            }
        }
        if (mRoommatePrefsAgeLayout.getVisibility() == View.VISIBLE) {
            shareDetails.setAgePreferenceMin(mRoommatePrefsAgeRange.getLeftIndex() + DiscoveryConstants.MIN_AGE);
            shareDetails.setAgePreferenceMax(mRoommatePrefsAgeRange.getRightIndex() + DiscoveryConstants.MIN_AGE);
        }
        if (mPetsPreferredAdapter != null) {
            if (mPetsPreferredAdapter.getSelected() != null && !mPetsPreferredAdapter.getSelected().isEmpty()) {
                ArrayList<Long> petsPreferredValues = new ArrayList<>();
                for (CatalogValueModel model : mPetsPreferredAdapter.getSelected()) {
                    petsPreferredValues.add(model.getValue());
                }
                shareDetails.setPetsPreference(petsPreferredValues);
            } else {
                shareDetails.setPetsPreference(null);
            }
        }
        if (mResidenceBuildTypeLayout.getVisibility() == View.VISIBLE) {
            Long buildingType = null;
            if (mResidenceBuildTypeId != -1) {
                buildingType = (long)mResidenceBuildTypeId;
            }
            shareDetails.setBuildingType(buildingType);
        }

        if (mResidenceMoveInFeeLayout.getVisibility() == View.VISIBLE) {
            Integer moveInFee = null;
            try {
                moveInFee = Integer.parseInt(mResidenceMoveInFee.getText().toString());
            } catch (NumberFormatException nfe) {
                // let the number be null
            }
            shareDetails.setMoveinFee(moveInFee);
        }

        if (mResidenceUtilitiesCostLayout.getVisibility() == View.VISIBLE) {
            Integer utilitiesCost = null;
            try {
                utilitiesCost = Integer.parseInt(mResidenceUtilitiesCost.getText().toString());
            } catch (NumberFormatException nfe) {
                // let the number be null
            }
            shareDetails.setUtilitiesCost(utilitiesCost);
        }

        if (mResidenceParkingRentLayout.getVisibility() == View.VISIBLE) {
            Integer parkingRent = null;
            try {
                parkingRent = Integer.parseInt(mResidenceParkingRent.getText().toString());
            } catch (NumberFormatException nfe) {
                // let the number be null
            }
            shareDetails.setParkingRent(parkingRent);
        }

        if (mResidenceFurnishedLayout.getVisibility() == View.VISIBLE) {
            shareDetails.setIsFurnished(mResidenceFurnishedToggle.isChecked());
        }

        if (mStudentsOnlyLayout.getVisibility() == View.VISIBLE) {
            shareDetails.setStudentsOnly(mStudentsOnlyToggle.isChecked());
        }

        if (mRoomAmenitiesLayout.getVisibility() == View.VISIBLE && mRoomAmenitiesAdapter != null) {
            if (mRoomAmenitiesAdapter.getSelected() != null && !mRoomAmenitiesAdapter.getSelected().isEmpty()) {
                ArrayList<Long> roomAmenities = new ArrayList<>();
                for (CatalogValueModel model : mRoomAmenitiesAdapter.getSelected()) {
                    roomAmenities.add((long)model.getValue());
                }
                shareDetails.setRoomAmenities(roomAmenities);
            } else {
                shareDetails.setRoomAmenities(null);
            }
        }

        mapShareDetails(mLifestyleShareDetails, shareDetails);
    }


    public void buildApartmentAmenities(ListingPutViewModel listingPutViewModel) {
        if (mAmenitiesAdapter != null && listingPutViewModel != null) {
            if (mAmenitiesAdapter.getSelected() != null && !mAmenitiesAdapter.getSelected().isEmpty()) {
                ArrayList<Long> aptAmenities = new ArrayList<>();
                for (CatalogValueModel model : mAmenitiesAdapter.getSelected()) {
                    aptAmenities.add((long)model.getId());
                }
                listingPutViewModel.setApartmentAmenities(aptAmenities);
            } else {
                listingPutViewModel.setApartmentAmenities(null);
            }
        }
    }


    private void saveHaveApartment(ListingPostViewModel listingPostViewModel) {
        HaveApartment haveApartment = listingPostViewModel.getHaveApartment() == null ?
                new HaveApartment() :
                listingPostViewModel.getHaveApartment();
        listingPostViewModel.setHaveApartment(haveApartment);
    }


    private void saveNeedApartment(ListingPostViewModel listingPostViewModel) {
        NeedApartment needApartment = listingPostViewModel.getNeedApartment() == null ?
                new NeedApartment() :
                listingPostViewModel.getNeedApartment();
        listingPostViewModel.setNeedApartment(needApartment);
    }

    private void init() {
        inflate(getContext(), R.layout.view_add_listing_additional_fields, this);
        RoomsterApplication.getRoomsterComponent().inject(this);
        ButterKnife.bind(this);

        mDescriptionLimitTextView.setText(String.valueOf(DESCRIPTION_CHARS_LIMIT));
        mHeadlineLimitTextView.setText(String.valueOf(HEADLINE_CHARS_LIMIT));
    }
    
    
    private void correctionApartmentAmenitiesTitle(){
        apartmentAmenitiesTitle.setText(RoomsterApplication.context.getString(R.string.el_amenities_preferences));
    }


    private int getHeadlineRemainingCharactersCount(String text) {
        if (text != null) {
            return HEADLINE_CHARS_LIMIT - text.length();
        }

        return HEADLINE_CHARS_LIMIT;
    }


    private int getDescriptionRemainingCharactersCount(String text) {
        if (text != null) {
            return DESCRIPTION_CHARS_LIMIT - text.length();
        }

        return DESCRIPTION_CHARS_LIMIT;
    }


    private void initLayoutForType() {
        switch (mType) {
        case ListingType.FINDING_ROOM_TYPE:
            setUpLifestyleShareDetails();
            setUpMyPetsOwned();
            setUpRoommatePrefsAge();
            setUpRoommatePrefsPets();
            mRoommatePrefsLayout.setVisibility(View.VISIBLE);
            break;
        case ListingType.OFFERING_ROOM_TYPE:
            setUpLifestyleHouseholdAge();
            setUpLifestyleShareDetails();
            setUpMyPetsOwned();
            setUpRoommatePrefsAge();
            setUpRoommatePrefsPets();
            setUpResidenceMoveInFee();
            setUpResidenceUtilitiesCost();
            setUpResidenceParkingRent();
            setUpRoomAmenities();
            setUpResidenceFurnished();
            mHouseholdLayout.setVisibility(View.VISIBLE);
            mRoommatePrefsLayout.setVisibility(View.VISIBLE);
            mResidenceLayout.setVisibility(View.VISIBLE);
            break;
        case ListingType.OFFERING_ENTIRE_PLACE_TYPE:
            setUpApartmentAmenitiesGrid();
            break;
        case ListingType.FINDING_ENTIRE_PLACE_TYPE:
            correctionApartmentAmenitiesTitle();
            setUpApartmentAmenitiesGrid();
            setUpResidenceFurnished();
            break;
        }
    }


    private void setUpRoomAmenities() {
        List<CatalogValueModel> allAmenities = mCatalogsManager.getRoomAmenitiesList();
        if (allAmenities != null) {

            mRoomAmenitiesAdapter = new AmenitiesGridAdapter(getContext(), allAmenities, null, false);
            mRoomAmenitiesGrid.setAdapter(mRoomAmenitiesAdapter);
            measureLayout(allAmenities.size(), mRoomAmenitiesGrid);

            mRoomAmenitiesLayout.setVisibility(View.VISIBLE);
        }
    }


    private void measureLayout(int itemsSize, GridView layout) {
        ViewGroup.LayoutParams params = layout.getLayoutParams();
        int amenityHeight = getResources().getDimensionPixelOffset(R.dimen.amenities_layout_height);
        int margin = getResources().getDimensionPixelSize(R.dimen.offset_4);
        int height = amenityHeight * (1 + (itemsSize / AMENITIES_COLUMNS));
        params.height = height + margin;
        layout.setLayoutParams(params);
    }


    private void setUpResidenceUtilitiesCost() {
        mResidenceUtilitiesCostLayout.setVisibility(View.VISIBLE);
    }


    private void setUpResidenceParkingRent() {
        mResidenceParkingRentLayout.setVisibility(View.VISIBLE);
    }


    private void setUpResidenceMoveInFee() {
        mResidenceMoveInFeeLayout.setVisibility(View.VISIBLE);
    }


    private void setUpResidenceFurnished() {
        mResidenceFurnishedLayout.setVisibility(View.VISIBLE);
    }


    private void setUpRoommatePrefsPets() {
        if (getContext() != null) {
            mPetsPreferredAdapter = new PetsAdapter(getContext(), mCatalogsManager.getPetsPreferredList(), null);
            mRoommatePrefsPetsGrid.setAdapter(mPetsPreferredAdapter);
            mRoommatePrefsPetsTitle.setVisibility(View.VISIBLE);
            mRoommatePrefsPetsGrid.setVisibility(View.VISIBLE);
        }
    }


    private void setUpRoommatePrefsAge() {
        if (getContext() != null) {
            prefAgeRangeBarTickCount = DiscoveryConstants.MAX_AGE - DiscoveryConstants.MIN_AGE + 1;
            mRoommatePrefsAgeRange.setTickCount(prefAgeRangeBarTickCount);

            try {
                mRoommatePrefsAgeRange.setThumbIndices(0, DiscoveryConstants.MAX_AGE - DiscoveryConstants.MIN_AGE);
                updateRangeAgeValueText(mRoommatePrefsAgeValue, 0,
                        DiscoveryConstants.MAX_AGE - DiscoveryConstants.MIN_AGE);
            } catch (IllegalArgumentException e) {
                //Nothing to do
            }

            mRoommatePrefsAgeRange.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {

                @Override
                public void onIndexChangeListener(RangeBar rangeBar, int leftThumbIndex, int rightThumbIndex) {

                    //There is a known bug in the RangeBar  - sometimes the indexes go beyond the set range!
                    if (leftThumbIndex >= 0 && rightThumbIndex < prefAgeRangeBarTickCount) {
                        updateRangeAgeValueText(mRoommatePrefsAgeValue, leftThumbIndex, rightThumbIndex);
                    } else {
                        setStoredAge(mRoommatePrefsAgeRange, prefAgeRangeBarTickCount);
                    }
                }
            });
            setStoredAge(mRoommatePrefsAgeRange, prefAgeRangeBarTickCount);
            mRoommatePrefsAgeLayout.setVisibility(View.VISIBLE);
        }
    }


    private void setStoredAge(RangeBar mAgeRangeBar, int mAgeRangeBarTickCount) {
        int minAge = mDPrefManager.getMinAge();
        if (minAge < DiscoveryConstants.MIN_AGE) {
            mDPrefManager.storeMinAge(DiscoveryConstants.MIN_AGE);
            minAge = DiscoveryConstants.MIN_AGE;
        }
        int maxAge = mDPrefManager.getMaxAge();
        if (maxAge > DiscoveryConstants.MAX_AGE) {
            mDPrefManager.storeMinAge(DiscoveryConstants.MAX_AGE);
            maxAge = DiscoveryConstants.MAX_AGE;
        }
        int leftValue = minAge - DiscoveryConstants.MIN_AGE;
        int rightValue = maxAge - DiscoveryConstants.MIN_AGE;
        if (leftValue >= 0 || rightValue < mAgeRangeBarTickCount) {
            try {
                mAgeRangeBar.setThumbIndices(leftValue, rightValue);
            } catch (IllegalArgumentException e) {
                //Nothing to do
            }
        }
    }


    private void updateRangeAgeValueText(TextView valueView, int left, int right) {
        if (getContext() != null) {
            int leftValue = left + DiscoveryConstants.MIN_AGE;
            int rightValue = right + DiscoveryConstants.MIN_AGE;
            String minValue = String.valueOf(leftValue);
            String maxValue = String.valueOf(rightValue);
            String str = String.format(getContext().getString(R.string.megaphone_age_min_max), minValue, maxValue);
            valueView.setText(str);
        }
    }


    private void setUpLifestyleHouseholdAge() {
        houseHoldAgeRangeBarTickCount = DiscoveryConstants.MAX_AGE - DiscoveryConstants.MIN_AGE + 1;
        mHouseholdAgeRange.setTickCount(houseHoldAgeRangeBarTickCount);

        try {
            mHouseholdAgeRange.setThumbIndices(0, DiscoveryConstants.MAX_AGE - DiscoveryConstants.MIN_AGE);
            updateRangeAgeValueText(mHouseholdAgeValue, 0,
                    DiscoveryConstants.MAX_AGE - DiscoveryConstants.MIN_AGE);
        } catch (IllegalArgumentException e) {
            //Nothing to do
        }
        mHouseholdAgeRange.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {

            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int leftThumbIndex, int rightThumbIndex) {

                //There is a known bug in the RangeBar  - sometimes the indexes go beyond the set range!
                if (leftThumbIndex >= 0 && rightThumbIndex < houseHoldAgeRangeBarTickCount) {
                    updateRangeAgeValueText(mHouseholdAgeValue, leftThumbIndex, rightThumbIndex);
                } else {
                    setStoredAge(mHouseholdAgeRange, houseHoldAgeRangeBarTickCount);
                }
            }
        });
        setStoredAge(mHouseholdAgeRange, houseHoldAgeRangeBarTickCount);
        mHouseholdAgeLayout.setVisibility(View.VISIBLE);
    }


    private void setUpLifestyleShareDetails() {
        mLifestyleTitle.setVisibility(View.VISIBLE);
        mLifestyleShareDetails.setModel(null);
        mLifestyleShareDetails.setVisibility(View.VISIBLE);
    }


    private void setUpMyPetsOwned() {
        if (getContext() != null) {
            mPetsOwnedAdapter = new PetsAdapter(getContext(), mCatalogsManager.getPetsOwnedList(), null);
            mMyPetsGrid.setAdapter(mPetsOwnedAdapter);
            mMyPetsTitle.setVisibility(View.VISIBLE);
            mMyPetsGrid.setVisibility(View.VISIBLE);
        }
    }


    private void setUpApartmentAmenitiesGrid() {
        List<CatalogValueModel> allAmenities = mCatalogsManager.getApartmentAmenitiesList();
        if (allAmenities != null) {
            mAmenitiesAdapter = new AmenitiesGridAdapter(getContext(), allAmenities, null, true);

            mAmenityGridView.setAdapter(mAmenitiesAdapter);
            measureLayout(allAmenities.size());
            mAmenityGridLayout.setVisibility(View.VISIBLE);
        }
    }


    private void measureLayout(int itemsSize) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mAmenityGridLayout.getLayoutParams();
        int amenityHeight = getResources().getDimensionPixelOffset(R.dimen.amenities_layout_height);
        int margin = getResources().getDimensionPixelSize(R.dimen.offset_4);
        int height = amenityHeight * (1 + itemsSize / (ROW + 1));
        params.height = height + margin;
        mAmenityGridLayout.setLayoutParams(params);
    }


    public String getHeadline(){
        return mHeadlineEditText.getText().toString();
    }
    public String getDescription(){
        return mDescriptionEditText.getText().toString();
    }


    private void setAdditionalMinStayId(ListingPostViewModel listingPostViewModel) {
        Calendar calendar = listingPostViewModel.getCalendar();
        if (mAdditionalMinStayId != -1) {
            calendar.setMinimumStayId(mAdditionalMinStayId);
        }
        listingPostViewModel.setCalendar(calendar);
    }


    //------------------ Min stay, Building type, People in household, Household sex, Smoking preference, picker methods --------------------------//
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(CatalogRestServiceEvent.GetCatalogSuccess event) {
        if (mNeedToShowBuildTypeDialog) {
            mNeedToShowBuildTypeDialog = false;
            onResidenceBuildType();
        }
        if (mNeedToShowPeopleInHouseholdDialog) {
            mNeedToShowPeopleInHouseholdDialog = false;
            onPeopleInHousehold();
        }
        if (mNeedToShowSexHouseholdDialog) {
            mNeedToShowSexHouseholdDialog = false;
            onSexHousehold();
        }
        if (mNeedToSmokingPreferenceDialog) {
            mNeedToSmokingPreferenceDialog = false;
            onSmokingPreference();
        }
        if (mNeedToShowAdditionalMinStayDialog) {
            mNeedToShowAdditionalMinStayDialog = false;
            onAdditionalMinStay();
        }
    }


    private CatalogModel getCatalogById(List<CatalogModel> catalogModels, Integer categoryId) {
        if (categoryId != null && categoryId != -1) {
            for (CatalogModel catalogModel : catalogModels) {
                if (catalogModel.getId() == categoryId) {
                    return catalogModel;
                }
            }
        }
        return null;
    }


    private void showChosenValue(List<CatalogModel> catalog, Integer categoryId, TextView valueTv) {
        CatalogModel catalogModel = getCatalogById(catalog, categoryId);
        if (catalogModel != null) {
            valueTv.setText(catalogModel.getName());
        } else {
            valueTv.setText("");
        }
    }


    private void showSharedDetailsDialog(List<CatalogModel> catalogList, Integer currentId,
                                         CatalogListChooserDialog.OKButtonClickListener okButtonClick, String title) {
        List<Integer> currentIdList = new ArrayList<>();
        currentIdList.add(currentId);
        CatalogListChooserDialog sharedDetailsChooserDialog = new CatalogListChooserDialog(getContext(), catalogList,
                currentIdList, false, okButtonClick, title);
        sharedDetailsChooserDialog.show();
    }


    private void showBedroomMultiSelectionDialog (List<CatalogModel> catalogList, List<Integer> currentIdList,
                                                  CatalogListChooserDialog.OKButtonClickListener okButtonClick, String title) {
        CatalogListChooserDialog sharedDetailsChooserDialog = new CatalogListChooserDialog(getContext(), catalogList,
                currentIdList, true, okButtonClick, title);
        sharedDetailsChooserDialog.show();
    }


    @OnClick(R.id.al_smoking_preference_layout)
    public void onSmokingPreference() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getSmokingPreferencesList();
        if(catalogModels == null) {
            mNeedToSmokingPreferenceDialog = true;
            mCatalogRestService.loadCatalogs();
        } else {
            showSharedDetailsDialog(catalogModels, mSmokingPreferenceId, new CatalogListChooserDialog.OKButtonClickListener() {
                @Override
                public void onOkClick(List<Integer> categoryId) {
                    if (categoryId != null && !categoryId.isEmpty()) {
                        mSmokingPreferenceId = categoryId.get(0);
                        showChosenValue(catalogModels, categoryId.get(0), mSmokingPreferenceValueTv);
                    }
                }
            }, getContext().getString(R.string.smoking_preference));
        }
    }


    @OnClick(R.id.al_sex_household_layout)
    public void onSexHousehold() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getHouseholdSexList();
        if(catalogModels == null) {
            mNeedToShowSexHouseholdDialog = true;
            mCatalogRestService.loadCatalogs();
        } else {
            showSharedDetailsDialog(catalogModels, mSexHouseholdId, new CatalogListChooserDialog.OKButtonClickListener() {
                @Override
                public void onOkClick(List<Integer> categoryId) {
                    if (categoryId != null && !categoryId.isEmpty()) {
                        mSexHouseholdId = categoryId.get(0);
                        showChosenValue(catalogModels, categoryId.get(0), mSexHouseholdValueTv);
                    }
                }
            }, getContext().getString(R.string.discovery_household_sex));
        }
    }


    @OnClick(R.id.al_residence_building_type_layout)
    public void onResidenceBuildType() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getBuildingTypesList();
        if(catalogModels == null) {
            mNeedToShowBuildTypeDialog = true;
            mCatalogRestService.loadCatalogs();
        } else {
            showSharedDetailsDialog(catalogModels, mResidenceBuildTypeId, new CatalogListChooserDialog.OKButtonClickListener() {
                @Override
                public void onOkClick(List<Integer> categoryId) {
                    if (categoryId != null && !categoryId.isEmpty()) {
                        mResidenceBuildTypeId = categoryId.get(0);
                        showChosenValue(catalogModels, categoryId.get(0), mResidenceBuildTypeValue);
                    }
                }
            }, getContext().getString(R.string.el_residence_building_type_title));
        }
    }


    @OnClick(R.id.al_people_in_household_layout)
    public void onPeopleInHousehold() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getPeopleInHouseholdList();
        if(catalogModels == null) {
            mNeedToShowPeopleInHouseholdDialog = true;
            mCatalogRestService.loadCatalogs();
        } else {
            showSharedDetailsDialog(catalogModels, mPeopleInHouseholdId, new CatalogListChooserDialog.OKButtonClickListener() {
                @Override
                public void onOkClick(List<Integer> categoryId) {
                    if (categoryId != null && !categoryId.isEmpty()) {
                        mPeopleInHouseholdId = categoryId.get(0);
                        showChosenValue(catalogModels, categoryId.get(0), mPeopleInHouseholdValueTv);
                    }
                }
            }, getContext().getString(R.string.el_people_in_household_title));
        }
    }


    @OnClick(R.id.al_additional_minimum_stay_layout)
    public void onAdditionalMinStay() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getMinimumStayList();
        if (catalogModels == null) {
            mNeedToShowAdditionalMinStayDialog = true;
            mCatalogRestService.loadCatalogs();
        } else {
            showSharedDetailsDialog(catalogModels, mAdditionalMinStayId, new CatalogListChooserDialog.OKButtonClickListener() {
                @Override
                public void onOkClick(List<Integer> categoryIds) {
                    if (categoryIds != null && !categoryIds.isEmpty()) {
                        mAdditionalMinStayId = categoryIds.get(0);
                        showChosenValue(catalogModels, categoryIds.get(0), mAdditionalMinStayValueTv);
                    }
                }
            }, getContext().getString(R.string.al_minimum_stay));
        }
    }
}
