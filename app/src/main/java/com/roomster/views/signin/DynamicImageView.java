package com.roomster.views.signin;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by andreybofanov on 20.07.16.
 */
public class DynamicImageView extends ImageView {

    public DynamicImageView(Context context) {
        super(context);
    }

    public DynamicImageView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        final Drawable d = this.getDrawable();

        if (d != null) {
            // ceil not round - avoid thin vertical gaps along the left/right edges
            final int height = MeasureSpec.getSize(heightMeasureSpec);
            final int width = (int) Math.ceil(height * (float) d.getIntrinsicWidth()/d.getIntrinsicHeight() );
            this.setMeasuredDimension(width, height);
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}
