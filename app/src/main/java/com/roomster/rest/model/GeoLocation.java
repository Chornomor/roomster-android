package com.roomster.rest.model;


import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class GeoLocation implements Serializable {

    /**
     **/
    @SerializedName("address_line")
    private String addressLine = null;

    /**
     **/
    @SerializedName("city")
    private String city = null;

    /**
     **/
    @SerializedName("city_lat")
    private Float cityLat = null;

    /**
     **/
    @SerializedName("city_lng")
    private Float cityLng = null;

    /**
     **/
    @SerializedName("country")
    private String country = null;

    /**
     **/
    @SerializedName("full_address")
    private String fullAddress = null;

    /**
     **/
    @SerializedName("google_formatted_address")
    private String googleFormattedAddress = null;

    /**
     **/
    @SerializedName("lat")
    private Float lat = null;

    /**
     **/
    @SerializedName("lng")
    private Float lng = null;

    /**
     **/
    @SerializedName("ne_lat")
    private Float neLat = null;

    /**
     **/
    @SerializedName("ne_lng")
    private Float neLng = null;

    /**
     **/
    @SerializedName("region")
    private String region = null;

    /**
     **/
    @SerializedName("sublocality")
    private String sublocality = null;

    /**
     **/
    @SerializedName("sw_lat")
    private Float swLat = null;

    /**
     **/
    @SerializedName("sw_lng")
    private Float swLng = null;

    /**
     **/
    @SerializedName("postal_code")
    private String postalCode = null;


    public String getAddressLine() {
        return addressLine;
    }


    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }


    public String getCity() {
        return city;
    }


    public void setCity(String city) {
        this.city = city;
    }


    public Float getCityLat() {
        return cityLat;
    }


    public void setCityLat(Float cityLat) {
        this.cityLat = cityLat;
    }


    public Float getCityLng() {
        return cityLng;
    }


    public void setCityLng(Float cityLng) {
        this.cityLng = cityLng;
    }


    public String getCountry() {
        return country;
    }


    public void setCountry(String country) {
        this.country = country;
    }


    public String getFullAddress() {
        return fullAddress;
    }


    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }


    public String getGoogleFormattedAddress() {
        return googleFormattedAddress;
    }


    public void setGoogleFormattedAddress(String googleFormattedAddress) {
        this.googleFormattedAddress = googleFormattedAddress;
    }


    public Float getLat() {
        return lat;
    }


    public void setLat(Float lat) {
        this.lat = lat;
    }


    public Float getLng() {
        return lng;
    }


    public void setLng(Float lng) {
        this.lng = lng;
    }


    public Float getNeLat() {
        return neLat;
    }


    public void setNeLat(Float neLat) {
        this.neLat = neLat;
    }


    public Float getNeLng() {
        return neLng;
    }


    public void setNeLng(Float neLng) {
        this.neLng = neLng;
    }


    public String getRegion() {
        return region;
    }


    public void setRegion(String region) {
        this.region = region;
    }


    public String getSublocality() {
        return sublocality;
    }


    public void setSublocality(String sublocality) {
        this.sublocality = sublocality;
    }


    public Float getSwLat() {
        return swLat;
    }


    public void setSwLat(Float swLat) {
        this.swLat = swLat;
    }


    public Float getSwLng() {
        return swLng;
    }


    public void setSwLng(Float swLng) {
        this.swLng = swLng;
    }


    public String getPostalCode() {
        return postalCode;
    }


    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }


    public LatLngBounds getBounds() {
        LatLng sw = new LatLng(swLat, swLng);
        LatLng ne = new LatLng(neLat, neLng);
        return new LatLngBounds(sw, ne);
    }


    public LatLng getLatLng() {
        return new LatLng(lat, lng);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class GeoLocation {\n");

        sb.append("  addressLine: ").append(addressLine).append("\n");
        sb.append("  city: ").append(city).append("\n");
        sb.append("  cityLat: ").append(cityLat).append("\n");
        sb.append("  cityLng: ").append(cityLng).append("\n");
        sb.append("  country: ").append(country).append("\n");
        sb.append("  fullAddress: ").append(fullAddress).append("\n");
        sb.append("  googleFormattedAddress: ").append(googleFormattedAddress).append("\n");
        sb.append("  lat: ").append(lat).append("\n");
        sb.append("  lng: ").append(lng).append("\n");
        sb.append("  neLat: ").append(neLat).append("\n");
        sb.append("  neLng: ").append(neLng).append("\n");
        sb.append("  region: ").append(region).append("\n");
        sb.append("  sublocality: ").append(sublocality).append("\n");
        sb.append("  swLat: ").append(swLat).append("\n");
        sb.append("  swLng: ").append(swLng).append("\n");
        sb.append("  postalCode: ").append(postalCode).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
