package com.roomster.event.RestServiceEvents;


import com.roomster.rest.model.InAppProductViewModel;

import java.util.List;


/**
 * Created by Bogoi.Bogdanov on 2/29/2016.
 */
public class InAppProductsRestServiceEvent {

    public static class GetInAppProductsSuccess extends InAppProductsRestServiceEvent {

        public List<InAppProductViewModel> inAppProductsList;


        public GetInAppProductsSuccess(List<InAppProductViewModel> inAppProductsList) {
            this.inAppProductsList = inAppProductsList;
        }
    }


    public static class GetInAppProductsFailure extends InAppProductsRestServiceEvent {

    }
}
