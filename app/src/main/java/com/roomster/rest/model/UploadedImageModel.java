package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


public class UploadedImageModel {

    /**
     **/
    @SerializedName("id")
    private Long id = null;

    /**
     **/
    @SerializedName("name")
    private String name = null;

    /**
     **/
    @SerializedName("is_main")
    private Boolean isMain = null;

    /**
     **/
    @SerializedName("url")
    private String url = null;

    /**
     **/
    @SerializedName("url_medium")
    private String urlMedium = null;

    /**
     **/
    @SerializedName("url_thumbnail")
    private String urlThumbnail = null;

    /**
     **/
    @SerializedName("name_original")
    private String nameOriginal = null;

    /**
     **/
    @SerializedName("type")
    private String type = null;

    /**
     **/
    @SerializedName("size")
    private Integer size = null;


    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public Boolean getIsMain() {
        return isMain;
    }


    public void setIsMain(Boolean isMain) {
        this.isMain = isMain;
    }


    public String getUrl() {
        return url;
    }


    public void setUrl(String url) {
        this.url = url;
    }


    public String getUrlMedium() {
        return urlMedium;
    }


    public void setUrlMedium(String urlMedium) {
        this.urlMedium = urlMedium;
    }


    public String getUrlThumbnail() {
        return urlThumbnail;
    }


    public void setUrlThumbnail(String urlThumbnail) {
        this.urlThumbnail = urlThumbnail;
    }


    public String getNameOriginal() {
        return nameOriginal;
    }


    public void setNameOriginal(String nameOriginal) {
        this.nameOriginal = nameOriginal;
    }


    public String getType() {
        return type;
    }


    public void setType(String type) {
        this.type = type;
    }


    public Integer getSize() {
        return size;
    }


    public void setSize(Integer size) {
        this.size = size;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class UploadedImageModel {\n");

        sb.append("  id: ").append(id).append("\n");
        sb.append("  name: ").append(name).append("\n");
        sb.append("  isMain: ").append(isMain).append("\n");
        sb.append("  url: ").append(url).append("\n");
        sb.append("  urlMedium: ").append(urlMedium).append("\n");
        sb.append("  urlThumbnail: ").append(urlThumbnail).append("\n");
        sb.append("  nameOriginal: ").append(nameOriginal).append("\n");
        sb.append("  type: ").append(type).append("\n");
        sb.append("  size: ").append(size).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
