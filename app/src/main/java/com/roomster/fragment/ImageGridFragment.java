package com.roomster.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.roomster.R;
import com.roomster.adapter.PhotoGridAdapter;
import com.roomster.rest.model.Photo;
import com.roomster.views.ImagePager;
import com.roomster.views.undo.ItemExtractor;
import com.roomster.views.undo.UndoScnackbar;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnItemClick;


/**
 * Created by michaelkatkov on 1/5/16.
 */
public class ImageGridFragment extends Fragment {

    private static final String PHOTOS = "photos";

    private PhotoGridAdapter              mPhotoGridAdapter;
    private ArrayList<Photo>              mPhotos;
    private ImagePager.ImageClickListener mImageClickListener;

    @Bind(R.id.images_grid)
    GridView mImagesGrid;


    public static ImageGridFragment newInstance(ArrayList<Photo> photos) {
        ImageGridFragment fragment = new ImageGridFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(PHOTOS, photos);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPhotos = getArguments().getParcelableArrayList(PHOTOS);
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_grid, container, false);
        ButterKnife.bind(this, view);
        mPhotoGridAdapter = new PhotoGridAdapter(mPhotos);
        mImagesGrid.setAdapter(mPhotoGridAdapter);
        return view;
    }


    @OnItemClick(R.id.images_grid)
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Photo photo = (Photo) parent.getItemAtPosition(position);
        if (mImageClickListener != null) {
            processClick(view,photo);
        }
    }

    void processClick(View view,final Photo photo){
        if(photo.getPath().equals(PhotoGridAdapter.ADD_NEW_IMAGE_PLACEHOLDER)) {
            view.findViewById(R.id.user_image_progress).setVisibility(View.VISIBLE);
            mImageClickListener.onImageClick(photo);
            return;
        }
        View parent = getActivity().findViewById(R.id.coordinator);
        if(parent == null || !(parent instanceof CoordinatorLayout)) {
            mImageClickListener.onImageClick(photo);
            Log.e("PHOTO_GRID","parent does not support snackbar. No coordinator layout found on id coordinator");
            return;
        }
        CoordinatorLayout coordinator = (CoordinatorLayout) parent;
        final ItemExtractor extract = mPhotoGridAdapter.extract(photo);
        new UndoScnackbar(coordinator, "Photo deleted", new UndoScnackbar.SnackBarEvent() {
            @Override
            public void onUndo() {
                mPhotoGridAdapter.restore(extract);
            }

            @Override
            public void onDismiss() {
                mImageClickListener.onImageClick(photo);
            }
        }).make();

    }


    public void setImageClickListener(ImagePager.ImageClickListener imageClickListener) {
        mImageClickListener = imageClickListener;
    }
}