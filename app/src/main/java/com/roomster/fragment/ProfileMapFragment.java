package com.roomster.fragment;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.utils.PermissionsAware;
import com.roomster.utils.PermissionsHelper;

import javax.inject.Inject;


/**
 * Created by "Michael Katkov" on 10/13/2015.
 */
public class ProfileMapFragment extends BaseMapFragment implements GoogleApiClient.ConnectionCallbacks,
  GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG        = "ProfileMapFragment";
    private static final int    ZOOM_LEVEL = 15;

    private GoogleApiClient mGoogleApiClient;

    private Location mLastLocation;

    @Inject
    Context mContext;


    public static ProfileMapFragment newInstance() {
        return new ProfileMapFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        buildGoogleApiClient();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (mGoogleMap != null) {
            mGoogleMap.getUiSettings().setScrollGesturesEnabled(false);
        }
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        Activity activity = getActivity();
        PermissionsHelper permissionsHelper;
        if (activity instanceof PermissionsAware) {
            permissionsHelper = ((PermissionsAware) activity).getPermissionsHelper();
        } else {
            permissionsHelper = new PermissionsHelper(getActivity());
        }
        permissionsHelper.actionOnLocationPermission(new PermissionsHelper.OnLocationPermissionActionListener() {

            @Override
            public void onLocationPermissionGranted() {
                buildGoogleApiClient();
                mGoogleApiClient.connect();
            }


            @Override
            public void onLocationPermissionDenied() {
                buildGoogleApiClient();
                mGoogleApiClient.connect();
            }
        });
    }


    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    @Override
    public void onConnected(Bundle connectionHint) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null && mGoogleMap != null) {
            mGoogleMap.addMarker(getMarker(mLastLocation, R.drawable.map_custom_pin));
            mGoogleMap.moveCamera(CameraUpdateFactory
              .newLatLngZoom(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), ZOOM_LEVEL));
        } else {
            Toast.makeText(mContext, R.string.no_location_detected, Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Toast.makeText(mContext, R.string.no_location_detected, Toast.LENGTH_LONG).show();
    }


    @Override
    public void onConnectionSuspended(int cause) {
        mGoogleApiClient.connect();
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext).addConnectionCallbacks(this).addOnConnectionFailedListener(this)
          .addApi(LocationServices.API).build();
    }


    private MarkerOptions getMarker(Location location, int drawableId) {
        return new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude()))
          .icon(BitmapDescriptorFactory.fromResource(drawableId));
    }
}
