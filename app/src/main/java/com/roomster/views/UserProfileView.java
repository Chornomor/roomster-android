package com.roomster.views;


import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.roomster.R;
import com.roomster.adapter.MyProfilePagerAdapter;
import com.roomster.model.FacebookInterestsModel;
import com.roomster.model.FacebookUserModel;
import com.roomster.rest.model.FacebookPage;
import com.roomster.rest.model.UserGetViewModel;
import com.roomster.utils.ScreenUtil;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class UserProfileView extends BottomSheetLayout implements ViewPagerWithIndicators.BottomViewExpandListener {

    private static final int USER_LIKES_SHOWN = 15;

    private SlidingUpPanelLayout.PanelSlideListener mAdditionalSliderListener;

    @Bind(R.id.profile_pager)
    ViewPagerWithIndicators mPageIndicator;

    @Bind(R.id.profile_bottom_sliding_view)
    BottomProfileSlidingView mBottomProfileView;

    @Bind(R.id.profile_sliding_panel_layout)
    FrameLayout mSlidingPanelLayout;

    @Bind(R.id.profile_sliding_layout)
    SlidingUpPanelLayout mSlidingLayout;

    boolean isMeProfile = false;


    public UserProfileView(Context context) {
        super(context);
        initView();
    }


    public UserProfileView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    @Override
    public void onShouldExpand() {
        mSlidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
    }


    @Override
    public void onShouldCollapse() {
        mSlidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
    }


    @OnClick({R.id.clickable_view, R.id.bs_profile_expanded_header})
    public void onBottomInfoContentClick() {
        switch (mSlidingLayout.getPanelState()) {
            case EXPANDED:
                mSlidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                break;
            case COLLAPSED:
                mSlidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                break;
            default:
                break;
        }
    }

    public void setUserGetViewModel(UserGetViewModel userGetViewModel) {
        MyProfilePagerAdapter mProfilePagerAdapter = new MyProfilePagerAdapter(
          ((AppCompatActivity) getContext()).getSupportFragmentManager(), userGetViewModel);
        mPageIndicator.setAdapter(mProfilePagerAdapter, mProfilePagerAdapter.getCount() + 1);
        mPageIndicator.setOnBottomViewExpandListener(this);
        mBottomProfileView.setUserGetViewModel(userGetViewModel);
        if(isMeProfile){
            mBottomProfileView.setupIsMeDiff();
            mBottomProfileView.setVisibility(VISIBLE);
            mBottomProfileView.setBottomViewVisibility(View.VISIBLE);
            mBottomProfileView.post(new Runnable() {
                @Override
                public void run() {
                    final int toolbarHeight = (int) getContext().getResources().getDimension(R.dimen.detailed_view_toolbar_height);
                    setPad(toolbarHeight);
                }
            });
        }
    }

    public void setMeProfile(boolean meProfile) {
        isMeProfile = meProfile;
        final int toolbarHeight = (int) getContext().getResources().getDimension(R.dimen.detailed_view_toolbar_height);
        setPad(toolbarHeight);
    }

    public void setFacebookUserModel(FacebookUserModel facebookUserModel) {
        mBottomProfileView.setFacebookUserModel(facebookUserModel);
    }


    public void setFacebookInterestsModel(FacebookInterestsModel facebookInterestsModel) {
        mBottomProfileView.setFacebookInterestsModel(facebookInterestsModel);
    }


    public void setUserLikes(List<FacebookPage> userLikes) {
        if (userLikes.size() <= USER_LIKES_SHOWN) {
            mBottomProfileView.setInterests(userLikes);
        } else {
            mBottomProfileView.setInterests(userLikes.subList(0, USER_LIKES_SHOWN));
        }
    }


    public void setMutualFriends(int mutualFriends) {
        mBottomProfileView.setMutualFriends(mutualFriends);
    }

    public void hideMutualFriends (){
        mBottomProfileView.hideMutualFriends();
    }

    public void setPanelSlidingListener(SlidingUpPanelLayout.PanelSlideListener panelSlidingListener) {
        mAdditionalSliderListener = panelSlidingListener;
    }


    public void setOnShowListingsClickListener(OnClickListener onClickListener) {
        mBottomProfileView.setOnShowListingsClickListener(onClickListener);
    }
    private void setPad(int toolbarHeight){
        View panel = mSlidingPanelLayout;
        int layoutBottom = mSlidingLayout.getBottom();
        int panelBottom = panel.getBottom();
        panel.setPadding(panel.getPaddingLeft(), panel.getPaddingTop(), panel.getPaddingRight(),
                (int) (panelBottom - layoutBottom) + (isMeProfile ? toolbarHeight/6 :toolbarHeight));
    }
    private void initView() {
        inflate(getContext(), R.layout.view_user_profile, this);
        ButterKnife.bind(this);
        setSlidingPanelLayoutHeight();
        final int toolbarHeight = (int) getContext().getResources().getDimension(R.dimen.detailed_view_toolbar_height);
        setPad(toolbarHeight);
        mBottomProfileView.setAboutExpanded(false);
        mSlidingLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {

            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                mBottomProfileView.setBottomViewVisibility(VISIBLE);
                if(isMeProfile){
                    mBottomProfileView.setAboutExpanded(true);
                }
                setPad(toolbarHeight);
                if (mAdditionalSliderListener != null) {
                    mAdditionalSliderListener.onPanelSlide(panel, slideOffset);
                }
            }


            @Override
            public void onPanelCollapsed(View panel) {
                mPageIndicator.setCurrentPositionSelected();
                if(!isMeProfile) {
                    mBottomProfileView.setBottomViewVisibility(GONE);

                } else {
                    mBottomProfileView.setAboutExpanded(false);
                }
                if (mAdditionalSliderListener != null) {
                    mAdditionalSliderListener.onPanelCollapsed(panel);
                }
            }


            @Override
            public void onPanelExpanded(View panel) {
                mPageIndicator.setInfoPositionSelected();

                if (mAdditionalSliderListener != null) {
                    mAdditionalSliderListener.onPanelExpanded(panel);
                }
            }


            @Override
            public void onPanelAnchored(View panel) {
                if (mAdditionalSliderListener != null) {
                    mAdditionalSliderListener.onPanelAnchored(panel);
                }
            }


            @Override
            public void onPanelHidden(View panel) {
                if (mAdditionalSliderListener != null) {
                    mAdditionalSliderListener.onPanelHidden(panel);
                }
            }
        });
    }

    private void setSlidingPanelLayoutHeight() {
        ViewGroup.LayoutParams params = mSlidingPanelLayout.getLayoutParams();
        params.height = ScreenUtil.getHeight(getContext()) - getResources()
          .getDimensionPixelOffset(R.dimen.profile_sliding_panel_delta);
        mSlidingPanelLayout.setLayoutParams(params);
    }
}