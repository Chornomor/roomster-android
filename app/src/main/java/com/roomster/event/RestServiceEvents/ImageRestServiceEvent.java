package com.roomster.event.RestServiceEvents;


import com.roomster.R;
import com.roomster.event.Event;
import com.roomster.rest.model.UploadedImageModel;

import java.util.List;


/**
 * Created by michaelkatkov on 12/29/15.
 */
public class ImageRestServiceEvent extends Event {

    public static class ImageAddSuccess extends ImageRestServiceEvent {

        public List<UploadedImageModel> mUploadedImageModelsList;


        public ImageAddSuccess(List<UploadedImageModel> uploadedImagesList) {
            mUploadedImageModelsList = uploadedImagesList;
        }


        public List<UploadedImageModel> getUploadedImageModelsList() {
            return mUploadedImageModelsList;
        }
    }


    public static class ImageAddFailed extends ImageRestServiceEvent {
        public int errorMessageId = R.string.failed_to_add_image;
        public ImageAddFailed(){
        }
        public ImageAddFailed(int errorMessageId) {
            this.errorMessageId = errorMessageId;
        }
    }


    public static class ImageDeleteSuccess extends ImageRestServiceEvent {

        private long mPhotoId;


        public ImageDeleteSuccess(long photoId) {
            mPhotoId = photoId;
        }


        public long getPhotoId() {
            return mPhotoId;
        }
    }


    public static class ImageDeleteFailed extends ImageRestServiceEvent {

    }
}
