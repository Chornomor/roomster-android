package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


public class BookmarkCounter {

    /**
     **/
    @SerializedName("bookmark_type")
    private String bookmarkType = null;

    /**
     **/
    @SerializedName("service_type")
    private String serviceType = null;

    /**
     **/
    @SerializedName("count")
    private Integer count = null;


    public String getBookmarkType() {
        return bookmarkType;
    }


    public void setBookmarkType(String bookmarkType) {
        this.bookmarkType = bookmarkType;
    }


    public String getServiceType() {
        return serviceType;
    }


    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }


    public Integer getCount() {
        return count;
    }


    public void setCount(Integer count) {
        this.count = count;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class BookmarkCounter {\n");

        sb.append("  bookmarkType: ").append(bookmarkType).append("\n");
        sb.append("  serviceType: ").append(serviceType).append("\n");
        sb.append("  count: ").append(count).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
