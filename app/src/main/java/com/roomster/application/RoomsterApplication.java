package com.roomster.application;


import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;

import com.roomster.activity.LoadingActivity;
import com.roomster.constants.IntentExtras;
import com.roomster.di.component.DaggerRoomsterComponent;
import com.roomster.di.component.RoomsterComponent;
import com.roomster.di.module.ApplicationModule;
import com.roomster.event.application.ApplicationEvent;
import com.roomster.manager.SettingsManager;
import com.roomster.utils.LocaleUtil;
import com.roomster.utils.LogUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import io.branch.referral.Branch;


/**
 * A class representing the global Application object. This object is always alive when the application is running and
 * provides the ApplicationContext.
 *
 * @author Bogoi.Bogdanov on 10/6/2015.
 */
public class RoomsterApplication extends Application {

    public static Context context;

    private static RoomsterComponent sRoomsterComponent;

    private int mLoadingRequestsCount;

    SettingsManager mSettingsManager;


    public static RoomsterComponent getRoomsterComponent() {
        return sRoomsterComponent;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        initComponents();
        EventBus.getDefault().register(this);
        context = getApplicationContext();
        Branch.enableCookieBasedMatching("bnc.lt");
        Branch.getAutoInstance(this);
        mSettingsManager = new SettingsManager();
        updateResources();
    }

    private void updateResources() {
        if (mSettingsManager.getLocale() != null && !mSettingsManager.getLocale().isEmpty()) {
            LocaleUtil.updateResources(this, mSettingsManager.getLocale());
        } else {
            if (mSettingsManager.getAppLocale() != null && !mSettingsManager.getAppLocale().isEmpty()) {
                LocaleUtil.updateResources(this, mSettingsManager.getAppLocale());
            } else {
                LocaleUtil.updateResources(this, "");
            }
        }
    }


    private void initComponents() {
        ApplicationModule applicationModule = new ApplicationModule(this);
        sRoomsterComponent = DaggerRoomsterComponent.builder().applicationModule(applicationModule).build();
        context = getApplicationContext();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ApplicationEvent.ShowLoading event) {
        if (mLoadingRequestsCount == 0) {
            mLoadingRequestsCount++;
            Intent intent = new Intent(this, LoadingActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            mLoadingRequestsCount++;
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ApplicationEvent.HideLoading event) {
        mLoadingRequestsCount--;
        if (mLoadingRequestsCount <= 0) {
            mLoadingRequestsCount = 0;
            EventBus.getDefault().postSticky(new ApplicationEvent.HideLoadingSticky());
//            sendBroadcast(new Intent(IntentExtras.LOADING_INTENT_FILTER));
        }
    }
}