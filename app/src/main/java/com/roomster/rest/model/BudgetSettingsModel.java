package com.roomster.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eugenetroyanskii on 07.10.16.
 */
public class BudgetSettingsModel {

    @SerializedName("currency")
    private String currency = null;

    @SerializedName("min")
    private Integer min = null;

    @SerializedName("max")
    private Integer max = null;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    @Override
    public String toString() {
        return "BudgetSettingsModel{" +
                "currency='" + currency + '\'' +
                ", min=" + min +
                ", max=" + max +
                '}';
    }
}
