package com.roomster.views.toolbar;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.roomster.R;
import com.roomster.rest.model.ResultItemModel;
import com.roomster.rest.model.UserGetViewModel;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by andreybofanov on 08.09.16.
 */
public class RRToolbar extends FrameLayout {

    @Bind(R.id.contact_action_message)
    ToolbarButton btnMessage;

    @Bind(R.id.contact_action_call)
    ToolbarButton btnCall;

    @Bind(R.id.contact_action_sms)
    ToolbarButton btnSms;

    @Bind(R.id.contact_action_social)
    ToolbarButton btnSocial;

    @Bind(R.id.contact_action_other)
    ToolbarButton btnOther;
    ViewGroup layout;
    OnActionListener listener;

    PopupMenu menuOther;

    public RRToolbar(Context context) {
        super(context);
        initViews();
    }

    public RRToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public RRToolbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    public void initViews(){
        layout = (ViewGroup) LayoutInflater.from(getContext()).inflate(R.layout.toolbar_layout,this);
        ButterKnife.bind(this,layout);
    }

    public void setListener(OnActionListener listener) {
        this.listener = listener;
    }

    public void setUpFromListing(ResultItemModel listing){
        if(listing == null){
            return;
        }
        UserGetViewModel user = listing.getUser();
        String phone = user.getPhone();

        boolean hasPhoneNumber = phone != null && !phone.isEmpty();
        boolean hasSocialConnections =
                user.getSocialConnections() != null && !user.getSocialConnections().isEmpty();
        setupButtons(hasPhoneNumber,hasSocialConnections);
        if (listing.getListing() != null) {
            updateListingActionMenu(getOtherMenu(),listing.getListing().getIsBookmarked());
        }
    }

    public void setupFromMeUser(UserGetViewModel user){
        boolean hasSocialConnections = user.getSocialConnections() != null && !user.getSocialConnections().isEmpty()
                && user.getIsVisibleSocialConnections();
        ViewGroup vg = (ViewGroup) layout.getChildAt(0);
        for(int i = 0; i < vg.getChildCount(); i++){
            View v = vg.getChildAt(i);
            if(R.id.contact_action_social != v.getId() || !hasSocialConnections){
                v.setVisibility(GONE);
            }
        }
    }

    public void setupFromUser(UserGetViewModel user){
        if(user == null){
            return;
        }
        String phone = user.getPhone();
        boolean hasPhoneNumber = phone != null && !phone.isEmpty();
        boolean hasSocialConnections = user.getSocialConnections() != null && !user.getSocialConnections().isEmpty();
        setupButtons(hasPhoneNumber,hasSocialConnections);
        updateUserActionManu(getOtherMenu());
    }

    private void setupButtons(boolean hasPhoneNumber,boolean hasSocialConnections){
        if (btnCall != null) {
            btnCall.setActive(hasPhoneNumber);
        }

        if (btnSms != null) {
            btnSms.setActive(hasPhoneNumber);
        }

        if (btnSocial != null) {
            btnSocial.setActive(hasSocialConnections);
        }

    }

    private Menu getOtherMenu(){
        if(menuOther == null) {
            PopupMenu popup = new PopupMenu(getContext(), btnOther);
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.other, popup.getMenu());
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if (listener != null) {
                        listener.onRRToolbarActionClick(item.getItemId());
                    }
                    return true;
                }
            });
            menuOther = popup;
        }
        return menuOther.getMenu();
    }

    public void updateBookmarkedListing(boolean isBookmarked){
        updateListingActionMenu(getOtherMenu(),isBookmarked);
    }

    private void updateListingActionMenu(Menu menu,boolean isListingBookmarked) {
            MenuItem bookmarkMenuItem = menu.findItem(R.id.contact_action_bookmark);
            if (isListingBookmarked) {
                bookmarkMenuItem.setTitle(getResources().getString(R.string.detailed_view_toolbar_action_unbookmark));
            } else {
                bookmarkMenuItem.setTitle(getResources().getString(R.string.detailed_view_toolbar_action_bookmark));
            }

    }
    private void updateUserActionManu(Menu menu){
        MenuItem actionBookmark = menu.findItem(R.id.contact_action_bookmark);
        if (actionBookmark != null) {
            actionBookmark.setVisible(false);
        }

        MenuItem actionShare = menu.findItem(R.id.contact_action_share);
        if (actionShare != null) {
            actionShare.setVisible(false);
        }
    }

    @OnClick({R.id.contact_action_message,  R.id.contact_action_call,
            R.id.contact_action_sms,  R.id.contact_action_social,
            R.id.contact_action_other})
    public void onItemClick(View v){
        if(!(v instanceof ToolbarButton)) {
            return;
        }
        ToolbarButton btn = (ToolbarButton) v;
        if(!btn.isActive){
            return;
        }
        if(btn.getId() == R.id.contact_action_other){
            showOtherAnchorView();
        } else {
            actionClicked(btn.getId());
        }
    }

    private void showOtherAnchorView(){
       if(menuOther != null){
           menuOther.show();
       }
    }

    private void actionClicked(int id){
        if(listener == null){
            return;
        }
        listener.onRRToolbarActionClick(id);
    }


    public interface OnActionListener {
        void onRRToolbarActionClick(int id);
    }

    public void setVisible(boolean visible){
        if(layout == null){
            return;
        }
        int visibility = visible ? VISIBLE : INVISIBLE;
        for(int i = 0; i < layout.getChildCount(); i++){
            layout.getChildAt(i).setVisibility(visibility);
        }
    }
}
