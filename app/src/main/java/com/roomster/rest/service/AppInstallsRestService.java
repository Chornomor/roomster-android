package com.roomster.rest.service;


import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.TokenManager;
import com.roomster.rest.api.AppInstallsApi;
import com.roomster.rest.model.AppInstallsViewModel;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AppInstallsRestService {

    private static final String OS_TYPE_ANDROID = "Android";

    @Inject
    Context mContext;

    @Inject
    EventBus mEventBus;

    @Inject
    TokenManager mTokenManager;

    @Inject
    AppInstallsApi mAppInstallsApi;

    @Inject
    NetworkManager mNetworkManager;


    public AppInstallsRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void sendRegistration(final String instanceId, final RestServiceListener listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callSaveRegistration(result, instanceId, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                if (throwable != null) {
                    throwable.printStackTrace();
                }
                listener.onFailure(code, throwable, msg);
            }
        });
    }


    public void updateRegistrationToken(final String instanceId, final boolean enableNotification,
                                        final RestServiceListener listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callUpdateRegistration(result, instanceId, enableNotification, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                if (throwable != null) {
                    throwable.printStackTrace();
                }
                listener.onFailure(code, throwable, msg);
            }
        });
    }


    private void callSaveRegistration(String authToken, String instanceId, final RestServiceListener listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            AppInstallsViewModel appInstallsViewModel = getAppInstallsViewModel(instanceId);
            Call<String> callSaveRegistration = mAppInstallsApi.saveRegistration(authToken, appInstallsViewModel);
            callSaveRegistration.enqueue(new Callback<String>() {

                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            listener.onSuccess(response.body());
                            break;
                        case ResponseCode.BAD_REQUEST:
                        case ResponseCode.SERVER_ERROR:
                        default:
                            listener.onFailure(response.code(), null, null);
                    }
                }


                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    listener.onFailure(-1, t, RoomsterApplication.context.getString(R.string.registration_cant_make_call_to_save));
                }
            });
        } else {
            listener.onFailure(-1, null, RoomsterApplication.context.getString(R.string.no_network_connection_message));
        }
    }


    private void callUpdateRegistration(String authToken, String instanceId, boolean enableNotification,
                                        final RestServiceListener listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<String> callSaveRegistration = mAppInstallsApi.updateRegistration(authToken, instanceId, enableNotification);

            callSaveRegistration.enqueue(new Callback<String>() {

                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            listener.onSuccess(response.body());
                            break;
                        case ResponseCode.BAD_REQUEST:
                        case ResponseCode.SERVER_ERROR:
                        default:
                            listener.onFailure(response.code(), null, null);
                    }
                }


                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    listener.onFailure(-1, t, RoomsterApplication.context.getString(R.string.registration_cant_make_call_to_save));
                }
            });
        } else {
            listener.onFailure(-1, null, RoomsterApplication.context.getString(R.string.no_network_connection_message));
        }
    }


    private AppInstallsViewModel getAppInstallsViewModel(String instanceId) {
        AppInstallsViewModel appInstallsViewModel = new AppInstallsViewModel();

        appInstallsViewModel.setInstanceId(instanceId);
        appInstallsViewModel.setOsType(OS_TYPE_ANDROID);
        appInstallsViewModel.setOsVersion(Integer.toString(Build.VERSION.SDK_INT));

        try {
            PackageInfo pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            appInstallsViewModel.setAppVersion(Integer.toString(pInfo.versionCode));
        } catch (PackageManager.NameNotFoundException e) {
            //Nothing to do
        }

        appInstallsViewModel.setDeviceModel(getDeviceModel());

        return appInstallsViewModel;
    }


    private String getDeviceModel() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return model;
        }
        return manufacturer + " " + model;
    }
}