package com.roomster.constants;


public enum Asset {
    LICENSES("licenses.md");
    public String path;


    Asset(String path) {
        this.path = path;
    }

}
