package com.roomster.views;

import android.content.Context;
import android.support.v4.widget.Space;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.model.CatalogValueModel;
import com.roomster.utils.LogUtil;
import com.roomster.utils.PetsUtil;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by denisshovhenia on 31.10.16.
 */

public class PetsTableLayout extends TableLayout {

    @Inject
    LayoutInflater layoutInflater;

    private TableRow mCurrentRow;

    public PetsTableLayout(Context context) {
        super(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
        init();
    }

    public PetsTableLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        RoomsterApplication.getRoomsterComponent().inject(this);
        init();
    }

    private void init() {
        mCurrentRow = new TableRow(getContext());
    }

    public void addContent(List<CatalogValueModel> pets) {
        if (pets != null && !pets.isEmpty()) {
            int elementsWithEmptyViews = round(pets.size(), 4);

            int widthItemInPixels = (int) getResources().getDimension(R.dimen.pets_table_item_layout_width);
            int heightItemInPixels = (int) getResources().getDimension(R.dimen.pets_table_item_layout_height);

            for (int i = 0; i < elementsWithEmptyViews; i++) {

                if (i < pets.size()) {
                    // add item view
                    View v = createAndFillPetView(pets.get(i));
                    TableRow.LayoutParams lp = new TableRow.LayoutParams(widthItemInPixels,
                            heightItemInPixels);
                    mCurrentRow.addView(v, lp);
                } else {
                    // add empty view
                    Space space = new Space(getContext());
                    TableRow.LayoutParams lp = new TableRow.LayoutParams(widthItemInPixels,
                            heightItemInPixels);
                    mCurrentRow.addView(space, lp);
                }

                // if 1, 2 or 3 element
                if ((i + 1) % 4 != 0) {
                    Space space = new Space(getContext());
                    TableRow.LayoutParams lp = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,
                            LayoutParams.WRAP_CONTENT, 1f);
                    mCurrentRow.addView(space, lp);
                }

                // if 4 element (last in row)
                if (((i + 1) % 4 == 0) && (i != 0)) {
                    finishRowAndStartNew();
                }
            }
        }
    }

    private void finishRowAndStartNew() {
        addView(mCurrentRow);
        mCurrentRow = new TableRow(getContext());
    }

    private LinearLayout createAndFillPetView(CatalogValueModel petModel) {
        LinearLayout v = (LinearLayout) layoutInflater.inflate(R.layout.view_table_pet, null, false);
        ImageView petsView = (ImageView) v.findViewById(R.id.pet_image);
        TextView petsText = (TextView) v.findViewById(R.id.pet_text);

        petsView.setImageResource(PetsUtil.getPetImageByName(petModel.getValue()));
        petsText.setText(petModel.getName());
        return v;
    }

    private int round(double i, int v) {
        return (int) Math.ceil(i / v) * v;
    }
}