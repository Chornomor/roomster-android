package com.roomster.receiver;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.network.NetworkConnectionEvent;
import com.roomster.manager.NetworkManager;
import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;


/**
 * Created by Bogoi Bogdanov
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    @Inject
    NetworkManager mNetworkManager;

    @Inject
    EventBus mEventBus;


    @Override
    public void onReceive(final Context context, final Intent intent) {
        RoomsterApplication.getRoomsterComponent().inject(this);

        if (mNetworkManager.hasNetworkAccess()) {
            mEventBus.post(new NetworkConnectionEvent.NetworkConnectionAvailable());
        } else {
            mEventBus.post(new NetworkConnectionEvent.NetworkConnectionUnavailable());
        }
    }
}
