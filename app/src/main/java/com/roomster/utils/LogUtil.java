package com.roomster.utils;

import android.content.Intent;
import android.util.Log;

import com.roomster.BuildConfig;
import com.roomster.activity.SignInActivity;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.IntentExtras;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by alexchern on 26.07.16.
 */
public class LogUtil {
    private static final String LOG_TAG_DEBUG = "DEBUG";
    private static final String LOG_TAG_REQUEST = "REQUEST";
    private static final String LOG_TAG_RESPONSE = "RESPONSE";
    private static final String LOG_TAG_AGENT = "AGENT ";

    private boolean needToLoging = true; //set false to clear logs from http req/resp

    public static void logE(String log) {
        Log.e(LOG_TAG_DEBUG, log);
    }

    public static void logD(String log) {
        Log.d(LOG_TAG_DEBUG, log);
    }

    public static void logD(String tag, String log) {
        Log.d(tag, log);
    }

    public void loggerForRequestDebug(Request request) {
        if(needToLoging) {
            StringBuffer stringBuffer;
            if (request != null) {
                if (BuildConfig.DEBUG) {
                    stringBuffer = new StringBuffer();
                    if (request.method() != null)
                        stringBuffer.append("method " + request.method().toString());
                    if (request.body() != null)
                        stringBuffer.append(" | body " + request.body().toString());
                    if (request.headers() != null)
                        stringBuffer.append(" | headers " + request.headers().toString());
                    if (request.url() != null)
                        stringBuffer.append(" | url " + request.url().toString());
                    Log.d(LOG_TAG_AGENT + LOG_TAG_REQUEST, stringBuffer.toString());
                }
            }
        }
    }


    public void loggerForRsponseDebug(Response response) {
        if(needToLoging) {
            if (response != null) {
                StringBuffer stringBuffer;
                if (BuildConfig.DEBUG) {
                    stringBuffer = new StringBuffer();
                    if (response.code() != 0) {
                        stringBuffer.append("response code " + response.code());
                    }
                    if (response.message() != null)
                        stringBuffer.append(" | message " + response.message().toString());
                    if (response.request() != null)
                        stringBuffer.append(" | " + response.request().toString());
                    Log.d(LOG_TAG_AGENT + LOG_TAG_RESPONSE, stringBuffer.toString());
                }
            }
        }
    }


    public void errorHandler(int code, ResponseBody body) {
        String message = "";
        Pattern pattern = Pattern.compile("Active .+ listing required");
        Matcher m;
        if (code == 403){
                message = parseResponseMessage(body);
                m = pattern.matcher(message);
                if (m.find()) return;
                else startSignInActivity(code);
            }
        if (code == 401) startSignInActivity(code);

    }

    private String parseResponseMessage(ResponseBody body) {
        String message = "";
        JSONObject jsonResponce;
        try {
            jsonResponce = new JSONObject(body.string());
            message      = jsonResponce.getString("Message");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return message;
    }


    private void startSignInActivity(int code) {
        if(SignInActivity.instance != null && code == 403){
            SignInActivity.instance.requestBanned();
            return;
        }
        Intent intent = new Intent(RoomsterApplication.context, SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(SignInActivity.KEY_ERROR_CODE,code);
        RoomsterApplication.context.startActivity(intent);
    }
}
