package com.roomster.model;


import com.google.gson.internal.LinkedTreeMap;


/**
 * Created by Bogoi.Bogdanov on 1/5/2016.
 */
public class CountryCatalogModel {

    private int mId;

    private String mName;

    private String mPhoneCode;

    private String mShortName;


    public CountryCatalogModel(int id, String name, String phoneCode, String shortName) {
        mId = id;
        mName = name;
        mPhoneCode = phoneCode;
        mShortName = shortName;
    }


    public CountryCatalogModel(LinkedTreeMap<String, Object> linkedTreeMap) {
        mId = ((Double) linkedTreeMap.get("Id")).intValue();
        mName = (String) linkedTreeMap.get("Name");
        mPhoneCode = (String) linkedTreeMap.get("PhoneCode");
        mShortName = (String) linkedTreeMap.get("ShortName");
    }


    public int getId() {
        return mId;
    }


    public void setId(int id) {
        mId = id;
    }


    public String getName() {
        return mName;
    }


    public void setName(String mName) {
        mName = mName;
    }


    public String getPhoneCode() {
        return mPhoneCode;
    }


    public void setPhoneCode(String phoneCode) {
        phoneCode = mPhoneCode;
    }


    public String getShortName() {
        return mShortName;
    }


    public void setShortName(String shortName) {
        shortName = mShortName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CountryCatalogModel)) {
            return false;
        }

        CountryCatalogModel that = (CountryCatalogModel) o;

        if (mId != that.mId) {
            return false;
        }

        return !(mName != null ? !mName.equals(that.mName) : that.mName != null);
    }


    @Override
    public int hashCode() {
        int result = mId;
        result = 31 * result + (mName != null ? mName.hashCode() : 0);
        return result;
    }
}
