package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import com.roomster.enums.BookmarkTypeEnum;
import com.roomster.enums.ServiceTypeEnum;


public class BookmarkGroup {

    @SerializedName("bookmark_type")
    private BookmarkTypeEnum bookmarkType = null;

    @SerializedName("service_type")
    private ServiceTypeEnum serviceType = null;


    public BookmarkTypeEnum getBookmarkType() {
        return bookmarkType;
    }


    public void setBookmarkType(BookmarkTypeEnum bookmarkType) {
        this.bookmarkType = bookmarkType;
    }


    public ServiceTypeEnum getServiceType() {
        return serviceType;
    }


    public void setServiceType(ServiceTypeEnum serviceType) {
        this.serviceType = serviceType;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class BookmarkGroup {\n");

        sb.append("  bookmarkType: ").append(bookmarkType).append("\n");
        sb.append("  serviceType: ").append(serviceType).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}