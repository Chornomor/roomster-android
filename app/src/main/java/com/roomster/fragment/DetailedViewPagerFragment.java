package com.roomster.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.roomster.R;
import com.roomster.activity.DiscoveryPreferencesActivity;
import com.roomster.adapter.DetailedViewPageAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.SearchRestServiceEvent;
import com.roomster.event.application.ApplicationEvent;
import com.roomster.manager.BookmarkManager;
import com.roomster.manager.MeManager;
import com.roomster.manager.SearchManager;
import com.roomster.rest.model.ResultItemModel;
import com.roomster.rest.service.SearchRestService;
import com.roomster.utils.FancyPageTransformer;
import com.roomster.views.NonSwipableViewPager;
import com.roomster.views.ReplacableViewPager;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


/**
 * Created by michaelkatkov on 11/26/15.
 */
public class DetailedViewPagerFragment extends Fragment {
    protected static final int BOOKMARK_PAGE_SIZE = 1000;
    protected static final int PAGE_SIZE              = 30;
    protected static final int LISTING_LOADING_OFFSET = 10;

    private static final String INITIAL_ITEM_POSITION = "initial_item_position";
    private int mInitialItem;

    boolean bookmarkMode = false;

    public DetailedViewPageAdapter mPagerAdapter;

    @Bind(R.id.detailed_view_container)
    LinearLayout mDetailedViewContainer;

    @Bind(R.id.empty_layout)
    RelativeLayout mEmptyLayout;

    @Bind(R.id.empty_result_header)
    TextView mEmptyResultHeaderTV;

    @Bind(R.id.empty_result_subtext)
    TextView mEmptyResultSubtextTV;

    @Bind(R.id.empty_layout_button)
    TextView mEmptyLayoutButtonTV;

    @Bind(R.id.detailed_view_pager)
    NonSwipableViewPager mViewPager;

    @Bind(R.id.detailed_view_right_arrow)
    ImageView mRightArrow;

    @Bind(R.id.detailed_view_left_arrow)
    ImageView mLeftArrow;

    @Inject
    EventBus mEventBus;

    @Inject
    SearchManager mSearchManager;

    @Inject
    BookmarkManager bookmarkManager;

    @Inject
    SearchRestService mSearchRestService;

    @Inject
    Context mContext;

    @Inject
    MeManager meManager;

    private boolean socialBtnsVisible = true;
    private boolean isBookmarksType = false;
    private boolean isUserListings = true;

    private ReplacableViewPager.SimpleOnPageChangeListener mLoadingPageListener = new ReplacableViewPager.SimpleOnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            onPageChanged(position);
        }
    };


    public static DetailedViewPagerFragment newInstance(int initialItem) {
        DetailedViewPagerFragment fragment = new DetailedViewPagerFragment();

        Bundle args = new Bundle();
        args.putInt(INITIAL_ITEM_POSITION, initialItem);
        fragment.setArguments(args);

        return fragment;
    }


    public DetailedViewPagerFragment setType(boolean isBookmarks) {
        isBookmarksType = isBookmarks;
        return this;
    }
    public DetailedViewPagerFragment setIsUser(boolean isUser) {
        this.isUserListings = isUser;
        return this;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detailed_view_pager, container, false);
        ButterKnife.bind(this, view);
        mSearchManager.clearResultModel();
        bookmarkManager.clearBookmarkModel();
        recoverState();

        initViews();
        init(isBookmarksType);
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        if (!mEventBus.isRegistered(this)) {
            mEventBus.register(this);
        }
    }


    @Override
    public void onStop() {
        if (mEventBus.isRegistered(this)) {
            mEventBus.unregister(this);
        }
        super.onStop();
    }


    @OnClick(R.id.empty_layout_button)
    public void onEmptyLayoutButtonClicked() {
        Intent intent = new Intent(getActivity(), DiscoveryPreferencesActivity.class);
        startActivity(intent);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(SearchRestServiceEvent.SearchSuccess event) {
        onSearchResultSuccess();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(SearchRestServiceEvent.BookmarkSearchSuccess event) {
        onSearchResultSuccess();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(SearchRestServiceEvent.SearchFailure event) {
        Toast.makeText(mContext, getString(R.string.unable_to_get_search_info), Toast.LENGTH_SHORT).show();
    }


    protected void onSearchResultSuccess() {
        if (mPagerAdapter == null) {
            if(isBookmarksType) {
                setupViewPager(bookmarkManager);
            } else {
                setupViewPager(mSearchManager);
            }
        } else {
            updatePagerAdapterItems(isBookmarksType);
        }

    }

    private void setupViewPager(SearchManager manager) {
        if(manager.getCurrentResultItemsCount() == 0) {
            showEmptyResult();
        } else {
            hideEmptyResult();
            initViewPager(manager);
        }
    }

    private void updatePagerAdapterItems(boolean isBookmarksType) {
        if(isBookmarksType) {
            if(bookmarkManager.getResultModel() != null) {
                mPagerAdapter.updateItems(bookmarkManager.getResultModel().getItems());
            }
        } else {
            if(mSearchManager.getResultModel() != null) {
                mPagerAdapter.updateItems(getListFromSearchManager());
            }
        }
    }


    protected void recoverState() {
        Bundle args = getArguments();
        if (args != null) {
            mInitialItem = args.getInt(INITIAL_ITEM_POSITION);
        }
    }


    protected void init(boolean isBookmarkSearched) {
        if (haveSearchResult(isBookmarkSearched)) {
            if(isBookmarkSearched) {
                initViewPager(bookmarkManager);
            } else {
                initViewPager(mSearchManager);
            }
        } else {
            if(isBookmarkSearched) {
                mSearchRestService.loadNextBookmarkedResultsPage(BOOKMARK_PAGE_SIZE, false);
            } else {
                getNextResultsPage(true);
            }
        }
    }

    public void checkItemsVisibility(){
        if((isBookmarksType && bookmarkManager.getResultModel().getCount() <=0) ||
                (!isBookmarksType && mSearchManager.getResultModel().getCount() <= 0)){
            showEmptyResult();
        } else {
            hideEmptyResult();
        }
    }

    public void setmInitialItem(int mInitialItem) {
        this.mInitialItem = mInitialItem;
    }

    protected void onPageChanged(int position) {
        int startLoadingPosition = mSearchManager.getCurrentResultItemsCount() - LISTING_LOADING_OFFSET;
        if(isBookmarksType){
            startLoadingPosition = bookmarkManager.getCurrentResultItemsCount() - LISTING_LOADING_OFFSET;
        }
        if (position >= startLoadingPosition && mSearchRestService.hasMoreResults()) {
            getNextResultsPage(false);
        }
    }


    protected void getNextResultsPage(boolean withLoading) {
        mSearchRestService.loadNextResultsPage(PAGE_SIZE, withLoading);
    }


    protected void initViews() {
        mEmptyResultHeaderTV.setText(getString(R.string.empty_search_header));
        mEmptyResultSubtextTV.setText(getString(R.string.empty_search_subtext));
        mEmptyLayoutButtonTV.setText(getString(R.string.empty_search_button));
        mEmptyLayoutButtonTV.setVisibility(View.VISIBLE);
    }


    private void initViewPager(SearchManager mSearchManager) {
        //TODO bad design
        mViewPager.destroyDrawingCache();
        mViewPager.invalidate();
        new FancyPageTransformer(mViewPager, mLeftArrow, mRightArrow);
        mPagerAdapter = new DetailedViewPageAdapter(getChildFragmentManager(),
                getListFromSearchManager(mSearchManager), mSearchManager);
        mPagerAdapter.setSocialVisible(socialBtnsVisible);
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(mLoadingPageListener);

        mViewPager.setCurrentItem(mSearchManager.getLastViewedPosition());

        if (mInitialItem > 0 && mInitialItem < mPagerAdapter.getCount()) {
            mViewPager.setCurrentItem(mInitialItem);
        }
    }


    private void showEmptyResult() {
        mDetailedViewContainer.setVisibility(View.GONE);
        mEmptyLayout.setVisibility(View.VISIBLE);
    }


    private void hideEmptyResult() {
        mDetailedViewContainer.setVisibility(View.VISIBLE);
        mEmptyLayout.setVisibility(View.GONE);
    }


    private boolean haveSearchResult(boolean isBookmarkSearched) {
        if (isBookmarkSearched) {
            if (bookmarkManager.getResultModel() != null && bookmarkManager.getResultModel().getItems() != null
                    && !bookmarkManager.getResultModel().getItems().isEmpty()) {
                return true;
            }
            return false;
        } else {
            if (mSearchManager.getResultModel() != null && mSearchManager.getResultModel().getItems() != null
                    && !getListFromSearchManager().isEmpty()) {
                return true;
            }
            return false;
        }
    }

    public void setSocialBtnsVisibility(boolean visible){
        this.socialBtnsVisible = visible;
        if(mPagerAdapter != null) {
            mPagerAdapter.setSocialVisible(visible);
        }
    }
    private List<ResultItemModel> getListFromSearchManager(){
        return getListFromSearchManager(mSearchManager);
    }

    private List<ResultItemModel> getListFromSearchManager(SearchManager searchManager){
        List<ResultItemModel> list = searchManager.getResultModel().getItems();
        if(list == null)
            return null;
        if(isUserListings)
            return list;
        Long uid = meManager.getMe().getId();
        return clearOutByUserId(list,uid);
    }
    private List<ResultItemModel> clearOutByUserId(List<ResultItemModel> list, Long uid){
        if(list == null){
            return null;
        }
        List<ResultItemModel> delteList = new ArrayList<>();
        for(ResultItemModel m : list){
            if (m.getUser().getId().equals(uid)){
                delteList.add(m);
            }
        }
        list.removeAll(delteList);
        return list;
    }
}