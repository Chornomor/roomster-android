package com.roomster.fragment;


import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.roomster.BuildConfig;
import com.roomster.R;
import com.roomster.activity.SignInActivity;
import com.roomster.activity.SubscriptionActivity;
import com.roomster.activity.SupportActivity;
import com.roomster.activity.TransactionsActivity;
import com.roomster.adapter.CurrenciesSpinnerAdapter;
import com.roomster.adapter.LocaleSpinnerAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.cache.sharedpreferences.SharedPreferencesFiles;
import com.roomster.constants.RoomsterEndpoint;
import com.roomster.dialog.LoadingDialog;
import com.roomster.event.RestServiceEvents.AccountRestServiceEvent;
import com.roomster.event.RestServiceEvents.CatalogRestServiceEvent;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.settings.SettingsEvent;
import com.roomster.manager.CatalogsManager;
import com.roomster.manager.FacebookManager;
import com.roomster.manager.MeManager;
import com.roomster.manager.SearchManager;
import com.roomster.manager.SettingsManager;
import com.roomster.manager.TokenManager;
import com.roomster.model.LocaleCatalogModel;
import com.roomster.rest.model.LocalizationSettingsModel;
import com.roomster.rest.model.NotificationsViewModel;
import com.roomster.rest.service.AccountRestService;
import com.roomster.rest.service.CatalogRestService;
import com.roomster.rest.service.LocaleRestService;
import com.roomster.rest.service.NotificationsRestService;
import com.roomster.rest.service.ResponseCode;
import com.roomster.rest.service.UserListingsRestService;
import com.roomster.utils.LocaleUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnItemSelected;


/**
 * Created by Bogoi.Bogdanov on 10/7/2015.
 */
public class SettingsFragment extends Fragment {

    @Bind(R.id.settings_currency_text)
    Spinner mCurrencySpinner;

    @Bind(R.id.settings_language_text)
    Spinner mLanguageSpinner;

    @Bind(R.id.settings_push_toggle)
    ToggleButton mPushToggle;

    @Bind(R.id.settings_account_updates_toggle)
    ToggleButton mOnAccountUpdateToggle;

    @Bind(R.id.settings_matches_toggle)
    ToggleButton mOnMatchesToggle;

    @Bind(R.id.settings_on_help_desk_toggle)
    ToggleButton mOnHelpDeskToggle;

    @Bind(R.id.settings_on_new_messages_toggle)
    ToggleButton mOnNewMessagesToggle;

    @Bind(R.id.settings_email_notifications_expandable_layout)
    ExpandableLinearLayout mEmailNotificationsExpandableLayout;

    @Bind(R.id.settings_version_name)
    TextView mVersionName;

    @Inject
    NotificationsRestService mNotificationsRestService;

    @Inject
    LocaleRestService mLocaleRestService;

    @Inject
    AccountRestService mAccountRestService;

    @Inject
    CatalogRestService mCatalogRestService;

    @Inject
    MeManager mMeManager;

    @Inject
    UserListingsRestService mUserListingsRestService;

    @Inject
    EventBus mEventBus;

    @Inject
    Context mContext;

    @Inject
    SettingsManager mSettingsManager;

    @Inject
    CatalogsManager mCatalogsManager;

    @Inject
    FacebookManager mFacebookManager;

    @Inject
    TokenManager mTokenManager;

    @Inject
    SharedPreferencesFiles mSharedPreferencesFiles;

    @Inject
    SearchManager mSearchManager;

    LoadingDialog loadingDialog;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, view);

        getNotificationSettings();
        initViews();
        checkData();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        mEventBus.register(this);
    }


    @Override
    public void onPause() {
        checkForCurrencyChange();
        mEventBus.unregister(this);
        super.onPause();
    }

    @OnClick(R.id.share_roomster_btn)
    void onShareRoomsterClicked() {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, R.string.share_roomster_intent);
        share.putExtra(Intent.EXTRA_TEXT, getShareUrl());
        startActivity(Intent.createChooser(share, getResources().getString(R.string.share_roomster_intent)));
    }


    @OnClick(R.id.settings_delete_account_btn)
    void onDeleteAccountClicked() {
        openOnDeleteDialog();
    }


    @OnClick(R.id.settings_logout_btn)
    void onLogoutClicked() {
        logout();
    }


    @OnClick(R.id.settings_rate_us_container)
    void onRateUsClicked() {
        showLoveRoomsterDialog();
    }


    @OnClick(R.id.settings_email_notifications)
    void onEmailNotificationsClick() {
        mEmailNotificationsExpandableLayout.toggle();
    }


    @OnItemSelected(R.id.settings_currency_text)
    public void onCurrencySelected(int position) {
        mSettingsManager.saveCurrency((String) mCurrencySpinner.getSelectedItem());
    }


    @OnItemSelected(R.id.settings_language_text)
    public void onLanguageSelected(int position) {

        String selection = (String) mLanguageSpinner.getSelectedItem();

        final String selectedLocale = getLocaleBySelection(selection);
        if (selectedLocale != null && !mSettingsManager.getLocale().equals(selectedLocale)) {
            showLoading();
            mLocaleRestService.saveLocalizationSettings(selectedLocale, new RestServiceListener<LocalizationSettingsModel>() {
                @Override
                public void onSuccess(LocalizationSettingsModel result) {
                    mSettingsManager.saveLocale(selectedLocale);
                    if (getActivity() != null && !getActivity().isFinishing()) {
                        LocaleUtil.updateResources(getActivity(), selectedLocale);
                    }
                    updateCatalogs();
                }

                @Override
                public void onFailure(int code, Throwable throwable, String msg) {
                    super.onFailure(code, throwable, msg);
                }
            });
        }
    }


    private void showLoading() {
        if (loadingDialog == null) {
            loadingDialog = new LoadingDialog(getActivity());
            loadingDialog.show();
        } else {
            loadingDialog.show();
        }
    }


    private void hideLoading() {
        if (loadingDialog != null) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }


    private String getLocaleBySelection(String selection) {
        List<LocaleCatalogModel> list = mCatalogsManager.getLocalesList();
        for (LocaleCatalogModel model : list) {
            if (model.equalsToReadebleName(selection)) {
                return model.getLocale();
            }
        }
        return null;
    }


    @OnCheckedChanged(R.id.settings_push_toggle)
    public void onPushChange(boolean isActive) {
        mSettingsManager.saveIsPushActive(isActive);
    }


    @OnCheckedChanged(R.id.settings_account_updates_toggle)
    public void onNotifyOnAccountUpdatesChange(boolean isActive) {
        mSettingsManager.saveNotifyOnAccountUpdates(isActive);
    }


    @OnCheckedChanged(R.id.settings_matches_toggle)
    public void onNotifyOnMatchesChange(boolean isActive) {
        mSettingsManager.saveNotifyOnMatches(isActive);
    }


    @OnCheckedChanged(R.id.settings_on_help_desk_toggle)
    public void onNotifyOnHelpDeskActivityChange(boolean isActive) {
        mSettingsManager.saveNotifyOnHelpDeskActivity(isActive);
    }


    @OnCheckedChanged(R.id.settings_on_new_messages_toggle)
    public void onNotifyOnNewMessagesChange(boolean isActive) {
        mSettingsManager.saveNotifyOnNewMessages(isActive);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(AccountRestServiceEvent.DeleteSuccess event) {
        logout();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(AccountRestServiceEvent.DeleteFailure event) {
        if (event.code == ResponseCode.FORBIDDEN)
            showOnDeleteForbidenDialog();
        Toast.makeText(getActivity(), getString(R.string.settings_toast_delete_account), Toast.LENGTH_SHORT).show();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(CatalogRestServiceEvent.GetCatalogSuccess event) {
        fillCurrenciesSpinner();
        fillLanguagesSpinner();
        hideLoading();
        getActivity().recreate();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(CatalogRestServiceEvent.GetCatalogFailure event) {
        Toast.makeText(getActivity(), getString(R.string.settings_toast_get_countries_failed), Toast.LENGTH_SHORT).show();
    }

    public void showOnDeleteForbidenDialog() {
        new AlertDialog.Builder(getContext()).setTitle(R.string.err_acc_del_forbiden_title)
                .setMessage(R.string.err_acc_del_forbiden_msg)
                .setPositiveButton(R.string.err_acc_del_forbiden_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onSubcriptionClick();
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .create().show();
    }

    private void onSubcriptionClick() {
        Intent intent = new Intent(getActivity(), SubscriptionActivity.class);
        startActivity(intent);
    }

    private void initViews() {
        mPushToggle.setChecked(mSettingsManager.isPushActive());
        mVersionName.setText(String.format(getString(R.string.settings_version_name_format), BuildConfig.VERSION_NAME));
    }


    private void getNotificationSettings() {
        mNotificationsRestService.getNotificationsSettings(new RestServiceListener<NotificationsViewModel>() {

            @Override
            public void onSuccess(NotificationsViewModel model) {
                mSettingsManager.saveNotifyOnAccountUpdates(model.getAccountUpdates());
                mSettingsManager.saveNotifyOnMatches(model.getMatches());
                mSettingsManager.saveNotifyOnHelpDeskActivity(model.getHelpdeskActivity());
                mSettingsManager.saveNotifyOnNewMessages(model.getNewMessages());
                setNotificationSettings();
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void setNotificationSettings() {
        mOnAccountUpdateToggle.setChecked(mSettingsManager.shouldNotifyOnAccountUpdates());
        mOnMatchesToggle.setChecked(mSettingsManager.shouldNotifyOnMatches());
        mOnHelpDeskToggle.setChecked(mSettingsManager.shouldNotifyOnHelpDeskActivity());
        mOnNewMessagesToggle.setChecked(mSettingsManager.shouldNotifyOnNewMessages());
    }


    private void fillLanguagesSpinner() {
        List<LocaleCatalogModel> list = mCatalogsManager.getLocalesList();
        List<String> localeNames = new ArrayList<>();
        String locale = mSettingsManager.getLocale();
        Collections.sort(list);
        String defaultValue = "";
        for (LocaleCatalogModel model : list) {
            localeNames.add(model.getReadableName());
            if (model.equalsToLocale(locale)) {
                defaultValue = model.getReadableName();
            }
        }

        LocaleSpinnerAdapter adapter = new LocaleSpinnerAdapter(mContext, localeNames);
        mLanguageSpinner.setAdapter(adapter);


        selectDefault(mLanguageSpinner, localeNames, defaultValue);
    }


    private void clearUserData() {
        mTokenManager.clearToken();
        mSharedPreferencesFiles.getUserSharedPreferences().edit().clear().commit();
        mSearchManager.clearResultModel();
    }


    private void logout() {
        clearUserData();
        mFacebookManager.logout();
        openSignInActivity();
    }


    private void openSignInActivity() {
        Intent intent = new Intent(mContext, SignInActivity.class);
        //set a flag which will replace current activity
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        getActivity().finish();
    }


    private void openTransactionsHistoryActivity() {
        Intent intent = new Intent(mContext, TransactionsActivity.class);
        startActivity(intent);
    }


    private void fillCurrenciesSpinner() {
        List<String> list = mCatalogsManager.getCurrenciesList();
        CurrenciesSpinnerAdapter adapter = new CurrenciesSpinnerAdapter(mContext, list,
                CurrenciesSpinnerAdapter.SETTINGS_TYPE);
        mCurrencySpinner.setAdapter(adapter);
        String defaultValue = mSettingsManager.getCurrency();
        selectDefault(mCurrencySpinner, list, defaultValue);
    }


    private void selectDefault(Spinner spinner, List<String> list, String defaultValue) {
        int position = list.indexOf(defaultValue);
        if (position != -1) {
            spinner.setSelection(position);
        }
    }


    private void openOnDeleteDialog() {
        new AlertDialog.Builder(getActivity()).setTitle(getString(R.string.delete_account_dialog_title))
                .setPositiveButton(getString(R.string.button_yes), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAccountRestService.deleteAccount();
                    }
                }).setNegativeButton(R.string.button_no, null).show();
    }


    private String getShareUrl() {
        return RoomsterEndpoint.SHARE_LINK;
    }


    private void checkData() {
        if (mCatalogsManager.getCurrenciesList() == null) {
            mCatalogRestService.loadCatalogs();
        } else {
            fillCurrenciesSpinner();
            fillLanguagesSpinner();
        }
    }


    private void updateCatalogs() {
        mCatalogRestService.loadCatalogs();
    }


    private void checkForCurrencyChange() {
        if (mSearchManager.getSearchCriteria() != null) {
            String currentCurrency = mSearchManager.getSearchCriteria().getCurrency();
            String newCurrecny = mSettingsManager.getCurrency();
            if (!currentCurrency.equals(newCurrecny)) {
                mSearchManager.clearResultModel();
                mEventBus.post(new SettingsEvent.CurrencyUpdateEvent());
            }
        }
    }


    private void showLoveRoomsterDialog() {
        new android.support.v7.app.AlertDialog.Builder(getActivity())
                .setMessage(R.string.settings_dialog_love_roomster_message)
                .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showThankYouDialog();
                    }
                }).setNegativeButton(R.string.button_no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), SupportActivity.class);
                    startActivity(intent);
                }
            }
        }).show();
    }


    private void showThankYouDialog() {
        new android.support.v7.app.AlertDialog.Builder(getActivity()).setTitle(R.string.settings_dialog_thank_you_title)
                .setMessage(R.string.settings_dialog_thank_you_message)
                .setPositiveButton(R.string.settings_dialog_thank_you_rate_us_button,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
                                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);

                                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                        Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                try {
                                    startActivity(goToMarket);
                                } catch (ActivityNotFoundException e) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getShareUrl())));
                                }
                            }
                        }).setNegativeButton(R.string.settings_dialog_thank_you_no_button, null).show();
    }
}