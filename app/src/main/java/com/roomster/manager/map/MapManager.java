package com.roomster.manager.map;

import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.roomster.listener.BookmarksCallbackListener;
import com.roomster.manager.DiscoveryPrefsManager;
import com.roomster.model.SearchCriteria;
import com.roomster.rest.model.AccountDetailsViewModel;
import com.roomster.rest.model.ResultItemModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by andreybofanov on 11.07.16.
 */
public class MapManager {
    final Object lock = new Object();
    private static MapManager instance;
    public static final int PAGE_COUNT = 25;
    private static final int CLUSTER_COUNT = 4;
    private static final int MAX_CLUSTER_TO_LOAD = 3;
    @Inject
    DiscoveryPrefsManager mDiscoveryPrefManager;
    MapNet mapNet = new MapNet();
    private MapRestService service = new MapRestService();
    ArrayList<MapCluster> currentClusters = new ArrayList<>();

    private SearchCriteria mSearchCriteria = null;
    private List<ResultItemModel> models = new ArrayList<>();
    private Callback listener;
    ReasignClustersTask task;
    private boolean searchDone = false;
    boolean isResortInProcess = false;
    boolean hasResults = true;
    private int loadingClusterCount = 0;
    int currentClusterIndex = 0;
    int fullLoadedClusters = 0;
    boolean bookmarkMode = false;
    public BookmarksCallbackListener mCallback;

    public MapManager(Callback listener, DiscoveryPrefsManager mDiscoveryPrefManager) {
        this.listener = listener;
        this.mDiscoveryPrefManager = mDiscoveryPrefManager;

    }
    public void setBookmarkMode(boolean enabled){
        bookmarkMode = enabled;
        service.setBookmark(enabled ? MapRestService.BOOKMARKED_TYPE : null);
    }
    public ArrayList<MapCluster> getCurrentClusters(){
        return currentClusters;
    } // for debug purposes

    public void reset(){
        currentClusterIndex = 0;
        fullLoadedClusters = 0;
        models.clear();
    }

    public void onBoundsChanged(LatLngBounds newBounds,double zoom){
        if(task != null) {
            task.canceled = true;
            task = null;
        }
       task =  new ReasignClustersTask(newBounds,zoom);
        task.execute();
    }

    private long requestsTime = 0;

      void nextCluster(){
        if(isResortInProcess){
            return;
        }
        if(currentClusterIndex == 0)
            requestsTime = System.currentTimeMillis();
        if(currentClusterIndex >= currentClusters.size()){
            if(!searchDone && getMapItems(null).isEmpty())
                hasResults = false;
            searchDone = true;
            return;
        }
        if(currentClusters.get(currentClusterIndex).loaded){
            currentClusterIndex++;
            fullLoadedClusters++;
            if(currentClusterIndex >= currentClusters.size()){
//                listener.onUpdate();
            } else {
                nextCluster();
            }
            return;
        }
        if(loadingClusterCount >= MAX_CLUSTER_TO_LOAD){
            return;
        }
        LatLngBounds cluster = currentClusters.get(currentClusterIndex).bounds;
        if(mSearchCriteria == null)
            mSearchCriteria = mDiscoveryPrefManager.getSearchCriteria();
        mSearchCriteria.setSearchBounds(cluster);
        MapCLusterLoader loader =
                new MapCLusterLoader(currentClusters.get(currentClusterIndex),mSearchCriteria);
        loader.setUpdateOnFinish(currentClusterIndex == (currentClusters.size() -1));
        loader.execute();
        ++currentClusterIndex;
        ++loadingClusterCount;
        if(loadingClusterCount < MAX_CLUSTER_TO_LOAD){
            nextCluster();
        }
    }

    public List<ResultItemModel> getMapItems(LatLngBounds inBounds){
        ArrayList<ResultItemModel> models = new ArrayList<>();
        synchronized (lock) {
            for (MapCluster cluster : currentClusters) {
                models.addAll(cluster.results);
            }
        }
        this.models = models;
        return models;
    }
    public boolean hasResults(){
        return hasResults;
    }

    public void setListener(BookmarksCallbackListener mCallback) {
        this.mCallback = mCallback;
    }

    private class MapCLusterLoader implements MapRestService.MapRestCallback {
        MapCluster cluster;
        SearchCriteria mSearchCriteria;
        MapRestService service;
        boolean updateOnFinish = false;
         MapCLusterLoader(MapCluster cluster,SearchCriteria searchCriteria) {
            this.cluster = cluster;
            mSearchCriteria = searchCriteria.createClone();
            service = new MapRestService();
            service.setBookmark(bookmarkMode ? MapRestService.BOOKMARKED_TYPE : null);
        }

         void execute(){
            service.getListingsCount(mSearchCriteria,this);
        }

         void setUpdateOnFinish(boolean updateOnFinish){
            this.updateOnFinish = updateOnFinish;
        }

        @Override
        public void onGot(List<ResultItemModel> result,int requestedSize) {
            if(result.isEmpty()) {
                next();
                cluster.loaded = true;
                return;
            }
            ResultItemModel firstModel = result.get(0);
            if (mCallback != null)  mCallback.onBookmarksCountChanged(firstModel.count);

            if(firstModel.isCountCluster() && firstModel.count <= PAGE_COUNT
                    && firstModel.count > 0){
                requestMore();
                return;
            }
            if(firstModel.count > 0) {
                cluster.putResult(firstModel);
            }

            cluster.loaded = true;
            next();
        }

        public void requestMore(){
            service.getListings(mSearchCriteria, new MapRestService.MapRestCallback() {
                @Override
                public void onGot(List<ResultItemModel> result, int requestedCount) {
                    cluster.putResult(result.toArray(new ResultItemModel[result.size()]));
                    cluster.loaded = true;
                    next();
                }

                @Override
                public void onFailure() {
                    Log.e("DEBUG","FALIURE");
                    loadingClusterCount--;
                    next();
                }
            });
        }

        private void next(){
            loadingClusterCount--;
            fullLoadedClusters++;
            listener.onUpdate();
            if(fullLoadedClusters >= currentClusters.size()){
                long time = System.currentTimeMillis();
                Log.e("DEBUG","requests DONE in "  + ((float)(time - requestsTime))/1000.0f + "seconds");
                listener.onFinished();
            } else {
                nextCluster();
            }
        }

        @Override
        public void onFailure() {
            Log.e("DEBUG","FALIURE");
            loadingClusterCount--;
            next();
        }

    }



    public interface Callback {
        void onUpdate();
        void onFinished();
    }
    private class ReasignClustersTask extends AsyncTask<Void,Void,Void> {
        LatLngBounds newBounds;
        boolean canceled = false;
        double zoom;
         ReasignClustersTask(LatLngBounds newBounds,double zoom) {
            this.newBounds = newBounds;
            this.zoom = zoom;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isResortInProcess = true;


        }

        @Override
        protected Void doInBackground(Void... params) {
            double width = Math.abs(newBounds.southwest.longitude - newBounds.northeast.longitude);
            double height = Math.abs(newBounds.northeast.latitude - newBounds.southwest.latitude);
            double x = newBounds.southwest.longitude;
            double y = newBounds.northeast.latitude;
            double endX = x+width;
            double endY = y-height;
            long numCells = (long)Math.ceil(256.0D * Math.pow(2.0D, zoom) / 100.0D);
            double stepY = 180.0D/numCells/2;
            double stepX = 360.0D/numCells/2;
            ArrayList<LatLng> positions; positions = new ArrayList<>();
            while ( x < endX){
                y = newBounds.northeast.latitude;
                while (y > endY){
                    if(canceled){
                        onCancel();
                        return null;
                    }
                    LatLng sw = new LatLng(y-stepY,x);
                    LatLng ne = new LatLng(y,x+stepX);
                    LatLngBounds bounds = new LatLngBounds(sw,ne);
                    positions.add(bounds.getCenter());
                    y-=stepY;
                }
                x+=stepX;
            }
            currentClusters = new ArrayList<>(mapNet.getClusters(zoom,positions));
            synchronized (lock) {
                Collections.sort(currentClusters, new Comparator<MapCluster>() {
                    @Override
                    public int compare(MapCluster lhs, MapCluster rhs) {
                        if (lhs == null && rhs != null) {
                            return -1;
                        } else if (lhs == null && rhs == null) {
                            return 0;
                        } else if (rhs == null && lhs != null) {
                            return 1;
                        }
                        Double lhsD = distance(lhs.getCenter(), newBounds.getCenter());
                        Double rhsD = distance(rhs.getCenter(), newBounds.getCenter());
                        return lhsD.compareTo(rhsD);
                    }
                });
            }
            return null;
        }

        public  double distance(LatLng StartP, LatLng EndP) {
            double lat1 = StartP.latitude;
            double lat2 = EndP.latitude;
            double lon1 = StartP.longitude;
            double lon2 = EndP.longitude;
            double dLat = Math.toRadians(lat2-lat1);
            double dLon = Math.toRadians(lon2-lon1);
            double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                    Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                            Math.sin(dLon/2) * Math.sin(dLon/2);
            double c = 2 * Math.asin(Math.sqrt(a));
            return 6366000 * c;
        }

        public void onCancel(){
            synchronized (lock) {
                currentClusters.clear();
            }
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            isResortInProcess = false;
            models.clear();
            currentClusterIndex = 0;
            fullLoadedClusters = 0;
            task = null;
            if(canceled) {
                onCancel();
                return;
            }
            nextCluster();
        }
    }


    public static MapManager getInstance(Callback listener, DiscoveryPrefsManager mDiscoveryPrefManager){
        return new MapManager(listener,mDiscoveryPrefManager);
    }
}
