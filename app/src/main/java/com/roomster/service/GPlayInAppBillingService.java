package com.roomster.service;


import com.google.gson.Gson;

import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;

import com.android.vending.billing.IInAppBillingService;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.AndroidReceiptRestServiceEvent;
import com.roomster.event.RestServiceEvents.InAppProductsRestServiceEvent;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.model.ProductItem;
import com.roomster.rest.model.AndroidReceiptModel;
import com.roomster.rest.model.InAppProductViewModel;
import com.roomster.rest.service.AndroidReceiptRestService;
import com.roomster.rest.service.InAppProductsRestService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.UUID;

import javax.inject.Inject;


public class GPlayInAppBillingService extends Service {

    private static final String IAP_INTENT_ACTION                    = "com.android.vending.billing.InAppBillingService.BIND";
    private static final String ANDROID_VENDING_PACKAGE              = "com.android.vending";
    private static final String QUERY_SKUS_PRODUCT_ID_LIST_KEY       = "ITEM_ID_LIST";
    private static final int    IN_APP_API_LEVEL                     = 3;
    private static final int    RESPONSE_CODE_OK                     = 0;
    private static final String RESPONSE_CODE_KEY                    = "RESPONSE_CODE";
    private static final String DETAILS_LIST_KEY                     = "DETAILS_LIST";
    private static final String BUY_INTENT_KEY                       = "BUY_INTENT";
    private static final String INAPP_PURCHASE_DATA_KEY              = "INAPP_PURCHASE_DATA";
    private static final String IN_APP_PRODUCT_TYPE_SUBSCRIPTION_KEY = "subs";

    private final IBinder mBinder = new LocalBinder();

    private IInAppBillingService mService;

    private Handler mHandler;

    private String mDeveloperPayload;

    private Queue<Runnable> mWaitingQueue;

    private ServiceConnection mServiceConn = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }


        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
            while (!mWaitingQueue.isEmpty()) {
                mHandler.post(mWaitingQueue.remove());
            }
        }
    };

    @Inject
    InAppProductsRestService mInAppProductsRestService;

    @Inject
    AndroidReceiptRestService mReceiptRestService;

    @Inject
    EventBus mEventBus;


    @Override
    public void onCreate() {
        super.onCreate();
        RoomsterApplication.getRoomsterComponent().inject(this);
        mHandler = new Handler();
        mWaitingQueue = new ArrayDeque<>();
        Intent serviceIntent = new Intent(IAP_INTENT_ACTION);
        serviceIntent.setPackage(ANDROID_VENDING_PACKAGE);
        bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mService != null) {
            unbindService(mServiceConn);
        }
    }


    public void requestPackages(final RestServiceListener<List<ProductItem>> listener) {
        if (mService != null) {
            mHandler.post(new GetProductsRunnable(listener));
        } else {
            mWaitingQueue.add(new GetProductsRunnable(listener));
        }
    }


    public void getBuyPendingIntent(String sku, final RestServiceListener<PendingIntent> listener) {
        try {
            mDeveloperPayload = UUID.randomUUID().toString();
            Bundle buyIntentBundle = mService
              .getBuyIntent(IN_APP_API_LEVEL, getPackageName(), sku, IN_APP_PRODUCT_TYPE_SUBSCRIPTION_KEY, mDeveloperPayload);
            int responseCode = buyIntentBundle.getInt(RESPONSE_CODE_KEY);
            if (responseCode == RESPONSE_CODE_OK) {
                PendingIntent pendingIntent = buyIntentBundle.getParcelable(BUY_INTENT_KEY);
                listener.onSuccess(pendingIntent);
            } else {
                listener.onFailure(responseCode, null, null);
            }
        } catch (RemoteException e) {
            listener.onFailure(-1, e, RoomsterApplication.context.getString(R.string.cant_get_buy_intent));
        }
    }


    public void verifyPurchase(Intent resultIntent, RestServiceListener<AndroidReceiptModel> listener) {
        int responseCode = resultIntent.getIntExtra(RESPONSE_CODE_KEY, -1);
        if (responseCode == RESPONSE_CODE_OK) {
            String purchaseData = resultIntent.getStringExtra(INAPP_PURCHASE_DATA_KEY);

            try {
                JSONObject jsonData = new JSONObject(purchaseData);
                AndroidReceiptModel receipt = mapJsonToReceipt(jsonData);
                if (mDeveloperPayload != null && mDeveloperPayload.equals(receipt.getDeveloperPayload())) {
                    mHandler.post(new SendPurchaseInfoRunnable(listener, receipt));
                } else {
                    listener.onFailure(-1, null, RoomsterApplication.context.getString(R.string.developer_payload_not_match));
                }
            } catch (JSONException e) {
                listener.onFailure(-1, e, RoomsterApplication.context.getString(R.string.malformed_verification_data));
            }
        } else {
            listener.onFailure(responseCode, null, RoomsterApplication.context.getString(R.string.cant_process_purchase_request));
        }
    }


    private AndroidReceiptModel mapJsonToReceipt(JSONObject purchaseData) {
        AndroidReceiptModel receipt = new AndroidReceiptModel();
        receipt.setOrderId(purchaseData.optString("orderId"));
        receipt.setPackageName(purchaseData.optString("packageName"));
        receipt.setProductId(purchaseData.optString("productId"));
        receipt.setPurchaseTime(purchaseData.optLong("purchaseTime"));
        receipt.setPurchaseState(purchaseData.optInt("purchaseState"));
        receipt.setDeveloperPayload(purchaseData.optString("developerPayload"));
        receipt.setPurchaseToken(purchaseData.optString("purchaseToken"));
        receipt.setAutoRenewing(purchaseData.optBoolean("autoRenewing"));
        return receipt;
    }


    public class LocalBinder extends Binder {

        public GPlayInAppBillingService getService() {
            return GPlayInAppBillingService.this;
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    private class SendPurchaseInfoRunnable implements Runnable {

        private RestServiceListener<AndroidReceiptModel> mListener;
        private AndroidReceiptModel                      mReceipt;


        public SendPurchaseInfoRunnable(RestServiceListener<AndroidReceiptModel> listener, AndroidReceiptModel receipt) {
            mEventBus.register(this);
            mListener = listener;
            mReceipt = receipt;
        }


        @Override
        public void run() {
            mReceiptRestService.sendProductPaymentData(mReceipt);
        }

        @Subscribe(threadMode = ThreadMode.POSTING)
        public void onEvent(AndroidReceiptRestServiceEvent.AndroidReceiptSentSuccessEvent event) {
            mListener.onSuccess(mReceipt);
            mEventBus.unregister(this);
        }

        @Subscribe(threadMode = ThreadMode.POSTING)
        public void onEvent(AndroidReceiptRestServiceEvent.AndroidReceiptSentFailureEvent event) {
            mListener.onFailure(-1, null, RoomsterApplication.context.getString(R.string.server_verification_failed));
            mEventBus.unregister(this);
        }
    }


    private class GetProductsRunnable implements Runnable {

        private RestServiceListener<List<ProductItem>> mListener;


        public GetProductsRunnable(RestServiceListener<List<ProductItem>> listener) {
            mEventBus.register(this);
            mListener = listener;
        }


        @Override
        public void run() {
            mInAppProductsRestService.getInAppProducts();
        }


        public void getProductsFromGoogle(List<InAppProductViewModel> inAppProductsList) {
            ArrayList<String> skuList = new ArrayList<>();
            for (InAppProductViewModel product : inAppProductsList) {
                skuList.add(product.getSku());
            }

            Bundle querySkus = new Bundle();
            querySkus.putStringArrayList(QUERY_SKUS_PRODUCT_ID_LIST_KEY, skuList);
            try {
                Bundle skuDetails = mService
                  .getSkuDetails(IN_APP_API_LEVEL, getPackageName(), IN_APP_PRODUCT_TYPE_SUBSCRIPTION_KEY, querySkus);

                int response = skuDetails.getInt(RESPONSE_CODE_KEY, -1);
                if (response == RESPONSE_CODE_OK) {
                    ArrayList<String> products = skuDetails.getStringArrayList(DETAILS_LIST_KEY);
                    List<ProductItem> result = new ArrayList<>();

                    for (String product : products) {
                        ProductItem item = new Gson().fromJson(product, ProductItem.class);
                        result.add(item);
                    }

                    mListener.onSuccess(result);
                } else {
                    mListener.onFailure(response, null, null);
                }
            } catch (RemoteException e) {
                mListener.onFailure(-1, e, RoomsterApplication.context.getString(R.string.unable_to_get_product_item));
            }
        }

        @Subscribe(threadMode = ThreadMode.POSTING)
        public void onEvent(InAppProductsRestServiceEvent.GetInAppProductsSuccess event) {
            getProductsFromGoogle(event.inAppProductsList);
            mEventBus.unregister(this);
        }

        @Subscribe(threadMode = ThreadMode.POSTING)
        public void onEvent(InAppProductsRestServiceEvent.GetInAppProductsFailure event) {
            mEventBus.unregister(this);
        }
    }
}
