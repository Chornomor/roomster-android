package com.roomster.di.module;


import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;

import com.roomster.R;
import com.roomster.cache.sharedpreferences.SharedPreferencesFiles;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class ApplicationModule {

    private Application mApplication;


    public ApplicationModule(Application application) {
        mApplication = application;
    }


    @Singleton
    @Provides
    public Context provideContext() {
        return mApplication;
    }


    @Singleton
    @Provides
    public EventBus provideEventBus() {
        return EventBus.getDefault();
    }


    @Provides
    @Singleton
    public SharedPreferencesFiles provideSharedPreferencesFiles() {
        return new SharedPreferencesFiles(mApplication);
    }


    @Provides
    @Singleton
    LayoutInflater provideLayoutInflater() {
        return (LayoutInflater) mApplication.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Provides
    @Singleton
    Tracker getAnalyticsTracker() {
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(mApplication);
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        Tracker tracker = analytics.newTracker(R.xml.global_tracker);
        return tracker;
    }
}
