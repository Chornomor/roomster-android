package com.roomster.adapter;


import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.rest.model.HelpdeskMessage;
import com.roomster.utils.DateUtil;

import java.util.Date;
import java.util.List;


/**
 *
 */
public class SupportAdapter extends MessagesAdapter {

    public SupportAdapter(List<HelpdeskMessage> messages) {
        super(messages);
    }


    @Override
    protected void fillMyMessage(MyMessageVH viewHolder, Object messageModel) {
        HelpdeskMessage message = (HelpdeskMessage) messageModel;
        MyMessageViewHolder myMessageViewHolder = (MyMessageViewHolder) viewHolder;

        myMessageViewHolder.messageTextView.setText(message.getBody());
        String date = getDate(message.getCreated());
        myMessageViewHolder.dateTextView.setText(date);
        myMessageViewHolder.subjectTextView.setText(message.getSubject());
    }


    @Override
    protected void fillTheirMessage(TheirMessageVH viewHolder, Object messageModel) {
        HelpdeskMessage message = (HelpdeskMessage) messageModel;
        TheirMessageViewHolder theirMessageViewHolder = (TheirMessageViewHolder) viewHolder;

        theirMessageViewHolder.mSupportAuthorTextView.setText(message.getAuthor());
        theirMessageViewHolder.mSupportMessageTextView.setText(Html.fromHtml(message.getBody()));

        String date = getDate(message.getCreated());
        theirMessageViewHolder.dateTextView.setText(date);
    }


    @Override
    protected int getMyMessageListItemLayout() {
        return R.layout.list_item_support_messages_my;
    }


    @Override
    protected int getTheirMessageListItemLayout() {
        return R.layout.list_item_support_messages_their;
    }


    @Override
    protected MyMessageVH getNewMyMessageViewHolder(View root) {
        return new MyMessageViewHolder(root);
    }


    @Override
    protected TheirMessageVH getNewTheirMessageViewHolder(View root) {
        return new TheirMessageViewHolder(root);
    }


    @Override
    public int getItemViewType(int position) {
        boolean isMyMessage = ((HelpdeskMessage) mMessages.get(position)).getByUser();
        if (isMyMessage) {
            return MY_MESSAGE_VIEW_TYPE;
        } else {
            return THEIR_MESSAGE_VIEW_TYPE;
        }
    }


    private String getDate(Date date) {
        String formattedDate = DateUtil.formatToDayAndHour(date);
        if (formattedDate != null) {
            return formattedDate;
        } else {
            return "Sending...";
        }
    }


    public static class MyMessageViewHolder extends MyMessageVH {

        TextView messageTextView;
        TextView subjectTextView;
        TextView dateTextView;


        public MyMessageViewHolder(View root) {
            super(root);
            subjectTextView = (TextView) root.findViewById(R.id.list_item_messages_my_subject);
            messageTextView = (TextView) root.findViewById(R.id.list_item_support_my_message_text);
            dateTextView = (TextView) root.findViewById(R.id.list_item_support_my_message_timestamp);
        }
    }


    public static class TheirMessageViewHolder extends TheirMessageVH {

        TextView mSupportAuthorTextView;
        TextView mSupportMessageTextView;
        TextView dateTextView;


        public TheirMessageViewHolder(View root) {
            super(root);
            mSupportAuthorTextView = (TextView) root.findViewById(R.id.list_item_support_author);
            mSupportMessageTextView = (TextView) root.findViewById(R.id.list_item_support_message_text);
            dateTextView = (TextView) root.findViewById(R.id.list_item_support_message_timestamp);
        }
    }
}

