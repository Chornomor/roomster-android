package com.roomster.model;


import java.util.List;


/**
 * Created by "Michael Katkov" on 10/27/2015.
 */
public class FacebookAlbumsModel {

    private List<Data> data;

    private PagingModel paging;


    public List<Data> getData() {
        return data;
    }


    public void setData(List<Data> data) {
        this.data = data;
    }


    public PagingModel getPaging() {
        return paging;
    }


    public void setPaging(PagingModel paging) {
        this.paging = paging;
    }


    public static class Data {

        private String id;

        private int count;

        private String cover_photo;

        private String name;

        private String type;


        public String getId() {
            return id;
        }


        public void setId(String id) {
            this.id = id;
        }


        public int getCount() {
            return count;
        }


        public void setCount(int count) {
            this.count = count;
        }


        public String getCover_photo() {
            return cover_photo;
        }


        public void setCover_photo(String cover_photo) {
            this.cover_photo = cover_photo;
        }


        public String getName() {
            return name;
        }


        public void setName(String name) {
            this.name = name;
        }


        public String getType() {
            return type;
        }


        public void setType(String type) {
            this.type = type;
        }
    }


    public static class PagingModel {

        private Cursors cursors;
        private String  previous;
        private String  next;


        public Cursors getCursors() {
            return cursors;
        }


        public void setCursors(Cursors cursors) {
            this.cursors = cursors;
        }


        public String getPrevious() {
            return previous;
        }


        public void setPrevious(String previous) {
            this.previous = previous;
        }


        public String getNext() {
            return next;
        }


        public void setNext(String next) {
            this.next = next;
        }


        public static class Cursors {

            private String after;

            private String before;


            public String getAfter() {
                return after;
            }


            public void setAfter(String after) {
                this.after = after;
            }


            public String getBefore() {
                return before;
            }


            public void setBefore(String before) {
                this.before = before;
            }
        }
    }
}
