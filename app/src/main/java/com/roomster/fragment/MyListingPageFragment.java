package com.roomster.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.roomster.R;
import com.roomster.adapter.UserListingAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.manager.MeManager;
import com.roomster.rest.model.ListingGetViewModel;
import com.roomster.utils.DateUtil;
import com.roomster.views.ViewPagerWithIndicators;

import javax.inject.Inject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;


public class MyListingPageFragment extends Fragment {

    @Inject
    MeManager mMeManager;

    @Bind(R.id.my_listing_pager_with_indicators)
    ViewPagerWithIndicators mViewPagerWithIndicators;

    @Bind(R.id.my_list_new_text)
    TextView mNewTv;

    @Bind(R.id.my_listing_date)
    TextView mDateTv;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_listing_page, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    public void setModel(ListingGetViewModel listingGetViewModel,
            ViewPagerWithIndicators.BottomViewExpandListener listener) {
        initPageIndicator(listingGetViewModel, listener);
        setDate(listingGetViewModel);
        setIfNew(listingGetViewModel);
    }


    private void initPageIndicator(ListingGetViewModel listingGetViewModel,
            ViewPagerWithIndicators.BottomViewExpandListener listener) {
        UserListingAdapter adapter = new UserListingAdapter(getChildFragmentManager(), listingGetViewModel);
        mViewPagerWithIndicators.setAdapter(adapter, adapter.getCount() + 1);
        mViewPagerWithIndicators.setOnBottomViewExpandListener(listener);
    }


    private void setDate(ListingGetViewModel listingGetViewModel) {
        String date = DateUtil.getRelativeTimeSpanString(getContext(), listingGetViewModel.getRefreshed());
        mDateTv.setText(date);
    }


    private void setIfNew(ListingGetViewModel listingGetViewModel) {
        if (listingGetViewModel.getIsNew()) {
            mNewTv.setVisibility(View.VISIBLE);
        } else {
            mNewTv.setVisibility(View.GONE);
        }
    }


    public void setCurrentPositionSelected() {
        mViewPagerWithIndicators.setCurrentPositionSelected();
    }


    public void setInfoPositionSelected() {
        mViewPagerWithIndicators.setInfoPositionSelected();
    }
}
