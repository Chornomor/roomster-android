package com.roomster.adapter;


import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.rest.model.MessageView;
import com.roomster.utils.DateUtil;
import com.roomster.utils.ImageHelper;

import java.util.Date;
import java.util.List;


public class MessagesListAdapter extends MessagesAdapter {

    private Context             mContext;
    private long                mCurrentUserId;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {

        void onItemClick();
    }


    public MessagesListAdapter(Context context, List<MessageView> messages, long currentUserId) {
        super(messages);
        mContext = context;
        mCurrentUserId = currentUserId;
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }


    @Override
    protected void fillMyMessage(MyMessageVH viewHolder, Object messageModel) {
        MessageView messageView = (MessageView) messageModel;
        MyMessageViewHolder myMessageViewHolder = (MyMessageViewHolder) viewHolder;
        myMessageViewHolder.messageTextView.setText(messageView.getText());
        String date = getDate(messageView.getCreated());
        myMessageViewHolder.dateTextView.setText(date);
    }


    @Override
    protected void fillTheirMessage(TheirMessageVH viewHolder, final Object messageModel) {
        final MessageView messageView = (MessageView) messageModel;
        TheirMessageViewHolder theirMessageViewHolder = (TheirMessageViewHolder) viewHolder;

        theirMessageViewHolder.messageTextView.setText(messageView.getText());

        String date = getDate(messageView.getCreated());
        theirMessageViewHolder.dateTextView.setText(date);

        String otherUserPhoto = messageView.getUser().getUserPhoto();
        ImageHelper.loadCircledImageInto(mContext, otherUserPhoto, theirMessageViewHolder.profileImageView);
        theirMessageViewHolder.profileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick();
            }
        });
    }

    @Override
    protected int getMyMessageListItemLayout() {
        return R.layout.list_item_messages_my;
    }


    @Override
    protected int getTheirMessageListItemLayout() {
        return R.layout.list_item_messages_their;
    }


    @Override
    protected MyMessageViewHolder getNewMyMessageViewHolder(View root) {
        return new MyMessageViewHolder(root);
    }


    @Override
    protected TheirMessageViewHolder getNewTheirMessageViewHolder(View root) {
        return new TheirMessageViewHolder(root);
    }


    @Override
    public int getItemViewType(int position) {
        return ((MessageView) mMessages.get(position)).getUserId().equals(mCurrentUserId) ? MY_MESSAGE_VIEW_TYPE
                                                                                          : THEIR_MESSAGE_VIEW_TYPE;
    }


    private String getDate(Date date) {
        String formattedDate = DateUtil.formatToDayAndHour(date);
        if (formattedDate != null) {
            return formattedDate;
        } else {
            return mContext.getString(R.string.messages_sending_in_progress);
        }
    }


    public static class MyMessageViewHolder extends MyMessageVH {

        TextView messageTextView;
        TextView dateTextView;


        public MyMessageViewHolder(View root) {
            super(root);
            messageTextView = (TextView) root.findViewById(R.id.list_item_messages_my_message_text);
            dateTextView = (TextView) root.findViewById(R.id.list_item_messages_my_message_timestamp);
        }
    }


    public static class TheirMessageViewHolder extends TheirMessageVH {

        TextView  messageTextView;
        TextView  dateTextView;
        ImageView profileImageView;


        public TheirMessageViewHolder(View root) {
            super(root);
            messageTextView = (TextView) root.findViewById(R.id.list_item_support_message_text);
            dateTextView = (TextView) root.findViewById(R.id.list_item_support_message_timestamp);
            profileImageView = (ImageView) root.findViewById(R.id.list_item_support_profile_image);
        }
    }
}