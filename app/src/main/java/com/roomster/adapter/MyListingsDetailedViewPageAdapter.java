package com.roomster.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.roomster.fragment.MyListingDetailedViewFragment;
import com.roomster.rest.model.ResultItemModel;

import java.util.List;


public class MyListingsDetailedViewPageAdapter extends DetailedViewPageAdapter {

    public MyListingsDetailedViewPageAdapter(FragmentManager fragmentManager, List<ResultItemModel> list) {
        super(fragmentManager, list);
    }


    @Override
    public Fragment getItem(int position) {
        return MyListingDetailedViewFragment.newInstance(mList.get(position));
    }
}
