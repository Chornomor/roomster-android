package com.roomster.event.RestServiceEvents;


import com.roomster.event.Event;


/**
 * Created by "Michael Katkov" on 12/6/2015.
 */
public class BookmarksRestServiceEvent extends Event {

    public static class AddBookmarkSuccess extends BookmarksRestServiceEvent {

        public long listingId;
    }


    public static class RemoveBookmarkSuccess extends BookmarksRestServiceEvent {

        public long listingId;
    }


    public static class AddBookmarkFailure extends BookmarksRestServiceEvent {

        public long listingId;
    }


    public static class RemoveBookmarkFailure extends BookmarksRestServiceEvent {

        public long listingId;
    }
}
