package com.roomster.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.model.CatalogModel;
import com.roomster.model.CatalogValueModel;
import com.roomster.utils.AmenityUtil;

import javax.inject.Inject;
import java.util.List;


/**
 * Created by "Michael Katkov" on 10/11/2015.
 */
public class AmenitiesAdapter extends ArrayAdapter<CatalogValueModel> {

    private final static float TOUCHED = 1f;
    private final static float NORMAL  = 0.4f;

    @Inject
    LayoutInflater layoutInflater;

    private List<CatalogValueModel> mStoredAmenities;


    public AmenitiesAdapter(Context context, List<CatalogValueModel> amenities,
            List<CatalogValueModel> storedAmenities) {
        super(context, 0, amenities);
        mStoredAmenities = storedAmenities;
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;
        if (v == null) {
            v = layoutInflater.inflate(R.layout.view_amenity, parent, false);
            holder = new ViewHolder();
            holder.mAmenityView = (ImageView) v.findViewById(R.id.amenity_image);
            holder.mAmenityText = (TextView) v.findViewById(R.id.amenity_text);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        CatalogModel amenityModel = getItem(position);
        holder.mAmenityView.setImageResource(AmenityUtil.getAmenityImageByName(amenityModel.getId()));
        holder.mAmenityText.setText(splitAmenityName(amenityModel.getName()));
        boolean isChosenAmenity = mStoredAmenities != null && mStoredAmenities.contains(amenityModel);
        if (mStoredAmenities == null || isChosenAmenity) {
            holder.mAmenityView.setAlpha(TOUCHED);
            holder.mAmenityText.setAlpha(TOUCHED);
        } else {
            holder.mAmenityView.setAlpha(NORMAL);
            holder.mAmenityText.setAlpha(NORMAL);
        }
        return v;
    }


    public static class ViewHolder {

        public ImageView mAmenityView;

        public TextView mAmenityText;
    }


    private String splitAmenityName(String name) {
        return name.replace(' ', '\n');
    }
}
