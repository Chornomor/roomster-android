package com.roomster.activity;


import com.google.android.gms.analytics.HitBuilders;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Toast;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.application.ApplicationEvent;
import com.roomster.event.upgradeaccount.UpgradeAccountEvent;
import com.roomster.fragment.ChoosePackageFragment;
import com.roomster.fragment.UpgradeAccountSuccessFragment;
import com.roomster.manager.MeManager;
import com.roomster.model.ProductItem;
import com.roomster.rest.model.AndroidReceiptModel;
import com.roomster.rest.service.MeRestService;
import com.roomster.service.GPlayInAppBillingService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;


public class UpgradeAccountActivity extends BaseToolbarActivity {

    private static final int BUY_INTENT_REQUEST_CODE = 1001;

    private GPlayInAppBillingService mService;

    private ServiceConnection mServiceConn = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }


        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = ((GPlayInAppBillingService.LocalBinder) service).getService();
            mService.requestPackages(new RestServiceListener<List<ProductItem>>() {

                @Override
                public void onSuccess(List<ProductItem> result) {
                    setContent(ChoosePackageFragment.newInstance(result));
                    mEventBus.post(new ApplicationEvent.HideLoading());
                }


                @Override
                public void onFailure(int code, Throwable throwable, String msg) {
                    Toast.makeText(UpgradeAccountActivity.this, getString(R.string.upgrade_account_unable_to_load_packages), Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

    @Inject
    MeManager mMeManager;

    @Inject
    MeRestService mMeRestService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        mEventBus.post(new ApplicationEvent.ShowLoading());
        mTitleTv.setText("");
        Intent serviceIntent = new Intent(this, GPlayInAppBillingService.class);
        bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mService != null) {
            unbindService(mServiceConn);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName(getString(R.string.screen_name_upgrade_account));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BUY_INTENT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (mService != null) {
                    mService.verifyPurchase(data, new RestServiceListener<AndroidReceiptModel>() {

                        @Override
                        public void onSuccess(AndroidReceiptModel result) {
                            mMeManager.getMe().setFullAccess(true);
                            mMeRestService.requestMe();
                            setContent(new UpgradeAccountSuccessFragment());
                        }


                        @Override
                        public void onFailure(int code, Throwable throwable, String msg) {
                            Toast.makeText(UpgradeAccountActivity.this, getString(R.string.upgrade_account_cant_process_purchase),
                              Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(UpgradeAccountEvent.PackageSelected event) {
        if (mService != null) {
            mService.getBuyPendingIntent(event.selectedPackage.getId(), new RestServiceListener<PendingIntent>() {

                @Override
                public void onSuccess(PendingIntent result) {
                    try {
                        startIntentSenderForResult(result.getIntentSender(), BUY_INTENT_REQUEST_CODE, new Intent(), 0, 0, 0);
                    } catch (IntentSender.SendIntentException e) {
                        Toast.makeText(UpgradeAccountActivity.this, getString(R.string.upgrade_account_cant_process_purchase),
                          Toast.LENGTH_SHORT).show();
                    }
                }


                @Override
                public void onFailure(int code, Throwable throwable, String msg) {
                    Toast.makeText(UpgradeAccountActivity.this, getString(R.string.upgrade_account_cant_process_purchase),
                      Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(UpgradeAccountEvent.SuccessContinue event) {
        finish();
    }


    public void setActionBarTitle(int titleResId) {
        mTitleTv.setText(getString(titleResId));
    }
}