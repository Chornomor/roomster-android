package com.roomster.event.RestServiceEvents;


import com.roomster.event.Event;


public class ConversationRestServiceEvent extends Event {

    public static class ConversationDeleted extends ConversationRestServiceEvent {
        public String conversationId;

        public ConversationDeleted(){

        }

        public ConversationDeleted(String conversationId) {
            this.conversationId = conversationId;
        }
    }

    public static class ConversationUpdated extends ConversationRestServiceEvent {

    }
}
