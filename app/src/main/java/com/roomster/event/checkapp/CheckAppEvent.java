package com.roomster.event.checkapp;

/**
 * Created by Bogoi.Bogdanov on 5/13/2016.
 */
public class CheckAppEvent {

    public static class ShouldUpdateAppEvent extends CheckAppEvent {

        public boolean shouldUpdate;

        public ShouldUpdateAppEvent(boolean shouldUpdate) {
            this.shouldUpdate = shouldUpdate;
        }
    }
}