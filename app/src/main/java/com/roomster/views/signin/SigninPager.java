package com.roomster.views.signin;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.roomster.R;

/**
 * Created by andreybofanov on 15.07.16.
 */
public class SigninPager  extends ViewPager  {
    SigninPagerIndicator indicator;
    boolean countSet = false;
    public SigninPager(Context context) {
        super(context);
        init(null);
    }

    public SigninPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    void init(AttributeSet attrs){
        setOverScrollMode(OVER_SCROLL_NEVER);
    }
    @Override
    public boolean onInterceptHoverEvent(MotionEvent event) {
        onTouchEvent(event);
        return true;
    }

    @Override
    public void setAdapter(PagerAdapter adapter) {
        super.setAdapter(adapter);
        trySetUpIndicator();
    }

    public void setIndicator(SigninPagerIndicator indicator){
        this.indicator = indicator;
        addOnPageChangeListener(new PageListener());
        trySetUpIndicator();
    }

    void trySetUpIndicator(){
        if(getAdapter() != null && indicator != null  && !countSet){
            indicator.setUpIndicators(getAdapter().getCount());
            countSet = true;
        }
    }

    private class PageListener implements OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            ((SlidePage)getChildAt(position)).setHorizontalOffset(positionOffset);
            if(getChildCount() > position+1)
                ((SlidePage)getChildAt(position+1)).setHorizontalOffset(1.0f - positionOffset);
        }

        @Override
        public void onPageSelected(int position) {
            if(indicator != null)
                indicator.onPageSelected(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }




}
