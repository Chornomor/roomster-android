package com.roomster.views;


import android.view.View;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;


/**
 * Created by michaelkatkov on 1/4/16.
 */
public abstract class AbstractPanelSlideListener implements SlidingUpPanelLayout.PanelSlideListener {

    @Override
    public void onPanelSlide(View panel, float slideOffset) {

    }


    @Override
    public void onPanelCollapsed(View panel) {

    }


    @Override
    public void onPanelExpanded(View panel) {

    }


    @Override
    public void onPanelAnchored(View panel) {

    }


    @Override
    public void onPanelHidden(View panel) {

    }
}

