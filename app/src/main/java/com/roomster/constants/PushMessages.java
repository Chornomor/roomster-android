package com.roomster.constants;


public class PushMessages {

    public static final String TYPE_USER_MESSAGE = "user_message";

    public static final String TYPE_SUPPORT_MESSAGE = "support_message";
}
