package com.roomster.event.transactions;

import com.roomster.event.Event;
import com.roomster.rest.model.TransactionsModel;

import java.util.List;

public class TransactionsEvent extends Event {

    public static class TransactionGetSuccess extends TransactionsEvent {

        public List<TransactionsModel> transactionsModel;

        public TransactionGetSuccess(List<TransactionsModel> transactionsModel) {
            this.transactionsModel = transactionsModel;
        }
    }


    public static class TransactionGetFail extends TransactionsEvent {

    }
}
