package com.roomster.rest.api;


import com.roomster.rest.model.SavedSearch;
import com.roomster.rest.model.SavedSearchPostViewModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface SavedSearchesApi {

    /**
     * Get saved searches
     *
     * @param serviceType
     *   Service type
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<List<SavedSearch>>
     */

    @GET("SavedSearches")
    Call<List<SavedSearch>> savedSearchesGet(@Query("service_type") String serviceType,
                                             @Header("Authorization") String authorization);

    /**
     * Add saved search
     *
     * @param model
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<SavedSearch>
     */

    @POST("SavedSearches")
    Call<SavedSearch> savedSearchesPost(@Body SavedSearchPostViewModel model, @Header("Authorization") String authorization);
}
