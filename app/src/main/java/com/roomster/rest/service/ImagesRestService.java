package com.roomster.rest.service;


import com.google.gson.reflect.TypeToken;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.ImageRestServiceEvent;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.TokenManager;
import com.roomster.rest.RestClient;
import com.roomster.rest.api.ImagesApi;
import com.roomster.rest.model.UploadedImageModel;
import okhttp3.*;
import org.greenrobot.eventbus.EventBus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * Created by michaelkatkov on 12/28/15.
 */
public class ImagesRestService {

    private static final String AUTHORIZATION = "Authorization";
    private static final String JSON          = "image/jpeg";
    private static final String FILE          = "file";

    @Inject
    ImagesApi mImagesApi;

    @Inject
    TokenManager mTokenManager;

    @Inject
    EventBus mEventBus;

    @Inject
    RestClient mRestClient;

    @Inject
    NetworkManager mNetworkManager;


    public ImagesRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void postImage(final String imageEntityType, final Long entityId, final String fileUrl) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callPostImage(imageEntityType, entityId, fileUrl, result);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                mEventBus.post(new ImageRestServiceEvent.ImageAddFailed());
            }
        });
    }


    private void callPostImage(String imageEntityType, Long entityId, String fileUrl, String authorization) {
        if (mNetworkManager.hasNetworkAccess()) {
            //TODO should be done with Retrofit
            String url = RestClient.getBaseUrl() + "Images?image_entity_type=" + imageEntityType;
            if (entityId != null) {
                url += "&entity_id=" + entityId;
            }

            File file = new File(fileUrl);
            RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart(FILE, file.getName(), RequestBody.create(MediaType.parse(JSON), file)).build();

            Request request = new Request.Builder().url(url).addHeader(AUTHORIZATION, authorization).post(requestBody)
                    .build();
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(0, TimeUnit.SECONDS)
                    .readTimeout(0,TimeUnit.SECONDS)
                    .build();
            client.newCall(request).enqueue(new okhttp3.Callback() {

                @Override
                public void onFailure(okhttp3.Call call, IOException e) {
                    e.printStackTrace();
                    if(e instanceof SocketTimeoutException){
                        mEventBus.post(new ImageRestServiceEvent.ImageAddFailed(R.string.err_image_add_bad_connection));
                    } else {
                        mEventBus.post(new ImageRestServiceEvent.ImageAddFailed());
                    }
                }


                @Override
                public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                    switch (response.code()) {
                    case ResponseCode.OK:
                        List<UploadedImageModel> uploadedImageModel = mRestClient.getGson()
                                .fromJson(response.body().string(), new TypeToken<List<UploadedImageModel>>() {

                                }.getType());
                        mEventBus.post(new ImageRestServiceEvent.ImageAddSuccess(uploadedImageModel));
                        break;
                    default:
                        mEventBus.post(new ImageRestServiceEvent.ImageAddFailed());
                    }
                }
            });
        } else {
            mEventBus.post(new ImageRestServiceEvent.ImageAddFailed());
        }
    }


    public void deleteImage(final String imageEntityType, final Long entityId) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callDeleteImage(entityId, imageEntityType, result);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                mEventBus.post(new ImageRestServiceEvent.ImageDeleteFailed());
            }
        });
    }


    private void callDeleteImage(final Long id, String imageEntityType, String authorization) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<ResponseBody> call = mImagesApi.imagesDelete(id, imageEntityType, authorization);
            call.enqueue(new Callback<ResponseBody>() {

                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    switch (response.code()) {
                    case ResponseCode.OK:
                        mEventBus.post(new ImageRestServiceEvent.ImageDeleteSuccess(id));
                        break;
                    default:
                        mEventBus.post(new ImageRestServiceEvent.ImageDeleteFailed());
                    }
                }


                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    mEventBus.post(new ImageRestServiceEvent.ImageDeleteFailed());
                }
            });
        } else {
            mEventBus.post(new ImageRestServiceEvent.ImageDeleteFailed());
        }
    }
}