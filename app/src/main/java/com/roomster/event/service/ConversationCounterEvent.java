package com.roomster.event.service;


import com.roomster.event.Event;
import com.roomster.rest.model.ConversationCounter;


public class ConversationCounterEvent extends Event {

    public static class ConversationCounterUpdate extends ConversationCounterEvent {

        public ConversationCounter inboxCounter;


        public ConversationCounterUpdate(ConversationCounter inboxCounter) {
            this.inboxCounter = inboxCounter;
        }
    }
}
