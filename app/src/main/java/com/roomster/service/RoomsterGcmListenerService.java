package com.roomster.service;


import com.google.android.gms.gcm.GcmListenerService;

import android.os.Bundle;

import com.roomster.application.RoomsterApplication;
import com.roomster.constants.PushMessages;
import com.roomster.event.RestServiceEvents.ConversationRestServiceEvent;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.message.MessageEvent;
import com.roomster.event.service.ConversationCounterEvent;
import com.roomster.event.support.SupportEvent;
import com.roomster.manager.MeManager;
import com.roomster.manager.RoomsterNotificationManager;
import com.roomster.manager.SettingsManager;
import com.roomster.rest.model.ConversationCounter;
import com.roomster.rest.model.UserGetViewModel;
import com.roomster.rest.service.ConversationsRestService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;


public class RoomsterGcmListenerService extends GcmListenerService {

    private static final String TAG = RoomsterGcmListenerService.class.getCanonicalName();

    @Inject
    RoomsterNotificationManager mRoomsterNotificationManager;

    @Inject
    MeManager mMeManager;

    @Inject
    EventBus mEventBus;

    @Inject
    ConversationsRestService mConversationsRestService;

    @Inject
    SettingsManager mSettingsManager;

    private ConversationCounter mLastCounter;


    @Override
    public void onCreate() {
        super.onCreate();
        RoomsterApplication.getRoomsterComponent().inject(this);
        mEventBus.register(this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mEventBus.unregister(this);
    }


    /**
     * Called when message is received.
     *
     * @param from
     *   SenderID of the sender.
     * @param data
     *   Data bundle containing message data as key/value pairs.
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {
        if (canHandlePush(data)) {
            String type = data.getString("type");
            switch (type) {
                case PushMessages.TYPE_USER_MESSAGE:
                    handleUserMessage(data);
                    break;
                case PushMessages.TYPE_SUPPORT_MESSAGE:
                    handleSupportMessage(data);
                    break;
                default:
                    break;
            }
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ConversationRestServiceEvent.ConversationDeleted event) {
        updateConversationCounters();
    }


    private boolean canHandlePush(Bundle data) {
        //Check if there is logged-in user or the logged-in user is the same that the notification is for.
        UserGetViewModel currentUser = mMeManager.getMe();
        Long userId = getUserId(data);
        if (userId != null && currentUser != null && currentUser.getId().equals(userId) && mSettingsManager.isPushActive()) {
            return true;
        }

        return false;
    }


    private Long getUserId(Bundle data) {
        Long userId = null;
        if (data != null && data.getString("user_id") != null) {
            userId = Long.parseLong(data.getString("user_id"));
        }
        return userId;
    }


    private void handleUserMessage(Bundle data) {
        if (mSettingsManager.shouldNotifyOnNewMessages()) {
            String title = data.getString("title");
            String message = data.getString("message");
            String conversationId = data.getString("conversation_id");
            Long userId = Long.parseLong(data.getString("user_id"));
            Long otherUserId = Long.parseLong(data.getString("other_user_id"));
            mEventBus.post(new MessageEvent.NewMessage(title, message, conversationId, userId, otherUserId));

            try {
                mRoomsterNotificationManager.showUserMessage(title, message, conversationId, userId, otherUserId);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        updateConversationCounters();
    }


    private void handleSupportMessage(Bundle data) {
        if (mSettingsManager.shouldNotifyOnHelpDeskActivity()) {
            String title = data.getString("title");
            String message = data.getString("message");

            mRoomsterNotificationManager.showSupportMessage(title, message);
        }

        mEventBus.post(new SupportEvent.NewMessage());
    }


    private void updateConversationCounters() {
        mConversationsRestService.getConversationCounters(new RestServiceListener<List<ConversationCounter>>() {

            @Override
            public void onSuccess(List<ConversationCounter> result) {
                ConversationCounterEvent.ConversationCounterUpdate event = prepareEvent(result);
                if (mLastCounter == null || !areEqual(mLastCounter, event.inboxCounter)) {
                    mEventBus.post(event);
                    mLastCounter = event.inboxCounter;
                }
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
            }
        });
    }


    private boolean areEqual(ConversationCounter counter, ConversationCounter other) {
        return counter.getLabel().equals(other.getLabel()) && counter.getCountNew().equals(other.getCountNew()) && counter
          .getCountAll().equals(other.getCountAll());
    }


    private ConversationCounterEvent.ConversationCounterUpdate prepareEvent(List<ConversationCounter> conversationCounters) {
        ConversationCounterEvent.ConversationCounterUpdate event = null;

        for (ConversationCounter counter : conversationCounters) {
            if (counter.getLabel().equals("Inbox")) {
                event = new ConversationCounterEvent.ConversationCounterUpdate(counter);
            }
        }

        return event;
    }
}