package com.roomster.fragment;


import android.content.Context;

import com.roomster.R;
import com.roomster.event.RestServiceEvents.BookmarksRestServiceEvent;
import com.roomster.listener.BookmarksCallbackListener;
import com.roomster.rest.model.ResultItemModel;
import com.roomster.utils.LogUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


public class BookmarksDetailedViewPagerFragment extends DetailedViewPagerFragment {

    private final boolean IS_BOOKMARKS = true;

    private BookmarksCallbackListener mCallback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        bookmarkMode = true;
    }

    public void setCallback(BookmarksCallbackListener callback) {
        mCallback = callback;
    }


    public BookmarksDetailedViewPagerFragment setParentBookmarkType(boolean isBookmark) {
        super.setType(isBookmark);
        return this;
    }


    @Override
    protected void initViews() {
        mEmptyResultHeaderTV.setText(getString(R.string.empty_bookmark_header));
        mEmptyResultSubtextTV.setText(getString(R.string.empty_bookmark_subtext));
    }


    @Override
    protected void onSearchResultSuccess() {
        super.onSearchResultSuccess();
        if (mCallback != null) {
            mCallback.onBookmarksCountChanged(bookmarkManager.getResultModel().getCount());
        }
    }

    @Override
    protected void getNextResultsPage(boolean withLoading) {
        mSearchRestService.loadNextBookmarkedResultsPage(BOOKMARK_PAGE_SIZE, false);
    }


    @Override
    protected void onPageChanged(int position) {
        super.onPageChanged(position);
        if (mCallback != null) {
            mCallback.onPageChanged(position);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(BookmarksRestServiceEvent.RemoveBookmarkSuccess event) {
        mSearchManager.listUnbookmarkedId.add(event.listingId);
        removeBookmark(event.listingId);
    }


    private void removeBookmark(long id) {
        ResultItemModel deletedItem = bookmarkManager.removeItemById(id);
        if (deletedItem != null) {
            mPagerAdapter.remove(deletedItem);
            bookmarkManager.getResultModel().setCount(bookmarkManager.getResultModel().getCount()-1);
            mCallback.onBookmarksCountChanged(bookmarkManager.getResultModel().getCount());
            setmInitialItem(getCurrentItem());
            bookmarkManager.setLastViewedPosition(getCurrentItem());
            cleanPager();
            init(IS_BOOKMARKS);
            checkItemsVisibility();
        }

    }

    public int getCurrentItem(){
        return mViewPager.getCurrentItem();
    }


    private void cleanPager() {
        if (mPagerAdapter != null) {
            mViewPager.removeAllViews();
            mViewPager.setAdapter(null);
            mPagerAdapter = null;
        }
    }
//
//    private void cleanCounter() {
//        if (mCallback != null) {
//            mCallback.onPageChanged(0);
//            mCallback.onBookmarksCountChanged(-1);
//        }
//    }
}