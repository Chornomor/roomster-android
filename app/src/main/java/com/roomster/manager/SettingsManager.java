package com.roomster.manager;


import com.roomster.application.RoomsterApplication;
import com.roomster.cache.sharedpreferences.SharedPreferencesFiles;
import com.roomster.utils.LogUtil;

import javax.inject.Inject;
import javax.inject.Singleton;


/**
 * Created by "Michael Katkov" on 10/8/2015.
 */
@Singleton
public class SettingsManager {

    private final static String LOCALE = "locale";
    private final static String LOCALE_OLD = "locale_old";
    private final static String LOCALE_APP = "locale_app";
    private final static String CURRENCY = "currency";
    private final static String PUSH = "push";
    private final static String NOTIFY_ON_ACCOUNT_UPDATES = "notify_on_account_updates";
    private final static String NOTIFY_ON_MATCHES = "notify_on_matches";
    private final static String NOTIFY_ON_HELP_DESK_ACTIVITY = "notify_on_help_desk_activity";
    private final static String NOTIFY_ON_NEW_MESSAGES = "notify_on_new_messages";

    @Inject
    SharedPreferencesFiles sharedPreferencesFiles;


    public SettingsManager() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void saveLocale(String locale) {
        sharedPreferencesFiles.getUserSharedPreferences().edit().putString(LOCALE_OLD, getLocale()).commit();
        sharedPreferencesFiles.getUserSharedPreferences().edit().putString(LOCALE, locale).commit();
        sharedPreferencesFiles.getAppSharedPreferences().edit().putString(LOCALE_APP, locale).commit();
    }


    public void saveCurrency(String currency) {
        sharedPreferencesFiles.getUserSharedPreferences().edit().putString(CURRENCY, currency).commit();
    }


    public void saveIsPushActive(boolean isActivated) {
        sharedPreferencesFiles.getUserSharedPreferences().edit().putBoolean(PUSH, isActivated).commit();
    }


    public void saveNotifyOnAccountUpdates(boolean isActivated) {
        sharedPreferencesFiles.getUserSharedPreferences().edit().putBoolean(NOTIFY_ON_ACCOUNT_UPDATES, isActivated)
                .commit();
    }


    public void saveNotifyOnMatches(boolean isActivated) {
        sharedPreferencesFiles.getUserSharedPreferences().edit().putBoolean(NOTIFY_ON_MATCHES, isActivated).commit();
    }


    public void saveNotifyOnHelpDeskActivity(boolean isActivated) {
        sharedPreferencesFiles.getUserSharedPreferences().edit().putBoolean(NOTIFY_ON_HELP_DESK_ACTIVITY, isActivated)
                .commit();
    }


    public void saveNotifyOnNewMessages(boolean isActivated) {
        sharedPreferencesFiles.getUserSharedPreferences().edit().putBoolean(NOTIFY_ON_NEW_MESSAGES, isActivated)
                .commit();
    }


    public String getLocale() {
        return sharedPreferencesFiles.getUserSharedPreferences().getString(LOCALE, "");
    }


    public String getAppLocale() {
        return sharedPreferencesFiles.getAppSharedPreferences().getString(LOCALE_APP, "");
    }


    private String getLocaleOld() {
        return sharedPreferencesFiles.getUserSharedPreferences().getString(LOCALE_OLD, "");
    }


    public void refreshCurrentLocale() {
        sharedPreferencesFiles.getUserSharedPreferences().edit().putString(LOCALE_OLD, getLocale()).commit();
        sharedPreferencesFiles.getAppSharedPreferences().edit().putString(LOCALE_APP, getLocale()).commit();
    }


    public boolean isLocaleChange() {
        return !getLocale().equals(getLocaleOld());
    }


    public String getCurrency() {
        return sharedPreferencesFiles.getUserSharedPreferences().getString(CURRENCY, "");
    }


    public boolean isPushActive() {
        return sharedPreferencesFiles.getUserSharedPreferences().getBoolean(PUSH, true);
    }


    public boolean shouldNotifyOnAccountUpdates() {
        return sharedPreferencesFiles.getUserSharedPreferences().getBoolean(NOTIFY_ON_ACCOUNT_UPDATES, true);
    }


    public boolean shouldNotifyOnMatches() {
        return sharedPreferencesFiles.getUserSharedPreferences().getBoolean(NOTIFY_ON_MATCHES, true);
    }


    public boolean shouldNotifyOnHelpDeskActivity() {
        return sharedPreferencesFiles.getUserSharedPreferences().getBoolean(NOTIFY_ON_HELP_DESK_ACTIVITY, true);
    }


    public boolean shouldNotifyOnNewMessages() {
        return sharedPreferencesFiles.getUserSharedPreferences().getBoolean(NOTIFY_ON_NEW_MESSAGES, true);
    }
}
