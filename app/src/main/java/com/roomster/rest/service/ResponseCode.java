package com.roomster.rest.service;


/**
 * Created by "Michael Katkov" on 10/8/2015.
 */
public class ResponseCode {

    public static final int OK             = 200;
    public static final int SERVER_ERROR   = 500;
    public static final int BAD_REQUEST    = 400;
    public static final int USER_NOT_FOUND = 412;
    public static final int USER_CREATED   = 201;
    public static final int FORBIDDEN      = 403;
    public static final int UNAUTHORIZED   = 401;
}
