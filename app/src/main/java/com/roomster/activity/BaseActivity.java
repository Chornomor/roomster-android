package com.roomster.activity;


import com.google.android.gms.analytics.Tracker;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.network.NetworkConnectionEvent;
import com.roomster.manager.NetworkManager;
import com.roomster.utils.PermissionsAware;
import com.roomster.utils.PermissionsHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by Vasko on 27-Mar-16.
 */
public abstract class BaseActivity extends AppCompatActivity implements PermissionsAware {

    private PermissionsHelper mPermissionsHelper;

    @Bind(R.id.no_network_connection)
    RelativeLayout mNoNetworkConnectionView;

    @Inject
    EventBus mEventBus;

    @Inject
    Tracker mTracker;

    @Inject
    NetworkManager mNetworkManager;

    @Inject
    Context mContext;


    protected abstract int getMainContentLayout();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        FrameLayout parent = (FrameLayout) findViewById(R.id.main_wrapper);
        inflater.inflate(getMainContentLayout(), parent);
        RoomsterApplication.getRoomsterComponent().inject(this);
        ButterKnife.bind(this);
        mPermissionsHelper = new PermissionsHelper(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!mEventBus.isRegistered(this)) {
            mEventBus.register(this);
        }
        if (mNetworkManager.hasNetworkAccess()) {
            hideNoNetworkConnection();
        } else {
            showNoNetworkConnection();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (mEventBus.isRegistered(this)) {
            mEventBus.unregister(this);
        }
    }


    @NonNull
    @Override
    public PermissionsHelper getPermissionsHelper() {
        return mPermissionsHelper;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
            @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPermissionsHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(NetworkConnectionEvent.NetworkConnectionAvailable event) {
        hideNoNetworkConnection();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(NetworkConnectionEvent.NetworkConnectionUnavailable event) {
        showNoNetworkConnection();
    }


    private void showNoNetworkConnection() {
        mNoNetworkConnectionView.setVisibility(View.VISIBLE);
    }


    private void hideNoNetworkConnection() {
        mNoNetworkConnectionView.setVisibility(View.GONE);
    }
}