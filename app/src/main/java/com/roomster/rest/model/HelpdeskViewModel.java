package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;


public class HelpdeskViewModel {

    /**
     **/
    @SerializedName("helpdesk_messages")
    private List<HelpdeskMessage> helpdeskMessages = null;

    /**
     **/
    @SerializedName("new_count")
    private Integer newCount = null;


    public List<HelpdeskMessage> getHelpdeskMessages() {
        return helpdeskMessages;
    }


    public void setHelpdeskMessages(List<HelpdeskMessage> messages) {
        this.helpdeskMessages = messages;
    }


    public Integer getNewCount() {
        return newCount;
    }


    public void setNewCount(Integer newCount) {
        this.newCount = newCount;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class HelpdeskViewModel {\n");

        sb.append("  helpdeskMessages: ").append(helpdeskMessages).append("\n");
        sb.append("  newCount: ").append(newCount).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
