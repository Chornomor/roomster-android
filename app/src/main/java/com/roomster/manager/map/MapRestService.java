package com.roomster.manager.map;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.roomster.application.RoomsterApplication;
import com.roomster.di.component.RoomsterComponent;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.RestServiceEvents.SearchRestServiceEvent;
import com.roomster.manager.MeManager;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.SearchManager;
import com.roomster.manager.SettingsManager;
import com.roomster.manager.TokenManager;
import com.roomster.model.SearchCriteria;
import com.roomster.rest.api.SearchApi;
import com.roomster.rest.model.ResultItemModel;
import com.roomster.rest.model.ResultModel;
import com.roomster.rest.service.SearchRestService;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by andreybofanov on 12.07.16.
 */
public class MapRestService   {
    public static final String BOOKMARKED_TYPE = "Bookmarked";
    @Inject
    EventBus mEventBus;

    @Inject
    SearchApi mSearchApi;

    @Inject
    TokenManager mTokenManager;

    @Inject
    SearchManager mSearchManager;

    @Inject
    SettingsManager mSettingsManager;

    @Inject
    NetworkManager mNetworkManager;

    private int pageSize = MapManager.PAGE_COUNT;
    private String bookmark = null;
    private boolean firstSearch = true;
    private LatLngBounds lastSearchBounds;
    private boolean isFullSearch = false;
    public MapRestService(){
        RoomsterApplication.getRoomsterComponent().inject(this);
    }



    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }


    public void setBookmark(String bookmark) {
        this.bookmark = bookmark;
    }

    public void getListings( final SearchCriteria criteria, final MapRestCallback callback){
        isFullSearch = true;
        lastSearchBounds = criteria.getSearchBounds();
        mTokenManager.getAccessToken(new RestServiceListener<String>() {
            @Override
            public void onSuccess(String result) {
                super.onSuccess(result);
                executeSearch(result,1,criteria,callback);
            }

            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                super.onFailure(code, throwable, msg);
                mEventBus.post(new SearchRestServiceEvent.SearchFailure());
            }
        });
    }

    public void getListingsCount(final SearchCriteria criteria, final MapRestCallback callback){
        isFullSearch = false;
        lastSearchBounds = criteria.getSearchBounds();
        mTokenManager.getAccessToken(new RestServiceListener<String>() {
            @Override
            public void onSuccess(String result) {
                super.onSuccess(result);
                executeSearch(result,1,criteria,callback);
            }

            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                super.onFailure(code, throwable, msg);
                mEventBus.post(new SearchRestServiceEvent.SearchFailure());
            }
        });
    }

    public void executeSearch(final String auth, final int pageNumber, final SearchCriteria criteria, final MapRestCallback callback){
        if(!mNetworkManager.hasNetworkAccess()){
            mEventBus.post(new SearchRestServiceEvent.SearchFailure());
            return;
        }
        final int pageSize = isFullSearch ? this.pageSize : 1;
        LatLngBounds bounds = criteria.getSearchBounds();
        if(bounds.southwest.longitude <= -180.0D){
            bounds = new LatLngBounds(
                    new LatLng(bounds.southwest.latitude,179.9D),
                    bounds.northeast
            );
            criteria.setSearchBounds(bounds);
        }
        Call<ResultModel> call = prepareCall(auth,pageNumber,pageSize,bookmark,criteria);
        call.enqueue(new Callback<ResultModel>() {
            @Override
            public void onResponse(Call<ResultModel> call, Response<ResultModel> response) {
//                if(firstSearch){
//                    mSearchManager.setResultModel(response.body(),criteria);
//                } else {
//                    mSearchManager.addResultModel(response.body());
//                }
                firstSearch = false;
                if(response.code() == 429){
                    executeSearch(auth,pageNumber,criteria,callback);
                    return;
                }
                if(isFullSearch){
                    callback.onGot(getItemsWithLocation(response.body()),pageSize);
                } else {
                    callback.onGot(getItemsCount(response.body()),pageSize);
                }

            }

            @Override
            public void onFailure(Call<ResultModel> call, Throwable t) {
                callback.onFailure();
            }
        });

    }

    /**
     * To make bookmarks call we use almost all parameters as null, otherwise we need to fill parameters
     */
    private Call<ResultModel> prepareCall(String auth, int pageNumber, int pageSize, final String bookmark,
                                          SearchCriteria criteria) {
        DecimalFormat dFormat = new DecimalFormat();
        dFormat.setMaximumFractionDigits(8);
        if (bookmark == null) {
            return mSearchApi
                    .searchGet(pageNumber, pageSize, criteria.getSort(), criteria.getServiceType(), turnicate(criteria.getLatSouthWest()),
                            turnicate(criteria.getLongSouthWest()), turnicate(criteria.getLatNorthEast()), turnicate(criteria.getLongNorthEast()), criteria.getRadiusScale(),
                            criteria.getBudgetMin(), criteria.getBudgetMax(), criteria.getAgeMin(), criteria.getAgeMax(),
                            criteria.getHouseholdSex(), criteria.getSex(),
                    /* zodiac */ null, criteria.getPetsPrefs(), criteria.getMyPets(), criteria.getBedrooms(),
                            criteria.getBathrooms(), criteria.getAmenities(), bookmark, criteria.getDataIn(), criteria.getDataOut(),!isFullSearch, !isFullSearch,
                            criteria.getCurrency(), /* locale */
                            mSettingsManager.getLocale(), auth);
        } else {
            return mSearchApi
                    .searchGet(pageNumber, pageSize, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
                            null, null, null, null, null, bookmark, null, null, criteria.getCurrency(), mSettingsManager.getLocale(), auth);
        }
    }
    DecimalFormat dFormat = new DecimalFormat();
    double turnicate(double value){
        dFormat.setMaximumFractionDigits(8);
        NumberFormat nFormat = NumberFormat.getInstance(Locale.US);

        try {
            return nFormat.parse(dFormat.format(value).replace(",",".")).doubleValue();
        } catch (ParseException e) {
            e.printStackTrace();
            return value;
        }
    }



    private List<ResultItemModel> getItemsCount(ResultModel resultModel) {
        List<ResultItemModel> list = new ArrayList<>();
        ResultItemModel item = new ResultItemModel();
        if(resultModel != null) {
            item.count = resultModel.getCount();
        } else {
            item.count = 0;
        }
        item.setCustomBounds(lastSearchBounds);
        item.setCustomLocaton(lastSearchBounds.getCenter());
        list.add(item);
        return list;
    }

    private List<ResultItemModel> getItemsWithLocation(ResultModel resultModel) {
        if(resultModel == null)
            return new ArrayList<>();
        List<ResultItemModel> resultItemModels = new ArrayList<>();
        for (ResultItemModel resultItemModel : resultModel.getItems()) {
            if (resultItemModel.getListing() != null && resultItemModel.getListing().getGeoLocation() != null
                    && resultItemModel.getListing().getGeoLocation().getFullAddress() != null) {
                resultItemModels.add(resultItemModel);
            }
        }
        return resultItemModels;
    }

    interface MapRestCallback {
        void onGot(List<ResultItemModel> result, int requestedCount);
        void onFailure();
    }
}
