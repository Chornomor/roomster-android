package com.roomster.rest.service;


import android.content.Context;
import android.support.annotation.Nullable;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.SettingsManager;
import com.roomster.manager.TokenManager;
import com.roomster.rest.api.NotificationsApi;
import com.roomster.rest.model.NotificationsViewModel;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Bogoi.Bogdanov on 4/28/2016.
 */
public class NotificationsRestService {

    @Inject
    SettingsManager mSettingsManager;

    @Inject
    Context mContext;

    @Inject
    EventBus mEventBus;

    @Inject
    TokenManager mTokenManager;

    @Inject
    NotificationsApi mNotificationsApi;

    @Inject
    NetworkManager mNetworkManager;


    public NotificationsRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void getNotificationsSettings(final RestServiceListener<NotificationsViewModel> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String token) {
                callNotificationsSettings(token, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                if (throwable != null) {
                    throwable.printStackTrace();
                }
                if (listener != null) {
                    listener.onFailure(code, throwable, msg);
                }
            }
        });
    }


    public void saveNotificationsSettings(final RestServiceListener<NotificationsViewModel> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String token) {
                callPutNotificationsSettings(token, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                if (listener != null) {
                    listener.onFailure(code, throwable, msg);
                }
            }
        });
    }


    private void callNotificationsSettings(String authToken,
                                           @Nullable final RestServiceListener<NotificationsViewModel> listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<NotificationsViewModel> callNotificationsSettings = mNotificationsApi.notificationsGet(authToken);
            callNotificationsSettings.enqueue(new Callback<NotificationsViewModel>() {

                @Override
                public void onResponse(Call<NotificationsViewModel> call, Response<NotificationsViewModel> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            if (listener != null) {
                                listener.onSuccess(response.body());
                            }
                            break;
                        case ResponseCode.BAD_REQUEST:
                        case ResponseCode.SERVER_ERROR:
                        default:
                            if (listener != null) {
                                listener.onFailure(response.code(), null, null);
                            }
                    }
                }


                @Override
                public void onFailure(Call<NotificationsViewModel> call, Throwable t) {
                    if (listener != null) {
                        listener.onFailure(-1, t, RoomsterApplication.context.getString(R.string.notification_cant_make_call_to_get));
                    }
                }
            });
        } else {
            if (listener != null) {
                listener.onFailure(-1, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
            }
        }
    }


    private void callPutNotificationsSettings(String token,
                                              @Nullable final RestServiceListener<NotificationsViewModel> listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            NotificationsViewModel notificationsViewModel = getCurrentNotifications();
            Call<NotificationsViewModel> callNotificationsSettings = mNotificationsApi
              .notificationsPut(notificationsViewModel, token);
            callNotificationsSettings.enqueue(new Callback<NotificationsViewModel>() {

                @Override
                public void onResponse(Call<NotificationsViewModel> call, Response<NotificationsViewModel> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            if (listener != null) {
                                listener.onSuccess(response.body());
                            }
                            break;
                        case ResponseCode.BAD_REQUEST:
                        case ResponseCode.SERVER_ERROR:
                        default:
                            if (listener != null) {
                                listener.onFailure(response.code(), null, null);
                            }
                    }
                }


                @Override
                public void onFailure(Call<NotificationsViewModel> call, Throwable t) {
                    if (listener != null) {
                        listener.onFailure(-1, t, RoomsterApplication.context.getString(R.string.notification_cant_make_call_to_put));
                    }
                }
            });
        } else {
            if (listener != null) {
                listener.onFailure(-1, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
            }
        }
    }


    private NotificationsViewModel getCurrentNotifications() {
        NotificationsViewModel notificationsViewModel = new NotificationsViewModel();
        notificationsViewModel.setAccountUpdates(mSettingsManager.shouldNotifyOnAccountUpdates());
        notificationsViewModel.setMatches(mSettingsManager.shouldNotifyOnMatches());
        notificationsViewModel.setHelpdeskActivity(mSettingsManager.shouldNotifyOnHelpDeskActivity());
        notificationsViewModel.setNewMessages(mSettingsManager.shouldNotifyOnNewMessages());

        return notificationsViewModel;
    }
}