package com.roomster.manager;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookRequestError;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.LoggingBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.enums.GenderEnum;
import com.roomster.enums.ProviderEnum;
import com.roomster.enums.ServiceTypeEnum;
import com.roomster.event.ManagersEvents.FacebookManagerEvent;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.model.FacebookInterestsModel;
import com.roomster.model.FacebookUserModel;
import com.roomster.rest.model.AccountViewModel;
import com.roomster.utils.DateUtil;
import com.roomster.utils.StringsUtil;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class FacebookManager {

    private static final int ERROR_CODE = -1;
    private static final int GET_ME_TRIAL_NUMBER = 4;

    //PERMISSIONS
    private static final String PUBLIC_PROFILE = "public_profile";
    private static final String USER_FRIENDS = "user_friends";
    public static final String EMAIL = "email";
    public static final String USER_BIRTHDAY = "user_birthday";
    private static final String USER_LIKES = "user_likes";
    private static final String USER_PHOTOS = "user_photos";

    //FIELDS
    private static final String FIELDS = "fields";
    private static final String ME = "me";
    private static final String NAME = "name";
    private static final String ID = "id";
    private static final String GENDER = "gender";
    private static final String BIRTHDAY = "birthday";
    private static final String HOMETOWN = "hometown";
    private static final String ABOUT = "about";
    private static final String LANGUAGES = "languages";
    private static final String IMAGE = "picture.width(500).height(500)";
    private static final String LIKES = "likes";
    private static final String PHOTOS = "photos";
    private static final String ME_LIKES = "/" + ME + "/" + LIKES;

    private static final String GET_USER_FIELDS = TextUtils
            .join(",", new String[]{NAME, ID, EMAIL, GENDER, BIRTHDAY, HOMETOWN, ABOUT, LANGUAGES, IMAGE, PHOTOS});


    public enum LoginError {
        NOT_ALL_PERMISSIONS_GRANTED, CANCELED_BY_USER, FACEBOOK_ERROR
    }


    private int mGetMeCurrentTrialNumber;
    private FacebookUserModel mCurrentUser;
    private FacebookInterestsModel mCurrentInterests;
    private CallbackManager mCallbackManager;
    private LoginListener mLoginListener;
    private Set<String> mRequiredPermissions;
    private Set<String> mPreferredPermissions;


    public interface LoginListener {

        void onSuccess();

        void onError(LoginError loginError, @Nullable Set<String> deniedPermissions);
    }


    @Inject
    EventBus mEventBus;

    @Inject
    Context mContext;


    public FacebookManager() {
        RoomsterApplication.getRoomsterComponent().inject(this);
        mPreferredPermissions = new HashSet<>(
                Arrays.asList(PUBLIC_PROFILE, USER_FRIENDS, EMAIL, USER_BIRTHDAY, USER_LIKES, USER_PHOTOS));
        mRequiredPermissions = new HashSet<>(Arrays.asList(EMAIL, USER_BIRTHDAY));
        sdkInitialize();
        initComponentsToLogin();
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }


    public void login(Activity activity, LoginListener loginListener) {
        mLoginListener = loginListener;
        LoginManager.getInstance().logInWithReadPermissions(activity, mPreferredPermissions);
    }


    public void logout() {
        LoginManager.getInstance().logOut();
        mCurrentUser = null;
        mCurrentInterests = null;
    }


    public void requestMyProfile(final RestServiceListener<FacebookUserModel> listener) {
        final GraphRequest graphRequest = prepareMeReguest();
        mGetMeCurrentTrialNumber = GET_ME_TRIAL_NUMBER;
        graphRequest.setCallback(new GraphRequest.Callback() {

                                     @Override
                                     public void onCompleted(GraphResponse graphResponse) {
                                         JSONObject json = graphResponse.getJSONObject();
                                         if (graphResponse.getError() == null && json != null) {
                                             mCurrentUser = (new Gson()).fromJson(json.toString(), FacebookUserModel.class);
                                             listener.onSuccess(mCurrentUser);
                                         } else if (mGetMeCurrentTrialNumber > 0) {
                                             mGetMeCurrentTrialNumber--;
                                             graphRequest.executeAsync();
                                         } else {
                                             listener.onFailure(ERROR_CODE, prepareError(graphResponse.getError()),
                                                     mContext.getString(R.string.facebook_error));
                                         }
                                     }
                                 }

        );
        graphRequest.executeAsync();
    }


    public void requestMyProfile() {
        GraphRequest graphRequest = prepareMeReguest();
        graphRequest.setCallback(new GraphRequest.Callback() {

            @Override
            public void onCompleted(GraphResponse graphResponse) {
                JSONObject json = graphResponse.getJSONObject();
                if (graphResponse.getError() == null && json != null) {
                    setCurrentUser((new Gson()).fromJson(json.toString(), FacebookUserModel.class));
                    mEventBus.post(new FacebookManagerEvent.GetMeSuccess());
                } else {
                    mEventBus.post(new FacebookManagerEvent.GetMeFailure());
                }
            }
        });
        graphRequest.executeAsync();
    }


    public void requestMyLikes() {
        new GraphRequest(AccessToken.getCurrentAccessToken(), ME_LIKES, null, HttpMethod.GET,
                new GraphRequest.Callback() {

                    public void onCompleted(GraphResponse graphResponse) {
                        JSONObject json = graphResponse.getJSONObject();
                        if (graphResponse.getError() == null && json != null) {
                            setCurrentInterests((new Gson()).fromJson(json.toString(), FacebookInterestsModel.class));
                            mEventBus.post(new FacebookManagerEvent.GetMeLikesSuccess());
                        } else {
                            mEventBus.post(new FacebookManagerEvent.GetMeLikesFailure());
                        }
                    }
                }).executeAsync();
    }


    public FacebookUserModel getCurrentUser() {
        return mCurrentUser;
    }


    public void setCurrentUser(FacebookUserModel currentUser) {
        mCurrentUser = currentUser;
    }


    public FacebookInterestsModel getCurrentInterests() {
        return mCurrentInterests;
    }


    public void setCurrentInterests(FacebookInterestsModel currentInterests) {
        mCurrentInterests = currentInterests;
    }


    public String getAccessToken() {
        if (AccessToken.getCurrentAccessToken() != null) {
            return AccessToken.getCurrentAccessToken().getToken();
        }

        return null;
    }


    public AccountViewModel getFacebookAccountModel(FacebookUserModel facebookUserModel) {
        AccountViewModel accountViewModel = new AccountViewModel();
        if (AccessToken.getCurrentAccessToken() != null && getAccessToken() != null) {
            accountViewModel.setAccessToken(getAccessToken());
            accountViewModel.setProviderUserId(AccessToken.getCurrentAccessToken().getUserId());
            accountViewModel
                    .setProviderProfileUrl("http://facebook.com/" + AccessToken.getCurrentAccessToken().getUserId());
        }
        fillEmail(facebookUserModel, accountViewModel);
        fillNames(facebookUserModel, accountViewModel);
        fillGender(facebookUserModel, accountViewModel);
        fillDate(facebookUserModel, accountViewModel);
        accountViewModel.setServiceType(ServiceTypeEnum.Undefined);
        accountViewModel.setProvider(ProviderEnum.Facebook);
        accountViewModel.setAccessTokenSecret("");
        accountViewModel.setImageUrl("");
        accountViewModel.setReferrer("");

        return accountViewModel;
    }


    private void fillEmail(FacebookUserModel facebookUserModel, AccountViewModel accountViewModel) {
        if (facebookUserModel.getEmail() != null) {
            accountViewModel.setEmail(facebookUserModel.getEmail());
        }
    }


    private void fillNames(FacebookUserModel facebookUserModel, AccountViewModel accountViewModel) {
        if (facebookUserModel.getName() != null) {
            accountViewModel.setFirstName(StringsUtil.getFirstName(facebookUserModel.getName()));
            accountViewModel.setLastName(StringsUtil.getLastName(facebookUserModel.getName()));
        }
    }


    private void fillGender(FacebookUserModel facebookUserModel, AccountViewModel accountViewModel) {
        if (facebookUserModel.getGender() != null) {
            String gender = prepearGender(facebookUserModel.getGender());
            accountViewModel.setGender(GenderEnum.valueOf(StringsUtil.capitalizeFirst(gender)));
        }
    }


    private String prepearGender(String gender) { //this hack use for correct old facebook users that still have gender (hidden)
        String actualGender = gender;
        if(gender.contains(" ")) {
            actualGender = gender.substring(0, gender.indexOf(" "));
        }
        return actualGender;
    }


    private void fillDate(FacebookUserModel facebookUserModel, AccountViewModel accountViewModel) {
        if (facebookUserModel.getBirthday() != null) {
            java.util.Calendar calendar = DateUtil.parseDate("MM/dd/yyyy", facebookUserModel.getBirthday().trim());
            accountViewModel.setBirthDay(calendar.get(Calendar.DAY_OF_MONTH));
            accountViewModel.setBirthMonth(calendar.get(Calendar.MONTH) + 1);
            accountViewModel.setBirthYear(calendar.get(Calendar.YEAR));
        }
    }


    private void sdkInitialize() {
        FacebookSdk.sdkInitialize(mContext);
    }


    private void initComponentsToLogin() {
        //Uncomment for debug purposes
        //initFacebookLog();
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookLoginCallback());
    }


    private Throwable prepareError(FacebookRequestError error) {
        if (error != null) {
            return error.getException();
        }
        return new Exception(mContext.getString(R.string.facebook_error));
    }


    private GraphRequest prepareMeReguest() {
        Bundle params = new Bundle();
        params.putString(FIELDS, GET_USER_FIELDS);
        return new GraphRequest(AccessToken.getCurrentAccessToken(), ME, params, HttpMethod.GET);
    }


    private void initFacebookLog() {
        FacebookSdk.setIsDebugEnabled(true);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.GRAPH_API_DEBUG_INFO);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.REQUESTS);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.GRAPH_API_DEBUG_WARNING);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_RAW_RESPONSES);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.DEVELOPER_ERRORS);
    }


    private class FacebookLoginCallback implements FacebookCallback<LoginResult> {

        @Override
        public void onSuccess(LoginResult loginResult) {
            if (mLoginListener == null) return;
            if (loginResult.getRecentlyGrantedPermissions() != null && loginResult.getRecentlyGrantedPermissions()
                    .containsAll(mRequiredPermissions)) {
                mLoginListener.onSuccess();
            } else {
                mLoginListener.onError(LoginError.NOT_ALL_PERMISSIONS_GRANTED, loginResult.getRecentlyDeniedPermissions());
            }
        }


        @Override
        public void onCancel() {
            if (mLoginListener != null) {
                mLoginListener.onError(LoginError.CANCELED_BY_USER, null);
            }
        }


        @Override
        public void onError(FacebookException e) {
            mLoginListener.onError(LoginError.FACEBOOK_ERROR, null);
        }
    }
}
