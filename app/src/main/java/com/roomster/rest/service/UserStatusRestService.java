package com.roomster.rest.service;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.TokenManager;
import com.roomster.rest.api.UserStatusApi;
import com.roomster.rest.model.BillingAvailableModel;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserStatusRestService {

    @Inject
    TokenManager mTokenManager;

    @Inject
    NetworkManager mNetworkManager;

    @Inject
    UserStatusApi mUserStatusApi;


    public UserStatusRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }

    public void activateUser(final RestServiceListener<String> listener) {
        if (listener != null) {
            mTokenManager.getAccessToken(new RestServiceListener<String>() {

                @Override
                public void onSuccess(String result) {
                    callActivateUser(result, listener);
                }

                @Override
                public void onFailure(int code, Throwable throwable, String msg) {
                    listener.onFailure(code, throwable, msg);
                }
            });
        }
    }

    private void callActivateUser(String auth, final RestServiceListener<String> listener) {
        Call<String> callActivateUser = mUserStatusApi.statusPut(true, auth, null, null);
        callActivateUser.enqueue(new Callback<String>() {

            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                switch (response.code()) {
                    case ResponseCode.OK:
                        if (listener != null) {
                            listener.onSuccess(response.body());
                        }
                        break;
                    case ResponseCode.BAD_REQUEST:
                    case ResponseCode.SERVER_ERROR:
                    default:
                        if (listener != null) {
                            listener.onFailure(response.code(), null, null);
                        }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                if (listener != null) {
                    listener.onFailure(-1, t, RoomsterApplication.context.getString(R.string.settings_cant_make_rest_call_to_get_subscription));
                }
            }
        });
    }

    public void checkUserStatus (final RestServiceListener<BillingAvailableModel> listener) {
        if (listener != null) {
            mTokenManager.getAccessToken(new RestServiceListener<String>() {

                @Override
                public void onSuccess(String result) {
                    callUserStatus(result, listener);
                }

                @Override
                public void onFailure(int code, Throwable throwable, String msg) {
                    listener.onFailure(code, throwable, msg);
                }
            });
        }
    }

    private void callUserStatus(String auth, final RestServiceListener<BillingAvailableModel> listener){
        Call<BillingAvailableModel> callActivateUser = mUserStatusApi.statusGet(auth, null, null);
        callActivateUser.enqueue(new Callback<BillingAvailableModel>() {

            @Override
            public void onResponse(Call<BillingAvailableModel> call, Response<BillingAvailableModel> response) {
                switch (response.code()) {
                    case ResponseCode.OK:
                        if (listener != null) {
                            listener.onSuccess(response.body());
                        }
                        break;
                    case ResponseCode.BAD_REQUEST:
                    case ResponseCode.SERVER_ERROR:
                    default:
                        if (listener != null) {
                            listener.onFailure(response.code(), null, null);
                        }
                }
            }

            @Override
            public void onFailure(Call<BillingAvailableModel> call, Throwable t) {
                if (listener != null) {
                    listener.onFailure(-1, t, RoomsterApplication.context.getString(R.string.settings_cant_make_rest_call_to_get_subscription));
                }
            }
        });
    }
}
