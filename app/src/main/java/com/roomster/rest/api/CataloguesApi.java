package com.roomster.rest.api;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface CataloguesApi {

    /**
     * Get enums/catalogues All catalogues:
     * &#39;spoken_languages,countries,states,provinces,social_networks,genders_enum,\&lt;br /&gt; \n
     * service_types,auth_providers,image_entity_types,currencies,languages,minimum_stay,apartment_types,\&lt;br /&gt;
     * \n guests,bedrooms,bathrooms,apt_size_units,sex_category,household_sex_category,zodiac_category,\&lt;br /&gt; \n
     * pets_preferred,pets_owned,apt_amenities,supported_locales,cleanliness,overnight_guests,\&lt;br /&gt; \n
     * party_habits,get_up,go_to_bed,food_preference,smoking_habits,work_schedule,smoking_preference,\&lt;br /&gt; \n
     * room_amenities,occupation,building_types&#39;.\&lt;br /&gt; \n If not passed the entire list is returned.
     *
     * @param catalogues
     *   Comma-delimited list of catalogues. Optional.
     * @param currency
     *   currency (USD, EUR, GBP etc.)
     * @param locale
     *   locale (en-US, ru-RU, ja-JP etc.)
     * @return Call<Object>
     */

    @GET("Catalogues")
    Call<Object> cataloguesGet(@Query("catalogues") String catalogues, @Header("Currency") String currency,
                               @Header("Locale") String locale);

    /**
     * Get enums/catalogues All catalogues:
     * &#39;spoken_languages,countries,states,provinces,social_networks,genders_enum,\&lt;br /&gt; \n
     * service_types,auth_providers,image_entity_types,currencies,languages,minimum_stay,apartment_types,\&lt;br /&gt;
     * \n guests,bedrooms,bathrooms,apt_size_units,sex_category,household_sex_category,zodiac_category,\&lt;br /&gt; \n
     * pets_preferred,pets_owned,apt_amenities,supported_locales,cleanliness,overnight_guests,\&lt;br /&gt; \n
     * party_habits,get_up,go_to_bed,food_preference,smoking_habits,work_schedule,smoking_preference,\&lt;br /&gt; \n
     * room_amenities,occupation,building_types&#39;.\&lt;br /&gt; \n If not passed the entire list is returned.
     *
     * @param catalogues
     *   Comma-delimited list of catalogues. Optional.
     * @param currency
     *   currency (USD, EUR, GBP etc.)
     * @param locale
     *   locale (en-US, ru-RU, ja-JP etc.)
     * @return Call<Object>
     */

    @GET("catalogues/{catalogues}")
    Call<Object> cataloguesGet_1(@Path("catalogues") String catalogues, @Header("Currency") String currency,
                                 @Header("Locale") String locale);

    @GET("Languages")
    Call<Object> catalogLanguagesGet();

}
