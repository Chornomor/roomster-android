package com.roomster.adapter;


import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.model.CatalogValueModel;
import com.roomster.utils.PetsUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;


public class PetsAdapter extends BaseAdapter {

    public interface OnPetItemClick {

        void onItemClick(CatalogValueModel model, boolean isSelected);
    }


    @Inject
    LayoutInflater layoutInflater;

    private List<CatalogValueModel> mAllPets;
    private List<CatalogValueModel> mStoredPets;
    private OnPetItemClick          mListener;
    private int                     mDefaultColor;
    private int                     mSelectedColor;


    public PetsAdapter(Context context, List<CatalogValueModel> pets, List<CatalogValueModel> storedPets) {
        if (pets != null) {
            mAllPets = new ArrayList<>(pets);
            sortPetsAlphabetically();
        } else {
            mAllPets = new ArrayList<>();
        }
        if (context != null) {
            mSelectedColor = ContextCompat.getColor(context, R.color.orange);
            mDefaultColor = ContextCompat.getColor(context, R.color.black);
        }
        if (storedPets != null) {
            mStoredPets = new ArrayList<>(storedPets);
        } else {
            mStoredPets = new ArrayList<>();
        }
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Override
    public int getCount() {
        return mAllPets.size();
    }


    @Override
    public Object getItem(int position) {
        return mAllPets.get(position);
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        final ViewHolder holder;
        if (v == null) {
            v = layoutInflater.inflate(R.layout.view_amenity, parent, false);
            holder = new ViewHolder();
            holder.mPetsView = (ImageView) v.findViewById(R.id.amenity_image);
            holder.mPetsText = (TextView) v.findViewById(R.id.amenity_text);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        final CatalogValueModel petModel = (CatalogValueModel) getItem(position);
        holder.mPetsView.setImageResource(PetsUtil.getPetImageByName(petModel.getValue()));
        holder.mPetsText.setText(petModel.getName());
        if (isInCheckedList(petModel)) {
            holder.setChecked(true);
        } else {
            holder.setChecked(false);
        }

        v.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (isInCheckedList(petModel)) {
                    removeCheckedByValue(petModel);
                    holder.setChecked(false);
                    if (mListener != null) {
                        mListener.onItemClick(petModel, false);
                    }
                } else {
                    mStoredPets.add(petModel);
                    holder.setChecked(true);
                    if (mListener != null) {
                        mListener.onItemClick(petModel, true);
                    }
                }
            }
        });
        return v;
    }


    public void setOnPetItemClickListener(OnPetItemClick listener) {
        mListener = listener;
    }


    public List<CatalogValueModel> getSelected() {
        return mStoredPets;
    }


    private void removeCheckedByValue(CatalogValueModel model) {
        if (model != null && mStoredPets != null) {
            for (CatalogValueModel checkedModel : mStoredPets) {
                if (checkedModel != null && checkedModel.getValue() == (model.getValue())) {
                    mStoredPets.remove(checkedModel);
                    return;
                }
            }
        }
    }


    private boolean isInCheckedList(CatalogValueModel model) {
        if (model != null && mStoredPets != null) {
            for (CatalogValueModel checkedModel : mStoredPets) {
                if (checkedModel != null && checkedModel.getValue() == (model.getValue())) {
                    return true;
                }
            }
        }
        return false;
    }


    private class ViewHolder {

        public ImageView mPetsView;

        public TextView mPetsText;


        void setChecked(boolean isChecked) {
            if (isChecked) {
                mPetsView.setColorFilter(mSelectedColor, PorterDuff.Mode.SRC_ATOP);
                mPetsText.setTextColor(mSelectedColor);
            } else {
                mPetsView.clearColorFilter();
                mPetsText.setTextColor(mDefaultColor);
            }
        }
    }


    private void sortPetsAlphabetically() {
        Collections.sort(mAllPets, new Comparator<CatalogValueModel>() {

            @Override
            public int compare(CatalogValueModel lhs, CatalogValueModel rhs) {
                if (lhs != null && rhs != null) {
                    return lhs.getName().compareTo(rhs.getName());
                } else {
                    return 0;
                }
            }
        });
    }
}
