package com.roomster.activity;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import com.google.android.gms.analytics.HitBuilders;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.ListingsRestServiceEvent;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.fragment.FragmentRequest;
import com.roomster.event.fragment.UserProfileFragmentEvent;
import com.roomster.fragment.DetailedListingListFragment;
import com.roomster.fragment.ListingListViewFragment;
import com.roomster.fragment.UserListingsListFragment;
import com.roomster.fragment.UserProfileFragment;
import com.roomster.manager.SearchManager;
import com.roomster.manager.UsersManager;
import com.roomster.rest.model.ListingGetViewModel;
import com.roomster.rest.model.ResultItemModel;
import com.roomster.rest.model.UserGetViewModel;
import com.roomster.rest.service.ListingsRestService;
import com.roomster.rest.service.UsersRestService;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;
import java.util.List;


/**
 * Created by "Michael Katkov" on 12/17/2015.
 */
public class UserProfileActivity extends BaseToolbarActivity {

    private static final String DETAIL_FRAGMENT_NAME = "DETAIL_FRAGMENT_NAME";
    private static final String LIST_FRAGMENT_NAME   = "LIST_FRAGMENT_NAME";
    public static final  String USER_ID              = "user_id";
    public static final  String LISTING_ID           = "listing_id";

    private Long mUserId;
    private Long mListingId;

    private UserGetViewModel    mUser;
    private MenuItem            mUserListingsMenuItem;
    private ListingGetViewModel mUserListing;

    private int     mSemaphore;
    private boolean mCanSetContent;
    private boolean mIsActivityExist;

    @Inject
    EventBus mEventBus;

    @Inject
    Context mContext;

    @Inject
    UsersRestService mUsersRestService;

    @Inject
    UsersManager mUsersManager;

    @Inject
    ListingsRestService mListingsRestService;

    @Inject
    SearchManager mSearchManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        mSemaphore = 0;
        mCanSetContent = true;
        mIsActivityExist = true;
        handleIntent();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!mEventBus.isRegistered(this)) {
            mEventBus.register(this);
        }
        mTracker.setScreenName(getString(R.string.screen_name_user_profile));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    @Override
    protected void onResumeFragments() {
        mCanSetContent = true;
        super.onResumeFragments();
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (mEventBus.isRegistered(this)) {
            mEventBus.unregister(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mIsActivityExist = false;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mCanSetContent = false;
        super.onSaveInstanceState(outState);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.user_profile, menu);
        mUserListingsMenuItem = menu.getItem(0);
        mUserListingsMenuItem.setEnabled(mUser != null);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.show_listings:
            setUserListingsFragment();
            return false;
        default:
            return super.onOptionsItemSelected(item);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ListingsRestServiceEvent.GetListingSuccess event) {
        haveListing(event.listingGetViewModel);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ListingsRestServiceEvent.GetListingFailed event) {
        haveListing(null);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(UserProfileFragmentEvent.ShowListings showListingsEvent) {
        setUserListingsFragment();
    }


    private void handleIntent() {
        mUserId = getIntent().getLongExtra(USER_ID, -1);
        // We need to get the user
        mSemaphore++;

        if (getIntent().hasExtra(LISTING_ID)) {
            // We need to get the listing
            mSemaphore++;
            mListingId = getIntent().getLongExtra(LISTING_ID, -1);
            ResultItemModel resultItemModel = mSearchManager.findItemModelByListingId(mListingId);
            if (resultItemModel != null) {
                haveListing(resultItemModel.getListing());
            } else {
                mListingsRestService.getListing(mListingId);
            }
        }

        mUser = mUsersManager.getUserById(mUserId);
        if (mUser != null) {
            // We have the user
            mSemaphore--;
            tryToSetContent();
        } else {
            mUsersRestService.getUserById(mUserId, mUserModelListener);
        }
    }


    private void setUserListingsFragment() {
        UserListingsListFragment fragment = UserListingsListFragment.newInstance(mUser);
        fragment.setOnListingClickListener(new ListingListViewFragment.OnListingClickListener() {

            @Override
            public void onClick(int index, List<ResultItemModel> models) {
                setDetailedListFragment(index, models);
            }
        });
        setContent(fragment, true, LIST_FRAGMENT_NAME);
    }


    private void setDetailedListFragment(int initialIndex, List<ResultItemModel> items) {
        setContent(DetailedListingListFragment.newInstance(items, initialIndex), true, DETAIL_FRAGMENT_NAME);
    }


    private void setContent(Fragment fragment, boolean addToBackStack, String backStackName) {
        if (addToBackStack) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fl_content, fragment)
                    .addToBackStack(backStackName).commitAllowingStateLoss();
        } else {
            super.setContent(fragment);
        }
    }


    private void haveListing(ListingGetViewModel listing) {
        mSemaphore--;
        mUserListing = listing;
        tryToSetContent();
    }


    private void haveUser(UserGetViewModel user) {
        mSemaphore--;
        mUser = user;
        mUsersManager.addUserModel(user);
        if (mUserListingsMenuItem != null) {
            mUserListingsMenuItem.setEnabled(true);
        }
        tryToSetContent();
    }


    private void tryToSetContent() {
        // We have the user and listing (if needed)
        if (mSemaphore == 0 && mCanSetContent) {
                setContent(UserProfileFragment.newInstance(mUser, mUserListing));
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(FragmentRequest.SetTitleRequest event) {
        mTitleTv.setText(event.title);
        mCounterTv.setText(event.subtitle);
    }


    @Override
    public void onBackPressed() {
        if(isFinishing()){
            return;
        }
        int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackCount > 0 && getSupportFragmentManager().getBackStackEntryAt(backStackCount - 1).getName()
                .equals(LIST_FRAGMENT_NAME)) {
            try {
                getSupportFragmentManager().popBackStackImmediate();
                tryToSetContent();
            } catch (IllegalStateException ignored) {
                // There's no way to avoid getting this if saveInstanceState has already been called.
            }
        } else {
            super.onBackPressed();
        }
    }


    private RestServiceListener<UserGetViewModel> mUserModelListener = new RestServiceListener<UserGetViewModel>() {

        @Override
        public void onSuccess(UserGetViewModel result) {
            if (mIsActivityExist) haveUser(result);
        }


        @Override
        public void onFailure(int code, Throwable throwable, String msg) {
            Toast.makeText(mContext, getString(R.string.unable_to_get_user_info), Toast.LENGTH_SHORT).show();
        }
    };
}