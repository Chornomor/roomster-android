package com.roomster.event.RestServiceEvents;

import com.roomster.event.Event;

/**
 * Created by andreybofanov on 04.08.16.
 */
public abstract class AccountRemovedEvent extends Event {
    public static class RemovedUserGot extends AccountRemovedEvent {
        public String id, message;

        public RemovedUserGot(String id, String message) {
            this.id = id;
            this.message = message;
        }
    }

    public static class RemovedUserGetFailure extends AccountRemovedEvent {
        public int code;

        public RemovedUserGetFailure(int code) {
            this.code = code;
        }
    }

    public static class RemovedUserResponde extends AccountRemovedEvent {

    }

    public static class RemovedUserRespondeFail extends AccountRemovedEvent {
        public int code;

        public RemovedUserRespondeFail(int code) {
            this.code = code;
        }
    }

}
