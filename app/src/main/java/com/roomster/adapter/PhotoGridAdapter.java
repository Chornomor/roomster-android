package com.roomster.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.rest.model.Photo;
import com.roomster.utils.ImageHelper;
import com.roomster.views.undo.ItemExtractor;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


/**
 * Created by "Michael Katkov" on 12/26/2015.
 */
public class PhotoGridAdapter extends BaseAdapter {

    private static final int    GRID_SIZE                 = 6;
    public static final  String ADD_NEW_IMAGE_PLACEHOLDER = "ADD_NEW_IMAGE_PLACEHOLDER";

    @Inject
    Context mContext;

    @Inject
    LayoutInflater mLayoutInflater;

    List<Photo> mPhotos;


    public PhotoGridAdapter(List<Photo> photos) {
        RoomsterApplication.getRoomsterComponent().inject(this);
        mPhotos = new ArrayList<>();
        mPhotos.addAll(photos);
        checkPlaceHolders();
    }


    private void checkPlaceHolders() {
        for (int i = mPhotos.size(); i < GRID_SIZE; i++) {
            Photo placeHolderPhoto = new Photo();
            placeHolderPhoto.setId(-1);
            placeHolderPhoto.setPath(ADD_NEW_IMAGE_PLACEHOLDER);
            mPhotos.add(placeHolderPhoto);
        }
    }

    Photo makePlaceholder(){
        Photo placeHolderPhoto = new Photo();
        placeHolderPhoto.setId(-1);
        placeHolderPhoto.setPath(ADD_NEW_IMAGE_PLACEHOLDER);
        return placeHolderPhoto;
    }
    @Override
    public int getCount() {
        return mPhotos.size();
    }


    @Override
    public Object getItem(int position) {
        return mPhotos.get(position);
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.grid_item_image, parent, false);
            holder = new ViewHolder();
            holder.mImageView = (ImageView) convertView.findViewById(R.id.user_image);
            holder.mActionButtonView = (ImageView) convertView.findViewById(R.id.user_image_action_button);
            holder.mProgress = (ProgressBar) convertView.findViewById(R.id.user_image_progress);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        setUpView(position, holder);

        return convertView;
    }


    private void setUpView(int position, ViewHolder holder) {
        if (mPhotos.get(position).getPath().equals(ADD_NEW_IMAGE_PLACEHOLDER)) {
            setUpPlaceHolderView(holder);
        } else {
            setUpStandardView(position, holder);
        }
    }


    private void setUpPlaceHolderView(ViewHolder holder) {
//        holder.mActionButtonView.setBackgroundResource(R.drawable.ic_image_add);
        holder.mImageView.setImageDrawable(null);
    }


    private void setUpStandardView(int position, ViewHolder holder) {
        holder.mActionButtonView.setBackgroundResource(R.drawable.ic_image_remove);
        ImageHelper.loadImageInto(mContext, mPhotos.get(position).getPath(), holder.mImageView);
    }


    public static class ViewHolder {

        public ImageView mImageView;

        public ImageView mActionButtonView;

        public ProgressBar mProgress;
    }


    public ItemExtractor extract(Photo photo){
        int position = mPhotos.indexOf(photo);
        mPhotos.remove(photo);
        mPhotos.add(makePlaceholder());
        notifyDataSetChanged();
        return new ItemExtractor(position,photo);
    }

    public void restore(ItemExtractor extractor){
        mPhotos.add(extractor.position, (Photo) extractor.object);
        if(mPhotos.get(mPhotos.size()-1).getPath().equals(ADD_NEW_IMAGE_PLACEHOLDER))
            mPhotos.remove(mPhotos.size()-1);
        notifyDataSetChanged();
    }
}
