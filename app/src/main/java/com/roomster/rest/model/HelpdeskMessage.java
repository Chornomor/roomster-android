package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import java.util.Date;


public class HelpdeskMessage {

    /**
     **/
    @SerializedName("by_user")
    private Boolean byUser = null;

    /**
     **/
    @SerializedName("author")
    private String author = null;

    /**
     **/
    @SerializedName("created")
    private Date created = null;

    /**
     **/
    @SerializedName("subject")
    private String subject = null;

    /**
     **/
    @SerializedName("body")
    private String body = null;


    public Boolean getByUser() {
        return byUser;
    }


    public void setByUser(Boolean byUser) {
        this.byUser = byUser;
    }


    public String getAuthor() {
        return author;
    }


    public void setAuthor(String author) {
        this.author = author;
    }


    public Date getCreated() {
        return created;
    }


    public void setCreated(Date created) {
        this.created = created;
    }


    public String getSubject() {
        return subject;
    }


    public void setSubject(String subject) {
        this.subject = subject;
    }


    public String getBody() {
        return body;
    }


    public void setBody(String body) {
        this.body = body;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class HelpdeskMessage {\n");

        sb.append("  byUser: ").append(byUser).append("\n");
        sb.append("  author: ").append(author).append("\n");
        sb.append("  created: ").append(created).append("\n");
        sb.append("  subject: ").append(subject).append("\n");
        sb.append("  body: ").append(body).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
