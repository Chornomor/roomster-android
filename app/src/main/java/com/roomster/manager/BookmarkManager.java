package com.roomster.manager;

import android.util.Log;

import com.roomster.application.RoomsterApplication;
import com.roomster.model.SearchCriteria;
import com.roomster.rest.model.ResultItemModel;
import com.roomster.rest.model.ResultModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by eugenetroyanskii on 25.10.16.
 */
public class BookmarkManager extends SearchManager{

    private ResultModel                 mResultModel;
    private Collection<ResultItemModel> mMapViewItems;

    private int                         mLastViewedPosition;
    private int                         mPageCount;
    private int                         mLastPageSize;

    public BookmarkManager() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public ResultItemModel removeItemById(long id){
        if(mResultModel == null || mResultModel.getItems() == null){
            return null;
        }
        ResultItemModel target = null;
        int i = 0;
        for(ResultItemModel model : mResultModel.getItems()){
            if(model.getListing().getListingId().equals(id)){
                target = model;
                break;
            }
            i++;
        }
        if(target != null) {
            mResultModel.getItems().remove(target);
            setLastViewedPosition(i);
        }
        return target;
    }

    public void clearBookmarkModel() {
        mResultModel = null;
        mPageCount = 0;
        mLastPageSize = 0;
        setLastViewedPosition(0);
    }

    public void setLastViewedPosition(int lastViewedPosition) {
        mLastViewedPosition = lastViewedPosition;
    }


    public ResultModel getResultModel() {
        return mResultModel;
    }


    public void setResultModel(ResultModel resultModel) {
        if(resultModel == null)
            return;
        List<ResultItemModel> resultItemModels = getItemsWithLocation(resultModel);
        resultModel.setItems(resultItemModels);
        mResultModel = resultModel;
        mPageCount = 1;
        mLastPageSize = resultItemModels.size();
    }


    public void addResultModel(ResultModel resultModel) {
        if (mResultModel != null && resultModel != null) {
            List<ResultItemModel> resultItemModels = getItemsWithLocation(resultModel);
            mResultModel.getItems().addAll(resultItemModels);
            mPageCount++;
            mLastPageSize = resultItemModels.size();
        }
    }


    private List<ResultItemModel> getItemsWithLocation(ResultModel resultModel) {
        List<ResultItemModel> resultItemModels = new ArrayList<>();
        for (ResultItemModel resultItemModel : resultModel.getItems()) {
            if (resultItemModel.getListing() != null && resultItemModel.getListing().getGeoLocation() != null
                    && resultItemModel.getListing().getGeoLocation().getFullAddress() != null) {
                resultItemModels.add(resultItemModel);
            }
        }
        return resultItemModels;
    }


    public ResultItemModel findItemModelByListingId(long listingId) {
        if (mResultModel != null) {
            for (ResultItemModel itemModel : mResultModel.getItems()) {
                if (itemModel.getListing().getListingId() == listingId) {
                    return itemModel;
                }
            }
        }
        if(mMapViewItems != null){
            for (ResultItemModel model : mMapViewItems){
                if (model.getListing() != null && model.getListing().getListingId() == listingId) {
                    return model;
                }
            }
        }
        return null;
    }

    public Collection<ResultItemModel> getMapViewItems() {
        return mMapViewItems;
    }


    public void setMapViewItems(Collection<ResultItemModel> mapViewItems) {
        mMapViewItems = mapViewItems;
    }

    public int getPageCount() {
        return mPageCount;
    }


    public int getCurrentResultItemsCount() {
        if (mResultModel != null && mResultModel.getItems() != null) {
            return mResultModel.getItems().size();
        } else {
            return 0;
        }
    }


}
