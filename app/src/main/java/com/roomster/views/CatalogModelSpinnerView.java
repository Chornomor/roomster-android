package com.roomster.views;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.adapter.CatalogSpinnerAdapter;
import com.roomster.model.CatalogModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class CatalogModelSpinnerView extends RelativeLayout {

    private List<CatalogModel> mCatalogModels;
    private CatalogModel       mHintCatalogModel;

    @Bind(R.id.catalog_model_spinner_title)
    TextView mTitle;

    @Bind(R.id.catalog_model_spinner)
    Spinner mSpinner;


    public CatalogModelSpinnerView(Context context) {
        super(context);
        init();
    }


    public CatalogModelSpinnerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    public CatalogModelSpinnerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    public void setCatalogModels(String title, List<CatalogModel> models) {
        if (title != null) {
            mTitle.setText(title);
        }
        if (models != null) {
            mCatalogModels.clear();
            mCatalogModels.add(mHintCatalogModel);
            mCatalogModels.addAll(models);
            CatalogSpinnerAdapter adapter = new CatalogSpinnerAdapter(getContext(), mCatalogModels);
            mSpinner.setAdapter(adapter);
        }
    }


    public void setSelectedId(Integer selectedId) {
        if (selectedId != null && mCatalogModels != null) {
            for (int i = 0; i < mCatalogModels.size(); i++) {
                if (mCatalogModels.get(i).getId() == selectedId) {
                    mSpinner.setSelection(i);
                    break;
                }
            }
        }
    }


    public CatalogModel getSelection() {
        Object selected = mSpinner.getSelectedItem();
        if (selected != null) {
            CatalogModel selectedModel = (CatalogModel) selected;
            if (selectedModel.getId() != mHintCatalogModel.getId()) {
                return selectedModel;
            }
        }
        return null;
    }


    private void init() {
        inflate(getContext(), R.layout.view_catalog_model_spinner, this);
        ButterKnife.bind(this);
        mCatalogModels = new ArrayList<>();
        mHintCatalogModel = new CatalogModel(-1, getContext().getString(R.string.spinner_default_hint));
    }
}
