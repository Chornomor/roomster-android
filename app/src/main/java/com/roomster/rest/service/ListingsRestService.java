package com.roomster.rest.service;


import com.roomster.R;
import com.roomster.activity.AddNewDetailedListingActivity;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.ListingsRestServiceEvent;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.application.ApplicationEvent;
import com.roomster.event.listing.ListingDeletedEvent;
import com.roomster.listener.AddListing;
import com.roomster.manager.MeManager;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.SettingsManager;
import com.roomster.manager.TokenManager;
import com.roomster.rest.api.ListingsApi;
import com.roomster.rest.model.ListingCreationResultModel;
import com.roomster.rest.model.ListingGetViewModel;
import com.roomster.rest.model.ListingPostViewModel;
import com.roomster.rest.model.ListingPutViewModel;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by "Michael Katkov" on 12/10/2015.
 */
public class ListingsRestService {

    @Inject
    ListingsApi mListingsApi;

    @Inject
    TokenManager mTokenManager;

    @Inject
    SettingsManager mSettingsManager;

    @Inject
    EventBus mEventBus;

    @Inject
    NetworkManager mNetworkManager;

    @Inject
    MeManager mMeManager;

    @Inject
    UserListingsRestService mUserListingRestService;

    public ListingsRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void addListing(final ListingPostViewModel model, final AddListing listener) {

        mTokenManager.getAccessToken(new RestServiceListener<String>() {
            @Override
            public void onSuccess(String result) {
                callAddListing(model, result, listener);
            }

            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                mEventBus.post(new ListingsRestServiceEvent.AddListingFailed());
            }
        });
    }


    public void callAddListing(ListingPostViewModel model, String authorization, final AddListing listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<ListingCreationResultModel> call = mListingsApi.listingsPost(model, authorization);
            call.enqueue(new Callback<ListingCreationResultModel>() {
                @Override
                public void onResponse(Call<ListingCreationResultModel> call, Response<ListingCreationResultModel> response) {
                    if (response.code() == ResponseCode.USER_CREATED) {
                        ListingCreationResultModel responseModel = response.body();
                        mEventBus.post(new ListingsRestServiceEvent.AddListingSuccess(responseModel.listingId));
                        mUserListingRestService.getUserListings(mMeManager.getMe().getId(), true);
                        listener.onAddListing();
                    } else {
                        mEventBus.post(new ListingsRestServiceEvent.AddListingFailed());
                    }
                }

                @Override
                public void onFailure(Call<ListingCreationResultModel> call, Throwable t) {
                    mEventBus.post(new ListingsRestServiceEvent.AddListingFailed());
                }
            });
        } else {
            mEventBus.post(new ListingsRestServiceEvent.AddListingFailed());
        }
    }


    public void deleteListing(final long listingId, final RestServiceListener<Object> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callDeleteListing(result, listingId, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                listener.onFailure(code, throwable, msg);
            }
        });
    }


    private void callDeleteListing(String authToken, final long listingId, final RestServiceListener<Object> listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<String> callListingDelete = mListingsApi.listingsDelete(listingId, authToken);
            callListingDelete.enqueue(new Callback<String>() {

                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    mEventBus.post(new ListingDeletedEvent(listingId));
                    listener.onSuccess(response.body());
                }


                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    listener.onFailure(-1, t, RoomsterApplication.context.getString(R.string.listing_cant_make_call_to_delete));
                }
            });
        } else {
            listener.onFailure(-1, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
        }
    }


    public void editListing(final ListingPutViewModel model) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callEditListing(model, result);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                mEventBus.post(new ListingsRestServiceEvent.EditListingFailed());
            }
        });
    }


    public void callEditListing(ListingPutViewModel model, String authorization) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<ResponseBody> call = mListingsApi.listingsPut(model, authorization);
            call.enqueue(new Callback<ResponseBody>() {

                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == ResponseCode.OK) {
                        mEventBus.post(new ListingsRestServiceEvent.EditListingSuccess());
                    } else {
                        mEventBus.post(new ListingsRestServiceEvent.EditListingFailed());
                    }
                }


                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    mEventBus.post(new ListingsRestServiceEvent.EditListingFailed());
                }
            });
        } else {
            mEventBus.post(new ListingsRestServiceEvent.EditListingFailed());
        }
    }


    public void getListing(long listingId) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<ListingGetViewModel> call = mListingsApi
              .listingsGet(listingId, mSettingsManager.getCurrency(), mSettingsManager.getLocale());
            call.enqueue(new Callback<ListingGetViewModel>() {

                @Override
                public void onResponse(Call<ListingGetViewModel> call, Response<ListingGetViewModel> response) {
                    if (response.code() == ResponseCode.OK) {
                        ListingsRestServiceEvent.GetListingSuccess success = new ListingsRestServiceEvent.GetListingSuccess();
                        success.listingGetViewModel = response.body();
                        mEventBus.post(success);
                    } else {
                        mEventBus.post(new ListingsRestServiceEvent.GetListingFailed());
                    }
                }


                @Override
                public void onFailure(Call<ListingGetViewModel> call, Throwable t) {
                    mEventBus.post(new ListingsRestServiceEvent.GetListingFailed());
                }
            });
        } else {
            mEventBus.post(new ListingsRestServiceEvent.GetListingFailed());
        }
    }

}