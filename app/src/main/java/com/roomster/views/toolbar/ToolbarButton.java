package com.roomster.views.toolbar;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.roomster.BuildConfig;
import com.roomster.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by andreybofanov on 07.09.16.
 */
public class ToolbarButton extends LinearLayout {

    ImageView icon;

    TextView text;

    int textColorActive = Color.GREEN;
    int textColorDisabled = Color.DKGRAY;

    Drawable iconDrawable;

    String title = null;

    boolean isActive = true;

    public ToolbarButton(Context context) {
        super(context);
        iconDrawable = null;
        initViews();
    }

    public ToolbarButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        readAttrs(attrs);
        initViews();
    }

    public ToolbarButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        readAttrs(attrs);
        initViews();
    }

    public void readAttrs(AttributeSet attrs){
        TypedArray arr = getContext().obtainStyledAttributes(attrs, R.styleable.ToolbarButton);
        textColorActive = arr.getColor(R.styleable.ToolbarButton_text_color_active,Color.GREEN);
        textColorDisabled = arr.getColor(R.styleable.ToolbarButton_text_color_disabled,Color.LTGRAY);
        iconDrawable = arr.getDrawable(R.styleable.ToolbarButton_icon_active);
        title = arr.getString(R.styleable.ToolbarButton_text);
    }

    public void initViews(){
//        View layout = LayoutInflater.from(getContext()).inflate(R.layout.toolbar_button,this);
        setOrientation(VERTICAL);
        icon = new ImageView(getContext());
        int size = (int) getResources().getDimension(R.dimen.size_3);
        LayoutParams params = new LayoutParams(size,size);
        params.gravity = Gravity.CENTER;
        addView(icon,params);
        text = new TextView(getContext());
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            text.setTextAppearance(getContext(), android.R.style.TextAppearance_Small);
        } else {
            text.setTextAppearance(android.R.style.TextAppearance_Small);
        }
        params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;
        addView(text,params);
        text.setSingleLine(true);
        text.setEllipsize(TextUtils.TruncateAt.MIDDLE);
        text.setText(title);
        defineDrawables();
    }

    public void setActive(boolean isActive){
        this.isActive = isActive;
        defineDrawables();
    }

    private void defineDrawables(){
        int textColor = isActive ? textColorActive : textColorDisabled;
        icon.setImageDrawable(iconDrawable);
        if(isActive){
            icon.setColorFilter(textColorActive);
        } else {
            icon.setColorFilter(Color.LTGRAY);
        }
        text.setTextColor(textColor);
    }
}
