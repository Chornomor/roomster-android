package com.roomster.rest.service;


import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.AndroidReceiptRestServiceEvent;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.TokenManager;
import com.roomster.rest.api.AndroidReceiptApi;
import com.roomster.rest.model.AndroidReceiptModel;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AndroidReceiptRestService {

    @Inject
    EventBus mEventBus;

    @Inject
    TokenManager mTokenManager;

    @Inject
    AndroidReceiptApi mAndroidReceiptApi;

    @Inject
    NetworkManager mNetworkManager;


    public AndroidReceiptRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void sendProductPaymentData(final AndroidReceiptModel model) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String token) {
                if (mNetworkManager.hasNetworkAccess()) {
                    Call<String> call = mAndroidReceiptApi.androidReceiptPost(token, model);
                    call.enqueue(new Callback<String>() {

                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            switch (response.code()) {
                                case ResponseCode.OK:
                                    mEventBus.post(new AndroidReceiptRestServiceEvent.AndroidReceiptSentSuccessEvent());
                                    break;
                                case ResponseCode.FORBIDDEN:
                                case ResponseCode.SERVER_ERROR:
                                default:
                                    mEventBus.post(new AndroidReceiptRestServiceEvent.AndroidReceiptSentFailureEvent());
                            }
                        }


                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            mEventBus.post(new AndroidReceiptRestServiceEvent.AndroidReceiptSentFailureEvent());
                        }
                    });
                } else {
                    mEventBus.post(new AndroidReceiptRestServiceEvent.AndroidReceiptSentFailureEvent());
                }
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                mEventBus.post(new AndroidReceiptRestServiceEvent.AndroidReceiptSentFailureEvent());
            }
        });
    }
}
