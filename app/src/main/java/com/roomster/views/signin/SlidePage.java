package com.roomster.views.signin;

/**
 * Created by andreybofanov on 18.07.16.
 */
public interface SlidePage {
    void setBottomMargin(int margin);
    void setHorizontalOffset(float offset);
    void setContentVisibility(boolean isVisible);
}
