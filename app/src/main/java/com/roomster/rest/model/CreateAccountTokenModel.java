package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


/**
 * Created by Bogoi.Bogdanov on 10/7/2015.
 */
public class CreateAccountTokenModel extends TokenModel {

    @SerializedName("message")
    private String message = null;


    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }

}
