package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;


public class BookmarksDeleteViewModel {

    /**
     **/
    @SerializedName("bookmark_groups")
    private List<BookmarkGroup> bookmarkGroups = null;


    public List<BookmarkGroup> getBookmarkGroups() {
        return bookmarkGroups;
    }


    public void setBookmarkGroups(List<BookmarkGroup> bookmarkGroups) {
        this.bookmarkGroups = bookmarkGroups;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class BookmarksDeleteViewModel {\n");

        sb.append("  bookmarkGroups: ").append(bookmarkGroups).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
