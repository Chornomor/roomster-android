package com.roomster.di.module;


import com.roomster.model.UserAccount;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


/**
 * A module with methods that provide objects for maintaining the user account.
 *
 * @author Bogoi.Bogdanov on 10/6/2015.
 */
@Module
public class AccountModule {

    @Singleton
    @Provides
    UserAccount provideUserAccount() {
        return new UserAccount();
    }
}
