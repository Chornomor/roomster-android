package com.roomster.event.listing;


import com.roomster.event.Event;


public class ListingDeletedEvent extends Event {

    public long listingId;


    public ListingDeletedEvent(long listingId) {
        this.listingId = listingId;
    }
}
