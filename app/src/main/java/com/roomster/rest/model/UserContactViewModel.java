package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class UserContactViewModel implements Serializable {

    /**
     **/
    @SerializedName("country_id")
    private Integer countryId = null;

    /**
     **/
    @SerializedName("country_code")
    private String countryCode = null;

    /**
     **/
    @SerializedName("phone")
    private String phone = null;

    /**
     **/
    @SerializedName("is_visible")
    private Boolean isVisible = null;


    public Integer getCountryId() {
        return countryId;
    }


    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }


    public String getCountryCode() {
        return countryCode;
    }


    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }


    public String getPhone() {
        return phone;
    }


    public void setPhone(String phone) {
        this.phone = phone;
    }


    public Boolean getIsVisible() {
        return isVisible;
    }


    public void setIsVisible(Boolean isVisible) {
        this.isVisible = isVisible;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class UserContactViewModel {\n");

        sb.append("  countryId: ").append(countryId).append("\n");
        sb.append("  countryCode: ").append(countryCode).append("\n");
        sb.append("  phone: ").append(phone).append("\n");
        sb.append("  isVisible: ").append(isVisible).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
