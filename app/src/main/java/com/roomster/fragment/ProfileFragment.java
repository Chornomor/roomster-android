package com.roomster.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.roomster.R;
import com.roomster.activity.UserListingsListActivity;
import com.roomster.application.RoomsterApplication;
import com.roomster.dialog.SocialConnectionsDialog;
import com.roomster.event.ManagersEvents.FacebookManagerEvent;
import com.roomster.event.RestServiceEvents.MeRestServiceEvent;
import com.roomster.event.updateprofle.UpdateProfileEvent;
import com.roomster.manager.FacebookManager;
import com.roomster.manager.MeManager;
import com.roomster.rest.model.SocialConnectionViewModel;
import com.roomster.rest.model.UserGetViewModel;
import com.roomster.rest.service.MeRestService;
import com.roomster.views.UserProfileView;
import com.roomster.views.toolbar.RRToolbar;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;


public class ProfileFragment extends Fragment {

    @Bind(R.id.profile_user_view)
    UserProfileView mProfileView;

    @Bind(R.id.profile_view_toolbar)
    RRToolbar mBottomToolbar;

    @Bind(R.id.profile_view_toolbar_container)
    LinearLayout mToolbarContainer;

    @Inject
    EventBus mEventBus;

    @Inject
    MeManager mMeManager;

    @Inject
    FacebookManager mFacebookManager;

    @Inject
    MeRestService mMeRestService;

    @Inject
    Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        initData();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!mEventBus.isRegistered(this)) {
            mEventBus.register(this);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mEventBus.isRegistered(this)) {
            mEventBus.unregister(this);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(MeRestServiceEvent.GetMeSuccess event) {
        mProfileView.setUserGetViewModel(mMeManager.getMe());
        initToolbar();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(MeRestServiceEvent.GetMeFailure event) {
        Toast.makeText(mContext, getString(R.string.unable_to_get_user_info), Toast.LENGTH_SHORT).show();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(FacebookManagerEvent.GetMeSuccess event) {
        mProfileView.setFacebookUserModel(mFacebookManager.getCurrentUser());
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(FacebookManagerEvent.GetMeFailure event) {
        Toast.makeText(mContext, getString(R.string.unable_to_get_user_info), Toast.LENGTH_SHORT).show();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(FacebookManagerEvent.GetMeLikesSuccess event) {
        mProfileView.setFacebookInterestsModel(mFacebookManager.getCurrentInterests());
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(FacebookManagerEvent.GetMeLikesFailure event) {
        Toast.makeText(mContext, getString(R.string.unable_to_get_user_info), Toast.LENGTH_SHORT).show();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(UpdateProfileEvent event) {
        mProfileView.setMeProfile(true);
        initData();
        mProfileView.setUserGetViewModel(mMeManager.getMe());

    }


    private void getOrRequestMe() {
        if (mMeManager.getMe() != null) {
            mProfileView.setMeProfile(true);
            mProfileView.setUserGetViewModel(mMeManager.getMe());
            initToolbar();
            mProfileView.setOnShowListingsClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), UserListingsListActivity.class);
                    startActivity(intent);
                }
            });
        } else {
            mMeRestService.requestMe();
        }
    }


    private void getOrRequestMeFacebook() {
        if (mFacebookManager.getCurrentUser() != null) {
            mProfileView.setFacebookUserModel(mFacebookManager.getCurrentUser());
        } else {
            mFacebookManager.requestMyProfile();
        }
    }


    private void getOrRequestMyInterests() {
        if (mFacebookManager.getCurrentInterests() != null) {
            mProfileView.setFacebookInterestsModel(mFacebookManager.getCurrentInterests());
        } else {
            mFacebookManager.requestMyLikes();
        }
    }


    private void initData() {
        getOrRequestMe();
        getOrRequestMeFacebook();
        getOrRequestMyInterests();
    }

    Menu actionMenu;
    private void initToolbar() {
//        mBottomToolbar.setTitle(null);
//        mToolbarMenu.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
//
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                onToolbarItemClick(item);
//                return true;
//            }
//        });
//
//        if (actionMenu == null) {
//            actionMenu = mToolbarMenu.getMenu();
//
//            MenuInflater inflater = getActivity().getMenuInflater();
//            inflater.inflate(R.menu.my_profile_contact_menu, actionMenu);
//        }
//
//        onCreateToolbarMenu(actionMenu);
//        mToolbarMenu.invalidate();
        mBottomToolbar.setupFromMeUser(mMeManager.getMe());
        mBottomToolbar.setListener(new RRToolbar.OnActionListener() {
            @Override
            public void onRRToolbarActionClick(int id) {
                onToolbarItemClick(id);
            }
        });
        mBottomToolbar.setVisibility(View.GONE);
        mProfileView.setPanelSlidingListener(new ToolbarSlider());
    }


    private void onToolbarItemClick(int id) {
        UserGetViewModel user = mMeManager.getMe();
        switch (id) {
            case R.id.contact_action_social:
                showSocialConnectionsDialog(user);
                break;
            default:
                break;
        }
    }


    private void onCreateToolbarMenu(Menu menu) {
        UserGetViewModel user = mMeManager.getMe();

        boolean hasSocialConnections = user.getSocialConnections() != null && !user.getSocialConnections().isEmpty()
                && user.getIsVisibleSocialConnections();
        mToolbarContainer.setVisibility(hasSocialConnections ? View.VISIBLE : View.GONE);
        MenuItem actionSocial = menu.findItem(R.id.contact_action_social);
        if (actionSocial != null) {
            actionSocial.setVisible(hasSocialConnections);
        }
    }


    private void showSocialConnectionsDialog(UserGetViewModel user) {
        List<SocialConnectionViewModel> connections = user.getSocialConnections();
        new SocialConnectionsDialog(getContext(), connections, user.getFirstName()).show();
    }


    private class ToolbarSlider implements SlidingUpPanelLayout.PanelSlideListener {

        private int mToolbarHeight;


        private ToolbarSlider() {
//            mToolbarHeight = (int) getActivity().getResources().getDimension(R.dimen.detailed_view_toolbar_height_with_border);
            mToolbarHeight = 0;
        }


        @Override
        public void onPanelSlide(View panel, float slideOffset) {
//            slideToolbar(slideOffset);
        }


        @Override
        public void onPanelCollapsed(View panel) {
//            slideToolbar(0);
        }


        @Override
        public void onPanelExpanded(View panel) {
//            slideToolbar(1);
        }


        @Override
        public void onPanelAnchored(View panel) {
            // nothing to do
        }


        @Override
        public void onPanelHidden(View panel) {
            // nothing to do
        }


        private void slideToolbar(float offset) {
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mToolbarContainer.getLayoutParams();
            params.height = (int) (mToolbarHeight * offset);
            mToolbarContainer.setLayoutParams(params);
        }
    }
}