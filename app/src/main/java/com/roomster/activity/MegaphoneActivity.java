package com.roomster.activity;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.edmodo.rangebar.RangeBar;
import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.DiscoveryConstants;
import com.roomster.constants.IntentExtras;
import com.roomster.constants.ListingType;
import com.roomster.enums.GenderEnum;
import com.roomster.enums.ServiceTypeEnum;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.RestServiceEvents.UserListingsRestServiceEvent;
import com.roomster.event.dialog.MegaphoneChooserDialogEvent;
import com.roomster.fragment.AddListingDetailedFragment;
import com.roomster.fragment.MegaphoneChooserDialogFragment;
import com.roomster.manager.CatalogsManager;
import com.roomster.manager.MeManager;
import com.roomster.manager.MegaphoneManager;
import com.roomster.model.CatalogModel;
import com.roomster.rest.model.ListingGetViewModel;
import com.roomster.rest.model.MegaphoneParamsModel;
import com.roomster.rest.model.MembersCountModel;
import com.roomster.rest.model.RangeModel;
import com.roomster.rest.service.CatalogRestService;
import com.roomster.rest.service.MegaphoneRestService;
import com.roomster.rest.service.ResponseCode;
import com.roomster.rest.service.UserListingsRestService;
import com.roomster.views.CatalogModelRadioGroup;
import com.roomster.views.RectangleWithStroke;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class MegaphoneActivity extends BaseActivity implements RectangleWithStroke.OnRectClickInterface {

    private static final long COOL_DOWN_INTERVAL = 750;
    private static final int  MOST_MEMBERS_SHOWN = 10_000;
    private int budgetRangeStep  = 10;
    private int radioButtonState = -1;

    private int mAgeRangeBarTickCount;
    private int mBudgetRangeBarTickCount;

    private long   mLastRequest;
    private String mGender;
    private String mBedrooms = null;
    private int leftAgeThumbIndex, rightAgeThumbIndex;

    private List<RectangleWithStroke> listBedroomType = new ArrayList<>();
    private List<ListingGetViewModel> userListing = new ArrayList<>();
    private AlertDialog dialog = null;

    private final String STUDIO      = "0";
    private final String BEDROOM_1   = "1";
    private final String BEDROOMS_2  = "2";
    private final String BEDROOMS_3  = "3";
    private final String BEDROOMS_4  = "4";
    private final String BEDROOMS_5  = "5";

    private boolean isTypeListSetUp = false;

    private static final int INTERESTED_TYPE = 1;
    private static final int SHOW_SEE_TYPE   = 2;
    private static final int PETS_TYPE       = 3;

    @Bind(R.id.global_megaphone_sv)
    ScrollView mScrollView;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.tv_title)
    TextView mToolbarTitle;

    @Bind(R.id.iv_back)
    ImageView mBackButton;

    @Bind(R.id.et_message)
    EditText mMessage;

    @Bind(R.id.btn_send_message)
    Button mSendMessage;

    @Bind(R.id.rb_age)
    RangeBar mAgeRangeBar;

    @Bind(R.id.rb_budget)
    RangeBar mBudgetRangeBar;

    @Bind(R.id.tv_age_min_max)
    TextView mAgeTextView;

    @Bind(R.id.tv_budget_min_max)
    TextView mBudgetTextView;

    @Bind(R.id.iv_comment)
    ImageView mComment;

    @Bind(R.id.tv_megaphone_members)
    TextView mMegaphoneMembers;

    @Bind(R.id.megaphone_members_progress_bar)
    DotProgressBar mMembersProgressBar;

    @Bind(R.id.rg_rent_or_find)
    RadioGroup mRentOrFind;

    @Bind(R.id.rg_room_or_place)
    RadioGroup mRoomOrPlace;

    @Bind(R.id.rb_rent_place)
    RadioButton mRentingRb;

    @Bind(R.id.rb_find_place)
    RadioButton mFindingRb;

    @Bind(R.id.rb_place)
    RadioButton mEntirePlaceRb;

    @Bind(R.id.rb_room)
    RadioButton mPrivateRoomRb;

    @Bind(R.id.megaphone_sex_find_room)
    CatalogModelRadioGroup mMegaphoneFindRoom;

    @Bind(R.id.megaphone_sex_rent_room)
    CatalogModelRadioGroup mMegaphoneRentRoom;

    @Bind(R.id.megaphone_bedrooms_layout)
    LinearLayout mBedroomLayout;

    @Bind(R.id.cb_bedroom0)
    RectangleWithStroke cbBedroom0;

    @Bind(R.id.cb_bedroom1)
    RectangleWithStroke cbBedroom1;

    @Bind(R.id.cb_bedroom2)
    RectangleWithStroke cbBedroom2;

    @Bind(R.id.cb_bedroom3)
    RectangleWithStroke cbBedroom3;

    @Bind(R.id.cb_bedroom4)
    RectangleWithStroke cbBedroom4;

    @Bind(R.id.cb_bedroom5)
    RectangleWithStroke cbBedroom5;

    @Bind(R.id.megaphone_age_layout)
    RelativeLayout mAgeLayout;

    @Inject
    MegaphoneRestService mMegaphoneService;

    @Inject
    MeManager mMeManager;

    @Inject
    CatalogsManager mCatalogsManager;

    @Inject
    CatalogRestService mCatalogRestService;

    @Inject
    Tracker mTracker;

    @Inject
    MegaphoneManager mMegaphoneManager;

    @Inject
    UserListingsRestService mUserListingsRestService;

    @Override
    protected int getMainContentLayout() {
        return R.layout.activity_megaphone;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        ButterKnife.bind(this);
        setUpScrollView();
        calculateBudgetRangeStep();
        initViews();
    }


    private void setUpScrollView() {
        mScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                onClickAway();
                return false;
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        storeBedrooms();
    }

    protected void onClickAway() {
        //hide soft keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }


    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName(getString(R.string.screen_name_megaphone));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        if (dialog != null && dialog.isShowing()) dialog.dismiss();
        initServiceType();
        getMembers();

    }


    @OnClick(R.id.iv_back)
    void onToolbarBackClicked() {
        onBackPressed();
    }

    private void saveRadioButtonState() {
        mMegaphoneManager.saveType(getListingType());
    }

    private void cleanMessageField(){
        mMessage.setText("");
    }

    @OnClick(R.id.btn_send_message)
    void onSendMessageClicked() {
        MegaphoneParamsModel model = new MegaphoneParamsModel();

        String message = mMessage.getText().toString();
        if (message.isEmpty()) {
            Toast.makeText(this, R.string.megaphone_empty_message_error, Toast.LENGTH_SHORT).show();
            return;
        }

        model.setMessage(mMessage.getText().toString());

        RangeModel age = new RangeModel();
        age.setMin(getMinAge());
        age.setMax(getMaxAge());
        model.setAge(age);

        RangeModel budget = new RangeModel();
        budget.setMin(mMegaphoneManager.getMinBudget());
        budget.setMax(mMegaphoneManager.getMaxBudget());
        model.setBudget(budget);

        model.setServiceType(getServiceTypeBasedOnSelection());
        String gender = mMeManager.getMe() != null && mMeManager.getMe().getGender() != null ?
                mMeManager.getMe().getGender().name() : GenderEnum.Unknown.name();
        model.setSex(gender);

        mMegaphoneService.sendMegaphoneMessage(model, new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                Toast.makeText(MegaphoneActivity.this, getString(R.string.megaphone_message_sent) + mMessage.getText(),
                        Toast.LENGTH_SHORT).show();
                mMessage.setText("");
                mMegaphoneMembers.setText(R.string.megaphone_number_of_members);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                Toast.makeText(MegaphoneActivity.this, R.string.megaphone_message_failed_to_send, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @OnClick(R.id.iv_comment)
    void onCommentClicked() {
        FragmentManager fm = this.getSupportFragmentManager();
        MegaphoneChooserDialogFragment dialogFragment = MegaphoneChooserDialogFragment.getMegaphoneChooserDialogFragment(getListingType());
        dialogFragment.show(fm, "Pick short text");
    }


    @OnClick({ R.id.rb_room, R.id.rb_place, R.id.rb_find_place, R.id.rb_rent_place })
    void onRentOrFindClick() {
        cleanMessageField();
        setLayout();
        invoiceDelayedServiceCall();
    }


    private void setLayout (){
        String typeString = getListingTypeString();
        if (typeString.equals(getString(R.string.megaphone_have_room)))        setRentRoom();
        if (typeString.equals(getString(R.string.megaphone_looking_for_room))) setFindRoom();
        if (typeString.equals(getString(R.string.megaphone_have_apartment)))   if (mBedroomLayout.getVisibility() == View.GONE)setRentPlace();
        if (typeString.equals(getString(R.string.megaphone_looking_for_apartment))) if (mBedroomLayout.getVisibility() == View.GONE) setFindPlace();

    }

    private void setFindRoom (){
        mMegaphoneFindRoom.setVisibility(View.VISIBLE);
        mMegaphoneRentRoom.setVisibility(View.GONE);
        mBedroomLayout.setVisibility(View.GONE);
        mAgeLayout.setVisibility(View.VISIBLE);
    }

    private void setFindPlace (){
        setRentPlace();
    }

    private void setRentRoom (){
        mMegaphoneFindRoom.setVisibility(View.GONE);
        mMegaphoneRentRoom.setVisibility(View.VISIBLE);
        mAgeLayout.setVisibility(View.VISIBLE);
        mBedroomLayout.setVisibility(View.GONE);
    }

    private void setRentPlace (){
        mMegaphoneFindRoom.setVisibility(View.GONE);
        mMegaphoneRentRoom.setVisibility(View.GONE);
        mBedroomLayout.setVisibility(View.VISIBLE);
        mAgeLayout.setVisibility(View.GONE);
        setMegaphoneRentBedrooms();
    }

    private void setSexLayoutFindRoom() {
        int megaphoneSex = mMegaphoneManager.getMegaphoneIntSexFind();
        List<CatalogModel> options = mCatalogsManager.getHouseholdSexList();
        options.add(new CatalogModel(-1, RoomsterApplication.context.getString(R.string.discovery_household_sex_everyone)));

        mMegaphoneFindRoom.setOptions(getString(R.string.discovery_sex), options, megaphoneSex);
        mMegaphoneFindRoom.setTitleTextSize(R.dimen.text_size_4);

        mMegaphoneFindRoom.setOnCheckedChangedListener(new CatalogModelRadioGroup.OnCheckedChangedListener() {

            @Override
            public void onCheckedChanged(CatalogModel option) {
                mMegaphoneManager.storeMegaphoneIntSexFind(option.getId());
                invoiceDelayedServiceCall();
            }
        });
    }

    private void setSexLayoutRentRoom() {
        int megaphoneSex = mMegaphoneManager.getMegaphoneIntSexRent();
        List<CatalogModel> options = mCatalogsManager.getSexCategoriesList();
        mMegaphoneRentRoom.setOptions(getString(R.string.discovery_sex), options, megaphoneSex);
        mMegaphoneRentRoom.setTitleTextSize(R.dimen.text_size_4);

        mMegaphoneRentRoom.setOnCheckedChangedListener(new CatalogModelRadioGroup.OnCheckedChangedListener() {

            @Override
            public void onCheckedChanged(CatalogModel option) {
                mMegaphoneManager.storeMegaphoneIntSexRent(option.getId());
                invoiceDelayedServiceCall();
            }
        });
    }


    private void setMegaphoneRentBedrooms(){
        if (mCatalogsManager.getBedroomsList() == null) {
            mCatalogRestService.loadCatalogs();
        } else {
            String[] types = mMegaphoneManager.getBedApartment().split(",");
            for (int i = 0; i < types.length; ++i) {
                updateBedroom(types[i]);
            }
        }
        mBedrooms = mMegaphoneManager.getBedApartment();
        setNamesInRectangles(mCatalogsManager.getBedroomsList(), listBedroomType);
    }

    private void storeBedrooms() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < listBedroomType.size(); ++i) {
            if(listBedroomType.get(i).isChecked()) {
                if(i != listBedroomType.size() - 1) {
                    sb.append(i + ",");
                } else {
                    sb.append(i);
                }
            }
        }
        mMegaphoneManager.storeBedApartment(sb.toString());
    }


    private void setNamesInRectangles(List<CatalogModel> models, List<RectangleWithStroke> listRectangles) {
        for(int i = 0; i < models.size(); ++i) {
            listRectangles.get(i).setText(models.get(i).getName());
        }
    }


    private void updateBedroom(String index) {
        switch (index) {
            case STUDIO:
                updateRectWithStroke(cbBedroom0);
                break;
            case BEDROOM_1:
                updateRectWithStroke(cbBedroom1);
                break;
            case BEDROOMS_2:
                updateRectWithStroke(cbBedroom2);
                break;
            case BEDROOMS_3:
                updateRectWithStroke(cbBedroom3);
                break;
            case BEDROOMS_4:
                updateRectWithStroke(cbBedroom4);
                break;
            case BEDROOMS_5:
                updateRectWithStroke(cbBedroom5);
                break;
        }
    }

    private void addBedsToList() {
        listBedroomType.add(cbBedroom0);
        listBedroomType.add(cbBedroom1);
        listBedroomType.add(cbBedroom2);
        listBedroomType.add(cbBedroom3);
        listBedroomType.add(cbBedroom4);
        listBedroomType.add(cbBedroom5);

    }


    private void updateRectWithStroke(RectangleWithStroke rect) {
        rect.setChecked(true);
        rect.setTextColor(getResources().getColor(R.color.orange));
    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(MegaphoneChooserDialogEvent event) {
        mMessage.setText(event.chosenMessage);
    }


    /**
     * Based on the selected switches pick the correct option.
     * If the user is looking or if he rends a place.
     *
     * @return the value which corresponds to he picked configuration
     */
    public ServiceTypeEnum getServiceTypeBasedOnSelection() {
        int rentOrFindId = mRentOrFind.getCheckedRadioButtonId();
        int place = mRoomOrPlace.getCheckedRadioButtonId();

        boolean isLookingForPlace = rentOrFindId == R.id.rb_find_place;

        if (isLookingForPlace) {
            if (place == R.id.rb_place) {
                return ServiceTypeEnum.HaveApartment;
            } else
                if (place == R.id.rb_room) {
                    return ServiceTypeEnum.HaveShare;
                }
        } else {
            if (place == R.id.rb_place) {
                return ServiceTypeEnum.NeedApartment;
            } else
                if (place == R.id.rb_room) {
                    return ServiceTypeEnum.NeedRoom;
                }
        }

        return ServiceTypeEnum.Undefined;
    }


    private void initViews() {
        mToolbarTitle.setText(getString(R.string.megaphone_title));
        if (dialog != null && dialog.isShowing()) dialog.dismiss();
        setSexLayoutFindRoom();
        setSexLayoutRentRoom();
        addBedsToList();
        setBedroomsListeners();
        setUpAgeBar();
        initGender();
        initServiceType();
        setUpBudgetBar();
    }

    private void setBedroomsListeners() {
        cbBedroom0.setListener(this);
        cbBedroom1.setListener(this);
        cbBedroom2.setListener(this);
        cbBedroom3.setListener(this);
        cbBedroom4.setListener(this);
        cbBedroom5.setListener(this);
    }


    private void initServiceType() {
        radioButtonState = -1;
        if (getIntent().getExtras() != null)
        radioButtonState = getIntent().getExtras().getInt(AddNewDetailedListingActivity.TO_MEGAPHONE_ACTIVITY_TYPE, -1);

        if (radioButtonState == -1) {
            radioButtonState = mMegaphoneManager.getType();
            if (mMeManager.getMyListings() != null) {
                userListing = mMeManager.getMyListings();
                if (userListing.size() > 0){
                    if (!existListing(radioButtonState)){
                        mMegaphoneManager.saveType(userListing.get(userListing.size()-1).getServiceType());
                        initServiceType();
                    }
                }else {
                    getListings();
                    isTypeListSetUp = false;
                    return;
                }
            }
        }
            switch (radioButtonState) {
            case ListingType.FINDING_ROOM_TYPE:
                mRentOrFind.check(R.id.rb_find_place);
                mRoomOrPlace.check(R.id.rb_room);
                setFindRoom();
                break;
            case ListingType.OFFERING_ROOM_TYPE:
                mRentOrFind.check(R.id.rb_rent_place);
                mRoomOrPlace.check(R.id.rb_room);
                setRentRoom();
                break;
            case ListingType.FINDING_ENTIRE_PLACE_TYPE:
                mRentOrFind.check(R.id.rb_find_place);
                mRoomOrPlace.check(R.id.rb_place);
                setFindPlace();
                break;
            case ListingType.OFFERING_ENTIRE_PLACE_TYPE:
                mRentOrFind.check(R.id.rb_rent_place);
                mRoomOrPlace.check(R.id.rb_place);
                setRentPlace();
                break;
            default:
                mRentOrFind.check(R.id.rb_find_place);
                mRoomOrPlace.check(R.id.rb_room);
                setFindRoom();
                break;
            }
        isTypeListSetUp = true;
    }

    void getListings() {
        mUserListingsRestService.getUserListings(mMeManager.getMe().getId(), true);
    }

    @Subscribe
    public void onEventMainThread(UserListingsRestServiceEvent.GetListingsSuccess getListingsSuccess) {
        if (!isFinishing()){
            mMeManager.setMyListings(getListingsSuccess.listings);
            initServiceType();
        }
    }

    private boolean existListing(int radioButtonState) {
               for (ListingGetViewModel model: userListing){
                       if (model.getServiceType() == radioButtonState) return true;
                    }
                return false;
           }
    private void setUpAgeBar() {
        mAgeRangeBarTickCount = DiscoveryConstants.MAX_AGE - DiscoveryConstants.MIN_AGE + 1;
        mAgeRangeBar.setTickCount(mAgeRangeBarTickCount);
        leftAgeThumbIndex  = mMegaphoneManager.getAgeMin() - DiscoveryConstants.MIN_AGE;
        rightAgeThumbIndex = mMegaphoneManager.getAgeMax() - DiscoveryConstants.MIN_AGE;
        mAgeRangeBar.setThumbIndices(leftAgeThumbIndex, rightAgeThumbIndex);
        onAgeChanged(mMegaphoneManager.getAgeMin(), mMegaphoneManager.getAgeMax());

        mAgeRangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int minThumbIndex, int maxThumbIndex) {
                //There is a known bug in the RangeBar  - sometimes the indexes go beyond the set range!
                if (minThumbIndex >= 0 && maxThumbIndex < mAgeRangeBarTickCount) {
                    updateAgeText(minThumbIndex, maxThumbIndex);
                } else {
                    mAgeRangeBar.setThumbIndices(0, mAgeRangeBarTickCount - 1);
                    updateAgeText(0, mAgeRangeBarTickCount - 1);
                }
            }
        });
        mAgeRangeBar.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    invoiceDelayedServiceCall();
                }
                return false;
            }
        });
    }

    private void setUpBudgetBar() {
        mBudgetRangeBarTickCount = ((DiscoveryConstants.MAX_BUDGET - DiscoveryConstants.MIN_BUDGET) / budgetRangeStep) + 1;
        mBudgetRangeBar.setTickCount(mBudgetRangeBarTickCount);
        mBudgetRangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int minThumbIndex, int maxThumbIndex) {
                if (minThumbIndex >= 0 && maxThumbIndex < mBudgetRangeBarTickCount) {
                    updateBudgetText(minThumbIndex, maxThumbIndex);
                } else {
                    setStoredBudget();
                }
            }
        });
        mBudgetRangeBar.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    invoiceDelayedServiceCall();
                }
                return false;
            }
        });
        setStoredBudget();
    }

    private void setStoredBudget() {
        int minBudget = mMegaphoneManager.getMinBudget();
        int maxBudget = mMegaphoneManager.getMaxBudget();
        if (minBudget < DiscoveryConstants.MIN_BUDGET) {
            mMegaphoneManager.storeMinBudget(DiscoveryConstants.MIN_BUDGET);
            minBudget = DiscoveryConstants.MIN_BUDGET;
        }
        if (maxBudget > DiscoveryConstants.MAX_BUDGET) {
            mMegaphoneManager.storeMaxBudget(DiscoveryConstants.MAX_BUDGET);
            maxBudget = DiscoveryConstants.MAX_BUDGET;
        }

        int leftValue = (minBudget - DiscoveryConstants.MIN_BUDGET) / budgetRangeStep;
        int rightValue = ((maxBudget - DiscoveryConstants.MIN_BUDGET) / budgetRangeStep);
        if (leftValue >= 0 || rightValue < mBudgetRangeBarTickCount) {
            try {
                mBudgetRangeBar.setThumbIndices(leftValue, rightValue);
                updateBudgetText(leftValue, rightValue);
                invoiceDelayedServiceCall();
            } catch (IllegalArgumentException e) {
                //Nothing to do
            }
        }
    }


    private void calculateBudgetRangeStep(){
        budgetRangeStep = DiscoveryConstants.prepareBudgetRangeStep();
    }


    private void initGender() {
        if (mMeManager.getMe() != null && mMeManager.getMe().getGender() != null) {
            mGender = mMeManager.getMe().getGender().name();
        } else {
            GenderEnum.Unknown.name();
        }
    }


    private void updateBudgetText(int leftIndex, int rightIndex) {
        int leftValue = (leftIndex * budgetRangeStep) + DiscoveryConstants.MIN_BUDGET;
        int rightValue = (rightIndex * budgetRangeStep) + DiscoveryConstants.MIN_BUDGET;
        onBudgetChanged(leftValue, rightValue);
        saveBudgetChoice(leftValue, rightValue);
    }


    private void updateAgeText(int leftIndex, int rightIndex) {

        int leftValue = leftIndex + DiscoveryConstants.MIN_AGE;
        int rightValue = rightIndex + DiscoveryConstants.MIN_AGE;

        onAgeChanged(leftValue, rightValue);
        saveAgeChoice(leftValue, rightValue);
    }


    private int getMinAge() {
        return mAgeRangeBar.getLeftIndex() + DiscoveryConstants.MIN_AGE;
    }


    private int getMaxAge() {
        return mAgeRangeBar.getRightIndex() + DiscoveryConstants.MIN_AGE;
    }


    private void onAgeChanged(int leftValue, int rightValue) {
        String minValue = String.valueOf(leftValue);
        String maxValue = String.valueOf(rightValue);
        String str = String.format(getString(R.string.megaphone_age_min_max), minValue, maxValue);

        mAgeTextView.setText(str);
    }

    private void saveAgeChoice(int minValue, int maxValue){
        mMegaphoneManager.storeMegaphoneAgeMax(maxValue);
        mMegaphoneManager.storeMegaphoneAgeMin(minValue);
    }

    private void saveBudgetChoice(int minValue, int maxValue){
        mMegaphoneManager.storeMaxBudget(maxValue);
        mMegaphoneManager.storeMinBudget(minValue);
    }

    private void onBudgetChanged(int leftValue, int rightValue) {
        String minValue = String.valueOf(leftValue);
        String maxValue = String.valueOf(rightValue);
        String str = String.format(getString(R.string.megaphone_budget_min_max), minValue, maxValue);

        mBudgetTextView.setText(str);
    }

    private void getMembers() {
        if (!isTypeListSetUp) return;
        showMembersLoading();
        final String serviceType = getServiceTypeBasedOnSelection().name();
        int minAge = DiscoveryConstants.MIN_AGE;
        int maxAge = DiscoveryConstants.MAX_AGE;
        String mHouseholdSex = getHouseholdSexIfNeed();
        mGender = getGenderIfNeed();
        mBedrooms = getBadroomsIfNeed();
        if (mAgeLayout.getVisibility() == View.VISIBLE) {
            minAge = getMinAge();
            maxAge = getMaxAge();
        }

        mMegaphoneService.getMembers(serviceType, mMegaphoneManager.getMinBudget(), mMegaphoneManager.getMaxBudget(), minAge, maxAge, mGender, mBedrooms,
                mMessage.getText().toString(), mHouseholdSex, new RestServiceListener<MembersCountModel>() {

                    @Override
                    public void onSuccess(MembersCountModel result) {
                        super.onSuccess(result);
                        if (result.count < MOST_MEMBERS_SHOWN) {
                            mMegaphoneMembers.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_4));
                            mMegaphoneMembers.setText(String.valueOf(result.count));
                        } else {
                            mMegaphoneMembers.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_3));
                            mMegaphoneMembers.setText(getCountMembers(result.count));
                        }
                        hideMembersLoading();
                        saveRadioButtonState();
                    }

                    @Override
                    public void onFailure(int code, Throwable throwable, String msg) {
                        super.onFailure(code, throwable, msg);
                        mMegaphoneMembers.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_4));

                        if (code == ResponseCode.FORBIDDEN) {
                            if (!isFinishing()) {
                                if (serviceType.equals(getServiceTypeBasedOnSelection().name()))
                                showAddNewListingDialog();
                            }
                        } else {
                            mMegaphoneMembers.setText(R.string.megaphone_number_of_members);
                            String toastText = msg != null ? msg : getString(R.string.megaphone_server_error_please_excuse_us);
                            Toast.makeText(MegaphoneActivity.this, toastText, Toast.LENGTH_SHORT).show();
                            saveRadioButtonState();
                        }
                        hideMembersLoading();
                    }
                });
    }

    private String getBadroomsIfNeed() {
        String bedrooms = null;
        if (mBedroomLayout.getVisibility() == View.VISIBLE) {
            bedrooms = mMegaphoneManager.getBedApartment();
        }
        return bedrooms;
    }

    private String getGenderIfNeed() {
        String gender = null;
        if (mMegaphoneFindRoom.getVisibility() == View.VISIBLE) {
            gender = mMeManager.getMe().getGender().toString();
        }
        if (mMegaphoneRentRoom.getVisibility() == View.VISIBLE) {
            gender = Integer.toString(mMegaphoneManager.getMegaphoneIntSexRent());
            if (gender.equals("-1")) {
                gender = null;
            }
        }
        return gender;
    }

    private String getHouseholdSexIfNeed() {
        String householdSex = null;
        if (mMegaphoneRentRoom.getVisibility() == View.VISIBLE) {
            householdSex = mMeManager.getMe().getGender().toString();
        }
        if (mMegaphoneFindRoom.getVisibility() == View.VISIBLE) {
            householdSex = Integer.toString(mMegaphoneManager.getMegaphoneIntSexFind());
            if (householdSex.equals("-1")) {
                householdSex = null;
            }
        }
        return householdSex;
    }

    private String getCountMembers(int count) {
        double countMembers = ((float)count)/ 1000;
        NumberFormat formatter = NumberFormat.getNumberInstance();
        formatter.setMinimumFractionDigits(1);
        formatter.setMaximumFractionDigits(1);

        return String.valueOf(formatter.format(countMembers)) + "K";
    }


    private void showAddNewListingDialog() {
        dialog = new AlertDialog.Builder(this)
                .setMessage(getString(R.string.megaphone_no_matching_listing_dialog_message, getListingTypeString()))
                .setNegativeButton(R.string.dialog_cancel_button, null)
                .setPositiveButton(R.string.button_yes, null).create();
        if (dialog.isShowing()) dialog.dismiss();
        dialog.show();

        Button positiveBtn = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int listingType = getListingType();
                Intent intent;
                if (listingType != -1) {
                    intent = new Intent(MegaphoneActivity.this, AddNewDetailedListingActivity.class);
                    intent.putExtra(AddListingDetailedFragment.LISTING_TYPE, listingType);
                } else {
                    intent = new Intent(MegaphoneActivity.this, AddNewListingActivity.class);
                }

                intent.putExtra(IntentExtras.WITH_BACK_ARROW, true);
                intent.putExtra(AddNewDetailedListingActivity.FROM_MEGAPHONE_ACTIVITY, true);
                startActivity(intent);
                finish();
            }
        });

        Button negativeBtn = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        negativeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initServiceType();
                dialog.dismiss();
            }
        });


        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                initServiceType();
            }
        });

    }


    private String getListingTypeString() {
        switch (getServiceTypeBasedOnSelection()) {
        case Undefined:
            return "";
        case NeedRoom:
            return getString(R.string.megaphone_have_room);
        case HaveShare:
            return getString(R.string.megaphone_looking_for_room);
        case NeedApartment:
            return getString(R.string.megaphone_have_apartment);
        case HaveApartment:
            return getString(R.string.megaphone_looking_for_apartment);
        default:
            return "";
        }
    }


    private int getListingType() {
        switch (getServiceTypeBasedOnSelection()) {
        case Undefined:
            return -1;
        case NeedRoom:
            return ListingType.OFFERING_ROOM_TYPE;
        case HaveShare:
            return ListingType.FINDING_ROOM_TYPE;
        case NeedApartment:
            return ListingType.OFFERING_ENTIRE_PLACE_TYPE;
        case HaveApartment:
            return ListingType.FINDING_ENTIRE_PLACE_TYPE;
        }
        return -1;
    }


    private void invoiceDelayedServiceCall() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - mLastRequest > COOL_DOWN_INTERVAL) {
            mLastRequest = currentTime;
             getMembers();
        }
    }


    private void showMembersLoading() {
        mMembersProgressBar.setVisibility(View.VISIBLE);
        mMegaphoneMembers.setVisibility(View.GONE);
    }


    private void hideMembersLoading() {
        mMembersProgressBar.setVisibility(View.GONE);
        mMegaphoneMembers.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRectClick() {
        storeBedrooms();
        mBedrooms = mMegaphoneManager.getBedApartment();
        invoiceDelayedServiceCall();
    }
}