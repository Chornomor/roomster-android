package com.roomster.utils;


import android.os.Build;
import android.util.Log;

import com.roomster.BuildConfig;

import java.io.IOException;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


/**
 * Created by michaelkatkov on 1/19/16.
 */
public class UserAgentInterceptor implements Interceptor {

    private LogUtil logger = new LogUtil();

    private static final String USER_AGENT_HEADER = "User-Agent";
    private static final String USER_AGENT_FORMAT = "Roomster/%d(Android;%s;%s)";


    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();
        logger.loggerForRequestDebug(originalRequest);
        Request requestWithUserAgent = originalRequest.newBuilder().header(USER_AGENT_HEADER, buildUserAgent()).build();
        Response response = chain.proceed(requestWithUserAgent);
        logger.loggerForRsponseDebug(response);
        if(response != null){
            logger.errorHandler(response.code(), response.body());
        }
        return response;
    }



    private String buildUserAgent() {
        String androidOS = Build.VERSION.RELEASE;
        int versionCode = BuildConfig.VERSION_CODE;
        String deviceName = Build.MANUFACTURER + " " + Build.MODEL;
        return String.format(Locale.US, USER_AGENT_FORMAT, versionCode, androidOS, deviceName);
    }
}
