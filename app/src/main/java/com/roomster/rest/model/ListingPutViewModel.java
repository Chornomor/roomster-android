package com.roomster.rest.model;


import android.util.Log;

import com.facebook.share.model.ShareModel;
import com.google.gson.annotations.SerializedName;

import com.roomster.enums.ServiceTypeEnum;

import java.util.ArrayList;
import java.util.List;


public class ListingPutViewModel {

    /**
     **/
    @SerializedName("listing_id")
    private Long listingId = null;

    /**
     **/
    @SerializedName("headline")
    private String headline = null;

    /**
     **/
    @SerializedName("description")
    private String description = null;

    /**
     **/
    @SerializedName("is_active")
    private Boolean isActive = null;

    /**
     **/
    @SerializedName("share_details")
    private ShareDetails shareDetails = null;

    /**
     **/
    @SerializedName("questions")
    private List<String> questions = null;

    /**
     **/
    @SerializedName("tags")
    private List<String> tags = null;

    /**
     **/
    @SerializedName("apartment_amenities")
    private List<Long>   apartmentAmenities = null;
    /**
     **/
    @SerializedName("service_type")
    private ServiceTypeEnum serviceType        = null;

    /**
     **/
    @SerializedName("full_address")
    private String        mFullAddress  = null;
    /**
     **/
    @SerializedName("calendar")
    private Calendar      calendar      = null;
    /**
     **/
    @SerializedName("rates")
    private Rates         rates         = null;
    /**
     **/
    @SerializedName("need_apartment")
    private NeedApartment needApartment = null;
    /**
     **/
    @SerializedName("have_apartment")
    private HaveApartment haveApartment = null;


    public Long getListingId() {
        return listingId;
    }


    public void setListingId(Long listingId) {
        this.listingId = listingId;
    }


    public String getHeadline() {
        return headline;
    }


    public void setHeadline(String headline) {
        this.headline = headline;
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }


    public Boolean getIsActive() {
        return isActive;
    }


    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }


    public ShareDetails getShareDetails() {
        return shareDetails;
    }


    public void setShareDetails(ShareDetails shareDetails) {
        this.shareDetails = shareDetails;
    }


    public List<String> getQuestions() {
        return questions;
    }


    public void setQuestions(List<String> questions) {
        this.questions = questions;
    }


    public List<String> getTags() {
        return tags;
    }


    public void setTags(List<String> tags) {
        this.tags = tags;
    }


    public List<Long> getApartmentAmenities() {
        return apartmentAmenities;
    }


    public void setApartmentAmenities(List<Long> apartmentAmenities) {
        if (apartmentAmenities != null) {
            this.apartmentAmenities = new ArrayList<>(apartmentAmenities);
        } else {
            this.apartmentAmenities = new ArrayList<>();
        }
    }


    public ServiceTypeEnum getServiceType() {
        return serviceType;
    }


    public void setServiceType(ServiceTypeEnum serviceType) {
        this.serviceType = serviceType;
    }


    public Calendar getCalendar() {
        if(calendar == null)
            calendar = new Calendar();
        return calendar;
    }


    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }


    public Rates getRates() {
        return rates;
    }


    public void setRates(Rates rates) {
        this.rates = rates;
    }


    public NeedApartment getNeedApartment() {
        return needApartment;
    }


    public void setNeedApartment(NeedApartment needApartment) {
        this.needApartment = needApartment;
    }


    public HaveApartment getHaveApartment() {
        return haveApartment;
    }


    public void setHaveApartment(HaveApartment haveApartment) {
        this.haveApartment = haveApartment;
    }


    public String getFullAddress() {
        return mFullAddress;
    }


    public void setFullAddress(String fullAddress) {
        mFullAddress = fullAddress;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ListingPutViewModel {\n");

        sb.append("  listingId: ").append(listingId).append("\n");
        sb.append("  headline: ").append(headline).append("\n");

        sb.append("  description: ").append(description).append("\n");
        sb.append("  isActive: ").append(isActive).append("\n");
        sb.append("  shareDetails: ").append(shareDetails).append("\n");
        sb.append("  questions: ").append(questions).append("\n");
        sb.append("  tags: ").append(tags).append("\n");
        sb.append("  apartmentAmenities: ").append(apartmentAmenities).append("\n");
        sb.append("  serviceType: ").append(serviceType).append("\n");
        sb.append("  calendar: ").append(calendar).append("\n");
        sb.append("  rates: ").append(rates).append("\n");
        sb.append("  needApartment: ").append(needApartment).append("\n");
        sb.append("  haveApartment: ").append(haveApartment).append("\n");
        sb.append("}\n");
        return sb.toString();
    }


    public static ListingPutViewModel fromPostToPut(ListingPostViewModel model){
        ListingPutViewModel put = new ListingPutViewModel();
        put.setFullAddress(model.getFullAddress());
        put.setServiceType(model.getServiceType());
        put.setShareDetails(model.getShareDetails());
        put.setIsActive(model.getIsActive());
        put.setCalendar(model.getCalendar());
        put.setRates(model.getRates());
        put.setNeedApartment(model.getNeedApartment());
        put.setHaveApartment(model.getHaveApartment());
        return put;
    }


}