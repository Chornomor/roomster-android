package com.roomster.model;

import com.google.gson.internal.LinkedTreeMap;

/**
 * Created by andreybofanov on 29.09.16.
 */

public class CatalogHelpdeskModel {
    public final String type;
    public final String name;

    public CatalogHelpdeskModel(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public CatalogHelpdeskModel(LinkedTreeMap<String,String> map){
        type = map.get("type");
        name = map.get("name");
    }
}
