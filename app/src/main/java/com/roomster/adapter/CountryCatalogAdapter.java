package com.roomster.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.roomster.R;
import com.roomster.model.CountryCatalogModel;

import java.util.ArrayList;
import java.util.List;


public class CountryCatalogAdapter extends BaseAdapter {

    private List<CountryCatalogModel> mCountries;
    private Context                   mContext;


    public CountryCatalogAdapter(Context context, List<CountryCatalogModel> countries) {
        mContext = context;
        if (countries != null) {
            mCountries = new ArrayList<>(countries);
        } else {
            mCountries = new ArrayList<>();
        }
    }


    @Override
    public int getCount() {
        return mCountries.size();
    }


    @Override
    public Object getItem(int position) {
        return mCountries.get(position);
    }


    @Override
    public long getItemId(int position) {
        return mCountries.get(position).getId();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.list_item_simple_text, parent, false);
            TextView countryName = (TextView) convertView.findViewById(R.id.text);

            convertView.setTag(new ViewHolder(countryName));
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();

        holder.countryName.setText(mCountries.get(position).getName());

        return convertView;
    }


    private class ViewHolder {

        public TextView countryName;


        private ViewHolder(TextView countryName) {
            this.countryName = countryName;
        }
    }
}
