package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import com.roomster.enums.GenderEnum;
import com.roomster.enums.ProviderEnum;
import com.roomster.enums.ServiceTypeEnum;


/**
 * Account model to create new user account \n linked to external auth provider
 **/
public class AccountViewModel {

    /**
     * User email
     **/
    @SerializedName("email")
    private String email = null;

    /**
     * User given name
     **/
    @SerializedName("first_name")
    private String firstName = null;

    /**
     * User last name
     **/
    @SerializedName("last_name")
    private String     lastName = null;
    /**
     * User gender. \n Allowed values: Male or 4, Female or 2, Unknown or 1.
     **/
    @SerializedName("gender")
    private GenderEnum gender   = null;

    /**
     * User birth day minimum: 1.0 maximum: 31.0
     **/
    @SerializedName("birth_day")
    private Integer         birthDay    = null;
    /**
     * User birth month minimum: 1.0 maximum: 12.0
     **/
    @SerializedName("birth_month")
    private Integer         birthMonth  = null;
    /**
     * User birth year minimum: 1900.0 maximum: 2100.0
     **/
    @SerializedName("birth_year")
    private Integer         birthYear   = null;
    /**
     * Initial service type. \n Allowed values: NeedRoom or 1, HaveShare or 2, NeedApartment or 4, HaveApartment = 5.
     **/
    @SerializedName("service_type")
    private ServiceTypeEnum serviceType = null;
    /**
     * Exernal auth provider. \n Allowed values: Facebook or 1, Google or 2, Twitter or 3
     **/
    @SerializedName("provider")
    private ProviderEnum    provider    = null;

    /**
     * External provider access token
     **/
    @SerializedName("access_token")
    private String accessToken       = null;
    /**
     * External provider access token secret (twitter)
     **/
    @SerializedName("access_token_secret")
    private String accessTokenSecret = null;

    /**
     * External auth provider user id
     **/
    @SerializedName("provider_user_id")
    private String providerUserId     = null;
    /**
     * Provider user profile image url
     **/
    @SerializedName("image_url")
    private String imageUrl           = null;
    /**
     * Provider user profile url
     **/
    @SerializedName("provider_profile_url")
    private String providerProfileUrl = null;
    /**
     * ReferrerManager id
     **/
    @SerializedName("referrer")
    private String referrer           = null;


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public String getFirstName() {
        return firstName;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public GenderEnum getGender() {
        return gender;
    }


    public void setGender(GenderEnum gender) {
        this.gender = gender;
    }


    public Integer getBirthDay() {
        return birthDay;
    }


    public void setBirthDay(Integer birthDay) {
        this.birthDay = birthDay;
    }


    public Integer getBirthMonth() {
        return birthMonth;
    }


    public void setBirthMonth(Integer birthMonth) {
        this.birthMonth = birthMonth;
    }


    public Integer getBirthYear() {
        return birthYear;
    }


    public void setBirthYear(Integer birthYear) {
        this.birthYear = birthYear;
    }


    public ServiceTypeEnum getServiceType() {
        return serviceType;
    }


    public void setServiceType(ServiceTypeEnum serviceType) {
        this.serviceType = serviceType;
    }


    public ProviderEnum getProvider() {
        return provider;
    }


    public void setProvider(ProviderEnum provider) {
        this.provider = provider;
    }


    public String getAccessToken() {
        return accessToken;
    }


    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }


    public String getAccessTokenSecret() {
        return accessTokenSecret;
    }


    public void setAccessTokenSecret(String accessTokenSecret) {
        this.accessTokenSecret = accessTokenSecret;
    }


    public String getProviderUserId() {
        return providerUserId;
    }


    public void setProviderUserId(String providerUserId) {
        this.providerUserId = providerUserId;
    }


    public String getImageUrl() {
        return imageUrl;
    }


    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }


    public String getProviderProfileUrl() {
        return providerProfileUrl;
    }


    public void setProviderProfileUrl(String providerProfileUrl) {
        this.providerProfileUrl = providerProfileUrl;
    }


    public String getReferrer() {
        return referrer;
    }


    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class AccountViewModel {\n");

        sb.append("  email: ").append(email).append("\n");
        sb.append("  firstName: ").append(firstName).append("\n");
        sb.append("  lastName: ").append(lastName).append("\n");
        sb.append("  gender: ").append(gender).append("\n");
        sb.append("  birthDay: ").append(birthDay).append("\n");
        sb.append("  birthMonth: ").append(birthMonth).append("\n");
        sb.append("  birthYear: ").append(birthYear).append("\n");
        sb.append("  serviceType: ").append(serviceType).append("\n");
        sb.append("  provider: ").append(provider).append("\n");
        sb.append("  accessToken: ").append(accessToken).append("\n");
        sb.append("  accessTokenSecret: ").append(accessTokenSecret).append("\n");
        sb.append("  providerUserId: ").append(providerUserId).append("\n");
        sb.append("  imageUrl: ").append(imageUrl).append("\n");
        sb.append("  providerProfileUrl: ").append(providerProfileUrl).append("\n");
        sb.append("  referrer: ").append(referrer).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
