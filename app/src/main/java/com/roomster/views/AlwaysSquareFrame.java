package com.roomster.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * Created by andreybofanov on 01.07.16.
 */
public class AlwaysSquareFrame extends FrameLayout {
    public AlwaysSquareFrame(Context context) {
        super(context);
    }

    public AlwaysSquareFrame(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AlwaysSquareFrame(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(),getMeasuredWidth());
    }
}
