package com.roomster.manager;


import android.content.Context;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.cache.sharedpreferences.SharedPreferencesFiles;
import com.roomster.constants.GcmPreferences;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.rest.service.AppInstallsRestService;

import javax.inject.Inject;
import java.io.IOException;


public class GcmManager {

    private static final String TAG = GcmManager.class.getCanonicalName();

    @Inject
    Context mContext;

    @Inject
    SharedPreferencesFiles mSharedPreferencesFiles;

    @Inject
    AppInstallsRestService mAppInstallsRestService;

    @Inject
    SettingsManager mSettingsManager;


    public GcmManager() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void registerToGcm() {
        if (checkPlayServices() && !isTokenSentToServer()) {
            new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        String token = getRegistrationToken();
                        Log.i(TAG, "GCM Registration Token: " + token);

                        sendRegistrationToServer(token);
                    } catch (Exception e) {
                        setTokenSentToServer(false);
                    }
                }
            }).start();
        }
    }


    /**
     * Called when the current registration token is expired, not valid or there is a change in the user settings.
     */
    public void refreshGcmToken() {
        //As we are refreshing, the server has obsolete token already.
        setTokenSentToServer(false);
        if (checkPlayServices()) {
            new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        boolean isPushEnabled = mSettingsManager.isPushActive();
                        String token = getRegistrationToken();
                        sendUpdateRegistrationToServer(token, isPushEnabled);
                    } catch (Exception e) {
                        //Nothing to do
                    }
                }
            }).start();
        }
    }


    private boolean isTokenSentToServer() {
        return mSharedPreferencesFiles.getUserSharedPreferences()
                .getBoolean(GcmPreferences.SENT_TOKEN_TO_SERVER, false);
    }


    private void setTokenSentToServer(boolean isSet) {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putBoolean(GcmPreferences.SENT_TOKEN_TO_SERVER, isSet)
                .apply();
    }


    private String getRegistrationToken() throws IOException {
        InstanceID instanceID = InstanceID.getInstance(mContext);
        String token = instanceID
                .getToken(mContext.getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE,
                        null);
        return token;
    }


    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(mContext);
        if (resultCode != ConnectionResult.SUCCESS) {
            return false;
        }
        return true;
    }


    /**
     * Persist registration to server.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {

        mAppInstallsRestService.sendRegistration(token, new RestServiceListener() {

            @Override
            public void onSuccess(Object result) {
                super.onSuccess(result);
                setTokenSentToServer(true);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                super.onFailure(code, throwable, msg);
                setTokenSentToServer(false);
            }
        });
    }


    /**
     * @param token The new token.
     */
    private void sendUpdateRegistrationToServer(String token, boolean enableNotification) {

        mAppInstallsRestService.updateRegistrationToken(token, enableNotification, new RestServiceListener() {

            @Override
            public void onSuccess(Object result) {
                super.onSuccess(result);
                setTokenSentToServer(true);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                super.onFailure(code, throwable, msg);
                setTokenSentToServer(false);
            }
        });
    }
}
