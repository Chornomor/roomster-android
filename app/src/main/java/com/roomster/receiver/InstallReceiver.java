package com.roomster.receiver;


import android.content.Context;
import android.content.Intent;

import com.google.android.gms.analytics.CampaignTrackingReceiver;


/**
 * A  Receiver triggered on installation time to get the referrer if present.
 */
public class InstallReceiver extends CampaignTrackingReceiver {



    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
    }


}