package com.roomster.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by andreybofanov on 04.08.16.
 */
public class AccountRemovedPutQuery {
    public String id;
    @SerializedName("removal_reply")
    public String message;

    public AccountRemovedPutQuery(String id, String message) {
        this.id = id;
        this.message = message;
    }
}
