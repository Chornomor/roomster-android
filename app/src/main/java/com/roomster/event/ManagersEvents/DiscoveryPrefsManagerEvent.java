package com.roomster.event.ManagersEvents;


import com.roomster.event.Event;


/**
 * Created by "Michael Katkov" on 11/12/2015.
 */
public class DiscoveryPrefsManagerEvent extends Event {

    public static class UpdateAmenitiesEvent extends DiscoveryPrefsManagerEvent {

    }

    public static class ResetSearchCriteriaEvent extends DiscoveryPrefsManagerEvent{

    }
}
