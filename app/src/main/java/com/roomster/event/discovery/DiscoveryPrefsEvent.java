package com.roomster.event.discovery;


import com.roomster.event.Event;


/**
 * Created by michaelkatkov on 2/4/16.
 */
public class DiscoveryPrefsEvent extends Event {

    public static class UpdatePrefsEvent extends DiscoveryPrefsEvent {

    }
}
