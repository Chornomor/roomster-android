package com.roomster.rest.api;


import com.roomster.rest.model.BillingRespModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;


public interface BillingApi {

    /**
     * Get eligible refund info - true or false
     *
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<UserGetViewModel>
     */

    @GET("Refunds")
    Call<BillingRespModel> getEligible(@Header("Authorization") String authorization);


    @POST("Refunds")
    Call<BillingRespModel> postEligible(@Header("Authorization") String authorization);
}
