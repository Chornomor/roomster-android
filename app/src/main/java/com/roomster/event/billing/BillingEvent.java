package com.roomster.event.billing;

import com.roomster.enums.BillingTypeEnum;
import com.roomster.event.Event;
import com.roomster.rest.model.DisputeModel;
import com.roomster.rest.model.RefundModel;
import com.roomster.rest.model.TransactionsModel;

import java.util.List;

/**
 * Created by yuliasokolova on 25.10.16.
 */
public class BillingEvent  extends Event{

    public static class EligibleGetSuccess extends BillingEvent {
        public boolean eligible;
        public RefundModel  refund  = null;
        public DisputeModel dispute = null;
        public String message;
        public BillingTypeEnum type;
    }

    public static class EligibleGetFail extends BillingEvent {
    }

}
