package com.roomster.model;

import com.google.gson.annotations.SerializedName;

import java.util.LinkedHashMap;

/**
 * Created by andreybofanov on 06.09.16.
 */
public class LocaleCatalogModel implements Comparable<LocaleCatalogModel>{
    private String name;

    @SerializedName("english_name")
    private String englishName;

    private String direction;

    private String locale;

    public boolean equalsToLocale(String locale){
        if(locale == null)
            return false;
        return this.locale.equals(locale);
    }

    public String getReadableName(){
        return name;
    }

    public boolean equalsToReadebleName(String name){
        if(getReadableName() == null)
            return false;
        return getReadableName().equals(name);
    }

    public String getLocale(){
        return locale;
    }


    @Override
    public int compareTo(LocaleCatalogModel another) {
        if(another == null) {
            return 1;
        }
        if(englishName == null && another.englishName != null) {
            return -1;
        } else if(englishName == null){
            return 0;
        }

        return englishName.compareTo(another.englishName);
    }
}
