package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import com.roomster.enums.LabelEnum;
import com.roomster.enums.StatusEnum;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class ConversationView implements Serializable {

    @SerializedName("ConversationId")
    private String conversationId = null;

    @SerializedName("ConversationUserId")
    private String conversationUserId = null;

    @SerializedName("Count")
    private Integer count = null;

    @SerializedName("CountNew")
    private Integer countNew = null;

    @SerializedName("Created")
    private Date created = null;

    @SerializedName("LastRead")
    private Date lastRead = null;

    @SerializedName("HasAttachments")
    private Boolean hasAttachments = null;

    @SerializedName("MessageId")
    private String messageId = null;

    @SerializedName("Status")
    private StatusEnum status = null;

    @SerializedName("Label")
    private LabelEnum label = null;

    @SerializedName("Text")
    private String text = null;

    @SerializedName("UserId")
    private Long userId = null;

    @SerializedName("Users")
    private List<ConversationUserProfile> users = null;

    @SerializedName("IsLocked")
    private Boolean isLocked = null;


    public String getConversationId() {
        return conversationId;
    }


    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }


    public String getConversationUserId() {
        return conversationUserId;
    }


    public void setConversationUserId(String conversationUserId) {
        this.conversationUserId = conversationUserId;
    }


    public Integer getCount() {
        return count;
    }


    public void setCount(Integer count) {
        this.count = count;
    }


    public Integer getCountNew() {
        return countNew;
    }


    public void setCountNew(Integer countNew) {
        this.countNew = countNew;
    }


    public Date getCreated() {
        return created;
    }


    public void setCreated(Date created) {
        this.created = created;
    }


    public Date getLastRead() {
        return lastRead;
    }


    public void setLastRead(Date lastRead) {
        this.lastRead = lastRead;
    }


    public Boolean getHasAttachments() {
        return hasAttachments;
    }


    public void setHasAttachments(Boolean hasAttachments) {
        this.hasAttachments = hasAttachments;
    }


    public String getMessageId() {
        return messageId;
    }


    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }


    public StatusEnum getStatus() {
        return status;
    }


    public void setStatus(StatusEnum status) {
        this.status = status;
    }


    public LabelEnum getLabel() {
        return label;
    }


    public void setLabel(LabelEnum label) {
        this.label = label;
    }


    public String getText() {
        return text;
    }


    public void setText(String text) {
        this.text = text;
    }


    public Long getUserId() {
        return userId;
    }


    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public List<ConversationUserProfile> getUsers() {
        return users;
    }


    public void setUsers(List<ConversationUserProfile> users) {
        this.users = users;
    }


    public Boolean getIsLocked() {
        return isLocked;
    }


    public void setIsLocked(Boolean isLocked) {
        this.isLocked = isLocked;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ConversationView {\n");

        sb.append("  conversationId: ").append(conversationId).append("\n");
        sb.append("  conversationUserId: ").append(conversationUserId).append("\n");
        sb.append("  count: ").append(count).append("\n");
        sb.append("  countNew: ").append(countNew).append("\n");
        sb.append("  created: ").append(created).append("\n");
        sb.append("  lastRead: ").append(lastRead).append("\n");
        sb.append("  hasAttachments: ").append(hasAttachments).append("\n");
        sb.append("  messageId: ").append(messageId).append("\n");
        sb.append("  status: ").append(status).append("\n");
        sb.append("  label: ").append(label).append("\n");
        sb.append("  text: ").append(text).append("\n");
        sb.append("  userId: ").append(userId).append("\n");
        sb.append("  users: ").append(users).append("\n");
        sb.append("  isLocked: ").append(isLocked).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}