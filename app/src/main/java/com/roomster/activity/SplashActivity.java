package com.roomster.activity;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.newrelic.agent.android.NewRelic;
import com.roomster.BuildConfig;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.PlayStore;
import com.roomster.event.RestServiceEvents.AccountRestServiceEvent;
import com.roomster.event.checkapp.CheckAppEvent;
import com.roomster.manager.SettingsManager;
import com.roomster.utils.LocaleUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.fabric.sdk.android.Fabric;


public class SplashActivity extends SignLogicActivity {

    private Handler                     mHandler;
    private OpenInitialActivityRunnable mOpenInitialActivityRunnable;


    @Bind(R.id.splash_activity_year)
    TextView                            mYeartV;

    @Inject
    SettingsManager mSettingsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
//        if (mSettingsManager.getLocale() != null && !mSettingsManager.getLocale().isEmpty()) {
//            LocaleUtil.updateResources(this, mSettingsManager.getLocale());
//        }
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        String copyright = getString(R.string.copyright) + getCurrentYear() + getString(R.string.right_reserved);
        mYeartV.setText(copyright);
    }

    private String getCurrentYear() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        return Integer.toString(year);
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (!BuildConfig.DEBUG) {
            Fabric.with(this, new Crashlytics());
            NewRelic.withApplicationToken("AA94f3c79e22bd252c43f0c41bfe54525b3942cf05").start(this.getApplication());
        }
        Branch branch = Branch.getInstance();

        branch.initSession(new Branch.BranchReferralInitListener(){
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    mCheckAppRestService.checkForUpdate();
                } else {
                    Log.i("MyApp", error.getMessage());
                }
            }
        }, this.getIntent().getData(), this);
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (mHandler != null) {
            mHandler.removeCallbacks(mOpenInitialActivityRunnable);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(CheckAppEvent.ShouldUpdateAppEvent event) {
        if (event.shouldUpdate) {
            openUpdateAppDialog();
        } else {
            init();
            mHandler.post(mOpenInitialActivityRunnable);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(AccountRestServiceEvent.EmailWasNotConfirmOrAdd event) {
        addOrConfirmEmailDialog();
    }


    private void addOrConfirmEmailDialog() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.title_sorry))
                .setMessage(getString(R.string.add_or_confirm_facebook_email))
                .setCancelable(false)
                .setNegativeButton(getString(R.string.button_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                openSignInActivity();
                                dialog.cancel();
                            }
                        });
        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(AccountRestServiceEvent.LoginFailure event) {
        openSignInActivity();
    }


    @Override
    protected void loginFailure(int code, String errorMessage) {
        openSignInActivity();
    }


    private void init() {
        HandlerThread handlerThread = new HandlerThread("Worker thread for signing in.");
        handlerThread.start();
        mHandler = new Handler(handlerThread.getLooper());
        mOpenInitialActivityRunnable = new OpenInitialActivityRunnable();
    }


    private void openUpdateAppDialog() {
        new AlertDialog.Builder(this).setTitle(R.string.update_app_dialog_title).setMessage(R.string.update_app_dialog_content)
          .setPositiveButton(R.string.dialog_ok_button, new DialogInterface.OnClickListener() {

              @Override
              public void onClick(DialogInterface dialog, int which) {
                  startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(PlayStore.PLAY_STORE_URL + getPackageName())));
              }
          }).setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                finish();
                return true;
            }
        }).show();
    }


    private void openSignInActivity() {
        Intent intent = new Intent(this, SignInActivity.class);
        //set a flag which will replace current activity
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        finish();
    }


    /**
     * Opens desired activity: - MainActivity for logged in users - SignIn activity for new users
     */
    private class OpenInitialActivityRunnable implements Runnable {

        @Override
        public void run() {
            //Facebook initialization is asynchronous, so we'll need to wait for it to initialize
            if (!FacebookSdk.isInitialized()) {
                mHandler.postDelayed(mOpenInitialActivityRunnable, 500);
            } else {
                mAccountRestService.login();
            }
        }
    }
}