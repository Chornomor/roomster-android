package com.roomster.utils;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import com.roomster.R;
import com.roomster.constants.Permission;
import com.roomster.dialog.PermissionDeniedDialog;

import static android.support.v4.app.ActivityCompat.checkSelfPermission;
import static android.support.v4.app.ActivityCompat.shouldShowRequestPermissionRationale;


/**
 * Created by Bogoi on 3/26/2016.
 */
public class PermissionsHelper {

    public interface OnCameraPermissionActionListener {

        void onCameraPermissionGranted();

        void onCameraPermissionDenied();
    }



    public interface OnStoragePermissionActionListener {

        void onStoragePermissionGranted();

        void onStoragePermissionDenied();
    }



    public interface OnLocationPermissionActionListener {

        void onLocationPermissionGranted();

        void onLocationPermissionDenied();
    }


    private Activity                           mActivity;
    private OnCameraPermissionActionListener   mOnCameraPermissionActionListener;
    private OnStoragePermissionActionListener  mOnStoragePermissionActionListener;
    private OnLocationPermissionActionListener mOnLocationPermissionActionListener;


    public PermissionsHelper(Activity activity) {
        mActivity = activity;
    }


    public void actionOnStoragePermission(OnStoragePermissionActionListener listener) {
        if (mActivity == null) {
            return;
        }

        if (listener != null) {
            if (mOnStoragePermissionActionListener != null) {
                // if the permission request is in progress just replace the listener
                mOnStoragePermissionActionListener = listener;
                return;
            } else {
                mOnStoragePermissionActionListener = listener;
            }
            //Check if we have the Camera Permission
            if (!hasReadWriteStoragePermissionGranted()) {
                // Check if we need to show an explanation dialog about why we need the camera permission
                if (shouldShowRequestStoragePermissionRationale()) {
                    openStoragePermissionExplanationDialog();
                } else {
                    requestStoragePermission();
                }
            } else {
                mOnStoragePermissionActionListener.onStoragePermissionGranted();
                mOnStoragePermissionActionListener = null;
            }
        }
    }


    public void actionOnCameraPermission(OnCameraPermissionActionListener listener) {
        if (mActivity == null) {
            return;
        }

        if (listener != null) {
            if (mOnCameraPermissionActionListener != null) {
                // if the permission request is in progress just replace the listener
                mOnCameraPermissionActionListener = listener;
                return;
            } else {
                mOnCameraPermissionActionListener = listener;
            }
            //Check if we have the Camera Permission
            if (!hasCameraPermissionGranted()) {
                // Check if we need to show an explanation dialog about why we need the camera permission
                if (shouldShowRequestPermissionRationale(mActivity, Manifest.permission.CAMERA)) {
                    openCameraPermissionExplanationDialog();
                } else {
                    requestCameraPermission();
                }
            } else {
                mOnCameraPermissionActionListener.onCameraPermissionGranted();
                mOnCameraPermissionActionListener = null;
            }
        }
    }


    public void actionOnLocationPermission(OnLocationPermissionActionListener listener) {
        if (mActivity == null) {
            return;
        }

        if (listener != null) {
            if (mOnLocationPermissionActionListener != null) {
                // if the permission request is in progress just replace the listener
                mOnLocationPermissionActionListener = listener;
                return;
            } else {
                mOnLocationPermissionActionListener = listener;
            }
            //Check if we have the Location Permission
            if (!hasLocationPermissionGranted()) {
                // Check if we need to show an explanation dialog about why we need the location permission
                if (shouldShowRequestLocationPermissionRationale()) {
                    openLocationPermissionExplanationDialog();
                } else {
                    requestLocationPermission();
                }
            } else {
                mOnLocationPermissionActionListener.onLocationPermissionGranted();
                mOnLocationPermissionActionListener = null;
            }
        }
    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
            @NonNull int[] grantResults) {
        switch (requestCode) {
        case Permission.PERMISSION_REQUEST_CAMERA:
            onPermissionRequestCamera(permissions, grantResults);
            break;
        case Permission.PERMISSION_REQUEST_STORAGE:
            onPermissionRequestStorage(permissions, grantResults);
            break;
        case Permission.PERMISSION_REQUEST_LOCATION:
            onPermissionRequestLocation(permissions, grantResults);
            break;
        default: {
            // Nothing to do
        }
        }
    }


    private void openCameraPermissionExplanationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(mActivity.getString(R.string.request_camera_permission_dialog_title));
        builder.setMessage(mActivity.getString(R.string.request_camera_permission_dialog_message));
        builder.setPositiveButton(mActivity.getString(R.string.dialog_ok_button),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestCameraPermission();
                    }
                }).show();
    }


    private void openStoragePermissionExplanationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(mActivity.getString(R.string.request_storage_permission_dialog_title));
        builder.setMessage(mActivity.getString(R.string.request_storage_permission_dialog_message));
        builder.setPositiveButton(mActivity.getString(R.string.dialog_ok_button),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestStoragePermission();
                    }
                }).show();
    }


    private void openLocationPermissionExplanationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(mActivity.getString(R.string.request_location_permission_dialog_title));
        builder.setMessage(mActivity.getString(R.string.request_location_permission_dialog_message));
        builder.setPositiveButton(mActivity.getString(R.string.dialog_ok_button),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestLocationPermission();
                    }
                }).show();
    }


    private void requestCameraPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mActivity.requestPermissions(new String[] { Manifest.permission.CAMERA },
                    Permission.PERMISSION_REQUEST_CAMERA);
        }
    }


    private void requestStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mActivity.requestPermissions(
                    new String[] { Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
                    }, Permission.PERMISSION_REQUEST_STORAGE);
        }
    }


    private void requestLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mActivity.requestPermissions(
                    new String[] { Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
                    }, Permission.PERMISSION_REQUEST_LOCATION);
        }
    }


    private boolean hasLocationPermissionGranted() {
        return checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }


    private boolean hasCameraPermissionGranted() {
        return checkSelfPermission(mActivity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }


    private boolean hasReadWriteStoragePermissionGranted() {
        boolean hasReadExternalStoragePermission = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            hasReadExternalStoragePermission = checkSelfPermission(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED;
        }

        return hasReadExternalStoragePermission || (
                checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED);
    }


    private boolean shouldShowRequestStoragePermissionRationale() {
        boolean checkReadExternalStorage = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            checkReadExternalStorage = shouldShowRequestPermissionRationale(mActivity,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        return checkReadExternalStorage || shouldShowRequestPermissionRationale(mActivity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }


    private boolean shouldShowRequestLocationPermissionRationale() {
        return shouldShowRequestPermissionRationale(mActivity, Manifest.permission.ACCESS_FINE_LOCATION)
                || shouldShowRequestPermissionRationale(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION);
    }


    private void onPermissionRequestCamera(String permissions[], int[] grantResults) {
        if (mOnCameraPermissionActionListener == null) {
            return;
        }
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0) {
            boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[0]);
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mOnCameraPermissionActionListener.onCameraPermissionGranted();
            } else {
                if (!showRationale) {
                    // The permission is always denied
                    new PermissionDeniedDialog(mActivity)
                            .show(mActivity.getString(R.string.camera_permission_denied_dialog_title),
                                    mActivity.getString(R.string.camera_permission_denied_dialog_message));
                }
                mOnCameraPermissionActionListener.onCameraPermissionDenied();
            }
        }
        mOnCameraPermissionActionListener = null;
    }


    private void onPermissionRequestStorage(String permissions[], int[] grantResults) {
        if (mOnStoragePermissionActionListener == null) {
            return;
        }
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0) {
            boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[0]);
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mOnStoragePermissionActionListener.onStoragePermissionGranted();
            } else {
                if (!showRationale) {
                    // The permission is always denied
                    new PermissionDeniedDialog(mActivity)
                            .show(mActivity.getString(R.string.storage_permission_denied_dialog_title),
                                    mActivity.getString(R.string.storage_permission_denied_dialog_message));
                }
                mOnStoragePermissionActionListener.onStoragePermissionDenied();
            }
        }
        mOnStoragePermissionActionListener = null;
    }


    private void onPermissionRequestLocation(String permissions[], int[] grantResults) {
        if (mOnLocationPermissionActionListener == null) {
            return;
        }
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0) {
            boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[0]);
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mOnLocationPermissionActionListener.onLocationPermissionGranted();
            } else {
                if (!showRationale) {
                    // The permission is always denied
                    new PermissionDeniedDialog(mActivity)
                            .show(mActivity.getString(R.string.location_permission_denied_dialog_title),
                                    mActivity.getString(R.string.location_permission_denied_dialog_message));
                }
                mOnLocationPermissionActionListener.onLocationPermissionDenied();
            }
        }
        mOnLocationPermissionActionListener = null;
    }
}