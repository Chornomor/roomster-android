package com.roomster.views;


import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.roomster.R;
import com.roomster.adapter.ListingPagerAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.rest.model.ResultItemModel;
import com.roomster.rest.service.BookmarksRestService;
import com.roomster.utils.DateUtil;
import com.roomster.utils.LogUtil;
import com.roomster.utils.ScreenUtil;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import javax.inject.Inject;


/**
 * Created by "Michael Katkov" on 11/18/2015.
 */
public class DetailedView extends BottomSheetLayout implements ViewPagerWithIndicators.BottomViewExpandListener {

    private ResultItemModel                         mCurrentResultItemModel;
    private SlidingUpPanelLayout.PanelSlideListener mPanelSlideListener;

    @Bind(R.id.dv_pager)
    ViewPagerWithIndicators mPageIndicator;

    @Bind(R.id.dv_info_sliding_view)
    BottomInfoSlidingView mBottomInfoView;

    @Bind(R.id.iv_bookmark)
    public ImageView mBookmark;

    @Bind(R.id.detailed_sliding_panel_layout)
    FrameLayout mSlidingPanelLayout;

    @Bind(R.id.dv_sliding_layout)
    SlidingUpPanelLayout mSlidingLayout;

    @Bind(R.id.dv_new_text)
    TextView mNewTv;

    @Bind(R.id.dv_item_listing_date)
    TextView mDateTv;

    @Inject
    BookmarksRestService mBookmarksService;

    boolean isMyListing = false;

    public DetailedView(Context context) {
        super(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
        initView();
    }


    public DetailedView(Context context, AttributeSet attrs) {
        super(context, attrs);
        RoomsterApplication.getRoomsterComponent().inject(this);
        initView();
    }



    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        post(new Runnable() {
            @Override
            public void run() {
                restorePanelState();
            }
        });
    }

    private void restorePanelState(){
        if(mSlidingLayout == null) {
            return;
        }
        if(mSlidingLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED){
            mPanelSlideListener.onPanelExpanded(mSlidingPanelLayout);
            mBottomInfoView.setBottomViewVisibility(VISIBLE);
            requestLayout();
        }
    }

    @Override
    public void onShouldExpand() {
        expandSlidingPanel();
    }


    @Override
    public void onShouldCollapse() {

    }


    @OnClick(R.id.rl_bookmark)
    public void оnBookmarkClick() {
        mBookmarksService.toggleBookmark(mCurrentResultItemModel);
    }


    @OnClick(R.id.main_content)
    public void onMainContentClick() {
        mSlidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
    }


    @OnClick({ R.id.clickable_view, R.id.bs_scroll_content })
    public void onBottomInfoContentClick() {
        switch (mSlidingLayout.getPanelState()) {
        case EXPANDED:
            mSlidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            break;
        case COLLAPSED:
            mSlidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            break;
        default:
            break;
        }
    }


    public void setPanelSlidingListener(SlidingUpPanelLayout.PanelSlideListener listener) {
        if (listener != null) {
            mPanelSlideListener = listener;
        }
    }

    public void setMyListing(boolean myListing) {
        this.isMyListing = myListing;
        mBottomInfoView.setLocationExpanded();
    }

    public void expandSlidingPanel() {
        mSlidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
    }


    public void collapseSlidingPanel() {
        mSlidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
    }


    public void setOnSeeProfileClickListener(View.OnClickListener listener) {
        mBottomInfoView.setOnSeeUserProfileClickListener(listener);
    }


    public void setOnReportClickListener(View.OnClickListener listener) {
        mBottomInfoView.setOnReportClickListener(listener);
    }


    public void setOnBlockClickListener(View.OnClickListener listener) {
        mBottomInfoView.setOnBlockClickListener(listener);
    }


    public void updateBookmarkView(ResultItemModel resultItemModel) {
        mCurrentResultItemModel = resultItemModel;
        if (mCurrentResultItemModel == null || mCurrentResultItemModel.getListing() == null) {
            return;
        }

        if (mCurrentResultItemModel.getListing().getIsBookmarked()) {
            mBookmark.setImageResource(R.drawable.ic_bookmark_light_active);
        } else {
            mBookmark.setImageResource(R.drawable.ic_bookmark_light);
        }
    }


    public void setPrice(int price, String currencyCode) {
        mBottomInfoView.setPrice(price);
        mBottomInfoView.setCurrency(currencyCode);
    }


    public void displayProfileActions(boolean shouldDisplay) {
        mBottomInfoView.displayProfileOptions(shouldDisplay);
    }


    public void setCurrentPositionSelected() {
        mPageIndicator.setCurrentPositionSelected();
    }


    public void setInfoPositionSelected() {
        mPageIndicator.setInfoPositionSelected();
    }


    public void populateViews(FragmentManager fragmentManager, ResultItemModel item) {
        loadSearchResults(fragmentManager, item);
    }


    private void initView() {
        inflate(getContext(), R.layout.view_detailed, this);
        ButterKnife.bind(this);
    }

    private void setPads(int toolbarHeight){
        View panel = mSlidingPanelLayout;
        int layoutBottom = mSlidingLayout.getBottom();
        int panelBottom = mSlidingPanelLayout.getBottom();
        mBottomInfoView.setIsMyListing(isMyListing);
        panel.setPadding(panel.getPaddingLeft(),panel.getPaddingTop(),panel.getPaddingRight(),
                (int) (panelBottom - layoutBottom) + (isMyListing ? toolbarHeight/6 : toolbarHeight));
    }
    private void preSetPads(int toolbarHeight){
        View panel = mSlidingPanelLayout;
        int bottom = (int) getResources().getDimension(R.dimen.size_205);
        mBottomInfoView.setIsMyListing(isMyListing);
        panel.setPadding(panel.getPaddingLeft(),panel.getPaddingTop(),panel.getPaddingRight(),
                (int) (bottom) + (isMyListing ? toolbarHeight/6 : toolbarHeight));
    }

    private void initSlidingPanel() {
        setSlidingPanelLayoutHeight();
        final int toolbarHeight = (int) getContext().getResources().getDimension(R.dimen.detailed_view_toolbar_height);
        mBottomInfoView.setBottomViewVisibility(View.INVISIBLE);
        preSetPads(toolbarHeight);
        mBottomInfoView.post(new Runnable() {
            @Override
            public void run() {
                setPads(toolbarHeight);
            }
        });
        mSlidingLayout.setPanelSlideListener(new AbstractPanelSlideListener() {

            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                mBottomInfoView.setBottomViewVisibility(VISIBLE);
                setPads(toolbarHeight);
                if (mPanelSlideListener != null) {
                    mPanelSlideListener.onPanelSlide(panel, slideOffset);
                }
            }


            @Override
            public void onPanelCollapsed(View panel) {
                mBottomInfoView.setBottomViewVisibility(GONE);
//                setSlidingPanelPaddingBottom();
                if (mPanelSlideListener != null) {
                    mPanelSlideListener.onPanelCollapsed(panel);
                }
            }


            @Override
            public void onPanelExpanded(View panel) {
//                setSlidingPanelPaddingBottom();
                if (mPanelSlideListener != null) {
                    mPanelSlideListener.onPanelExpanded(panel);
                }
            }
        });
    }


    private void setSlidingPanelLayoutHeight() {
        ViewGroup.LayoutParams params = mSlidingPanelLayout.getLayoutParams();
        params.height = ScreenUtil.getHeight(getContext()) - getResources()
                .getDimensionPixelOffset(R.dimen.profile_sliding_panel_delta);
        mSlidingPanelLayout.setLayoutParams(params);
    }


    private void loadSearchResults(FragmentManager fragmentManager, ResultItemModel item) {
        if (item != null) {
            mCurrentResultItemModel = item;
            mBottomInfoView.setData(mCurrentResultItemModel);
            ListingPagerAdapter listingPagerAdapter = new ListingPagerAdapter(fragmentManager, mCurrentResultItemModel);
            mPageIndicator.setAdapter(listingPagerAdapter, listingPagerAdapter.getCount() + 1);
            mPageIndicator.setOnBottomViewExpandListener(this);
            updateBookmarkView(mCurrentResultItemModel);
            setDate();
            setIfNew();
            if(isMyListing) {
                mBottomInfoView.setVisibility(VISIBLE);
                mBottomInfoView.setBottomViewVisibility(INVISIBLE);
            }
            mBottomInfoView.post(new Runnable() {
                @Override
                public void run() {
                    initSlidingPanel();
                }
            });
        }
    }


    private void loadBookmarks(FragmentManager fragmentManager, int index) {
        if (mCurrentResultItemModel != null) {
            mBottomInfoView.setData(mCurrentResultItemModel);
            ListingPagerAdapter listingPagerAdapter = new ListingPagerAdapter(fragmentManager, mCurrentResultItemModel);
            mPageIndicator.setAdapter(listingPagerAdapter, listingPagerAdapter.getCount() + 1);
            mPageIndicator.setOnBottomViewExpandListener(this);
        }
    }


    private void setDate() {
        String date = DateUtil.getRelativeTimeSpanString(getContext(), mCurrentResultItemModel.getListing().getRefreshed());
        mDateTv.setText(date);
    }


    private void setIfNew() {
        if (mCurrentResultItemModel.getListing().getIsNew()) {
            mNewTv.setVisibility(VISIBLE);
        } else {
            mNewTv.setVisibility(GONE);
        }
    }
}