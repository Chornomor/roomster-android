package com.roomster.fragment;


import android.content.Context;

import com.roomster.R;
import com.roomster.listener.BookmarksCallbackListener;


public class BookmarksMapViewFragment extends MapViewFragment {

    public void setCallback(BookmarksCallbackListener callback) {
        super.mCallback = callback;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mapManager.setBookmarkMode(true);
    }

    @Override
    protected void initViews() {
        mEmptyResultHeaderTV.setText(getString(R.string.empty_bookmark_header));
        mEmptyResultSubtextTV.setText(getString(R.string.empty_bookmark_subtext));
    }

}
