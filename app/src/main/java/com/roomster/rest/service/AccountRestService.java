package com.roomster.rest.service;


import android.content.Context;

import com.facebook.AccessToken;
import com.newrelic.agent.android.harvest.HarvestResponse;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.AccountRemovedEvent;
import com.roomster.event.RestServiceEvents.AccountRestServiceEvent;
import com.roomster.event.RestServiceEvents.NoConnectionError;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.manager.DiscoveryPrefsManager;
import com.roomster.manager.FacebookManager;
import com.roomster.manager.MeManager;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.ReferrerManager;
import com.roomster.manager.TokenManager;
import com.roomster.model.FacebookUserModel;
import com.roomster.rest.api.AccountApi;
import com.roomster.rest.api.TokenApi;
import com.roomster.rest.model.AccountDetailsViewModel;
import com.roomster.rest.model.AccountRemovedGetResponse;
import com.roomster.rest.model.AccountRemovedPutQuery;
import com.roomster.rest.model.AccountViewModel;
import com.roomster.rest.model.CreateAccountTokenModel;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.Locale;

import javax.inject.Inject;

import io.branch.referral.Branch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AccountRestService {

    @Inject
    EventBus mEventBus;

    @Inject
    TokenApi mTokenApi;

    @Inject
    AccountApi mAccountApi;

    @Inject
    TokenManager mTokenManager;

    @Inject
    FacebookManager mFacebookManager;

    @Inject
    ReferrerManager mReferrerManager;

    @Inject
    MeManager mMeManager;

    @Inject
    NetworkManager mNetworkManager;

    @Inject
    DiscoveryPrefsManager mDPrefsManager;

    @Inject
    Context context;


    public AccountRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    /**
     * Logs the user. If the user doesn't exist, it is created.
     */
    public void login() {
        mTokenManager.clearToken();
        if (!hasFacebookToken()) {
            mEventBus.post(new AccountRestServiceEvent.LoginFailure());
            return;
        }

        mTokenManager.getAccessToken(loginRestServiceListener);
    }

    public void getRemovedUser(){
        if(!mNetworkManager.hasNetworkAccess()){
            mEventBus.post(new NoConnectionError());
            return;
        }
        Call<AccountRemovedGetResponse> getRemoved;

        if (AccessToken.getCurrentAccessToken() != null) {
            getRemoved = mAccountApi.getRemovedUser(AccessToken.getCurrentAccessToken().getToken(), "facebook");
            getRemoved.enqueue(new Callback<AccountRemovedGetResponse>() {
                @Override
                public void onResponse(Call<AccountRemovedGetResponse> call, Response<AccountRemovedGetResponse> response) {
                    if (response.code() == HarvestResponse.Code.OK.getStatusCode()) {
                        mEventBus.post(new AccountRemovedEvent.RemovedUserGot(response.body().id, response.body().message));
                    } else {
                        mEventBus.post(new AccountRemovedEvent.RemovedUserGetFailure(response.code()));
                    }

                }

                @Override
                public void onFailure(Call<AccountRemovedGetResponse> call, Throwable t) {
                    mEventBus.post(new AccountRemovedEvent.RemovedUserGetFailure(-1));
                }
            });
        }

    }

    public void respondeOnRemoval(String id, String message){
        if(!mNetworkManager.hasNetworkAccess()){
            mEventBus.post(new NoConnectionError());
            return;
        }

        Call<String> getRemoved = mAccountApi.respondOnRemoval(new AccountRemovedPutQuery(id,message));
        getRemoved.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.code() == HarvestResponse.Code.OK.getStatusCode()) {
                    mEventBus.post(new AccountRemovedEvent.RemovedUserResponde());
                } else {
                    mEventBus.post(new AccountRemovedEvent.RemovedUserRespondeFail(response.code()));
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                mEventBus.post(new AccountRemovedEvent.RemovedUserRespondeFail(-1));
            }
        });
    }


    public void reLogin() {
        if (hasFacebookToken()) {
            mTokenManager.clearToken();
            mTokenManager.refreshToken(loginRestServiceListener);
        } else {
            mEventBus.post(new AccountRestServiceEvent.LoginFailure());
        }
    }


    private boolean hasFacebookToken() {
        if (AccessToken.getCurrentAccessToken() != null) {
            mTokenManager.setExternalProvider("facebook");
            mTokenManager.setExternalProviderAccessToken(AccessToken.getCurrentAccessToken().getToken());
            return true;
        } else {
            return false;
        }
    }


    private RestServiceListener<String> loginRestServiceListener = new RestServiceListener<String>() {

        @Override
        public void onSuccess(String result) {
            mDPrefsManager.storeTutorialWasShowed(true);
            mEventBus.post(new AccountRestServiceEvent.LoginSuccess());
        }


        @Override
        public void onFailure(int code, Throwable throwable, String msg) {
            switch (code) {
                case ResponseCode.USER_NOT_FOUND:
                case ResponseCode.BAD_REQUEST:
                    createUser();
                    break;
                case ResponseCode.SERVER_ERROR:
                default:
                    AccountRestServiceEvent.LoginFailure event = new AccountRestServiceEvent.LoginFailure(code);
                    if (msg != null) {
                        event.errorMessage = msg;
                    }
                    event.code = code;
                    mEventBus.post(event);
            }
        }
    };


    public void deleteAccount() {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String token) {
                if (mNetworkManager.hasNetworkAccess()) {
                    Call<String> call = mAccountApi.accountDelete(token);
                    call.enqueue(new Callback<String>() {

                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            switch (response.code()) {
                                case ResponseCode.OK:
                                    onAccountDeleteSuccess();
                                    break;
                                case ResponseCode.FORBIDDEN:
                                case ResponseCode.SERVER_ERROR:
                                default:
                                    mEventBus.post(new AccountRestServiceEvent.DeleteFailure(response.code()));
                            }
                        }


                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            mEventBus.post(new AccountRestServiceEvent.DeleteFailure());
                        }
                    });
                } else {
                    mEventBus.post(new AccountRestServiceEvent.DeleteFailure());
                }
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                mEventBus.post(new AccountRestServiceEvent.DeleteFailure());
            }
        });
    }


    public void updateAccount(final AccountDetailsViewModel model) {

        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String token) {
                if (mNetworkManager.hasNetworkAccess()) {
                    Call<String> call = mAccountApi.accountPut(model, token, null, null);

                    call.enqueue(new Callback<String>() {

                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            switch (response.code()) {
                                case ResponseCode.OK:
                                    mEventBus.post(new AccountRestServiceEvent.UpdateSuccess());
                                    break;
                                case ResponseCode.SERVER_ERROR:
                                default:
                                    mEventBus.post(new AccountRestServiceEvent.UpdateFailure());
                            }
                        }


                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            mEventBus.post(new AccountRestServiceEvent.UpdateFailure());
                        }
                    });
                } else {
                    mEventBus.post(new AccountRestServiceEvent.UpdateFailure());
                }
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                mEventBus.post(new AccountRestServiceEvent.UpdateFailure());
            }
        });
    }


    private void onAccountDeleteSuccess() {
        mTokenManager.clearToken();
        mEventBus.post(new AccountRestServiceEvent.DeleteSuccess());
    }


    private void onEmailWasNotAddOrConfirm() {
        mEventBus.post(new AccountRestServiceEvent.EmailWasNotConfirmOrAdd());
    }


    private void createUser() {
        if (mNetworkManager.hasNetworkAccess()) {
            //get data about me from Facebook
            mFacebookManager.requestMyProfile(new RestServiceListener<FacebookUserModel>() {

                @Override
                public void onSuccess(FacebookUserModel result) {
                    if (result.getEmail() == null){
                        onEmailWasNotAddOrConfirm();
                    } else {
                        onRequestFacebookProfileSuccess(result);
                    }
                }


                @Override
                public void onFailure(int code, Throwable throwable, String msg) {
                    AccountRestServiceEvent.LoginFailure failure = new AccountRestServiceEvent.LoginFailure();
                    failure.errorMessage = msg;
                    mEventBus.post(failure);
                }
            });
        } else {
            mEventBus.post(new AccountRestServiceEvent.LoginFailure());
        }
    }


    private void onRequestFacebookProfileSuccess(FacebookUserModel facebookUserModel) {
        if (mNetworkManager.hasNetworkAccess()) {
            AccountViewModel accountViewModel = mFacebookManager.getFacebookAccountModel(facebookUserModel);
            JSONObject o = Branch.getInstance(context).getFirstReferringParams();
            String refferer = null;
            try {
                refferer = o.has("referral")? o.getString("referral") : o.getString("~referring_link");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            accountViewModel.setReferrer(refferer);

            Call<CreateAccountTokenModel> call = mAccountApi
              .accountPost(accountViewModel, NumberFormat.getCurrencyInstance().getCurrency().toString(),
                Locale.getDefault().toString().replace("_", "-"));

            //Sometimes the facebook access token is null
            if (mFacebookManager.getAccessToken() != null) {
                call.enqueue(new Callback<CreateAccountTokenModel>() {

                    @Override
                    public void onResponse(Call<CreateAccountTokenModel> call, Response<CreateAccountTokenModel> response) {
                        switch (response.code()) {
                            case ResponseCode.OK:
                            case ResponseCode.USER_CREATED:
                                onUserCreated(response.body());
                                break;
                            case ResponseCode.BAD_REQUEST:
                            case ResponseCode.SERVER_ERROR:
                            default:
                                mEventBus.post(new AccountRestServiceEvent.LoginFailure());
                        }
                    }


                    @Override
                    public void onFailure(Call<CreateAccountTokenModel> call, Throwable t) {
                        mEventBus.post(new AccountRestServiceEvent.LoginFailure());
                    }
                });
            } else {
                mEventBus.post(new AccountRestServiceEvent.LoginFailure());
            }
        } else {
            mEventBus.post(new AccountRestServiceEvent.LoginFailure());
        }
    }


    private void onUserCreated(CreateAccountTokenModel model) {
        mDPrefsManager.storeTutorialWasShowed(false);
        //Sometimes the facebook access token is null
        if (mFacebookManager.getAccessToken() != null) {
            mTokenManager.setExternalProvider("facebook");
            mTokenManager.setExternalProviderAccessToken(AccessToken.getCurrentAccessToken().getToken());
            mTokenManager.setCurrentTokenModel(model);

            if (model.getUser() != null) {
                mMeManager.setMe(model.getUser());
                mMeManager.setIsNewUser(true);
            }

            //Post event for successful login that could be caught by anyone who is registered to EventBus.
            AccountRestServiceEvent.LoginSuccess loginSuccessEvent = new AccountRestServiceEvent.LoginSuccess();
            loginSuccessEvent.isNewUser = true;
            mEventBus.post(loginSuccessEvent);

            //get user interests from facebook
            mFacebookManager.requestMyLikes();
        } else {
            mEventBus.post(new AccountRestServiceEvent.LoginFailure());
        }
    }
}