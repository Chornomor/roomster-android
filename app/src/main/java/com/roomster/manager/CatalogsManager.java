package com.roomster.manager;


import android.content.Context;
import android.support.annotation.Nullable;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.cache.sharedpreferences.SharedPreferencesFiles;
import com.roomster.model.CatalogHelpdeskModel;
import com.roomster.model.CatalogModel;
import com.roomster.model.CatalogValueModel;
import com.roomster.model.CountryCatalogModel;
import com.roomster.model.LocaleCatalogModel;

import javax.inject.Inject;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Bogoi on 12/29/2015.
 */
public class CatalogsManager {

    public static final int HOUSEHOLD_SEX_EVERYONE_ID = -1;

    private static final String HOUSEHOLD_SEX_NAME      = "Everyone";
    private static final int    PEOPLE_IN_HOUSEHOLD_MAX = 15;
    private static final String CURRENCIES              = "currencies";
    private static final String LOCALE                  = "supported_locales";
    private static final String APARTMENT_TYPES         = "apartment_types";
    private static final String APARTMENT_AMENITIES     = "apt_amenities";
    private static final String ROOM_AMENITIES          = "room_amenities";
    private static final String HOUSEHOLD_SEX           = "household_sex_category";
    private static final String BATHROOMS               = "bathrooms";
    private static final String APT_SIZE_UNITS          = "apt_size_units";
    private static final String SPOKEN_LANGUAGES        = "spoken_languages";
    private static final String COUNTRIES               = "countries";
    private static final String STATES                  = "states";
    private static final String PROVINCES               = "provinces";
    private static final String SOCIAL_NETWORKS         = "social_networks";
    private static final String GENDER_ENUM             = "genders_enum";
    private static final String SERVICE_TYPES           = "service_types";
    private static final String AUTH_PROVIDERS          = "auth_providers";
    private static final String IMAGE_ENTITY_TYPES      = "image_entity_types";
    private static final String LANGUAGES               = "languages";
    private static final String MINIMUM_STAY            = "minimum_stay";
    private static final String GUESTS                  = "guests";
    private static final String BEDROOMS                = "bedrooms";
    private static final String SEX_CATEGORY            = "sex_category";
    private static final String ZODIAC_CATEGORY         = "zodiac_category";
    private static final String PETS_PREFERRED          = "pets_preferred";
    private static final String PETS_OWNED              = "pets_owned";
    private static final String CLEANLINESS             = "cleanliness";
    private static final String OVERNIGHT_GUESTS        = "overnight_guests";
    private static final String PARTY_HABITS            = "party_habits";
    private static final String GET_UP                  = "get_up";
    private static final String GO_TO_BED               = "go_to_bed";
    private static final String FOOD_PREFERENCE         = "food_preference";
    private static final String SMOKING_HABITS          = "smoking_habits";
    private static final String WORK_SCHEDULE           = "work_schedule";
    private static final String SMOKING_PREFERENCE      = "smoking_preference";
    private static final String OCCUPATION              = "occupation";
    private static final String BUILDING_TYPES          = "building_types";
    private static final String HELPDESK_SUBJECTS          = "helpdesk_subjects";

    private Type               mCatalogModelListType;
    private Type               mCatalogValueModelListType;
    private Type               mCatalogStringType;
    private Type               mCatalogLocaleType;
    private Type               mCatalogCountryType;
    private Type               mCatalogHelpdeskType;
    private List<CatalogModel> mPeopleInHousehold;

    @Inject
    SharedPreferencesFiles mSharedPreferencesFiles;
    @Inject
    Context context;

    public CatalogsManager() {
        RoomsterApplication.getRoomsterComponent().inject(this);
        mCatalogValueModelListType = new TypeToken<List<CatalogValueModel>>() {

        }.getType();

        mCatalogModelListType = new TypeToken<List<CatalogModel>>() {

        }.getType();

        mCatalogStringType = new TypeToken<List<String>>() {

        }.getType();

        mCatalogLocaleType = new TypeToken<List<LocaleCatalogModel>>(){

        }.getType();
        mCatalogCountryType = new TypeToken<List<CountryCatalogModel>>() {

        }.getType();
        mCatalogHelpdeskType = new TypeToken<List<CatalogHelpdeskModel>>(){
        }.getType();
        mPeopleInHousehold = new ArrayList<>();
        for (int i = 1; i <= PEOPLE_IN_HOUSEHOLD_MAX; i++) {
            mPeopleInHousehold.add(new CatalogModel(i, String.valueOf(i)));
        }
    }


    public List<CatalogModel> getPeopleInHouseholdList() {
        return mPeopleInHousehold;
    }


    public void setCurrenciesList(List<String> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(CURRENCIES, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<String> getCurrenciesList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(CURRENCIES, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogStringType);
        } else {
            return null;
        }
    }


    @Nullable
    public List<CountryCatalogModel> getCountriesList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(COUNTRIES, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogCountryType);
        } else {
            return null;
        }
    }


    public void setCountriesList(List<CountryCatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(COUNTRIES, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<LocaleCatalogModel> getLocalesList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(LOCALE, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogLocaleType);
        } else {
            return null;
        }
    }

    
    public void setLocalesList(List<LinkedTreeMap> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(LOCALE, new Gson().toJson(catalog)).commit();
    }


    public void setBathroomsList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(BATHROOMS, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getBathroomsList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(BATHROOMS, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setApartmentTypesList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(APARTMENT_TYPES, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getApartmentTypesList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(APARTMENT_TYPES, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setApartmentAmenitiesList(List<CatalogValueModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit()
                .putString(APARTMENT_AMENITIES, new Gson().toJson(catalog)).commit();
    }


    @Nullable
    public List<CatalogValueModel> getApartmentAmenitiesList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(APARTMENT_AMENITIES, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogValueModelListType);
        } else {
            return null;
        }
    }


    public void setRoomAmenitiesList(List<CatalogValueModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(ROOM_AMENITIES, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogValueModel> getRoomAmenitiesList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(ROOM_AMENITIES, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogValueModelListType);
        } else {
            return null;
        }
    }
    public void setHelpdeskSubjects(List<CatalogHelpdeskModel> catalog){
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(HELPDESK_SUBJECTS, new Gson().toJson(catalog))
                .commit();
    }
    public List<CatalogHelpdeskModel> getHelpdeskSubjects(){
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(HELPDESK_SUBJECTS, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogHelpdeskType);
        } else {
            return null;
        }
    }

    public void setHouseholdSexList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(HOUSEHOLD_SEX, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getHouseholdSexList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(HOUSEHOLD_SEX, null);
        if (catalogJson != null) {
            List<CatalogModel> list = new Gson().fromJson(catalogJson, mCatalogModelListType);
            if (list == null) {
                list = new ArrayList<>();
            }
            return list;
        } else {
            return null;
        }
    }


    public void setAptSizeUnitsList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(APT_SIZE_UNITS, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getAptSizeUnitsList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(APT_SIZE_UNITS, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setSpokenLanguagesList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(SPOKEN_LANGUAGES, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getSpokenLanguagesList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(SPOKEN_LANGUAGES, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setStatesList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(STATES, new Gson().toJson(catalog)).commit();
    }


    @Nullable
    public List<CatalogModel> getStatesList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(STATES, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setProvincesList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(PROVINCES, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getProvincesList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(PROVINCES, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setSocialNetworksList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(SOCIAL_NETWORKS, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getSocialNetworksList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(SOCIAL_NETWORKS, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setGenderEnumList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(GENDER_ENUM, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getGenderEnumList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(GENDER_ENUM, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setServiceTypesList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(SERVICE_TYPES, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getServiceTypesList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(SERVICE_TYPES, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    @Nullable
    public CatalogModel getServiceTypeById(Integer serviceTypeId) {
        List<CatalogModel> serviceTypes = getServiceTypesList();
        if (serviceTypes != null) {
            for (CatalogModel catalogModel : serviceTypes) {
                if (catalogModel.getId() == serviceTypeId) {
                    return catalogModel;
                }
            }
        }
        return null;
    }


    @Nullable
    public CatalogModel getBathroomById(Integer bathroomId) {
        List<CatalogModel> bathrooms = getBathroomsList();
        if (bathrooms != null) {
            for (CatalogModel catalogModel : bathrooms) {
                if (catalogModel.getId() == bathroomId) {
                    return catalogModel;
                }
            }
        }
        return null;
    }


    @Nullable
    public CatalogModel getBedroomById(Integer bedroomId) {
        List<CatalogModel> bedrooms = getBedroomsList();
        if (bedrooms != null) {
            for (CatalogModel catalogModel : bedrooms) {
                if (catalogModel.getId() == bedroomId) {
                    return catalogModel;
                }
            }
        }
        return null;
    }


    @Nullable
    public CatalogModel getApartmentTypeById(Integer apartmentTypeId) {
        List<CatalogModel> apartmentTypes = getApartmentTypesList();
        if (apartmentTypes != null) {
            for (CatalogModel catalogModel : apartmentTypes) {
                if (catalogModel.getId() == apartmentTypeId) {
                    return catalogModel;
                }
            }
        }
        return null;
    }


    @Nullable
    public CatalogModel getApartmentSizeUnitById(Integer apartmentSizeUnitId) {
        List<CatalogModel> aptSizeUnitsList = getAptSizeUnitsList();
        if (aptSizeUnitsList != null) {
            for (CatalogModel catalogModel : aptSizeUnitsList) {
                if (catalogModel.getId() == apartmentSizeUnitId) {
                    return catalogModel;
                }
            }
        }
        return null;
    }


    @Nullable
    public CatalogModel getMinimumStayById(Integer minimumStayId) {
        List<CatalogModel> minumumStayList = getMinimumStayList();
        if (minumumStayList != null) {
            for (CatalogModel catalogModel : minumumStayList) {
                if (catalogModel.getId() == minimumStayId) {
                    return catalogModel;
                }
            }
        }
        return null;
    }


    public void setAuthProvidersList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(AUTH_PROVIDERS, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getAuthProvidersList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(AUTH_PROVIDERS, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setImageEntityTypesList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit()
                .putString(IMAGE_ENTITY_TYPES, new Gson().toJson(catalog)).commit();
    }


    @Nullable
    public List<CatalogModel> getImageEntityTypesList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(IMAGE_ENTITY_TYPES, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setLanguagesList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(LANGUAGES, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getLanguagesList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(LANGUAGES, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setMinimumStayList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(MINIMUM_STAY, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getMinimumStayList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(MINIMUM_STAY, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setGuestsList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(GUESTS, new Gson().toJson(catalog)).commit();
    }


    @Nullable
    public List<CatalogModel> getGuestsList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(GUESTS, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setBedroomsList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(BEDROOMS, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getBedroomsList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(BEDROOMS, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setSexCategoriesList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(SEX_CATEGORY, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getSexCategoriesList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(SEX_CATEGORY, null);
        if (catalogJson != null) {
            List<CatalogModel> list = new Gson().fromJson(catalogJson, mCatalogModelListType);
            if (list == null) {
                list = new ArrayList<>();
            }
            list.add(new CatalogModel(HOUSEHOLD_SEX_EVERYONE_ID,
                    context.getResources().getString(R.string.discovery_household_sex_everyone)));
            return list;
        } else {
            return null;
        }
    }


    public void setZodiacCategoriesList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(ZODIAC_CATEGORY, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getZodiacCategoriesList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(ZODIAC_CATEGORY, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setPetsPreferredList(List<CatalogValueModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(PETS_PREFERRED, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogValueModel> getPetsPreferredList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(PETS_PREFERRED, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogValueModelListType);
        } else {
            return null;
        }
    }


    public void setPetsOwnedList(List<CatalogValueModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(PETS_OWNED, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogValueModel> getPetsOwnedList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(PETS_OWNED, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogValueModelListType);
        } else {
            return null;
        }
    }


    public void setCleanlinessList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(CLEANLINESS, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getCleanlinessList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(CLEANLINESS, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setOvernightGuestsList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(OVERNIGHT_GUESTS, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getOvernightGuestsList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(OVERNIGHT_GUESTS, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setPartyHabitsList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(PARTY_HABITS, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getPartyHabitsList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(PARTY_HABITS, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setGetUpList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(GET_UP, new Gson().toJson(catalog)).commit();
    }


    @Nullable
    public List<CatalogModel> getGetUpList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(GET_UP, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setGoToBedList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(GO_TO_BED, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getGoToBedList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(GO_TO_BED, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setFoodPreferencesList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(FOOD_PREFERENCE, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getFoodPreferencesList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(FOOD_PREFERENCE, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setSmokingHabitsList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(SMOKING_HABITS, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getSmokingHabitsList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(SMOKING_HABITS, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setWorkScheduleList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(WORK_SCHEDULE, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getWorkScheduleList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(WORK_SCHEDULE, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setSmokingPreferencesList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit()
                .putString(SMOKING_PREFERENCE, new Gson().toJson(catalog)).commit();
    }


    @Nullable
    public List<CatalogModel> getSmokingPreferencesList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(SMOKING_PREFERENCE, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setOccupationList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(OCCUPATION, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getOccupationList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(OCCUPATION, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public void setBuildingTypesList(List<CatalogModel> catalog) {
        mSharedPreferencesFiles.getAppSharedPreferences().edit().putString(BUILDING_TYPES, new Gson().toJson(catalog))
                .commit();
    }


    @Nullable
    public List<CatalogModel> getBuildingTypesList() {
        String catalogJson = mSharedPreferencesFiles.getAppSharedPreferences().getString(BUILDING_TYPES, null);
        if (catalogJson != null) {
            return new Gson().fromJson(catalogJson, mCatalogModelListType);
        } else {
            return null;
        }
    }


    public CatalogModel getCatalogModeltModelById(List<CatalogModel> modelList, int id) {
        for (CatalogModel model : modelList) {
            if (id == model.getId()) {
                return model;
            }
        }
        return null;
    }


    public CatalogValueModel getCatalogValueModelModelById(List<CatalogValueModel> modelList, long id) {
        for (CatalogValueModel model : modelList) {
            if (id == model.getId()) {
                return model;
            }
        }
        return null;
    }


    public CatalogValueModel getCatalogValueModelModelByValue(List<CatalogValueModel> modelList, long value) {
        for (CatalogValueModel model : modelList) {
            if (value == model.getValue()) {
                return model;
            }
        }
        return null;
    }
}