package com.roomster.activity;


import com.google.android.gms.analytics.HitBuilders;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.SearchRestServiceEvent;
import com.roomster.fragment.MapClusterListFragment;
import com.roomster.fragment.MapListingsListFragment;
import com.roomster.manager.SearchManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;


/**
 * Created by "Michael Katkov" on 12/20/2015.
 */
public class MapListViewActivity extends BaseToolbarActivity {

    @Inject
    SearchManager searchManager;

    @Inject
    EventBus mEventBus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        Fragment fragment;
        if(getIntent().hasExtra(MapClusterListFragment.KEY_BOUNDS)){
            fragment = MapClusterListFragment.getInstance(getIntent().getExtras());
        } else {
            fragment = new MapListingsListFragment();
        }
        setContent(fragment);
    }


    @Override
    public void onStart() {
        super.onStart();
        if (!this.mEventBus.isRegistered(this)) {
            this.mEventBus.register(this);
        }
    }


    @Override
    public void onStop() {
        if (this.mEventBus.isRegistered(this)) {
            this.mEventBus.unregister(this);
        }
        super.onStop();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(SearchRestServiceEvent.SearchSuccess event) {
        setupTitle();
    }

    //TODO // FIXME: 24.10.16
    /*
    we have to use method setupTitle in onResume because eventbus don't
    catch event in 1st time for unknown reason
     */
    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName(getString(R.string.screen_name_map_listings_list));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        setupTitle();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        searchManager.clearResultModel();
        searchManager.setMapViewItems(null);
    }

    private void setupTitle() {
        if(searchManager.getMapViewItems() != null) {
            mTitleTv.setText(getResources().getString(R.string.listings) + " (" + searchManager.getMapViewItems().size() + ")");
        } else if (searchManager.getResultModel() != null){
            mTitleTv.setText(getResources().getString(R.string.listings) + " (" + searchManager.getResultModel().getCount() + ")");
        }
    }
}
