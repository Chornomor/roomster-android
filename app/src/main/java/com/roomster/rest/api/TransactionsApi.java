package com.roomster.rest.api;


import com.roomster.rest.model.TransactionsModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;


public interface TransactionsApi {

    /**
     * Get billing transactions
     *
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<List<BillingHistory>>
     */

    @GET("Transactions")
    Call<List<TransactionsModel>> transactionsGet(@Header("Authorization") String authorization);
}
