package com.roomster.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bogoi.Bogdanov on 3/2/2016.
 */
public class AndroidReceiptModel {

    /**
     * Indicates whether the subscription renews automatically.
     * If true, the subscription is active, and will automatically renew on the next billing date.
     * If false, indicates that the user has canceled the subscription.
     * The user has access to subscription content until the next billing date and will lose access
     * at that time unless they re-enable automatic renewal (or manually renew, as described in Manual Renewal).
     * If you offer a grace period, this value remains set to true for all subscriptions, as long as the grace period has not
     * lapsed.
     * The next billing date is extended dynamically every day until the end of the grace period or until the user fixes their
     * payment method.
     */
    @SerializedName("autoRenewing")
    private Boolean autoRenewing;

    /**
     * A unique order identifier for the transaction.
     * This identifier corresponds to the Google payments order ID.
     * If the order is a test purchase made through the In-app Billing Sandbox, orderId is blank.
     */
    @SerializedName("orderId")
    private String orderId;

    /**
     * The application package from which the purchase originated.
     */
    @SerializedName("packageName")
    private String packageName;

    /**
     * The item's product identifier.
     * Every item has a product ID, which you must specify in the application's product list on the Google Play Developer
     * Console.
     */
    @SerializedName("productId")
    private String productId;

    /**
     * The time the product was purchased, in milliseconds since the epoch (Jan 1, 1970).
     */
    @SerializedName("purchaseTime")
    private Long purchaseTime;

    /**
     * The purchase state of the order.
     * Possible values are 0 (purchased), 1 (canceled), or 2 (refunded). = ['Purchased', 'Canceled', 'Refunded']
     */
    @SerializedName("purchaseState")
    private Integer purchaseState;

    /**
     * A developer-specified string that contains supplemental information about an order.
     * You can specify a value for this field when you make a getBuyIntent request.
     */
    @SerializedName("developerPayload")
    private String developerPayload;

    /**
     * A token that uniquely identifies a purchase for a given item and user pair.
     */
    @SerializedName("purchaseToken")
    private String purchaseToken;

    public Boolean getAutoRenewing() {
        return autoRenewing;
    }

    public void setAutoRenewing(Boolean autoRenewing) {
        this.autoRenewing = autoRenewing;
    }

    public String getDeveloperPayload() {
        return developerPayload;
    }

    public void setDeveloperPayload(String developerPayload) {
        this.developerPayload = developerPayload;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getPurchaseState() {
        return purchaseState;
    }

    public void setPurchaseState(Integer purchaseState) {
        this.purchaseState = purchaseState;
    }

    public Long getPurchaseTime() {
        return purchaseTime;
    }

    public void setPurchaseTime(Long purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    public String getPurchaseToken() {
        return purchaseToken;
    }

    public void setPurchaseToken(String purchaseToken) {
        this.purchaseToken = purchaseToken;
    }
}
