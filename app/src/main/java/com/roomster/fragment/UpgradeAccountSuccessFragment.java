package com.roomster.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.upgradeaccount.UpgradeAccountEvent;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class UpgradeAccountSuccessFragment extends Fragment {

    @Inject
    EventBus mEventBus;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_upgrade_succes, container, false);
        ButterKnife.bind(this, view);
        RoomsterApplication.getRoomsterComponent().inject(this);

        return view;
    }


    @OnClick(R.id.upgrade_success_button)
    void onContinueClicked() {
        mEventBus.post(new UpgradeAccountEvent.SuccessContinue());
    }
}
