package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


public class LocalizationSettingsModel {

    @SerializedName("currency")
    private String currency;

    @SerializedName("locale")
    private String locale;


    public String getCurrency() {
        return currency;
    }


    public String getLocale() {
        return locale;
    }


    public void setCurrency(String currency) {
        this.currency = currency;
    }


    public void setLocale(String locale) {
        this.locale = locale;
    }

}
