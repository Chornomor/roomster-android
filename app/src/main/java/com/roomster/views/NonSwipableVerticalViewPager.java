package com.roomster.views;


import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import fr.castorflex.android.verticalviewpager.VerticalViewPager;


/**
 * Created by michaelkatkov on 1/4/16.
 */
public class NonSwipableVerticalViewPager extends VerticalViewPager {

    private boolean enabled;


    public NonSwipableVerticalViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.enabled = true;
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.enabled) {
            return super.onTouchEvent(event);
        }

        return false;
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        try {
            if (this.enabled) {
                return super.onInterceptTouchEvent(event);
            } else {
                return false;
            }
        } catch (IllegalArgumentException e) {
            return false;
        }
    }


    public void setPagingEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
