/***************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ***************************************************************/
package com.roomster.adapter;


import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.model.CatalogValueModel;
import com.roomster.utils.AmenityUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;


public class AmenitiesGridAdapter extends BaseAdapter {

    public interface OnAmenityItemClick {

        void onItemClick(CatalogValueModel model, boolean isSelected);
    }


    @Inject
    LayoutInflater layoutInflater;

    private List<CatalogValueModel> mStoredAmenities;
    private List<CatalogValueModel> mAllAmenities;
    private OnAmenityItemClick      mListener;
    private boolean                 mCompareValues;
    private int                     mDefaultColor;
    private int                     mSelectedColor;


    public AmenitiesGridAdapter(Context context, List<CatalogValueModel> allAmenities, List<CatalogValueModel> selectedAmenities,
                                boolean compareValues) {
        if (allAmenities != null) {
            mAllAmenities = new ArrayList<>(allAmenities);
            sortAmenitiesAlphabetically();
        } else {
            mAllAmenities = new ArrayList<>();
        }
        mCompareValues = compareValues;
        if (context != null) {
            mSelectedColor = ContextCompat.getColor(context, R.color.orange);
            mDefaultColor = ContextCompat.getColor(context, R.color.black);
        }
        if (selectedAmenities != null) {
            mStoredAmenities = new ArrayList<>(selectedAmenities);
        } else {
            mStoredAmenities = new ArrayList<>();
        }
        RoomsterApplication.getRoomsterComponent().inject(this);
    }

    @Override
    public int getCount() {
        return mAllAmenities.size();
    }


    @Override
    public Object getItem(int position) {
        return mAllAmenities.get(position);
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        final ViewHolder holder;
        if (v == null) {
            v = layoutInflater.inflate(R.layout.view_amenity, parent, false);
            holder = new ViewHolder();
            holder.amenityImage = (ImageView) v.findViewById(R.id.amenity_image);
            holder.amenityText = (TextView) v.findViewById(R.id.amenity_text);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        final CatalogValueModel amenityModel = (CatalogValueModel) getItem(position);
        Integer amenityImageResId = AmenityUtil.getAmenityImageByName(amenityModel.getId());
        if (amenityImageResId != null) {
            holder.amenityImage.setImageResource(amenityImageResId);
            holder.amenityText.setText(amenityModel.getName());
            if (isInCheckedList(amenityModel)) {
                holder.setChecked(true);
            } else {
                holder.setChecked(false);
            }
        }

        v.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (isInCheckedList(amenityModel)) {
                    removeCheckedByValue(amenityModel);
                    holder.setChecked(false);
                    if (mListener != null) {
                        mListener.onItemClick(amenityModel, false);
                    }
                } else {
                    mStoredAmenities.add(amenityModel);
                    holder.setChecked(true);
                    if (mListener != null) {
                        mListener.onItemClick(amenityModel, true);
                    }
                }
            }
        });
        return v;
    }


    public void setOnAmenityItemClickListener(OnAmenityItemClick listener) {
        mListener = listener;
    }


    public List<CatalogValueModel> getSelected() {
        return mStoredAmenities;
    }


    private void removeCheckedByValue(CatalogValueModel model) {
        if (model != null && mStoredAmenities != null) {
            for (CatalogValueModel checkedModel : mStoredAmenities) {
                if (checkedModel != null && checkedModel.getValue() == (model.getValue())) {
                    mStoredAmenities.remove(checkedModel);
                    return;
                }
            }
        }
    }


    private boolean isInCheckedList(CatalogValueModel model) {
        if (model != null && mStoredAmenities != null) {
            for (CatalogValueModel checkedModel : mStoredAmenities) {
                if (mCompareValues) {
                    if (checkedModel != null && checkedModel.getValue() == (model.getValue())) {
                        return true;
                    }
                } else {
                    if (checkedModel != null && checkedModel.getId() == (model.getId())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void sortAmenitiesAlphabetically() {
        Collections.sort(mAllAmenities, new Comparator<CatalogValueModel>() {

            @Override
            public int compare(CatalogValueModel lhs, CatalogValueModel rhs) {
                if (lhs != null && rhs != null) {
                    return lhs.getName().compareTo(rhs.getName());
                } else {
                    return 0;
                }
            }
        });
    }


    private class ViewHolder {

        public ImageView amenityImage;

        public TextView amenityText;


        void setChecked(boolean isChecked) {
            if (isChecked) {
                amenityImage.setColorFilter(mSelectedColor, PorterDuff.Mode.SRC_ATOP);
                amenityText.setTextColor(mSelectedColor);
            } else {
                amenityImage.clearColorFilter();
                amenityText.setTextColor(mDefaultColor);
            }
        }
    }
}


