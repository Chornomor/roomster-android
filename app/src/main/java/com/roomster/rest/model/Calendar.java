package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;


public class Calendar implements Serializable {

    /**
     **/
    @SerializedName("date_in")
    private Date dateIn = null;

    /**
     **/
    @SerializedName("date_out")
    private Date dateOut = null;

    /**
     * Allowed values @ /v1/catalogues/minimum_stay
     **/
    @SerializedName("minimum_stay_id")
    private Integer minimumStayId = null;


    public Date getDateIn() {
        return dateIn;
    }


    public void setDateIn(Date dateIn) {
        this.dateIn = dateIn;
    }


    public Date getDateOut() {
        return dateOut;
    }


    public void setDateOut(Date dateOut) {
        this.dateOut = dateOut;
    }


    public Integer getMinimumStayId() {
        return minimumStayId;
    }


    public void setMinimumStayId(Integer minimumStayId) {
        this.minimumStayId = minimumStayId;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Calendar {\n");

        sb.append("  dateIn: ").append(dateIn).append("\n");
        sb.append("  dateOut: ").append(dateOut).append("\n");
        sb.append("  minimumStayId: ").append(minimumStayId).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
