package com.roomster.fragment;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.roomster.R;


/**
 * Created by michaelkatkov on 11/18/15.
 */
public class ListingMapFragment extends BaseMapFragment {

    private static final String TAG        = "ListingMapFragment";
    private static final int    ZOOM_LEVEL = 15;
    private static final String LATITUDE   = "latitude";
    private static final String LONGITUDE  = "longitude";

    private double mLatitude;
    private double mLongitude;


    public static ListingMapFragment newInstance(double latitude, double longitude) {
        ListingMapFragment fragment = new ListingMapFragment();
        Bundle args = new Bundle();
        args.putDouble(LATITUDE, latitude);
        args.putDouble(LONGITUDE, longitude);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mLatitude = getArguments().getDouble(LATITUDE);
            mLongitude = getArguments().getDouble(LONGITUDE);
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (mGoogleMap != null) {
            mGoogleMap.getUiSettings().setScrollGesturesEnabled(false);
        }
        return view;
    }


    private MarkerOptions getMarker(double latitude, double longitude, int drawableId) {
        return new MarkerOptions().position(new LatLng(latitude, longitude))
          .icon(BitmapDescriptorFactory.fromResource(drawableId));
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleMap != null) {
            mGoogleMap.addMarker(getMarker(mLatitude, mLongitude, R.drawable.map_custom_pin));
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLatitude, mLongitude), ZOOM_LEVEL));
        }
    }
}
