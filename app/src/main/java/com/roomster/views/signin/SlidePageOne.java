package com.roomster.views.signin;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.roomster.R;

/**
 * Created by andreybofanov on 18.07.16.
 */
public class SlidePageOne extends AbstactSlidePage  {

    public SlidePageOne(Context context) {
        super(context);
        init();
    }

    public SlidePageOne(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SlidePageOne(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    public void init(){
        View v = LayoutInflater.from(getContext()).inflate(R.layout.signin_slide_layout_1,null);
        addView(v);
        TextView tv = (TextView) v.findViewById(R.id.tv_text);
        tv.setText(Html.fromHtml(getContext().getString(R.string.signin_slide_1)));
        layout = (ViewGroup) v.findViewById(R.id.text_layout);
        setTypefaces();
    }

}
