package com.roomster.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

import com.roomster.R;

/**
 * Created by alexchern on 26.07.16.
 */
public class NonEditableEditText extends EditText {
    private final String RESET = "reset";
    private String nonEditableText;

    public NonEditableEditText(Context context) {
        super(context);
    }

    public NonEditableEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public NonEditableEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (isInEditMode())
            return;

        if (null == attrs) {
            throw new IllegalArgumentException("Attributes should be provided to this view,");
        }
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.NonEditableEditText);
        nonEditableText = typedArray.getString(R.styleable.NonEditableEditText_non_editable_text);
        addTextChangedListener(textWatcher);
    }

    public void resetText(){
        setText("");
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!s.toString().equals("") && !s.toString().startsWith(nonEditableText)){
                setText(nonEditableText);
                Selection.setSelection(getText(), getText().length());
            }
        }
    };
}
