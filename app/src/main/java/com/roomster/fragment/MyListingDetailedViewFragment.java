package com.roomster.fragment;


import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.roomster.R;
import com.roomster.rest.model.ResultItemModel;


public class MyListingDetailedViewFragment extends DetailedViewFragment {

    public static MyListingDetailedViewFragment newInstance(ResultItemModel item) {
        MyListingDetailedViewFragment fragment = new MyListingDetailedViewFragment();

        //Save the listing item so to be restored on fragment recreation
        Bundle args = new Bundle();
        args.putSerializable(LISTING, item);

        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected void setUpDetailedView() {
        super.setUpDetailedView();
        mDetailedView.mBookmark.setVisibility(View.GONE);
        mDetailedView.setPrice(mListing.getListing().getRates().getMonthlyRate(),
                mListing.getListing().getRates().getCurrency());
        mDetailedView.displayProfileActions(false);
    }


    @Override
    protected void onCreateToolbarMenu(Menu menu) {
        super.onCreateToolbarMenu(menu);
        MenuItem bookmarkMenuItem = menu.findItem(R.id.contact_action_bookmark);
        if (bookmarkMenuItem != null) {
            bookmarkMenuItem.setVisible(false);
        }
        MenuItem chatMenuItem = menu.findItem(R.id.contact_action_message);
        if (chatMenuItem != null) {
            chatMenuItem.setVisible(false);
        }

        MenuItem callMenuItem = menu.findItem(R.id.contact_action_call);
        if (callMenuItem != null) {
            callMenuItem.setVisible(false);
        }

        MenuItem smsMenuItem = menu.findItem(R.id.contact_action_sms);
        if (smsMenuItem != null) {
            smsMenuItem.setVisible(false);
        }

        MenuItem reportMenuItem = menu.findItem(R.id.contact_action_report);
        if (reportMenuItem != null) {
            reportMenuItem.setVisible(false);
        }

        MenuItem shareMenuItem = menu.findItem(R.id.contact_action_share);
        if (shareMenuItem != null) {
            shareMenuItem.setVisible(false);
        }
    }
}
