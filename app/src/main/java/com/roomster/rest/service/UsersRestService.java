package com.roomster.rest.service;


import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.TokenManager;
import com.roomster.rest.api.UsersApi;
import com.roomster.rest.model.UserBlockViewModel;
import com.roomster.rest.model.UserGetViewModel;
import com.roomster.rest.model.UserPutViewModel;
import com.roomster.rest.model.UserReportViewModel;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UsersRestService {

    @Inject
    UsersApi mUsersApi;

    @Inject
    TokenManager mTokenManager;

    @Inject
    NetworkManager mNetworkManager;


    public UsersRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void blockUser(final UserBlockViewModel blockViewModel, final RestServiceListener<String> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                UserPutViewModel putViewModel = new UserPutViewModel();
                putViewModel.setUserBlock(blockViewModel);
                callPutUserViewModel(result, putViewModel, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                listener.onFailure(code, throwable, msg);
            }
        });
    }


    public void reportUser(final UserReportViewModel reportViewModel, final RestServiceListener<String> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                UserPutViewModel putViewModel = new UserPutViewModel();
                putViewModel.setUserReport(reportViewModel);
                callPutUserViewModel(result, putViewModel, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                listener.onFailure(code, throwable, msg);
            }
        });
    }


    public void getUserById(final Long userId, final RestServiceListener<UserGetViewModel> listener) {
        callGetUserById(userId, listener);
    }


    private void callPutUserViewModel(String authToken, UserPutViewModel userPutViewModel,
                                      final RestServiceListener<String> listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<String> callPutUser = mUsersApi.usersPut(userPutViewModel, authToken);
            callPutUser.enqueue(new Callback<String>() {

                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            if (listener != null) {
                                listener.onSuccess(response.body());
                            }
                            break;
                        case ResponseCode.BAD_REQUEST:
                        case ResponseCode.SERVER_ERROR:
                        default:
                            if (listener != null) {
                                listener.onFailure(response.code(), null, null);
                            }
                    }
                }


                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    if (listener != null) {
                        listener.onFailure(-1, t, RoomsterApplication.context.getString(R.string.cant_make_rest_call_to_put_user));
                    }
                }
            });
        } else {
            if (listener != null) {
                listener.onFailure(-1, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
            }
        }
    }


    private void callGetUserById(final Long userId, final RestServiceListener<UserGetViewModel> listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<UserGetViewModel> getUser = mUsersApi.usersGet(userId);
            getUser.enqueue(new Callback<UserGetViewModel>() {

                @Override
                public void onResponse(Call<UserGetViewModel> call, Response<UserGetViewModel> response) {
                    if (listener != null) {
                        listener.onSuccess(response.body());
                    }
                }


                @Override
                public void onFailure(Call<UserGetViewModel> call, Throwable t) {
                    if (listener != null) {
                        listener.onFailure(-1, null, null);
                    }
                }
            });
        } else {
            if (listener != null) {
                listener.onFailure(-1, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
            }
        }
    }
}