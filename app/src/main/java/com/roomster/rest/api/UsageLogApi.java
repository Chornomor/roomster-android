package com.roomster.rest.api;


import com.roomster.rest.model.UsageLogViewModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;


public interface UsageLogApi {

    /**
     * Get usage log
     *
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<List<UsageLogViewModel>>
     */

    @GET("UsageLog")
    Call<List<UsageLogViewModel>> usageLogGet(@Header("Authorization") String authorization);
}
