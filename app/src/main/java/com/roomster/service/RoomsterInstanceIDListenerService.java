package com.roomster.service;


import com.google.android.gms.iid.InstanceIDListenerService;

import com.roomster.application.RoomsterApplication;
import com.roomster.manager.GcmManager;

import javax.inject.Inject;


/**
 * Created by Bogoi.Bogdanov on 12/9/2015.
 */
public class RoomsterInstanceIDListenerService extends InstanceIDListenerService {

    private static final String TAG = RoomsterInstanceIDListenerService.class.getCanonicalName();

    @Inject
    GcmManager mGcmManager;


    @Override
    public void onCreate() {
        super.onCreate();
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. This call is initiated by the
     * InstanceID provider.
     */
    @Override
    public void onTokenRefresh() {
        mGcmManager.refreshGcmToken();
    }
}