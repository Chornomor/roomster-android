package com.roomster.constants;

/**
 * Created by Bogoi.Bogdanov on 5/13/2016.
 */
public class PlayStore {

    public static final String PLAY_STORE_URL = "http://play.google.com/store/apps/details?id=";
}
