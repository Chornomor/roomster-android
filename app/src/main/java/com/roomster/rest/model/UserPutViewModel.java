package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


public class UserPutViewModel {

    /**
     **/
    @SerializedName("user_block")
    private UserBlockViewModel userBlock = null;

    /**
     **/
    @SerializedName("user_report")
    private UserReportViewModel userReport = null;


    public UserBlockViewModel getUserBlock() {
        return userBlock;
    }


    public void setUserBlock(UserBlockViewModel userBlock) {
        this.userBlock = userBlock;
    }


    public UserReportViewModel getUserReport() {
        return userReport;
    }


    public void setUserReport(UserReportViewModel userReport) {
        this.userReport = userReport;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class UserPutViewModel {\n");

        sb.append("  userBlock: ").append(userBlock).append("\n");
        sb.append("  userReport: ").append(userReport).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
