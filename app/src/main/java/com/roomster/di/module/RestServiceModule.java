package com.roomster.di.module;


import com.roomster.rest.service.AccountRestService;
import com.roomster.rest.service.AndroidReceiptRestService;
import com.roomster.rest.service.AppInstallsRestService;
import com.roomster.rest.service.BillingRefundsRestService;
import com.roomster.rest.service.BookmarksRestService;
import com.roomster.rest.service.CatalogRestService;
import com.roomster.rest.service.CheckAppRestService;
import com.roomster.rest.service.ConversationsRestService;
import com.roomster.rest.service.FbMutualFriendsService;
import com.roomster.rest.service.ImagesRestService;
import com.roomster.rest.service.InAppProductsRestService;
import com.roomster.rest.service.LikesRestService;
import com.roomster.rest.service.ListingsRestService;
import com.roomster.rest.service.LocaleRestService;
import com.roomster.rest.service.MeRestService;
import com.roomster.rest.service.MegaphoneRestService;
import com.roomster.rest.service.NotificationsRestService;
import com.roomster.rest.service.SearchRestService;
import com.roomster.rest.service.SubscriptionRestService;
import com.roomster.rest.service.SupportRestService;
import com.roomster.rest.service.TokenRestService;
import com.roomster.rest.service.TransactionsRestService;
import com.roomster.rest.service.UpgradeAccountRestService;
import com.roomster.rest.service.UserListingsRestService;
import com.roomster.rest.service.UserStatusRestService;
import com.roomster.rest.service.UsersRestService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


/**
 * Created by Bogoi.Bogdanov on 10/6/2015.
 */
@Module
public class RestServiceModule {

    @Singleton
    @Provides
    AccountRestService provideAccountRestService() {
        return new AccountRestService();
    }


    @Singleton
    @Provides
    BookmarksRestService provideBookmarksRestService() {
        return new BookmarksRestService();
    }


    @Singleton
    @Provides
    CatalogRestService provideCatalogRestService() {
        return new CatalogRestService();
    }


    @Singleton
    @Provides
    TokenRestService provideTokenRestService() {
        return new TokenRestService();
    }


    @Singleton
    @Provides
    ConversationsRestService provideConversationsRestService() {
        return new ConversationsRestService();
    }


    @Singleton
    @Provides
    MegaphoneRestService provideMegaphoneRestService() {
        return new MegaphoneRestService();
    }


    @Singleton
    @Provides
    BillingRefundsRestService provideBillingRestService() {
        return new BillingRefundsRestService();
    }

    @Singleton
    @Provides
    MeRestService provideMeRestService() {
        return new MeRestService();
    }


    @Singleton
    @Provides
    SupportRestService provideSupportRestService() {
        return new SupportRestService();
    }


    @Singleton
    @Provides
    UpgradeAccountRestService provideUpgradeAccountRestService() {
        return new UpgradeAccountRestService();
    }


    @Singleton
    @Provides
    SearchRestService provideSearchRestService() {
        return new SearchRestService();
    }


    @Singleton
    @Provides
    UsersRestService provideUsersRestService() {
        return new UsersRestService();
    }


    @Singleton
    @Provides
    UserListingsRestService provideUsersListingsRestService() {
        return new UserListingsRestService();
    }


    @Singleton
    @Provides
    ListingsRestService provideListingsRestService() {
        return new ListingsRestService();
    }


    @Singleton
    @Provides
    AppInstallsRestService provideAppInstallsRestService() {
        return new AppInstallsRestService();
    }


    @Singleton
    @Provides
    ImagesRestService provideImagesRestService() {
        return new ImagesRestService();
    }


    @Singleton
    @Provides
    FbMutualFriendsService provideFbMutualFriendsRestService() {
        return new FbMutualFriendsService();
    }


    @Singleton
    @Provides
    LikesRestService provideLikesRestService() {
        return new LikesRestService();
    }


    @Singleton
    @Provides
    InAppProductsRestService provideInAppProductsRestService() {
        return new InAppProductsRestService();
    }


    @Singleton
    @Provides
    AndroidReceiptRestService provideAndroidReceiptRestService() {
        return new AndroidReceiptRestService();
    }


    @Singleton
    @Provides
    SubscriptionRestService provideSubscriptionRestService() {
        return new SubscriptionRestService();
    }


    @Singleton
    @Provides
    TransactionsRestService provideTransactionsRestService() {
        return new TransactionsRestService();
    }


    @Singleton
    @Provides
    NotificationsRestService provideNotificationsRestService() {
        return new NotificationsRestService();
    }


    @Provides
    @Singleton
    LocaleRestService provideLocaleRestService() {
        return new LocaleRestService();
    }


    @Provides
    @Singleton
    CheckAppRestService provideCheckAppRestService() {
        return new CheckAppRestService();
    }

    @Provides
    @Singleton
    UserStatusRestService provideUserStatusRestService() {
        return new UserStatusRestService();
    }
}