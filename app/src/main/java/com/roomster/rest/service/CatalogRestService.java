
package com.roomster.rest.service;


import com.google.gson.internal.LinkedTreeMap;

import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.CatalogRestServiceEvent;
import com.roomster.manager.CatalogsManager;
import com.roomster.manager.DiscoveryPrefsManager;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.SettingsManager;
import com.roomster.model.CatalogHelpdeskModel;
import com.roomster.model.CatalogModel;
import com.roomster.model.CatalogValueModel;
import com.roomster.model.CountryCatalogModel;
import com.roomster.rest.api.CataloguesApi;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by "Michael Katkov" on 10/7/2015.
 */
public class CatalogRestService {

    private static final String TAG                 = "ResourceRestService";
    private static final String CURRENCIES          = "currencies";
    private static final String LOCALE              = "supported_locales";
    private static final String APARTMENT_TYPES     = "apartment_types";
    private static final String APARTMENT_AMENITIES = "apt_amenities";
    private static final String ROOM_AMENITIES      = "room_amenities";
    private static final String HOUSEHOLD_SEX       = "household_sex_category";
    private static final String BATHROOMS           = "bathrooms";
    private static final String APT_SIZE_UNITS      = "apt_size_units";
    private static final String SPOKEN_LANGUAGES    = "spoken_languages";
    private static final String COUNTRIES           = "countries";
    private static final String STATES              = "states";
    private static final String PROVINCES           = "provinces";
    private static final String SOCIAL_NETWORKS     = "social_networks";
    private static final String GENDER_ENUM         = "genders_enum";
    private static final String SERVICE_TYPES       = "service_types";
    private static final String AUTH_PROVIDERS      = "auth_providers";
    private static final String IMAGE_ENTITY_TYPES  = "image_entity_types";
    private static final String LANGUAGES           = "languages";
    private static final String MINIMUM_STAY        = "minimum_stay";
    private static final String GUESTS              = "guests";
    private static final String BEDROOMS            = "bedrooms";
    private static final String SEX_CATEGORY        = "sex_category";
    private static final String ZODIAC_CATEGORY     = "zodiac_category";
    private static final String PETS_PREFERRED      = "pets_preferred";
    private static final String PETS_OWNED          = "pets_owned";
    private static final String CLEANLINESS         = "cleanliness";
    private static final String OVERNIGHT_GUESTS    = "overnight_guests";
    private static final String PARTY_HABITS        = "party_habits";
    private static final String GET_UP              = "get_up";
    private static final String GO_TO_BED           = "go_to_bed";
    private static final String FOOD_PREFERENCE     = "food_preference";
    private static final String SMOKING_HABITS      = "smoking_habits";
    private static final String WORK_SCHEDULE       = "work_schedule";
    private static final String SMOKING_PREFERENCE  = "smoking_preference";
    private static final String OCCUPATION          = "occupation";
    private static final String BUILDING_TYPES      = "building_types";
    private static final String LANGUAGE_LOCATE_KEY = "locale";
    private static final String HELPDESK_SUBJECTS = "helpdesk_subjects";

    private static final String REQUEST = CURRENCIES + "," + LOCALE + "," + BATHROOMS + "," +
            APARTMENT_TYPES + "," + APARTMENT_AMENITIES + "," + ROOM_AMENITIES + "," + HOUSEHOLD_SEX +
            "," + APT_SIZE_UNITS + "," + SPOKEN_LANGUAGES + "," + COUNTRIES + "," + STATES + "," + PROVINCES + "," + SOCIAL_NETWORKS + "," + GENDER_ENUM + "," + SERVICE_TYPES + "," + AUTH_PROVIDERS + "," + IMAGE_ENTITY_TYPES + "," + LANGUAGES + "," + MINIMUM_STAY + "," + GUESTS + "," + BEDROOMS + "," + SEX_CATEGORY + "," + ZODIAC_CATEGORY + "," + PETS_PREFERRED + "," + PETS_OWNED + "," + CLEANLINESS + "," + OVERNIGHT_GUESTS + "," + PARTY_HABITS + "," + GET_UP + "," + GO_TO_BED + "," + FOOD_PREFERENCE + "," + SMOKING_HABITS + "," + WORK_SCHEDULE + "," + SMOKING_PREFERENCE + "," + OCCUPATION + "," + BUILDING_TYPES
            + "," + HELPDESK_SUBJECTS;

    private boolean mIsLoadingCatalogs;

    @Inject
    EventBus mEventBus;

    @Inject
    CataloguesApi mCataloguesApi;

    @Inject
    DiscoveryPrefsManager mDPrefsManager;

    @Inject
    CatalogsManager mCatalogsManager;

    @Inject
    SettingsManager mSettingsManager;

    @Inject
    NetworkManager mNetworkManager;


    public CatalogRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
        mIsLoadingCatalogs = false;
    }


    public void loadCatalogs() {
        if (mNetworkManager.hasNetworkAccess()) {
            if (!mIsLoadingCatalogs) {
                mIsLoadingCatalogs = true;
                Call<Object> call = mCataloguesApi.cataloguesGet(REQUEST, null, mSettingsManager.getLocale());
                call.enqueue(new Callback<Object>() {

                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        mIsLoadingCatalogs = false;
                        switch (response.code()) {
                            case ResponseCode.OK:
                                LinkedTreeMap<String, List> map = (LinkedTreeMap) response.body();
                                storeCatalogs(map);
                                loadLanguageCatalog();
                                break;
                            default:
                                mEventBus.post(new CatalogRestServiceEvent.GetCatalogFailure());
                        }
                    }


                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                        mEventBus.post(new CatalogRestServiceEvent.GetCatalogFailure());
                        mIsLoadingCatalogs = false;
                    }
                });
            }
        } else {
            mEventBus.post(new CatalogRestServiceEvent.GetCatalogFailure());
        }
    }


    private void loadLanguageCatalog() {
        if (!mIsLoadingCatalogs) {
            mIsLoadingCatalogs = true;
            Call<Object> call = mCataloguesApi.catalogLanguagesGet();
            call.enqueue(new Callback<Object>() {

                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    mIsLoadingCatalogs = false;
                    switch (response.code()) {
                        case ResponseCode.OK:
                            mCatalogsManager.setLocalesList((List<LinkedTreeMap>) response.body());
                            getSupportDropdown();
                            break;
                        default:
                            mEventBus.post(new CatalogRestServiceEvent.GetCatalogFailure());
                    }
                }


                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    mEventBus.post(new CatalogRestServiceEvent.GetCatalogFailure());
                    mIsLoadingCatalogs = false;
                }
            });
        }
    }

    private void getSupportDropdown(){
        mEventBus.post(new CatalogRestServiceEvent.GetCatalogSuccess());
    }


    private List<String> getLanguagesFromResponse(List<LinkedTreeMap> response){
        List<String> languageList = new ArrayList<>();
        for(LinkedTreeMap map : response) {
            languageList.add(map.get(LANGUAGE_LOCATE_KEY).toString());
        }
        return languageList;
    }


    private void storeCatalogs(LinkedTreeMap<String, List> map) {
        mCatalogsManager.setCurrenciesList(map.get(CURRENCIES));
        mCatalogsManager.setCountriesList(parseCountriesCatalog(map.get(COUNTRIES)));
        mCatalogsManager.setBathroomsList(parseCatalog(map.get(BATHROOMS)));
        mCatalogsManager.setApartmentTypesList(parseCatalog(map.get(APARTMENT_TYPES)));
        mCatalogsManager.setApartmentAmenitiesList(parseValueCatalog(map.get(APARTMENT_AMENITIES)));
        mCatalogsManager.setRoomAmenitiesList(parseValueCatalog(map.get(ROOM_AMENITIES)));
        mCatalogsManager.setHouseholdSexList(parseCatalog(map.get(HOUSEHOLD_SEX)));
        mCatalogsManager.setAptSizeUnitsList(parseCatalog(map.get(APT_SIZE_UNITS)));
        mCatalogsManager.setSpokenLanguagesList(parseCatalog(map.get(SPOKEN_LANGUAGES)));
        mCatalogsManager.setStatesList(parseCatalog(map.get(STATES)));
        mCatalogsManager.setProvincesList(parseCatalog(map.get(PROVINCES)));
        mCatalogsManager.setSocialNetworksList(parseCatalog(map.get(SOCIAL_NETWORKS)));
        mCatalogsManager.setGenderEnumList(parseCatalog(map.get(GENDER_ENUM)));
        mCatalogsManager.setServiceTypesList(parseCatalog(map.get(SERVICE_TYPES)));
        mCatalogsManager.setAuthProvidersList(parseCatalog(map.get(AUTH_PROVIDERS)));
        mCatalogsManager.setImageEntityTypesList(parseCatalog(map.get(IMAGE_ENTITY_TYPES)));
        mCatalogsManager.setLanguagesList(parseCatalog(map.get(LANGUAGES)));
        mCatalogsManager.setMinimumStayList(parseCatalog(map.get(MINIMUM_STAY)));
        mCatalogsManager.setGuestsList(parseCatalog(map.get(GUESTS)));
        mCatalogsManager.setBedroomsList(parseCatalog(map.get(BEDROOMS)));
        mCatalogsManager.setSexCategoriesList(parseCatalog(map.get(SEX_CATEGORY)));
        mCatalogsManager.setZodiacCategoriesList(parseCatalog(map.get(ZODIAC_CATEGORY)));
        mCatalogsManager.setPetsPreferredList(parseValueCatalog(map.get(PETS_PREFERRED)));
        mCatalogsManager.setPetsOwnedList(parseValueCatalog(map.get(PETS_OWNED)));
        mCatalogsManager.setCleanlinessList(parseCatalog(map.get(CLEANLINESS)));
        mCatalogsManager.setOvernightGuestsList(parseCatalog(map.get(OVERNIGHT_GUESTS)));
        mCatalogsManager.setPartyHabitsList(parseCatalog(map.get(PARTY_HABITS)));
        mCatalogsManager.setGetUpList(parseCatalog(map.get(GET_UP)));
        mCatalogsManager.setGoToBedList(parseCatalog(map.get(GO_TO_BED)));
        mCatalogsManager.setFoodPreferencesList(parseCatalog(map.get(FOOD_PREFERENCE)));
        mCatalogsManager.setSmokingHabitsList(parseCatalog(map.get(SMOKING_HABITS)));
        mCatalogsManager.setWorkScheduleList(parseCatalog(map.get(WORK_SCHEDULE)));
        mCatalogsManager.setSmokingPreferencesList(parseCatalog(map.get(SMOKING_PREFERENCE)));
        mCatalogsManager.setOccupationList(parseCatalog(map.get(OCCUPATION)));
        mCatalogsManager.setBuildingTypesList(parseCatalog(map.get(BUILDING_TYPES)));
        mCatalogsManager.setHelpdeskSubjects(parseHelpdeskCatalog(map.get(HELPDESK_SUBJECTS)));
    }


    private List<CatalogModel> parseCatalog(List<LinkedTreeMap<String, Object>> types) {
        if (types != null) {
            List<CatalogModel> catalogList = new ArrayList<>();
            for (LinkedTreeMap<String, Object> node : types) {
                CatalogModel model = new CatalogModel(node);
                catalogList.add(model);
            }
            return catalogList;
        }

        return null;
    }

    private List<CatalogHelpdeskModel> parseHelpdeskCatalog(List<LinkedTreeMap<String, String>> types) {
        if (types != null) {
            List<CatalogHelpdeskModel> catalogList = new ArrayList<>();
            for (LinkedTreeMap<String, String> node : types) {
                CatalogHelpdeskModel model = new CatalogHelpdeskModel(node);
                catalogList.add(model);
            }
            return catalogList;
        }

        return null;
    }

    private List<CatalogValueModel> parseValueCatalog(List<LinkedTreeMap<String, Object>> types) {
        if (types != null) {
            List<CatalogValueModel> catalogList = new ArrayList<>();
            for (LinkedTreeMap<String, Object> node : types) {
                CatalogValueModel model = new CatalogValueModel(node);
                catalogList.add(model);
            }
            return catalogList;
        }

        return null;
    }


    private List<CountryCatalogModel> parseCountriesCatalog(List<LinkedTreeMap<String, Object>> types) {
        if (types != null) {
            List<CountryCatalogModel> catalogList = new ArrayList<>();
            for (LinkedTreeMap<String, Object> node : types) {
                CountryCatalogModel model = new CountryCatalogModel(node);
                catalogList.add(model);
            }
            return catalogList;
        }

        return null;
    }
}