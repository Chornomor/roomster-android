package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


public class CardModel {

    @SerializedName("id")
    private Integer id = null;

    @SerializedName("exp_month")
    private Integer expMonth = null;

    @SerializedName("exp_year")
    private Integer expYear = null;

    @SerializedName("start_month")
    private Integer startMonth = null;

    @SerializedName("start_year")
    private Integer startYear = null;

    @SerializedName("cvc")
    private String cvc = null;

    @SerializedName("number")
    private String number = null;

    @SerializedName("card_type")
    private String cardType = null;

    @SerializedName("issue_number")
    private String issueNumber = null;

    @SerializedName("first_name")
    private String firstName = null;

    @SerializedName("last_name")
    private String lastName = null;

    @SerializedName("billing_address")
    private BillingAddress billingAddress = null;


    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public Integer getExpMonth() {
        return expMonth;
    }


    public void setExpMonth(Integer expMonth) {
        this.expMonth = expMonth;
    }


    public Integer getExpYear() {
        return expYear;
    }


    public void setExpYear(Integer expYear) {
        this.expYear = expYear;
    }


    public Integer getStartMonth() {
        return startMonth;
    }


    public void setStartMonth(Integer startMonth) {
        this.startMonth = startMonth;
    }


    public Integer getStartYear() {
        return startYear;
    }


    public void setStartYear(Integer startYear) {
        this.startYear = startYear;
    }


    public String getCvc() {
        return cvc;
    }


    public void setCvc(String cvc) {
        this.cvc = cvc;
    }


    public String getNumber() {
        return number;
    }


    public void setNumber(String number) {
        this.number = number;
    }


    public String getCardType() {
        return cardType;
    }


    public void setCardType(String cardType) {
        this.cardType = cardType;
    }


    public String getIssueNumber() {
        return issueNumber;
    }


    public void setIssueNumber(String issueNumber) {
        this.issueNumber = issueNumber;
    }


    public String getFirstName() {
        return firstName;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public BillingAddress getBillingAddress() {
        return billingAddress;
    }


    public void setBillingAddress(BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class CardModel {\n");

        sb.append("  id: ").append(id).append("\n");
        sb.append("  expMonth: ").append(expMonth).append("\n");
        sb.append("  expYear: ").append(expYear).append("\n");
        sb.append("  startMonth: ").append(startMonth).append("\n");
        sb.append("  startYear: ").append(startYear).append("\n");
        sb.append("  cvc: ").append(cvc).append("\n");
        sb.append("  number: ").append(number).append("\n");
        sb.append("  cardType: ").append(cardType).append("\n");
        sb.append("  issueNumber: ").append(issueNumber).append("\n");
        sb.append("  firstName: ").append(firstName).append("\n");
        sb.append("  lastName: ").append(lastName).append("\n");
        sb.append("  billingAddress: ").append(billingAddress).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
