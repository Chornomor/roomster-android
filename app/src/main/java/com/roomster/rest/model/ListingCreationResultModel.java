package com.roomster.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by andreybofanov on 04.07.16.
 */
public class ListingCreationResultModel {
    @SerializedName("listing_id")
    public Long listingId;
}
