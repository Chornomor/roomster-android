package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import com.roomster.enums.ComplaintTypeEnum;


public class UserReportViewModel {

    @SerializedName("user_id")
    private Long userId = null;

    @SerializedName("complaint_type")
    private ComplaintTypeEnum complaintType = null;


    @SerializedName("description")
    private String description = null;


    public Long getUserId() {
        return userId;
    }


    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public ComplaintTypeEnum getComplaintType() {
        return complaintType;
    }


    public void setComplaintType(ComplaintTypeEnum complaintType) {
        this.complaintType = complaintType;
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class UserReportViewModel {\n");

        sb.append("  userId: ").append(userId).append("\n");
        sb.append("  complaintType: ").append(complaintType).append("\n");
        sb.append("  description: ").append(description).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}