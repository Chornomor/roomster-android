import zipfile
import os
import xml.etree.ElementTree as ET
from shutil import copyfile

languageCodes = {u'cs-CZ': 'cs-rCZ',
u'es-LA': 'es',
u'pt-PT': 'pt-rPT',
u'es-ES': 'es-rES',
u'it-IT': 'it-rIT',
u'ko-KR': 'ko-rKR',
u'zh-CN': 'zh-rCN',
u'de-DE': 'de',
u'fr-FR': 'fr-rFR',
u'el-GR': 'el',
u'tr-TR': 'tr-rTR',
u'pl-PL': 'pl-rPL',
u'sv-SE': 'sv-rSE',
u'ja-JP': 'ja-rJP',
u'he-IL': 'iw',
u'ru-RU': 'ru',
u'ar-SA': 'ar'}



archiveName = 'locale.zip'
originalFilePath = 'app/src/main/res/values/string_locale.xml'
targetPathPrefix = 'app/src/main/res/values-'
targetPathPostfix = '/string_locale.xml'

originalXml = ET.parse(originalFilePath)
originalDict = dict()
for line  in originalXml.iter('string'):
    name = line.attrib.get('name')
    val = line.text
    originalDict[name] = val
fh = open(archiveName, 'rb')
log = open("locale_log.txt",'w')

z = zipfile.ZipFile(fh)
for fileName in z.namelist():
    chunks = fileName.split("_")
    languageName = chunks[len(chunks)-1].replace('.xml','')
    z.extract(fileName)
    tree = ET.parse(fileName)
    fileDict = dict()
    for line  in tree.findall('string'):
        name = line.attrib.get('name')
        val = line.text
        fileDict[name] = val
    for lineName in originalDict.iterkeys():
        val = fileDict.get(lineName)
        msg = None
        if val == None:
            msg = lineName + " in " + fileName + " not found!" + os.linesep
        elif val == originalDict.get(lineName):
            msg = lineName + " in " + fileName + " not localized!" + os.linesep
        if msg != None:
            print msg
            log.write(msg)
    copyfile(fileName,targetPathPrefix + languageCodes[languageName] + targetPathPostfix)
    os.remove(fileName)
fh.close()
log.close()
