package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import com.roomster.enums.GenderEnum;
import com.roomster.manager.MeManager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class UserGetViewModel implements Serializable {

    private static final int GENDER_UNKNOWN_INDEX = 1;
    private static final int GENDER_FEMALE_INDEX  = 2;
    private static final int GENDER_MALE_INDEX    = 4;

    public enum UserStatus {
        Inactive, Active
    }

    /**
     **/
    @SerializedName("id")
    private Long id = null;

    /**
     **/
    @SerializedName("first_name")
    private String firstName = null;

    /**
     **/
    @SerializedName("last_name")
    private String lastName = null;

    /**
     **/
    @SerializedName("dob")
    private Date dob = null;

    /**
     **/
    @SerializedName("description")
    private String description;

    /**
     **/
    @SerializedName("age")
    private Integer age = null;

    /**
     **/
    @SerializedName("last_login")
    private Date lastLogin = null;

    /**
     **/
    @SerializedName("phone")
    private String phone = null;

    /**
     **/
    @SerializedName("images")
    private ArrayList<String> images = null;
    /**
     **/
    @SerializedName("gender")
    private Integer           gender = null;

    /**
     **/
    @SerializedName("social_connections")
    private ArrayList<SocialConnectionViewModel> socialConnections = null;

    @SerializedName("photos")
    private ArrayList<Photo> photos;

    @SerializedName("full_access")
    private Boolean fullAccess;

    @SerializedName("isNewUser")
    private Boolean isNewUser = false;

    @SerializedName("spoken_languages")
    private ArrayList<String> spokenLanguages;

    @SerializedName("total_listings")
    private Integer totalListings;

    @SerializedName("contact_phone")
    private UserContactViewModel contactPhone = null;

    @SerializedName("home_town")
    private String hometown;

    @SerializedName("status")
    private UserStatus status;

    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public String getFirstName() {
        return firstName;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public Date getDob() {
        return dob;
    }


    public void setDob(Date dob) {
        this.dob = dob;
    }


    public Integer getAge() {
        return age;
    }


    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getIsNewUser() {
        return isNewUser;
    }

    public void setIsNewUser(Boolean isNewUser) {
        this.isNewUser = isNewUser;
    }

    public Date getLastLogin() {
        return lastLogin;
    }


    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }


    public String getPhone() {
        return phone;
    }


    public void setPhone(String phone) {
        this.phone = phone;
    }


    public List<String> getImages() {
        return images;
    }


    public String getLastName() {
        return lastName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public List<Photo> getPhotos() {
        return photos;
    }


    public void setPhotos(List<Photo> photos) {
        this.photos = new ArrayList<>(photos);
    }


    public Boolean getFullAccess() {
        return fullAccess;
    }


    public void setFullAccess(Boolean fullAccess) {
        this.fullAccess = fullAccess;
    }


    public List<String> getSpokenLanguages() {
        return spokenLanguages;
    }


    public void setSpokenLanguages(List<String> spokenLanguages) {
        if (spokenLanguages != null)
        this.spokenLanguages = new ArrayList<>(spokenLanguages);
    }


    public Integer getTotalListings() {
        if(totalListings == null)
            totalListings = 0;
        return totalListings;
    }


    public void setTotalListings(Integer totalListings) {
        this.totalListings = totalListings;
    }


    public UserContactViewModel getContactPhone() {
        return contactPhone;
    }


    public void setContactPhone(UserContactViewModel contactPhone) {
        this.contactPhone = contactPhone;
    }


    public String getTitleImage() {
        if (images != null && !images.isEmpty()) {
            return images.get(0);
        }
        return null;
    }


    public void addImage(String image) {
        if (images == null) {//if images is null we should init them to add image
            images = new ArrayList<>();
        }
        String title = getTitleImage(); // if exists fake image from the server than we need to delete it to add others
        if (title != null && title.equals(MeManager.FAKE_SERVER_IMAGE)) {
            images.remove(title);
        }
        images.add(image);
    }


    public void setImages(List<String> images) {
        this.images = new ArrayList<>(images);
    }


    public Integer getGenderIndex() {
        if (gender != null) {
            return gender;
        }
        return 0;
    }


    public GenderEnum getGender() {
        switch (gender) {
            case GENDER_UNKNOWN_INDEX:
                return GenderEnum.Unknown;
            case GENDER_FEMALE_INDEX:
                return GenderEnum.Female;
            case GENDER_MALE_INDEX:
                return GenderEnum.Male;
            default: {
                return null;
            }
        }
    }


    public void setGender(Integer gender) {
        this.gender = gender;
    }


    public List<SocialConnectionViewModel> getSocialConnections() {
        return socialConnections;
    }


    public boolean getIsVisibleSocialConnections() {
        boolean isHasVisibleSocial = false;
        for (SocialConnectionViewModel connection : socialConnections){
            if (connection.getIsVisible())
                isHasVisibleSocial = true;
        }
        return isHasVisibleSocial;
    }


    public void setSocialConnections(List<SocialConnectionViewModel> socialConnections) {
        this.socialConnections = new ArrayList<>(socialConnections);
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }


    public String getHometown() {
        return hometown;
    }


    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "UserGetViewModel{" +
          "id=" + id +
          ", firstName='" + firstName + '\'' +
          ", dob=" + dob +
          ", description='" + description + '\'' +
          ", age=" + age +
          ", lastLogin=" + lastLogin +
          ", phone='" + phone + '\'' +
          ", images=" + images +
          ", gender=" + gender +
          ", socialConnections=" + socialConnections +
          ", status=" + status +
          '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserGetViewModel that = (UserGetViewModel) o;

        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) {
            return false;
        }
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) {
            return false;
        }
        if (dob != null ? !dob.equals(that.dob) : that.dob != null) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (age != null ? !age.equals(that.age) : that.age != null) {
            return false;
        }
        if (lastLogin != null ? !lastLogin.equals(that.lastLogin) : that.lastLogin != null) {
            return false;
        }
        if (phone != null ? !phone.equals(that.phone) : that.phone != null) {
            return false;
        }
        if (images != null ? !images.equals(that.images) : that.images != null) {
            return false;
        }
        if (!gender.equals(that.gender)) {
            return false;
        }
        if (socialConnections != null ? !socialConnections.equals(that.socialConnections) : that.socialConnections != null) {
            return false;
        }
        if (photos != null ? !photos.equals(that.photos) : that.photos != null) {
            return false;
        }
        if (fullAccess != null ? !fullAccess.equals(that.fullAccess) : that.fullAccess != null) {
            return false;
        }

        return spokenLanguages != null ? spokenLanguages.equals(that.spokenLanguages)
                                       : that.spokenLanguages == null && !(totalListings != null ? !totalListings
                                         .equals(that.totalListings) : that.totalListings != null);
    }


    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (dob != null ? dob.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (age != null ? age.hashCode() : 0);
        result = 31 * result + (lastLogin != null ? lastLogin.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (images != null ? images.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (socialConnections != null ? socialConnections.hashCode() : 0);
        result = 31 * result + (photos != null ? photos.hashCode() : 0);
        result = 31 * result + (fullAccess != null ? fullAccess.hashCode() : 0);
        result = 31 * result + (spokenLanguages != null ? spokenLanguages.hashCode() : 0);
        result = 31 * result + (totalListings != null ? totalListings.hashCode() : 0);
        return result;
    }
}
