package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import com.roomster.enums.ServiceTypeEnum;


public class AttachmentListingView {

    @SerializedName("ListingId")
    private Long listingId = null;

    @SerializedName("ServiceType")
    private ServiceTypeEnum serviceType = null;

    @SerializedName("CurrencyId")
    private Integer currencyId = null;
    /**
     **/
    @SerializedName("DailyRent")
    private Integer dailyRent  = null;

    @SerializedName("WeeklyRent")
    private Integer weeklyRent = null;

    @SerializedName("MonthlyRent")
    private Integer monthlyRent = null;

    @SerializedName("Headline")
    private String headline = null;

    @SerializedName("GoogleFormattedAddress")
    private String googleFormattedAddress = null;

    @SerializedName("Photo")
    private String photo = null;

    @SerializedName("ApartmentType")
    private Integer apartmentType = null;


    public Long getListingId() {
        return listingId;
    }


    public void setListingId(Long listingId) {
        this.listingId = listingId;
    }


    public ServiceTypeEnum getServiceType() {
        return serviceType;
    }


    public void setServiceType(ServiceTypeEnum serviceType) {
        this.serviceType = serviceType;
    }


    public Integer getCurrencyId() {
        return currencyId;
    }


    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }


    public Integer getDailyRent() {
        return dailyRent;
    }


    public void setDailyRent(Integer dailyRent) {
        this.dailyRent = dailyRent;
    }


    public Integer getWeeklyRent() {
        return weeklyRent;
    }


    public void setWeeklyRent(Integer weeklyRent) {
        this.weeklyRent = weeklyRent;
    }


    public Integer getMonthlyRent() {
        return monthlyRent;
    }


    public void setMonthlyRent(Integer monthlyRent) {
        this.monthlyRent = monthlyRent;
    }


    public String getHeadline() {
        return headline;
    }


    public void setHeadline(String headline) {
        this.headline = headline;
    }


    public String getGoogleFormattedAddress() {
        return googleFormattedAddress;
    }


    public void setGoogleFormattedAddress(String googleFormattedAddress) {
        this.googleFormattedAddress = googleFormattedAddress;
    }


    public String getPhoto() {
        return photo;
    }


    public void setPhoto(String photo) {
        this.photo = photo;
    }


    public Integer getApartmentType() {
        return apartmentType;
    }


    public void setApartmentType(Integer apartmentType) {
        this.apartmentType = apartmentType;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class AttachmentListingView {\n");

        sb.append("  listingId: ").append(listingId).append("\n");
        sb.append("  serviceType: ").append(serviceType).append("\n");
        sb.append("  currencyId: ").append(currencyId).append("\n");
        sb.append("  dailyRent: ").append(dailyRent).append("\n");
        sb.append("  weeklyRent: ").append(weeklyRent).append("\n");
        sb.append("  monthlyRent: ").append(monthlyRent).append("\n");
        sb.append("  headline: ").append(headline).append("\n");
        sb.append("  googleFormattedAddress: ").append(googleFormattedAddress).append("\n");
        sb.append("  photo: ").append(photo).append("\n");
        sb.append("  apartmentType: ").append(apartmentType).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
