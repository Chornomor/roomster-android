package com.roomster.rest.service;


import com.roomster.BuildConfig;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.checkapp.CheckAppEvent;
import com.roomster.manager.NetworkManager;
import com.roomster.rest.api.CheckAppApi;
import com.roomster.rest.model.CheckAppModel;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CheckAppRestService {

    private static final String OS_TYPE = "Android";

    @Inject
    CheckAppApi mCheckAppApi;

    @Inject
    EventBus mEventBus;

    @Inject
    NetworkManager mNetworkManager;


    public CheckAppRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void checkForUpdate() {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<CheckAppModel> call = mCheckAppApi.shouldUpdateApp(OS_TYPE, (long) BuildConfig.VERSION_CODE);
            call.enqueue(new Callback<CheckAppModel>() {

                @Override
                public void onResponse(Call<CheckAppModel> call, Response<CheckAppModel> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            mEventBus.post(new CheckAppEvent.ShouldUpdateAppEvent(response.body().shouldForceUpdate()));
                            break;
                        case ResponseCode.FORBIDDEN:
                        case ResponseCode.SERVER_ERROR:
                        default:
                            mEventBus.post(new CheckAppEvent.ShouldUpdateAppEvent(false));
                    }
                }


                @Override
                public void onFailure(Call<CheckAppModel> call, Throwable t) {
                    mEventBus.post(new CheckAppEvent.ShouldUpdateAppEvent(false));
                }
            });
        } else {
            mEventBus.post(new CheckAppEvent.ShouldUpdateAppEvent(false));
        }
    }
}