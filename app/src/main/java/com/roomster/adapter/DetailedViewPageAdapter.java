package com.roomster.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.fragment.DetailedViewFragment;
import com.roomster.manager.BookmarkManager;
import com.roomster.manager.SearchManager;
import com.roomster.rest.model.ResultItemModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by michaelkatkov on 11/26/15.
 */
public class DetailedViewPageAdapter extends FragmentStatePagerAdapter {

    public List<ResultItemModel> mList = new ArrayList<>();
    protected SearchManager mSearchManager;
    private List<DetailedViewFragment> fragmentList = new ArrayList<>();
    boolean isSocialVisible = true;

    public DetailedViewPageAdapter(FragmentManager fragmentManager, List<ResultItemModel> list, SearchManager searchManager) {
        super(fragmentManager);
        mSearchManager = searchManager;
        if (list != null) {
            mList = new ArrayList<>(list);
        } else {
            mList = new ArrayList<>();
        }
    }


    public DetailedViewPageAdapter(FragmentManager fragmentManager, List<ResultItemModel> list) {
        super(fragmentManager);
        if (list != null) {
            mList = new ArrayList<>(list);
        } else {
            mList = new ArrayList<>();
        }
    }


    public void updateItems(List<ResultItemModel> newItems) {
        if (newItems != null && !newItems.isEmpty()) {
            synchronized (mList) {
                mList.clear();
                mList.addAll(0, newItems);
                notifyDataSetChanged();
            }
        }
    }

    public void remove(ResultItemModel target) {
        synchronized (mList) {
            mList.remove(target);
            notifyDataSetChanged();
        }
    }

    public List<ResultItemModel> clearUnbookmarkedBookmarks() {
        ArrayList<ResultItemModel> updatedList = new ArrayList<>();
        synchronized (mList) {
            if (mSearchManager != null && mSearchManager.listUnbookmarkedId.size() > 0 && mList != null) {
                for (ResultItemModel model : mList) {
                    for (int i = 0; i < mSearchManager.listUnbookmarkedId.size(); ++i) {
                        if (model.getListing().getListingId().equals(mSearchManager.listUnbookmarkedId.get(i))) {
                            model.getListing().setIsBookmarked(false);
                            mSearchManager.listUnbookmarkedId.remove(i);
                            break;
                        }
                    }
                    updatedList.add(model);
                }
                notifyDataSetChanged();
            }
        }
        return updatedList;
    }


    @Override
    public int getCount() {
        synchronized (mList) {
            return mList.size();
        }
    }


    @Override
    public Fragment getItem(int position) {
        synchronized (mList) {
            DetailedViewFragment f = DetailedViewFragment.newInstance(mList.get(position), isSocialVisible);
            fragmentList.add(f);
            return f;
        }
    }

    @Override
    public int getItemPosition(Object object) {
        // refresh all fragments when data set changed
        return PagerAdapter.POSITION_NONE;
    }

    public void setSocialVisible(boolean visible) {
        isSocialVisible = visible;
        for (DetailedViewFragment f : fragmentList) {
            f.setToolbarVisibility(visible);
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        fragmentList.remove(object);

    }

    @Override
    public CharSequence getPageTitle(int position) {
        return RoomsterApplication.context.getString(R.string.detailed_view_page_title) + position;
    }
}
