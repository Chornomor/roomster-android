package com.roomster.rest.api;


import com.roomster.rest.model.InAppProductViewModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Created by Bogoi.Bogdanov on 2/29/2016.
 */
public interface InAppProductsApi {

    /**
     * Update account info. All parameters are optional. Only pass what you need to update.
     *
     * @param platform
     *   Specify the platform for which we are getting the products (iOS, Android)
     * @return Call<Object>
     */
    @GET("InAppProducts")
    Call<List<InAppProductViewModel>> inAppProductsGet(@Query("platform") String platform);
}
