package com.roomster.fragment;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.roomster.utils.DateUtil;
import com.roomster.utils.LogUtil;

import java.util.Calendar;
import java.util.Date;


/**
 * Created by michaelkatkov on 12/10/15.
 */
public class DataPickerDialogFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private static final String MIN_DATE_ARG = "min_date";
    private static final String MAX_DATE_ARG = "max_date";

    public DatePickerDialog.OnDateSetListener mDataPickerListener;


    public static DataPickerDialogFragment newInstance(Date minDate, Date maxDate) {
        Bundle args = new Bundle();
        if (minDate != null) {
            args.putLong(MIN_DATE_ARG, minDate.getTime());
        }
        if (maxDate != null) {
            args.putLong(MAX_DATE_ARG, maxDate.getTime());
        }
        DataPickerDialogFragment fragment = new DataPickerDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
        if (getArguments() != null && getArguments().containsKey(MIN_DATE_ARG)) {
            long minDate = getArguments().getLong(MIN_DATE_ARG, 0);
            if (minDate < dialog.getDatePicker().getMinDate()){
                minDate = dialog.getDatePicker().getMinDate() + 100;
            }
            dialog.getDatePicker().setMinDate(minDate);
        }
        if (getArguments() != null && getArguments().containsKey(MAX_DATE_ARG)) {
            dialog.getDatePicker().setMaxDate(getArguments().getLong(MAX_DATE_ARG, 0));
        }
        return dialog;
    }


    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        if (mDataPickerListener != null) {
            mDataPickerListener.onDateSet(view, year, monthOfYear, dayOfMonth);
        }
    }


    public void setDataPickerListener(DatePickerDialog.OnDateSetListener dataPickerListener) {
        mDataPickerListener = dataPickerListener;
    }
}
