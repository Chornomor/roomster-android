package com.roomster.fragment;

import android.content.Context;
import android.os.Bundle;

import com.google.android.gms.maps.model.LatLngBounds;
import com.roomster.model.SearchCriteria;

/**
 * Created by andreybofanov on 10.08.16.
 */
public class MapClusterListFragment extends SearchListingsListFragment {
    public static final String KEY_BOUNDS = "bounds";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        SearchCriteria criteria = mDiscoveryPrefsManager.getSearchCriteria();
        criteria.setSearchBounds(getBounds());
        mSearchManager.setSearchCriteria(criteria);
        mSearchRestService.mapMode = true;
    }

    @Override
    public void onResume() {
        SearchCriteria criteria = mDiscoveryPrefsManager.getSearchCriteria();
        criteria.setSearchBounds(getBounds());
        mSearchManager.setSearchCriteria(criteria);
        mSearchRestService.mapMode = true;
        super.onResume();
    }

    LatLngBounds getBounds(){
        return getArguments().getParcelable(KEY_BOUNDS);
    }

    public static MapClusterListFragment getInstance(Bundle args){
        MapClusterListFragment f = new MapClusterListFragment();
        f.setArguments(args);
        return f;
    }
}
