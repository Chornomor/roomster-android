package com.roomster.activity;


import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.roomster.R;
import com.roomster.adapter.TransactionsListAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.transactions.TransactionsEvent;
import com.roomster.rest.model.TransactionsModel;
import com.roomster.rest.service.TransactionsRestService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class TransactionsActivity extends BaseActivity {

    private TransactionsListAdapter mTransactionsListAdapter;

    @Bind(R.id.transactions_recycler_view)
    RecyclerView mTransactionsRV;

    @Bind(R.id.transactions_toolbar)
    Toolbar mToolbar;

    @Bind(R.id.empty_layout)
    RelativeLayout mEmptyLayout;

    @Bind(R.id.empty_result_header)
    TextView mEmptyResultHeaderTV;

    @Bind(R.id.empty_result_subtext)
    TextView mEmptyResultSubtextTV;

    @Inject
    Context mContext;

    @Inject
    TransactionsRestService mTransactionsRestService;

    @Inject
    Tracker mTracker;


    @Override
    protected int getMainContentLayout() {
        return R.layout.activity_transactions;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        RoomsterApplication.getRoomsterComponent().inject(this);

        initView();

        mTransactionsRestService.getTransactions();
    }


    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName(getString(R.string.screen_name_messages));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    @OnClick(R.id.transactions_toolbar_back)
    void onToolbarBackClick() {
        onBackPressed();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(TransactionsEvent.TransactionGetSuccess event) {
        if (event.transactionsModel == null || event.transactionsModel.isEmpty()) {
            showEmptyTransactions();
        } else {
            List<TransactionsModel> transactions = event.transactionsModel;
            mTransactionsListAdapter.addItems(transactions);
            hideEmptyTransactions();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(TransactionsEvent.TransactionGetFail event) {
        Toast.makeText(this, getString(R.string.error_getting_transactions), Toast.LENGTH_SHORT).show();
        showEmptyTransactions();
    }


    private void initView() {
        mTransactionsListAdapter = new TransactionsListAdapter(mContext, null);
        mTransactionsRV.setAdapter(mTransactionsListAdapter);
        mTransactionsRV.setLayoutManager(new LinearLayoutManager(mContext));

        mEmptyResultHeaderTV.setText(getString(R.string.empty_transactions_header));
        mEmptyResultSubtextTV.setText(getString(R.string.empty_transactions_subtext));
        initToolbar();
    }


    private void initToolbar() {
        setSupportActionBar(mToolbar);
        mToolbar.setTitle("");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }


    private void showEmptyTransactions() {
        mTransactionsRV.setVisibility(View.GONE);
        mEmptyLayout.setVisibility(View.VISIBLE);
    }


    private void hideEmptyTransactions() {
        mTransactionsRV.setVisibility(View.VISIBLE);
        mEmptyLayout.setVisibility(View.GONE);
    }
}