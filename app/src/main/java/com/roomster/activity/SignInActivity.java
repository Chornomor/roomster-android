package com.roomster.activity;


import com.facebook.AccessToken;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import android.animation.LayoutTransition;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AlertDialog;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.newrelic.agent.android.harvest.HarvestResponse;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.IntentExtras;
import com.roomster.event.RestServiceEvents.AccountRemovedEvent;
import com.roomster.event.RestServiceEvents.AccountRestServiceEvent;
import com.roomster.event.RestServiceEvents.NoConnectionError;
import com.roomster.event.network.NetworkConnectionEvent;
import com.roomster.manager.FacebookManager;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.ReferrerManager;
import com.roomster.utils.InfoDialog;
import com.roomster.views.signin.SigninPager;
import com.roomster.views.signin.SigninPagerIndicator;
import com.roomster.views.signin.SlidePage;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Set;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.branch.referral.Branch;


public class SignInActivity extends SignLogicActivity {
    public static SignInActivity instance;
    public static final String KEY_ERROR_CODE = "error_code";
    private static final String TERMS = "terms";

    private boolean mIsLoggingIn;

    @Bind(R.id.no_network_connection)
    RelativeLayout mNoNetworkConnectionView;
    @Bind(R.id.signin_pager)
    SigninPager signinPager;
    @Bind(R.id.btn_more_info)
    ImageView btnMoreInfo;
    @Bind(R.id.info_bubble)
    TextView infoBubble;
    @Bind(R.id.pagerIndicator)
    SigninPagerIndicator indicator;
    @Inject
    NetworkManager mNetworkManager;
    @Inject
    ReferrerManager referrerManager;

    @Inject
    Tracker mTracker;
    SignInPagerAdapter adapter;
    boolean heightSet = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

        btnMoreInfo.setOnClickListener(new OnMoreInfoClickListener());
        signinPager.setOffscreenPageLimit(3);

        adapter = new SignInPagerAdapter();
        signinPager.setAdapter(adapter);
        signinPager.setIndicator(indicator);
        mIsLoggingIn = false;
        signinPager.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if(heightSet)
                    return;
                heightSet = true;
                int height = findViewById(R.id.bottom_layout).getHeight();
                adapter.setBottomLayoutHeight(height);
            }
        });
        checkIsUserWasBanned ();
    }


    @Override
    protected void onResume() {
        super.onResume();
        instance = this;

        if (mNetworkManager.hasNetworkAccess()) {
            hideNoNetworkConnection();
        } else {
            showNoNetworkConnection();
        }

        mTracker.setScreenName(getString(R.string.screen_name_sign_in));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    protected void onPause() {
        super.onPause();
        instance = null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mFacebookManager.onActivityResult(requestCode, resultCode, data);
    }


    @OnClick(R.id.facebook_login_btn_layout)
    void onFacebookLoginClicked() {
        if (!mIsLoggingIn) {
            mIsLoggingIn = true;
            mFacebookManager.login(this, new FacebookManager.LoginListener() {

                @Override
                public void onSuccess() {
                    mAccountRestService.login();
                }


                @Override
                public void onError(FacebookManager.LoginError error, Set<String> deniedPermissions) {
                    switch (error) {
                        case NOT_ALL_PERMISSIONS_GRANTED:
                            Toast.makeText(SignInActivity.this, constructError(deniedPermissions), Toast.LENGTH_SHORT)
                              .show();
                            break;
                        case CANCELED_BY_USER:
                            Toast.makeText(SignInActivity.this, R.string.facebook_login_cancelled, Toast.LENGTH_SHORT).show();
                            break;
                        case FACEBOOK_ERROR:
                            mFacebookManager.logout();
                            Toast.makeText(SignInActivity.this, R.string.social_login_failed, Toast.LENGTH_LONG).show();
                            break;
                    }
                    mIsLoggingIn = false;
                }
            });
        }
    }

    private String constructError(Set<String> deniedPermissions) {
        StringBuilder sb = new StringBuilder();
        for (String permission : deniedPermissions) {
            switch (permission) {
                case FacebookManager.USER_BIRTHDAY:
                    if (sb.length() > 0) sb.append("\n");
                    sb.append(getString(R.string.fb_error_birthday_required));
                    break;

                case FacebookManager.EMAIL:
                    if (sb.length() > 0) sb.append("\n");
                    sb.append(getString(R.string.fb_error_email_required));
                    break;
            }
        }
        return sb.toString();
    }


    @OnClick(R.id.terms)
    void onTermsTextViewClicked() {
        startAboutActivity();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(AccountRestServiceEvent.EmailWasNotConfirmOrAdd event) {
        addOrConfirmEmailDialog();
    }


    private void addOrConfirmEmailDialog() {
        InfoDialog dialog = new InfoDialog(this);
        dialog.createInfoDialog(getString(R.string.title_sorry), getString(R.string.add_or_confirm_facebook_email));
        AccessToken.setCurrentAccessToken(null);
        mIsLoggingIn = false;
    }


    private void checkIsUserWasBanned () {
        int errorCode = getIntent().getIntExtra(KEY_ERROR_CODE,-1);
        if(errorCode == -1){
            return;
        }
        if(errorCode == 403){
            requestBanned();
        } else if(errorCode == 401){
            mAccountRestService.reLogin();
        }
    }


    public void requestBanned(){
        mAccountRestService.getRemovedUser();
        AccessToken.setCurrentAccessToken(null);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(AccountRestServiceEvent.LoginFailure event) {
        mIsLoggingIn = false;
        if(event.code == HarvestResponse.Code.FORBIDDEN.getStatusCode()){
           requestBanned();
        } else {
            loginFailure(LOGIN_CODE_INACTIVE_USER, event.errorMessage);
            AccessToken.setCurrentAccessToken(null);
        }
    }

    @Override
    protected void loginFailure(int code, String errorMessage) {
        mIsLoggingIn = false;
        super.loginFailure(code, errorMessage);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(NetworkConnectionEvent.NetworkConnectionAvailable event) {
        hideNoNetworkConnection();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(NetworkConnectionEvent.NetworkConnectionUnavailable event) {
        showNoNetworkConnection();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(AccountRemovedEvent.RemovedUserGot event){
        showUserBannedDialog(event.id,event.message);
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(AccountRemovedEvent.RemovedUserGetFailure event){
        Toast.makeText(this,R.string.err_general,Toast.LENGTH_SHORT).show();
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(AccountRemovedEvent.RemovedUserResponde event){
        showRespondeSucces();
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(AccountRemovedEvent.RemovedUserRespondeFail event){
        Toast.makeText(this,R.string.err_general,Toast.LENGTH_SHORT).show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(NoConnectionError event){
        Toast.makeText(this,R.string.err_no_coonection,Toast.LENGTH_SHORT).show();
    }

    private void startAboutActivity() {
        Intent intent = new Intent(this, AboutActivity.class);
        intent.putExtra(IntentExtras.EXTRA_ABOUT_URL, TERMS);
        startActivity(intent);
    }


    private void showNoNetworkConnection() {
        mNoNetworkConnectionView.setVisibility(View.VISIBLE);
    }


    private void hideNoNetworkConnection() {
        mNoNetworkConnectionView.setVisibility(View.GONE);
    }


    class SignInPagerAdapter extends PagerAdapter {
        int [] pageIds = {R.id.signin_page_one,R.id.signin_page_two
                ,R.id.signin_page_three,R.id.signin_page_four};
        int bottomLayoutHeight = -1;
        SlidePage[] pages = new SlidePage[4];
        public Object instantiateItem(ViewGroup collection, int position) {
            if(pages[position] == null){
                pages[position] = (SlidePage) findViewById(pageIds[position]);
                if(bottomLayoutHeight != -1)
                    pages[position].setBottomMargin(bottomLayoutHeight);
            }
            return pages[position];
        }

        public void setBottomLayoutHeight(int bottomLayoutHeight){
            this.bottomLayoutHeight = bottomLayoutHeight;
            for(SlidePage page : pages)
                if(page != null)
                    page.setBottomMargin(bottomLayoutHeight);
        }

        public void setContentVisibility(boolean isVisible){
            for(SlidePage page : pages)
                if(page != null)
                    page.setContentVisibility(isVisible);
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == ((View) arg1);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
        }
    }

    private class OnMoreInfoClickListener implements View.OnClickListener {
        LayoutTransition.TransitionListener listener = new LayoutTransition.TransitionListener() {
            @Override
            public void startTransition(LayoutTransition transition, ViewGroup container, View view, int transitionType) {
                infoBubble.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(infoBubble.getVisibility() == View.GONE)
                        adapter.setContentVisibility(true);
                    }
                },500);
            }

            @Override
            public void endTransition(LayoutTransition transition, ViewGroup container, View view, int transitionType) {

            }
        };
        @Override
        public void onClick(View v) {
            if(infoBubble.getVisibility() == View.VISIBLE)
                turnOff();
            else
                turnOn();
        }

        public void turnOn(){
            infoBubble.setVisibility(View.VISIBLE);
            btnMoreInfo.setImageResource(R.mipmap.x);
            adapter.setContentVisibility(false);
            LayoutTransition mainLayoutTransition = ((ViewGroup)findViewById(R.id.bottom_layout)).getLayoutTransition();
            for (LayoutTransition.TransitionListener trListener : mainLayoutTransition.getTransitionListeners())
            mainLayoutTransition.removeTransitionListener(trListener);
            indicator.setVisibility(View.GONE);
        }

        public void turnOff(){
            infoBubble.setVisibility(View.GONE);
            btnMoreInfo.setImageResource(R.mipmap.question);
            LayoutTransition mainLayoutTransition = ((ViewGroup)findViewById(R.id.bottom_layout)).getLayoutTransition();
            mainLayoutTransition.addTransitionListener(listener);
            indicator.setVisibility(View.VISIBLE);
        }
    }

    public void showUserBannedDialog(final String id, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.err_ban_title);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.err_ban_contact_us, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showContactUs(id);
            }
        });
        builder.setNegativeButton(R.string.cancel,null);
        builder.create().show();
    }

    public void showContactUs(final String id){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.err_ban_contact_title);
        final EditText input = new EditText(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,10,
                getResources().getDisplayMetrics());
        lp.leftMargin = padding;
        lp.rightMargin = padding;
        input.setLayoutParams(lp);
        input.setHint(R.string.err_ban_contact_msg_hint);

        input.setPadding(padding,padding,padding,padding);

        builder.setView(input);
        builder.setPositiveButton(R.string.err_ban_contact_send, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mAccountRestService.respondeOnRemoval(id,input.getText().toString());
            }
        });
        builder.setNegativeButton(R.string.cancel,null);
        builder.create().show();
    }

    public void showRespondeSucces(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.err_ban_responded_title);
        builder.setMessage(R.string.err_ban_responded_msg);
        builder.setPositiveButton(R.string.ok, null);
        builder.create().show();
    }
}
