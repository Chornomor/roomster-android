package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import com.roomster.enums.SocialNetworkEnum;

import java.io.Serializable;


public class SocialConnectionViewModel implements Serializable {

    /**
     **/
    @SerializedName("social_network")
    private int socialNetworkIndex;

    /**
     **/
    @SerializedName("profile_url")
    private String  profileUrl = null;
    /**
     **/
    @SerializedName("is_visible")
    private Boolean isVisible  = null;

    @SerializedName("network_user_id")
    private String networkUserId;


    public SocialNetworkEnum getSocialNetwork() {
        SocialNetworkEnum[] socialNetworks = SocialNetworkEnum.values();
        if (0 <= socialNetworkIndex && socialNetworkIndex < socialNetworks.length) {
            return SocialNetworkEnum.values()[socialNetworkIndex];
        } else {
            return SocialNetworkEnum.Undefine;
        }
    }


    public void setSocialNetwork(SocialNetworkEnum socialNetwork) {
        socialNetworkIndex = socialNetwork.ordinal();
    }


    public String getProfileUrl() {
        return profileUrl;
    }


    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }


    public Boolean getIsVisible() {
        return isVisible;
    }


    public void setIsVisible(Boolean isVisible) {
        this.isVisible = isVisible;
    }

    public String getNetworkUserId() {
        return networkUserId;
    }

    public void setNetworkUserId(String networkUserId) {
        this.networkUserId = networkUserId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class SocialConnectionViewModel {\n");

        sb.append("  socialNetwork: ").append(getSocialNetwork()).append("\n");
        sb.append("  profileUrl: ").append(profileUrl).append("\n");
        sb.append("  isVisible: ").append(isVisible).append("\n");
        sb.append("  networkUserId: ").append(networkUserId).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}