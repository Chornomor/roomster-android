package com.roomster.rest.service;


import com.roomster.application.RoomsterApplication;
import com.roomster.enums.BookmarkTypeEnum;
import com.roomster.event.RestServiceEvents.BookmarksRestServiceEvent;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.manager.CatalogsManager;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.TokenManager;
import com.roomster.model.CatalogModel;
import com.roomster.rest.api.BookmarksApi;
import com.roomster.rest.model.BookmarkPostModel;
import com.roomster.rest.model.ResultItemModel;
import okhttp3.ResponseBody;
import org.greenrobot.eventbus.EventBus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import javax.inject.Inject;


public class BookmarksRestService {

    private static final String BOOKMARKED_TYPE = "Bookmarked";
    private static final String UNDEFINED_TYPE  = "Undefined";

    private ResultItemModel mResultItemModel;

    @Inject
    EventBus mEventBus;

    @Inject
    TokenManager mTokenManager;

    @Inject
    CatalogsManager mCatalogsManager;

    @Inject
    BookmarksApi mBookmarksApi;

    @Inject
    NetworkManager mNetworkManager;


    public BookmarksRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void toggleBookmark(ResultItemModel resultItemModel) {
        if (resultItemModel != null && resultItemModel.getListing() != null) {
            mResultItemModel = resultItemModel;
            if (mResultItemModel.getListing().getIsBookmarked()) {
                removeBookmark(mResultItemModel.getListing().getListingId(),
                        mResultItemModel.getListing().getServiceType());
            } else {
                addBookmark(mResultItemModel.getListing().getListingId(),
                        mResultItemModel.getListing().getServiceType());
            }
        }
    }


    public void addBookmark(final Long listingId, final Integer serviceTypeId) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callAddBookmark(result, listingId, serviceTypeId);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                sendFailureAddBookmark(listingId);
            }
        });
    }


    private void callAddBookmark(String authToken, final Long listingId, Integer serviceTypeId) {
        if (mNetworkManager.hasNetworkAccess()) {
            String serviceType = "";
            CatalogModel catalog = mCatalogsManager.getServiceTypeById(serviceTypeId);
            if (catalog != null && catalog.getName() != null) {
                serviceType = catalog.getName();
            }

            BookmarkPostModel bookmarkPostModel = new BookmarkPostModel();
            bookmarkPostModel.setListingId(listingId);
            bookmarkPostModel.setBookmarkType(BookmarkTypeEnum.valueOf(BOOKMARKED_TYPE));
            bookmarkPostModel.setServiceType(serviceType);
            Call<ResponseBody> callBookmarkPost = mBookmarksApi.bookmarksPost(bookmarkPostModel, authToken);
            callBookmarkPost.enqueue(new Callback<ResponseBody>() {

                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    switch (response.code()) {
                    case ResponseCode.OK:
                        sendSuccessAddBookmark(listingId);
                        break;
                    default:
                        sendFailureAddBookmark(listingId);
                    }
                }


                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    sendFailureAddBookmark(listingId);
                }
            });
        } else {
            sendFailureAddBookmark(listingId);
        }
    }


    private void sendFailureAddBookmark(long listingId) {
        BookmarksRestServiceEvent.AddBookmarkFailure event = new BookmarksRestServiceEvent.AddBookmarkFailure();
        event.listingId = listingId;
        mEventBus.post(event);
    }


    private void sendSuccessAddBookmark(long listingId) {
        BookmarksRestServiceEvent.AddBookmarkSuccess event = new BookmarksRestServiceEvent.AddBookmarkSuccess();
        event.listingId = listingId;
        mEventBus.post(event);
    }


    private void removeBookmark(final Long listingId, final Integer serviceTypeId) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callRemoveBookmark(result, listingId, serviceTypeId);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                sendFailureRemoveBookmark(listingId);
            }
        });
    }


    private void callRemoveBookmark(String authToken, final Long listingId, Integer serviceTypeId) {
        if (mNetworkManager.hasNetworkAccess()) {
            String serviceType = mCatalogsManager.getServiceTypeById(serviceTypeId).getName();
            Call<ResponseBody> callBookmarkDelete = mBookmarksApi
                    .bookmarksDelete(listingId, serviceType, BOOKMARKED_TYPE, authToken);
            callBookmarkDelete.enqueue(new Callback<ResponseBody>() {

                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    switch (response.code()) {
                    case ResponseCode.OK:
                        sendSuccessRemoveBookmark(listingId);
                        break;
                    default:
                        sendFailureRemoveBookmark(listingId);
                    }
                }


                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    sendFailureRemoveBookmark(listingId);
                }
            });
        } else {
            sendFailureRemoveBookmark(listingId);
        }
    }


    private void sendFailureRemoveBookmark(long listingId) {
        BookmarksRestServiceEvent.RemoveBookmarkFailure event = new BookmarksRestServiceEvent.RemoveBookmarkFailure();
        event.listingId = listingId;
        mEventBus.post(event);
    }


    private void sendSuccessRemoveBookmark(long listingId) {
        BookmarksRestServiceEvent.RemoveBookmarkSuccess event = new BookmarksRestServiceEvent.RemoveBookmarkSuccess();
        event.listingId = listingId;
        mEventBus.post(event);
    }
}