package com.roomster.views.signin;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.roomster.R;

/**
 * Created by andreybofanov on 18.07.16.
 */
public class SlidePageTwo extends AbstactSlidePage{

    public SlidePageTwo(Context context) {
        super(context);
        init();
    }

    public SlidePageTwo(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SlidePageTwo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    void init(){
        View v = LayoutInflater.from(getContext()).inflate(R.layout.signin_page_two,null);
        addView(v);
        TextView tv = (TextView) v.findViewById(R.id.slide_2_line_3);
        tv.setText(Html.fromHtml(getContext().getString(R.string.signin_slide_3_line_3)));
        tv = (TextView) v.findViewById(R.id.slide_2_line_4);
        tv.setText(Html.fromHtml(getContext().getString(R.string.signin_slide_3_line_4)));
        layout = (ViewGroup) v.findViewById(R.id.layout);
        setTypefaces();
    }
}
