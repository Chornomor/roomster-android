package com.roomster.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.roomster.manager.SettingsManager;
import com.roomster.utils.LocaleUtil;
import com.roomster.utils.LogUtil;

/**
 * Created by alexchern on 11.10.16.
 */

public class LocaleChangeReceiver extends BroadcastReceiver {

    SettingsManager mSettingsManager;

    @Override
    public void onReceive(Context context, Intent intent) {
        mSettingsManager = new SettingsManager();
        if (mSettingsManager.getLocale() != null && !mSettingsManager.getLocale().isEmpty()) {
            LocaleUtil.updateResources(context, mSettingsManager.getLocale());
        } else {
            LocaleUtil.updateResources(context, "");
        }
    }
}