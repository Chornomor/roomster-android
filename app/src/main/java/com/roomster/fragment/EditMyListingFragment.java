package com.roomster.fragment;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.InputFilter;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import butterknife.*;
import com.edmodo.rangebar.RangeBar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.roomster.R;
import com.roomster.activity.EditMyListingActivity;
import com.roomster.adapter.*;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.DiscoveryConstants;
import com.roomster.dialog.CatalogListChooserDialog;
import com.roomster.dialog.ImageChooserDialog;
import com.roomster.enums.ServiceTypeEnum;
import com.roomster.event.RestServiceEvents.CatalogRestServiceEvent;
import com.roomster.event.RestServiceEvents.ImageRestServiceEvent;
import com.roomster.event.RestServiceEvents.ListingsRestServiceEvent;
import com.roomster.event.fragment.AmenityDialogListingFragmentEvent;
import com.roomster.manager.CatalogsManager;
import com.roomster.manager.DiscoveryPrefsManager;
import com.roomster.manager.MeManager;
import com.roomster.manager.RoomsterLocationManager;
import com.roomster.model.CatalogModel;
import com.roomster.model.CatalogValueModel;
import com.roomster.rest.model.*;
import com.roomster.rest.service.CatalogRestService;
import com.roomster.rest.service.ImagesRestService;
import com.roomster.rest.service.ListingsRestService;
import com.roomster.utils.AmenityUtil;
import com.roomster.utils.DateUtil;
import com.roomster.utils.LocationUtil;
import com.roomster.utils.PermissionsAware;
import com.roomster.utils.PermissionsHelper;
import com.roomster.views.*;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.roomster.utils.StringsUtil.getLocationName;


public class EditMyListingFragment extends Fragment
        implements GoogleApiClient.OnConnectionFailedListener, ImageChooserListener, ImagePager.ImageClickListener,
        RoomsterLocationManager.LocationCallbacks{

    public static final  String TAG         = "EditMyListingFragment";
    private static final String ONE_STRING  = "1";
    private static final String DATA_PICKER = "data_picker";
    private static final String LISTING     = "Listing";

    private List<Integer> bedroomIdList = new ArrayList<>();
    private Integer mBedroomId                = -1;
    private Integer mBathroomId               = -1;
    private Integer mMinStayId                = -1;
    private boolean mNeedToShowBedroomsDialog = false;
    private boolean mNeedToShowBathDialog     = false;
    private boolean mNeedToShowMinStayDialog  = false;

    private static final int    HEADLINE_CHARS_LIMIT    = 35;
    private static final int    DESCRIPTION_CHARS_LIMIT = 300;
    private static final int    AVAILABLE_DATE          = 20;
    private static final int    LEAVE_BY_DATE           = 10;
    private static final int    ROW                     = 5;
    private static final String NEED_ROOM               = "NeedRoom";
    private static final String HAVE_SHARE              = "HaveShare";
    private static final String NEED_APARTMENT          = "NeedApartment";
    private static final String HAVE_APARTMENT          = "HaveApartment";
    private static final int    AMENITIES_COLUMNS       = 4;


    private Integer mAdditionalMinStayId                = -1;
    private Integer mResidenceBuildTypeId               = -1;
    private Integer mPeopleInHouseholdId                = -1;
    private Integer mSexHouseholdId                     = -1;
    private Integer mSmokingPreferenceId                = -1;

    private boolean mNeedToShowAdditionalMinStayDialog  = false;
    private boolean mNeedToShowBuildTypeDialog          = false;
    private boolean mNeedToShowPeopleInHouseholdDialog  = false;
    private boolean mNeedToShowSexHouseholdDialog       = false;
    private boolean mNeedToSmokingPreferenceDialog      = false;

    private long                     mListingId;
    private ListingGetViewModel      mCurrentListingModel;
    private GoogleApiClient          mGoogleApiClient;
    private PlaceAutocompleteAdapter mPlaceAutocompleteAdapter;
    private Date                     mDateIn;
    private Date                     mDateOut;
    private AmenitiesGridAdapter     mAmenitiesAdapter;
    private ImageChooserManager      mImageChooserManager;
    private int                      mChooserType;
    private String                   mFilePath;
    private ImageChooserDialog       mImageChooserDialog;
    private PermissionsHelper        mPermissionsHelper;
    private PetsAdapter              mPetsOwnedAdapter;
    private PetsAdapter              mPetsPreferredAdapter;
    private RoomsterLocationManager  mRoomsterLocationManager;
    private LatLngBounds             mLatLngBounds;
    private CatalogModel             serviceType;

    private List<RectangleWithStroke> listBedroomType = new ArrayList<>();

    private final String STUDIO      = "1";
    private final String BEDROOM_1   = "2";
    private final String BEDROOMS_2  = "6";
    private final String BEDROOMS_3  = "11";
    private final String BEDROOMS_4  = "16";
    private final String BEDROOMS_5  = "20";

    @Bind(R.id.el_global_sv)
    ScrollView mScrollView;

    @Bind(R.id.el_add_images_grid)
    ImagePager mImagesPager;

    @Bind(R.id.el_current_location_tv)
    DoneAutoCompleteEditText mLocationView;

    @Bind(R.id.el_location_pin)
    ImageView mCurrentLocationView;

    @Bind(R.id.headline_limit)
    TextView mHeadlineLimitTextView;

    @Bind(R.id.el_headline_text)
    EditText mHeadlineEditText;

    @Bind(R.id.description_limit)
    TextView mDescriptionLimitTextView;

    @Bind(R.id.el_description_text)
    EditText mDescriptionEditText;

    @Nullable
    @Bind(R.id.el_looking_bedrooms_layout)
    View mGridBedroomLayout;

    @Nullable
    @Bind(R.id.cb_bedroom0)
    RectangleWithStroke cbBedroom0;

    @Nullable
    @Bind(R.id.cb_bedroom1)
    RectangleWithStroke cbBedroom1;

    @Nullable
    @Bind(R.id.cb_bedroom2)
    RectangleWithStroke cbBedroom2;

    @Nullable
    @Bind(R.id.cb_bedroom3)
    RectangleWithStroke cbBedroom3;

    @Nullable
    @Bind(R.id.cb_bedroom4)
    RectangleWithStroke cbBedroom4;

    @Nullable
    @Bind(R.id.cb_bedroom5)
    RectangleWithStroke cbBedroom5;


    @Nullable
    @Bind(R.id.el_bedroom_layout)
    View mBedroomLayout;

    @Nullable
    @Bind(R.id.el_bedroom_value)
    TextView mBedroomValueTv;

    @Nullable
    @Bind(R.id.el_bath_layout)
    View mBathLayout;

    @Nullable
    @Bind(R.id.el_bath_value)
    TextView mBathValueTv;

    @Nullable
    @Bind(R.id.el_minimum_stay_layout)
    LinearLayout mMinStayLayout;

    @Nullable
    @Bind(R.id.el_minimum_stay_value)
    TextView mMinStayValueTv;

    @Bind(R.id.el_measurement_title)
    TextView mMeasurementTitleTv;

    @Bind(R.id.el_measurement_layout)
    RelativeLayout mMeasurementLayout;

    @Bind(R.id.el_measurement_et)
    EditText mMeasurementEt;

    @Bind(R.id.el_measurement_spinner)
    Spinner mMeasurementSpinner;

    @Bind(R.id.el_is_furnished_layout)
    RelativeLayout mFurnishedLayout;

    @Bind(R.id.el_is_furnished_toggle)
    ToggleButton mFurnishedToggle;

    @Bind(R.id.el_rental_et)
    EditText mRentalEt;

    @Bind(R.id.el_rental_spinner)
    Spinner mRentalSpinner;

    @Bind(R.id.el_rb_short_term)
    RadioButton mShortTermRb;

    @Bind(R.id.el_rb_long_term)
    RadioButton rbLongTerm;

    @Bind(R.id.el_available_dates_layout)
    LinearLayout mAvailableDatesLayout;

    @Bind(R.id.el_available_date_tv)
    TextView mAvailableDateTv;

    @Bind(R.id.el_leave_by_tv)
    TextView mLeaveByTv;

    @Bind(R.id.el_leave_by_tv_title)
    TextView mLeaveByTitleTv;

    @Bind(R.id.el_lifestyle_layout)
    View mLifestyleLayout;

    @Bind(R.id.el_lifestyle_shared_details_view)
    SharedDetailsView mLifestyleShareDetails;

    @Nullable
    @Bind(R.id.el_global_household_layout)
    LinearLayout mGlobalHouseholdLayout;

    @Nullable
    @Bind(R.id.el_household_age_layout)
    View mLifestyleHouseholdAgeLayout;

    @Nullable
    @Bind(R.id.el_household_age_value)
    TextView mLifestyleHouseholdAgeValue;

    @Nullable
    @Bind(R.id.el_household_age_range)
    RangeBar mLifestyleHouseholdAgeRange;

    @Bind(R.id.el_lifestyle_pets_title)
    TextView mMyPetsTitle;

    @Bind(R.id.el_my_pets_grid)
    GridView mMyPetsGrid;

    @Bind(R.id.el_roommate_prefs_layout)
    View mRoommatePrefsLayout;

    @Bind(R.id.el_roommate_prefs_age_layout)
    View mRoommatePrefsAgeLayout;

    @Bind(R.id.el_roommate_prefs_age_value)
    TextView mRoommatePrefsAgeValue;

    @Bind(R.id.el_roommate_prefs_age_range)
    RangeBar mRoommatePrefsAgeRange;

    @Bind(R.id.el_roommate_prefs_pets_title)
    TextView mRoommatePrefsPetsTitle;

    @Bind(R.id.el_roommate_prefs_pets_grid)
    GridView mRoommatePrefsPetsGrid;

    @Bind(R.id.el_residence_layout)
    View mResidenceLayout;

    @Nullable
    @Bind(R.id.el_additional_minimum_stay_layout)
    View mAdditionalMinStayLayout;

    @Nullable
    @Bind(R.id.el_additional_minimum_stay_value)
    TextView mAdditionalMinStayValueTv;

    @Nullable
    @Bind(R.id.el_residence_move_in_fee_layout)
    View mResidenceMoveInFeeLayout;

    @Nullable
    @Bind(R.id.el_residence_move_in_fee_et)
    EditText mResidenceMoveInFee;

    @Nullable
    @Bind(R.id.el_residence_cost_of_utilities_layout)
    View mResidenceUtilitiesCostLayout;

    @Nullable
    @Bind(R.id.el_residence_cost_of_utilities_et)
    EditText mResidenceUtilitiesCost;

    @Nullable
    @Bind(R.id.el_residence_parking_rent_layout)
    View mResidenceParkingRentLayout;

    @Nullable
    @Bind(R.id.el_residence_parking_rent_et)
    EditText mResidenceParkingRent;

    @Nullable
    @Bind(R.id.el_residence_furnished_layout)
    View mResidenceFurnishedLayout;

    @Nullable
    @Bind(R.id.el_residence_furnished_toggle)
    ToggleButton mResidenceFurnishedToggle;

    @Bind(R.id.el_room_amenities_layout)
    View mRoomAmenitiesLayout;

    @Bind(R.id.el_room_amenities)
    GridView mRoomAmenitiesGrid;

    @Bind(R.id.el_aminities_tv)
    TextView mAmenitiesTv;

    @Bind(R.id.el_amenity_grid)
    GridView mAmenityGridView;

    @Bind(R.id.el_grid_layout)
    LinearLayout mApartmentAmenitiesLayout;

    @Nullable
    @Bind(R.id.el_residence_building_type_layout)
    View mResidenceBuildTypeLayout;

    @Nullable
    @Bind(R.id.el_residence_building_type_value)
    TextView mResidenceBuildTypeValue;

    @Nullable
    @Bind(R.id.el_people_in_household_layout)
    View mPeopleInHouseholdLayout;

    @Nullable
    @Bind(R.id.el_people_in_household_value_tv)
    TextView mPeopleInHouseholdValueTv;

    @Nullable
    @Bind(R.id.el_sex_household_layout)
    View mSexHouseholdLayour;

    @Nullable
    @Bind(R.id.el_sex_household_value_tv)
    TextView mSexHouseholdValueTv;

    @Nullable
    @Bind(R.id.el_smoking_preference_layout)
    View mSmokingPreferenceLayout;

    @Nullable
    @Bind(R.id.el_smoking_preference_value_tv)
    TextView mSmokingPreferenceValueTv;

    @Nullable
    @Bind(R.id.el_students_only_layout)
    View mStudentsOnlyLayout;

    @Nullable
    @Bind(R.id.el_students_only_toggle)
    ToggleButton mStudentsOnlyToggle;

    @Inject
    Context mContext;

    @Inject
    DiscoveryPrefsManager mDPrefManager;

    @Inject
    CatalogsManager mCatalogsManager;

    @Inject
    CatalogRestService mCatalogRestService;

    @Inject
    ListingsRestService mListingsRestService;

    @Inject
    ImagesRestService mImagesRestService;

    @Inject
    MeManager mMeManager;

    @Inject
    EventBus mEventBus;

    NeedApartment needApartment;

    public static EditMyListingFragment newInstance(long listingId) {
        EditMyListingFragment fragment = new EditMyListingFragment();
        Bundle args = new Bundle();
        args.putLong(EditMyListingActivity.LISTING_ID, listingId);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
        mEventBus.register(this);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mListingId = getArguments().getLong(EditMyListingActivity.LISTING_ID);
            mCurrentListingModel = mMeManager.findListingById(mListingId);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mEventBus.unregister(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflateFragment(inflater, container);
        ButterKnife.bind(this, view);
        setUpScrollView();
        setUpLayout();
        setUpLocationView();
        setUpCurrentCoordinates();
        setUpHeadline();
        setUpDescription();
        setUpRentalEditText();
        setUpRentalSpinner();
        setUpAvailableDate();
        setUpLeaveByDate();
        setUpShortOrLong();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        mRoomsterLocationManager.setActivity(getActivity());
    }


    @Override
    public void onPause() {
        super.onPause();
        mRoomsterLocationManager.resetActivity();
    }


    private void setUpScrollView() {
        mScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                onClickAway();
                return false;
            }
        });

    }

    protected void onClickAway() {
        //hide soft keyboard
        if (!getActivity().isFinishing()) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }


    private View inflateFragment(LayoutInflater inflater, ViewGroup container) {
        View view;
        CatalogModel serviceType = mCatalogsManager.getServiceTypeById(mCurrentListingModel.getServiceType());
        if (serviceType != null) {
            switch (serviceType.getName()) {
                case HAVE_APARTMENT:
                    view = inflater.inflate(R.layout.fragment_edit_listing_offer_place, container, false);
                    break;
                case NEED_APARTMENT:
                    view = inflater.inflate(R.layout.fragment_edit_listing_looking_entire, container, false);
                    break;
                case HAVE_SHARE:
                    view = inflater.inflate(R.layout.fragment_edit_listing_offer_room, container, false);
                    break;
                default:
                    view = inflater.inflate(R.layout.fragment_edit_listing_looking_room, container, false);
            }
        } else {
            view = inflater.inflate(R.layout.fragment_edit_listing_looking_room, container, false);
        }
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            if (activity instanceof PermissionsAware) {
                mPermissionsHelper = ((PermissionsAware) activity).getPermissionsHelper();
            } else {
                mPermissionsHelper = new PermissionsHelper(activity);
            }
            if (mGoogleApiClient != null) {
                mGoogleApiClient.connect();
            }
            initLocationManager();
        }
    }

    private void initLocationManager() {
        mRoomsterLocationManager = new RoomsterLocationManager();
        mRoomsterLocationManager.startConnection(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(mContext, getString(R.string.google_api_error), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onImageClick(Photo photo) {
        if (photo != null) {
            if (photo.getPath().equals(PhotoGridAdapter.ADD_NEW_IMAGE_PLACEHOLDER)) {
                showImageChooserDialog();
            } else {
                mImagesRestService.deleteImage(LISTING, photo.getId());
            }
        }
    }


    @Override
    public void onImageChosen(final ChosenImage image) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    String imageUrl = image.getFileThumbnail();
                    if (imageUrl == null) {
                        imageUrl = image.getFilePathOriginal();
                    }
                    mImagesRestService.postImage(LISTING, mCurrentListingModel.getListingId(), imageUrl);
                }
            });
        }
    }


    @Override
    public void onError(final String reason) {
        final FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(activity, reason, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    @OnClick(R.id.el_clear_button)
    public void onClear() {
        mLocationView.setText("");
        mLocationView.setHint("");
        mLocationView.requestFocus();
    }

    @OnClick(R.id.el_location_pin)
    public void onCurrentLocation() {
        mCurrentLocationView.setEnabled(false);
        getPermissionsHelper().actionOnLocationPermission(new PermissionsHelper.OnLocationPermissionActionListener() {

                                                              @Override
                                                              public void onLocationPermissionGranted() {
                                                                  if (mRoomsterLocationManager != null && mRoomsterLocationManager.isConnected()) {
                                                                      mRoomsterLocationManager.requestLocationUpdates();
                                                                  } else {
                                                                      if (getActivity() != null)
                                                                          Toast.makeText(getActivity(), R.string.no_location_detected, Toast.LENGTH_SHORT).show();
                                                                  }
                                                                  mCurrentLocationView.setEnabled(true);
                                                              }

                                                              @Override
                                                              public void onLocationPermissionDenied() {
                                                                  if (getActivity() != null)
                                                                      Toast.makeText(getActivity(), R.string.no_location_detected, Toast.LENGTH_SHORT).show();
                                                                  mCurrentLocationView.setEnabled(true);
                                                              }
                                                          }

        );

        // mLocationView.setText(getCurrentLocation());
    }

    private PermissionsHelper getPermissionsHelper() {
        Activity activity = getActivity();
        if (activity instanceof PermissionsAware) {
            return ((PermissionsAware) activity).getPermissionsHelper();
        } else {
            return new PermissionsHelper(getActivity());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mRoomsterLocationManager.stopLocationUpdates();
        updateLocationView(location);
    }

    @Override
    public void onConnected() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED  ){
            switch (requestCode){
                case RoomsterLocationManager.REQUEST_PERMISSION_LAST_LOCATION:{
                    updateLocationView(mRoomsterLocationManager.getLastKnownLocation());
                    break;
                }
                case RoomsterLocationManager.REQUEST_PERMISSION_UPDATE_LOCATION:{
                    mRoomsterLocationManager.requestLocationUpdates();
                    break;
                }
            }
        }
    }


    private void updateLocationView(Location location) {
        mLocationView.setText(getLocationName(mContext, location));
//        mDPrefManager.storeLocation(location);
        setBounds(location, DiscoveryConstants.DEFAULT_DISTANCE);
    }

    private void setBounds(Location location, int mDistanceInMeters) {
        mLatLngBounds = LocationUtil.getBounds(location.getLatitude(), location.getLongitude(), mDistanceInMeters);
        setAdapterWithBounds(mLatLngBounds);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && (requestCode == ChooserType.REQUEST_PICK_PICTURE
                || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
            if (mImageChooserManager == null) {
                reinitializeImageChooser();
            }
            mImageChooserManager.submit(requestCode, data);
        } else {
            initImageGrid();
        }
    }


    @OnTextChanged(R.id.el_headline_text)
    void onHeadlineTextChanged(CharSequence text) {
        String headlineText = text.toString();
        int remainingChars = getHeadlineRemainingCharactersCount(headlineText);
        mHeadlineLimitTextView.setText(String.valueOf(remainingChars));

        //Make the first letter of the headline text capital.
        if (text.length() == 1 && Character.isLowerCase(text.charAt(0))) {
            mHeadlineEditText.setText(headlineText.toUpperCase());
            mHeadlineEditText.setSelection(1);
        }
    }


    @OnTextChanged(R.id.el_description_text)
    void onDescriptionTextChanged(CharSequence text) {
        String descriptionText = text.toString();
        int remainingChars = getDescriptionRemainingCharactersCount(descriptionText);
        mDescriptionLimitTextView.setText(String.valueOf(remainingChars));

        //Make the first letter of the description text capital.
        if (text.length() == 1 && Character.isLowerCase(text.charAt(0))) {
            mDescriptionEditText.setText(descriptionText.toUpperCase());
            mDescriptionEditText.setSelection(1);
        }
    }


    @OnClick(R.id.el_available_date_tv)
    public void onAvailableDateClick() {
        pickDate(mAvailableDateTv, AVAILABLE_DATE);
    }


    @OnClick(R.id.el_leave_by_tv)
    public void onLeaveDateClick() {
        pickDate(mLeaveByTv, LEAVE_BY_DATE);
    }


    @OnClick({ R.id.el_rb_short_term, R.id.el_rb_long_term })
    public void onShortOrLong() {
        if (mShortTermRb.isChecked()) {
            mLeaveByTv.setVisibility(View.VISIBLE);
            mLeaveByTitleTv.setVisibility(View.VISIBLE);
        } else {
            mLeaveByTv.setVisibility(View.GONE);
            mLeaveByTitleTv.setVisibility(View.GONE);
        }
    }


    @OnItemClick(R.id.el_room_amenities)
    void onRoomAmenityGridClick(AdapterView<?> parent, View view, int position, long id) {
        CatalogValueModel currentModel = (CatalogValueModel) parent.getItemAtPosition(position);
        if (currentModel.getName().equals(AmenityUtil.AMENITY_ADD)) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                FragmentManager fm = activity.getSupportFragmentManager();
                AmenityDialogListingFragment amenityDialog = AmenityDialogListingFragment
                        .newInstance(AmenityDialogFragment.ROOM_TYPE);
                amenityDialog.show(fm, AmenityDialogFragment.TAG);
            }
        } else {
            removeAmenity(currentModel, false);
        }
    }


    @OnItemClick(R.id.el_amenity_grid)
    public void onGridClick(AdapterView<?> parent, View view, int position, long id) {
        CatalogValueModel currentModel = (CatalogValueModel) parent.getItemAtPosition(position);
        if (currentModel.getName().equals(AmenityUtil.AMENITY_ADD)) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                FragmentManager fm = activity.getSupportFragmentManager();
                AmenityDialogListingFragment amenityDialog = AmenityDialogListingFragment
                        .newInstance(AmenityDialogFragment.APARTMENT_TYPE);
                amenityDialog.show(fm, AmenityDialogListingFragment.TAG);
            }
        } else {
            removeAmenity(currentModel, true);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(AmenityDialogListingFragmentEvent.AddAmenityEvent event) {
        mMeManager.getCurrentAmenities().add(event.amenity);
        mAmenitiesAdapter.notifyDataSetChanged();
        if (event.amenityType == AmenityDialogFragment.APARTMENT_TYPE) {
            measureLayout(mMeManager.getCurrentAmenities().size());
        } else
        if (event.amenityType == AmenityDialogFragment.ROOM_TYPE) {
            measureLayout(mMeManager.getCurrentAmenities().size(), mAmenityGridView);
        }
        setUpMeasurementSpinner();
        setUpRentalSpinner();
        if (mNeedToShowBedroomsDialog) {
            mNeedToShowBedroomsDialog = false;
            onBedroom();
        }
        if (mNeedToShowBathDialog) {
            mNeedToShowBathDialog = false;
            onBath();
        }
        if (mNeedToShowMinStayDialog) {
            mNeedToShowMinStayDialog = false;
            onMinStay();
        }
        if (mNeedToShowAdditionalMinStayDialog) {
            mNeedToShowAdditionalMinStayDialog = false;
            onAdditionalMinStay();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(AmenityDialogListingFragmentEvent.RemoveAmenityEvent event) {
        removeAmenity(event.amenity, event.amenityType == AmenityDialogFragment.APARTMENT_TYPE);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(CatalogRestServiceEvent.GetCatalogSuccess event) {
        //TODO re-init all controls
        CatalogModel serviceType = mCatalogsManager.getServiceTypeById(mCurrentListingModel.getServiceType());
        if (serviceType != null) {
            setUpBedroom(serviceType.getName());
        }
        setUpBath();
        setUpMeasurementSpinner();
        setUpRentalSpinner();
        setUpApartmentAmenitiesGrid();
        if (mNeedToShowBuildTypeDialog) {
            mNeedToShowBuildTypeDialog = false;
            onResidenceBuildType();
        }
        if (mNeedToShowPeopleInHouseholdDialog) {
            mNeedToShowPeopleInHouseholdDialog = false;
            onPeopleInHousehold();
        }
        if (mNeedToShowSexHouseholdDialog) {
            mNeedToShowSexHouseholdDialog = false;
            onSexHousehold();
        }
        if (mNeedToSmokingPreferenceDialog) {
            mNeedToSmokingPreferenceDialog = false;
            onSmokingPreference();
        }
        if (mNeedToShowAdditionalMinStayDialog) {
            mNeedToShowAdditionalMinStayDialog = false;
            onMinStay();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(CatalogRestServiceEvent.GetCatalogFailure event) {
        Toast.makeText(mContext, R.string.alert_failed_receive_catlog, Toast.LENGTH_LONG).show();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ImageRestServiceEvent.ImageAddSuccess event) {
        mListingsRestService.getListing(mCurrentListingModel.getListingId());
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ImageRestServiceEvent.ImageAddFailed event) {
        initImageGrid();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Toast.makeText(activity, getString(event.errorMessageId), Toast.LENGTH_SHORT).show();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ImageRestServiceEvent.ImageDeleteSuccess event) {
        mListingsRestService.getListing(mCurrentListingModel.getListingId());
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ImageRestServiceEvent.ImageDeleteFailed event) {
        initImageGrid();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Toast.makeText(activity, getString(R.string.failed_to_delete_image), Toast.LENGTH_SHORT).show();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ListingsRestServiceEvent.GetListingSuccess event) {
        mMeManager.updateMyListings(event.listingGetViewModel);
        mCurrentListingModel = event.listingGetViewModel;
        initImageGrid();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ListingsRestServiceEvent.GetListingFailed event) {
        initImageGrid();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Toast.makeText(activity, getString(R.string.unable_to_update_image_grid), Toast.LENGTH_SHORT).show();
        }
    }


    public void pickDate(final TextView textView, final int type) {
        DataPickerDialogFragment dateFragment = DataPickerDialogFragment.newInstance(null, null);
        dateFragment.setDataPickerListener(new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                storeDate(type, year, month, day);
                if (type == LEAVE_BY_DATE) {
                    setUpDate(mDateOut, textView);
                } else {
                    setUpDate(mDateIn, textView);
                }
            }
        });
        FragmentActivity activity = getActivity();
        if (activity != null) {
            dateFragment.show(activity.getSupportFragmentManager(), DATA_PICKER);
        }
    }


    public void setAdapterWithBounds(LatLngBounds bounds) {
        mPlaceAutocompleteAdapter = new PlaceAutocompleteAdapter(mContext, mGoogleApiClient, bounds, null);
        mLocationView.setAdapter(mPlaceAutocompleteAdapter);
    }


    public void saveListing() {
        ListingPutViewModel listingViewModel = new ListingPutViewModel();
        if (canBuildListViewModel(listingViewModel)) {
            mListingsRestService.editListing(listingViewModel);
        }
    }


    private void removeAmenity(CatalogValueModel amenity, boolean isApartment) {
        mMeManager.getCurrentAmenities().remove(amenity);
        mAmenitiesAdapter.notifyDataSetChanged();
        if (isApartment) {
            measureLayout(mMeManager.getCurrentAmenities().size());
        } else {
            measureLayout(mMeManager.getCurrentAmenities().size(), mAmenityGridView);
        }
    }


    private void setUpLayout() {
        serviceType = mCatalogsManager.getServiceTypeById(mCurrentListingModel.getServiceType());
        if (serviceType == null) {
            return;
        }
        switch (serviceType.getName()) {
            case NEED_ROOM:
                setUpLifestyleSharedDetailsView();
                setUpStudentsOnly();
                setUpMyPetsOwned(mCurrentListingModel.getShareDetails());
                setUpRoommatePrefsAge(mCurrentListingModel.getShareDetails());
                setUpRoommatePrefsSmoker(mCurrentListingModel.getShareDetails());
                setUpRoommatePrefsPets(mCurrentListingModel.getShareDetails());
                mLifestyleLayout.setVisibility(View.VISIBLE);
                mRoommatePrefsLayout.setVisibility(View.VISIBLE);
                break;
            case HAVE_SHARE:
                initImageGrid();
                setUpPeopleInHousehold();
                setUpHouseholdSex();
                setUpLifestyleSharedDetailsView();
                setUpHouseholdAge(mCurrentListingModel.getShareDetails());
                setUpMyPetsOwned(mCurrentListingModel.getShareDetails());
                setUpRoommatePrefsAge(mCurrentListingModel.getShareDetails());
                setUpRoommatePrefsSmoker(mCurrentListingModel.getShareDetails());
                setUpRoommatePrefsPets(mCurrentListingModel.getShareDetails());
                setUpResidenceBuildingType();
                setUpResidenceMoveInFee();
                setUpResidenceUtilitiesCost();
                setUpResidenceParkingRent();
                setUpResidenceFurnished();
                setUpRoomAmenities();
                setUpStudentsOnly();
                setUpMinStay(mCurrentListingModel);
                mLifestyleLayout.setVisibility(View.VISIBLE);
                mRoommatePrefsLayout.setVisibility(View.VISIBLE);
                mResidenceLayout.setVisibility(View.VISIBLE);
                break;
            case NEED_APARTMENT:
                if (getActivity() != null) {
                    setUpApartmentAmenitiesGrid();
                    setUpBedroom(NEED_APARTMENT);
                    setUpFurnishedNeedApt();
                }
                break;
            case HAVE_APARTMENT:
                setUpApartmentAmenitiesGrid();
                initImageGrid();
                setUpMinStay();
                setUpBedroom(HAVE_APARTMENT);
                setUpBath();
                setUpMeasurementEditText();
                setUpMeasurementSpinner();
                mMeasurementLayout.setVisibility(View.VISIBLE);
                setUpFurnishedToggle();
                break;
            default:
                break;
        }
        scrollToTop();
    }


    private void scrollToTop() {
        mAvailableDatesLayout.setFocusableInTouchMode(true);
        mAvailableDatesLayout.setFocusable(true);
        mAvailableDatesLayout.requestFocus();
    }


    private void setUpMinStay(ListingGetViewModel mCurrentListingModel) {
        Calendar calendar = mCurrentListingModel.getCalendar();
        List<CatalogModel> catalogModels = mCatalogsManager.getMinimumStayList();
        if(calendar.getMinimumStayId() != null) {
            mAdditionalMinStayId = calendar.getMinimumStayId();
        }
        showChosenValue(catalogModels, mAdditionalMinStayId, mAdditionalMinStayValueTv);
    }


    private void setUpStudentsOnly() {
        ShareDetails shareDetails = mCurrentListingModel.getShareDetails();
        if (shareDetails != null) {
            Boolean isStudentsOnly = shareDetails.getStudentsOnly();
            if (isStudentsOnly != null) {
                mStudentsOnlyToggle.setChecked(isStudentsOnly);
            }
        }
    }


    private void setUpRoomAmenities() {
        List<CatalogValueModel> allAmenities = mCatalogsManager.getRoomAmenitiesList();
        if (allAmenities != null) {
            List<CatalogValueModel> roomAmenities = new ArrayList<>();

            if (mCurrentListingModel.getShareDetails() != null) {
                List<Long> amenitiesIds = mCurrentListingModel.getShareDetails().getRoomAmenities();
                if (amenitiesIds != null) {
                    for (Long amenityId : amenitiesIds) {
                        CatalogValueModel amenityModel = mCatalogsManager.getCatalogValueModelModelByValue(allAmenities, Long.valueOf(amenityId));
                        if (amenityModel != null) {
                            roomAmenities.add(amenityModel);
                        }
                    }
                }
            }

            mAmenitiesAdapter = new AmenitiesGridAdapter(getContext(), allAmenities, roomAmenities, false);
            mRoomAmenitiesGrid.setAdapter(mAmenitiesAdapter);
            measureLayout(allAmenities.size(), mRoomAmenitiesGrid);

            mRoomAmenitiesLayout.setVisibility(View.VISIBLE);
        }
    }


    private void measureLayout(int itemsSize, GridView layout) {
        ViewGroup.LayoutParams params = layout.getLayoutParams();
        int amenityHeight = getResources().getDimensionPixelOffset(R.dimen.amenities_layout_height);
        int margin = getResources().getDimensionPixelSize(R.dimen.offset_4);
        int height = amenityHeight * (1 + (itemsSize / AMENITIES_COLUMNS));
        params.height = height + margin;
        layout.setLayoutParams(params);
    }


    private void measureApartmentLayout(int itemsSize, GridView layout) {
        ViewGroup.LayoutParams params = layout.getLayoutParams();
        int amenityHeight = getResources().getDimensionPixelOffset(R.dimen.amenities_layout_height);
        int margin = getResources().getDimensionPixelSize(R.dimen.offset_4);
        int height = amenityHeight * (1 + (itemsSize / ROW));
        params.height = height + margin;
        layout.setLayoutParams(params);
    }


    private void setUpResidenceUtilitiesCost() {
        ShareDetails shareDetails = mCurrentListingModel.getShareDetails();
        if (shareDetails != null && shareDetails.getUtilitiesCost() != null) {
            mResidenceUtilitiesCost.setText(String.valueOf(shareDetails.getUtilitiesCost()));
        }
        mResidenceUtilitiesCostLayout.setVisibility(View.VISIBLE);
    }


    private void setUpResidenceParkingRent() {
        ShareDetails shareDetails = mCurrentListingModel.getShareDetails();
        if (shareDetails != null && shareDetails.getParkingRent() != null) {
            mResidenceParkingRent.setText(String.valueOf(shareDetails.getParkingRent()));
        }
        mResidenceParkingRentLayout.setVisibility(View.VISIBLE);
    }


    private void setUpResidenceMoveInFee() {
        ShareDetails shareDetails = mCurrentListingModel.getShareDetails();
        if (shareDetails != null && shareDetails.getMoveinFee() != null) {
            mResidenceMoveInFee.setText(String.valueOf(shareDetails.getMoveinFee()));
        }
        mResidenceMoveInFeeLayout.setVisibility(View.VISIBLE);
    }


    private void setUpResidenceBuildingType() {
        ShareDetails shareDetails = mCurrentListingModel.getShareDetails();
        if (shareDetails != null && shareDetails.getBuildingType() != null) {
            List<CatalogModel> catalogModels = mCatalogsManager.getBuildingTypesList();
            if(shareDetails.getBuildingType() != null) {
                mResidenceBuildTypeId = (int) (long) shareDetails.getBuildingType();
            }
            showChosenValue(catalogModels, mResidenceBuildTypeId, mResidenceBuildTypeValue);
        }
    }


    private void setUpResidenceFurnished() {
        ShareDetails shareDetails = mCurrentListingModel.getShareDetails();
        if (shareDetails != null && shareDetails.getIsFurnished() != null) {
            mFurnishedToggle.setChecked(shareDetails.getIsFurnished());
        }
        mFurnishedLayout.setVisibility(View.VISIBLE);
    }


    private void setUpRoommatePrefsPets(ShareDetails shareDetails) {
        List<Long> petsPrefs = shareDetails == null ? null : shareDetails.getPetsPreference();
        List<CatalogValueModel> listingPets = new ArrayList<>();
        if (petsPrefs != null) {
            for (Long value : petsPrefs) {
                listingPets.add(mCatalogsManager.getCatalogValueModelModelByValue(mCatalogsManager.getPetsPreferredList(), value));
            }
        }
        FragmentActivity activity = getActivity();
        if (activity != null) {
            mPetsPreferredAdapter = new PetsAdapter(activity, mCatalogsManager.getPetsPreferredList(), listingPets);
        }
        mRoommatePrefsPetsGrid.setAdapter(mPetsPreferredAdapter);
        mRoommatePrefsPetsTitle.setVisibility(View.VISIBLE);
        mRoommatePrefsPetsGrid.setVisibility(View.VISIBLE);
    }

    private void setUpRoommatePrefsSmoker(ShareDetails shareDetails) {
        List<CatalogModel> catalogModels = mCatalogsManager.getSmokingPreferencesList();
        if (shareDetails != null && shareDetails.getSmokingPreference() != null) {
            mSmokingPreferenceId = (int) (long)shareDetails.getSmokingPreference();
            showChosenValue(catalogModels, mSmokingPreferenceId, mSmokingPreferenceValueTv);
        }
    }


    private void setUpRoommatePrefsAge(ShareDetails shareDetails) {
        Integer agePreferenceMin = shareDetails == null ? null : shareDetails.getAgePreferenceMin();
        Integer agePreferenceMax = shareDetails == null ? null : shareDetails.getAgePreferenceMax();
        final int ageRangeBarTickCount = DiscoveryConstants.MAX_AGE - DiscoveryConstants.MIN_AGE + 1;
        mRoommatePrefsAgeRange.setTickCount(ageRangeBarTickCount);

        if (agePreferenceMin == null || agePreferenceMin < DiscoveryConstants.MIN_AGE
                || agePreferenceMin > DiscoveryConstants.MAX_AGE) {
            agePreferenceMin = DiscoveryConstants.MIN_AGE;
        }
        if (agePreferenceMax == null || agePreferenceMax > DiscoveryConstants.MAX_AGE
                || agePreferenceMax < agePreferenceMin) {
            agePreferenceMax = DiscoveryConstants.MAX_AGE;
        }
        int leftValue = agePreferenceMin - DiscoveryConstants.MIN_AGE;
        int rightValue = agePreferenceMax - DiscoveryConstants.MIN_AGE;
        if (leftValue >= 0 || rightValue < ageRangeBarTickCount) {
            try {
                mRoommatePrefsAgeRange.setThumbIndices(leftValue, rightValue);
                updateRangeAgeValueText(mRoommatePrefsAgeValue, leftValue, rightValue);
            } catch (IllegalArgumentException e) {
                //Nothing to do
            }
        }
        mRoommatePrefsAgeRange.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {

            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int leftThumbIndex, int rightThumbIndex) {

                //There is a known bug in the RangeBar  - sometimes the indexes go beyond the set range!
                if (leftThumbIndex >= 0 && rightThumbIndex < ageRangeBarTickCount) {
                    updateRangeAgeValueText(mRoommatePrefsAgeValue, leftThumbIndex, rightThumbIndex);
                }
            }
        });
        mRoommatePrefsAgeLayout.setVisibility(View.VISIBLE);
    }


    private void setUpMinStay() {
        Calendar calendar = mCurrentListingModel.getCalendar();
        if (calendar != null) {
            final List<CatalogModel> catalogModels = mCatalogsManager.getMinimumStayList();
            if(calendar.getMinimumStayId() != null) {
                mMinStayId = calendar.getMinimumStayId();
            }
                showChosenValue(catalogModels, mMinStayId, mMinStayValueTv);
        }
    }


    private void updateRangeAgeValueText(TextView valueView, int left, int right) {
        int leftValue = left + DiscoveryConstants.MIN_AGE;
        int rightValue = right + DiscoveryConstants.MIN_AGE;
        String minValue = String.valueOf(leftValue);
        String maxValue = String.valueOf(rightValue);
        String str = String.format(getString(R.string.megaphone_age_min_max), minValue, maxValue);
        valueView.setText(str);
    }


    private void setUpHouseholdAge(ShareDetails shareDetails) {
        Integer householdAgeMin = shareDetails == null ? null : shareDetails.getHouseholdAgeMin();
        Integer householdAgeMax = shareDetails == null ? null : shareDetails.getHouseholdAgeMax();
        final int ageRangeBarTickCount = DiscoveryConstants.MAX_AGE - DiscoveryConstants.MIN_AGE + 1;
        mLifestyleHouseholdAgeRange.setTickCount(ageRangeBarTickCount);

        if (householdAgeMin == null || householdAgeMin < DiscoveryConstants.MIN_AGE
                || householdAgeMin > DiscoveryConstants.MAX_AGE) {
            householdAgeMin = DiscoveryConstants.MIN_AGE;
        }
        if (householdAgeMax == null || householdAgeMax > DiscoveryConstants.MAX_AGE
                || householdAgeMax < householdAgeMin) {
            householdAgeMax = DiscoveryConstants.MAX_AGE;
        }
        int leftValue = householdAgeMin - DiscoveryConstants.MIN_AGE;
        int rightValue = householdAgeMax - DiscoveryConstants.MIN_AGE;
        if (leftValue >= 0 || rightValue < ageRangeBarTickCount) {
            try {
                mLifestyleHouseholdAgeRange.setThumbIndices(leftValue, rightValue);
                updateRangeAgeValueText(mLifestyleHouseholdAgeValue, leftValue, rightValue);
            } catch (IllegalArgumentException e) {
                //Nothing to do
            }
        }
        mLifestyleHouseholdAgeRange.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {

            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int leftThumbIndex, int rightThumbIndex) {

                //There is a known bug in the RangeBar  - sometimes the indexes go beyond the set range!
                if (leftThumbIndex >= 0 && rightThumbIndex < ageRangeBarTickCount) {
                    updateRangeAgeValueText(mLifestyleHouseholdAgeValue, leftThumbIndex, rightThumbIndex);
                }
            }
        });
        mGlobalHouseholdLayout.setVisibility(View.VISIBLE);
    }


    private void setUpMyPetsOwned(ShareDetails shareDetails) {
        List<Long> petsOwned = shareDetails == null ? null : shareDetails.getMyPets();
        List<CatalogValueModel> listingPets = new ArrayList<>();
        if (petsOwned != null) {
            for (Long value : petsOwned) {
                listingPets.add(mCatalogsManager.getCatalogValueModelModelByValue(mCatalogsManager.getPetsOwnedList(), value));
            }
        }
        FragmentActivity activity = getActivity();
        if (activity != null) {
            mPetsOwnedAdapter = new PetsAdapter(activity, mCatalogsManager.getPetsOwnedList(), listingPets);
        }
        mMyPetsGrid.setAdapter(mPetsOwnedAdapter);
        mMyPetsTitle.setVisibility(View.VISIBLE);
        mMyPetsGrid.setVisibility(View.VISIBLE);
    }


    private void setUpPeopleInHousehold() {
        List<CatalogModel> catalogModels = mCatalogsManager.getPeopleInHouseholdList();
        ShareDetails shareDetails = mCurrentListingModel.getShareDetails();
        if (shareDetails != null && shareDetails.getPeopleInHousehold() != null) {
            mPeopleInHouseholdId = shareDetails.getPeopleInHousehold();
            showChosenValue(catalogModels, mPeopleInHouseholdId, mPeopleInHouseholdValueTv);
        }
    }


    private void setUpHouseholdSex() {
        List<CatalogModel> catalogModels = mCatalogsManager.getHouseholdSexList();
        ShareDetails shareDetails = mCurrentListingModel.getShareDetails();
        if (shareDetails != null && shareDetails.getHouseholdSex() != null) {
            mSexHouseholdId = shareDetails.getHouseholdSex();
            showChosenValue(catalogModels, mSexHouseholdId, mSexHouseholdValueTv);
        }
    }


    private void initImageGrid() {
        mCurrentListingModel = mMeManager.findListingById(mListingId);
        List<Photo> listImages = mCurrentListingModel.getPhotos();
        mImagesPager.setAdapter(getChildFragmentManager(), listImages, this);
        mImagesPager.setVisibility(View.VISIBLE);
    }


    private void setUpHeadline() {
        String headLineActual = mCurrentListingModel.getHeadlineActual();
        mHeadlineLimitTextView.setText(String.valueOf(HEADLINE_CHARS_LIMIT));
        mHeadlineEditText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(HEADLINE_CHARS_LIMIT) });
        //if headlineActual field has some info than fill field by this info, else get value from headline
        if (headLineActual != null && !headLineActual.isEmpty()) {
            mHeadlineEditText.setText(headLineActual);
        }
    }


    private void setUpDescription() {
        String descriptionActual = mCurrentListingModel.getDescriptionActual();
        mDescriptionLimitTextView.setText(String.valueOf(DESCRIPTION_CHARS_LIMIT));
        mDescriptionEditText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(DESCRIPTION_CHARS_LIMIT) });
        //if descriptionActual field has some info than fill field by this info, else get value from description
        if (descriptionActual != null && !descriptionActual.isEmpty()) {
            mDescriptionEditText.setText(descriptionActual);
        }
    }


    private void setUpLocationView() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext).addOnConnectionFailedListener(this)
                .addApi(Places.GEO_DATA_API).build();
        mLocationView.setOnItemClickListener(mAutocompleteClickListener);
    }


    private void setUpCurrentCoordinates() {
        GeoLocation listingLocation = mCurrentListingModel.getGeoLocation();
        setAdapterWithBounds(listingLocation.getBounds());
        writeLocation(listingLocation.getFullAddress());
    }


    private int getHeadlineRemainingCharactersCount(String text) {
        if (text != null) {
            return HEADLINE_CHARS_LIMIT - text.length();
        }

        return HEADLINE_CHARS_LIMIT;
    }


    private int getDescriptionRemainingCharactersCount(String text) {
        if (text != null) {
            return DESCRIPTION_CHARS_LIMIT - text.length();
        }

        return DESCRIPTION_CHARS_LIMIT;
    }


    private AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            AutocompletePrediction item = mPlaceAutocompleteAdapter.getItem(position);
            if (item != null) {
                String placeId = item.getPlaceId();
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(mAdditionalDetailsCallback);
                String locationString = item.getPrimaryText(new StyleSpan(Typeface.NORMAL)) + ", " + item
                        .getSecondaryText(new StyleSpan(Typeface.NORMAL));
//                mDPrefManager.storeLocationString(locationString);
            }
        }
    };

    private ResultCallback<PlaceBuffer> mAdditionalDetailsCallback = new ResultCallback<PlaceBuffer>() {

        @Override
        public void onResult(@NonNull PlaceBuffer places) {
            if (!places.getStatus().isSuccess() || places.getCount() == 0) {
                places.release();
                return;
            }
            final Place place = places.get(0);
            LatLngBounds latLngBounds = place.getViewport();
            if (latLngBounds != null) {
                setAdapterWithBounds(latLngBounds);
            }
            Location location = fromLatLng(place.getLatLng());
            setBounds(location, DiscoveryConstants.DEFAULT_DISTANCE);
//            mDPrefManager.storeLocation(location);
            places.release();
        }
    };


    private Location fromLatLng(LatLng latLng) {
        Location location = new Location("");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);
        return location;
    }

    private void writeLocation(String address) {
        if (address != null) {
            mLocationView.setText(address);
        }
    }


    private void setUpBedroom(String serviceTypeName) {
        if (mCatalogsManager.getBedroomsList() == null) {
            mCatalogRestService.loadCatalogs();
        }
        List<CatalogModel> catalogModels = mCatalogsManager.getBedroomsList();

        switch (serviceTypeName){
            case HAVE_APARTMENT:{
                HaveApartment haveApartment = mCurrentListingModel.getHaveApartment();
                if (haveApartment != null) {
                    mBedroomId = haveApartment.getBedroomsId();
                    showChosenValue(catalogModels, mBedroomId, mBedroomValueTv);
                    mBedroomLayout.setVisibility(View.VISIBLE);
                }
                break;
            }
            case NEED_APARTMENT:{
                addBedsToList();
                NeedApartment needApartment = mCurrentListingModel.getNeedApartment();
                if (needApartment != null) {
                    bedroomIdList = needApartment.getApartmentTypeIds();
                    setUpGridBedrooms(bedroomIdList, catalogModels);
                }
                break;
            }
        }

    }


    private void setUpGridBedrooms(List<Integer> bedroomIdList, List<CatalogModel> catalogModels) {
        if (bedroomIdList == null || bedroomIdList.isEmpty()) return;

        for (int i = 0; i < bedroomIdList.size(); i++) {
            updateBedroom(bedroomIdList.get(i).toString());
        }
        setNamesInRectangles(catalogModels, listBedroomType);

    }


    private void setNamesInRectangles(List<CatalogModel> models, List<RectangleWithStroke> listRectangles) {
        for(int i = 0; i < models.size(); ++i) {
            listRectangles.get(i).setText(models.get(i).getName());
        }
    }


    private void updateBedroom(String index) {
        switch (index) {
            case STUDIO:
                updateRectWithStroke(cbBedroom0);
                break;
            case BEDROOM_1:
                updateRectWithStroke(cbBedroom1);
                break;
            case BEDROOMS_2:
                updateRectWithStroke(cbBedroom2);
                break;
            case BEDROOMS_3:
                updateRectWithStroke(cbBedroom3);
                break;
            case BEDROOMS_4:
                updateRectWithStroke(cbBedroom4);
                break;
            case BEDROOMS_5:
                updateRectWithStroke(cbBedroom5);
                break;
        }
    }

    private void addBedsToList() {
        listBedroomType.add(cbBedroom0);
        listBedroomType.add(cbBedroom1);
        listBedroomType.add(cbBedroom2);
        listBedroomType.add(cbBedroom3);
        listBedroomType.add(cbBedroom4);
        listBedroomType.add(cbBedroom5);
    }


    private void updateRectWithStroke(RectangleWithStroke rect) {
        rect.setChecked(true);
        rect.setTextColor(getResources().getColor(R.color.orange));
    }

    private void setUpBath() {
        if (mCatalogsManager.getBathroomsList() == null) {
            mCatalogRestService.loadCatalogs();
        }
        HaveApartment haveApartment = mCurrentListingModel.getHaveApartment();
        if (haveApartment != null) {
            List<CatalogModel> catalogModels = mCatalogsManager.getBathroomsList();
            mBathroomId = haveApartment.getBathroomsId();
            showChosenValue(catalogModels, mBathroomId, mBathValueTv);
        }
        mBathLayout.setVisibility(View.VISIBLE);
    }


    private void setUpMeasurementEditText() {
        if (mCurrentListingModel.getHaveApartment() != null
                && mCurrentListingModel.getHaveApartment().getAptSize() != null) {
            Integer aptSize = mCurrentListingModel.getHaveApartment().getAptSize();
            mMeasurementEt.setHint(String.valueOf(aptSize));
        }
    }


    private void setUpMeasurementSpinner() {
        if (mCatalogsManager.getAptSizeUnitsList() == null) {
            mCatalogRestService.loadCatalogs();
        } else {
            List<CatalogModel> list = mCatalogsManager.getAptSizeUnitsList();
            AptSizesSpinnerAdapter adapter = new AptSizesSpinnerAdapter(mContext, list);
            mMeasurementSpinner.setAdapter(adapter);
            selectDefaultMeasure(mMeasurementSpinner, list);
        }
    }


    private void selectDefaultMeasure(Spinner spinner, List<CatalogModel> list) {
        if (mCurrentListingModel.getHaveApartment() == null
                || mCurrentListingModel.getHaveApartment().getAptSizeUnits() == null) {
            return;
        }
        Integer currentValueId = mCurrentListingModel.getHaveApartment().getAptSizeUnits();
        int position = -1;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId() == currentValueId) {
                position = i;
                break;
            }
        }
        if (position != -1) {
            spinner.setSelection(position);
        }
    }


    private void setUpFurnishedToggle() {
        if (mCurrentListingModel.getHaveApartment() != null
                && mCurrentListingModel.getHaveApartment().getIsFurnished() != null) {
            Boolean furnished = mCurrentListingModel.getHaveApartment().getIsFurnished();
            mFurnishedToggle.setChecked(furnished);
        }
        mFurnishedLayout.setVisibility(View.VISIBLE);
    }


    private void setUpFurnishedNeedApt(){
        if (mCurrentListingModel.getNeedApartment() != null &&
                mCurrentListingModel.getNeedApartment().getIsFurnished() != null){
            Boolean furnished =  mCurrentListingModel.getNeedApartment().getIsFurnished();
            mFurnishedToggle.setChecked(furnished);
        }
        mFurnishedLayout.setVisibility(View.VISIBLE);
    }


    private void setUpRentalEditText() {
        if (mCurrentListingModel.getRates() != null && mCurrentListingModel.getRates().getMonthlyRate() != null) {
            Integer rate = mCurrentListingModel.getRates().getMonthlyRate();
            mRentalEt.setText(String.valueOf(rate));
        }
    }


    private void setUpRentalSpinner() {
        if (mCatalogsManager.getCurrenciesList() == null) {
            mCatalogRestService.loadCatalogs();
        } else {
            List<String> list = mCatalogsManager.getCurrenciesList();
            CurrenciesSpinnerAdapter adapter = new CurrenciesSpinnerAdapter(mContext, list,
                    CurrenciesSpinnerAdapter.ADD_LISTING_TYPE);
            mRentalSpinner.setAdapter(adapter);
            String currentValue = mCurrentListingModel.getRates().getCurrency();
            selectDefault(mRentalSpinner, list, currentValue);
        }
    }


    private void selectDefault(Spinner spinner, List<String> list, String defaultValue) {
        int position = list.indexOf(defaultValue);
        if (position != -1) {
            spinner.setSelection(position);
        }
    }


    private void setUpShortOrLong() {
        if (mDateOut != null) {
            mShortTermRb.setChecked(true);
        } else {
            rbLongTerm.setChecked(true);
        }
        mAvailableDatesLayout.setVisibility(View.VISIBLE);
        onShortOrLong();
    }


    private void setUpDate(Date date, TextView textView) {
        if(date != null) {
            textView.setText(DateUtil.formatDate(date));
        }
    }


    private void setUpLeaveByDate() {
        mDateOut = mCurrentListingModel.getCalendar().getDateOut();
        setUpDate(mDateOut, mLeaveByTv);
    }


    private void setUpAvailableDate() {
        mDateIn = mCurrentListingModel.getCalendar().getDateIn();
        setUpDate(mDateIn, mAvailableDateTv);
    }


    private void setUpApartmentAmenitiesGrid() {
        List<CatalogValueModel> allAmenities = mCatalogsManager.getApartmentAmenitiesList();
        if (allAmenities != null) {
            List<CatalogValueModel> aptAmenities = new ArrayList<>();

            if (mCurrentListingModel.getApartmentAmenities() != null) {
                for (Long aptAmenitiesId : mCurrentListingModel.getApartmentAmenities()) {
                    CatalogValueModel aptAmenity = mCatalogsManager.getCatalogValueModelModelById(allAmenities, aptAmenitiesId);
                    if (aptAmenity != null) {
                        aptAmenities.add(aptAmenity);
                    }
                }
            }

            mAmenitiesAdapter = new AmenitiesGridAdapter(getContext(), allAmenities, aptAmenities, true);
            mAmenityGridView.setAdapter(mAmenitiesAdapter);
            measureApartmentLayout(allAmenities.size(), mAmenityGridView);
            mApartmentAmenitiesLayout.setVisibility(View.VISIBLE);
            mAmenitiesTv.setVisibility(View.VISIBLE);
        }
    }


    private void measureLayout(int itemsSize) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mRoomAmenitiesLayout.getLayoutParams();
        int amenityHeight = getResources().getDimensionPixelOffset(R.dimen.amenities_layout_height);
        int margin = getResources().getDimensionPixelSize(R.dimen.offset_4);
        int height = amenityHeight * (1 + itemsSize / (ROW + 1));
        params.height = height + margin;
        mRoomAmenitiesLayout.setLayoutParams(params);
    }


    private void setUpLifestyleSharedDetailsView() {
        mLifestyleShareDetails.setModel(mCurrentListingModel.getShareDetails());
        mLifestyleShareDetails.setVisibility(View.VISIBLE);
    }


    private void storeDate(int type, int year, int month, int day) {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.set(year, month, day);
        if (type == LEAVE_BY_DATE) {
            mDateOut = calendar.getTime();
        } else {
            mDateIn = calendar.getTime();
        }
    }


    private boolean canBuildListViewModel(ListingPutViewModel listingViewModel) {
        CatalogModel serviceType = mCatalogsManager.getServiceTypeById(mCurrentListingModel.getServiceType());
        boolean result = false;
        if (serviceType != null && serviceType.getName() != null) {
            switch (serviceType.getName()) {
                case HAVE_APARTMENT:
                    result = canBuildOfferEntirePlaceModel(listingViewModel);
                    break;
                case HAVE_SHARE:
                    result = canBuildOfferRoomModel(listingViewModel);
                    break;
                case NEED_APARTMENT:
                    result = canBuildFindEntirePlaceModel(listingViewModel);
                    break;
                case NEED_ROOM:
                    result = canBuildFindRoomModel(listingViewModel);
                    break;
            }
        }

        return result;
    }


    private boolean canBuildOfferEntirePlaceModel(ListingPutViewModel listingViewModel) {
        listingViewModel.setServiceType(ServiceTypeEnum.HaveApartment);
        buildApartmentAmenities(listingViewModel);
        boolean isCommonPropertiesBuilt = buildCommonProperties(listingViewModel);
        boolean isHaveApartmentBuilt = buildHaveApartment(listingViewModel);
        return isCommonPropertiesBuilt && isHaveApartmentBuilt;
    }


    private boolean canBuildOfferRoomModel(ListingPutViewModel listingViewModel) {
        listingViewModel.setServiceType(ServiceTypeEnum.HaveShare);
        listingViewModel.setShareDetails(buildSharedDetails(mLifestyleShareDetails.getModel()));
        return buildCommonProperties(listingViewModel);
    }


    private boolean canBuildFindEntirePlaceModel(ListingPutViewModel listingViewModel) {
        listingViewModel.setServiceType(ServiceTypeEnum.NeedApartment);
        buildApartmentAmenities(listingViewModel);
        boolean isNeedApartment = buildNeedApartment(listingViewModel);
        return buildCommonProperties(listingViewModel) && isNeedApartment;
    }


    private boolean canBuildFindRoomModel(ListingPutViewModel listingViewModel) {
        listingViewModel.setServiceType(ServiceTypeEnum.NeedRoom);
        listingViewModel.setShareDetails(buildSharedDetails(mLifestyleShareDetails.getModel()));
        return buildCommonProperties(listingViewModel);
    }


    private boolean buildRates(ListingPutViewModel listingViewModel) {
        String valueString = mRentalEt.getText().toString();
        if (valueString.isEmpty()) {
            Toast.makeText(getContext(), getString(R.string.alert_fill_the_rate), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            Rates rates = new Rates();
            rates.setCurrency((String) mRentalSpinner.getSelectedItem());
            try {
                if (!valueString.isEmpty())
                    rates.setMonthlyRate(Integer.parseInt(valueString));
            } catch (NumberFormatException exception) {
                Toast.makeText(getContext(), getString(R.string.alert_fill_the_rate_correctly), Toast.LENGTH_SHORT)
                        .show();
                return false;
            }
            listingViewModel.setRates(rates);
            return true;
        }
    }


    private boolean buildCommonProperties(ListingPutViewModel listingViewModel) {
        listingViewModel.setListingId(mCurrentListingModel.getListingId());
        listingViewModel.setIsActive(true);
        listingViewModel.setHeadline(mHeadlineEditText.getText().toString());
        listingViewModel.setDescription(mDescriptionEditText.getText().toString());
        boolean isAddressBuilt = buildAddress(listingViewModel);
        boolean isRatesBuilt = buildRates(listingViewModel);
        boolean isCalendarBuilt = buildCalendar(listingViewModel);
        setAdditionalMinStayId(listingViewModel);
        return isAddressBuilt && isRatesBuilt && isCalendarBuilt;
    }


    private ShareDetails buildSharedDetails(ShareDetails shareDetails) {
        if (shareDetails != null) {
            if (mLifestyleHouseholdAgeLayout.getVisibility() == View.VISIBLE) {
                shareDetails
                        .setHouseholdAgeMin(mLifestyleHouseholdAgeRange.getLeftIndex() + DiscoveryConstants.MIN_AGE);
                shareDetails
                        .setHouseholdAgeMax(mLifestyleHouseholdAgeRange.getRightIndex() + DiscoveryConstants.MIN_AGE);
            }
            if (mPeopleInHouseholdLayout.getVisibility() == View.VISIBLE) {
                Integer peopleInHousehold = null;
                if (mPeopleInHouseholdId != -1) {
                    peopleInHousehold = mPeopleInHouseholdId;
                }
                shareDetails.setPeopleInHousehold(peopleInHousehold);
            }
            if (mSexHouseholdLayour.getVisibility() == View.VISIBLE) {
                Integer householdSex = null;
                if (mSexHouseholdId != -1) {
                    householdSex = mSexHouseholdId;
                }
                shareDetails.setHouseholdSex(householdSex);
            }
            if (mPetsOwnedAdapter != null) {
                if (mPetsOwnedAdapter.getSelected() != null && !mPetsOwnedAdapter.getSelected().isEmpty()) {
                    ArrayList<Long> petsOwnedValues = new ArrayList<>();
                    for (CatalogValueModel model : mPetsOwnedAdapter.getSelected()) {
                        if (model != null) {
                            petsOwnedValues.add(model.getValue());
                        }
                    }
                    shareDetails.setMyPets(petsOwnedValues);
                } else {
                    shareDetails.setMyPets(null);
                }
            }
            if (mRoommatePrefsAgeLayout.getVisibility() == View.VISIBLE) {
                shareDetails.setAgePreferenceMin(mRoommatePrefsAgeRange.getLeftIndex() + DiscoveryConstants.MIN_AGE);
                shareDetails.setAgePreferenceMax(mRoommatePrefsAgeRange.getRightIndex() + DiscoveryConstants.MIN_AGE);
            }
            if (mSmokingPreferenceLayout.getVisibility() == View.VISIBLE) {
                Long smokingPreference = null;
                if (mSmokingPreferenceId != -1) {
                    smokingPreference = (long)mSmokingPreferenceId;
                }
                shareDetails.setSmokingPreference(smokingPreference);
            }
            if (mPetsPreferredAdapter != null) {
                if (mPetsPreferredAdapter.getSelected() != null && !mPetsPreferredAdapter.getSelected().isEmpty()) {
                    ArrayList<Long> petsPreferredValues = new ArrayList<>();
                    for (CatalogValueModel model : mPetsPreferredAdapter.getSelected()) {
                        if (model != null) {
                            petsPreferredValues.add(model.getValue());
                        }
                    }
                    shareDetails.setPetsPreference(petsPreferredValues);
                } else {
                    shareDetails.setPetsPreference(null);
                }
            }
            if (mResidenceBuildTypeLayout.getVisibility() == View.VISIBLE) {
                Long buildType = null;
                if (mResidenceBuildTypeId != -1) {
                    buildType = (long)mResidenceBuildTypeId;
                }
                shareDetails.setBuildingType(buildType);
            }

            if (mResidenceMoveInFeeLayout.getVisibility() == View.VISIBLE) {
                Integer moveInFee = null;
                try {
                    moveInFee = Integer.parseInt(mResidenceMoveInFee.getText().toString());
                } catch (NumberFormatException nfe) {
                    // let the number be null
                }
                shareDetails.setMoveinFee(moveInFee);
            }

            if (mResidenceUtilitiesCostLayout.getVisibility() == View.VISIBLE) {
                Integer utilitiesCost = null;
                try {
                    utilitiesCost = Integer.parseInt(mResidenceUtilitiesCost.getText().toString());
                } catch (NumberFormatException nfe) {
                    // let the number be null
                }
                shareDetails.setUtilitiesCost(utilitiesCost);
            }

            if (mResidenceParkingRentLayout.getVisibility() == View.VISIBLE) {
                Integer parkingRent = null;
                try {
                    parkingRent = Integer.parseInt(mResidenceParkingRent.getText().toString());
                } catch (NumberFormatException nfe) {
                    // let the number be null
                }
                shareDetails.setParkingRent(parkingRent);
            }

            if (mStudentsOnlyLayout.getVisibility() == View.VISIBLE) {
                shareDetails.setStudentsOnly(mStudentsOnlyToggle.isChecked());
            }

            if (mResidenceFurnishedLayout.getVisibility() == View.VISIBLE) {
                shareDetails.setIsFurnished(mResidenceFurnishedToggle.isChecked());
            }

            if (mFurnishedLayout.getVisibility() == View.VISIBLE) {
                shareDetails.setIsFurnished(mFurnishedToggle.isChecked());
            }

            if (mRoomAmenitiesLayout.getVisibility() == View.VISIBLE) {
                buildRoomAmenities(shareDetails);
            }
        }
        return shareDetails;
    }


    private void setAdditionalMinStayId(ListingPutViewModel listingPutViewModel) {
        Calendar calendar = listingPutViewModel.getCalendar();
        if (mAdditionalMinStayId != -1) {
            calendar.setMinimumStayId(mAdditionalMinStayId);
        }
        listingPutViewModel.setCalendar(calendar);
    }


    private boolean buildAddress(ListingPutViewModel listingPutViewModel) {
        String addressString = mLocationView.getText().toString();
        if (addressString.isEmpty()) {
            Toast.makeText(getContext(), getString(R.string.alert_fill_the_address), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            listingPutViewModel.setFullAddress(addressString);
            return true;
        }
    }


    private boolean buildCalendar(ListingPutViewModel listingPutViewModel) {
        Date currentDate = new Date();
        if (mDateIn == null) {
            Toast.makeText(getContext(), getString(R.string.alert_available_date_is_empty), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            if (mDateOut != null && mDateOut.before(currentDate)) {
                Toast.makeText(getContext(), getString(R.string.alert_date_out_should_be_in_future), Toast.LENGTH_SHORT)
                        .show();
                return false;
            } else {
                if (mDateIn != null && mDateOut != null && mDateIn.after(mDateOut)) {
                    Toast.makeText(getContext(), getString(R.string.alert_date_out_before_date_in), Toast.LENGTH_SHORT)
                            .show();
                    return false;
                } else {
                    Calendar calendar = new Calendar();
                    calendar.setDateIn(specifyDataIn());
                    if (mShortTermRb.isChecked()) {
                        calendar.setDateOut(mDateOut);
                    }
                    if (mMinStayId != -1) {
                        calendar.setMinimumStayId(mMinStayId);
                    }
                    listingPutViewModel.setCalendar(calendar);
                    return true;
                }
            }
        }
    }


    private Date specifyDataIn() {
        //server doesn't support date in past
        Date currentDate = new Date();
        if (mDateIn.before(currentDate)) {
            return currentDate;
        }
        return mDateIn;
    }


    private boolean buildHaveApartment(ListingPutViewModel listingPutViewModel) {
        HaveApartment haveApartment = new HaveApartment();
        haveApartment.setIsFurnished(mFurnishedToggle.isChecked());
        buildBathrooms(haveApartment);
        buildBedrooms(haveApartment);
        haveApartment.setAptSizeUnits(((CatalogModel) mMeasurementSpinner.getSelectedItem()).getId());
        String sizeValue = mMeasurementEt.getText().toString();
        String sizeHint = mMeasurementEt.getHint().toString();
        if (sizeValue.isEmpty() && sizeHint.isEmpty()) {
            Toast.makeText(getContext(), getString(R.string.alert_apartment_type_is_empty), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            if (!sizeValue.isEmpty()) {
                haveApartment.setAptSize(Integer.parseInt(sizeValue));
            } else {
                haveApartment.setAptSize(Integer.parseInt(sizeHint));
            }
            listingPutViewModel.setHaveApartment(haveApartment);
            return true;
        }
    }


    private boolean buildNeedApartment(ListingPutViewModel listingPutViewModel) {
        if (needApartment == null) needApartment = new NeedApartment();
        needApartment.setIsFurnished(mFurnishedToggle.isChecked());
        buildBedrooms(needApartment);
        listingPutViewModel.setNeedApartment(needApartment);
        return true;
    }


    private void buildApartmentAmenities(ListingPutViewModel listingPutViewModel) {
        if (mAmenitiesAdapter != null && listingPutViewModel != null) {
            if (mAmenitiesAdapter.getSelected() != null && !mAmenitiesAdapter.getSelected().isEmpty()) {
                ArrayList<Long> aptAmenities = new ArrayList<>();
                for (CatalogValueModel model : mAmenitiesAdapter.getSelected()) {
                    aptAmenities.add((long)model.getId());
                }
                listingPutViewModel.setApartmentAmenities(aptAmenities);
            } else {
                listingPutViewModel.setApartmentAmenities(null);
            }
        }
    }


    private void buildRoomAmenities(ShareDetails shareDetails) {
        if (mAmenitiesAdapter != null && shareDetails != null) {
            if (mAmenitiesAdapter.getSelected() != null && !mAmenitiesAdapter.getSelected().isEmpty()) {
                ArrayList<Long> roomAmenities = new ArrayList<>();
                for (CatalogValueModel model : mAmenitiesAdapter.getSelected()) {
                    roomAmenities.add(model.getValue());
                }
                shareDetails.setRoomAmenities(roomAmenities);
            } else {
                shareDetails.setRoomAmenities(null);
            }
        }
    }


    private void buildBathrooms(HaveApartment haveApartment) {
        if (mBathroomId != -1) {
            haveApartment.setBathroomsId(mBathroomId);
        }
    }

    private void buildBedrooms(NeedApartment needApartment) {
        storeListBedrooms();
        needApartment.setApartmentTypeIds(bedroomIdList);
    }


    private void buildBedrooms(HaveApartment haveApartment) {
        if (mBedroomId != -1) {
            haveApartment.setBedroomsId(mBedroomId);
        }
    }


    private void storeListBedrooms() {
        List<CatalogModel> catalogModels = mCatalogsManager.getBedroomsList();
        bedroomIdList = new ArrayList<>();
        for (int i = 0; i < listBedroomType.size(); ++i) {
            if (listBedroomType.get(i).isChecked()) {
                bedroomIdList.add(catalogModels.get(i).getId());
            }
        }
    }


    private void showImageChooserDialog() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            mImageChooserDialog = new ImageChooserDialog(activity, new ImageChooserDialog.Listener() {

                @Override
                public void onFacebookClick() {

                }


                @Override
                public void onCameraClick() {
                    mImageChooserDialog.dismiss();
                    mPermissionsHelper
                            .actionOnCameraPermission(new PermissionsHelper.OnCameraPermissionActionListener() {

                                @Override
                                public void onCameraPermissionGranted() {
                                    mPermissionsHelper.actionOnStoragePermission(new PermissionsHelper.OnStoragePermissionActionListener() {
                                        @Override
                                        public void onStoragePermissionGranted() {
                                            takePicture();
                                        }

                                        @Override
                                        public void onStoragePermissionDenied() {
                                            //TODO: Fix this hack - we need to remove all loading indicators from the grid
                                            initImageGrid();
                                        }
                                    });
                                }


                                @Override
                                public void onCameraPermissionDenied() {
                                    initImageGrid();
                                }
                            });
                }


                @Override
                public void onGalleryClick() {
                    mImageChooserDialog.dismiss();
                    mPermissionsHelper
                            .actionOnStoragePermission(new PermissionsHelper.OnStoragePermissionActionListener() {

                                @Override
                                public void onStoragePermissionGranted() {
                                    chooseImage();
                                }


                                @Override
                                public void onStoragePermissionDenied() {
                                    initImageGrid();
                                }
                            });
                }


                @Override
                public void onCancel() {
                    mImageChooserDialog.dismiss();
                    initImageGrid();
                }
            });
            mImageChooserDialog.show();
        }
    }


    private void chooseImage() {
        mChooserType = ChooserType.REQUEST_PICK_PICTURE;
        mImageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_PICK_PICTURE, true);
        mImageChooserManager.setImageChooserListener(this);
        mImageChooserManager.clearOldFiles();
        try {
            mFilePath = mImageChooserManager.choose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void takePicture() {
        mChooserType = ChooserType.REQUEST_CAPTURE_PICTURE;
        mImageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_CAPTURE_PICTURE, true);
        mImageChooserManager.setImageChooserListener(this);
        try {
            mFilePath = mImageChooserManager.choose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void reinitializeImageChooser() {
        mImageChooserManager = new ImageChooserManager(this, mChooserType, true);
        mImageChooserManager.setImageChooserListener(this);
        mImageChooserManager.reinitialize(mFilePath);
    }


    //------------------ Bathroom, Badroom, Min stay picker methods --------------------------//
    private CatalogModel getCatalogById(List<CatalogModel> catalogModels, Integer categoryId) {
        if (categoryId != null && categoryId != -1) {
            for (CatalogModel catalogModel : catalogModels) {
                if (catalogModel.getId() == categoryId) {
                    return catalogModel;
                }
            }
        }
        return null;
    }


    private void showChosenValue(List<CatalogModel> catalog, Integer categoryId, TextView valueTv) {
        CatalogModel catalogModel = getCatalogById(catalog, categoryId);
        if (catalogModel != null) {
            valueTv.setText(catalogModel.getName());
        } else {
            valueTv.setText("");
        }
    }

    private void showBedroomsMultiChosenValue(List<CatalogModel> catalog, List<Integer> categoryIdList, TextView valueTv){
        if (categoryIdList != null) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < categoryIdList.size(); i++) {
                Integer bedroomId = categoryIdList.get(i);
                if (bedroomId != null) {
                    CatalogModel catalogModel = getCatalogById(catalog, bedroomId);
                    if (catalogModel != null) {
                        sb.append(catalogModel.getName());
                        if (i != categoryIdList.size() - 1) {
                            sb.append(", ");
                            if ((i + 1) % 2 == 0) {
                                sb.append('\n');
                            }
                        }
                    }
                }
            }
            if (sb.length() != 0) {
                valueTv.setText(sb.toString());
            } else {
                valueTv.setText("");
            }
        }
    }

    private void showSharedDetailsDialog(List<CatalogModel> catalogList, Integer currentId,
                                         CatalogListChooserDialog.OKButtonClickListener okButtonClick, String title) {
        List<Integer> currentIdList = new ArrayList<>();
        currentIdList.add(currentId);
        CatalogListChooserDialog sharedDetailsChooserDialog = new CatalogListChooserDialog(getContext(), catalogList,
                currentIdList, false, okButtonClick, title);
        sharedDetailsChooserDialog.show();
    }


    @Nullable
    @OnClick(R.id.el_bedroom_layout)
    public void onBedroom() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getBedroomsList();
        if (catalogModels == null) {
            mNeedToShowBedroomsDialog = true;
            mCatalogRestService.loadCatalogs();
        } else {
                showSharedDetailsDialog(catalogModels, mBedroomId, new CatalogListChooserDialog.OKButtonClickListener() {

                    @Override
                    public void onOkClick(List<Integer> categoryIds) {
                        if (categoryIds != null && !categoryIds.isEmpty()) {
                            mBedroomId = categoryIds.get(0);
                            showChosenValue(catalogModels, mBedroomId, mBedroomValueTv);
                        }
                    }
                }, getContext().getString(R.string.al_bedrooms));
        }
    }


    @Nullable
    @OnClick(R.id.el_bath_layout)
    public void onBath() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getBathroomsList();
        if (catalogModels == null) {
            mNeedToShowBathDialog = true;
            mCatalogRestService.loadCatalogs();
        } else {
            showSharedDetailsDialog(catalogModels, mBathroomId, new CatalogListChooserDialog.OKButtonClickListener() {

                @Override
                public void onOkClick(List<Integer> categoryIds) {
                    if (categoryIds != null && !categoryIds.isEmpty()) {
                        mBathroomId = categoryIds.get(0);
                        showChosenValue(catalogModels, mBathroomId, mBathValueTv);
                    }
                }
            }, getContext().getString(R.string.al_bath));
        }
    }


    @Nullable
    @OnClick(R.id.el_minimum_stay_layout)
    public void onMinStay() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getMinimumStayList();
        if (catalogModels == null) {
            mNeedToShowMinStayDialog = true;
            mCatalogRestService.loadCatalogs();
        } else {
            showSharedDetailsDialog(catalogModels, mMinStayId, new CatalogListChooserDialog.OKButtonClickListener() {

                @Override
                public void onOkClick(List<Integer> categoryIds) {
                    if (categoryIds != null && !categoryIds.isEmpty()) {
                        mMinStayId = categoryIds.get(0);
                        showChosenValue(catalogModels, mMinStayId, mMinStayValueTv);
                    }
                }
            }, getContext().getString(R.string.al_minimum_stay));
        }
    }


    @Nullable
    @OnClick(R.id.el_additional_minimum_stay_layout)
    public void onAdditionalMinStay() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getMinimumStayList();
        if (catalogModels == null) {
            mNeedToShowAdditionalMinStayDialog = true;
            mCatalogRestService.loadCatalogs();
        } else {
            showSharedDetailsDialog(catalogModels, mAdditionalMinStayId, new CatalogListChooserDialog.OKButtonClickListener() {

                @Override
                public void onOkClick(List<Integer> categoryIds) {
                    if (categoryIds != null && !categoryIds.isEmpty()) {
                        mAdditionalMinStayId = categoryIds.get(0);
                        showChosenValue(catalogModels, mAdditionalMinStayId, mAdditionalMinStayValueTv);
                    }
                }
            }, getContext().getString(R.string.al_minimum_stay));
        }
    }


    @Nullable
    @OnClick(R.id.el_smoking_preference_layout)
    public void onSmokingPreference() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getSmokingPreferencesList();
        if(catalogModels == null) {
            mNeedToSmokingPreferenceDialog = true;
            mCatalogRestService.loadCatalogs();
        } else {
            showSharedDetailsDialog(catalogModels, mSmokingPreferenceId, new CatalogListChooserDialog.OKButtonClickListener() {
                @Override
                public void onOkClick(List<Integer> categoryId) {
                    if (categoryId != null && !categoryId.isEmpty()) {
                        mSmokingPreferenceId = categoryId.get(0);
                        showChosenValue(catalogModels, mSmokingPreferenceId, mSmokingPreferenceValueTv);
                    }
                }
            }, getContext().getString(R.string.smoking_preference));
        }
    }


    @Nullable
    @OnClick(R.id.el_sex_household_layout)
    public void onSexHousehold() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getHouseholdSexList();
        if(catalogModels == null) {
            mNeedToShowSexHouseholdDialog = true;
            mCatalogRestService.loadCatalogs();
        } else {
            showSharedDetailsDialog(catalogModels, mSexHouseholdId, new CatalogListChooserDialog.OKButtonClickListener() {
                @Override
                public void onOkClick(List<Integer> categoryId) {
                    if (categoryId != null && !categoryId.isEmpty()) {
                        mSexHouseholdId = categoryId.get(0);
                        showChosenValue(catalogModels, mSexHouseholdId, mSexHouseholdValueTv);
                    }
                }
            }, getContext().getString(R.string.discovery_household_sex));
        }
    }


    @Nullable
    @OnClick(R.id.el_residence_building_type_layout)
    public void onResidenceBuildType() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getBuildingTypesList();
        if(catalogModels == null) {
            mNeedToShowBuildTypeDialog = true;
            mCatalogRestService.loadCatalogs();
        } else {
            showSharedDetailsDialog(catalogModels, mResidenceBuildTypeId, new CatalogListChooserDialog.OKButtonClickListener() {
                @Override
                public void onOkClick(List<Integer> categoryId) {
                    if (categoryId != null && !categoryId.isEmpty()) {
                        mResidenceBuildTypeId = categoryId.get(0);
                        showChosenValue(catalogModels, mResidenceBuildTypeId, mResidenceBuildTypeValue);
                    }
                }
            }, getContext().getString(R.string.el_residence_building_type_title));
        }
    }


    @Nullable
    @OnClick(R.id.el_people_in_household_layout)
    public void onPeopleInHousehold() {
        final List<CatalogModel> catalogModels = mCatalogsManager.getPeopleInHouseholdList();
        if(catalogModels == null) {
            mNeedToShowPeopleInHouseholdDialog = true;
            mCatalogRestService.loadCatalogs();
        } else {
            showSharedDetailsDialog(catalogModels, mPeopleInHouseholdId, new CatalogListChooserDialog.OKButtonClickListener() {
                @Override
                public void onOkClick(List<Integer> categoryId) {
                    if (categoryId != null && !categoryId.isEmpty()) {
                        mPeopleInHouseholdId = categoryId.get(0);
                        showChosenValue(catalogModels, mPeopleInHouseholdId, mPeopleInHouseholdValueTv);
                    }
                }
            }, getContext().getString(R.string.el_people_in_household_title));
        }
    }


}
