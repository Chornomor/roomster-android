package com.roomster.manager;


import com.google.gson.Gson;
import com.roomster.application.RoomsterApplication;
import com.roomster.cache.sharedpreferences.SharedPreferencesFiles;
import com.roomster.model.CatalogValueModel;
import com.roomster.rest.model.ListingGetViewModel;
import com.roomster.rest.model.UserGetViewModel;
import com.roomster.views.undo.ItemExtractor;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by "Michael Katkov" on 10/12/2015.
 */
public class MeManager {

    public static final String FAKE_SERVER_IMAGE = "https://halcyon-static.roomster.com/Content/images/placeholders/no_user_original.gif";

    private static final String CURRENT_USER_CACHE_KEY = "CURRENT_USER_CACHE_KEY";

    private static final int FIRST = 1;

    private UserGetViewModel mCurrentUser;

    private List<ListingGetViewModel> mMyListings;

    private List<CatalogValueModel> mCurrentAmenities;

    @Inject
    SharedPreferencesFiles mSharedPreferencesFiles;


    public MeManager() {
        RoomsterApplication.getRoomsterComponent().inject(this);
        mCurrentUser = getCurrentUserFromCache();
        mCurrentAmenities = new ArrayList<>();
    }


    public UserGetViewModel getMe() {
        return mCurrentUser;
    }


    public void setMe(UserGetViewModel currentUser) {
        cacheCurrentUser(currentUser);
        mCurrentUser = currentUser;
    }


    public void setIsNewUser(boolean isNewUser) {
        mCurrentUser.setIsNewUser(isNewUser);
    }


    public boolean getIsNewUser() {
        return mCurrentUser.getIsNewUser();
    }


    private void cacheCurrentUser(UserGetViewModel currentUser) {
        if (mCurrentUser != currentUser) {
            mSharedPreferencesFiles.getUserSharedPreferences().edit()
                    .putString(CURRENT_USER_CACHE_KEY, new Gson().toJson(currentUser)).commit();
        }
    }


    private UserGetViewModel getCurrentUserFromCache() {
        synchronized (this) {
            String currentUserJson = mSharedPreferencesFiles.getUserSharedPreferences()
                    .getString(CURRENT_USER_CACHE_KEY, null);
            if (currentUserJson != null) {
                return new Gson().fromJson(currentUserJson, UserGetViewModel.class);
            } else {
                return null;
            }
        }
    }


    public List<ListingGetViewModel> getMyListings() {
        return mMyListings;
    }


    public void setMyListings(List<ListingGetViewModel> myListings) {
        mMyListings = myListings;
    }


    public ListingGetViewModel findListingById(Long id) {
        if (mMyListings != null) {
            for (ListingGetViewModel listingGetViewModel : mMyListings) {
                if (listingGetViewModel.getListingId().equals(id)) {
                    return listingGetViewModel;
                }
            }
        }
        return null;
    }


    public void updateMyListings(ListingGetViewModel listing) {
        for (int i = 0; i < mMyListings.size(); i++) {
            if (mMyListings.get(i).getListingId().equals(listing.getListingId())) {
                mMyListings.set(i, listing);
            }
        }
    }


    public boolean hasTheFirstListing() {
        if (getMyListings() != null && getMyListings().size() == FIRST) {
            return true;
        }
        return false;
    }


    public List<CatalogValueModel> getCurrentAmenities() {
        return mCurrentAmenities;
    }


    public void setCurrentAmenities(List<CatalogValueModel> currentAmenities) {
        mCurrentAmenities = currentAmenities;
    }


    public void addCurrentAmenity(CatalogValueModel currentAmenity) {
        mCurrentAmenities.add(currentAmenity);
    }

}
