package com.roomster.rest.service;


import android.content.Context;
import android.support.annotation.Nullable;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.RestCode;
import com.roomster.enums.LabelEnum;
import com.roomster.enums.StatusEnum;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.TokenManager;
import com.roomster.rest.api.ConversationCountersApi;
import com.roomster.rest.api.ConversationsApi;
import com.roomster.rest.api.MessagesApi;
import com.roomster.rest.model.ConversationCounter;
import com.roomster.rest.model.ConversationMessagePostViewModel;
import com.roomster.rest.model.ConversationPutViewModel;
import com.roomster.rest.model.ConversationView;
import com.roomster.rest.model.MessageView;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ConversationsRestService {

    private static final String INBOX = "Inbox";

    @Inject
    TokenManager mTokenManager;

    @Inject
    ConversationsApi mConversationsApi;

    @Inject
    MessagesApi mMessagesApi;

    @Inject
    ConversationCountersApi mConversationCounterApi;

    @Inject
    Context mContext;

    @Inject
    NetworkManager mNetworkManager;


    public ConversationsRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void getConversations(final boolean isNewDelta, final String lastMessageId,
                                 final RestServiceListener<List<ConversationView>> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callGetConversations(result, isNewDelta, lastMessageId, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                listener.onFailure(code, throwable, msg);
            }
        });
    }


    public void getAllConversations(final RestServiceListener<List<ConversationView>> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callGetAllConversations(result, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                listener.onFailure(code, throwable, msg);
            }
        });
    }


    public void sendInitialMessage(final Long recipientUserId, final String messageText, @Nullable final Long listingId,
                                   final RestServiceListener<String> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callMessagePost(result, null, recipientUserId, messageText, listingId, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                listener.onFailure(code, throwable, msg);
            }
        });
    }


    public void replyToMessage(final String conversationId, final String messageText,
                               final RestServiceListener<String> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callMessagePost(result, conversationId, null, messageText, null, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                listener.onFailure(code, throwable, msg);
            }
        });
    }


    public void getMessages(final String conversationId, final String lastMessageId, final Boolean isNewDelta,
                            final RestServiceListener<List<MessageView>> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callGetMessages(result, conversationId, lastMessageId, isNewDelta, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                listener.onFailure(code, throwable, msg);
            }
        });
    }


    public void getAllMessages(final String conversationId, final RestServiceListener<List<MessageView>> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callGetAllMessages(result, conversationId, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                listener.onFailure(code, throwable, msg);
            }
        });
    }


    public void getConversationCounters(final RestServiceListener<List<ConversationCounter>> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callGetConversationCounters(result, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                listener.onFailure(code, throwable, msg);
            }
        });
    }


    public void updateConversation(final String conversationId, final StatusEnum status, final RestServiceListener listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callPutConversation(result, conversationId, status, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                if (listener != null) {
                    listener.onFailure(code, throwable, msg);
                }
            }
        });
    }


    public void deleteConversation(final String conversationId, final RestServiceListener<String> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callDeleteConversation(result, conversationId, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                listener.onFailure(code, throwable, msg);
            }
        });
    }


    private void callDeleteConversation(String authToken, String conversationId, final RestServiceListener<String> listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<String> callDeleteConversation = mConversationsApi.conversationsDelete(conversationId, authToken);
            callDeleteConversation.enqueue(new Callback<String>() {

                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            listener.onSuccess(response.body());
                            break;
                        default:
                            listener.onFailure(response.code(), null, null);
                    }
                }


                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    listener.onFailure(RestCode.REQUEST_NOT_MADE, t, mContext.getString(R.string.error_delete_conversations));
                }
            });
        } else {
            listener.onFailure(RestCode.REQUEST_NOT_MADE, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
        }
    }


    private void callGetMessages(String authToken, String conversationId, String lastMessageId, Boolean isNewDelta,
                                 final RestServiceListener<List<MessageView>> listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<List<MessageView>> callMessageGet = mMessagesApi
              .messagesGet(conversationId, 0, authToken, lastMessageId, isNewDelta);
            callMessageGet.enqueue(new Callback<List<MessageView>>() {

                @Override
                public void onResponse(Call<List<MessageView>> call, Response<List<MessageView>> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            listener.onSuccess(response.body());
                            break;
                        default:
                            listener.onFailure(response.code(), null, null);
                    }
                }


                @Override
                public void onFailure(Call<List<MessageView>> call, Throwable t) {
                    listener.onFailure(RestCode.REQUEST_NOT_MADE, t, mContext.getString(R.string.error_getting_messages));
                }
            });
        } else {
            listener.onFailure(RestCode.REQUEST_NOT_MADE, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
        }
    }


    private void callGetAllMessages(String authToken, String conversationId,
                                    final RestServiceListener<List<MessageView>> listener) {
        callGetMessages(authToken, conversationId, null, null, listener);
    }


    private void callMessagePost(String authToken, @Nullable String conversationId, @Nullable Long recipientUserId,
                                 String messageText, @Nullable Long listingId, final RestServiceListener<String> listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            ConversationMessagePostViewModel message = new ConversationMessagePostViewModel();
            message.setConversation(conversationId);
            message.setMessageText(messageText);
            message.setRecipientUserId(recipientUserId);
            message.setListingId(listingId);
            Call<String> callMessagePost = mMessagesApi.messagesPost(message, authToken);
            callMessagePost.enqueue(new Callback<String>() {

                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            listener.onSuccess(response.body());
                            break;
                        default:
                            listener.onFailure(response.code(), null, response.body());
                    }
                }


                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    listener.onFailure(RestCode.REQUEST_NOT_MADE, t, mContext.getString(R.string.error_seding_message));
                }
            });
        } else {
            listener.onFailure(RestCode.REQUEST_NOT_MADE, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
        }
    }


    private void callGetConversationCounters(String authToken, final RestServiceListener<List<ConversationCounter>> listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<List<ConversationCounter>> callGetConversationCounters = mConversationCounterApi
              .conversationCountersGet(authToken);

            callGetConversationCounters.enqueue(new Callback<List<ConversationCounter>>() {

                @Override
                public void onResponse(Call<List<ConversationCounter>> call, Response<List<ConversationCounter>> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            listener.onSuccess(response.body());
                            break;
                        case ResponseCode.BAD_REQUEST:
                        case ResponseCode.SERVER_ERROR:
                        default:
                            listener.onFailure(response.code(), null, null);
                    }
                }


                @Override
                public void onFailure(Call<List<ConversationCounter>> call, Throwable t) {
                    listener.onFailure(RestCode.REQUEST_NOT_MADE, t, mContext.getString(R.string.error_getting_messages_count));
                }
            });
        } else {
            listener.onFailure(RestCode.REQUEST_NOT_MADE, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
        }
    }


    private void callGetAllConversations(String authToken, final RestServiceListener<List<ConversationView>> listener) {
        callGetConversations(authToken, false, "", listener);
    }


    private void callGetConversations(String authToken, boolean isNewDelta, String lastMessageId,
                                      final RestServiceListener<List<ConversationView>> listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<List<ConversationView>> callGetConversations = mConversationsApi
              .conversationsGet(authToken, INBOX, 0, isNewDelta, lastMessageId, "");
            callGetConversations.enqueue(new Callback<List<ConversationView>>() {

                @Override
                public void onResponse(Call<List<ConversationView>> call, Response<List<ConversationView>> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            listener.onSuccess(response.body());
                            break;
                        case ResponseCode.BAD_REQUEST:
                        case ResponseCode.SERVER_ERROR:
                        default:
                            listener.onFailure(response.code(), null, null);
                    }
                }


                @Override
                public void onFailure(Call<List<ConversationView>> call, Throwable t) {
                    listener.onFailure(RestCode.REQUEST_NOT_MADE, t, mContext.getString(R.string.error_loading_conversations));
                }
            });
        } else {
            listener.onFailure(RestCode.REQUEST_NOT_MADE, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
        }
    }


    private void callPutConversation(String authToken, final String conversationId, final StatusEnum status,
                                     final RestServiceListener<String> listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            ConversationPutViewModel model = new ConversationPutViewModel();
            model.setConversation(conversationId);
            model.setStatus(status);
            model.setLabel(LabelEnum.Inbox);

            Call<String> callPutConversation = mConversationsApi.conversationsPut(model, authToken);
            callPutConversation.enqueue(new Callback<String>() {

                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            if (listener != null) {
                                listener.onSuccess(response.body());
                            }
                            break;
                        case ResponseCode.BAD_REQUEST:
                        case ResponseCode.SERVER_ERROR:
                        default:
                            listener.onFailure(response.code(), null, null);
                    }
                }


                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    listener.onFailure(RestCode.REQUEST_NOT_MADE, t, mContext.getString(R.string.error_update_messages));
                }
            });
        } else {
            listener.onFailure(RestCode.REQUEST_NOT_MADE, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
        }
    }
}