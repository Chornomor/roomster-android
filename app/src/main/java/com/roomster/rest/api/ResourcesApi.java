package com.roomster.rest.api;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ResourcesApi {

    /**
     * Get resources All resources: &#39;privacy,terms&#39;. \n If not passed the entire list is returned. &lt;br /&gt;
     * \n Pass Locale for a localized version of the resource(s)
     *
     * @param resources
     *   Comma-delimited list of resources. Optional.
     * @param currency
     *   currency (USD, EUR, GBP etc.)
     * @param locale
     *   locale (en-US, ru-RU, ja-JP etc.)
     * @return Call<Object>
     */

    @GET("Resources")
    Call<Object> resourcesGet(@Query("resources") String resources, @Header("Currency") String currency,
                              @Header("Locale") String locale);

    /**
     * Get resources All resources: &#39;privacy,terms&#39;. \n If not passed the entire list is returned. &lt;br /&gt;
     * \n Pass Locale for a localized version of the resource(s)
     *
     * @param resources
     *   Comma-delimited list of resources. Optional.
     * @param currency
     *   currency (USD, EUR, GBP etc.)
     * @param locale
     *   locale (en-US, ru-RU, ja-JP etc.)
     * @return Call<Object>
     */

    @GET("/v1/resources/{resources}")
    Call<Object> resourcesGet_1(@Path("resources") String resources, @Header("Currency") String currency,
                                @Header("Locale") String locale);
}
