package com.roomster.rest.api;


import com.roomster.rest.model.ListingCreationResultModel;
import com.roomster.rest.model.ListingGetViewModel;
import com.roomster.rest.model.ListingPostViewModel;
import com.roomster.rest.model.ListingPutViewModel;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;


public interface ListingsApi {

    /**
     * Update listing Common for &lt;b&gt;ALL&lt;/b&gt; listing types \n &lt;ul&gt;&lt;li&gt;listing_id
     * (Required)&lt;/li&gt;&lt;li&gt;headline&lt;/li&gt;&lt;li&gt;description&lt;/li&gt;&lt;li&gt;is_active&lt;/li&gt;&
     * lt;li&gt;geo_location&lt;/li&gt;&lt;li&gt;rates&lt;/li&gt;&lt;li&gt;calendar&lt;/li&gt;&lt;/ul&gt; \n For
     * &lt;b&gt;NeedRoom&lt;/b&gt; include: \n &lt;ul&gt;&lt;li&gt;share_details (&lt;b&gt;skip&lt;/b&gt;
     * household_age_min, household_age_max, \n household_sex, people_in_household,movein_fee,utilities_cost,
     * parking_rent, \n general_location, is_furnished,
     * building_type)&lt;/li&gt;&lt;li&gt;questions&lt;/li&gt;&lt;li&gt;tags&lt;/li&gt;&lt;/ul&gt; \n For
     * &lt;b&gt;HaveShare&lt;/b&gt; include: \n &lt;ul&gt;&lt;li&gt;share_details (&lt;b&gt;include&lt;/b&gt;
     * household_age_min, household_age_max, \n household_sex, people_in_household,movein_fee,utilities_cost,
     * parking_rent, \n general_location, is_furnished,
     * building_type)&lt;/li&gt;&lt;li&gt;questions&lt;/li&gt;&lt;li&gt;tags&lt;/li&gt;&lt;/ul&gt; \n For
     * &lt;b&gt;NeedApartment&lt;/b&gt; include: \n
     * &lt;ul&gt;&lt;li&gt;tags&lt;/li&gt;&lt;li&gt;apartment_amenities&lt;/li&gt;&lt;/ul&gt; \n For
     * &lt;b&gt;HaveApartment&lt;/b&gt; include: \n
     * &lt;ul&gt;&lt;li&gt;tags&lt;/li&gt;&lt;li&gt;apartment_amenities&lt;/li&gt;&lt;/ul&gt;
     *
     * @param model
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<Object>
     */

    @PUT("Listings")
    Call<ResponseBody> listingsPut(@Body ListingPutViewModel model, @Header("Authorization") String authorization);

    /**
     * Create new listing For &lt;b&gt;NeedRoom&lt;/b&gt; include: \n
     * &lt;ul&gt;&lt;li&gt;service_type&lt;/li&gt;&lt;li&gt;geo_location&lt;/li&gt;&lt;li&gt;calendar
     * (&lt;b&gt;skip&lt;/b&gt; minimum_stay_id)&lt;/li&gt;&lt;li&gt;rates&lt;/li&gt;&lt;/ul&gt; \n For
     * &lt;b&gt;HaveShare&lt;/b&gt; include: \n
     * &lt;ul&gt;&lt;li&gt;service_type&lt;/li&gt;&lt;li&gt;geo_location&lt;/li&gt;&lt;li&gt;calendar
     * (&lt;b&gt;include&lt;/b&gt; minimum_stay_id)&lt;/li&gt;&lt;li&gt;rates&lt;/li&gt;&lt;/ul&gt; \n For
     * &lt;b&gt;NeedApartment&lt;/b&gt; include: \n
     * &lt;ul&gt;&lt;li&gt;service_type&lt;/li&gt;&lt;li&gt;geo_location&lt;/li&gt;&lt;li&gt;calendar
     * (&lt;b&gt;skip&lt;/b&gt;
     * minimum_stay_id)&lt;/li&gt;&lt;li&gt;rates&lt;/li&gt;&lt;li&gt;need_apartment&lt;/li&gt;&lt;/ul&gt; \n For
     * &lt;b&gt;HaveApartment&lt;/b&gt; include: \n
     * &lt;ul&gt;&lt;li&gt;service_type&lt;/li&gt;&lt;li&gt;geo_location&lt;/li&gt;&lt;li&gt;calendar
     * (&lt;b&gt;include&lt;/b&gt;
     * minimum_stay_id)&lt;/li&gt;&lt;li&gt;rates&lt;/li&gt;&lt;li&gt;have_apartment&lt;/li&gt;&lt;/ul&gt;
     *
     * @param model
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<Object>
     */

    @POST("Listings")
    Call<ListingCreationResultModel> listingsPost(@Body ListingPostViewModel model, @Header("Authorization") String authorization);

    /**
     * Get listing
     *
     * @param id
     *   Listing id
     * @param currency
     *   currency (USD, EUR, GBP etc.)
     * @param locale
     *   locale (en-US, ru-RU, ja-JP etc.)
     * @return Call<ListingGetViewModel>
     */

    @GET("Listings/{id}")
    Call<ListingGetViewModel> listingsGet(@Path("id") Long id, @Header("Currency") String currency,
                                          @Header("Locale") String locale);

    /**
     * Delete listing
     *
     * @param id
     *   Listing id
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<Object>
     */

    @DELETE("Listings/{id}")
    Call<String> listingsDelete(@Path("id") Long id, @Header("Authorization") String authorization);
}
