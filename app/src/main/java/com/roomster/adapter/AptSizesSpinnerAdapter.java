package com.roomster.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.model.CatalogModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


/**
 * Created by michaelkatkov on 12/8/15.
 */
public class AptSizesSpinnerAdapter extends ArrayAdapter<CatalogModel> {

    private static final String ONE = "1";

    @Inject
    LayoutInflater mLayoutInflater;


    public AptSizesSpinnerAdapter(Context context, List<CatalogModel> objects) {
        super(context, 0, objects);
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.item_spinner_add_listing_layout, parent, false);
        }
        TextView textV = (TextView) view.findViewById(R.id.spinnerItem);
        CatalogModel aptSize = getItem(position);
        textV.setText(splitString(aptSize.getName()));
        return view;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.item_dropdown_add_listing_layout, parent, false);
        }
        TextView textV = (TextView) view.findViewById(R.id.spinnerDropDownItem);
        CatalogModel aptSize = getItem(position);
        textV.setText(splitString(aptSize.getName()));
        return view;
    }

    private String splitString(String string) {
        String result = "";
        List<String> words = new ArrayList<>();
        int appLetterIndex = 0;
        for (int i = 0; i < string.length(); i++) {
            if (Character.isUpperCase(string.charAt(i)) && appLetterIndex != i) {
                words.add(string.substring(appLetterIndex, i));
                appLetterIndex = i;
            }
        }
        words.add(string.substring(appLetterIndex, string.length()));
        for (String word : words) {
            result = result + word + " ";
        }
        return result;
    }
}