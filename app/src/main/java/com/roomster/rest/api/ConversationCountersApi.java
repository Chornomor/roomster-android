package com.roomster.rest.api;


import com.roomster.rest.model.ConversationCounter;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;


public interface ConversationCountersApi {

    /**
     * Get mailbox counters for all labels
     *
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<List<ConversationCounter>>
     */

    @GET("ConversationCounters")
    Call<List<ConversationCounter>> conversationCountersGet(@Header("Authorization") String authorization);
}
