package com.roomster.manager.map;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.roomster.rest.model.ListingGetViewModel;
import com.roomster.rest.model.ResultItemModel;
import com.roomster.rest.model.ResultModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by andreybofanov on 11.07.16.
 */
public class MapCluster {

    LatLngBounds bounds;
    LatLng center;
    List<ResultItemModel> results = new ArrayList<>();
    public boolean loaded = false;

    public MapCluster(LatLngBounds bounds) {
        this.bounds = bounds;
        this.center = bounds.getCenter();
    }
    public MapCluster(LatLng center, LatLngBounds bounds) {
        this.center = center;
        this.bounds = bounds;
    }

    public void putResult(ResultItemModel... res){
        results.addAll(Arrays.asList(res));
    }
    public LatLngBounds getBounds() {
        return bounds;
    }

    public LatLng getCenter(){
        return center;
    }

}
