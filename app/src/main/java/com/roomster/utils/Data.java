package com.roomster.utils;

import java.util.List;

/**
 * Created by andreybofanov on 07.07.16.
 */
public class Data {

    public static void safeAdd(List list,int position, Object object){
        if(position >= list.size() || position < 0)
            list.add(object);
        else
            list.add(position,object);
    }
}
