package com.roomster.fragment;


import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.dialog.ReportUserDialog;
import com.roomster.dialog.SendMessageDialog;
import com.roomster.dialog.SocialConnectionsDialog;
import com.roomster.event.RestServiceEvents.LikesRestServiceEvent;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.fragment.FragmentRequest;
import com.roomster.event.fragment.UserProfileFragmentEvent;
import com.roomster.manager.MeManager;
import com.roomster.manager.SearchManager;
import com.roomster.rest.model.ListingGetViewModel;
import com.roomster.rest.model.MutualFriendsModel;
import com.roomster.rest.model.SocialConnectionViewModel;
import com.roomster.rest.model.UserGetViewModel;
import com.roomster.rest.model.UserReportViewModel;
import com.roomster.rest.service.ConversationsRestService;
import com.roomster.rest.service.FbMutualFriendsService;
import com.roomster.rest.service.LikesRestService;
import com.roomster.rest.service.UsersRestService;
import com.roomster.views.UserProfileView;
import com.roomster.views.toolbar.RRToolbar;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Field;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;


public class UserProfileFragment extends Fragment {

    private static final String USER = "user";
    private static final String USER_LISTING = "user_listing";

    private ListingGetViewModel mUserListing;
    private UserGetViewModel mUser;

    @Bind(R.id.profile_user_view)
    UserProfileView mProfileView;

    @Bind(R.id.profile_view_toolbar)
    RRToolbar mBottomToolbar;


    @Bind(R.id.profile_view_toolbar_container)
    LinearLayout mToolbarContainer;

    @Inject
    EventBus mEventBus;

    @Inject
    SearchManager mSearchManager;

    @Inject
    UsersRestService mUsersRestService;

    @Inject
    LikesRestService mLikesRestService;

    @Inject
    FbMutualFriendsService mFbMutualFriends;

    @Inject
    Context mContext;

    @Inject
    ConversationsRestService mConversationsRestService;

    @Inject
    MeManager mMeManager;


    public static UserProfileFragment newInstance(UserGetViewModel user, ListingGetViewModel userListing) {
        UserProfileFragment fragment = new UserProfileFragment();
        Bundle args = new Bundle();
        args.putSerializable(USER, user);

        if (userListing != null) {
            args.putSerializable(USER_LISTING, userListing);
        }

        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            mUser = (UserGetViewModel) args.getSerializable(USER);
            if (args.containsKey(USER_LISTING)) {
                mUserListing = (ListingGetViewModel) args.getSerializable(USER_LISTING);
            }
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);

        initData();
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        if (!mEventBus.isRegistered(this)) {
            mEventBus.register(this);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        mEventBus.post(new FragmentRequest.SetTitleRequest(getString(R.string.user_profile), null));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public void onStop() {
        if (mEventBus.isRegistered(this)) {
            mEventBus.unregister(this);
        }
        super.onStop();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(LikesRestServiceEvent.GetLikesSuccess event) {
        mProfileView.setUserLikes(event.likes);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(LikesRestServiceEvent.GetLikesFailure event) {
        Toast.makeText(mContext, getString(R.string.unable_to_get_user_info), Toast.LENGTH_SHORT).show();
    }


    private void initData() {
        setUser();
        getUserLikes();
        getMutualFriends();
    }

    private void setUser() {
        if (mUser != null) {
            mProfileView.setUserGetViewModel(mUser);
            mProfileView.setOnShowListingsClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    mEventBus.post(new UserProfileFragmentEvent.ShowListings());
                }
            });
            initToolbar(mUser);
        }
    }

    private void getUserLikes() {
        if (mUser != null) {
            mLikesRestService.getLikes(mUser.getId(), null);
        }
    }

    private void getMutualFriends() {
        UserGetViewModel mMe = mMeManager.getMe();
        if (mMe == null || mMe.getId() == null || mUser == null || mUser.getId() == null || mMe.getId() == mUser.getId())
            return;

        mFbMutualFriends.callGetFbMutualFriends(mUser.getId(), new RestServiceListener<MutualFriendsModel>() {
                @Override
                public void onSuccess(MutualFriendsModel result) {
                    if (result != null){
                        int mutualFriendsCount = result.total_count;
                        if (mutualFriendsCount > 0) mProfileView.setMutualFriends(result.total_count);
                        else  mProfileView.hideMutualFriends();
                    }
                }

                @Override
                public void onFailure(int code, Throwable throwable, String msg) {
                }
            });
    }

    private void initToolbar(final UserGetViewModel user) {
        mBottomToolbar.setupFromUser(user);
        mBottomToolbar.setListener(new RRToolbar.OnActionListener() {
            @Override
            public void onRRToolbarActionClick(int id) {
                onToolbarItemClick(id,user);
            }
        });
        mProfileView.setPanelSlidingListener(new ToolbarSlider());
    }


    private void onCreateToolbarMenu(Menu menu, UserGetViewModel user) {
        String phone = user.getPhone();

        boolean hasPhoneNumber = phone != null && !phone.isEmpty();
        boolean hasSocialConnections = user.getSocialConnections() != null && !user.getSocialConnections().isEmpty();

        MenuItem actionCall = menu.findItem(R.id.contact_action_call);
        if (actionCall != null) {
            actionCall.setVisible(hasPhoneNumber);
        }

        MenuItem actionSms = menu.findItem(R.id.contact_action_sms);
        if (actionSms != null) {
            actionSms.setVisible(hasPhoneNumber);
        }

        MenuItem actionSocial = menu.findItem(R.id.contact_action_social);
        if (actionSocial != null) {
            actionSocial.setVisible(hasSocialConnections);
        }

        MenuItem actionBookmark = menu.findItem(R.id.contact_action_bookmark);
        if (actionBookmark != null) {
            actionBookmark.setVisible(false);
        }

        MenuItem actionShare = menu.findItem(R.id.contact_action_share);
        if (actionShare != null) {
            actionShare.setVisible(false);
        }
    }


    private void onToolbarItemClick(int id, UserGetViewModel user) {
        String phone = user.getPhone();
        switch (id) {
            case R.id.contact_action_message:
                showMessageDialog(user);
                break;
            case R.id.contact_action_call:
                sendPhoneIntent(user, Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
                break;
            case R.id.contact_action_sms:
                sendPhoneIntent(user, Intent.ACTION_VIEW, Uri.fromParts("sms", phone, null));
                break;
            case R.id.contact_action_report:
                showReportUserDialog();
                break;
            case R.id.contact_action_social:
                showSocialConnectionsDialog(user);
                break;
        }
    }


    private void sendPhoneIntent(UserGetViewModel user, String intentAction, Uri uri) {
        String phone = user.getPhone();
        if (phone != null) {
            if (PhoneNumberUtils.isGlobalPhoneNumber(phone)) {
                Intent intent = new Intent(intentAction, uri);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    // do nothing
                }
            } else if (!phone.isEmpty()) {
                Toast.makeText(getContext(), phone, Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void showSocialConnectionsDialog(UserGetViewModel user) {
        List<SocialConnectionViewModel> connections = user.getSocialConnections();
        new SocialConnectionsDialog(getContext(), connections, user.getFirstName()).show();
    }


    private void showMessageDialog(final UserGetViewModel user) {
        if (user != null) {
            SendMessageDialog dialog = new SendMessageDialog(getContext(), user.getFirstName(),
                    new SendMessageDialog.SendMessageListener() {

                        @Override
                        public void onSendMessage(String message) {
                            if (mUserListing != null) {
                                mConversationsRestService.sendInitialMessage(user.getId(), message, mUserListing.getListingId(),
                                        new RestServiceListener<String>() {

                                            @Override
                                            public void onSuccess(String result) {
                                                Context context = getActivity();
                                                if (context != null) {
                                                    Toast
                                                            .makeText(getContext(), R.string.detailed_view_send_message_success, Toast.LENGTH_SHORT)
                                                            .show();
                                                }
                                            }


                                            @Override
                                            public void onFailure(int code, Throwable throwable, String msg) {
                                                Context context = getActivity();
                                                if (context != null) {
                                                    Toast.makeText(getContext(), R.string.detailed_view_send_message_fail, Toast.LENGTH_SHORT)
                                                            .show();
                                                }
                                            }
                                        });
                            }
                        }
                    });
            dialog.show();
        }
    }


    private void showReportUserDialog() {
        ReportUserDialog dialog = new ReportUserDialog(getActivity(), new ReportUserDialog.SubmitReportListener() {

            @Override
            public void onReportSubmitted(UserReportViewModel userReportViewModel) {
                if (mUser != null) {
                    userReportViewModel.setUserId(mUser.getId());
                    mUsersRestService.reportUser(userReportViewModel, new RestServiceListener<String>() {

                        @Override
                        public void onSuccess(String result) {
                            Context context = getActivity();
                            if (context != null) {
                                Toast.makeText(context, getString(R.string.messages_user_reported_toast), Toast.LENGTH_SHORT)
                                        .show();
                            }
                        }


                        @Override
                        public void onFailure(int code, Throwable throwable, String msg) {
                            Context context = getActivity();
                            if (context != null) {
                                Toast.makeText(context, getString(R.string.messages_user_not_reported_toast), Toast.LENGTH_SHORT)
                                        .show();
                            }
                        }
                    });
                }
            }
        });

        dialog.show();
    }


    private class ToolbarSlider implements SlidingUpPanelLayout.PanelSlideListener {

        private int mToolbarHeight;


        private ToolbarSlider() {
            mToolbarHeight = (int) getActivity().getResources().getDimension(R.dimen.detailed_view_toolbar_height_with_border);
        }


        @Override
        public void onPanelSlide(View panel, float slideOffset) {
//            slideToolbar(slideOffset);
        }


        @Override
        public void onPanelCollapsed(View panel) {
//            slideToolbar(0);
        }


        @Override
        public void onPanelExpanded(View panel) {
//            slideToolbar(1);
        }


        @Override
        public void onPanelAnchored(View panel) {
            // nothing to do
        }


        @Override
        public void onPanelHidden(View panel) {
            // nothing to do
        }


        private void slideToolbar(float offset) {
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mToolbarContainer.getLayoutParams();
            params.height = (int) (mToolbarHeight * offset);
            mToolbarContainer.setLayoutParams(params);
        }
    }
}