package com.roomster.listener;

/**
 * Created by Bogoi.Bogdanov on 5/12/2016.
 */
public interface BookmarksCallbackListener {

    void onBookmarksCountChanged(int count);

    void onPageChanged(int pageNumber);
}
