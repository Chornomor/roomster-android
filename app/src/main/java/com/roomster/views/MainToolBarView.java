package com.roomster.views;


import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.roomster.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by "Michael Katkov" on 10/31/2015.
 */
public class MainToolBarView extends LinearLayout {

    private static final double LEFT_MARGIN     = -0.45;
    private static final double RIGHT_MARGIN    = 0.45;
    private static final int    SETTINGS_TAB    = 0;
    private static final int    MAIN_TAB        = 1;
    private static final int    CHAT_TAB        = 2;
    private static final int    FULL_VISIBILITY = 255;
    private static final int    NO_VISIBILITY   = 0;
    private static final float  ONE_F           = 1.0f;
    private static final float  TEN_F           = 10.0f;
    private static final int    ZER0            = 0;

    private ReplacableViewPager mPager;
    private int                 mCurrentPage;
    private GestureDetector     mGestureDetector;

    @Bind(R.id.mt_layout)
    LinearLayout mLayout;

    @Bind(R.id.mt_inner_layout)
    RelativeLayout mInnerLayout;

    @Bind(R.id.mt_fake_left_view)
    ImageView mFakeLeftIv;

    @Bind(R.id.mt_settings_icon_view)
    ImageView mSettingsIv;

    @Bind(R.id.mt_settings_active_icon_view)
    ImageView mSettingsActiveIv;

    @Bind(R.id.mt_logo_icon_view)
    ImageView mLogoIv;

    @Bind(R.id.mt_logo_icon_view_small)
    ImageView mLogoSmallIv;

    @Bind(R.id.mt_chat_icon_view)
    ImageView mChatIv;

    @Bind(R.id.mt_chat_active_icon_view)
    ImageView mChatActiveIv;

    @Bind(R.id.mt_fake_right_view)
    ImageView mFakeRightIv;


    public MainToolBarView(Context context) {
        super(context);
        initView();
    }


    public MainToolBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }


    public void showUnreadMessages(boolean unreadMessages) {
        if (unreadMessages) {
            mChatIv.setImageResource(R.drawable.ic_chat_grey_unread);
            mChatActiveIv.setImageResource(R.drawable.ic_chat_green_unread);
        } else {
            mChatIv.setImageResource(R.drawable.ic_chat_grey);
            mChatActiveIv.setImageResource(R.drawable.ic_chat_green);
        }
    }


    public void setViewPager(ReplacableViewPager pager) {
        mCurrentPage = MAIN_TAB;
        mPager = pager;
        mPager.addOnPageChangeListener(pageChangeListener);
    }


    @OnClick(R.id.mt_settings_icon_view)
    void onSettingsClick() {
        mPager.setCurrentItem(SETTINGS_TAB, true);
    }


    @OnClick(R.id.mt_chat_icon_view)
    void onChatClick() {
        mPager.setCurrentItem(CHAT_TAB, true);
    }


    @OnClick(R.id.mt_logo_icon_view_small)
    void onLogoClick() {
        mPager.setCurrentItem(MAIN_TAB, true);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mGestureDetector != null) {
            mGestureDetector.onTouchEvent(event);
            return true;
        }
        return super.onTouchEvent(event);
    }


    private void initView() {
        inflate(getContext(), R.layout.toolbar_main, this);
        ButterKnife.bind(this);
        setMainLayoutParams();
        setInnerLayoutParams();
        setLeftFakeViewParams();
        setRightFakeViewParams();
        setMainIconVisibility(FULL_VISIBILITY, NO_VISIBILITY);
        setSettingsIconVisibility(FULL_VISIBILITY, NO_VISIBILITY);
        setChatIconVisibility(FULL_VISIBILITY, NO_VISIBILITY);
        mGestureDetector = new GestureDetector(getContext(), gestureListener);
    }


    private void setMainIconVisibility(int bigAlpha, int smallAlpha) {
        mLogoIv.getDrawable().setAlpha(bigAlpha);
        mLogoSmallIv.getDrawable().setAlpha(smallAlpha);
    }


    private void setSettingsIconVisibility(int passiveAlpha, int activeAlpha) {
        mSettingsIv.getDrawable().setAlpha(passiveAlpha);
        mSettingsActiveIv.getDrawable().setAlpha(activeAlpha);
    }


    private void setChatIconVisibility(int passiveAlpha, int activeAlpha) {
        mChatIv.getDrawable().setAlpha(passiveAlpha);
        mChatActiveIv.getDrawable().setAlpha(activeAlpha);
    }


    private int calcScreenWidth() {
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }


    private void setMainLayoutParams() {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mLayout.getLayoutParams();
        layoutParams.leftMargin = -getFakeMargin();
        layoutParams.rightMargin = -getFakeMargin();
        mLayout.setLayoutParams(layoutParams);
    }


    private int getFakeMargin() {
        return (int) (RIGHT_MARGIN * calcScreenWidth());
    }


    private void setInnerLayoutParams() {
        ViewGroup.LayoutParams innerLayoutParams = mInnerLayout.getLayoutParams();
        mInnerLayout.setLayoutParams(new LinearLayout.LayoutParams(calcScreenWidth(), innerLayoutParams.height));
    }


    private void setLeftFakeViewParams() {
        ViewGroup.LayoutParams fakeLeftParams = mFakeLeftIv.getLayoutParams();
        mFakeLeftIv.setLayoutParams(new LinearLayout.LayoutParams(getFakeMargin(), fakeLeftParams.height));
    }


    private void setRightFakeViewParams() {
        ViewGroup.LayoutParams fakeRightParams = mFakeRightIv.getLayoutParams();
        mFakeRightIv.setLayoutParams(new LinearLayout.LayoutParams(getFakeMargin(), fakeRightParams.height));
    }


    private ReplacableViewPager.OnPageChangeListener pageChangeListener = new ReplacableViewPager.SimpleOnPageChangeListener() {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            mCurrentPage = position;
            scroll(position, positionOffsetPixels);
            animateIcon(position, positionOffset);
        }
    };


    private void scroll(int position, int positionOffsetPixels) {
        if (position == SETTINGS_TAB) {
            scrollTo((int) (LEFT_MARGIN * (calcScreenWidth() - positionOffsetPixels)), ZER0);
        } else if (position == MAIN_TAB) {
            scrollTo((int) (RIGHT_MARGIN * positionOffsetPixels), ZER0);
        } else if (position == CHAT_TAB) {
            scrollTo((int) (RIGHT_MARGIN * calcScreenWidth()), ZER0);
        }
    }


    private void animateIcon(int position, float positionOffset) {
        int directAnimation = (int) (FULL_VISIBILITY * positionOffset);
        int reverseAnimation = (int) (FULL_VISIBILITY * (ONE_F - positionOffset));

        switch (position) {
            case SETTINGS_TAB:
                setMainIconVisibility(directAnimation, reverseAnimation);
                setSettingsIconVisibility(directAnimation, reverseAnimation);
                setChatIconVisibility(FULL_VISIBILITY, NO_VISIBILITY);
                break;
            case CHAT_TAB:
                setMainIconVisibility(NO_VISIBILITY, FULL_VISIBILITY);
                setSettingsIconVisibility(FULL_VISIBILITY, NO_VISIBILITY);
                setChatIconVisibility(NO_VISIBILITY, FULL_VISIBILITY);
                break;
            case MAIN_TAB:
            default:
                setMainIconVisibility(reverseAnimation, directAnimation);
                setSettingsIconVisibility(FULL_VISIBILITY, NO_VISIBILITY);
                setChatIconVisibility(reverseAnimation, directAnimation);
                break;
        }
    }


    private final GestureDetector.SimpleOnGestureListener gestureListener = new GestureDetector.SimpleOnGestureListener() {

        private boolean mIsInScroll = false;


        @Override
        public boolean onDown(MotionEvent event) {
            mIsInScroll = true;
            return true;
        }


        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            processScrollOrFling(e1, e2);
            return true;
        }


        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            processScrollOrFling(e1, e2);
            return true;
        }


        private void processScrollOrFling(MotionEvent e1, MotionEvent e2) {
            if (mIsInScroll) {
                mIsInScroll = false;
                float distX = e2.getX() - e1.getX();
                if (Math.abs(distX) > TEN_F) {
                    if (distX < ZER0) {
                        scrollRight();
                    } else {
                        scrollLeft();
                    }
                }
            }
        }


        private void scrollLeft() {
            if (mCurrentPage > SETTINGS_TAB) {
                int nextItem = mPager.getCurrentItem() - 1;
                mPager.setCurrentItem(nextItem, true);
                scroll(nextItem, ZER0);
            }
        }


        private void scrollRight() {
            if (mCurrentPage < CHAT_TAB) {
                int nextItem = mPager.getCurrentItem() + 1;
                mPager.setCurrentItem(nextItem, true);
                scroll(nextItem, ZER0);
            }
        }
    };
}
