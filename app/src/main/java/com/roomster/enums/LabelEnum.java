package com.roomster.enums;

public enum LabelEnum {
    Inbox, Archive, Other, Spam
}
