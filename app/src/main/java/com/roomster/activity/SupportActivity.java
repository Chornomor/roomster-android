package com.roomster.activity;


import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.roomster.R;
import com.roomster.adapter.SupportAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.support.SupportEvent;
import com.roomster.manager.CatalogsManager;
import com.roomster.model.CatalogHelpdeskModel;
import com.roomster.rest.model.HelpdeskMessage;
import com.roomster.rest.model.HelpdeskViewModel;
import com.roomster.rest.service.SupportRestService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SupportActivity extends BaseActivity {

    private List<HelpdeskMessage> mMessagesList;
    private SupportAdapter        mSupportAdapter;
    private HelpdeskMessage       mPendingMessage;

    @Bind(R.id.support_toolbar)
    Toolbar toolbar;

    @Bind(R.id.support_messages_rv)
    RecyclerView mSupportRV;

    @Bind(R.id.support_send_message_spinner)
    Spinner mSupportSendMessageSpinner;

    @Bind(R.id.message_edit_text)
    TextView mMessageEditText;

    @Bind(R.id.send_message_btn)
    Button mSendMessageButton;

    @Inject
    SupportRestService mSupportService;

    @Inject
    CatalogsManager catalogsManager;

    @Inject
    Tracker mTracker;

    Map<String,String> titleToKeyMap = new HashMap<>();

    @Override
    protected int getMainContentLayout() {
        return R.layout.activity_support;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        RoomsterApplication.getRoomsterComponent().inject(this);

        initToolbar();
        initSpinner();
        initAdapter();
        loadMessages();
    }


    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName(getString(R.string.screen_name_support));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        // getIntent() should always return the most recent
        setIntent(intent);
    }


    @OnClick(R.id.support_toolbar_back)
    void onToolbarBackClick() {
        onBackPressed();
    }

    @OnClick(R.id.send_message_btn)
    void onSendMessageButtonClicked() {
        String subject = prepareSubject();
        String message = mMessageEditText.getText().toString();
        if (!message.isEmpty()) {
            mMessageEditText.setEnabled(false);
            mSendMessageButton.setEnabled(false);
            addPendingMessage(message);
            mSupportService.sendMessage(subject, message, new RestServiceListener<String>() {

                @Override
                public void onSuccess(String result) {
                    updateScreen(true);
                }


                @Override
                public void onFailure(int code, Throwable throwable, String msg) {
                    Toast.makeText(SupportActivity.this, getString(R.string.detailed_view_send_message_fail), Toast.LENGTH_SHORT)
                      .show();
                    updateScreen(false);
                }
            });
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(SupportEvent.NewMessage event) {
        loadMessages();
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    private void updateScreen(boolean needToCleanMessage) {
        if (needToCleanMessage) {
            mMessageEditText.setText("");
        }
        mMessageEditText.setEnabled(true);
        mSendMessageButton.setEnabled(true);
        loadMessages();
    }


    private String prepareSubject() {
        String selection = (String) mSupportSendMessageSpinner.getSelectedItem();
        return titleToKeyMap.get(selection);
    }


    private void initToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }


    private void initSpinner() {
        // Create an ArrayAdapter using the string array and a default spinner layout
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter
//          .createFromResource(this, R.array.support_subjects, android.R.layout.simple_spinner_item);
        List<CatalogHelpdeskModel> modelList = catalogsManager.getHelpdeskSubjects();
        List<CharSequence> titleList = new ArrayList<>();
        for(CatalogHelpdeskModel model : modelList){
            titleList.add(model.name);
            titleToKeyMap.put(model.name,model.type);
        }
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,titleList);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mSupportSendMessageSpinner.setAdapter(adapter);
    }


    private void initAdapter() {
        mSupportAdapter = new SupportAdapter(mMessagesList);
        mSupportRV.setAdapter(mSupportAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        mSupportRV.setLayoutManager(linearLayoutManager);
    }


    private void updateRecyclerView(List<HelpdeskMessage> messagesList) {
        mMessagesList = messagesList;
        mSupportAdapter.setMessages(mMessagesList);
        if (mMessagesList != null) {
            int lastMessageIndex = mMessagesList.size() - 1;
            try {
                mSupportRV.smoothScrollToPosition(lastMessageIndex);
            } catch (IllegalArgumentException exception) {
                // do nothing
            }
        }
    }


    private void loadMessages() {
        mSupportService.getAllMessages(new RestServiceListener<HelpdeskViewModel>() {

            @Override
            public void onSuccess(HelpdeskViewModel result) {
                updateRecyclerView(result.getHelpdeskMessages());
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
            }
        });
    }


    /**
     * Adds a pending message in the list, which is being send to the server, so the user has indication that the message is
     * being
     * processed.
     */
    private void addPendingMessage(String message) {
        mPendingMessage = new HelpdeskMessage();
        mPendingMessage.setBody(message);
        mPendingMessage.setByUser(true);
        if (mMessagesList == null) {
            mMessagesList = new ArrayList<>();
        }
        mMessagesList.add(mPendingMessage);
        mSupportAdapter.setMessages(mMessagesList);
        mSupportRV.smoothScrollToPosition(mMessagesList.size() - 1);
    }
}
