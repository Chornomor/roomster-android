package com.roomster.adapter;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.rest.model.TransactionsModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;


public class TransactionsListAdapter extends RecyclerView.Adapter<TransactionsListAdapter.ViewHolder> {

    private List<TransactionsModel> mItems;
    private Context                 mContext;

    public TransactionsListAdapter(Context context, List<TransactionsModel> items) {
        mContext = context;
        if (items != null) {
            mItems = items;
        } else {
            mItems = new ArrayList<>();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_transaction, parent, false);

        return new ViewHolder(root);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TransactionsModel transaction = mItems.get(position);
        if (position % 2 == 0) {
            holder.viewWrapper.setBackgroundColor(ContextCompat.getColor(mContext, R.color.transaction_gray_background));
        } else {
            holder.viewWrapper.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
        }
        initType(holder, transaction);
        initDate(holder, transaction);
        initSubscription(holder, transaction);
        initStatus(holder, transaction);
        initPaymentMethod(holder, transaction);
        initAmount(holder, transaction);
    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }


    public void addItems(List<TransactionsModel> newItems) {
        if (newItems != null && !newItems.isEmpty()) {
            mItems.clear();
            mItems.addAll(0, newItems);
            notifyDataSetChanged();
        }
    }

    private void initType(ViewHolder holder, TransactionsModel transaction) {
        if (transaction.getType() != null) {
            holder.transactionTypeTextView.setText(transaction.getType());
            if (transaction.getType().equals(TransactionsModel.TYPE_CANCELLATION) && transaction
              .getByUser() != null && transaction.getByUser()) {
                holder.cancellationByTextView.setVisibility(View.VISIBLE);
            } else {
                holder.cancellationByTextView.setVisibility(View.GONE);
            }
        } else {
            holder.cancellationByTextView.setVisibility(View.GONE);
        }
    }

    private void initDate(ViewHolder holder, TransactionsModel transaction) {
        if (transaction.getDate() != null) {
            DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
            dateFormat.setTimeZone(TimeZone.getDefault());
            String date = dateFormat.format(transaction.getDate());
            holder.dateTextView.setText(date);
        }
    }

    private void initSubscription(ViewHolder holder, TransactionsModel transaction) {
        if (transaction.getProductName() != null) {
            holder.productNameTextView.setText(transaction.getProductName());
        }
    }

    private void initStatus(ViewHolder holder, TransactionsModel transaction) {
        String status = transaction.getStatus();
        if (status != null) {
            holder.statusTextView.setText(status);
            if (isStatusError(status)) {
                holder.statusTextView.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_red_dark));
                //Add comment if there is one
                if (transaction.getComment() != null) {
                    holder.commentTextView.setText(transaction.getComment());
                    holder.commentTextView.setVisibility(View.VISIBLE);
                } else {
                    holder.commentTextView.setVisibility(View.GONE);
                }
            } else {
                holder.statusTextView.setTextColor(ContextCompat.getColor(mContext, R.color.green_roomster_light));
                holder.commentTextView.setVisibility(View.GONE);
            }
        }
    }

    private boolean isStatusError(String status) {
        if (status.equals(TransactionsModel.STATUS_DECLINED) ||
          status.equals(TransactionsModel.STATUS_UNSUPPORTED_CARD) ||
          status.equals(TransactionsModel.STATUS_ERROR)) {
            return true;
        } else {
            return false;
        }
    }

    private void initAmount(ViewHolder holder, TransactionsModel transaction) {
        if (transaction.getAmount() != null) {
            String amount = "";
            if (transaction.getCurrency() != null) {
                amount += transaction.getCurrency();
            }
            amount += Double.toString(transaction.getAmount());
            holder.amountTextView.setText(amount);
        }
    }

    private void initPaymentMethod(ViewHolder holder, TransactionsModel transaction) {
        if (transaction.getPaymentMethod() != null) {
            holder.paymentMethodTextView.setText(transaction.getPaymentMethod());
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        View     viewWrapper;
        TextView transactionTypeTextView;
        TextView cancellationByTextView;
        TextView dateTextView;
        TextView productNameTextView;
        TextView paymentMethodTextView;
        TextView amountTextView;
        TextView statusTextView;
        TextView commentTextView;

        public ViewHolder(View root) {
            super(root);
            viewWrapper = root;
            transactionTypeTextView = (TextView) root.findViewById(R.id.transaction_type);
            cancellationByTextView = (TextView) root.findViewById(R.id.cancellation_by);
            dateTextView = (TextView) root.findViewById(R.id.date);
            productNameTextView = (TextView) root.findViewById(R.id.product_name);
            paymentMethodTextView = (TextView) root.findViewById(R.id.payment_method);
            amountTextView = (TextView) root.findViewById(R.id.price);
            statusTextView = (TextView) root.findViewById(R.id.status);
            commentTextView = (TextView) root.findViewById(R.id.comment);
        }
    }
}
