package com.roomster.constants;


public class Notifications {

    public static final int GCM_USER_NOTIFICATION_ID = 0;

    public static final int GCM_USER_NOTIFICATION_PENDING_INTENT_CODE = 1;

    public static final int GCM_SUPPORT_NOTIFICATION_ID = 2;

    public static final int GCM_SUPPORT_NOTIFICATION_PENDING_INTENT_CODE = 3;
}
