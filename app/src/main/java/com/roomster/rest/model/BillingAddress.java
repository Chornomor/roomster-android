package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


public class BillingAddress {

    /**
     **/
    @SerializedName("address")
    private String address = null;

    /**
     **/
    @SerializedName("city")
    private String city = null;

    /**
     * 2-letter code from /v1/catalogues/countries
     **/
    @SerializedName("state")
    private String state = null;

    /**
     * 2-letter code from /v1/catalogues/states (US) or /v1/catalogues/provinces (CA)
     **/
    @SerializedName("country")
    private String country = null;

    /**
     **/
    @SerializedName("postal_code")
    private String postalCode = null;


    public String getAddress() {
        return address;
    }


    public void setAddress(String address) {
        this.address = address;
    }


    public String getCity() {
        return city;
    }


    public void setCity(String city) {
        this.city = city;
    }


    public String getState() {
        return state;
    }


    public void setState(String state) {
        this.state = state;
    }


    public String getCountry() {
        return country;
    }


    public void setCountry(String country) {
        this.country = country;
    }


    public String getPostalCode() {
        return postalCode;
    }


    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class BillingAddress {\n");

        sb.append("  address: ").append(address).append("\n");
        sb.append("  city: ").append(city).append("\n");
        sb.append("  state: ").append(state).append("\n");
        sb.append("  country: ").append(country).append("\n");
        sb.append("  postalCode: ").append(postalCode).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
