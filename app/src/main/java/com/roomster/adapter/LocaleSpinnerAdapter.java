package com.roomster.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.utils.LocaleUtil;

import java.util.List;

import javax.inject.Inject;


/**
 * Created by "Michael Katkov" on 10/7/2015.
 */
public class LocaleSpinnerAdapter extends ArrayAdapter<String> {

    @Inject
    LayoutInflater mLayoutInflater;


    public LocaleSpinnerAdapter(Context context, List<String> objects) {
        super(context, 0, objects);
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.item_spinner_layout, null);
        }
        TextView textV = (TextView) view.findViewById(R.id.spinnerItem);
//        textV.setText(LocaleUtil.toDisplayFormat(getItem(position)));
        textV.setText(getItem(position));
        return view;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.item_spinner_dropdown_layout, null);
        }
        TextView textV = (TextView) view.findViewById(R.id.spinnerDropDownItem);
//        textV.setText(LocaleUtil.toDisplayFormat(getItem(position)));
        textV.setText(getItem(position));
        return view;
    }
}
