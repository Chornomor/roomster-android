package com.roomster.activity;


import com.google.android.gms.analytics.HitBuilders;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.application.ApplicationEvent;
import com.roomster.rest.model.SubscriptionGetModel;
import com.roomster.rest.service.SubscriptionRestService;
import com.roomster.utils.DateUtil;

import java.util.Date;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SubscriptionActivity extends BaseActivity {

    @Bind(R.id.subscription_status)
    TextView mSubscriptionStatus;

    @Bind(R.id.subscription_full_access_until)
    TextView mFullAccessUntil;

    @Bind(R.id.subscription_next_billing)
    TextView mNextBilling;

    @Bind(R.id.subscription_cancel_button)
    Button mCancel;

    @Bind(R.id.tv_title)
    TextView mToolbarTitle;

    @Inject
    SubscriptionRestService mSubscriptionsRestService;


    @Override
    protected int getMainContentLayout() {
        return R.layout.activity_subscription;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        ButterKnife.bind(this);
        initToolbar();
    }


    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName(getString(R.string.screen_name_subscription));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        refreshInfo();
    }


    @OnClick(R.id.subscription_cancel_button)
    public void onCancelButtonClicked() {
        mCancel.setEnabled(false);
        mEventBus.post(new ApplicationEvent.ShowLoading());
        mSubscriptionsRestService.cancelSubscription(new RestServiceListener<Object>() {

            @Override
            public void onSuccess(Object result) {
                refreshInfo();
                mEventBus.post(new ApplicationEvent.HideLoading());
                mCancel.setEnabled(true);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                mEventBus.post(new ApplicationEvent.HideLoading());
                Toast.makeText(SubscriptionActivity.this, getString(R.string.subscription_toast_cant_cancel_now),
                  Toast.LENGTH_SHORT).show();
                mCancel.setEnabled(true);
            }
        });
    }


    @OnClick(R.id.iv_back)
    public void onToolbarBackClicked() {
        onBackPressed();
    }


    private void initToolbar() {
        mToolbarTitle.setText(R.string.subscription_title);
    }


    private void refreshInfo() {
        mSubscriptionsRestService.getSubscription(new RestServiceListener<SubscriptionGetModel>() {

            @Override
            public void onSuccess(SubscriptionGetModel result) {
                initView(result);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                Toast.makeText(SubscriptionActivity.this, getString(R.string.subscription_toast_cant_obtain_info_now),
                  Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }


    private void initView(SubscriptionGetModel model) {
        initStatus(model.getActive());
        initFullAccess(model.getFullAccessExpires());
        initNextBilling(model.getNextBillingDate());
    }


    private void initNextBilling(Date nextBillingDate) {
        if (nextBillingDate != null) {
            mNextBilling.setText(DateUtil.formatDate(nextBillingDate));
        } else {
            mNextBilling.setText(R.string.subscription_next_billing_unknown);
        }
        mNextBilling.setVisibility(View.VISIBLE);
    }


    private void initFullAccess(Date fullAccessExpires) {
        if (fullAccessExpires != null) {
            mFullAccessUntil.setText(DateUtil.formatDate(fullAccessExpires));
        } else {
            mFullAccessUntil.setText(R.string.subscription_full_access_until_unknown);
        }
        mFullAccessUntil.setVisibility(View.VISIBLE);
    }


    private void initStatus(Boolean active) {
        if (active != null && active) {
            mSubscriptionStatus.setText(R.string.subscription_status_active);
            mSubscriptionStatus
              .setTextColor(ContextCompat.getColor(SubscriptionActivity.this, R.color.green_roomster_text_light));
            mCancel.setVisibility(View.VISIBLE);
        } else {
            mSubscriptionStatus.setText(R.string.subscription_status_inactive);
            mSubscriptionStatus.setTextColor(ContextCompat.getColor(SubscriptionActivity.this, R.color.gray_text));
            mCancel.setVisibility(View.INVISIBLE);
        }
        mSubscriptionStatus.setVisibility(View.VISIBLE);
    }
}