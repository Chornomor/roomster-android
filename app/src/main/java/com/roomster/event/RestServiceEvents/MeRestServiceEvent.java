package com.roomster.event.RestServiceEvents;


/**
 * Created by "Michael Katkov" on 10/11/2015.
 */
public class MeRestServiceEvent {

    public static class GetMeSuccess extends MeRestServiceEvent {

    }

    public static class GetMeFailure extends MeRestServiceEvent {

        public boolean needReLogin;
    }
}
