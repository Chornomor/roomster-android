package com.roomster.rest.service;


import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.RestCode;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.manager.NetworkManager;
import com.roomster.rest.api.TokenApi;
import com.roomster.rest.model.TokenModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import javax.inject.Inject;
import java.io.IOException;


public class TokenRestService {

    @Inject
    TokenApi mTokenApi;

    @Inject
    NetworkManager mNetworkManager;


    public TokenRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void requestToken(String externalProvider, String externalAccessToken, String externalAccessTokenSecret,
            final RestServiceListener<TokenModel> listener) {

        if (mNetworkManager.hasNetworkAccess()) {
            Call<TokenModel> call = mTokenApi
                    .tokenGet(externalAccessToken, externalAccessTokenSecret, externalProvider);
            call.enqueue(new Callback<TokenModel>() {

                @Override
                public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {
                    switch (response.code()) {
                    case ResponseCode.OK:
                        listener.onSuccess(response.body());
                        break;
                    case ResponseCode.USER_NOT_FOUND:
                    case ResponseCode.BAD_REQUEST:
                    case ResponseCode.SERVER_ERROR:
                    default:

                        String errorMessage = RoomsterApplication.context.getString(R.string.token_cant_obtain_access);
                        if (response.errorBody() != null) {
                            try {
                                errorMessage = errorMessage + '\n' + response.errorBody().string();
                            } catch (IOException exception) {
                                // nothing to do
                            }
                        }
                        listener.onFailure(response.code(), null, errorMessage);
                    }
                }


                @Override
                public void onFailure(Call<TokenModel> call, Throwable t) {
                    listener.onFailure(RestCode.REQUEST_NOT_MADE, t, RoomsterApplication.context.getString(R.string.token_cant_make_request_call));
                }
            });
        } else {
            listener.onFailure(-1, null, RoomsterApplication.context.getString(R.string.no_network_connection_message));
        }
    }
}