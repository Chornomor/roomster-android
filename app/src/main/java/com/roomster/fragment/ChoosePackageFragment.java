package com.roomster.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.roomster.R;
import com.roomster.activity.UpgradeAccountActivity;
import com.roomster.adapter.PackagesAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.upgradeaccount.UpgradeAccountEvent;
import com.roomster.model.ProductItem;
import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;
import java.util.List;


public class ChoosePackageFragment extends Fragment {

    @Bind(R.id.choose_package_recycler_view)
    RecyclerView mRecyclerView;

    @Inject
    EventBus mEventBus;

    private List<ProductItem> mPackages;


    public static ChoosePackageFragment newInstance(@Nullable List<ProductItem> packages) {
        ChoosePackageFragment fragment = new ChoosePackageFragment();
        fragment.mPackages = packages;
        return fragment;
    }


    @Override
    public void onResume() {
        super.onResume();
        Activity activity = getActivity();
        if (activity != null && activity instanceof UpgradeAccountActivity) {
            ((UpgradeAccountActivity) activity).setActionBarTitle(R.string.upgrade_account_choose_package_title);
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choose_package, container, false);
        ButterKnife.bind(this, view);
        RoomsterApplication.getRoomsterComponent().inject(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        PackagesAdapter adapter = new PackagesAdapter(mPackages);
        adapter.setOnItemClickListener(new PackagesAdapter.OnItemClickListener() {

            @Override
            public void onClick(ProductItem productItem) {
                mEventBus.post(new UpgradeAccountEvent.PackageSelected(productItem));
            }
        });

        mRecyclerView.setAdapter(adapter);

        return view;
    }
}
