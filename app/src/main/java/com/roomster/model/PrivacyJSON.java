package com.roomster.model;


/**
 * Created by emil on 9/15/15. Email: heitara@gmail.com
 * <p/>
 * Project: Roomster-Android
 * <p/>
 * Copyright 2015 (c)
 */
public class PrivacyJSON {

    public String privacy;
    public String terms;
}
