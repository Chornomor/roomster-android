package com.roomster.rest.api;


import com.roomster.rest.model.PackageViewModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;


public interface PackagesApi {

    /**
     * Get available bililing packages for the user
     *
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @param currency
     *   currency (USD, EUR, GBP etc.)
     * @param locale
     *   locale (en-US, ru-RU, ja-JP etc.)
     * @return Call<List<PackageViewModel>>
     */

    @GET("Packages")
    Call<List<PackageViewModel>> packagesGet(@Header("Authorization") String authorization, @Header("Currency") String currency,
                                             @Header("Locale") String locale);
}
