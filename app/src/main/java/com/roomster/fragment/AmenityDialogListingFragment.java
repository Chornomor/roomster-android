package com.roomster.fragment;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.roomster.R;
import com.roomster.adapter.AmenitiesAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.fragment.AmenityDialogListingFragmentEvent;
import com.roomster.manager.CatalogsManager;
import com.roomster.manager.MeManager;
import com.roomster.model.CatalogValueModel;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;


/**
 * Created by "Michael Katkov" on 12/27/2015.
 */
public class AmenityDialogListingFragment extends DialogFragment {

    public static final String TAG                      = "AmenityDialogListingFragment";
    public static final String DIALOG_TYPE              = "dialog_type";
    public static final int    ROOM_TYPE                = 1;
    public static final int    APARTMENT_TYPE           = 2;
    public static final float  TOUCHED                  = 1f;
    public static final float  NORMAL                   = 0.4f;
    public static final int    AMENITY_IMAGE_VIEW_INDEX = 0;
    public static final int    AMENITY_IMAGE_TEXT_INDEX = 1;

    private int mDialogType = ROOM_TYPE;

    @Bind(R.id.amenity_grid)
    GridView mAmenitiesGrid;

    @Bind(R.id.amenities_dialog_layout)
    FrameLayout mAmenitiesLayout;

    @Inject
    CatalogsManager mCatalogsManager;

    @Inject
    MeManager mMeManager;

    @Inject
    EventBus mEventBus;


    public static AmenityDialogListingFragment newInstance(int type) {
        AmenityDialogListingFragment fragment = new AmenityDialogListingFragment();
        Bundle args = new Bundle();
        args.putInt(DIALOG_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mDialogType = getArguments().getInt(DIALOG_TYPE);
        }
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_amenity, container, false);
        ButterKnife.bind(this, view);
        setAdapter();
        return view;
    }


    private void setAdapter() {
        AmenitiesAdapter amenitiesAdapter = null;
        switch (mDialogType) {
            case ROOM_TYPE:
                amenitiesAdapter = new AmenitiesAdapter(getActivity(), mCatalogsManager.getRoomAmenitiesList(),
                  mMeManager.getCurrentAmenities());
                break;
            case APARTMENT_TYPE:
                amenitiesAdapter = new AmenitiesAdapter(getActivity(), mCatalogsManager.getApartmentAmenitiesList(),
                  mMeManager.getCurrentAmenities());
                break;
        }

        mAmenitiesGrid.setAdapter(amenitiesAdapter);
    }


    @OnItemClick(R.id.amenity_grid)
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        CatalogValueModel amenityModel = (CatalogValueModel) parent.getItemAtPosition(position);
        makeClick(view, amenityModel);
    }


    private void makeClick(View view, CatalogValueModel catalogModel) {
        LinearLayout layout = (LinearLayout) view;
        View amenityImage = layout.getChildAt(AMENITY_IMAGE_VIEW_INDEX);
        View amenityText = layout.getChildAt(AMENITY_IMAGE_TEXT_INDEX);
        if (amenityImage.getAlpha() == TOUCHED) {
            amenityImage.setAlpha(NORMAL);
            amenityText.setAlpha(NORMAL);
            sendRemoveAmenityEvent(catalogModel);
        } else {
            amenityImage.setAlpha(TOUCHED);
            amenityText.setAlpha(TOUCHED);
            sendAddAmenityEvent(catalogModel);
        }
    }


    private void sendRemoveAmenityEvent(CatalogValueModel catalogModel) {
        AmenityDialogListingFragmentEvent.RemoveAmenityEvent remove = new AmenityDialogListingFragmentEvent.RemoveAmenityEvent();
        remove.amenity = catalogModel;
        remove.amenityType = mDialogType;
        mEventBus.post(remove);
    }


    private void sendAddAmenityEvent(CatalogValueModel catalogModel) {
        AmenityDialogListingFragmentEvent.AddAmenityEvent add = new AmenityDialogListingFragmentEvent.AddAmenityEvent();
        add.amenity = catalogModel;
        add.amenityType = mDialogType;
        mEventBus.post(add);
    }


    @OnClick(R.id.amenities_dialog_layout)
    public void OnLayoutClick() {
        dismiss();
    }
}
