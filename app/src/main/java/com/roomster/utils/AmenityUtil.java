package com.roomster.utils;


import com.roomster.R;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by "Michael Katkov" on 11/10/2015.
 */
public class AmenityUtil {

    public static final String AMENITY_ADD = "+Add";
    public static final int AMENITY_ADD_ID = -1;

    private static Map<Integer, Integer> amenitiesMap = new HashMap<>();


    public static void loadAmenities() {
//        amenitiesMap.put("High-rise", R.drawable.ic_amenity_high_rise);
        amenitiesMap.put(3, R.drawable.ic_amenity_high_rise);
//        amenitiesMap.put("Low-rise", R.drawable.ic_amenity_low_rise);
        amenitiesMap.put(4, R.drawable.ic_amenity_low_rise);
//        amenitiesMap.put("Disability Access", R.drawable.ic_amenity_disability);
        amenitiesMap.put(5, R.drawable.ic_amenity_disability);
//        amenitiesMap.put("Doorman", R.drawable.ic_amenity_doorman);
        amenitiesMap.put(7, R.drawable.ic_amenity_doorman);
//        amenitiesMap.put("Elevator", R.drawable.ic_amenity_elevator);
        amenitiesMap.put(8, R.drawable.ic_amenity_elevator);
//        amenitiesMap.put("Walkup", R.drawable.ic_amenity_walkup);
        amenitiesMap.put(9, R.drawable.ic_amenity_walkup);
//        amenitiesMap.put("Health Club", R.drawable.ic_amenity_health_club);
        amenitiesMap.put(10, R.drawable.ic_amenity_health_club);
//        amenitiesMap.put("Laundromat", R.drawable.ic_amenity_laundry);
        amenitiesMap.put(12, R.drawable.ic_amenity_laundry);
//        amenitiesMap.put("Covered Parking", R.drawable.ic_amenity_covered_parking);
        amenitiesMap.put(13, R.drawable.ic_amenity_covered_parking);
//        amenitiesMap.put("Garage", R.drawable.ic_amenity_garage);
        amenitiesMap.put(14, R.drawable.ic_amenity_garage);
//        amenitiesMap.put("Parking Lot", R.drawable.ic_amenity_parking_lot);
        amenitiesMap.put(15, R.drawable.ic_amenity_parking_lot);
//        amenitiesMap.put("Street Parking", R.drawable.ic_amenity_street_parking);
        amenitiesMap.put(17, R.drawable.ic_amenity_street_parking);
//        amenitiesMap.put("Near Bus Stop", R.drawable.ic_amenity_bus_stop);
        amenitiesMap.put(18, R.drawable.ic_amenity_bus_stop);
//        amenitiesMap.put("Near Subway", R.drawable.ic_amenity_subway);
        amenitiesMap.put(19, R.drawable.ic_amenity_subway);
//        amenitiesMap.put("Electronic Security", R.drawable.ic_amenity_electronic_security);
        amenitiesMap.put(21, R.drawable.ic_amenity_electronic_security);
//        amenitiesMap.put("Security", R.drawable.ic_amenity_security);
        amenitiesMap.put(22, R.drawable.ic_amenity_security);
//        amenitiesMap.put("Swimming Pool", R.drawable.ic_amenity_swimming_pool);
        amenitiesMap.put(23, R.drawable.ic_amenity_swimming_pool);
//        amenitiesMap.put("Internet", R.drawable.ic_amenity_internet);
        amenitiesMap.put(24, R.drawable.ic_amenity_internet);
//        amenitiesMap.put("Wireless Internet", R.drawable.ic_amenity_wireless);
        amenitiesMap.put(25, R.drawable.ic_amenity_wireless);
//        amenitiesMap.put("Air Conditioning", R.drawable.ic_amenity_air_conditioning);
        amenitiesMap.put(191, R.drawable.ic_amenity_air_conditioning);
//        amenitiesMap.put("Deck or Patio", R.drawable.ic_amenity_deck_or_patio);
        amenitiesMap.put(192, R.drawable.ic_amenity_deck_or_patio);
//        amenitiesMap.put("Wood Floors", R.drawable.ic_amenity_wood_floors);
        amenitiesMap.put(193, R.drawable.ic_amenity_wood_floors);
//        amenitiesMap.put("Yard", R.drawable.ic_amenity_yard);
        amenitiesMap.put(194, R.drawable.ic_amenity_yard);
//        amenitiesMap.put("Carpet", R.drawable.ic_amenity_carpet);
        amenitiesMap.put(195, R.drawable.ic_amenity_carpet);
//        amenitiesMap.put("Storage", R.drawable.ic_amenity_storage);
        amenitiesMap.put(196, R.drawable.ic_amenity_storage);
//        amenitiesMap.put("Gym", R.drawable.ic_amenity_health_club);
        amenitiesMap.put(197, R.drawable.ic_amenity_health_club);
//        amenitiesMap.put("Parking", R.drawable.ic_amenity_parking_lot);
        amenitiesMap.put(198, R.drawable.ic_amenity_parking_lot);
//        amenitiesMap.put("Elevator", R.drawable.ic_amenity_elevator);
        amenitiesMap.put(199, R.drawable.ic_amenity_elevator);
//        amenitiesMap.put("Fireplace", R.drawable.ic_amenity_fireplace);
        amenitiesMap.put(200, R.drawable.ic_amenity_fireplace);
//        amenitiesMap.put("Laundry", R.drawable.ic_amenity_laundry);
        amenitiesMap.put(201, R.drawable.ic_amenity_laundry);
//        amenitiesMap.put("Tennis", R.drawable.ic_amenity_tennis);
        amenitiesMap.put(202, R.drawable.ic_amenity_tennis);
//        amenitiesMap.put("Jacuzzi", R.drawable.ic_amenity_jacuzzi);
        amenitiesMap.put(203, R.drawable.ic_amenity_jacuzzi);
//        amenitiesMap.put("Dishwasher", R.drawable.ic_amenity_dishwasher);
        amenitiesMap.put(204, R.drawable.ic_amenity_dishwasher);
//        amenitiesMap.put("Pool", R.drawable.ic_amenity_swimming_pool);
        amenitiesMap.put(205, R.drawable.ic_amenity_swimming_pool);
//        amenitiesMap.put("Private Bathroom", R.drawable.ic_amenity_private_bathroom);
        amenitiesMap.put(206, R.drawable.ic_amenity_private_bathroom);
//        amenitiesMap.put("Phone Jack", R.drawable.ic_amenity_phone_jack);
        amenitiesMap.put(207, R.drawable.ic_amenity_phone_jack);
//        amenitiesMap.put("Private Entrance", R.drawable.ic_amenity_private_entrance);
        amenitiesMap.put(208, R.drawable.ic_amenity_private_entrance);
//        amenitiesMap.put("Cable TV Jack", R.drawable.ic_amenity_tv_jack);
        amenitiesMap.put(209, R.drawable.ic_amenity_tv_jack);
//        amenitiesMap.put("Private Closet", R.drawable.ic_amenity_private_closet);
        amenitiesMap.put(210, R.drawable.ic_amenity_private_closet);
//        amenitiesMap.put("Internet", R.drawable.ic_amenity_internet);
        amenitiesMap.put(211, R.drawable.ic_amenity_internet);
//        amenitiesMap.put("Wireless Internet", R.drawable.ic_amenity_wireless);
        amenitiesMap.put(212, R.drawable.ic_amenity_wireless);
        amenitiesMap.put(AMENITY_ADD_ID, R.drawable.ic_amenity_add);
    }


    public static Integer getAmenityImageByName(int id) {
        if (amenitiesMap.isEmpty()) {
            loadAmenities();
        }
        return amenitiesMap.get(id);
    }
}
