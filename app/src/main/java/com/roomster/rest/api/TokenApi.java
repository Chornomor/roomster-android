package com.roomster.rest.api;


import com.roomster.rest.model.TokenModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface TokenApi {

    /**
     * Authenticate with an external provider (facebook, twitter, google) Pass access_token and access_token_secret
     * (twitter only) to get Roomster Bearer access_token
     */

    @GET("Token")
    Call<TokenModel> tokenGet(@Query("access_token") String accessToken, @Query("access_token_secret") String accessTokenSecret,
                              @Query("provider") String provider);
}
