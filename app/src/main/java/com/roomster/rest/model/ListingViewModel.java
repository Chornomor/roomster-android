package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import com.roomster.enums.ServiceTypeEnum;


public class ListingViewModel {

    @SerializedName("service_type")
    private ServiceTypeEnum serviceType = null;


    @SerializedName("geo_location")
    private GeoLocation geoLocation = null;

    @SerializedName("calendar")
    private Calendar calendar = null;

    @SerializedName("rates")
    private Rates rates = null;

    @SerializedName("need_apartment")
    private NeedApartment needApartment = null;


    @SerializedName("have_apartment")
    private HaveApartment haveApartment = null;


    public ServiceTypeEnum getServiceType() {
        return serviceType;
    }


    public void setServiceType(ServiceTypeEnum serviceType) {
        this.serviceType = serviceType;
    }


    public GeoLocation getGeoLocation() {
        return geoLocation;
    }


    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }


    public Calendar getCalendar() {
        return calendar;
    }


    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }


    public Rates getRates() {
        return rates;
    }


    public void setRates(Rates rates) {
        this.rates = rates;
    }


    public NeedApartment getNeedApartment() {
        return needApartment;
    }


    public void setNeedApartment(NeedApartment needApartment) {
        this.needApartment = needApartment;
    }


    public HaveApartment getHaveApartment() {
        return haveApartment;
    }


    public void setHaveApartment(HaveApartment haveApartment) {
        this.haveApartment = haveApartment;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ListingViewModel {\n");

        sb.append("  serviceType: ").append(serviceType).append("\n");
        sb.append("  geoLocation: ").append(geoLocation).append("\n");
        sb.append("  calendar: ").append(calendar).append("\n");
        sb.append("  rates: ").append(rates).append("\n");
        sb.append("  needApartment: ").append(needApartment).append("\n");
        sb.append("  haveApartment: ").append(haveApartment).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}