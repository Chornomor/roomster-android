package com.roomster.activity;


import com.google.android.gms.analytics.HitBuilders;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.IntentExtras;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.fragment.FragmentRequest;
import com.roomster.fragment.DetailedListingListFragment;
import com.roomster.fragment.ListingListViewFragment;
import com.roomster.fragment.UserListingsListFragment;
import com.roomster.manager.UsersManager;
import com.roomster.rest.model.ResultItemModel;
import com.roomster.rest.model.UserGetViewModel;
import com.roomster.rest.service.UsersRestService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;


/**
 * Created by Vasko on 16-Apr-16.
 */
public class UserListingsActivity extends BaseToolbarActivity {

    private static final String DETAIL_FRAGMENT_NAME = "DETAIL_FRAGMENT_NAME";
    private static final String LIST_FRAGMENT_NAME = "LIST_FRAGMENT_NAME";

    private Long mUserId;

    private UserGetViewModel mUser;

    @Inject
    Context mContext;

    @Inject
    UsersRestService mUsersRestService;

    @Inject
    UsersManager mUsersManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);

        handleIntent();
    }


    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName(getString(R.string.screen_name_user_profile));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    private void handleIntent() {
        mUserId = getIntent().getLongExtra(IntentExtras.OTHER_USER_ID_EXTRA, -1);

        mUser = mUsersManager.getUserById(mUserId);
        if (mUser != null) {
            setUserListingsFragment();
        } else {
            mUsersRestService.getUserById(mUserId, mUserModelListener);
        }
    }


    private void setUserListingsFragment() {
        UserListingsListFragment fragment = UserListingsListFragment.newInstance(mUser);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN)
        if (!isFinishing() && !isDestroyed()) {
            fragment.setOnListingClickListener(new ListingListViewFragment.OnListingClickListener() {

                @Override
                public void onClick(int index, List<ResultItemModel> models) {
                    setDetailedListFragment(index, models);
                }
            });
            setContent(fragment, false, LIST_FRAGMENT_NAME);
        }else {
            if (!isFinishing()) {
                fragment.setOnListingClickListener(new ListingListViewFragment.OnListingClickListener() {

                    @Override
                    public void onClick(int index, List<ResultItemModel> models) {
                        setDetailedListFragment(index, models);
                    }
                });
                setContent(fragment, false, LIST_FRAGMENT_NAME);
        }
        }
    }


    private void setDetailedListFragment(int initialIndex, List<ResultItemModel> items) {
        setContent(DetailedListingListFragment.newInstance(items, initialIndex), true, DETAIL_FRAGMENT_NAME);
    }


    private void setContent(Fragment fragment, boolean addToBackStack, String backStackName) {
        if (addToBackStack) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fl_content, fragment).addToBackStack(backStackName)
                    .commit();
        } else {
            super.setContent(fragment);
        }
    }


    private void haveUser(UserGetViewModel user) {
        mUser = user;
        mUsersManager.addUserModel(user);
        setUserListingsFragment();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(FragmentRequest.SetTitleRequest event) {
        mTitleTv.setText(event.title);
        mCounterTv.setText(event.subtitle);
    }


    private RestServiceListener mUserModelListener = new RestServiceListener() {

        @Override
        public void onSuccess(Object result) {
            haveUser((UserGetViewModel) result);
        }


        @Override
        public void onFailure(int code, Throwable throwable, String msg) {
            Toast.makeText(mContext, getString(R.string.unable_to_get_user_info), Toast.LENGTH_SHORT).show();
            finish();
        }
    };
}
