package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


public class PackageViewModel {

    /**
     **/
    @SerializedName("requested_price")
    private Double requestedPrice = null;

    /**
     **/
    @SerializedName("requested_price_renew")
    private Double requestedPriceRenew = null;

    /**
     **/
    @SerializedName("requested_currency")
    private String requestedCurrency = null;

    /**
     **/
    @SerializedName("id")
    private Integer id = null;

    /**
     **/
    @SerializedName("name")
    private String name = null;

    /**
     **/
    @SerializedName("days")
    private Integer days = null;

    /**
     **/
    @SerializedName("days_renew")
    private Integer daysRenew = null;

    /**
     **/
    @SerializedName("price")
    private Double price = null;

    /**
     **/
    @SerializedName("price_renew")
    private Double priceRenew = null;

    /**
     **/
    @SerializedName("renewals_number")
    private Integer renewalsNumber = null;

    /**
     **/
    @SerializedName("currency")
    private String currency = null;

    /**
     **/
    @SerializedName("is_renewal")
    private Boolean isRenewal = null;


    public Double getRequestedPrice() {
        return requestedPrice;
    }


    public void setRequestedPrice(Double requestedPrice) {
        this.requestedPrice = requestedPrice;
    }


    public Double getRequestedPriceRenew() {
        return requestedPriceRenew;
    }


    public void setRequestedPriceRenew(Double requestedPriceRenew) {
        this.requestedPriceRenew = requestedPriceRenew;
    }


    public String getRequestedCurrency() {
        return requestedCurrency;
    }


    public void setRequestedCurrency(String requestedCurrency) {
        this.requestedCurrency = requestedCurrency;
    }


    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public Integer getDays() {
        return days;
    }


    public void setDays(Integer days) {
        this.days = days;
    }


    public Integer getDaysRenew() {
        return daysRenew;
    }


    public void setDaysRenew(Integer daysRenew) {
        this.daysRenew = daysRenew;
    }


    public Double getPrice() {
        return price;
    }


    public void setPrice(Double price) {
        this.price = price;
    }


    public Double getPriceRenew() {
        return priceRenew;
    }


    public void setPriceRenew(Double priceRenew) {
        this.priceRenew = priceRenew;
    }


    public Integer getRenewalsNumber() {
        return renewalsNumber;
    }


    public void setRenewalsNumber(Integer renewalsNumber) {
        this.renewalsNumber = renewalsNumber;
    }


    public String getCurrency() {
        return currency;
    }


    public void setCurrency(String currency) {
        this.currency = currency;
    }


    public Boolean getIsRenewal() {
        return isRenewal;
    }


    public void setIsRenewal(Boolean isRenewal) {
        this.isRenewal = isRenewal;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class PackageViewModel {\n");

        sb.append("  requestedPrice: ").append(requestedPrice).append("\n");
        sb.append("  requestedPriceRenew: ").append(requestedPriceRenew).append("\n");
        sb.append("  requestedCurrency: ").append(requestedCurrency).append("\n");
        sb.append("  id: ").append(id).append("\n");
        sb.append("  name: ").append(name).append("\n");
        sb.append("  days: ").append(days).append("\n");
        sb.append("  daysRenew: ").append(daysRenew).append("\n");
        sb.append("  price: ").append(price).append("\n");
        sb.append("  priceRenew: ").append(priceRenew).append("\n");
        sb.append("  renewalsNumber: ").append(renewalsNumber).append("\n");
        sb.append("  currency: ").append(currency).append("\n");
        sb.append("  isRenewal: ").append(isRenewal).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
