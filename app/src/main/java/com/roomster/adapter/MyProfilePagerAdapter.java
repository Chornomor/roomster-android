package com.roomster.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.roomster.R;
import com.roomster.fragment.ImageFragment;
import com.roomster.rest.model.UserGetViewModel;


/**
 * Created by "Michael Katkov" on 10/13/2015.
 */
public class MyProfilePagerAdapter extends BasePageAdapter {

    public static final int EDIT_INDICATOR_COUNT = 1;

    private UserGetViewModel mGetViewModel;

    private int mSelectedIndicators[];
    private int mDeselectedIndicators[];


    public MyProfilePagerAdapter(FragmentManager fragmentManager, UserGetViewModel getViewModel) {
        super(fragmentManager);
        mGetViewModel = getViewModel;
        int indicatorsCount = mGetViewModel.getImages().size() + EDIT_INDICATOR_COUNT;
        mSelectedIndicators = new int[indicatorsCount];
        mDeselectedIndicators = new int[indicatorsCount];
        fillSelectedIndicators();
        fillDeselectedIndicators();
    }


    @Override
    public int getCount() {
        return mGetViewModel.getImages().size();
    }


    @Override
    public Fragment getItem(int position) {
        return ImageFragment.newInstance(mGetViewModel.getImages().get(position));
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }


    private void fillSelectedIndicators() {
        for (int i = 0; i < mGetViewModel.getImages().size(); i++) {
            mSelectedIndicators[i] = R.drawable.ic_bubble_selected;
        }
        mSelectedIndicators[mSelectedIndicators.length - 1] = R.drawable.ic_info_bubble_selected;
    }


    private void fillDeselectedIndicators() {
        for (int i = 0; i < mGetViewModel.getImages().size(); i++) {
            mDeselectedIndicators[i] = R.drawable.ic_bubble_deselected;
        }
        mDeselectedIndicators[mDeselectedIndicators.length - 1] = R.drawable.ic_info_bubble_deselected;
    }


    @Override
    public int getSelectedIndicator(int position) {
        return mSelectedIndicators[position];
    }


    @Override
    public int getDiselectedIndicator(int position) {
        return mDeselectedIndicators[position];
    }
}


