package com.roomster.activity;


import com.google.android.gms.analytics.HitBuilders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.fragment.SettingsFragment;
import com.roomster.manager.SettingsManager;
import com.roomster.rest.service.NotificationsRestService;

import javax.inject.Inject;


/**
 * Created by "Michael Katkov" on 10/19/2015.
 */
public class AppSettingsActivity extends BaseToolbarActivity {

    @Inject
    Context mContext;

    @Inject
    NotificationsRestService mNotificationsRestService;

    @Inject
    SettingsManager mSettingsManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContent(new SettingsFragment());
        RoomsterApplication.getRoomsterComponent().inject(this);
        mTitleTv.setText(getString(R.string.settings_screen_title));
    }


    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName(getString(R.string.screen_name_app_settings));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    @Override
    protected void onPause() {
        super.onPause();
        mNotificationsRestService.saveNotificationsSettings(null);
    }

    @Override
    void onToolbarBackClick() {
        if (mSettingsManager.isLocaleChange()) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            super.onToolbarBackClick();
        }
    }
}