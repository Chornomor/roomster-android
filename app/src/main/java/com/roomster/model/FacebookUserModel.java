package com.roomster.model;


import java.util.List;


/**
 * Created by "Michael Katkov" on 10/10/2015.
 */
public class FacebookUserModel {

    private String          id;
    private String          name;
    private String          gender;
    private String          email;
    private String          birthday;
    private ItemModel       hometown;
    private List<ItemModel> languages;
    private Picture         picture;


    public Picture getPicture() {
        return picture;
    }


    public void setPicture(Picture picture) {
        this.picture = picture;
    }


    public ItemModel getHometown() {
        return hometown;
    }


    public void setHometown(ItemModel hometown) {
        this.hometown = hometown;
    }


    public List<ItemModel> getLanguages() {
        return languages;
    }


    public void setLanguages(List<ItemModel> languages) {
        this.languages = languages;
    }


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getGender() {
        return gender;
    }


    public void setGender(String gender) {
        this.gender = gender;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public String getBirthday() {
        return birthday;
    }


    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }


    public static class ItemModel {

        private String id;

        private String name;


        public String getId() {
            return id;
        }


        public void setId(String id) {
            this.id = id;
        }


        public String getName() {
            return name;
        }


        public void setName(String name) {
            this.name = name;
        }
    }


    public static class Picture {

        private Data data;


        public Data getData() {
            return data;
        }


        public void setData(Data data) {
            this.data = data;
        }


        public static class Data {

            private boolean is_silhouette;

            private String url;


            public boolean is_silhouette() {
                return is_silhouette;
            }


            public void setIs_silhouette(boolean is_silhouette) {
                this.is_silhouette = is_silhouette;
            }


            public String getUrl() {
                return url;
            }


            public void setUrl(String url) {
                this.url = url;
            }
        }
    }
}
