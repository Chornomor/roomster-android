package com.roomster.manager;


import com.roomster.application.RoomsterApplication;
import com.roomster.model.SearchCriteria;
import com.roomster.rest.model.ListingGetViewModel;
import com.roomster.rest.model.ResultItemModel;
import com.roomster.rest.model.ResultModel;
import com.roomster.rest.model.UserGetViewModel;
import com.roomster.rest.service.SearchRestService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class SearchManager {

    private ResultModel                 mResultModel;
    private Collection<ResultItemModel> mMapViewItems;
    private int                         mPageCount;
    private int                         mLastPageSize;
    private int                         mLastViewedPosition;
    private String                      mResultsType;
    private SearchCriteria              mSearchCriteria;
    public ArrayList<Long> listUnbookmarkedId = new ArrayList<>();


    public SearchManager() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public ResultModel getResultModel() {
        return mResultModel;
    }


    public void clearResultModel() {
        mResultModel = null;
        mPageCount = 0;
        mLastPageSize = 0;
        setLastViewedPosition(0);
        mSearchCriteria = null;
    }


    public void setResultModel(ResultModel resultModel, SearchCriteria criteria) {
        if(resultModel == null)
            return;
        List<ResultItemModel> resultItemModels = getItemsWithLocation(resultModel);
        prepareIfBookmarked(resultItemModels);
        resultModel.setItems(resultItemModels);
        mResultModel = resultModel;
        mPageCount = 1;
        mLastPageSize = resultItemModels.size();
        mSearchCriteria = criteria;
    }

    public ResultItemModel removeItemById(long id){
        if(mResultModel == null || mResultModel.getItems() == null){
            return null;
        }
        ResultItemModel target = null;
        int i = 0;
        for(ResultItemModel model : mResultModel.getItems()){
            if(model.getListing().getListingId().equals(id)){
                target = model;
                break;
            }
            i++;
        }
        if(target != null) {
            mResultModel.getItems().remove(target);
            setLastViewedPosition(i);
        }
        return target;
    }


    public void addResultModel(ResultModel resultModel) {
        if (mResultModel != null && resultModel != null) {
            List<ResultItemModel> resultItemModels = getItemsWithLocation(resultModel);
            prepareIfBookmarked(resultItemModels);
            mResultModel.getItems().addAll(resultItemModels);
            mPageCount++;
            mLastPageSize = resultItemModels.size();
        }
    }


    public ResultItemModel findItemModelByListingId(long listingId) {
        if (mResultModel != null) {
            for (ResultItemModel itemModel : mResultModel.getItems()) {
                if (itemModel.getListing().getListingId() == listingId) {
                    return itemModel;
                }
            }
        }
        if(mMapViewItems != null){
            for (ResultItemModel model : mMapViewItems){
                if (model.getListing() != null && model.getListing().getListingId() == listingId) {
                    return model;
                }
            }
        }
        return null;
    }


    public UserGetViewModel findUserModelById(long userId) {
        if (mResultModel != null) {
            for (ResultItemModel itemModel : mResultModel.getItems()) {
                if (itemModel.getUser().getId() == userId) {
                    return itemModel.getUser();
                }
            }
        }
        return null;
    }


    public Collection<ResultItemModel> getMapViewItems() {
        return mMapViewItems;
    }


    public void setMapViewItems(Collection<ResultItemModel> mapViewItems) {
        mMapViewItems = mapViewItems;
    }


    public int getPageCount() {
        return mPageCount;
    }


    public int getLastPageSize() {
        return mLastPageSize;
    }


    public int getCurrentResultItemsCount() {
        if (mResultModel != null && mResultModel.getItems() != null) {
            return mResultModel.getItems().size();
        } else {
            return 0;
        }
    }


    public String getResultsType() {
        return mResultsType;
    }


    public void setResultsType(String resultsType) {
        mResultsType = resultsType;
    }


    public SearchCriteria getSearchCriteria() {
        return mSearchCriteria;
    }

    public void setSearchCriteria(SearchCriteria criteria) {
        mSearchCriteria = criteria;
    }
    public void setLastViewedPosition(int lastViewedPosition) {
        mLastViewedPosition = lastViewedPosition;
    }


    public int getLastViewedPosition() {
        return mLastViewedPosition;
    }


    private List<ResultItemModel> getItemsWithLocation(ResultModel resultModel) {
        List<ResultItemModel> resultItemModels = new ArrayList<>();
        for (ResultItemModel resultItemModel : resultModel.getItems()) {
            if (resultItemModel.getListing() != null && resultItemModel.getListing().getGeoLocation() != null
                    && resultItemModel.getListing().getGeoLocation().getFullAddress() != null) {
                resultItemModels.add(resultItemModel);
            }
        }
        return resultItemModels;
    }


    private void prepareIfBookmarked(List<ResultItemModel> resultItemModels) {
        if (!SearchRestService.BOOKMARKED_TYPE.equals(mResultsType)) {
            return;
        }
        for (ResultItemModel resultItemModel : resultItemModels) {
            if (resultItemModel.getListing() != null) {
                resultItemModel.getListing().setIsBookmarked(true);
            }
        }
    }
}
