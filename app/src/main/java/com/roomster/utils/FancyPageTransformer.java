package com.roomster.utils;


import android.view.View;
import android.widget.ImageView;

import com.roomster.views.ReplacableViewPager;


/**
 * Created by michaelkatkov on 1/27/16.
 */
public class FancyPageTransformer implements ReplacableViewPager.PageTransformer, ReplacableViewPager.OnPageChangeListener {

    public static final String TAG = "FancyPageTransformer";

    private ReplacableViewPager mViewPager;

    private static final float MIN_SCALE  = 0.85f;
    private static final float ROT_DEGREE = -1.25f;
    private static final float ROT_MOD    = -15f;
    private static final float HALF       = 0.5f;

    private static final int LEFT             = 1;
    private static final int RIGHT            = 2;
    private static final int CHANGED_TO_RIGHT = 3;
    private static final int CHANGED_TO_LEFT  = 4;
    private static final int NONE             = 0;

    private int mCurrentPosition;
    private int mCurrentPositionOffset;

    private int mScrollDirection = NONE;


    public FancyPageTransformer(ReplacableViewPager viewPager, ImageView leftArrow, ImageView rightArrow) {
        mViewPager = viewPager;
        mViewPager.setPageTransformer(false, this);
        mViewPager.addOnPageChangeListener(this);
        mCurrentPosition = mViewPager.getCurrentItem();
        FancyImageAnimator animator = new FancyImageAnimator(viewPager, leftArrow, rightArrow);
        mViewPager.addOnPageChangeListener(animator);
    }


    @Override
    public void transformPage(View view, float position) {
        if (mScrollDirection == LEFT || mScrollDirection == CHANGED_TO_RIGHT) {
            makeSwipeLeft(view, position);
        } else if (mScrollDirection == RIGHT || mScrollDirection == CHANGED_TO_LEFT) {
            makeSwipeRight(view, position);
        }
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (mCurrentPositionOffset < positionOffsetPixels) {
            if (mCurrentPosition == position) {
                findLeftDirection(false);
            } else if (mCurrentPosition < position) {
                mScrollDirection = NONE;
            } else if (mCurrentPosition > position) {
                findRightDirection(true);
            }
        } else if (mCurrentPositionOffset > positionOffsetPixels) {
            if (mCurrentPosition == position) {
                findRightDirection(false);
            } else if (mCurrentPosition < position) {
                findLeftDirection(true);
            } else if (mCurrentPosition > position) {
                findLeftDirection(true);
            }
        }
        mCurrentPosition = position;
        mCurrentPositionOffset = positionOffsetPixels;
    }


    @Override
    public void onPageSelected(int position) {

    }


    @Override
    public void onPageScrollStateChanged(int state) {
        if (state == ReplacableViewPager.SCROLL_STATE_IDLE) {
            mScrollDirection = NONE;
            invalidate();
        }
    }


    public void invalidate() {
        for (int i = 0; i < mViewPager.getChildCount(); i++) {
            normalize(mViewPager.getChildAt(i));
        }
    }

    /**
     * Make swipe animation left
     *
     * @param view
     *   page which will animate
     * @param position
     *   [-infinity, -1] - page is invisible from left,
     *   [-1, 0] - page started to hide behind the left edge of the screen (We should make rotation of this view)
     *   (0, 1) - page started to be visible (We should make fadeOut)
     *   [1, infinity] - page is invisible from right
     */
    private void makeSwipeLeft(View view, float position) {
        mViewPager.setDrawingOrder(true);
        if (position <= -1) {
            normalize(view);
        } else if (position <= 0) {
            rotate(view, position);
        } else if (position < 1) {
            fadeOut(view, position);
        } else {
            normalize(view);
        }
    }


    /**
     * Make swipe animation right
     *
     * @param view
     *   page which will animate
     * @param position
     *   [-infinity, -1] - page is invisible from left,
     *   [-1, 0] - page started to be visible (We should make fadeOut)
     *   (0, 1) - page started to hide behind the left edge of the screen (We should make rotation of this view)
     *   [1, infinity] - page is invisible from right
     */
    private void makeSwipeRight(View view, float position) {
        mViewPager.setDrawingOrder(false);
        if (position < -1) {
            normalize(view);
        } else if (position <= 0) {
            fadeOut(view, position);
        } else if (position < 1) {
            rotate(view, position);
        } else if (position >= 1) {
            normalize(view);
        }
    }


    /**
     * make all animation parameters for current view as default
     */
    private void normalize(View view) {
        final float width = view.getWidth();
        final float height = view.getHeight();
        view.setAlpha(1);
        view.setTranslationX(0);
        view.setTranslationY(0);
        view.setScaleX(1);
        view.setScaleY(1);
        view.setPivotX(width * HALF);
        view.setPivotY(height);
        view.setRotation(0);
    }


    /**
     * make rotation animation for current view
     */
    private void rotate(View view, float position) {
        final float width = view.getWidth();
        final float height = view.getHeight();
        final float rotation = ROT_MOD * position * ROT_DEGREE;
        view.setPivotX(width * HALF);
        view.setPivotY(height);
        view.setRotation(rotation);
    }


    /**
     * make fade out animation for current view
     */
    private void fadeOut(View view, float position) {
        final float width = view.getWidth();
        final float height = view.getHeight();
        view.setAlpha(1 - Math.abs(position));
        view.setTranslationX(width * -position);
        float scaleFactor = MIN_SCALE + (1 - MIN_SCALE) * (1 - Math.abs(position));
        view.setScaleX(scaleFactor);
        view.setScaleY(scaleFactor);
        view.setPivotX(width * HALF);
        view.setPivotY(height);
        view.setRotation(0);
    }


    private void findLeftDirection(boolean isOriginIntersected) {
        if (mScrollDirection == NONE || mScrollDirection == LEFT || mScrollDirection == CHANGED_TO_RIGHT) {
            mScrollDirection = LEFT;
        } else {
            mScrollDirection = CHANGED_TO_LEFT;
        }
        if (isOriginIntersected && mScrollDirection == CHANGED_TO_LEFT) {
            mScrollDirection = NONE;
        }
    }


    private void findRightDirection(boolean isOriginIntersected) {
        if (mScrollDirection == NONE || mScrollDirection == RIGHT || mScrollDirection == CHANGED_TO_LEFT) {
            mScrollDirection = RIGHT;
        } else {
            mScrollDirection = CHANGED_TO_RIGHT;
        }
        if (isOriginIntersected && mScrollDirection == CHANGED_TO_RIGHT) {
            mScrollDirection = NONE;
        }
    }
}
