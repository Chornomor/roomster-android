package com.roomster.rest.service;


import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.InAppProductsRestServiceEvent;
import com.roomster.manager.NetworkManager;
import com.roomster.rest.api.InAppProductsApi;
import com.roomster.rest.model.InAppProductViewModel;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Bogoi.Bogdanov on 2/29/2016.
 */
public class InAppProductsRestService {

    private final static String PLATFORM = "Android";

    @Inject
    EventBus mEventBus;

    @Inject
    InAppProductsApi mInAppProductsApi;

    @Inject
    NetworkManager mNetworkManager;


    public InAppProductsRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void getInAppProducts() {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<List<InAppProductViewModel>> call = mInAppProductsApi.inAppProductsGet(PLATFORM);
            call.enqueue(new Callback<List<InAppProductViewModel>>() {

                @Override
                public void onResponse(Call<List<InAppProductViewModel>> call, Response<List<InAppProductViewModel>> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            mEventBus.post(new InAppProductsRestServiceEvent.GetInAppProductsSuccess(response.body()));
                            break;
                        default:
                            mEventBus.post(new InAppProductsRestServiceEvent.GetInAppProductsFailure());
                    }
                }


                @Override
                public void onFailure(Call<List<InAppProductViewModel>> call, Throwable t) {
                    mEventBus.post(new InAppProductsRestServiceEvent.GetInAppProductsFailure());
                }
            });
        } else {
            mEventBus.post(new InAppProductsRestServiceEvent.GetInAppProductsFailure());
        }
    }
}