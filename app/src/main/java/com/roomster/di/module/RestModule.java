package com.roomster.di.module;


import com.roomster.rest.RestClient;
import com.roomster.rest.api.AccountApi;
import com.roomster.rest.api.AndroidReceiptApi;
import com.roomster.rest.api.AppInstallsApi;
import com.roomster.rest.api.BillingApi;
import com.roomster.rest.api.BookmarksApi;
import com.roomster.rest.api.CataloguesApi;
import com.roomster.rest.api.CheckAppApi;
import com.roomster.rest.api.ConversationCountersApi;
import com.roomster.rest.api.ConversationsApi;
import com.roomster.rest.api.FBFriendsCountApi;
import com.roomster.rest.api.FBMutualFriendsApi;
import com.roomster.rest.api.HelpdeskApi;
import com.roomster.rest.api.ImagesApi;
import com.roomster.rest.api.ImagesExternalApi;
import com.roomster.rest.api.InAppProductsApi;
import com.roomster.rest.api.LikesApi;
import com.roomster.rest.api.ListingsApi;
import com.roomster.rest.api.LocalizationSettingsApi;
import com.roomster.rest.api.MeApi;
import com.roomster.rest.api.MegaphoneApi;
import com.roomster.rest.api.MessagesApi;
import com.roomster.rest.api.NotificationsApi;
import com.roomster.rest.api.PackagesApi;
import com.roomster.rest.api.PaymentCardApi;
import com.roomster.rest.api.ResourcesApi;
import com.roomster.rest.api.SavedSearchesApi;
import com.roomster.rest.api.SearchApi;
import com.roomster.rest.api.SubscriptionApi;
import com.roomster.rest.api.TokenApi;
import com.roomster.rest.api.TransactionsApi;
import com.roomster.rest.api.UsageLogApi;
import com.roomster.rest.api.UserListingsApi;
import com.roomster.rest.api.UserStatusApi;
import com.roomster.rest.api.UsersApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


/**
 * Module that provides objects for all Rest API classes.
 *
 * @author Bogoi.Bogdanov on 10/6/2015.
 */
@Module
public class RestModule {

    private RestClient mRestClient;


    public RestModule() {
        mRestClient = new RestClient();
    }


    @Provides
    @Singleton
    public RestClient provideRestClient() {
        return mRestClient;
    }


    @Provides
    @Singleton
    public AccountApi provideAccountApi() {
        return mRestClient.createService(AccountApi.class);
    }


    @Provides
    @Singleton
    public BookmarksApi provideBookmarksApi() {
        return mRestClient.createService(BookmarksApi.class);
    }


    @Provides
    @Singleton
    public CataloguesApi provideCataloguesApi() {
        return mRestClient.createService(CataloguesApi.class);
    }


    @Provides
    @Singleton
    public ConversationCountersApi provideConversationCountersApi() {
        return mRestClient.createService(ConversationCountersApi.class);
    }


    @Provides
    @Singleton
    public ConversationsApi provideConversationsApi() {
        return mRestClient.createService(ConversationsApi.class);
    }


    @Provides
    @Singleton
    public FBFriendsCountApi provideFBFriendsCountApi() {
        return mRestClient.createService(FBFriendsCountApi.class);
    }


    @Provides
    @Singleton
    public FBMutualFriendsApi provideFBMutualFriendsApi() {
        return mRestClient.createService(FBMutualFriendsApi.class);
    }


    @Provides
    @Singleton
    public HelpdeskApi provideHelpdeskApi() {
        return mRestClient.createService(HelpdeskApi.class);
    }


    @Provides
    @Singleton
    public ImagesApi provideImagesApi() {
        return mRestClient.createService(ImagesApi.class);
    }


    @Provides
    @Singleton
    public ImagesExternalApi provideImagesExternalApi() {
        return mRestClient.createService(ImagesExternalApi.class);
    }


    @Provides
    @Singleton
    public LikesApi provideLikesApi() {
        return mRestClient.createService(LikesApi.class);
    }


    @Provides
    @Singleton
    public ListingsApi provideListingsApi() {
        return mRestClient.createService(ListingsApi.class);
    }


    @Provides
    @Singleton
    public MeApi provideMeApi() {
        return mRestClient.createService(MeApi.class);
    }


    @Provides
    @Singleton
    public MegaphoneApi provideMegaphoneApi() {
        return mRestClient.createService(MegaphoneApi.class);
    }


    @Provides
    @Singleton
    public MessagesApi provideMessagesApi() {
        return mRestClient.createService(MessagesApi.class);
    }


    @Provides
    @Singleton
    public NotificationsApi provideNotificationsApi() {
        return mRestClient.createService(NotificationsApi.class);
    }


    @Provides
    @Singleton
    public PackagesApi providePackagesApi() {
        return mRestClient.createService(PackagesApi.class);
    }


    @Provides
    @Singleton
    public PaymentCardApi providePaymentCardApi() {
        return mRestClient.createService(PaymentCardApi.class);
    }

    @Provides
    @Singleton
    public BillingApi provideBillingApi() {
        return mRestClient.createService(BillingApi.class);
    }


    @Provides
    @Singleton
    public ResourcesApi provideResourcesApi() {
        return mRestClient.createService(ResourcesApi.class);
    }


    @Provides
    @Singleton
    public SavedSearchesApi provideSavedSearchesApi() {
        return mRestClient.createService(SavedSearchesApi.class);
    }


    @Provides
    @Singleton
    public SearchApi provideSearchApi() {
        return mRestClient.createService(SearchApi.class);
    }


    @Provides
    @Singleton
    public SubscriptionApi provideSubscriptionApi() {
        return mRestClient.createService(SubscriptionApi.class);
    }


    @Provides
    @Singleton
    public TokenApi provideTokenApi() {
        return mRestClient.createService(TokenApi.class);
    }


    @Provides
    @Singleton
    public TransactionsApi provideTransactionsApi() {
        return mRestClient.createService(TransactionsApi.class);
    }


    @Provides
    @Singleton
    public UsageLogApi provideUsageLogApi() {
        return mRestClient.createService(UsageLogApi.class);
    }


    @Provides
    @Singleton
    public UserListingsApi provideUserListingsApi() {
        return mRestClient.createService(UserListingsApi.class);
    }


    @Provides
    @Singleton
    public UsersApi provideUsersApi() {
        return mRestClient.createService(UsersApi.class);
    }


    @Provides
    @Singleton
    public AppInstallsApi provideAppInstallsApi() {
        return mRestClient.createService(AppInstallsApi.class);
    }


    @Provides
    @Singleton
    public InAppProductsApi provideInAppProductsApi() {
        return mRestClient.createService(InAppProductsApi.class);
    }


    @Provides
    @Singleton
    public AndroidReceiptApi provideAndroidReceiptApi() {
        return mRestClient.createService(AndroidReceiptApi.class);
    }


    @Provides
    @Singleton
    public LocalizationSettingsApi provideLocalizationSettingsApi() {
        return mRestClient.createService(LocalizationSettingsApi.class);
    }

    @Provides
    @Singleton
    public CheckAppApi provideCheckAppApi() {
        return mRestClient.createService(CheckAppApi.class);
    }

    @Provides
    @Singleton
    public UserStatusApi provideUserStatusApi() {
        return mRestClient.createService(UserStatusApi.class);
    }
}