package com.roomster.rest.api;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface FBFriendsCountApi {

    /**
     * Get total count of Facebook friends
     *
     * @param userId
     *   User Id
     * @return Call<Object>
     */

    @GET("FBFriendsCount")
    Call<Object> fBFriendsCountGet(@Query("user_id") Long userId);
}
