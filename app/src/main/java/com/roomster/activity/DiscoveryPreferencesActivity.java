package com.roomster.activity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.android.gms.analytics.HitBuilders;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.ManagersEvents.DiscoveryPrefsManagerEvent;
import com.roomster.event.discovery.DiscoveryPrefsEvent;
import com.roomster.fragment.DiscoveryPrefsFragment;


public class DiscoveryPreferencesActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.tv_title)
    TextView mTitleTv;


    @Override
    protected int getMainContentLayout() {
        return R.layout.activity_discovery_preferences;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        setUpActionBar();
        setContent(new DiscoveryPrefsFragment());
        mTitleTv.setText(getString(R.string.discovery_preferences));
    }


    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName(getString(R.string.screen_name_discovery_preferences));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    @Override
    public void onBackPressed() {
        mEventBus.post(new DiscoveryPrefsEvent.UpdatePrefsEvent());
        mEventBus.post(new DiscoveryPrefsManagerEvent.ResetSearchCriteriaEvent());
        super.onBackPressed();
    }


    @OnClick(R.id.tv_done)
    public void onDoneClick() {
        onBackPressed();
    }


    public void setContent(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_content, fragment).commit();
    }


    private void setUpActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
    }
}