package com.roomster.fragment;


import com.roomster.R;
import com.roomster.event.RestServiceEvents.BookmarksRestServiceEvent;
import com.roomster.event.RestServiceEvents.SearchRestServiceEvent;
import com.roomster.listener.BookmarksCallbackListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


public class BookmarksListViewFragment extends SearchListingsListBookmarkFragment {

    public void setCallback(BookmarksCallbackListener callback) {
        super.mCallback = callback;
    }


    @Override
    protected void onInitEmptyLayoutViews() {
        mEmptyResultHeaderTV.setText(getString(R.string.empty_bookmark_header));
        mEmptyResultSubtextTV.setText(getString(R.string.empty_bookmark_subtext));
    }


    @Override
    protected void onBookmarked(BookmarksRestServiceEvent.AddBookmarkSuccess event) {
        updateBookmarks();
    }


    @Override
    protected void onUnbookmarked(BookmarksRestServiceEvent.RemoveBookmarkSuccess event) {
        updateBookmarks();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(SearchRestServiceEvent.BookmarkSearchSuccess event) {
        onNewListings(bookmarkManager.getResultModel().getItems());
        if (mCallback != null) mCallback.onBookmarksCountChanged(bookmarkManager.getResultModel().getCount());
    }
}