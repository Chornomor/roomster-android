package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


public class FacebookPage {

    /**
     **/
    @SerializedName("Id")
    private Long id = null;

    /**
     **/
    @SerializedName("FacebookId")
    private String facebookId = null;

    /**
     **/
    @SerializedName("Name")
    private String name = null;

    /**
     **/
    @SerializedName("Category")
    private String category = null;


    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public String getFacebookId() {
        return facebookId;
    }


    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getCategory() {
        return category;
    }


    public void setCategory(String category) {
        this.category = category;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class FacebookPage {\n");

        sb.append("  id: ").append(id).append("\n");
        sb.append("  facebookId: ").append(facebookId).append("\n");
        sb.append("  name: ").append(name).append("\n");
        sb.append("  category: ").append(category).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
