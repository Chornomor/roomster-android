package com.roomster.rest.api;


import com.roomster.rest.model.CheckAppModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface CheckAppApi {

    @GET("CheckApp")
    Call<CheckAppModel> shouldUpdateApp(@Query("osType") String osType, @Query("build") Long build);
}
