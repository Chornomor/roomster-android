package com.roomster.views;


import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.emilsjolander.components.StickyScrollViewItems.StickyScrollView;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.FacebookGenders;
import com.roomster.model.FacebookInterestsModel;
import com.roomster.model.FacebookUserModel;
import com.roomster.rest.model.FacebookPage;
import com.roomster.rest.model.UserGetViewModel;
import com.wefika.flowlayout.FlowLayout;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by "Michael Katkov" on 10/14/2015.
 */
public class BottomProfileSlidingView extends LinearLayout {

    @Bind(R.id.bs_profile_name)
    TextView mProfileNameTv;

    @Bind(R.id.bs_profile_gender)
    ImageView mProfileGenderIv;

    @Bind(R.id.bs_iv_price_layout)
    RelativeLayout mPriceLayout;

    @Bind(R.id.bs_profile_user_listings)
    TextView mShowListings;

    @Bind(R.id.currency_sign_text_view)
    TextView mCurrencySign;

    @Bind(R.id.price_text_view)
    TextView mPriceTv;

    @Bind(R.id.currency_text_view)
    TextView mCurrencyTv;

    @Bind(R.id.bs_iv_period_text)
    TextView mPeriodTv;

    @Bind(R.id.bs_profile_about_title_layout)
    LinearLayout mAboutLayout;

    @Bind(R.id.bs_profile_about_text)
    TextView mAboutDescriptionTv;

    @Bind(R.id.bs_profile_languages_title_layout)
    LinearLayout mLanguagesLayout;

    @Bind(R.id.bs_profile_languages_chips_layout)
    FlowLayout mLanguagesChipsLayout;

    @Bind(R.id.bs_profile_interests_title_layout)
    LinearLayout mInterestsLayout;

    @Bind(R.id.bs_profile_interests_chips_layout)
    FlowLayout mInterestsChipsLayout;

    @Bind(R.id.bs_facebook_connections_layout)
    RelativeLayout mFacebookConnectionsLayout;

    @Bind(R.id.bs_profile_friend_in_common)
    TextView mFriendsInCommon;

    @Bind(R.id.bs_profile_info_container)
    LinearLayout mInfoContainer;

    @Bind(R.id.bs_profile_content)
    FrameLayout mContentView;
    @Bind(R.id.bottom_view_scroll)
    StickyScrollView scrollView;

    @Inject
    LayoutInflater mLayoutInflater;


    public BottomProfileSlidingView(Context context) {
        super(context);
        initView();
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public BottomProfileSlidingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public void setUserGetViewModel(UserGetViewModel userGetViewModel) {
        clearLayouts();
        fillUserName(userGetViewModel);
        fillGender(userGetViewModel);
        fillAboutDescription(userGetViewModel);
        fillShowListings(userGetViewModel);
        if (userGetViewModel.getSpokenLanguages() != null) {
            fillLanguages(userGetViewModel);
        }
    }


    private void fillGender(UserGetViewModel userGetViewModel) {
        if (userGetViewModel.getGender() != null) {
            switch (userGetViewModel.getGender()) {
                case Female:
                    mProfileGenderIv.setImageResource(R.drawable.ic_female);
                    mProfileGenderIv.setVisibility(VISIBLE);
                    break;
                case Male:
                    mProfileGenderIv.setImageResource(R.drawable.ic_male);
                    mProfileGenderIv.setVisibility(VISIBLE);
                    break;
                default:
                    break;
            }
        }
    }


    public void setFacebookUserModel(FacebookUserModel facebookUserModel) {
        fillGender(facebookUserModel);
    }


    public void setFacebookInterestsModel(FacebookInterestsModel facebookInterestsModel) {
        List<FacebookInterestsModel.DataModel> interests = facebookInterestsModel.getData();
        if (mInterestsChipsLayout.getChildCount() > 0 ) mInterestsChipsLayout.removeAllViews();
        if (interests != null && !interests.isEmpty()) {
            FlowLayout.LayoutParams params = initFlowParams();
            for (FacebookInterestsModel.DataModel dataModel : interests) {
                addChip(dataModel.getName(), mInterestsChipsLayout, params);
            }
            mInterestsChipsLayout.setVisibility(VISIBLE);
            mInterestsLayout.setVisibility(VISIBLE);
        }
    }


    public void setInterests(List<FacebookPage> interests) {
        if (interests != null && !interests.isEmpty()) {
            if (mInterestsChipsLayout.getChildCount() > 0 ) mInterestsChipsLayout.removeAllViews();
            FlowLayout.LayoutParams params = initFlowParams();
            for (FacebookPage dataModel : interests) {
                addChip(dataModel.getName(), mInterestsChipsLayout, params);
            }
            mInterestsChipsLayout.setVisibility(VISIBLE);
            mInterestsLayout.setVisibility(VISIBLE);
        }
    }


    public void setMutualFriends(int mutualFriends) {
        mFriendsInCommon.setText(String.valueOf(mutualFriends));
        mFacebookConnectionsLayout.setVisibility(VISIBLE);
    }

    public void hideMutualFriends (){
        mFacebookConnectionsLayout.setVisibility(GONE);
    }


    public void setBottomViewVisibility(int visibility) {
        ViewGroup.LayoutParams layoutParams = mContentView.getLayoutParams();
        if (visibility == VISIBLE) {
            layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
        } else {
            layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        }
        mInfoContainer.setVisibility(visibility);
        mContentView.setLayoutParams(layoutParams);
    }


    private void clearLayouts() {
        mLanguagesChipsLayout.removeAllViews();
    }


    private void initView() {
        inflate(getContext(), R.layout.view_profile_bottom_sliding, this);
        ButterKnife.bind(this);
        //we don't show price in user profile
        mPriceLayout.setVisibility(GONE);
        //set all views invisible and add them only when data are available
        mProfileNameTv.setVisibility(GONE);
        mAboutDescriptionTv.setVisibility(GONE);
        mProfileGenderIv.setVisibility(GONE);
        mLanguagesChipsLayout.setVisibility(GONE);
        mInterestsChipsLayout.setVisibility(GONE);
        mInterestsLayout.setVisibility(GONE);
        mFacebookConnectionsLayout.setVisibility(GONE);
    }


    private void fillUserName(UserGetViewModel userGetViewModel) {
        String userName = userGetViewModel.getFirstName();
        int userAge = userGetViewModel.getAge();
        if (!userName.equals("")) {
            String nameAndAge = userName + ", " + userAge;
            mProfileNameTv.setText(nameAndAge);
            mProfileNameTv.setVisibility(VISIBLE);
        }
    }


    private void fillAboutDescription(UserGetViewModel userGetViewModel) {
        String description = userGetViewModel.getDescription();
        if (description != null && !description.trim().isEmpty()) {
            mAboutDescriptionTv.setText(description);
            mAboutLayout.setVisibility(VISIBLE);
            mAboutDescriptionTv.setVisibility(VISIBLE);
        } else {
            mAboutDescriptionTv.setVisibility(GONE);
            mAboutDescriptionTv.setPadding(0,0,0,0);
        }
    }


    private void fillShowListings(UserGetViewModel userGetViewModel) {
        if (userGetViewModel != null && userGetViewModel.getTotalListings() != null && getContext() != null) {
            mShowListings
              .setText(String.format(getContext().getString(R.string.show_listings_format), userGetViewModel.getTotalListings()));
            mShowListings.setVisibility(VISIBLE);
        }
    }


    private FlowLayout.LayoutParams initFlowParams() {
        FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(FlowLayout.LayoutParams.WRAP_CONTENT,
          FlowLayout.LayoutParams.WRAP_CONTENT);
        int margin = getContext().getResources().getDimensionPixelSize(R.dimen.offset_0_5);
        params.setMargins(margin, margin, margin, margin);
        return params;
    }


    private void fillLanguages(UserGetViewModel userGetViewModel) {
        List<String> languages = userGetViewModel.getSpokenLanguages();
        if (languages != null && !languages.isEmpty()) {
            FlowLayout.LayoutParams params = initFlowParams();
            for (String language : languages) {
                addChip(language, mLanguagesChipsLayout, params);
            }
            mLanguagesChipsLayout.setVisibility(VISIBLE);
            mLanguagesLayout.setVisibility(VISIBLE);
        }
    }


    private void addChip(String text, FlowLayout flowLayout, FlowLayout.LayoutParams params) {
        TextView chipsView = (TextView) mLayoutInflater.inflate(R.layout.view_chip, null);
        chipsView.setLayoutParams(params);
        chipsView.setText(text);
        flowLayout.addView(chipsView);
    }


    private void fillGender(FacebookUserModel facebookUserModel) {
        if (mProfileGenderIv.getVisibility() == VISIBLE) {
            //prefer the gender from roomster if it is set
            return;
        }

        String gender = facebookUserModel.getGender();
        if (gender == null) {
            return;
        }
        switch (gender.toLowerCase()) {
            case FacebookGenders.FEMALE:
                mProfileGenderIv.setImageResource(R.drawable.ic_female);
                break;
            case FacebookGenders.MALE:
                mProfileGenderIv.setImageResource(R.drawable.ic_male);
                break;
        }
        mProfileGenderIv.setVisibility(VISIBLE);
    }


    public void setOnShowListingsClickListener(View.OnClickListener onShowListingsClickListener) {
        mShowListings.setOnClickListener(onShowListingsClickListener);
    }

    public void setupIsMeDiff(){
        mAboutLayout.setPadding(0,0,0,0);
    }

    public void setAboutExpanded(boolean expanded){
        scrollView.scrollTo(0,0);
        mAboutDescriptionTv.setSingleLine(!expanded);
    }
}
