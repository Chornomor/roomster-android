package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class NeedApartment implements Serializable {

    /**
     * Allowed values @ /v1/catalogues/apartment_types
     **/
    @SerializedName("apartment_type_ids")
    private ArrayList<Integer> apartmentTypeIds = null;


    /**
     **/
    @SerializedName("is_furnished")
    private Boolean isFurnished = null;


    public List<Integer> getApartmentTypeIds() {
        return apartmentTypeIds;
    }


    public void setApartmentTypeIds(List<Integer> apartmentTypeIds) {
        this.apartmentTypeIds = new ArrayList<>(apartmentTypeIds);
    }


    public Boolean getIsFurnished() {
        return isFurnished;
    }


    public void setIsFurnished(Boolean isFurnished) {
        this.isFurnished = isFurnished;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class NeedApartment {\n");

        sb.append("  apartmentTypeIds: ").append(apartmentTypeIds).append("\n");
        sb.append("  isFurnished: ").append(isFurnished).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
