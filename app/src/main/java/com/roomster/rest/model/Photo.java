package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;


/**
 * Created by "Michael Katkov" on 11/21/2015.
 */
public class Photo implements Parcelable, Serializable {

    /**
     **/
    @SerializedName("id")
    private long mId;

    /**
     **/
    @SerializedName("path")
    private String mPath;


    public Photo() {

    }


    protected Photo(Parcel in) {
        mId = in.readLong();
        mPath = in.readString();
    }


    public static final Creator<Photo> CREATOR = new Creator<Photo>() {

        @Override
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }


        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };


    public long getId() {
        return mId;
    }


    public void setId(long id) {
        this.mId = id;
    }


    public String getPath() {
        return mPath;
    }


    public void setPath(String path) {
        this.mPath = path;
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeString(mPath);
    }
}
