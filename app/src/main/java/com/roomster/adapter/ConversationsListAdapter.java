package com.roomster.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.rest.model.ConversationUserProfile;
import com.roomster.rest.model.ConversationView;
import com.roomster.utils.ImageHelper;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;


public class ConversationsListAdapter extends RecyclerView.Adapter<ConversationsListAdapter.ViewHolder> {



    private List<ConversationView>      mItems;
    private TreeSet<ConversationView>   mItemsSet;
    private Context                     mContext;
    private OnConversationClickListener mOnItemClickListener;


    public interface OnConversationClickListener {

        void onClick(ConversationView model);
    }


    public ConversationsListAdapter(Context context, List<ConversationView> items,
                                    OnConversationClickListener onItemClickListener) {
        mContext = context;
        mOnItemClickListener = onItemClickListener;
        if (items != null) {
            mItems = items;
        } else {
            mItems = new ArrayList<>();
        }
        mItemsSet = new TreeSet<>(new Comparator<ConversationView>() {

            // preserve the ordering of the list in the set
            @Override
            public int compare(ConversationView lhs, ConversationView rhs) {
                if (lhs.getConversationId().equals(rhs.getConversationId())) {
                    return 0;
                }
                return 1;
            }
        });
        mItemsSet.addAll(mItems);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_conversation, parent, false);

        return new ViewHolder(root);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ConversationView conversation = mItems.get(position);

        holder.conversationModel = conversation;
        holder.lastMessageTextView.setText(conversation.getText());
        holder.onClickListener = mOnItemClickListener;

        ConversationUserProfile otherUser = conversation.getUsers().get(0);
        holder.profileNameTextView.setText(otherUser.getFirstName());
        if (conversation.getCountNew().equals(0)) {
            ImageHelper.loadCircledImageInto(mContext, otherUser.getUserPhoto(), holder.profilePictureImageView);
        } else {
            ImageHelper.loadGreenBorderedCircledImageInto(mContext, otherUser.getUserPhoto(), holder.profilePictureImageView);
        }
    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }


    public String getLastReadMessageId() {
        String lastMessageId = "";
        for (int i = 0; i < mItems.size(); i++) {
            ConversationView conversation = mItems.get(i);
            if (conversation.getCountNew().equals(Integer.valueOf(0))) {
                lastMessageId = conversation.getMessageId();
                break;
            }
        }
        return lastMessageId;
    }

    public ExtractModel extract(String id){
        ConversationView view = getById(id);
        if(view == null)
            return null;
        int position = mItems.indexOf(view);
        mItems.remove(view);
        mItemsSet.remove(view);
        notifyDataSetChanged();
        return new ExtractModel(position,view);
    }

    public ConversationView getById(String id){
        for(ConversationView view : mItems)
            if(view.getConversationId().equals(id))
                return view;
        return null;
    }
    public void returnConversation(ExtractModel extractModel){
        mItems.add(extractModel.position,extractModel.object);
        mItemsSet.clear();
        mItemsSet.addAll(mItems);
        mItems.clear();
        mItems.addAll(mItemsSet);
        notifyDataSetChanged();
    }
    public void addItems(List<ConversationView> newItems) {
        if (newItems != null && !newItems.isEmpty()) {
            mItems.addAll(0, newItems);
            mItemsSet.clear();
            mItemsSet.addAll(mItems);
            mItems.clear();
            mItems.addAll(mItemsSet);
            notifyDataSetChanged();
        }
    }

    public void updateAllItems(List<ConversationView> newItems) {
        if (newItems != null && !newItems.isEmpty()) {
            mItemsSet.clear();
            mItemsSet.addAll(newItems);
            mItems.clear();
            mItems.addAll(mItemsSet);

            notifyDataSetChanged();
        }
    }
    public List<ConversationView> getItems() {
        return mItems;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ConversationView            conversationModel;
        ImageView                   profilePictureImageView;
        TextView                    profileNameTextView;
        TextView                    lastMessageTextView;
        OnConversationClickListener onClickListener;


        public ViewHolder(View root) {
            super(root);
            root.setOnClickListener(this);
            profilePictureImageView = (ImageView) root.findViewById(R.id.list_item_conversation_profile_picture);
            profileNameTextView = (TextView) root.findViewById(R.id.list_item_conversation_profile_name);
            lastMessageTextView = (TextView) root.findViewById(R.id.list_item_conversation_last_message);
        }


        @Override
        public void onClick(View v) {
            onClickListener.onClick(conversationModel);
        }
    }

    public static class ExtractModel {
        public int position;
        public ConversationView object;

        public ExtractModel(int position, ConversationView object) {
            this.position = position;
            this.object = object;
        }
    }
}
