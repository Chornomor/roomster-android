package com.roomster.views.signin;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.roomster.R;

/**
 * Created by andreybofanov on 18.07.16.
 */
public class SigninPagerIndicator extends RadioGroup {
    int radioButtonPadding;
    public SigninPagerIndicator(Context context) {
        super(context);
        init();
    }

    public SigninPagerIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    void init(){
        radioButtonPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,5,getResources().getDisplayMetrics());
        setOrientation(HORIZONTAL);
    }

    public void onPageSelected(int which){
        if(getChildCount() <= which)
            return;
        if( ! (getChildAt(which) instanceof RadioButton))
            return;

        ((RadioButton)getChildAt(which)).setChecked(true);
    }

    public void setUpIndicators(int count){
        RadioGroup.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER;
        for(int i = 0; i < count; i++)
            addView(getRadioButton(i),params);
    }

    RadioButton getRadioButton(int position){
        RadioButton rb = new RadioButton(getContext());
        rb.setButtonDrawable(R.drawable.signin_slider_indicator);
        rb.setPadding(radioButtonPadding,0,0,0);
        rb.setId(position);
        if(position == 0)
            rb.setChecked(true);
        return rb;
    }



}
