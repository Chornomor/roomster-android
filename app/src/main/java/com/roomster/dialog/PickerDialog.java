package com.roomster.dialog;


import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.roomster.R;

import butterknife.Bind;
import butterknife.ButterKnife;


public class PickerDialog extends AlertDialog {

    @Bind(R.id.dialog_picker_list_view)
    ListView mListView;

    private int mSelectedPosition;


    public PickerDialog(Context context, ListAdapter adapter, int titleResId, final AdapterView.OnItemClickListener listener) {
        super(context);

        setButton(BUTTON_NEGATIVE, getContext().getString(R.string.dialog_cancel_button), new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        setTitle(titleResId);
        initView();

        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mSelectedPosition = position;
                listener.onItemClick(parent, view, position, id);
                dismiss();
            }
        });
    }


    public ListAdapter getAdapter() {
        return mListView.getAdapter();
    }


    public int getSelectedPosition() {
        return mSelectedPosition;
    }


    private void initView() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.dialog_picker, null);
        ButterKnife.bind(this, contentView);
        setView(contentView);
    }
}
