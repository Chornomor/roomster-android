package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import com.roomster.enums.BookmarkEnum;
import com.roomster.enums.ServiceTypeEnum;
import com.roomster.enums.SortEnum;


public class ParamsModel {

    /**
     * Page number
     **/
    @SerializedName("page_number")
    private Integer pageNumber = null;

    /**
     * Records per page
     **/
    @SerializedName("page_size")
    private Integer pageSize = null;

    @SerializedName("sort")
    private SortEnum sort = null;

    @SerializedName("service_type")
    private ServiceTypeEnum serviceType = null;

    @SerializedName("geo")
    private GeoModel geo = null;

    @SerializedName("budget")
    private RangeModel budget = null;

    @SerializedName("age")
    private RangeModel age = null;

    /**
     * CSV list of ids from <br />
     * /v1/catalogues/household_sex_category
     **/
    @SerializedName("household_sex")
    private String       householdSex   = null;
    /**
     * CSV list of ids from <br />
     * /v1/catalogues/sex_category
     **/
    @SerializedName("sex")
    private String       sex            = null;
    /**
     * CSV list of ids from <br />
     * /v1/catalogues/zodiac_category
     **/
    @SerializedName("zodiac")
    private String       zodiac         = null;
    /**
     * CSV list of ids from <br />
     * /v1/catalogues/pets_preferred <br />
     * 0 - no pets
     **/
    @SerializedName("pets_preference")
    private String       petsPreference = null;
    /**
     * CSV list of ids from <br />
     * /v1/catalogues/pets_owned <br />
     * 0 - no pets
     **/
    @SerializedName("my_pets")
    private String       myPets         = null;
    /**
     * CSV list of ids from <br />
     * /v1/catalogues/bedrooms
     **/
    @SerializedName("bedrooms")
    private String       bedrooms       = null;
    /**
     * CSV list of ids from <br />
     * /v1/catalogues/bathrooms
     **/
    @SerializedName("bathrooms")
    private String       bathrooms      = null;
    /**
     * CSV list of ids from <br />
     * /v1/catalogues/apt_amenities
     **/
    @SerializedName("amenities")
    private String       amenities      = null;
    /**
     **/
    @SerializedName("bookmark")
    private BookmarkEnum bookmark       = null;


    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public SortEnum getSort() {
        return sort;
    }

    public void setSort(SortEnum sort) {
        this.sort = sort;
    }

    public ServiceTypeEnum getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceTypeEnum serviceType) {
        this.serviceType = serviceType;
    }

    public GeoModel getGeo() {
        return geo;
    }

    public void setGeo(GeoModel geo) {
        this.geo = geo;
    }

    public RangeModel getBudget() {
        return budget;
    }

    public void setBudget(RangeModel budget) {
        this.budget = budget;
    }

    public RangeModel getAge() {
        return age;
    }

    public void setAge(RangeModel age) {
        this.age = age;
    }

    public String getHouseholdSex() {
        return householdSex;
    }

    public void setHouseholdSex(String householdSex) {
        this.householdSex = householdSex;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getZodiac() {
        return zodiac;
    }

    public void setZodiac(String zodiac) {
        this.zodiac = zodiac;
    }

    public String getPetsPreference() {
        return petsPreference;
    }

    public void setPetsPreference(String petsPreference) {
        this.petsPreference = petsPreference;
    }

    public String getMyPets() {
        return myPets;
    }

    public void setMyPets(String myPets) {
        this.myPets = myPets;
    }

    public String getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(String bedrooms) {
        this.bedrooms = bedrooms;
    }

    public String getBathrooms() {
        return bathrooms;
    }

    public void setBathrooms(String bathrooms) {
        this.bathrooms = bathrooms;
    }

    public String getAmenities() {
        return amenities;
    }

    public void setAmenities(String amenities) {
        this.amenities = amenities;
    }

    public BookmarkEnum getBookmark() {
        return bookmark;
    }

    public void setBookmark(BookmarkEnum bookmark) {
        this.bookmark = bookmark;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ParamsModel {\n");

        sb.append("  pageNumber: ").append(pageNumber).append("\n");
        sb.append("  pageSize: ").append(pageSize).append("\n");
        sb.append("  sort: ").append(sort).append("\n");
        sb.append("  serviceType: ").append(serviceType).append("\n");
        sb.append("  geo: ").append(geo).append("\n");
        sb.append("  budget: ").append(budget).append("\n");
        sb.append("  age: ").append(age).append("\n");
        sb.append("  householdSex: ").append(householdSex).append("\n");
        sb.append("  sex: ").append(sex).append("\n");
        sb.append("  zodiac: ").append(zodiac).append("\n");
        sb.append("  petsPreference: ").append(petsPreference).append("\n");
        sb.append("  myPets: ").append(myPets).append("\n");
        sb.append("  bedrooms: ").append(bedrooms).append("\n");
        sb.append("  bathrooms: ").append(bathrooms).append("\n");
        sb.append("  amenities: ").append(amenities).append("\n");
        sb.append("  bookmark: ").append(bookmark).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}