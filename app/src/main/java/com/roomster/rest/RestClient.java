package com.roomster.rest;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.roomster.constants.RoomsterEndpoint;
import com.roomster.utils.DateTypeDeserializer;
import com.roomster.utils.NullOnEmptyConverterFactory;
import com.roomster.utils.ToStringConverterFactory;
import com.roomster.utils.UserAgentInterceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.Date;
import java.util.concurrent.TimeUnit;


/**
 * General Rest client class that creates a Retrofit object, used for executing http requests.
 */
public class RestClient {

    private final Retrofit mRetrofit;
    private final Gson     mGson;

    private static final int THIRTY_SECONDS = 30;


    public RestClient() {

        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(THIRTY_SECONDS, TimeUnit.SECONDS)
                .readTimeout(THIRTY_SECONDS, TimeUnit.SECONDS).writeTimeout(THIRTY_SECONDS, TimeUnit.SECONDS)
                .addInterceptor(new UserAgentInterceptor()).build();
        mGson = new GsonBuilder().registerTypeAdapter(Date.class, new DateTypeDeserializer()).create();
        mRetrofit = new Retrofit.Builder().client(client).addConverterFactory(new NullOnEmptyConverterFactory())
                .addConverterFactory(GsonConverterFactory.create(mGson)).baseUrl(getBaseUrl()).build();

    }


    public static String getBaseUrl() {
        return RoomsterEndpoint.BASE_URL_PRODUCTION;
    }


    public Gson getGson() {
        return mGson;
    }


    public <S> S createService(Class<S> serviceClass) {
        return mRetrofit.create(serviceClass);
    }
}