package com.roomster.model;


import java.util.List;


/**
 * Created by "Michael Katkov" on 10/14/2015.
 */
public class FacebookInterestsModel {

    private List<DataModel> data;

    private PagingModel paging;


    public List<DataModel> getData() {
        return data;
    }


    public void setData(List<DataModel> data) {
        this.data = data;
    }


    public PagingModel getPaging() {
        return paging;
    }


    public void setPaging(PagingModel paging) {
        this.paging = paging;
    }


    public static class DataModel {

        private String name;
        private String category;
        private String id;
        private String created_time;


        public String getCreated_time() {
            return created_time;
        }


        public void setCreated_time(String created_time) {
            this.created_time = created_time;
        }


        public String getId() {
            return id;
        }


        public void setId(String id) {
            this.id = id;
        }


        public String getCategory() {
            return category;
        }


        public void setCategory(String category) {
            this.category = category;
        }


        public String getName() {
            return name;
        }


        public void setName(String name) {
            this.name = name;
        }
    }


    public static class PagingModel {

        private Cursors cursors;
        private String  previous;
        private String  next;


        public Cursors getCursors() {
            return cursors;
        }


        public void setCursors(Cursors cursors) {
            this.cursors = cursors;
        }


        public String getPrevious() {
            return previous;
        }


        public void setPrevious(String previous) {
            this.previous = previous;
        }


        public String getNext() {
            return next;
        }


        public void setNext(String next) {
            this.next = next;
        }


        public static class Cursors {

            private String after;

            private String before;


            public String getAfter() {
                return after;
            }


            public void setAfter(String after) {
                this.after = after;
            }


            public String getBefore() {
                return before;
            }


            public void setBefore(String before) {
                this.before = before;
            }
        }
    }
}
