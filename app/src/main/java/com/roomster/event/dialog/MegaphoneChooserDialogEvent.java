package com.roomster.event.dialog;


import com.roomster.event.Event;


public class MegaphoneChooserDialogEvent extends Event {

    public String chosenMessage;


    public MegaphoneChooserDialogEvent(String chosenMessage) {
        this.chosenMessage = chosenMessage;
    }
}
