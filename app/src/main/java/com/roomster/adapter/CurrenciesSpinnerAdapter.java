package com.roomster.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;

import java.util.List;

import javax.inject.Inject;


/**
 * Created by "Michael Katkov" on 10/7/2015.
 */
public class CurrenciesSpinnerAdapter extends ArrayAdapter<String> {

    public static final int SETTINGS_TYPE    = 1;
    public static final int ADD_LISTING_TYPE = 2;
    private             int mType            = SETTINGS_TYPE;

    @Inject
    LayoutInflater mLayoutInflater;


    public CurrenciesSpinnerAdapter(Context context, List<String> objects, int type) {
        super(context, 0, objects);
        mType = type;
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = chooseView(parent);
        }
        TextView textV = (TextView) view.findViewById(R.id.spinnerItem);
        textV.setText(getItem(position));
        return view;
    }


    private View chooseView(ViewGroup parent) {
        View view;
        switch (mType) {
            case SETTINGS_TYPE:
                view = mLayoutInflater.inflate(R.layout.item_spinner_layout, parent, false);
                break;
            case ADD_LISTING_TYPE:
                view = mLayoutInflater.inflate(R.layout.item_spinner_add_listing_layout, parent, false);
                break;
            default:
                view = mLayoutInflater.inflate(R.layout.item_spinner_layout, parent, false);
                break;
        }
        return view;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = chooseDropDownView(parent);
        }
        TextView textV = (TextView) view.findViewById(R.id.spinnerDropDownItem);
        textV.setText(getItem(position));
        return view;
    }


    private View chooseDropDownView(ViewGroup parent) {
        View view;
        switch (mType) {
            case SETTINGS_TYPE:
                view = mLayoutInflater.inflate(R.layout.item_spinner_dropdown_layout, parent, false);
                break;
            case ADD_LISTING_TYPE:
                view = mLayoutInflater.inflate(R.layout.item_dropdown_add_listing_layout, parent, false);
                break;
            default:
                view = mLayoutInflater.inflate(R.layout.item_spinner_dropdown_layout, parent, false);
                break;
        }
        return view;
    }
}
