package com.roomster.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.fragment.ProfileFragment;

import javax.inject.Inject;


public class ProfileActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.iv_back)
    ImageView mBackButton;

    @Bind(R.id.tv_title)
    TextView mTitleTv;

    @Inject
    Tracker mTracker;


    @Override
    protected int getMainContentLayout() {
        return R.layout.activity_profile;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        ButterKnife.bind(this);
        initView();
    }


    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName(getString(R.string.screen_name_my_profile));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    @OnClick(R.id.iv_back)
    void onToolbarBackClick() {
        onBackPressed();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_profile, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.edit_profile:
            openEditProfileActivity();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }


    private void initView() {
        setSupportActionBar(mToolbar);
        setUpActionBar();
        mTitleTv.setText(getString(R.string.my_profile_title));
        setProfileFragment();
    }


    private void setProfileFragment() {
        Fragment profileFragment = new ProfileFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.profile_fl_content, profileFragment).commit();
    }


    private void setUpActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeButtonEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(false);
        }
    }


    private void openEditProfileActivity() {
        Intent editProfileActivity = new Intent(this, EditProfileActivity.class);
        startActivity(editProfileActivity);
    }
}