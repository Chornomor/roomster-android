package com.roomster.rest.api;


import com.roomster.rest.model.SubscriptionGetModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import retrofit2.http.Query;


public interface SubscriptionApi {

    /**
     * Updates subscription status. Only deactivation is allowed (active = false).
     *
     * @param active
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<Object>
     */

    @PUT("Subscription")
    Call<Void> subscriptionPut(@Query("active") Boolean active, @Header("Authorization") String authorization);

    @GET("Subscription")
    Call<SubscriptionGetModel> subscriptionGet(@Header("Authorization") String authorization);
}
