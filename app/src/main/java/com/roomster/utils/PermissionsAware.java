package com.roomster.utils;


import android.support.annotation.NonNull;


/**
 * Created by Vassil Angelov
 */
public interface PermissionsAware {

    @NonNull
    PermissionsHelper getPermissionsHelper();
}
