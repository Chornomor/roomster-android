package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import com.roomster.enums.BookmarkTypeEnum;


public class BookmarkPostModel {

    @SerializedName("listing_id")
    private Long listingId = null;

    @SerializedName("service_type")
    private String serviceType = null;

    @SerializedName("bookmark_type")
    private BookmarkTypeEnum bookmarkType = null;


    public Long getListingId() {
        return listingId;
    }


    public void setListingId(Long listingId) {
        this.listingId = listingId;
    }


    public String getServiceType() {
        return serviceType;
    }


    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }


    public BookmarkTypeEnum getBookmarkType() {
        return bookmarkType;
    }


    public void setBookmarkType(BookmarkTypeEnum bookmarkType) {
        this.bookmarkType = bookmarkType;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class BookmarkPostModel {\n");

        sb.append("  listingId: ").append(listingId).append("\n");
        sb.append("  serviceType: ").append(serviceType).append("\n");
        sb.append("  bookmarkType: ").append(bookmarkType).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}