package com.roomster.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.roomster.R;
import com.roomster.activity.MessagesActivity;
import com.roomster.adapter.ConversationsListAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.IntentExtras;
import com.roomster.event.RestServiceEvents.ConversationRestServiceEvent;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.fragment.ConversationsListFragmentEvent;
import com.roomster.event.service.ConversationCounterEvent;
import com.roomster.manager.MeManager;
import com.roomster.rest.model.ConversationUserProfile;
import com.roomster.rest.model.ConversationView;
import com.roomster.rest.service.ConversationsRestService;
import com.roomster.rest.service.MeRestService;
import com.roomster.views.undo.UndoScnackbar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;


public class ConversationsListFragment extends Fragment {

    private ConversationsListAdapter mConversationsAdapter;
    CoordinatorLayout mParentLayout;
    @Bind(R.id.conversations_recycler_view)
    RecyclerView mConversationsRV;

    @Bind(R.id.empty_layout)
    RelativeLayout mEmptyLayout;

    @Bind(R.id.empty_result_header)
    TextView mEmptyResultHeaderTV;

    @Bind(R.id.empty_result_subtext)
    TextView mEmptyResultSubtextTV;

    @Inject
    ConversationsRestService mConversationsRestService;

    @Inject
    EventBus mEventBus;

    @Inject
    Context mContext;

    @Inject
    MeManager mMeManager;

    @Inject
    MeRestService mMeRestService;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
        mEventBus.register(this);
        initMe();
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mEventBus.unregister(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_conversations_list, container, false);
        mParentLayout = (CoordinatorLayout) view;
        ButterKnife.bind(this, view);
        OnConversationClickListener onItemClickListener = new OnConversationClickListener();
        mConversationsAdapter = new ConversationsListAdapter(mContext, null, onItemClickListener);
        mConversationsRV.setAdapter(mConversationsAdapter);
        mConversationsRV.setLayoutManager(new LinearLayoutManager(mContext));

        mEmptyResultHeaderTV.setText(getString(R.string.empty_messages_header));
        mEmptyResultSubtextTV.setText(getString(R.string.empty_messages_subtext));
        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        requestNewConversations();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ConversationCounterEvent.ConversationCounterUpdate event) {
        requestNewConversations();
    }

    String excludedID;
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ConversationRestServiceEvent.ConversationDeleted event) {
        Log.d("ChatDEBUG","hide ");
        final ConversationsListAdapter.ExtractModel extractModel = hideConversation(event.conversationId);
        excludedID = event.conversationId;
        if(extractModel == null){
            Toast.makeText(getActivity(), getString(R.string.messages_conversation_not_deleted_toast),
                    Toast.LENGTH_SHORT).show();
            return;
        }

        if (mConversationsAdapter.getItemCount() > 0) hideEmptyMessages();
        else showEmptyMessages();

        final String conversationToDelete = event.conversationId;
        new UndoScnackbar(mParentLayout, getString(R.string.messages_conversation_deleted_toast),
                new UndoScnackbar.SnackBarEvent() {
                    @Override
                    public void onUndo() {
                        hideEmptyMessages();
                        Log.d("ChatDEBUG","restore");
                        returnConversation(extractModel);
                    }

                    @Override
                    public void onDismiss() {
                        Log.d("ChatDEBUG","delete");
                        actualyDeleteConversation(conversationToDelete);
                    }
                })
                .make();

//        requestAllConversations();
    }

    public ConversationsListAdapter.ExtractModel hideConversation(String id){
        return mConversationsAdapter.extract(id);
    }

    public void actualyDeleteConversation(String id){
        mConversationsRestService.deleteConversation(id, new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                excludedID = null;
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                if(isAdded() && !isDetached()) {
                    Toast.makeText(getActivity(), getString(R.string.messages_conversation_not_deleted_toast),
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void returnConversation(ConversationsListAdapter.ExtractModel extractModel){
        excludedID = null;
        mConversationsAdapter.returnConversation(extractModel);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ConversationRestServiceEvent.ConversationUpdated event) {
        requestAllConversations();
    }


    private void initMe() {
        if (mMeManager.getMe() == null) {
            mMeRestService.requestMe();
        }
    }


    private void requestNewConversations() {
        if (mConversationsAdapter != null) {
            int itemsCount = mConversationsAdapter.getItemCount();
            if (itemsCount > 0) {
                String lastMessageId = mConversationsAdapter.getLastReadMessageId();
                mConversationsRestService
                  .getConversations(true, lastMessageId, new RestServiceListener<List<ConversationView>>() {

                      @Override
                      public void onSuccess(List<ConversationView> result) {
                          turnicateHidden(result);
                          if (result != null && !result.isEmpty()) {
                              Log.d("ChatDEBUG","add items");
                              for (ConversationView cv : result)
                                Log.d("ChatDEBUG","id " + cv.getConversationId());
                              mConversationsAdapter.addItems(result);
                              hideEmptyMessages();
                          } else if(mConversationsAdapter.getItemCount() == 0){
                              showEmptyMessages();
                          }
                          checkForUnreadMessages(mConversationsAdapter.getItems());
                      }
                      @Override
                      public void onFailure(int code, Throwable throwable, String msg) {
                          Activity activity = getActivity();
                          if (activity != null) {
                              Toast.makeText(activity, getString(R.string.error_loading_conversations), Toast.LENGTH_SHORT)
                                .show();
                          }
                      }
                  });
            } else {
                Log.d("ChatDEBUG","chat debug load all");
                requestAllConversations();
            }
        }
    }
    public List<ConversationView> turnicateHidden(List<ConversationView> list){
        if(list == null || list.isEmpty() || excludedID == null)
            return list;
        ConversationView toRemove = null;
        for(ConversationView cv : list)
            if (cv.getConversationId().equals(excludedID)){
                toRemove = cv;
                break;
            }
        if(toRemove != null){
            list.remove(toRemove);
        }
        return list;
    }

    private void requestAllConversations() {
        mConversationsRestService.getAllConversations(new RestServiceListener<List<ConversationView>>() {

            @Override
            public void onSuccess(List<ConversationView> result) {
                turnicateHidden(result);
                if (result != null && !result.isEmpty()) {
                    mConversationsAdapter.updateAllItems(result);
                    hideEmptyMessages();
                } else {
                    showEmptyMessages();
                }
                checkForUnreadMessages(result);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                showEmptyMessages();
                Activity activity = getActivity();
                if (activity != null) {
                    Toast.makeText(activity, getString(R.string.error_loading_conversations), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void checkForUnreadMessages(List<ConversationView> conversations) {
        for (ConversationView conversation : conversations) {
            if (conversation.getCountNew() > 0) {
                mEventBus.post(new ConversationsListFragmentEvent.UnreadMessages());
                return;
            }
        }

        mEventBus.post(new ConversationsListFragmentEvent.NoUnreadMessages());
    }


    private void showEmptyMessages() {
        mConversationsRV.setVisibility(View.GONE);
        mEmptyLayout.setVisibility(View.VISIBLE);
    }


    private void hideEmptyMessages() {
        mConversationsRV.setVisibility(View.VISIBLE);
        mEmptyLayout.setVisibility(View.GONE);
    }


    public class OnConversationClickListener implements ConversationsListAdapter.OnConversationClickListener {

        @Override
        public void onClick(ConversationView model) {
            List<ConversationUserProfile> users = model.getUsers();
            if (users != null && !users.isEmpty() && mMeManager.getMe() != null) {
                Intent intent = new Intent(mContext, MessagesActivity.class);
                intent.putExtra(IntentExtras.CONVERSATION_ID_EXTRA, model.getConversationId());
                intent.putExtra(IntentExtras.USER_ID_EXTRA, mMeManager.getMe().getId());
                intent.putExtra(IntentExtras.OTHER_USER_ID_EXTRA, users.get(0).getUserId());
                startActivity(intent);
            } else {
                Activity activity = getActivity();
                if (activity != null) {
                    Toast.makeText(activity, getString(R.string.error_opening_conversation), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}