package com.roomster.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.ListingType;
import com.roomster.manager.DiscoveryPrefsManager;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by "Michael Katkov" on 12/2/2015.
 */
public class AddNewListingFragment extends Fragment {

    private static final int OFFERING_STATE = 1;
    private static final int FINDING_STATE  = 2;

    @Bind(R.id.al_rg_rg_rent_or_find)
    RadioGroup mRentOrFindRG;

    @Bind(R.id.al_rb_offering)
    RadioButton mOfferingRb;

    @Bind(R.id.al_rb_finding)
    RadioButton mFindingRb;

    @Bind(R.id.al_room)
    TextView mRoom;

    @Bind(R.id.al_place)
    TextView mPlace;

    @Inject
    DiscoveryPrefsManager mDPrefManager;

    private int mState = FINDING_STATE;

    private AddNewListingListener mAddNewListingListener;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mAddNewListingListener = (AddNewListingListener) getActivity();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_new_listing, container, false);
        RoomsterApplication.getRoomsterComponent().inject(this);
        ButterKnife.bind(this, view);
        setUpView();
        return view;
    }


    @OnClick({R.id.al_rb_offering, R.id.al_rb_finding})
    public void onOfferingOrFinding() {
        if (mOfferingRb.isChecked()) {
            setOfferingState();
        } else {
            setFindingState();
        }
    }


    @OnClick(R.id.al_place)
    public void onPlace() {
        mDPrefManager.storeEntirePlace();
        switch (mState) {
            case OFFERING_STATE:
                mAddNewListingListener.onNewListingItemClick(ListingType.OFFERING_ENTIRE_PLACE_TYPE);
                break;
            case FINDING_STATE:
                mAddNewListingListener.onNewListingItemClick(ListingType.FINDING_ENTIRE_PLACE_TYPE);
                break;
        }
    }


    @OnClick(R.id.al_room)
    public void onRoom() {
        mDPrefManager.storePrivateRoom();
        switch (mState) {
            case OFFERING_STATE:
                mAddNewListingListener.onNewListingItemClick(ListingType.OFFERING_ROOM_TYPE);
                break;
            case FINDING_STATE:
                mAddNewListingListener.onNewListingItemClick(ListingType.FINDING_ROOM_TYPE);
                break;
        }
    }

    public interface AddNewListingListener {

        void onNewListingItemClick(int listingType);
    }

    private void setUpView() {
        String rentingOrFinding = mDPrefManager.getRentingOrFinding();
        if (rentingOrFinding != null) {
            switch (rentingOrFinding) {
                case DiscoveryPrefsManager.RENTING_A_PLACE:
                    mOfferingRb.setChecked(true);
                    setOfferingState();
                    break;
                case DiscoveryPrefsManager.FINDING_A_PLACE:
                    mFindingRb.setChecked(true);
                    setFindingState();
                    break;
            }
        } else {
            mFindingRb.setChecked(true);
            setFindingState();
        }
    }


    private void setOfferingState() {
        mState = OFFERING_STATE;
        mRoom.setText(getString(R.string.al_offering_room));
        mPlace.setText(getString(R.string.al_offering_apartment));
        mDPrefManager.storeRentingPlace();
    }


    private void setFindingState() {
        mState = FINDING_STATE;
        mRoom.setText(getString(R.string.al_looking_for_room));
        mPlace.setText(getString(R.string.al_looking_for_apartment));
        mDPrefManager.storeFindingPlace();
    }
}
