package com.roomster.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.roomster.R;
import com.roomster.activity.DetailedViewActivity;
import com.roomster.adapter.ListingListAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.BookmarksRestServiceEvent;
import com.roomster.listener.BookmarksCallbackListener;
import com.roomster.manager.BookmarkManager;
import com.roomster.manager.DiscoveryPrefsManager;
import com.roomster.manager.SearchManager;
import com.roomster.rest.model.ResultItemModel;
import com.roomster.rest.service.BookmarksRestService;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;
import java.util.List;


/**
 * Created by Bogoi.Bogdanov on 11/30/2015.
 */
public class ListingListViewBookmarkFragment extends Fragment {

    protected static final int PAGE_SIZE = 20;

    public ListingListAdapter mListingListAdapter;

    private LinearLayoutManager    mLayoutManager;
    private OnListingClickListener mOnListingClickListener;

    public BookmarksCallbackListener mCallback;

    @Bind(R.id.listing_recycler_view)
    RecyclerView mListingRV;

    @Bind(R.id.empty_layout)
    RelativeLayout mEmptyLayout;

    @Bind(R.id.empty_result_header)
    TextView mEmptyResultHeaderTV;

    @Bind(R.id.empty_result_subtext)
    TextView mEmptyResultSubtextTV;

    @Bind(R.id.empty_layout_button)
    TextView mEmptyLayoutButtonTV;

    @Inject
    Context mContext;

    @Inject
    DiscoveryPrefsManager mDiscoveryPrefsManager;

    @Inject
    BookmarksRestService mBookmarksService;

    @Inject
    SearchManager mSearchManager;

    @Inject
    BookmarkManager bookmarkManager;

    public void setListener(BookmarksCallbackListener mCallback) {
        this.mCallback = mCallback;
    }


    public interface OnListingClickListener {

        void onClick(int index, List<ResultItemModel> models);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_listing_list_view, container, false);
        ButterKnife.bind(this, view);
        initView();

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        initListings();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(BookmarksRestServiceEvent.AddBookmarkSuccess event) {
        onBookmarked(event);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(BookmarksRestServiceEvent.AddBookmarkFailure event) {
        if (mContext != null) {
            Toast.makeText(mContext, getString(R.string.error_add_bookmark), Toast.LENGTH_SHORT).show();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(BookmarksRestServiceEvent.RemoveBookmarkSuccess event) {
        onUnbookmarked(event);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(BookmarksRestServiceEvent.RemoveBookmarkFailure event) {
        if (mContext != null) {
            Toast.makeText(mContext, getString(R.string.error_delete_bookmark), Toast.LENGTH_SHORT).show();
        }
    }


    public void setOnListingClickListener(OnListingClickListener listingClickListener) {
        mOnListingClickListener = listingClickListener;
    }


    protected boolean hasMoreBookmarks() {
        return false;
    }


    protected List<ResultItemModel> getBookmarkListings() {
        return bookmarkManager.getResultModel() == null ? null : bookmarkManager.getResultModel().getItems();
    }


    protected void onRequestBookmarks() {
    }


    protected void initListings() {
        List<ResultItemModel> initialBookmark = getBookmarkListings();
        if (hasListings(initialBookmark)) {
            hideEmptyResult();
            mListingListAdapter.updateItems(initialBookmark);
        } else if (hasMoreBookmarks()) {
            onRequestBookmarks();
        } else {
            showEmptyResult();
        }
    }


    protected final void onNewListings(List<ResultItemModel> listings) {
        if (hasListings(listings)) {
            hideEmptyResult();
            mListingListAdapter.updateItems(listings);
        } else {
            showEmptyResult();
        }
    }


    protected void showEmptyResult() {
        mListingRV.setVisibility(View.GONE);
        mEmptyLayout.setVisibility(View.VISIBLE);
        if (mCallback != null) mCallback.onBookmarksCountChanged(0);
    }


    protected void hideEmptyResult() {
        mListingRV.setVisibility(View.VISIBLE);
        mEmptyLayout.setVisibility(View.GONE);
    }


    protected void onInitEmptyLayoutViews() {

    }


    protected void onBookmarked(BookmarksRestServiceEvent.AddBookmarkSuccess event) {
        if (mListingListAdapter != null) {
            mListingListAdapter.setBookmarked(event.listingId);
        }
    }


    protected void onUnbookmarked(BookmarksRestServiceEvent.RemoveBookmarkSuccess event) {
        if (mListingListAdapter != null) {
            mListingListAdapter.setUnbookmarked(event.listingId);
        }
    }


    private void initView() {
        boolean shouldShowProfileImage = true;
        String rentingOrFinding = mDiscoveryPrefsManager.getRentingOrFinding();
        if (rentingOrFinding != null && rentingOrFinding.equals(DiscoveryPrefsManager.RENTING_A_PLACE)) {
            shouldShowProfileImage = false;
        }

        mListingListAdapter = new ListingListAdapter(getActivity(), null, shouldShowProfileImage);
        mListingListAdapter.setOnListingClickListener(new ListingListAdapter.OnListingClickListener() {

            @Override
            public void onClick(int index, ResultItemModel model) {
                if (mOnListingClickListener != null) {
                    mOnListingClickListener.onClick(index, mListingListAdapter.getItems());
                } else {
                    startDetailedViewActivity(model.getListing().getListingId());
                }
            }
        });

        mListingListAdapter.setOnBookmarkClickListener(new ListingListAdapter.OnBookmarkClickListener() {

            @Override
            public void onClick(int index, ResultItemModel model) {
                mBookmarksService.toggleBookmark(model);
            }
        });

        mListingRV.setAdapter(mListingListAdapter);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mListingRV.setLayoutManager(mLayoutManager);
        mListingRV.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int lastVisiblePosition = mLayoutManager.findLastCompletelyVisibleItemPosition();
                int loadedItemsCount = mListingListAdapter.getItemCount();
                if (mCallback != null) {
                    mCallback.onBookmarksCountChanged(loadedItemsCount);
                }
                if (loadedItemsCount < lastVisiblePosition + 3) {
                    onRequestBookmarks();
                }
            }
        });
        onInitEmptyLayoutViews();
    }


    private boolean hasListings(List<ResultItemModel> listingsList) {
        return listingsList != null && !listingsList.isEmpty();
    }


    private void startDetailedViewActivity(long listingId) {
        Intent intent = new Intent(getActivity(), DetailedViewActivity.class);
        intent.putExtra(DetailedViewActivity.LISTING_ID, listingId);
        intent.putExtra(DetailedViewActivity.IS_BOOKMARK_TYPE, true);
        getActivity().startActivity(intent);
    }
}
