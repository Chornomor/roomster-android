package com.roomster.di.module;


import com.google.gson.Gson;

import com.roomster.cache.sharedpreferences.SharedPreferencesFiles;
import com.roomster.manager.TokenManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class TokenModule {

    @Provides
    @Singleton
    public TokenManager provideTokenManager(SharedPreferencesFiles sharedPreferencesFiles) {
        String tokenManagerJson = sharedPreferencesFiles.getUserSharedPreferences()
          .getString(TokenManager.TOKEN_MANAGER_SP_KEY, null);
        if (tokenManagerJson != null) {
            return new Gson().fromJson(tokenManagerJson, TokenManager.class);
        } else {
            return new TokenManager();
        }
    }
}
