package com.roomster.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.IntentExtras;
import com.roomster.constants.ListingType;
import com.roomster.dialog.LoadingDialog;
import com.roomster.event.RestServiceEvents.ListingsRestServiceEvent;
import com.roomster.event.RestServiceEvents.MeRestServiceEvent;
import com.roomster.event.RestServiceEvents.UserListingsRestServiceEvent;
import com.roomster.event.application.ApplicationEvent;
import com.roomster.fragment.AddListingDetailedFragment;
import com.roomster.listener.AddListing;
import com.roomster.manager.MeManager;
import com.roomster.manager.SearchManager;
import com.roomster.rest.service.MeRestService;
import com.roomster.rest.service.UserListingsRestService;
import com.roomster.utils.LogUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;


/**
 * Created by michaelkatkov on 12/10/15.
 */
public class AddNewDetailedListingActivity extends BaseActivity implements AddListing {

    public static final String FROM_MEGAPHONE_ACTIVITY = "from_megaphone_activity";
    public static final String TO_MEGAPHONE_ACTIVITY_TYPE = "to_megaphone_activity_type";

    private AddListingDetailedFragment mAddListingDetailedFragment;
    private boolean mIsMyProfileLoaded = false;
    private boolean mIsMyListingsLoaded = false;
    private boolean mWithBackArrow;
    private boolean mFinishOnContinue;
    private boolean mFromMegaphone;
    private int mListingType;

    @Bind(R.id.add_listing_toolbar)
    Toolbar mToolbar;

    @Bind(R.id.add_listing_back)
    ImageView mBackButton;

    @Bind(R.id.add_new_listing_second_line)
    TextView mSecondLineTv;

    @Bind(R.id.add_new_listing_save)
    TextView mAddNewListingSaveBtn;

    @Inject
    MeManager mMeManager;

    @Inject
    SearchManager mSearchManager;

    @Inject
    MeRestService mMeRestService;

    @Inject
    UserListingsRestService mUserListingRestService;

    @Inject
    EventBus mEventBus;

    LoadingDialog loadingDialog;


    @Override
    protected int getMainContentLayout() {
        return R.layout.activity_add_listing_detailed;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        ButterKnife.bind(this);
        readIntent();
        initView();
    }


    @Override
    public void onBackPressed() {
        backToAddNewListingActivity();
    }


    @OnClick(R.id.add_listing_back)
    void onToolbarBackClick() {
        onBackPressed();
    }


    @OnClick(R.id.add_new_listing_save)
    void onSave() {
        mAddListingDetailedFragment.saveListing(this);
    }


    private void showLoading() {
        if (loadingDialog == null) {
            loadingDialog = new LoadingDialog(this);
            loadingDialog.show();
        } else {
            loadingDialog.show();
        }
    }


    private void hideLoading() {
        if (loadingDialog != null) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }

    @Override
    public void onAddListing() {
        showLoading();
    }

    public TextView getAddNewListingSaveBtn() {
        return mAddNewListingSaveBtn;
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ListingsRestServiceEvent.AddListingSuccess event) {
        mUserListingRestService.getUserListings(mMeManager.getMe().getId(), true);
        if (mFromMegaphone) {
            Intent intent = new Intent(AddNewDetailedListingActivity.this, MegaphoneActivity.class);
            intent.putExtra(TO_MEGAPHONE_ACTIVITY_TYPE, mListingType);
            startActivity(intent);
            finish();
        } else {
            mUserListingRestService.getUserListings(mMeManager.getMe().getId(), true);
            mMeRestService.requestMe();
        }

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ListingsRestServiceEvent.AddListingFailed event) {
        Toast.makeText(this, getString(R.string.alert_listing_wasnt_created), Toast.LENGTH_SHORT).show();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(UserListingsRestServiceEvent.GetListingsSuccess event) {
        mIsMyProfileLoaded = true;
        checkToFinish();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(UserListingsRestServiceEvent.GetListingsFailed event) {
        Toast.makeText(this, getString(R.string.unable_to_get_listings_info), Toast.LENGTH_SHORT).show();
        mIsMyProfileLoaded = true;
        checkToFinish();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(MeRestServiceEvent.GetMeSuccess event) {
        mIsMyListingsLoaded = true;
        checkToFinish();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(MeRestServiceEvent.GetMeFailure event) {
        Toast.makeText(this, getString(R.string.unable_to_get_user_info), Toast.LENGTH_SHORT).show();
        mIsMyListingsLoaded = true;
        checkToFinish();
    }


    private void readIntent() {
        mListingType = getIntent()
                .getIntExtra(AddListingDetailedFragment.LISTING_TYPE, ListingType.OFFERING_ENTIRE_PLACE_TYPE);
        mWithBackArrow = getIntent().getBooleanExtra(IntentExtras.WITH_BACK_ARROW, false);
        mFinishOnContinue = getIntent().getBooleanExtra(IntentExtras.FINISH_ON_CONTINUE, false);
        mFromMegaphone = getIntent().getExtras().getBoolean(FROM_MEGAPHONE_ACTIVITY, false);
    }


    private void initView() {
        setSupportActionBar(mToolbar);
        setTitleBar(mListingType);
        mAddListingDetailedFragment = AddListingDetailedFragment.newInstance(mListingType);
        setContent(mAddListingDetailedFragment);
    }


    private void setContent(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.add_new_listing_content, fragment).commit();
    }


    private void setTitleBar(int listingType) {
        switch (listingType) {
            case ListingType.OFFERING_ENTIRE_PLACE_TYPE:
                mSecondLineTv.setText(getString(R.string.al_offering_apartment));
                break;
            case ListingType.OFFERING_ROOM_TYPE:
                mSecondLineTv.setText(getString(R.string.al_offering_room));
                break;
            case ListingType.FINDING_ENTIRE_PLACE_TYPE:
                mSecondLineTv.setText(getString(R.string.al_looking_for_apartment));
                break;
            case ListingType.FINDING_ROOM_TYPE:
                mSecondLineTv.setText(getString(R.string.el_looking_for_room));
                break;
        }
    }


    private void backToAddNewListingActivity() {
        Intent intent = null;
        if (!mFromMegaphone) {
            intent = new Intent(this, AddNewListingActivity.class);
            intent.putExtra(IntentExtras.WITH_BACK_ARROW, mWithBackArrow);
            startActivity(intent);
        } else {
            intent = new Intent(this, MegaphoneActivity.class);
            startActivity(intent);
        }
        finish();


    }


    private void checkToFinish() {
        if (mIsMyListingsLoaded && mIsMyProfileLoaded) {
            mSearchManager.clearResultModel();
            if (mFinishOnContinue) {
                finish();
            } else {
                openMainActivity();
            }
        }
    }


    private void openMainActivity() {
        if (!isFinishing()) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            hideLoading();
            startActivity(intent);
            finish();
        }
    }

}
