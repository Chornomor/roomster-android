package com.roomster.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class SubscriptionGetModel {

    @SerializedName("is_active")
    private Boolean isActive;

    @SerializedName("full_access_expires")
    private Date fullAccessExpires;

    @SerializedName("next_billing_date")
    private Date nextBillingDate;

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Date getFullAccessExpires() {
        return fullAccessExpires;
    }

    public void setFullAccessExpires(Date fullAccessExpires) {
        this.fullAccessExpires = fullAccessExpires;
    }

    public Date getNextBillingDate() {
        return nextBillingDate;
    }

    public void setNextBillingDate(Date nextBillingDate) {
        this.nextBillingDate = nextBillingDate;
    }
}
