package com.roomster.enums;

public enum StatusEnum {
    Default, New, Read, Replied
}
