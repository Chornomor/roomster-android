package com.roomster.constants;


public class Permission {

    public static final int PERMISSION_REQUEST_CAMERA   = 1;
    public static final int PERMISSION_REQUEST_STORAGE  = 2;
    public static final int PERMISSION_REQUEST_LOCATION = 3;
}
