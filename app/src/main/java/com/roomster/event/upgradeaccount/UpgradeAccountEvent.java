package com.roomster.event.upgradeaccount;


import com.roomster.event.Event;
import com.roomster.model.ProductItem;


public class UpgradeAccountEvent extends Event {

    public static class PackageSelected {

        public ProductItem selectedPackage;


        public PackageSelected(ProductItem productItem) {
            this.selectedPackage = productItem;
        }
    }


    public static class SuccessContinue {

    }
}
