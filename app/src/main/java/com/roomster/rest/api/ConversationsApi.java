package com.roomster.rest.api;


import com.roomster.rest.model.ConversationPutViewModel;
import com.roomster.rest.model.ConversationView;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ConversationsApi {

    /**
     * Get conversations is_new_delta in combination with last_message are used to load more conversations&lt;br /&gt;
     * \n is_new_delta = true + last_message load new conversations that came in after the freshest conversation
     * (last_message) was loaded&lt;br /&gt; \n is_new_delta = false + last_message load conversations that are beyond
     * the page size&lt;br /&gt; \n q - query to filter by users&#39; names
     *
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @param label
     *   inbox|archive
     * @param pageSize
     *   0 - unlimited
     * @param isNewDelta
     *   true|false
     * @param lastMessage
     *   message id
     * @param q
     *   search query (queries user names)
     * @return Call<List<ConversationView>>
     */

    @GET("Conversations")
    Call<List<ConversationView>> conversationsGet(@Header("Authorization") String authorization, @Query("label") String label,
                                                  @Query("page_size") Integer pageSize, @Query("is_new_delta") Boolean isNewDelta,
                                                  @Query("last_message") String lastMessage, @Query("q") String q);

    /**
     * Update conversation
     *
     * @param model
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<Object>
     */

    @PUT("Conversations")
    Call<String> conversationsPut(@Body ConversationPutViewModel model, @Header("Authorization") String authorization);

    /**
     * Delete conversation
     *
     * @param conversation
     *   conversation id
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<Object>
     */

    @DELETE("Conversations")
    Call<String> conversationsDelete(@Query("conversation") String conversation, @Header("Authorization") String authorization);

    /**
     * Get conversations is_new_delta in combination with last_message are used to load more conversations&lt;br /&gt;
     * \n is_new_delta = true + last_message load new conversations that came in after the freshest conversation
     * (last_message) was loaded&lt;br /&gt; \n is_new_delta = false + last_message load conversations that are beyond
     * the page size&lt;br /&gt; \n q - query to filter by users&#39; names
     *
     * @param label
     *   inbox|archive
     * @param pageSize
     *   0 - unlimited
     * @param isNewDelta
     *   true|false
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @param lastMessage
     *   message id
     * @param q
     *   search query (queries user names)
     * @return Call<List<ConversationView>>
     */

    @GET("conversations/{label}/{page_size}/{is_new_delta}")
    Call<List<ConversationView>> conversationsGet_1(@Path("label") String label, @Path("page_size") Integer pageSize,
                                                    @Path("is_new_delta") Boolean isNewDelta,
                                                    @Header("Authorization") String authorization,
                                                    @Query("last_message") String lastMessage, @Query("q") String q);
}
