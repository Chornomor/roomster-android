package com.roomster.rest.api;


import com.roomster.rest.model.UserGetViewModel;
import com.roomster.rest.model.UserPutViewModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import retrofit2.http.Path;


public interface UsersApi {

    /**
     * (Un)Block and/or report a user All parameters are optional. Only pass what you need to update.
     *
     * @param model
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<Object>
     */

    @PUT("Users")
    Call<String> usersPut(@Body UserPutViewModel model, @Header("Authorization") String authorization);

    /**
     * Get user info
     *
     * @param id
     *   User id
     * @return Call<UserGetViewModel>
     */

    @GET("Users/{id}")
    Call<UserGetViewModel> usersGet(@Path("id") Long id);
}
