package com.roomster.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.roomster.R;
import com.roomster.activity.AddNewListingActivity;
import com.roomster.activity.EditMyListingActivity;
import com.roomster.activity.MyListingActivity;
import com.roomster.activity.UserListingsListActivity;
import com.roomster.adapter.MyListingListAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.IntentExtras;
import com.roomster.event.RestServiceEvents.MeRestServiceEvent;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.RestServiceEvents.SearchRestServiceEvent;
import com.roomster.event.RestServiceEvents.UserListingsRestServiceEvent;
import com.roomster.manager.MeManager;
import com.roomster.rest.model.ListingGetViewModel;
import com.roomster.rest.service.ListingsRestService;
import com.roomster.rest.service.MeRestService;
import com.roomster.rest.service.UserListingsRestService;
import com.roomster.utils.Data;
import com.roomster.utils.LogUtil;
import com.roomster.utils.ShareUtil;
import com.roomster.views.undo.ItemExtractor;
import com.roomster.views.undo.UndoScnackbar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MyListingListFragment extends Fragment implements MyListingListAdapter.ItemClickListener {

    private static final int REQ_GALLERY = 1;

    private static final int LISTING_ITEM_REQUEST_CODE = 0;

    public static final String LISTING_TO_SHARE_SAVED_STATE_KEY = "com.roomster.fragment.LISTING_TO_SHARE_SAVED_STATE_KEY";

    private MyListingListAdapter mListingListAdapter;
    private CoordinatorLayout mParentView;
    private long deletedListing = -1;
    private boolean isNewListingActivityStarted = false;

    @Bind(R.id.listing_recycler_view)
    RecyclerView mListingRV;

    @Inject
    EventBus mEventBus;

    @Inject
    MeManager mMeManager;

    @Inject
    MeRestService mMeRestService;

    @Inject
    UserListingsRestService mUserListingRestService;
    @Inject
    ListingsRestService mListingRestService;
    @Inject
    Context mContext;

    TextView mTitle;
    ItemExtractor managerExtractor;

    private ListingGetViewModel mListingToShare;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!mEventBus.isRegistered(this)) {
            mEventBus.register(this);
        }
        if (savedInstanceState != null) {
            mListingToShare = (ListingGetViewModel) savedInstanceState.getSerializable(LISTING_TO_SHARE_SAVED_STATE_KEY);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RoomsterApplication.getRoomsterComponent().inject(this);

    }

    public void setTitle(TextView title) {
        this.mTitle = title;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listing_list_view, container, false);
        mParentView = (CoordinatorLayout) view;
        ButterKnife.bind(this, view);

        checkData();
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mEventBus.isRegistered(this)) {
            mEventBus.unregister(this);
        }
    }


    @Override
    public void onItemClick(int index, ListingGetViewModel listingGetViewModel) {
        if (getActivity() != null) {
            Intent intent = new Intent(getActivity(), MyListingActivity.class);
            intent.putExtra(MyListingActivity.LISTING_ID, listingGetViewModel.getListingId());
            intent.putExtra(MyListingActivity.LISTING_INDEX, index);
            startActivityForResult(intent, LISTING_ITEM_REQUEST_CODE);
        }
    }


    @Override
    public void onItemClickEdit(int listingIndex, ListingGetViewModel listingGetViewModel) {
        if (getActivity() != null) {
            Intent editListingIntent = new Intent(getActivity(), EditMyListingActivity.class);
            editListingIntent.putExtra(EditMyListingActivity.LISTING_ID, listingGetViewModel.getListingId());
            editListingIntent.putExtra(EditMyListingActivity.LISTING_INDEX, listingIndex);
            startActivityForResult(editListingIntent, LISTING_ITEM_REQUEST_CODE);
        }
    }

    @Override
    public void onItemClickOptions(int listingIndex, final ListingGetViewModel listingGetViewModel, View optionsView) {
        if (getActivity() != null) {
            PopupMenu popup = new PopupMenu(getActivity(), optionsView);
            popup.getMenuInflater().inflate(R.menu.my_litings, popup.getMenu());

            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.share_listing:
                            mListingToShare = listingGetViewModel;
                            checkWriteStoragePermissionBeforeShare();
                            return true;
                        case R.id.delete_listing:
                            showDeleteListingDialog(listingGetViewModel.getListingId());
                            return true;
                        case R.id.add_new_listing:
                            startAddNewListingActivity(true);
                            return true;
                        default:
                            return false;
                    }
                }
            });

            popup.show();
        }
    }


    private void checkWriteStoragePermissionBeforeShare() {
        if (getActivity() != null && !getActivity().isFinishing()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                int permission = getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (permission == PackageManager.PERMISSION_GRANTED) {
                    shareListing(mListingToShare);
                } else {
                    requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQ_GALLERY);
                }
            } else {
                shareListing(mListingToShare);
            }
        }
    }


    private void shareListing(final ListingGetViewModel listingGetViewModel) {
        if (getActivity() != null) {
            Target<Bitmap> loadTarget = new SimpleTarget<Bitmap>() {

                @Override
                public void onResourceReady(Bitmap bitmap, GlideAnimation glideAnimation) {
                    String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bitmap, "bitmap", null);
                    if (path != null) {
                        Uri uri = Uri.parse(path);
                        ShareUtil.shareListing(getActivity(), listingGetViewModel, uri);
                    } else {
                        Toast.makeText(getActivity(), R.string.my_listing_share_failed, Toast.LENGTH_SHORT).show();
                    }
                }
            };
            Glide.with(this).load(listingGetViewModel.getPhotos().get(0).getPath()).asBitmap().into(loadTarget);
        }
    }


    private void showDeleteListingDialog(final long listingId) {
        if (getActivity() != null) {
            new AlertDialog.Builder(getActivity(), R.style.AppTheme_AlertDialog).setMessage(R.string.delete_listing_dialog_content)
                    .setTitle(R.string.delete_listing)
                    .setPositiveButton(R.string.delete_listing_dialog_delete_button, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mEventBus.post(new UserListingsRestServiceEvent.ListingDeleted(listingId));
                        }
                    }).setNegativeButton(R.string.dialog_cancel_button, null).show();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtil.logE("onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK)
            return;
        if (requestCode != LISTING_ITEM_REQUEST_CODE)
            return;
        deletedListing = data.getLongExtra(UndoScnackbar.KEY_DELTED_ITEM, -1);
        checkData();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(SearchRestServiceEvent.SearchFailure event) {
        Toast.makeText(mContext, getString(R.string.unable_to_get_search_info), Toast.LENGTH_SHORT).show();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(MeRestServiceEvent.GetMeSuccess event) {
        LogUtil.logE("GetMeSuccess");
        getOrrequestMyListings();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(MeRestServiceEvent.GetMeFailure event) {
        Toast.makeText(mContext, getString(R.string.unable_to_get_user_info), Toast.LENGTH_SHORT).show();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(UserListingsRestServiceEvent.GetListingsSuccess event) {
        LogUtil.logE("GetListingsSuccess");
        if (mMeManager.getMyListings() == null || mMeManager.getMyListings().isEmpty()) {
            if (!isNewListingActivityStarted) {          /*TODO fix multi call to api for stop geting 4 times result in "GetListingsSuccess" if all listing removed */
                isNewListingActivityStarted = true;
                startAddNewListingActivity(false);
            }
        } else {
            initView();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(UserListingsRestServiceEvent.GetListingsFailed event) {
        Toast.makeText(mContext, getString(R.string.unable_to_get_my_listing_info), Toast.LENGTH_SHORT).show();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(UserListingsRestServiceEvent.ListingDeleted event) {
        deletedListing = event.id;
        final ItemExtractor extractor = hideListing(event.id);
        new UndoScnackbar(mParentView, getString(R.string.delete_listing_success), new UndoScnackbar.SnackBarEvent() {
            @Override
            public void onUndo() {
                LogUtil.logE("onUndo");
                deletedListing = -1;
                setTitleBarTotalListing(1);
                restoreListing(extractor);
            }

            @Override
            public void onDismiss() {
                LogUtil.logE("onDismiss");
                actualyDeleteListing(deletedListing);
                deletedListing = -1;
            }
        }).make();

    }


    public ItemExtractor hideListing(long id) {
        return mListingListAdapter.extract(id);
    }

    public void actualyDeleteListing(long id) {
        mListingRestService.deleteListing(id, new RestServiceListener<Object>() {

            @Override
            public void onSuccess(Object result) {
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                if (isFragmentUIActive()) {
                    Toast.makeText(getActivity(), R.string.delete_listing_failed,
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    public boolean isFragmentUIActive() {
        return isAdded() && !isDetached() && !isRemoving();
    }


    public void restoreListing(ItemExtractor itemExtractor) {
        mListingListAdapter.restoreListing(itemExtractor);
    }

    private void initView() {
        LogUtil.logE("initView");
        checkOnRemoved();
        if (mListingListAdapter == null) {
            mListingListAdapter = new MyListingListAdapter(getActivity(), mMeManager.getMyListings(), mMeManager.getMe());
            mListingListAdapter.setItemClickListener(this);
            mListingListAdapter.setFooterClickListener(onFooterClick);
            mListingRV.setAdapter(mListingListAdapter);
            mListingRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        } else {
            mListingListAdapter.setItems(mMeManager.getMyListings());
            mListingListAdapter.setModel(mMeManager.getMe());
            mListingListAdapter.notifyDataSetChanged();
        }
        setTitleBarTotalListing(0);
    }

    private void checkOnRemoved() {
        if (deletedListing == -1)
            return;
        List<ListingGetViewModel> listings = mMeManager.getMyListings();
        ListingGetViewModel target = null;
        for (ListingGetViewModel model : listings) {
            if (model.getListingId().equals(deletedListing)) {
                target = model;
                break;
            }
        }
        if (target == null)
            return;
        managerExtractor = new ItemExtractor(listings.indexOf(target), target);
        listings.remove(target);
    }


    private View.OnClickListener onFooterClick = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            startAddNewListingActivity(true);
            isNewListingActivityStarted = false;
        }
    };


    private void getOrRequestMe() {
        if (mMeManager.getMe() != null) {
            getOrrequestMyListings();
        }
        if (deletedListing != -1)
            mMeRestService.requestMe();

    }


    private void getOrrequestMyListings() {
        if (mMeManager.getMyListings() != null) {
            initView();
        }
        mUserListingRestService.getUserListings(mMeManager.getMe().getId(), true);
    }


    private void checkData() {
        LogUtil.logE("checkData");
        if (mMeManager.getMyListings() != null && mMeManager.getMe() != null) {
            initView();
        }
        getOrRequestMe();

    }


    private void startAddNewListingActivity(boolean shouldShowBackArrow) {
        Intent addNewListingIntent = new Intent(getContext(), AddNewListingActivity.class);
        addNewListingIntent.putExtra(IntentExtras.WITH_BACK_ARROW, shouldShowBackArrow);
        startActivity(addNewListingIntent);
    }


    private void setTitleBarTotalListing(int modyfier) {
        if (mMeManager.getMyListings() != null && mTitle != null) {
            String totalListingsAdd = "(" + (mMeManager.getMyListings().size() + modyfier) + ")";
            String stringToFill = getString(R.string.my_listings) + " " + totalListingsAdd;
            mTitle.setText(stringToFill);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQ_GALLERY:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    shareListing(mListingToShare);
                } else {
                    LogUtil.logE("WRITE_EXTERNAL_STORAGE permission denied");
                }
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(LISTING_TO_SHARE_SAVED_STATE_KEY, mListingToShare);
    }
}
