package com.roomster.rest.api;


import com.roomster.rest.model.FacebookPage;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Query;


public interface LikesApi {

    /**
     * Get user likes or mutual user likes
     *
     * @param userId
     *   User Id
     * @param userIdCompare
     *   User Id for mutual likes. Optional.
     * @return Call<List<FacebookPage>>
     */

    @GET("Likes")
    Call<List<FacebookPage>> likesGet(@Query("user_id") Long userId, @Query("user_id_compare") Long userIdCompare);

    /**
     * Update user likes from Facebook Calls FB graph API on behalf of the user to get all pages that user liked.
     *
     * @param userId
     *   User Id
     * @return Call<Object>
     */

    @PUT("Likes")
    Call<Object> likesPut(@Query("user_id") Long userId);
}
