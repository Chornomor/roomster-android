package com.roomster.utils;


import com.roomster.R;

import java.util.HashMap;
import java.util.Map;


public class PetsUtil {

    public static final Long CATS            = (long) 1;
    public static final Long DOGS            = (long) 2;
    public static final Long SMALL_PETS      = (long) 16;
    public static final Long SMALL_PET      = (long) 8;
    public static final Long BIRDS           = (long) 32;
    public static final Long FISH            = (long) 64;
    public static final Long REPTILES        = (long) 128;

    private static Map<Long, Integer> petsMap = new HashMap<>();


    public static void loadPets() {
        petsMap.put(SMALL_PETS, R.drawable.ic_pets_small);
        petsMap.put(SMALL_PET, R.drawable.ic_pets_small);
        petsMap.put(FISH, R.drawable.ic_pets_fish);
        petsMap.put(BIRDS, R.drawable.ic_pets_bird);
        petsMap.put(DOGS, R.drawable.ic_pets_dog);
        petsMap.put(REPTILES, R.drawable.ic_pets_reptiles);
        petsMap.put(CATS, R.drawable.ic_pets_cat);
    }


    public static int getPetImageByName(long id) {
        if (petsMap.isEmpty()) {
            loadPets();
        }
        return petsMap.get(id);
    }
}
