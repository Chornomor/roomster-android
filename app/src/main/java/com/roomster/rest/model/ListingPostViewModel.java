package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import com.roomster.enums.ServiceTypeEnum;


public class ListingPostViewModel {

    /**
     **/
    @SerializedName("service_type")
    private ServiceTypeEnum mServiceType = null;

    /**
     **/
    @SerializedName("full_address")
    private String mFullAddress = null;

    /**
     **/
    @SerializedName("share_details")
    private ShareDetails mShareDetails = null;

    /**
     **/
    @SerializedName("is_active")
    private Boolean       mIsActive      = null;
    /**
     **/
    @SerializedName("calendar")
    private Calendar      mCalendar      = null;
    /**
     **/
    @SerializedName("rates")
    private Rates         mRates         = null;
    /**
     **/
    @SerializedName("need_apartment")
    private NeedApartment mNeedApartment = null;
    /**
     **/
    @SerializedName("have_apartment")
    private HaveApartment mHaveApartment = null;


    public HaveApartment getHaveApartment() {
        return mHaveApartment;
    }


    public void setHaveApartment(HaveApartment haveApartment) {
        mHaveApartment = haveApartment;
    }


    public ServiceTypeEnum getServiceType() {
        return mServiceType;
    }


    public void setServiceType(ServiceTypeEnum serviceType) {
        mServiceType = serviceType;
    }


    public String getFullAddress() {
        return mFullAddress;
    }


    public void setFullAddress(String fullAddress) {
        mFullAddress = fullAddress;
    }


    public ShareDetails getShareDetails() {
        return mShareDetails;
    }


    public void setShareDetails(ShareDetails shareDetails) {
        mShareDetails = shareDetails;
    }


    public Boolean getIsActive() {
        return mIsActive;
    }


    public void setIsActive(Boolean isActive) {
        mIsActive = isActive;
    }


    public Calendar getCalendar() {
        return mCalendar;
    }


    public void setCalendar(Calendar calendar) {
        mCalendar = calendar;
    }


    public Rates getRates() {
        return mRates;
    }


    public void setRates(Rates rates) {
        mRates = rates;
    }


    public NeedApartment getNeedApartment() {
        return mNeedApartment;
    }


    public void setNeedApartment(NeedApartment needApartment) {
        mNeedApartment = needApartment;
    }

}