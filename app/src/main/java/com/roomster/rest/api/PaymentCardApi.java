package com.roomster.rest.api;


import com.roomster.rest.model.CardGetModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;


public interface PaymentCardApi {

    /**
     * Get stored payment card
     *
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<CardGetModel>
     */

    @GET("PaymentCard")
    Call<CardGetModel> paymentCardGet(@Header("Authorization") String authorization);
}
