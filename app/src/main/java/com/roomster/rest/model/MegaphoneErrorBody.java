package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;


public class MegaphoneErrorBody {

    @SerializedName("Message")
    private String message;


    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }
}
