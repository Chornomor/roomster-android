package com.roomster.rest.api;


import com.roomster.rest.model.AppInstallsViewModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;


/**
 * Api for managing the registration id, taken from GCM in order to receive push notifications.
 */
public interface AppInstallsApi {

    @POST("AppInstalls/")
    @Headers("Content-Type: application/json")
    Call<String> saveRegistration(@Header("Authorization") String authorization, @Body AppInstallsViewModel model);

    @PUT("AppInstalls/")
    Call<String> updateRegistration(@Header("Authorization") String authorization, @Query("instance_id") String instanceId,
                                    @Query("notification_enabled") Boolean notificationEnabled);
}
