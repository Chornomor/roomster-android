package com.roomster.rest.api;


import com.roomster.rest.model.UserGetViewModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;


public interface MeApi {

    /**
     * Get user info for logged in user
     *
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<UserGetViewModel>
     */

    @GET("Me")
    Call<UserGetViewModel> meGet(@Header("Authorization") String authorization);
}
