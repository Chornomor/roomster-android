package com.roomster.utils;


import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.geometry.Point;


public class LocationUtil {

    public static final  double METERS_IN_MILE             = 1609.344;
    private static final int    DEGREE_DISTANCE_AT_EQUATOR = 111329;
    /**
     * the radius of the earth in meters.
     */
    private static final double EARTH_RADIUS               = 6378137; //meters
    /**
     * the length of one minute of latitude in meters, i.e. one nautical mile in meters.
     */
    private static final double MINUTES_TO_METERS          = 1852d;
    /**
     * the amount of minutes in one degree.
     */
    private static final double DEGREE_TO_MINUTES          = 60d;


    public static double milesToMeters(double miles) {
        return miles * METERS_IN_MILE;
    }


    public static LatLngBounds getBounds(double latitude, double longitude, double distanceInMeters) {
        Point boundsNE = extrapolate(latitude, longitude, 45, distanceInMeters);
        Point boundsSW = extrapolate(latitude, longitude, 225, distanceInMeters);

        return new LatLngBounds(new LatLng(boundsSW.x, boundsSW.y), new LatLng(boundsNE.x, boundsNE.y));
    }


    /**
     * This method extrapolates the endpoint of a movement with a given length from a given starting point using a given
     * course.
     *
     * @param startPointLat
     *   the latitude of the starting point in degrees, must not be {@link Double#NaN}.
     * @param startPointLon
     *   the longitude of the starting point in degrees, must not be {@link Double#NaN}.
     * @param course
     *   the course to be used for extrapolation in degrees, must not be {@link Double#NaN}.
     * @param distance
     *   the distance to be extrapolated in meters, must not be {@link Double#NaN}.
     * @return the extrapolated point.
     */
    public static Point extrapolate(final double startPointLat, final double startPointLon, final double course,
                                    final double distance) {

        final double crs = Math.toRadians(course);
        final double d12 = Math.toRadians(distance / MINUTES_TO_METERS / DEGREE_TO_MINUTES);

        final double lat1 = Math.toRadians(startPointLat);
        final double lon1 = Math.toRadians(startPointLon);

        final double lat = Math.asin(Math.sin(lat1) * Math.cos(d12) + Math.cos(lat1) * Math.sin(d12) * Math.cos(crs));
        final double dlon = Math
          .atan2(Math.sin(crs) * Math.sin(d12) * Math.cos(lat1), Math.cos(d12) - Math.sin(lat1) * Math.sin(lat));
        final double lon = (lon1 + dlon + Math.PI) % (2 * Math.PI) - Math.PI;

        return new Point(Math.toDegrees(lat), Math.toDegrees(lon));
    }


    /**
     * calculates the length of one degree of longitude at the given latitude.
     *
     * @param latitude
     *   the latitude to calculate the longitude distance for, must not be {@link Double#NaN}.
     * @return the length of one degree of longitude at the given latitude in meters.
     */
    public static double longitudeDistanceAtLatitude(final double latitude) {

        final double longitudeDistanceScaleForCurrentLatitude = Math.cos(Math.toRadians(latitude));
        return DEGREE_DISTANCE_AT_EQUATOR * longitudeDistanceScaleForCurrentLatitude;
    }
}
