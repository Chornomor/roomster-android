package com.roomster.event.message;


import com.roomster.event.Event;


public class MessageEvent extends Event {

    public static class NewMessage extends MessageEvent {

        String mTitle;
        String mMessage;
        String mConversationId;
        Long   mUserId;
        Long   mOtherUserId;


        public NewMessage(String title, String message, String conversationId, Long userId, Long otherUserId) {
            mTitle = title;
            mMessage = message;
            mConversationId = conversationId;
            mUserId = userId;
            mOtherUserId = otherUserId;
        }


        public String getTitle() {
            return mTitle;
        }


        public String getMessage() {
            return mMessage;
        }


        public String getConversationId() {
            return mConversationId;
        }


        public Long getUserId() {
            return mUserId;
        }


        public Long getOtherUserId() {
            return mOtherUserId;
        }
    }
}
