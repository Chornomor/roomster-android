package com.roomster.views;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.roomster.R;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by "Michael Katkov" on 10/19/2015.
 */
public class MenuItemView extends LinearLayout {

    @Bind(R.id.view_menu_item_icon)
    ImageView mIconIv;

    @Bind(R.id.view_menu_item_upper_text)
    TextView mUpperTv;

    @Bind(R.id.view_menu_item_lower_text)
    TextView mLowerTv;


    public MenuItemView(Context context) {
        super(context);
        initView();
    }


    public MenuItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }


    private void initView() {
        inflate(getContext(), R.layout.view_menu_item, this);
        ButterKnife.bind(this);
    }


    public void setIcon(int drawable) {
        mIconIv.setImageResource(drawable);
    }


    public void setUpperText(int resource) {
        mUpperTv.setText(getContext().getString(resource));
    }


    public void setLowerText(int resource) {
        mLowerTv.setText(getContext().getString(resource));
    }
}
