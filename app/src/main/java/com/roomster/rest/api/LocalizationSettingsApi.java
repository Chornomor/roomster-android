package com.roomster.rest.api;


import com.roomster.rest.model.LocalizationSettingsModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import retrofit2.http.Query;


public interface LocalizationSettingsApi {

    @GET("LocalizationSettings")
    Call<LocalizationSettingsModel> localizationSettingsGet(@Query("country") String country);

    @GET("LocalizationSettings")
    Call<LocalizationSettingsModel> localizationSettingsGet(@Header("Authorization") String authorization, @Query("country") String country);

    @PUT("LocalizationSettings")
    Call<LocalizationSettingsModel> localizationSettingsPut(@Header("Locale") String locale, @Header("Authorization") String authorization);
}
