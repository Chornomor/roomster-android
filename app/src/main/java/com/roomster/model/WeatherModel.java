package com.roomster.model;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;


//Response:
// {"coord":{"lon":-0.13,"lat":51.51},
// "weather":[{"id":800,"main":"Clear","description":"Sky is Clear","icon":"01n"}],
// "base":"stations",
// "main":{"temp":289.46,"pressure":1028,"humidity":52,"temp_min":286.15,"temp_max":291.85},
// "visibility":10000,
// "wind":{"speed":2.6,"deg":310},"clouds":{"all":0},"dt":1441567026,
// "sys":{"type":1,"id":5091,"message":0.0118,"country":"GB","sunrise":1441516901,"sunset":1441564481},
// "id":2643743,
// "name":"London",
// "cod":200}
public class WeatherModel {

    @SerializedName("coord")
    private HashMap<String, String> coord;

    @SerializedName("weather")
    private ArrayList<Object> weather;

    @SerializedName("base")
    private String base;

    @SerializedName("main")
    private Object main;

    @SerializedName("visibility")
    private int visibility;

    @SerializedName("wind")
    private Object wind;

    @SerializedName("sys")
    private Object sys;

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("cod")
    private int cod;


    public HashMap<String, String> getCoord() {
        return coord;
    }


    public ArrayList<Object> getWeather() {
        return weather;
    }


    public String getBase() {
        return base;
    }


    public Object getMain() {
        return main;
    }


    public int getVisibility() {
        return visibility;
    }


    public Object getWind() {
        return wind;
    }


    public Object getSys() {
        return sys;
    }


    public int getId() {
        return id;
    }


    public String getName() {
        return name;
    }


    public int getCod() {
        return cod;
    }
}
