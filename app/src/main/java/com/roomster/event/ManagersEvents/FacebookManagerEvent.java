package com.roomster.event.ManagersEvents;


import com.roomster.event.Event;
import com.roomster.model.FacebookPhotosModel;


/**
 * Created by "Michael Katkov" on 10/15/2015.
 */
public class FacebookManagerEvent extends Event {

    public static class GetMeSuccess extends FacebookManagerEvent {

    }


    public static class GetMeFailure extends FacebookManagerEvent {

        public String errorMessage;
    }


    public static class GetMeLikesSuccess extends FacebookManagerEvent {

    }


    public static class GetMeLikesFailure extends FacebookManagerEvent {

        public String errorMessage;
    }


    public static class GetMyProfileAlbumSuccess extends FacebookManagerEvent {

        public FacebookPhotosModel facebookPhotosModel;
    }


    public static class GetMyProfileAlbumFailure extends FacebookManagerEvent {

        public String errorMessage;
    }
}
