package com.roomster.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.constants.IntentExtras;
import com.roomster.enums.BillingTypeEnum;
import com.roomster.event.billing.BillingEvent;
import com.roomster.rest.service.BillingRefundsRestService;
import com.roomster.utils.DateUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by yuliasokolova on 25.10.16.
 */
public class RefundActivity extends  BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.tv_title)
    TextView mToolbarTitle;

    @Bind(R.id.iv_back)
    ImageView mBackButton;

    @Bind(R.id.btn_billing_policy)
    Button mPolicyButton;

    @Bind(R.id.billing_text_content)
    TextView mContentText;

    @Bind(R.id.billing_text_policy)
    TextView mPolicyTextStart;

    @Bind(R.id.refund_result_layout)
    LinearLayout mResultLayout;

    @Bind(R.id.empty_layout)
    RelativeLayout mEmptyLayout;

    @Bind(R.id.empty_result_header)
    TextView mEmptyResultHeaderTV;

    @Bind(R.id.empty_result_subtext)
    TextView mEmptyResultSubtextTV;

    @Inject
    Tracker mTracker;

    @Inject
    BillingRefundsRestService mBillingRestService;

    boolean callInProgress = false;

    private String mPrice = "$17.22";

    @Override
    protected int getMainContentLayout() {
        return R.layout.refund_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomsterApplication.getRoomsterComponent().inject(this);
        ButterKnife.bind(this);
        getBillingInfo();
    }


    private void getBillingInfo() {
        mBillingRestService.getBillingInfo(BillingTypeEnum.GET);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName(getString(R.string.screen_name_refund));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    @OnClick(R.id.iv_back)
    void onToolbarBackClicked() {
        onBackPressed();
    }

    @OnClick(R.id.btn_billing_policy)
    void onBillingBtnClick (){
        if(callInProgress)
            return;
        callInProgress= true;
        mBillingRestService.getBillingInfo(BillingTypeEnum.POST);
    }


    private void onPoliceLinkClick () {
        Intent intent = new Intent(this, AboutActivity.class);
        intent.putExtra(IntentExtras.EXTRA_ABOUT_URL, "terms");
        startActivity(intent);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(BillingEvent.EligibleGetSuccess event) {
        callInProgress = false;
        initViews(event);
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEligbleMethodFail(BillingEvent.EligibleGetFail event){
        callInProgress = false;

    }

    private void showEmptyTransactions(String message) {
        mResultLayout.setVisibility(View.GONE);
        mEmptyLayout.setVisibility(View.VISIBLE);
        mEmptyResultHeaderTV.setText(message);
        mEmptyResultSubtextTV.setText(getString(R.string.empty_transactions_subtext));
    }

    private void hideEmptyTransactions() {
        mResultLayout.setVisibility(View.VISIBLE);
        mEmptyLayout.setVisibility(View.GONE);
    }

    private void initViews(BillingEvent.EligibleGetSuccess event) {
        hideEmptyTransactions();
        mToolbarTitle.setText(getString(R.string.refund_title));
        Spanned policy = Html.fromHtml(String.format(getString(R.string.billing_policy), "<b> " + getString(R.string.billing_refund_policy) + " </b>"));
        SpannableString ss = new SpannableString(policy);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                onPoliceLinkClick();
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.blue_500));
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(clickableSpan, 15, 29, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        mPolicyTextStart.setText(ss);
        mPolicyTextStart.setMovementMethod(LinkMovementMethod.getInstance());
        mPolicyTextStart.setHighlightColor(Color.TRANSPARENT);


        String text = "";

        if (event.type == BillingTypeEnum.POST) {
            text = getResources().getString(R.string.billing_success_submit);
            mContentText.setText(text);
            mPolicyButton.setVisibility(View.GONE);
        } else if (event.type == BillingTypeEnum.GET) {
            if (event.eligible) {
                mContentText.setText(Html.fromHtml(String.format(getString(R.string.billing_full_membership), "<b>" + mPrice + "</b>")));
                mPolicyButton.setVisibility(View.VISIBLE);
            } else {
                if (event.refund != null) {
                    String date = DateUtil.parseDateBilling(event.refund.getDateAdded());
                    String dateUTC = " " + date + " (UTC)";
                    mContentText.setText(Html.fromHtml(String.format(getString(R.string.billing_refund), "<b>" + mPrice + "</b>", dateUTC)));

                } else {
                    if (event.dispute != null){
                        text = getResources().getString(R.string.billing_dispute);
                        mContentText.setText(text);
                    }else {
                        showEmptyTransactions(getString(R.string.empty_transactions_header));
                    }
                }
                mPolicyButton.setVisibility(View.GONE);
            }
        }
    }
}