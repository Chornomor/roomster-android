package com.roomster.adapter;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.manager.CatalogsManager;
import com.roomster.model.CatalogModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;


/**
 * Created by Bogoi on 12/23/2015.
 */
public class LanguagesAdapter extends BaseAdapter {

    private Set<String> mSelectedItems;

    private List<CatalogModel> mLanguagesList;

    Context mContext;

    @Inject
    LayoutInflater mLayoutInflater;

    @Inject
    CatalogsManager mCatalogsManager;


    public LanguagesAdapter(Context context, List<String> selectedLanguages) {
        RoomsterApplication.getRoomsterComponent().inject(this);
        mContext = context;
        mLanguagesList = mCatalogsManager.getSpokenLanguagesList();

        if (selectedLanguages != null) {
            mSelectedItems = new HashSet<>(selectedLanguages);
        } else {
            mSelectedItems = new HashSet<>();
        }
    }


    @Override
    public int getCount() {
        return mLanguagesList.size();
    }


    @Override
    public Object getItem(int position) {
        return mLanguagesList.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        CatalogModel languageModel = mLanguagesList.get(position);
        String language = languageModel.getName();
        if (convertView == null || convertView.getTag() == null) {

            convertView = mLayoutInflater.inflate(R.layout.grid_item_language, parent, false);
            holder = new ViewHolder();
            holder.languageTextView = (TextView) convertView.findViewById(R.id.language_name);
            convertView.setOnClickListener(new SwitchSelectionClickListener(language));
            convertView.setTag(holder);
        }

        holder = (ViewHolder) convertView.getTag();

        int itemColor = ContextCompat.getColor(mContext, R.color.gray_text);
        if (mSelectedItems.contains(language)) {
            itemColor = ContextCompat.getColor(mContext, R.color.orange);
            convertView.setBackgroundResource(R.drawable.orange_rounded_corners);
        } else {
            convertView.setBackgroundResource(R.drawable.gray_rounded_corners);
        }

        holder.languageTextView.setTextColor(itemColor);
        holder.languageTextView.setText(language);

        return convertView;
    }


    public List<Integer> getLanguageIds() {
        ArrayList<Integer> list = new ArrayList<>();
        for (String language : mSelectedItems) {
            for (CatalogModel languageModel : mLanguagesList) {
                if (languageModel.getName().equals(language)) {
                    list.add(languageModel.getId());
                    break;
                }
            }
        }

        return list;
    }


    public List<String> getLanguages() {

        return new ArrayList<>(mSelectedItems);
    }

    public static class ViewHolder {

        public TextView languageTextView;
    }


    private class SwitchSelectionClickListener implements View.OnClickListener {

        private String language;


        private SwitchSelectionClickListener(String language) {
            this.language = language;
        }


        @Override
        public void onClick(View v) {
            if (mSelectedItems.contains(language)) {
                mSelectedItems.remove(language);
            } else {
                mSelectedItems.add(language);
            }
            notifyDataSetChanged();
        }
    }
}
