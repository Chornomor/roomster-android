package com.roomster.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.roomster.R;
import com.roomster.activity.MyListingActivity;
import com.roomster.adapter.MyListingsDetailedViewPageAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.MeRestServiceEvent;
import com.roomster.event.RestServiceEvents.UserListingsRestServiceEvent;
import com.roomster.manager.MeManager;
import com.roomster.rest.model.ListingGetViewModel;
import com.roomster.rest.model.ResultItemModel;
import com.roomster.rest.service.MeRestService;
import com.roomster.rest.service.UserListingsRestService;
import com.roomster.utils.FancyPageTransformer;
import com.roomster.views.NonSwipableViewPager;
import com.roomster.views.ReplacableViewPager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MyListingFragment extends Fragment {

    private MyListingsDetailedViewPageAdapter mPagerAdapter;
    private int                               mListingIndex;
    private SwipeCallback                     mCallback;

    @Bind(R.id.my_listing_view_pager)
    NonSwipableViewPager mViewPager;

    @Bind(R.id.my_listing_view_right_arrow)
    ImageView mRightArrow;

    @Bind(R.id.my_listing_view_left_arrow)
    ImageView mLeftArrow;

    @Inject
    MeManager mMeManager;

    @Inject
    MeRestService mMeRestService;

    @Inject
    UserListingsRestService mUserListingsRestService;

    @Inject
    EventBus mEventBus;

    @Inject
    Context mContext;


    public interface SwipeCallback {

        void onPageChanged(int page);
    }


    public static MyListingFragment newInstance(int listingIndex) {
        MyListingFragment fragment = new MyListingFragment();
        Bundle args = new Bundle();
        args.putInt(MyListingActivity.LISTING_INDEX, listingIndex);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mListingIndex = getArguments().getInt(MyListingActivity.LISTING_INDEX);
        }
        if (!mEventBus.isRegistered(this)) {
            mEventBus.register(this);
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_listing, container, false);
        ButterKnife.bind(this, view);
        checkData();
        return view;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mEventBus.isRegistered(this)) {
            mEventBus.unregister(this);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(MeRestServiceEvent.GetMeSuccess event) {
        mUserListingsRestService.getUserListings(mMeManager.getMe().getId(), true);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(MeRestServiceEvent.GetMeFailure event) {
        Toast.makeText(mContext, getString(R.string.unable_to_get_user_info), Toast.LENGTH_SHORT).show();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(UserListingsRestServiceEvent.GetListingsSuccess event) {
        initView();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(UserListingsRestServiceEvent.GetListingsFailed event) {
        Toast.makeText(mContext, getString(R.string.unable_to_get_listings_info), Toast.LENGTH_SHORT).show();
    }


    public void setCallback(SwipeCallback callback) {
        mCallback = callback;
    }


    private void checkData() {
        if (mMeManager.getMyListings() != null && mMeManager.getMe() != null) {
            initView();
        } else {
            getOrRequestMe();
        }
    }


    private void getOrRequestMe() {
        if (mMeManager.getMe() == null) {
            mMeRestService.requestMe();
        } else {
            mUserListingsRestService.getUserListings(mMeManager.getMe().getId(), true);
        }
    }


    private void initView() {
        FancyPageTransformer transformer = new FancyPageTransformer(mViewPager, mLeftArrow, mRightArrow);
        mPagerAdapter = new MyListingsDetailedViewPageAdapter(getChildFragmentManager(), createList());
        mViewPager.setAdapter(mPagerAdapter);
        if (0 < mListingIndex && mListingIndex < mPagerAdapter.getCount()) {
            mViewPager.setCurrentItem(mListingIndex);
        }
        mViewPager.addOnPageChangeListener(mPageChangeListener);
    }


    private ResultItemModel makeResultItemModel(ListingGetViewModel listingGetViewModel) {
        ResultItemModel resultItemModel = new ResultItemModel();
        resultItemModel.setListing(listingGetViewModel);
        resultItemModel.setUser(mMeManager.getMe());
        return resultItemModel;
    }


    private List<ResultItemModel> createList() {
        List<ResultItemModel> result = new ArrayList<>();
        for (ListingGetViewModel listingGetViewModel : mMeManager.getMyListings()) {
            result.add(makeResultItemModel(listingGetViewModel));
        }
        return result;
    }


    private ReplacableViewPager.SimpleOnPageChangeListener mPageChangeListener = new ReplacableViewPager.SimpleOnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            if (mCallback != null) {
                mCallback.onPageChanged(position);
            }
        }
    };
}