package com.roomster.event.RestServiceEvents;


import com.roomster.event.Event;
import com.roomster.rest.model.ListingGetViewModel;

import java.util.List;


/**
 * Created by "Michael Katkov" on 12/10/2015.
 */
public class UserListingsRestServiceEvent extends Event {

    public static class GetListingsSuccess extends UserListingsRestServiceEvent {

        public List<ListingGetViewModel> listings;


        public GetListingsSuccess(List<ListingGetViewModel> listings) {
            this.listings = listings;
        }
    }


    public static class GetListingsFailed extends UserListingsRestServiceEvent {

    }

    public static class ListingDeleted extends UserListingsRestServiceEvent {
        public long id;

        public ListingDeleted(long id) {
            this.id = id;
        }
    }
}
