package com.roomster.utils;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;


/**
 * Created by "Michael Katkov" on 10/8/2015.
 */
public class LocaleUtil {

    /**
     * Transform server localies to display format
     *
     * @param serverLocale server format local string
     * @return display format local string
     */

    public static String toDisplayFormat(String serverLocale) {
        String language = serverLocale.substring(0, 2);
        String country = serverLocale.substring(3, 5);
        Locale locale = new Locale(language, country);
        String value = locale.getDisplayLanguage() + "(" + locale.getCountry() + ")";
        return value;
    }


    public static void updateResources(Context context, String language) {
        DateUtil.resetTimes();
        if (language.isEmpty()){
            language = Resources.getSystem().getConfiguration().locale.getLanguage();
            language += "_" + Resources.getSystem().getConfiguration().locale.getCountry();
        }
        String country = language.substring(3, language.length());
        language = language.substring(0, 2);

        Locale locale = new Locale(language, country);
        Locale.setDefault(locale);

        Resources resources = context.getApplicationContext().getResources();

        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLayoutDirection(locale);
        }

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }


    public static String toDisplayLanguageFormat(String serverLocale) {
        String language = serverLocale.substring(0, 2);
        String country = serverLocale.substring(3, 5);
        Locale locale = new Locale(language, country);
        String value = locale.getDisplayLanguage();
        return value;
    }


    public static ArrayList<String> getCountryCodesList() {
        Locale[] locales = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        for (Locale locale : locales) {
            String country = locale.getCountry();
            if (country.trim().length() > 0 && !countries.contains(country)) {
                countries.add(country);
            }
        }
        Collections.sort(countries);

        return countries;
    }


    public static boolean isLocaleMeasureUnitImperial() {
        Locale locale = Locale.getDefault();
        String countryCode = locale.getCountry();
        return "US".equals(countryCode) || "LR".equals(countryCode) || "MM".equals(countryCode);
    }
}
