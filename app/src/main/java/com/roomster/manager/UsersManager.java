package com.roomster.manager;


import com.roomster.application.RoomsterApplication;
import com.roomster.rest.model.UserGetViewModel;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by michaelkatkov on 12/17/15.
 */
public class UsersManager {

    private List<UserGetViewModel> mUsers;

    @Inject
    SearchManager mSearchManager;


    public UsersManager() {
        RoomsterApplication.getRoomsterComponent().inject(this);
        mUsers = new ArrayList<>();
    }


    public List<UserGetViewModel> getUsers() {
        return mUsers;
    }


    public void setUsers(List<UserGetViewModel> users) {
        mUsers = users;
    }


    public void addUserModel(UserGetViewModel userGetViewModel) {
        if (!mUsers.contains(userGetViewModel)) {
            mUsers.add(userGetViewModel);
        }
    }


    private UserGetViewModel getUserByIdLocal(Long id) {
        if (mUsers != null && mUsers.size() > 0){
            for (UserGetViewModel userGetViewModel : mUsers) {
                if (userGetViewModel.getId().equals(id)) {
                    return userGetViewModel;
                }
            }
        }
        return null;
    }


    public UserGetViewModel getUserById(Long id) {
        UserGetViewModel userGetViewModel = getUserByIdLocal(id);
        if (userGetViewModel == null) {
            userGetViewModel = mSearchManager.findUserModelById(id);
        }
        if (userGetViewModel != null) {
            addUserModel(userGetViewModel);
        }
        return userGetViewModel;
    }
}
