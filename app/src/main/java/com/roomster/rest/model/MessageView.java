package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import java.util.Date;


public class MessageView {

    /**
     **/
    @SerializedName("Id")
    private String id = null;

    /**
     **/
    @SerializedName("UserId")
    private Long userId = null;

    /**
     **/
    @SerializedName("Text")
    private String text = null;

    /**
     **/
    @SerializedName("Created")
    private Date created = null;

    /**
     **/
    @SerializedName("ConversationId")
    private String conversationId = null;

    /**
     **/
    @SerializedName("User")
    private ConversationUserProfile user = null;

    /**
     **/
    @SerializedName("ByConversationUser")
    private Boolean byConversationUser = null;

    /**
     **/
    @SerializedName("AttachmentListing")
    private AttachmentListingView attachmentListing = null;

    /**
     **/
    @SerializedName("IsLocked")
    private Boolean isLocked = null;


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public Long getUserId() {
        return userId;
    }


    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public String getText() {
        return text;
    }


    public void setText(String text) {
        this.text = text;
    }


    public Date getCreated() {
        return created;
    }


    public void setCreated(Date created) {
        this.created = created;
    }


    public String getConversationId() {
        return conversationId;
    }


    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }


    public ConversationUserProfile getUser() {
        return user;
    }


    public void setUser(ConversationUserProfile user) {
        this.user = user;
    }


    public Boolean getByConversationUser() {
        return byConversationUser;
    }


    public void setByConversationUser(Boolean byConversationUser) {
        this.byConversationUser = byConversationUser;
    }


    public AttachmentListingView getAttachmentListing() {
        return attachmentListing;
    }


    public void setAttachmentListing(AttachmentListingView attachmentListing) {
        this.attachmentListing = attachmentListing;
    }


    public Boolean getIsLocked() {
        return isLocked;
    }


    public void setIsLocked(Boolean isLocked) {
        this.isLocked = isLocked;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class MessageView {\n");

        sb.append("  id: ").append(id).append("\n");
        sb.append("  userId: ").append(userId).append("\n");
        sb.append("  text: ").append(text).append("\n");
        sb.append("  created: ").append(created).append("\n");
        sb.append("  conversationId: ").append(conversationId).append("\n");
        sb.append("  user: ").append(user).append("\n");
        sb.append("  byConversationUser: ").append(byConversationUser).append("\n");
        sb.append("  attachmentListing: ").append(attachmentListing).append("\n");
        sb.append("  isLocked: ").append(isLocked).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
