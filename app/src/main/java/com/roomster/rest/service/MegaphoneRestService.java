package com.roomster.rest.service;


import com.google.gson.Gson;

import com.roomster.R;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.TokenManager;
import com.roomster.rest.api.MegaphoneApi;
import com.roomster.rest.model.MegaphoneParamsModel;
import com.roomster.rest.model.MembersCountModel;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MegaphoneRestService {

    private static String MEGAPHONE_KEY_MEMBERS = "count";

    @Inject
    EventBus mEventBus;

    @Inject
    TokenManager mTokenManager;

    @Inject
    MegaphoneApi mMegaphoneApi;

    @Inject
    NetworkManager mNetworkManager;


    public MegaphoneRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
    }

    public void getMembers(final String serviceType, final Integer minBudget, final Integer maxBudget, final Integer minAge,
                           final Integer maxAge, final String sex, final String bedrooms, final String searchMessage, final String householdSex,
                           final RestServiceListener<MembersCountModel> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callMegaphoneGet(serviceType, result, minBudget, maxBudget, minAge, maxAge, sex, bedrooms, searchMessage, householdSex,
                  listener);
            }

            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                if (listener != null) {
                    listener.onFailure(code, throwable, msg);
                }
            }
        });
    }


    public void sendMegaphoneMessage(final MegaphoneParamsModel model, final RestServiceListener<String> listener) {
        mTokenManager.getAccessToken(new RestServiceListener<String>() {

            @Override
            public void onSuccess(String result) {
                callMessagePost(model, result, listener);
            }


            @Override
            public void onFailure(int code, Throwable throwable, String msg) {
                listener.onFailure(code, throwable, msg);
            }
        });
    }


    private void callMessagePost(MegaphoneParamsModel messageModel, String authToken,
                                 final RestServiceListener<String> listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<String> callMessagePost = mMegaphoneApi.megaphonePost(messageModel, authToken);
            callMessagePost.enqueue(new Callback<String>() {

                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    switch (response.code()) {
                        case ResponseCode.OK:
                            if (listener != null) {
                                listener.onSuccess(response.body());
                            }
                            break;
                        default:
                            if (listener != null) {
                                listener.onFailure(response.code(), null, response.body());
                            }
                    }
                }


                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    if (listener != null) {
                        listener.onFailure(-1, t, RoomsterApplication.context.getString(R.string.megaphone_cant_make_rest_call_to_send) + RoomsterApplication.context.getString(R.string.megaphone_cant_make_rest_call_message));
                    }
                }
            });
        } else {
            if (listener != null) {
                listener.onFailure(-1, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
            }
        }
    }


    private void callMegaphoneGet(String serviceType, String authToken, Integer minBudget, Integer maxBudget, Integer minAge,
                                  Integer maxAge, String sex, String bedrooms, String searchMessage,String householdSex,
                                  final RestServiceListener<MembersCountModel> listener) {
        if (mNetworkManager.hasNetworkAccess()) {
            Call<Map<String, Object>> callMessagePost = mMegaphoneApi
              .megaphoneGet(serviceType, authToken, minBudget, maxBudget, minAge, maxAge, sex, bedrooms, searchMessage, householdSex);

            callMessagePost.enqueue(new Callback<Map<String, Object>>() {

                @Override
                public void onResponse(Call<Map<String, Object>> call, Response<Map<String, Object>> response) {
                    int responseCode = response.code();
                    switch (responseCode) {
                        case ResponseCode.OK:
                            Object result = "";
                            if (response.body() != null) {
                                result = response.body().get(MEGAPHONE_KEY_MEMBERS);
                                MembersCountModel model = new MembersCountModel();
                                model.count = ((Double)result).intValue();
                                listener.onSuccess(model);
                            }else
                                listener.onFailure(response.code(), null, null);

                            break;
                        default:
                            String errorMessage = null;
                            try {
                                if (response.errorBody() != null) {
                                    errorMessage = response.errorBody().string();
                                }
                            } catch (IOException e) {
                                // nothing to do
                            }
                            if (listener != null) {
                                listener.onFailure(response.code(), null, errorMessage);
                            }
                    }
                }

                @Override
                public void onFailure(Call<Map<String, Object>> call, Throwable t) {
                    if (listener != null) {
                        t.printStackTrace();
                        listener.onFailure(-1, t, RoomsterApplication.context.getString(R.string.megaphone_cant_make_rest_call_to) + RoomsterApplication.context.getString(R.string.megaphone_cant_make_rest_call_server));
                    }
                }

            });
        } else {
            if (listener != null) {
                listener.onFailure(-1, null, RoomsterApplication.context.getString(R.string.notification_no_network_access));
            }
        }
    }
}