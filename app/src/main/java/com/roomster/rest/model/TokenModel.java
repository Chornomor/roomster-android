package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


/**
 * Created by Bogoi.Bogdanov on 10/7/2015.
 */
public class TokenModel implements Serializable {

    @SerializedName("access_token")
    private String token = null;

    @SerializedName("email")
    private String email = null;

    @SerializedName("token_type")
    private String tokenType = null;

    @SerializedName("expires_in")
    private long expiresIn = 0;

    @SerializedName("user")
    private UserGetViewModel user;


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public String getToken() {
        return token;
    }


    public void setToken(String token) {
        this.token = token;
    }


    public String getTokenType() {
        return tokenType;
    }


    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }


    public long getExpiresIn() {
        return expiresIn;
    }


    public void setExpiresIn(long expiresIn) {
        this.expiresIn = expiresIn;
    }

    public UserGetViewModel getUser() {
        return user;
    }

    public void setUser(UserGetViewModel user) {
        this.user = user;
    }
}
