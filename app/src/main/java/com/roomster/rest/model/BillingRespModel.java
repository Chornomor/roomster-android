package com.roomster.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yuliasokolova on 25.10.16.
 */
public class BillingRespModel {

    @SerializedName("Eligible")
    private boolean eligible;

    @SerializedName("Dispute")
    private DisputeModel dispute;

    @SerializedName("Refund")
    private RefundModel refund;

    @SerializedName("Message")
    private String message;

    public boolean isEligible() {
        return eligible;
    }

    public DisputeModel getDispute() {
        return dispute;
    }

    public RefundModel getRefund() {
        return refund;
    }

    public String getMessage() {
        return message;
    }

}
