package com.roomster.fragment;


import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.MarkerManager;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.algo.GridBasedAlgorithm;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.roomster.R;
import com.roomster.activity.DiscoveryPreferencesActivity;
import com.roomster.activity.MapListViewActivity;
import com.roomster.activity.UserProfileActivity;
import com.roomster.adapter.MapMarkerAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.ManagersEvents.DiscoveryPrefsManagerEvent;
import com.roomster.event.RestServiceEvents.SearchRestServiceEvent;
import com.roomster.event.application.ApplicationEvent;
import com.roomster.listener.BookmarksCallbackListener;
import com.roomster.manager.DiscoveryPrefsManager;
import com.roomster.manager.SearchManager;
import com.roomster.manager.map.MapCluster;
import com.roomster.manager.map.MapManager;
import com.roomster.rest.model.ResultItemModel;
import com.roomster.rest.service.SearchRestService;
import com.roomster.utils.ImageHelper;
import com.roomster.utils.LogUtil;
import com.roomster.utils.PermissionsAware;
import com.roomster.utils.PermissionsHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by michaelkatkov on 11/18/15.
 */
public class MapViewFragment extends BaseMapFragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ClusterManager.OnClusterItemClickListener<ResultItemModel>,
        ClusterManager.OnClusterItemInfoWindowClickListener<ResultItemModel>, ClusterManager.OnClusterClickListener<ResultItemModel>,
        GoogleMap.OnMapClickListener, GoogleMap.OnCameraChangeListener, MapManager.Callback {

    protected static final int PAGE_SIZE = 100;
    private static final int RESEARCH_DELAY = 1000;
    private static final int REDRAW_DELAY = 2000;
    private static final int ZOOM_LEVEL = 10;
    private static final int IMAGE_REDRAW_ATTEMPTS_COUNT = 3;

    private boolean mIsInitialLoading;

    private GoogleApiClient mGoogleApiClient;

    private Location mLastLocation;

    private MapClusterManager mClusterManager;

    private MapMarkerAdapter mMapMarkerAdapter;

    private boolean mIsDialogShown = false;

    private Handler mHandler;

    private boolean hasDelayedResult = false;

    @Bind(R.id.empty_layout)
    RelativeLayout mEmptyLayout;

    @Bind(R.id.empty_result_header)
    TextView mEmptyResultHeaderTV;

    @Bind(R.id.empty_result_subtext)
    TextView mEmptyResultSubtextTV;

    @Bind(R.id.empty_layout_button)
    TextView mEmptyLayoutButtonTV;

    @Inject
    SearchManager mSearchManager;

    @Inject
    DiscoveryPrefsManager mDiscoveryPrefManager;

    @Inject
    LayoutInflater mLayoutInflater;

    @Inject
    SearchRestService mSearchRestService;

    @Inject
    EventBus mEventBus;

    @Inject
    Context mContext;
    private boolean mapInited = false;

    MapManager mapManager;

    private MarkerImageManager markerImageManager = new MarkerImageManager();

    private HashSet<ResultItemModel> modelsInMap = new HashSet<>();
    HashMap<Marker, LatLngBounds> boundsMap = new HashMap<Marker, LatLngBounds>();
    public BookmarksCallbackListener mCallback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mIsInitialLoading = true;
        RoomsterApplication.getRoomsterComponent().inject(this);
        mapManager = MapManager.getInstance(this,mDiscoveryPrefManager);
        mapManager.setBookmarkMode(false);
        mapManager.setListener(mCallback);
        mEventBus.register(this);
    }




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, view);
        mSearchManager.clearResultModel();
        initViews();
        initMap();
        updateSearchResult();
        view.post(new Runnable() {
            @Override
            public void run() {
                callMapUpdate();
            }
        });
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        Activity activity = getActivity();
        PermissionsHelper permissionsHelper;
        if (activity instanceof PermissionsAware) {
            permissionsHelper = ((PermissionsAware) activity).getPermissionsHelper();
        } else {
            permissionsHelper = new PermissionsHelper(getActivity());
        }

        permissionsHelper.actionOnLocationPermission(new PermissionsHelper.OnLocationPermissionActionListener() {

            @Override
            public void onLocationPermissionGranted() {
                buildGoogleApiClient();
                mGoogleApiClient.connect();
            }


            @Override
            public void onLocationPermissionDenied() {
                buildGoogleApiClient();
                mGoogleApiClient.connect();
            }
        });
    }


    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mEventBus.unregister(this);
    }


    @Override
    public void onConnected(Bundle connectionHint) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            mGoogleMap.addMarker(getMarker(mLastLocation, R.drawable.map_custom_pin));
        } else {
            Toast.makeText(mContext, R.string.no_location_detected, Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Toast.makeText(mContext, R.string.no_location_detected, Toast.LENGTH_LONG).show();
    }


    @Override
    public void onConnectionSuspended(int cause) {
        mGoogleApiClient.connect();
    }


    @Override
    public boolean onClusterItemClick(ResultItemModel item) {
        mIsDialogShown = true;
        mMapMarkerAdapter.setModel(item);
        return false;
    }


    @Override
    public void onClusterItemInfoWindowClick(ResultItemModel item) {
        startUserProfileActivity(item);
    }


    @Override
    public boolean onClusterClick(Cluster<ResultItemModel> cluster) {
        if (cluster != null) {
            mMapMarkerAdapter.setModel(null);
            mSearchManager.setMapViewItems(cluster.getItems());
            startListListingViewActivity();
        }
        onFocusChanged();
        return false;
    }


    @Override
    public void onMapClick(LatLng latLng) {
        onFocusChanged();
    }

    private void onFocusChanged(){
        mIsDialogShown = false;
        if(hasDelayedResult)
            showSearchResultsOrEmptyStateView();
    }


    @OnClick(R.id.empty_layout_button)
    void onEmptyLayoutButtonClicked() {
        Intent intent = new Intent(getActivity(), DiscoveryPreferencesActivity.class);
        startActivity(intent);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(SearchRestServiceEvent.SearchSuccess event) {
        LogUtil.logD("map count " + mSearchManager.getResultModel().getItems().size());
        showSearchResultsOrEmptyStateView();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(SearchRestServiceEvent.SearchFailure event) {
        if(!event.canceled)
            Toast.makeText(mContext, getString(R.string.unable_to_get_search_info) + event.error, Toast.LENGTH_SHORT).show();
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(DiscoveryPrefsManagerEvent.ResetSearchCriteriaEvent event){
        if(getActivity() == null || getActivity().isFinishing()) {
            return;
        }
        if(!mDiscoveryPrefManager.getViewType().equals(DiscoveryPrefsManager.MAP)){
            return;
        }
        if(mSearchRestService != null) {
            mSearchRestService.resetSearchCriteria();
            mapManager.reset();
            mClusterManager.clearItems();
            updateSearchResult();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext).addConnectionCallbacks(this).addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    protected void updateSearchResult(){
        updateSearchResult(null);
    }
    protected void updateSearchResult(LatLngBounds bounds) {
//        mapManager.startSearch();
    }


    protected void initViews() {
        mEmptyResultHeaderTV.setText(getString(R.string.empty_search_header));
        mEmptyResultSubtextTV.setText(getString(R.string.empty_search_subtext));
        mEmptyLayoutButtonTV.setText(getString(R.string.empty_search_button));
        mEmptyLayoutButtonTV.setVisibility(View.VISIBLE);
    }

    private void initMap() {
        if (mGoogleMap != null) {
            mGoogleMap.setOnMapClickListener(this);
            mClusterManager = new MapClusterManager(mContext, mGoogleMap);

        }
    }

    private void startUserProfileActivity(ResultItemModel item) {
        Intent intent = new Intent(getActivity(), UserProfileActivity.class);
        intent.putExtra(UserProfileActivity.USER_ID, item.getUser().getId());
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }


    private void startListListingViewActivity(LatLngBounds bounds) {
        Intent intent = new Intent(getActivity(), MapListViewActivity.class);
        if(bounds != null){
            intent.putExtra(MapClusterListFragment.KEY_BOUNDS,bounds);
        }
        startActivity(intent);
    }
    private void startListListingViewActivity() {
        startListListingViewActivity(null);
    }

    private MarkerOptions getMarker(Location location, int drawableId) {
        return new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude()))
                .icon(BitmapDescriptorFactory.fromResource(drawableId));
    }


    private void showSearchResultsOrEmptyStateView() {
        if(!isScreenActive()) {
            return;
        }
        if (haveSearchResult()) {
            hideEmptyResult();

            if(mIsDialogShown){
                hasDelayedResult = true;
                return;
            }
            invalidateClusters();
        } else {
            showEmptyResult();
        }
    }
    boolean isScreenActive(){
        return getActivity() != null && isAdded() && !isDetached();
    }

    private MarkerRenderer mMarkerRenderer;
    private ArrayList<Marker> gridMarkers = new ArrayList<>();
    private ArrayList<ResultItemModel> gridModels = new ArrayList<>();
    private void invalidateClusters() {
        hasDelayedResult = false;
        List<ResultItemModel> list = mapManager.getMapItems(mGoogleMap.getProjection().getVisibleRegion().latLngBounds);
        ArrayList<ResultItemModel> listingsModels = new ArrayList<>();

//       showDebugGrid();
        if(shouldInvalidateGridMarkers){
            clearGridMarkers();
            shouldInvalidateGridMarkers = false;
        }
            for (ResultItemModel itemModel : list) {
                if (itemModel.isCountCluster()) {
                    addUnclusteredCountMarker(itemModel);
                } else {
                    listingsModels.add(itemModel);
                }
            }
            mClusterManager.clearItems();
            mClusterManager.addItems(listingsModels);

            callMapUpdate();
    }

    void clearGridMarkers(){
        for(Marker m : gridMarkers){
            m.remove();
        }
        boundsMap.clear();
        gridMarkers.clear();
        gridModels.clear();
    }

//    void  showDebugGrid(){
//        for(MapCluster cluster : mapManager.getCurrentClusters()){
//            LatLng ne = cluster.getBounds().northeast;
//            LatLng sw = cluster.getBounds().southwest;
//            PolylineOptions ops = new PolylineOptions();
//            ops.add(sw)
//                    .add(new LatLng(ne.latitude,sw.longitude))
//                    .add(ne)
//                    .add(new LatLng(sw.latitude,ne.longitude))
//                    .add(sw);
//            ops.color(cluster.loaded ? Color.RED : Color.BLACK);
//            mGoogleMap.addPolyline(ops);
//        }
//    }

    public void callMapUpdate(){
        if(!mapInited) {
            initMapClustering();
        } else {
            mClusterManager.cluster();
            markerImageManager.checkWaitingImages();
        }
    }

    @Override
    public void onFinished() {
//        Handler handler = getHandler();
//        handler.removeCallbacks(mImagesRedrawTask);
//        handler.postDelayed(mImagesRedrawTask,1000);
    }

    public void initMapClustering(){
        mapInited = true;
        mMarkerRenderer = new MarkerRenderer();
        mClusterManager.setRenderer(mMarkerRenderer);
        mGoogleMap.animateCamera(
                CameraUpdateFactory.newLatLngBounds(mSearchRestService.getSearchCriteria().getSearchBounds(),0));
        mGoogleMap.setOnCameraChangeListener(this); //FIXME
        mMapMarkerAdapter = new MapMarkerAdapter();
        mClusterManager.getClusterMarkerCollection().setOnInfoWindowAdapter(mMapMarkerAdapter);
        mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(mMapMarkerAdapter);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);
        mClusterManager.setOnClusterClickListener(this);
//        mClusterManager.setAlgorithm(new GridBasedAlgorithm<ResultItemModel>());
        mGoogleMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());
        mGoogleMap.setOnInfoWindowClickListener(mClusterManager);
        mGoogleMap.setOnMarkerClickListener(mClusterManager);
    }




    private boolean haveSearchResult() {
        return mapManager.hasResults();
    }



    private void showEmptyResult() {
        mMapView.setVisibility(View.GONE);
        mEmptyLayout.setVisibility(View.VISIBLE);
    }


    private void hideEmptyResult() {
        mMapView.setVisibility(View.VISIBLE);
        mEmptyLayout.setVisibility(View.GONE);
    }


    private Runnable mImagesRedrawTask = new Runnable() {

        @Override
        public void run() {
            //redraw only if there is no dialog shown
            if (!mIsDialogShown) {
                mMarkerRenderer = new MarkerRenderer();
                mClusterManager.setRenderer(mMarkerRenderer);
            }
        }
    };





    private Handler getHandler() {
        if (mHandler == null) {
            mHandler = new Handler();
        }
        return mHandler;
    }
    private float prevZoom = -1;
    private boolean shouldInvalidateGridMarkers = false;
    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        if(mIsDialogShown)
            return;
        if(prevZoom != cameraPosition.zoom){
            shouldInvalidateGridMarkers = true;
        }
        prevZoom = cameraPosition.zoom;
        mapManager.onBoundsChanged(mGoogleMap.getProjection().getVisibleRegion().latLngBounds,
                mGoogleMap.getCameraPosition().zoom);
    }

    @Override
    public void onUpdate() {
        showSearchResultsOrEmptyStateView();
    }



    private class MapClusterManager extends ClusterManager<ResultItemModel> {

        public MapClusterManager(Context context, GoogleMap map) {
            super(context, map);
        }


        @Override
        public void onCameraChange(CameraPosition cameraPosition) {
            super.onCameraChange(cameraPosition);
        }


        @Override
        public void onInfoWindowClick(Marker marker) {
            super.onInfoWindowClick(marker);
            mIsDialogShown = false;
        }

        @Override
        public boolean onMarkerClick(Marker marker) {
            if (boundsMap.containsKey(marker)) {
                LatLngBounds bounds = boundsMap.get(marker);
                if(bounds == null){
                    return false;
                }
                startListListingViewActivity(bounds);
                return true;
            }
            return super.onMarkerClick(marker);
        }
    }
    private IconGenerator countIconGenerator = null;
    private TextView countTextView = null;
    private void addUnclusteredCountMarker(ResultItemModel itemModel){
        if(gridModels.contains(itemModel)) {
            return;
        }
        if(getActivity() == null || getActivity().isFinishing()) {
            return;
        }
        if(itemModel.count == 0) {
            return;
        }
        if(countTextView == null){
            initIconGenerator();
        }
        if(itemModel.count == 0) {
            return;
        }
        MarkerOptions ops = new MarkerOptions();
        ops.position(itemModel.getPosition());
        countTextView.setText(String.valueOf(itemModel.count));
        Bitmap icon = countIconGenerator.makeIcon();
        ops.icon(BitmapDescriptorFactory.fromBitmap(icon));
        Marker m = mGoogleMap.addMarker(ops);

        gridMarkers.add(m);
        gridModels.add(itemModel);
        boundsMap.put(m,itemModel.getCustomBounds());
    }

    private void initIconGenerator(){
        countIconGenerator = new IconGenerator(getContext());
        View multiProfile = mLayoutInflater.inflate(R.layout.map_cluster_layout, null);
        countTextView = (TextView) multiProfile.findViewById(R.id.map_cluster_number);
        countIconGenerator.setBackground(null);
        countIconGenerator.setContentView(multiProfile);
    }

    private class MarkerRenderer extends DefaultClusterRenderer<ResultItemModel> {

        private IconGenerator mIconGenerator = new IconGenerator(mContext);
        private IconGenerator mIconReGenerator = new IconGenerator(mContext);
        private IconGenerator mClusterIconGenerator = new IconGenerator(mContext);
        private TextView mClusterTextView;
        private CircleImageView mFaceView;
        private CircleImageView mFaceViewRecreate;

        public MarkerRenderer() {
            super(mContext, mGoogleMap, mClusterManager);
            View multiProfile = mLayoutInflater.inflate(R.layout.map_cluster_layout, null);
            mClusterTextView = (TextView) multiProfile.findViewById(R.id.map_cluster_number);
            mClusterIconGenerator.setBackground(null);
            mClusterIconGenerator.setContentView(multiProfile);

            View profile = mLayoutInflater.inflate(R.layout.map_pin_layout, null);
            View profileForRecreate = mLayoutInflater.inflate(R.layout.map_pin_layout, null);
            mFaceView = (CircleImageView) profile.findViewById(R.id.map_pin_face);
            mFaceViewRecreate = (CircleImageView) profileForRecreate.findViewById(R.id.map_pin_face);
            mIconGenerator.setBackground(null);
            mIconGenerator.setContentView(profile);
            mIconReGenerator.setBackground(null);
            mIconReGenerator.setContentView(profileForRecreate);

        }

        public BitmapDescriptor redrawIcon(Drawable drawable){
            mFaceViewRecreate.setImageDrawable(drawable);
            return BitmapDescriptorFactory.fromBitmap(mIconReGenerator.makeIcon());
        }

        @Override
        protected void onBeforeClusterItemRendered(ResultItemModel resultItemModel, final MarkerOptions markerOptions) {
            if(resultItemModel.getUser() == null)
                return;
            String url = resultItemModel.getUser().getTitleImage();
            ImageHelper.loadCircleImageIntoMapMarker(mContext, url, mFaceView,markerImageManager);
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.title(url);
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }


        @Override
        protected void onBeforeClusterRendered(Cluster<ResultItemModel> cluster, MarkerOptions markerOptions) {
            mClusterTextView.setText(String.valueOf(cluster.getSize()));
            Bitmap icon = mClusterIconGenerator.makeIcon();
            markerOptions.title(null);
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }


        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            return cluster.getSize() > 1;
        }
    }


    private class MarkerImageManager implements ImageHelper.MapImageLoadedListener {
        private HashMap<String,Drawable> markerImages = new HashMap<>();
        @Override
        public void onMapImageLoaded(String url, Drawable drawable) {
           markerImages.put(url,drawable);
            if(isDetached() || getActivity() == null || getActivity().isFinishing())
                return;
            checkWaitingImages();
        }

        public void checkWaitingImages(){
            ArrayList<String> loaded = new ArrayList<>();
            for(String keyUrl : markerImages.keySet()){
                if(setToMarker(keyUrl,markerImages.get(keyUrl))){
                    loaded.add(keyUrl);
                }
            }
            for(String key : loaded){
                markerImages.remove(key);
            }

        }

        private boolean setToMarker(String url, Drawable drawable){
            MarkerManager.Collection markerCollection = mClusterManager.getMarkerCollection();
            Collection<Marker> markers = markerCollection.getMarkers();
            for (Marker m : markers) {
                if (url.equals(m.getTitle())) {
                    m.setIcon(mMarkerRenderer.redrawIcon(drawable));
                    return true;
                }
            }
            return false;
        }
    }
}
