package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import java.util.Date;


public class CardGetModel {

    /**
     **/
    @SerializedName("id")
    private Integer id = null;

    /**
     **/
    @SerializedName("type")
    private CreditCardType type = null;

    /**
     **/
    @SerializedName("last_four_digits")
    private String lastFourDigits = null;

    /**
     **/
    @SerializedName("exp_date")
    private Date expDate = null;

    /**
     **/
    @SerializedName("first_name")
    private String firstName = null;

    /**
     **/
    @SerializedName("last_name")
    private String lastName = null;

    /**
     **/
    @SerializedName("billing_address")
    private BillingAddress billingAddress = null;


    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public CreditCardType getType() {
        return type;
    }


    public void setType(CreditCardType type) {
        this.type = type;
    }


    public String getLastFourDigits() {
        return lastFourDigits;
    }


    public void setLastFourDigits(String lastFourDigits) {
        this.lastFourDigits = lastFourDigits;
    }


    public Date getExpDate() {
        return expDate;
    }


    public void setExpDate(Date expDate) {
        this.expDate = expDate;
    }


    public String getFirstName() {
        return firstName;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public BillingAddress getBillingAddress() {
        return billingAddress;
    }


    public void setBillingAddress(BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class CardGetModel {\n");

        sb.append("  id: ").append(id).append("\n");
        sb.append("  type: ").append(type).append("\n");
        sb.append("  lastFourDigits: ").append(lastFourDigits).append("\n");
        sb.append("  expDate: ").append(expDate).append("\n");
        sb.append("  firstName: ").append(firstName).append("\n");
        sb.append("  lastName: ").append(lastName).append("\n");
        sb.append("  billingAddress: ").append(billingAddress).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
