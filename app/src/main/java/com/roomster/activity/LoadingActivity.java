package com.roomster.activity;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.roomster.R;
import com.roomster.constants.IntentExtras;
import com.roomster.event.application.ApplicationEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


public class LoadingActivity extends AppCompatActivity {

    private boolean           mShouldStop;
    private boolean           mIsResumed;
    private AnimationDrawable mAnimation;
    private Handler           mHandler;

    private final BroadcastReceiver mFinishActivityReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            mShouldStop = true;
            if (mIsResumed) {
                mHandler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        finish();
                        overridePendingTransition(0, android.R.anim.fade_out);
                    }
                }, 1000);
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(android.R.anim.fade_in, 0);
        setContentView(R.layout.activity_loading);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mHandler = new Handler();
        EventBus.getDefault().register(this);
//        registerReceiver(mFinishActivityReceiver, new IntentFilter(IntentExtras.LOADING_INTENT_FILTER));
        initAnimation();
    }


    @Override
    protected void onResume() {
        super.onResume();
        mIsResumed = true;
        if (mShouldStop) {
            mHandler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    finish();
                    overridePendingTransition(0, android.R.anim.fade_out);
                }
            }, 1000);
        }
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (mAnimation != null) {
            mAnimation.start();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        mIsResumed = false;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
//        unregisterReceiver(mFinishActivityReceiver);
    }


    private void initAnimation() {
        ImageView loadingImage = (ImageView) findViewById(R.id.loading_image);
        if (loadingImage != null) {
            loadingImage.setBackgroundResource(R.drawable.loading_animation);
            mAnimation = (AnimationDrawable) loadingImage.getBackground();
        }
    }


    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ApplicationEvent.HideLoadingSticky event) {
        mShouldStop = true;
        if (mIsResumed) {
            mHandler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    finish();
                    overridePendingTransition(0, android.R.anim.fade_out);
                }
            }, 1000);
        }
    }


    @Override
    public void onBackPressed() {
        //do nothing
    }
}
