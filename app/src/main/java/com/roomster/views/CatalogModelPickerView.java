/***************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ***************************************************************/
package com.roomster.views;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.roomster.R;
import com.roomster.dialog.CatalogListChooserDialog;
import com.roomster.model.CatalogModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class CatalogModelPickerView extends RelativeLayout {

    public interface ItemsSelectedListener {

        void onItemsSelected(List<Integer> selectedItems);
    }


    private List<CatalogModel>    mOptions;
    private List<CatalogModel>    mSelectedOptions;
    private List<Integer>         mSelectedIds;
    private boolean               mIsMultipleChoice;
    private ItemsSelectedListener mListener;

    @Bind(R.id.catalog_model_picker_title)
    TextView mTitle;

    @Bind(R.id.catalog_model_picker_value)
    TextView mValue;

    @Bind(R.id.catalog_model_picker_arrow)
    ImageView mArrow;


    public CatalogModelPickerView(Context context) {
        super(context);
        init();
    }


    public CatalogModelPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    public CatalogModelPickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    public void setOptions(String title, List<CatalogModel> options, List<Integer> selectedIds, boolean isMultipleChoice) {
        if (options != null) {
            mOptions = options;
        } else {
            mOptions = new ArrayList<>();
        }
        mIsMultipleChoice = isMultipleChoice;
        mSelectedOptions = new ArrayList<>();

        if (selectedIds != null) {
            mSelectedIds = selectedIds;
        } else {
            mSelectedIds = new ArrayList<>();
        }
        updateSelectedOptionsByIds();
        updateValueText();

        if (title != null) {
            mTitle.setText(title);
        } else {
            mTitle.setText("");
        }
    }


    public void setOnItemsSelectedListener(ItemsSelectedListener listener) {
        mListener = listener;
    }


    public List<Integer> getSelectedIds() {
        return mSelectedIds;
    }


    private void init() {
        inflate(getContext(), R.layout.view_catalog_model_picker, this);
        ButterKnife.bind(this);
        setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                CatalogListChooserDialog sharedDetailsChooserDialog = new CatalogListChooserDialog(getContext(), mOptions,
                  mSelectedIds, mIsMultipleChoice, new CatalogListChooserDialog.OKButtonClickListener() {

                    @Override
                    public void onOkClick(List<Integer> categoryIds) {
                        if (categoryIds != null) {
                            mSelectedIds = categoryIds;
                            updateSelectedOptionsByIds();
                            updateValueText();
                            if (mListener != null) {
                                mListener.onItemsSelected(mSelectedIds);
                            }
                        }
                    }
                }, mTitle.getText().toString());
                sharedDetailsChooserDialog.show();
            }
        });
    }


    private void updateValueText() {
        if (mSelectedOptions != null && !mSelectedOptions.isEmpty()) {
            if (!mIsMultipleChoice) {
                mValue.setText(mSelectedOptions.get(0).getName());
            } else {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < mSelectedOptions.size(); i++) {
                    CatalogModel option = mSelectedOptions.get(i);
                    sb.append(option.getName());
                    if (i != mSelectedOptions.size() - 1) {
                        sb.append(", ");
                        if ((i + 1) % 2 == 0) {
                            sb.append('\n');
                        }
                    }
                }
                mValue.setText(sb.toString());
            }
            mArrow.setVisibility(View.GONE);
        } else {
            mValue.setText("");
            mArrow.setVisibility(View.VISIBLE);
        }
    }


    private void updateSelectedOptionsByIds() {
        if (mSelectedIds != null) {
            mSelectedOptions.clear();
            for (CatalogModel option : mOptions) {
                if (mSelectedIds.contains(option.getId())) {
                    mSelectedOptions.add(option);
                }
            }
        }
    }
}
