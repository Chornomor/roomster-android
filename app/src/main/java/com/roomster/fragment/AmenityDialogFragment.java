package com.roomster.fragment;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.roomster.R;
import com.roomster.adapter.AmenitiesAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.manager.CatalogsManager;
import com.roomster.manager.DiscoveryPrefsManager;
import com.roomster.model.CatalogValueModel;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;


/**
 * Created by "Michael Katkov" on 11/10/2015.
 */
public class AmenityDialogFragment extends DialogFragment {

    public static final String TAG                      = "AmenityDialogFragment";
    public static final String DIALOG_TYPE              = "dialog_type";
    public static final int    ROOM_TYPE                = 1;
    public static final int    APARTMENT_TYPE           = 2;
    public static final float  TOUCHED                  = 0.4f;
    public static final float  NORMAL                   = 1f;
    public static final int    AMENITY_IMAGE_VIEW_INDEX = 0;
    public static final int    AMENITY_IMAGE_TEXT_INDEX = 1;

    private int mDialogType;

    @Bind(R.id.amenity_grid)
    GridView mAmenitiesGrid;

    @Bind(R.id.amenities_dialog_layout)
    FrameLayout mAmenitiesLayout;

    @Inject
    DiscoveryPrefsManager mDPManager;

    @Inject
    CatalogsManager mCatalogsManager;


    public static AmenityDialogFragment newInstance(int type) {
        AmenityDialogFragment fragment = new AmenityDialogFragment();
        Bundle args = new Bundle();
        args.putInt(DIALOG_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mDialogType = getArguments().getInt(DIALOG_TYPE);
        }
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_amenity, container, false);
        ButterKnife.bind(this, view);
        setAdapter();
        return view;
    }


    private void setAdapter() {
        AmenitiesAdapter amenitiesAdapter = null;
        switch (mDialogType) {
            case ROOM_TYPE:
                amenitiesAdapter = new AmenitiesAdapter(getActivity(), mCatalogsManager.getRoomAmenitiesList(),
                  mDPManager.restoreRoomAmenities());
                break;
            case APARTMENT_TYPE:
                amenitiesAdapter = new AmenitiesAdapter(getActivity(), mCatalogsManager.getApartmentAmenitiesList(),
                  mDPManager.restoreApartmentAmenities());
                break;
        }
        mAmenitiesGrid.setAdapter(amenitiesAdapter);
    }


    @OnItemClick(R.id.amenity_grid)
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        vizualizeClick(view);
        CatalogValueModel amenityModel = (CatalogValueModel) parent.getItemAtPosition(position);
        switch (mDialogType) {
            case ROOM_TYPE:
                mDPManager.storeRoomAmenities(amenityModel);
                break;
            case APARTMENT_TYPE:
                mDPManager.storeApartmentAmenities(amenityModel);
                break;
        }
    }


    private void vizualizeClick(View view) {
        LinearLayout layout = (LinearLayout) view;
        View amenityImage = layout.getChildAt(AMENITY_IMAGE_VIEW_INDEX);
        View amenityText = layout.getChildAt(AMENITY_IMAGE_TEXT_INDEX);
        if (amenityImage.getAlpha() == TOUCHED) {
            amenityImage.setAlpha(NORMAL);
            amenityText.setAlpha(NORMAL);
        } else {
            amenityImage.setAlpha(TOUCHED);
            amenityText.setAlpha(TOUCHED);
        }
    }


    @OnClick(R.id.amenities_dialog_layout)
    public void OnLayoutClick() {
        dismiss();
    }
}
