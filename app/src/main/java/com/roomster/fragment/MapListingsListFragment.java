package com.roomster.fragment;


import android.content.Context;
import com.roomster.application.RoomsterApplication;
import com.roomster.manager.SearchManager;
import com.roomster.rest.model.ResultItemModel;

import javax.inject.Inject;
import java.util.List;


public class MapListingsListFragment extends ListingListViewFragment {

    @Inject
    SearchManager mSearchManager;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    @Override
    protected boolean hasMoreListings() {
        return false;
    }


    @Override
    protected List<ResultItemModel> getListings() {
        return (List<ResultItemModel>) mSearchManager.getMapViewItems();
    }
}
