package com.roomster.enums;

public enum GenderEnum {
    Unknown, Female, Male
}
