package com.roomster.rest.service;


import com.roomster.application.RoomsterApplication;
import com.roomster.event.RestServiceEvents.ListingsRestServiceEvent;
import com.roomster.event.RestServiceEvents.MeRestServiceEvent;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.event.listing.ListingDeletedEvent;
import com.roomster.manager.MeManager;
import com.roomster.manager.NetworkManager;
import com.roomster.manager.TokenManager;
import com.roomster.rest.api.MeApi;
import com.roomster.rest.model.UserGetViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by "Michael Katkov" on 10/10/2015.
 */
public class MeRestService {

    private boolean mIsGetMeInProgress;

    @Inject
    EventBus mEventBus;

    @Inject
    MeApi mMeApi;

    @Inject
    TokenManager mTokenManager;

    @Inject
    MeManager mMeManager;

    @Inject
    NetworkManager mNetworkManager;


    public MeRestService() {
        RoomsterApplication.getRoomsterComponent().inject(this);
        mIsGetMeInProgress = false;
        // register with max priority
        mEventBus.register(this);
    }


    public void requestMe() {
        callRequestMe();
    }


    @Subscribe(threadMode = ThreadMode.MAIN, priority = Integer.MAX_VALUE)
    public void onEventMainThread(ListingDeletedEvent event) {
        // update the current user model while a refreshed one is obtained in the main thread at max priority
        // to ensure the affected views will be updated quickly
        UserGetViewModel currentUser = mMeManager.getMe();
        currentUser.setTotalListings(currentUser.getTotalListings() - 1);
        mMeManager.getMyListings().remove(mMeManager.findListingById(event.listingId));
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND, priority = Integer.MAX_VALUE)
    public void onEventBackgroundThread(ListingDeletedEvent event) {
        callRequestMe();
    }


    @Subscribe(threadMode = ThreadMode.MAIN, priority = Integer.MAX_VALUE)
    public void onEventMainThread(ListingsRestServiceEvent.AddListingSuccess event) {
        UserGetViewModel currentUser = mMeManager.getMe();
        currentUser.setTotalListings(currentUser.getTotalListings() + 1);
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND, priority = Integer.MAX_VALUE)
    public void onEventBackgroundThread(ListingsRestServiceEvent.AddListingSuccess event) {
        callRequestMe();
    }


    private void callRequestMe() {
        if (!mIsGetMeInProgress) {
            synchronized (this) {
                if (!mIsGetMeInProgress) {
                    mIsGetMeInProgress = true;
                    mTokenManager.getAccessToken(new RestServiceListener<String>() {

                        @Override
                        public void onSuccess(String token) {
                            if (mNetworkManager.hasNetworkAccess()) {
                                Call<UserGetViewModel> call = mMeApi.meGet(token);
                                call.enqueue(new Callback<UserGetViewModel>() {

                                    @Override
                                    public void onResponse(Call<UserGetViewModel> call, Response<UserGetViewModel> response) {
                                        mIsGetMeInProgress = false;
                                        switch (response.code()) {
                                            case ResponseCode.OK:
                                                mMeManager.setMe(response.body());
                                                mEventBus.post(new MeRestServiceEvent.GetMeSuccess());
                                                break;
                                            case ResponseCode.UNAUTHORIZED:
                                                sendFailure(true);
                                                break;
                                            case ResponseCode.FORBIDDEN:
                                            case ResponseCode.SERVER_ERROR:
                                            default:
                                                sendFailure(false);
                                        }
                                    }


                                    @Override
                                    public void onFailure(Call<UserGetViewModel> call, Throwable t) {
                                        mIsGetMeInProgress = false;
                                        sendFailure(false);
                                    }
                                });
                            } else {
                                mIsGetMeInProgress = false;
                                sendFailure(false);
                            }
                        }


                        @Override
                        public void onFailure(int code, Throwable throwable, String msg) {
                            mEventBus.post(new MeRestServiceEvent.GetMeFailure());
                        }
                    });
                }
            }
        }
    }


    private void sendFailure(boolean needReLogin) {
        MeRestServiceEvent.GetMeFailure failure = new MeRestServiceEvent.GetMeFailure();
        failure.needReLogin = needReLogin;
        mEventBus.post(failure);
    }
}