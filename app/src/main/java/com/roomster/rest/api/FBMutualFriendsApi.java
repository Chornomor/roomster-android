package com.roomster.rest.api;


import com.roomster.rest.model.MutualFriendsModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface FBMutualFriendsApi {

    /**
     * @param userId
     * @param userAccessToken
     * @return Call<Object>
     */

    @GET("FBMutualFriends")
    Call<MutualFriendsModel> fBMutualFriendsGet(@Query("user_id") Long userId,
                                                @Query("user_access_token") String userAccessToken);
}
