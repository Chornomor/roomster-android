package com.roomster.receiver;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.roomster.BuildConfig;
import com.roomster.application.RoomsterApplication;
import com.roomster.cache.sharedpreferences.SharedPreferencesFiles;
import com.roomster.manager.FacebookManager;
import com.roomster.manager.SearchManager;
import com.roomster.manager.TokenManager;

import javax.inject.Inject;


/**
 * Receiver for application upgrades
 */
public class AppUpgradeReceiver extends BroadcastReceiver {

    @Inject
    TokenManager mTokenManager;

    @Inject
    SharedPreferencesFiles mSharedPreferencesFiles;

    @Inject
    SearchManager mSearchManager;

    @Inject
    FacebookManager mFacebookManager;


    @Override
    public void onReceive(Context context, Intent intent) {
        if (!BuildConfig.DEBUG) {
            RoomsterApplication.getRoomsterComponent().inject(this);
            mTokenManager.clearToken();
            mSharedPreferencesFiles.getUserSharedPreferences().edit().clear().commit();
            mSearchManager.clearResultModel();
            mFacebookManager.logout();
        }
    }
}
