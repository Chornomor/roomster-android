package com.roomster.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.ImageView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.roomster.R;
import com.roomster.adapter.DetailedViewPageAdapter;
import com.roomster.application.RoomsterApplication;
import com.roomster.event.fragment.FragmentRequest;
import com.roomster.manager.CatalogsManager;
import com.roomster.manager.SearchManager;
import com.roomster.model.CatalogModel;
import com.roomster.rest.model.ListingGetViewModel;
import com.roomster.rest.model.ResultItemModel;
import com.roomster.utils.FancyPageTransformer;
import com.roomster.views.NonSwipableViewPager;
import com.roomster.views.ReplacableViewPager;
import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class DetailedListingListFragment extends Fragment {

    private static final int OFFSCREEN_PAGE_LIMIT = 2;

    private static final String NEED_ROOM      = "NeedRoom";
    private static final String HAVE_SHARE     = "HaveShare";
    private static final String NEED_APARTMENT = "NeedApartment";
    private static final String HAVE_APARTMENT = "HaveApartment";
    private static final String LISTING_INDEX  = "listing_index";
    private static final String LISTINGS_LIST  = "listings_list";

    @Bind(R.id.my_listing_view_pager)
    NonSwipableViewPager mViewPager;

    @Bind(R.id.my_listing_view_right_arrow)
    ImageView mRightArrow;

    @Bind(R.id.my_listing_view_left_arrow)
    ImageView mLeftArrow;

    @Inject
    Context mContext;

    @Inject
    EventBus mEventBus;

    @Inject
    CatalogsManager mCatalogsManager;

    @Inject
    SearchManager mSearchManager;

    private DetailedViewPageAdapter mPagerAdapter;

    private int mListingIndex;

    private ArrayList<ResultItemModel> mListings;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RoomsterApplication.getRoomsterComponent().inject(this);
    }


    public static DetailedListingListFragment newInstance(List<ResultItemModel> listings, int initialListingIndex) {
        DetailedListingListFragment fragment = new DetailedListingListFragment();
        Bundle args = new Bundle();
        args.putInt(LISTING_INDEX, initialListingIndex);
        args.putSerializable(LISTINGS_LIST, new ArrayList<>(listings));
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mListingIndex = getArguments().getInt(LISTING_INDEX);
            Serializable list = getArguments().getSerializable(LISTINGS_LIST);
            if (list != null) {
                mListings = (ArrayList<ResultItemModel>) list;
            }
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_listing, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        getActivity().invalidateOptionsMenu();
        updateTitle(mViewPager.getCurrentItem());
    }


    private void initView() {
        FancyPageTransformer transformer = new FancyPageTransformer(mViewPager, mLeftArrow, mRightArrow);
        mPagerAdapter = new DetailedViewPageAdapter(getChildFragmentManager(), mListings, mSearchManager);
        mViewPager.setAdapter(mPagerAdapter);
        if (0 < mListingIndex && mListingIndex < mPagerAdapter.getCount()) {
            mViewPager.setCurrentItem(mListingIndex);
        }
        mViewPager.addOnPageChangeListener(new ReplacableViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                updateTitle(position);
            }
        });

        mViewPager.setOffscreenPageLimit(OFFSCREEN_PAGE_LIMIT);
    }


    private void updateTitle(int currentIndex) {
        String title = getString(R.string.listings);
        mEventBus.post(new FragmentRequest.SetTitleRequest(title, null));
    }


    private String getListingType(ListingGetViewModel listingGetViewModel) {
        CatalogModel serviceTypeModel = mCatalogsManager.getServiceTypeById(listingGetViewModel.getServiceType());
        if (serviceTypeModel == null) {
            return null;
        }
        switch (serviceTypeModel.getName()) {
        case NEED_ROOM:
            return getString(R.string.looking_for_room);
        case HAVE_SHARE:
            return getString(R.string.offering_room);
        case NEED_APARTMENT:
            return getString(R.string.looking_for_apartment);
        case HAVE_APARTMENT:
            return getString(R.string.al_offering_apartment);
        default:
            return null;
        }
    }
}