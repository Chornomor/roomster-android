package com.roomster.views.signin;

import android.content.Context;
import android.graphics.Typeface;
import android.transition.Slide;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Created by andreybofanov on 18.07.16.
 */
public class AbstactSlidePage extends FrameLayout implements SlidePage {
    ViewGroup layout;
    Typeface typeface;
    public AbstactSlidePage(Context context) {
        super(context);
        initTypeFace();
    }

    public AbstactSlidePage(Context context, AttributeSet attrs) {
        super(context, attrs);
        initTypeFace();
    }

    public AbstactSlidePage(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initTypeFace();
    }
    void initTypeFace(){
        typeface = Typeface.createFromAsset(getContext().getAssets(),"fonts/Comfortaa-Regular.ttf");
    }

    @Override
    public void setBottomMargin(int margin) {
        LayoutParams params = (LayoutParams) layout.getLayoutParams();
        params.bottomMargin = margin;
        layout.setLayoutParams(params);
    }

    @Override
    public void setHorizontalOffset(float offset) {
        float alpha = 1.0f-offset;
        layout.setAlpha(alpha);
        for(int i = 0; i < layout.getChildCount(); i++){
            View v = layout.getChildAt(i);
            v.setAlpha(alpha);
        }
    }

    @Override
    public void setContentVisibility(boolean isVisible) {
        layout.setVisibility(isVisible? VISIBLE : GONE);
    }

    public void setTypefaces(){
        setTypeface(layout);
    }

    void setTypeface(ViewGroup in){
        for (int i = 0; i < in.getChildCount(); i++){
            View v = in.getChildAt(i);
            if(v instanceof TextView)
                ((TextView)v).setTypeface(typeface);
            else if(v instanceof ViewGroup)
                setTypeface((ViewGroup) v);
        }
    }
}
