package com.roomster.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by yuliasokolova on 26.10.16.
 */
public class DisputeModel {
    @SerializedName("Id")
    private int id;
    @SerializedName("TransId")
    private int transId;
    @SerializedName("ProviderId")
    private int providerId;
    @SerializedName("DisputedDate")
    private Date disputedDate;
    @SerializedName("ChargedBack")
    private boolean chargedBack;
    @SerializedName("Reversed")
    private boolean reversed;

    public int getId() {
        return id;
    }

    public int getTransId() {
        return transId;
    }

    public int getProviderId() {
        return providerId;
    }

    public Date getDisputedDate() {
        return disputedDate;
    }

    public boolean isChargedBack() {
        return chargedBack;
    }

    public boolean isReversed() {
        return reversed;
    }
}
