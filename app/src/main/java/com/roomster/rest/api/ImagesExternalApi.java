package com.roomster.rest.api;


import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface ImagesExternalApi {

    /**
     * Upload image from external source
     *
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @param url
     *   Url of the image
     * @param setMain
     *   Make image primary (avatar)
     */
    @POST("ImagesExternal/")
    Call<Object> imagesExternalPost(@Header("Authorization") String authorization, @Query("url") String url,
                                    @Query("set_main") Boolean setMain);
}
