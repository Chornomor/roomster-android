package com.roomster.manager;


import com.google.gson.Gson;

import com.roomster.application.RoomsterApplication;
import com.roomster.cache.sharedpreferences.SharedPreferencesFiles;
import com.roomster.event.RestServiceEvents.RestServiceListener;
import com.roomster.rest.model.TokenModel;
import com.roomster.rest.service.TokenRestService;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class TokenManager {

    public static final String TOKEN_MANAGER_SP_KEY = "TOKEN_MANAGER_SP_KEY";

    private static final long SECONDS_TOLERANCE = 60;

    @Inject
    transient TokenRestService mTokenService;

    @Inject
    transient SharedPreferencesFiles mSharedPreferencesFiles;

    @Inject
    transient EventBus mEventBus;

    @Inject
    transient MeManager mMeManager;

    private TokenModel mCurrentToken;

    private long mTokenSetTimestampSeconds;

    //TODO add better way to represent external provider
    private String mExternalProvider;
    private String mExternalProviderAccessToken;
    private String mExternalAccessTokenSecret = "";

    private transient boolean                   mIsGettingToken;
    private transient List<RestServiceListener> mGetTokenListeners;
    private transient Object                    mLock;


    public TokenManager() {
        RoomsterApplication.getRoomsterComponent().inject(this);
        mIsGettingToken = false;
        mGetTokenListeners = new ArrayList<RestServiceListener>();
        mLock = new Object();
    }


    public String getExternalProvider() {
        return mExternalProvider;
    }


    public void setExternalProvider(String externalProvider) {
        mExternalProvider = externalProvider;
    }


    public String getExternalProviderAccessToken() {
        return mExternalProviderAccessToken;
    }


    public void setExternalProviderAccessToken(String externalProviderAccessToken) {
        mExternalProviderAccessToken = externalProviderAccessToken;
    }


    public TokenModel getCurrentTokenModel() {
        return mCurrentToken;
    }


    public void setCurrentTokenModel(TokenModel currentToken) {
        mTokenSetTimestampSeconds = getTimeNowSeconds();
        mCurrentToken = currentToken;
        storeToSharedPreferences();
    }


    public String getCurrentAccessToken() {
        if (mCurrentToken != null) {
            return "Bearer " + mCurrentToken.getToken();
        } else {
            return null;
        }
    }


    public boolean getIsNewUser () {
        if (mMeManager != null) {
            return mMeManager.getIsNewUser();
        } else {
            return false;
        }
    }


    /**
     * Returns the access token if cached or tries to get it otherwise
     *
     * @param {@RestServiceListener}
     *   callback, called on success getting a token and on fail otherwise.
     */
    public void getAccessToken(final RestServiceListener<String> listener) {
        if (hasValidToken()) {
            listener.onSuccess(getCurrentAccessToken());
        } else {
            //If the token is expired, we should remove the saved user from cache.
            mMeManager.setMe(null);
            refreshToken(listener);
        }
    }


    public boolean hasValidToken() {
        if (mCurrentToken == null) {
            return false;
        }
        return mTokenSetTimestampSeconds + mCurrentToken.getExpiresIn() > getTimeNowSeconds() + SECONDS_TOLERANCE;
    }


    /**
     * Deletes stored Bearer token
     */
    public void clearToken() {
        mCurrentToken = null;
        mExternalProvider = null;
        mExternalProviderAccessToken = null;
        mTokenSetTimestampSeconds = 0;
        storeToSharedPreferences();
        mMeManager.setMe(null);
    }


    /**
     * Refresh the current access token with the current external provider info.
     **/
    public void refreshToken(final RestServiceListener<String> listener) {
        synchronized (mLock) {
            if (hasValidToken()) {
                listener.onSuccess(getCurrentAccessToken());
                return;
            }

            synchronized (mGetTokenListeners) {
                mGetTokenListeners.add(listener);
            }

            if (!mIsGettingToken) {
                mIsGettingToken = true;
                mTokenService.requestToken(mExternalProvider, mExternalProviderAccessToken, mExternalAccessTokenSecret,
                  new RestServiceListener<TokenModel>() {

                      @Override
                      public void onSuccess(TokenModel result) {
                          setCurrentTokenModel(result);
                          if (result != null) {
                              mMeManager.setMe(result.getUser());
                          }
                          notifyAllGetTokenListenersSuccess();
                          mIsGettingToken = false;
                      }


                      @Override
                      public void onFailure(int code, Throwable throwable, String msg) {
                          notifyAllGetTokenListenersFailure(code, throwable, msg);
                          mIsGettingToken = false;
                      }
                  });
            }
        }
    }


    private void notifyAllGetTokenListenersFailure(int code, Throwable throwble, String msg) {
        if (mGetTokenListeners != null) {
            synchronized (mGetTokenListeners) {
                for (RestServiceListener listener : mGetTokenListeners) {
                    listener.onFailure(code, throwble, msg);
                }
                mGetTokenListeners.clear();
            }
        }
    }


    private void notifyAllGetTokenListenersSuccess() {
        if (mGetTokenListeners != null) {
            synchronized (mGetTokenListeners) {
                for (RestServiceListener listener : mGetTokenListeners) {
                    listener.onSuccess(getCurrentAccessToken());
                }
                mGetTokenListeners.clear();
            }
        }
    }


    //TODO this is unreliable if the user changes the time
    private long getTimeNowSeconds() {
        return System.currentTimeMillis() / 1000;
    }


    private void storeToSharedPreferences() {
        mSharedPreferencesFiles.getUserSharedPreferences().edit().putString(TOKEN_MANAGER_SP_KEY, new Gson().toJson(this))
          .commit();
    }
}