package com.roomster.rest.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class ConversationUserProfile implements Serializable {

    /**
     **/
    @SerializedName("UserId")
    private Long userId = null;

    /**
     **/
    @SerializedName("FirstName")
    private String firstName = null;

    /**
     **/
    @SerializedName("LastName")
    private String lastName = null;

    /**
     **/
    @SerializedName("UserPhoto")
    private String userPhoto = null;

    /**
     **/
    @SerializedName("IsBlocked")
    private Boolean isBlocked = null;

    /**
     **/
    @SerializedName("IsActive")
    private Boolean isActive = null;

    /**
     **/
    @SerializedName("IsRemoved")
    private Boolean isRemoved = null;

    /**
     **/
    @SerializedName("IsDeleted")
    private Boolean isDeleted = null;

    /**
     **/
    @SerializedName("Phone")
    private String phone = null;

    /**
     **/
    @SerializedName("SocialConnections")
    private List<SocialConnectionLight> socialConnections = null;


    public Long getUserId() {
        return userId;
    }


    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public String getFirstName() {
        return firstName;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public String getUserPhoto() {
        return userPhoto;
    }


    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }


    public Boolean getIsBlocked() {
        return isBlocked;
    }


    public void setIsBlocked(Boolean isBlocked) {
        this.isBlocked = isBlocked;
    }


    public Boolean getIsActive() {
        return isActive;
    }


    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }


    public Boolean getIsRemoved() {
        return isRemoved;
    }


    public void setIsRemoved(Boolean isRemoved) {
        this.isRemoved = isRemoved;
    }


    public Boolean getIsDeleted() {
        return isDeleted;
    }


    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }


    public String getPhone() {
        return phone;
    }


    public void setPhone(String phone) {
        this.phone = phone;
    }


    public List<SocialConnectionLight> getSocialConnections() {
        return socialConnections;
    }


    public void setSocialConnections(List<SocialConnectionLight> socialConnections) {
        this.socialConnections = socialConnections;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ConversationUserProfile {\n");

        sb.append("  userId: ").append(userId).append("\n");
        sb.append("  firstName: ").append(firstName).append("\n");
        sb.append("  lastName: ").append(lastName).append("\n");
        sb.append("  userPhoto: ").append(userPhoto).append("\n");
        sb.append("  isBlocked: ").append(isBlocked).append("\n");
        sb.append("  isActive: ").append(isActive).append("\n");
        sb.append("  isRemoved: ").append(isRemoved).append("\n");
        sb.append("  isDeleted: ").append(isDeleted).append("\n");
        sb.append("  phone: ").append(phone).append("\n");
        sb.append("  socialConnections: ").append(socialConnections).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
