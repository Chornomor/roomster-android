package com.roomster.rest.api;


import com.roomster.rest.model.AccountDetailsViewModel;
import com.roomster.rest.model.AccountRemovedGetResponse;
import com.roomster.rest.model.AccountRemovedPutQuery;
import com.roomster.rest.model.AccountViewModel;
import com.roomster.rest.model.CreateAccountTokenModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;


public interface AccountApi {

    /**
     * Update account info. All parameters are optional. Only pass what you need to update.
     *
     * @param model
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @param currency
     *   currency (USD, EUR, GBP etc.)
     * @param locale
     *   locale (en-US, ru-RU, ja-JP etc.)
     * @return Call<Object>
     */
    @PUT("Account")
    Call<String> accountPut(@Body AccountDetailsViewModel model, @Header("Authorization") String authorization,
                            @Header("Currency") String currency, @Header("Locale") String locale);

    /**
     * Create user account
     *
     * @param model
     * @param currency
     *   currency (USD, EUR, GBP etc.)
     * @param locale
     *   locale (en-US, ru-RU, ja-JP etc.)
     * @return Call<Object>
     */
    @POST("Account")
    @Headers("Content-Type: application/json")
    Call<CreateAccountTokenModel> accountPost(@Body AccountViewModel model, @Header("Currency") String currency,
                                              @Header("Locale") String locale);

    /**
     * Delete user account Deletes user account that does not have an active subscription.
     *
     * @param authorization
     *   Bearer Token (use /v1/token to acquire)
     * @return Call<Object>
     */
    @DELETE("Account")
    Call<String> accountDelete(@Header("Authorization") String authorization);

    @GET("RemovedUser")
    Call<AccountRemovedGetResponse> getRemovedUser(@Query("access_token") String accessToken, @Query("provider") String provider);

    @PUT("RemovedUser")
    @Headers("Content-Type: application/json")
    Call<String> respondOnRemoval(@Body AccountRemovedPutQuery model);
}
