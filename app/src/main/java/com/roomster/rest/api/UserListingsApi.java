package com.roomster.rest.api;


import com.roomster.rest.model.ListingGetViewModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;


public interface UserListingsApi {

    /**
     * Get user listings
     *
     * @param userId
     *   User Id
     * @param activeOnly
     *   Optional. Default: false
     * @param currency
     *   currency (USD, EUR, GBP etc.)
     * @param locale
     *   locale (en-US, ru-RU, ja-JP etc.)
     * @return Call<List<ListingGetViewModel>>
     */

    @GET("UserListings")
    Call<List<ListingGetViewModel>> userListingsGet(@Query("user_id") Long userId, @Query("active_only") Boolean activeOnly,
                                                    @Header("Currency") String currency, @Header("Locale") String locale);
}
